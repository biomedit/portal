import {
  type CustomAffiliation,
  type DataProvider,
  type DataTransferDataProviderApprovalsInner,
  DataTransferDataProviderApprovalsInnerStatusEnum,
  type DataTransferGroupApprovalsInner,
  DataTransferGroupApprovalsInnerStatusEnum,
  type DataTransferNodeApprovalsInner,
  DataTransferNodeApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInnerTypeEnum,
  type DataTransferPermissions,
  type DataTransferPermissionsEdit,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  type Flag,
  type Group,
  type IdentityProvider,
  type Node,
  NodeNodeStatusEnum,
  type Permission,
  type PgpKeyInfo,
  PgpKeyInfoStatusEnum,
  type Project,
  type ProjectPermissions,
  type ProjectPermissionsEdit,
  type ProjectResourcesInner,
  type ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
  type Terminology,
  type TerminologyVersionsInner,
  TerminologyVersionsInnerStatusEnum,
  type TermsOfUse,
  TermsOfUseVersionTypeEnum,
  type User,
  type Userinfo,
  type UserinfoManages,
  type UserinfoPermissions,
  type UserProfile,
} from './src/api/generated';
import {
  type DataTransferPermissions as DataTransferPagePermission,
  noDataTransferPermissions,
  noPermissions,
  noProjectPermissions,
  type PagePermissions,
  type ProjectPermissions as ProjectPagePermission,
} from './src/permissions';
import type { AuthState } from './src/reducers/auth';
import type { ExpectedDataTransfer } from './src/reducers/dataTransfer';
import { fakerEN } from '@faker-js/faker';
import { snakeCase } from 'lodash';

type Factory<T> = (partialEntity?: Partial<T>) => T;
type SnakeCasedFactory<T> = (partialEntity?: Partial<T>) => Partial<T>;

export function createBatch<T>(
  factory: Factory<T>,
  times: number,
  partialEntity: Partial<T> = {},
): T[] {
  return Array.from({ length: times }, () => factory(partialEntity));
}

export function toSnakeCased<T extends object>(entity: T): Partial<T> {
  return Object.entries(entity).reduce((acc, [key, value]) => {
    return { ...acc, [snakeCase(key)]: value };
  }, {});
}

export function createSnakeCased<T extends object>(
  factory: Factory<T>,
): SnakeCasedFactory<T> {
  return (partialEntity: Partial<T> = {}) =>
    toSnakeCased(factory(partialEntity));
}

export function createNode(node: Partial<Node> = {}): Required<Node> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
    nodeStatus: fakerEN.helpers.enumValue(NodeNodeStatusEnum),
    ticketingSystemEmail: fakerEN.internet.email(),
    objectStorageUrl: fakerEN.internet.url({ protocol: 'https' }),
    oauth2Client: fakerEN.number.int(),
    ipAddressRanges: [],
    ...node,
  };
}

export function createIdentityProvider(
  identityProvider: Partial<IdentityProvider> = {},
) {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
    ...identityProvider,
  };
}

export function createDataProvider(
  dataProvider: Partial<DataProvider> = {},
): Required<DataProvider> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
    enabled: fakerEN.datatype.boolean(),
    nodes: [fakerEN.word.noun()],
    coordinators: [],
    ...dataProvider,
  };
}

export function createUser(user: Partial<User> = {}): Required<User> {
  const id = fakerEN.number.int();
  const firstName = fakerEN.person.firstName();
  const lastName = fakerEN.person.lastName();
  const email = fakerEN.internet.email();
  return {
    email,
    firstName,
    flags: [],
    groups: [],
    id,
    isActive: fakerEN.datatype.boolean(),
    lastLogin: fakerEN.date.recent(),
    lastName,
    profile: createUserProfile(),
    username: fakerEN.internet.username(),
    displayName: `${firstName} ${lastName} (ID: ${id}) (${email})`,
    termsOfUse: [],
    openPgpKeys: [],
    ...user,
  };
}

function createUserProfile(): Required<UserProfile> {
  const institution = fakerEN.word.noun();
  const id = fakerEN.string.numeric();

  return {
    affiliation: institution,
    affiliationId: fakerEN.internet.email({ provider: institution }),
    uid: fakerEN.number.int(),
    gid: fakerEN.number.int(),
    emails: `${fakerEN.internet.email()},${fakerEN.internet.email()}`,
    localUsername: fakerEN.internet.username(),
    displayName: `${fakerEN.person.firstName()} ${fakerEN.person.lastName()} (ID: ${id}) (${fakerEN.internet.email()})`,
    displayId: `ID: ${id}`,
    namespace: fakerEN.location.countryCode(),
    displayLocalUsername: `ID: ${id}`,
    affiliationConsent: fakerEN.datatype.boolean(),
    customAffiliation: fakerEN.number.int(),
  };
}

function createDataTransferApproval() {
  return {
    id: fakerEN.number.int(),
    canApprove: fakerEN.datatype.boolean(),
    rejectionReason: fakerEN.word.words(),
    created: fakerEN.date.recent(),
    changeDate: fakerEN.date.recent(),
  };
}

export function createDataTransferNodeApproval(
  approval: Partial<DataTransferNodeApprovalsInner> = {},
): Required<DataTransferNodeApprovalsInner> {
  return {
    ...createDataTransferApproval(),
    status: fakerEN.helpers.enumValue(DataTransferNodeApprovalsInnerStatusEnum),
    type: DataTransferNodeApprovalsInnerTypeEnum.H,
    node: createNode(),
    ...approval,
  };
}

export function createDataTransferDataProviderApproval(
  approval: Partial<DataTransferDataProviderApprovalsInner> = {},
): Required<DataTransferDataProviderApprovalsInner> {
  return {
    ...createDataTransferApproval(),
    status: fakerEN.helpers.enumValue(
      DataTransferDataProviderApprovalsInnerStatusEnum,
    ),
    dataProvider: createDataProvider(),
    ...approval,
  };
}

export function createDataTransferGroupApproval(
  approval: Partial<DataTransferGroupApprovalsInner> = {},
): Required<DataTransferGroupApprovalsInner> {
  return {
    ...createDataTransferApproval(),
    status: fakerEN.helpers.enumValue(
      DataTransferGroupApprovalsInnerStatusEnum,
    ),
    group: createGroup(),
    ...approval,
  };
}

export function createDataTransferPermissionsEdit(
  edit: Partial<DataTransferPermissionsEdit> = {},
): Required<DataTransferPermissionsEdit> {
  return {
    maxPackages: false,
    dataProvider: false,
    purpose: false,
    status: false,
    requestor: false,
    legalBasis: false,
    dataSpecification: false,
    dataRecipients: false,
    ...edit,
  };
}

export function createDataTransferPermissions(
  permissions: Partial<DataTransferPermissions> = {},
): Required<DataTransferPermissions> {
  return {
    edit: createDataTransferPermissionsEdit(),
    _delete: false,
    ...permissions,
  };
}

export function createDataTransfer(
  dataTransfer: Partial<ExpectedDataTransfer> = {},
): Required<ExpectedDataTransfer> {
  const requestor = createUser();
  const project = createProject();
  const dataProvider = createDataProvider();

  return {
    maxPackages: [-1, 1][fakerEN.number.int(2)],
    project: project.id || fakerEN.number.int(),
    purpose: fakerEN.helpers.enumValue(DataTransferPurposeEnum),
    id: fakerEN.number.int(),
    packages: [],
    nodeApprovals: [createDataTransferNodeApproval()],
    dataProviderApprovals: [
      createDataTransferDataProviderApproval({ dataProvider: dataProvider }),
    ],
    groupApprovals: [createDataTransferGroupApproval()],
    requestorName: `${requestor.firstName} Norris (ID: 63457666153) (${requestor.email})`,
    requestorDisplayId: `ID: ${fakerEN.string.numeric()}`,
    requestorFirstName: requestor.firstName,
    requestorLastName: requestor.lastName,
    requestorFullName: `${requestor.firstName} ${requestor.lastName}`,
    requestorEmail: requestor.email || fakerEN.internet.email(),
    projectName: project.name,
    projectArchived: project.archived,
    status: fakerEN.helpers.enumValue(DataTransferStatusEnum),
    dataProvider: dataProvider.code,
    dataProviderName: dataProvider.name,
    requestor: requestor.id,
    legalBasis: fakerEN.word.words(),
    dataSpecification: fakerEN.word.words(),
    creationDate: fakerEN.date.recent(),
    changeDate: fakerEN.date.recent(),
    permissions: {},
    dataRecipients: [],
    ...dataTransfer,
  };
}

function createProjectResource(): Required<ProjectResourcesInner> {
  return {
    id: fakerEN.number.int(),
    name: fakerEN.word.noun(),
    location: fakerEN.internet.url(),
    description: fakerEN.word.words(),
    contact: fakerEN.internet.email(),
  };
}

export function createProjectUser(
  projectUser: Partial<ProjectUsersInner> = {},
): Required<ProjectUsersInner> {
  const userProfile = createUserProfile();
  const user = createUser({ profile: userProfile });

  return {
    ...user,
    ...userProfile,
    uid: userProfile.uid.toString(),
    gid: userProfile.gid.toString(),
    customAffiliation: fakerEN.string.numeric(),
    authorized: fakerEN.datatype.boolean(),
    termsOfUseAccepted: fakerEN.system.semver(),
    roles: [fakerEN.helpers.enumValue(ProjectUsersInnerRolesEnum)],
    ...projectUser,
  };
}

export function createProjectPermissionsEdit(
  edit: Partial<ProjectPermissionsEdit> = {},
): Required<ProjectPermissionsEdit> {
  return {
    name: false,
    code: false,
    destination: false,
    users: [],
    ipAddressRanges: false,
    resources: false,
    legalSupportContact: false,
    legalApprovalGroup: false,
    dataSpecificationApprovalGroup: false,
    expirationDate: false,
    sshSupport: false,
    resourcesSupport: false,
    servicesSupport: false,
    identityProvider: false,
    additionalInfo: false,
    ...edit,
  };
}

export function createProjectPermissions(
  permissions: Partial<ProjectPermissions> = {},
): Required<ProjectPermissions> {
  return {
    edit: createProjectPermissionsEdit(),
    archive: false,
    ...permissions,
  };
}

export function createProject(
  project: Partial<Project> = {},
): Required<Project> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    created: fakerEN.date.recent(),
    gid: fakerEN.number.int(),
    permissions: {},
    dataTransferCount: fakerEN.number.int(),
    archived: fakerEN.datatype.boolean(),
    name: fakeCode.toUpperCase(),
    code: fakeCode,
    legalApprovalGroup: fakerEN.number.int(),
    dataSpecificationApprovalGroup: fakerEN.number.int(),
    sshSupport: fakerEN.datatype.boolean(),
    resourcesSupport: fakerEN.datatype.boolean(),
    servicesSupport: fakerEN.datatype.boolean(),
    destination: fakerEN.word.noun(),
    legalSupportContact: fakerEN.internet.email(),
    expirationDate: fakerEN.date.soon(),
    ipAddressRanges: [],
    resources: [createProjectResource()],
    users: [createProjectUser()],
    userHasRole: fakerEN.datatype.boolean(),
    identityProvider: 1,
    additionalInfo: 'Additional project information',
    ...project,
  };
}

export function createFlag(flag: Partial<Flag>): Required<Flag> {
  return {
    id: fakerEN.number.int(),
    code: fakerEN.word.noun(),
    description: fakerEN.word.words(),
    users: [],
    ...flag,
  };
}

export function createCustomAffiliation(): Required<CustomAffiliation> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
  };
}

export function createGroup(group: Partial<Group> = {}): Required<Group> {
  return {
    id: fakerEN.number.int(),
    name: fakerEN.word.noun(),
    profile: {},
    users: [],
    permissions: [],
    permissionsObject: [],
    ...group,
  };
}

export function createPgpKeyInfo(
  pgpKeyInfo: Partial<PgpKeyInfo> = {},
): Required<PgpKeyInfo> {
  return {
    fingerprint: fakerEN.string.hexadecimal({ length: 40 }),
    status: fakerEN.helpers.enumValue(PgpKeyInfoStatusEnum),
    id: fakerEN.number.int(),
    keyUserId: `${fakerEN.person.firstName} ${fakerEN.person.lastName}`,
    keyEmail: fakerEN.internet.email(),
    ...pgpKeyInfo,
  };
}

export function createAuthState(
  authState: Partial<AuthState> = {},
): Required<AuthState> {
  return {
    isAuthenticated: true,
    isFetching: false,
    isSubmitting: false,
    isExistingUsername: false,
    invalidMsg: fakerEN.word.words(),
    user: createUserinfo(),
    pagePermissions: createPagePermissions(),
    ...authState,
  };
}

export function createUserinfo(
  userinfo: Partial<Userinfo> = {},
): Required<Userinfo> {
  const user = createUser();

  return {
    ipAddress: fakerEN.internet.ip(),
    permissions: createUserinfoPermissions(),
    manages: createUserinfoManages(),
    ...user,
    username: user.username || fakerEN.internet.username(),
    id: user.id || fakerEN.number.int(),
    ...userinfo,
  };
}

function createUserinfoPermissions(
  userinfoPermissions: Partial<UserinfoPermissions> = {},
): Required<UserinfoPermissions> {
  return {
    manager: false,
    staff: false,
    dataManager: false,
    projectLeader: false,
    dataProviderAdmin: false,
    dataProviderViewer: false,
    nodeAdmin: false,
    nodeViewer: false,
    groupManager: false,
    hasProjects: false,
    legalApprover: false,
    dataSpecificationApprover: false,
    ...userinfoPermissions,
  };
}

function createUserinfoManages(
  userinfoManages: Partial<UserinfoManages> = {},
): Required<UserinfoManages> {
  return {
    dataProviderAdmin: [],
    nodeAdmin: [],
    ...userinfoManages,
  };
}

export function createPagePermissions(
  pagePermissions: Partial<PagePermissions> = {},
): Required<PagePermissions> {
  return {
    ...noPermissions,
    ...pagePermissions,
  };
}

export function createProjectPagePermissions(
  projectPermissions: Partial<ProjectPagePermission> = {},
): Required<ProjectPagePermission> {
  return {
    ...noProjectPermissions,
    ...projectPermissions,
  };
}

function createDataTransferPagePermissions(
  dataTransferPermissions: Partial<DataTransferPagePermission> = {},
): Required<DataTransferPagePermission> {
  return {
    ...noDataTransferPermissions,
    ...dataTransferPermissions,
  };
}

export function createNodeAdminAuthState(nodes: Node[] = [createNode()]) {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        manager: true,
        dataProviderAdmin: true,
        nodeAdmin: true,
        hasProjects: true,
      }),
      manages: createUserinfoManages({ nodeAdmin: nodes }),
    }),
    pagePermissions: createPagePermissions({
      project: createProjectPagePermissions({ add: true, edit: true }),
      dataTransfer: createDataTransferPagePermissions({ nodeAdmin: true }),
    }),
  });
}

export function createDataProviderAdminAuthState(
  dataProviders: DataProvider[] = [createDataProvider()],
) {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        dataProviderAdmin: true,
      }),
      manages: createUserinfoManages({
        dataProviderAdmin: dataProviders,
      }),
    }),
    pagePermissions: createPagePermissions({
      dataTransfer: createDataTransferPagePermissions({
        dataProviderAdmin: true,
      }),
    }),
  });
}

export function createProjectLeaderAuthState() {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        hasProjects: true,
        manager: true,
        projectLeader: true,
      }),
    }),
    pagePermissions: createPagePermissions({
      project: createProjectPagePermissions({
        add: true,
        view: true,
      }),
      dataTransfer: createDataTransferPagePermissions({
        hasProjects: true,
      }),
    }),
  });
}

export function createProjectUserAuthState() {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        hasProjects: true,
      }),
    }),
    pagePermissions: createPagePermissions({
      project: createProjectPagePermissions({
        view: true,
      }),
      dataTransfer: createDataTransferPagePermissions({
        hasProjects: true,
      }),
    }),
  });
}

export function createStaffAuthState() {
  return createAuthState({
    user: createUserinfo({
      permissions: createUserinfoPermissions({
        staff: true,
        dataManager: true,
        hasProjects: true,
      }),
    }),
    pagePermissions: createPagePermissions({
      project: createProjectPagePermissions({
        add: true,
        edit: true,
        del: true,
      }),
      dataTransfer: createDataTransferPagePermissions({
        staff: true,
      }),
    }),
  });
}

export function createPermission(
  permission: Partial<Permission> = {},
): Required<Permission> {
  const verb = fakerEN.word.verb();
  const noun = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    name: `Can ${verb} ${noun}`,
    codename: `${verb}_${noun}`,
    contentType: fakerEN.number.int(),
    ...permission,
  };
}

type PermissionObject = { id: number; name: string };

export function createPermissionObject(
  permissionObject: Partial<PermissionObject>,
): Required<PermissionObject> {
  return {
    id: fakerEN.number.int(),
    name: fakerEN.word.noun(),
    ...permissionObject,
  };
}

export function createObjectByPermission(
  perm_id: number = fakerEN.number.int(),
  objects: PermissionObject[] = [],
) {
  return { [perm_id]: objects };
}

export function createTerminology(
  terminology: Partial<Terminology> = {},
): Required<Terminology> {
  const fakeCode = fakerEN.word.noun();

  return {
    id: fakerEN.number.int(),
    code: fakeCode,
    name: fakeCode.toUpperCase(),
    contact: fakerEN.internet.email(),
    description: fakerEN.word.words(),
    copyright: fakerEN.word.words(),
    documentation: fakerEN.word.words(),
    confirmationCheckboxText: fakerEN.word.words(),
    confirmationDescription: fakerEN.word.words(),
    versions: [],
    ...terminology,
  };
}

export function createVersion(
  version: Partial<TerminologyVersionsInner> = {},
): Required<TerminologyVersionsInner> {
  return {
    id: fakerEN.number.int(),
    files: [],
    name: fakerEN.word.noun(),
    released: fakerEN.date.recent(),
    status: fakerEN.helpers.enumValue(TerminologyVersionsInnerStatusEnum),
    ...version,
  };
}

export function createTermsOfUse(
  termsOfUse: Partial<TermsOfUse> = {},
): Required<TermsOfUse> {
  return {
    id: fakerEN.number.int(),
    created: fakerEN.date.recent(),
    node: fakerEN.number.int(),
    versionType: fakerEN.helpers.enumValue(TermsOfUseVersionTypeEnum),
    version: fakerEN.system.semver(),
    text: fakerEN.word.words(),
    users: [],
    ...termsOfUse,
  };
}
