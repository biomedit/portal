module.exports = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  keySeparator: '.',
  interpolation: {
    format: (value, format) => {
      if (format === 'lowercase') {
        return value.toLowerCase();
      }
      return value;
    },
  },
  transpilePackages: ['i18next'],
  serializeConfig: false,
};
