import { isGroupWithRole, useGroups } from './RolesTable';
import { createGroup } from '../../factories';
import { GroupProfileRoleEnum } from '../api/generated';

describe('isGroupWithRole', () => {
  it.each`
    group                                             | expected
    ${{ profile: { role: GroupProfileRoleEnum.NA } }} | ${true}
    ${{ profile: {} }}                                | ${false}
    ${{}}                                             | ${false}
  `(
    'should return $expected when the group is $group and profile is $group.profile',
    ({ group, expected }) => {
      expect(isGroupWithRole(group)).toBe(expected);
    },
  );
});

describe('useGroups', () => {
  it('should filter out users of groups with no role', () => {
    const groups = [
      createGroup({
        profile: { role: GroupProfileRoleEnum.NA },
        users: [{ id: 1 }],
      }),
      createGroup({ profile: {}, users: [{ id: 2 }] }),
    ];

    expect(useGroups(groups)).toHaveLength(1);
  });

  it('should not return the same user twice', () => {
    const groups = [
      createGroup({
        profile: { role: GroupProfileRoleEnum.NA },
        users: [{ id: 1 }],
      }),
      createGroup({
        profile: { role: GroupProfileRoleEnum.NV },
        users: [{ id: 1 }],
      }),
    ];

    expect(useGroups(groups)).toHaveLength(1);
  });

  it('should return all roles for a user', () => {
    const groups = [
      createGroup({
        profile: { role: GroupProfileRoleEnum.NA },
        users: [{ id: 1 }],
      }),
      createGroup({
        profile: { role: GroupProfileRoleEnum.NV },
        users: [{ id: 1 }],
      }),
    ];

    expect(useGroups(groups)[0]).toEqual({
      id: 1,
      roles: [GroupProfileRoleEnum.NA, GroupProfileRoleEnum.NV],
    });
  });
});
