import { type ColumnDef, createColumnHelper } from '@tanstack/react-table';
import { EnhancedTable, type IdType } from '@biomedit/next-widgets';
import {
  type Group,
  type GroupProfile,
  GroupProfileRoleEnum,
  ProjectUsersInnerRolesEnum,
} from '../api/generated';
import React, { useMemo } from 'react';
import DoneIcon from '@mui/icons-material/Done';
import { I18nNamespace } from '../i18n';
import { useTranslation } from 'react-i18next';

type Role = GroupProfileRoleEnum | ProjectUsersInnerRolesEnum;

type RolesTableUser = IdType & {
  email?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  roles: Role[];
};

type RolesTableProps<T extends RolesTableUser> = {
  additionalColumns?: ColumnDef<T>[];
  isFetching: boolean;
  roles: Role[];
  users: T[];
};

type GroupWithRole = Group & {
  profile: GroupProfile & Required<Pick<GroupProfile, 'role'>>;
};

export function isGroupWithRole(group: Group): group is GroupWithRole {
  return !!group.profile && !!group.profile.role;
}

export const useGroups = (groups: Group[]) => {
  const ids = new Set();
  const users = groups
    .filter(isGroupWithRole)
    .flatMap((group: Group) => group.users)
    .filter((user) => {
      if (ids.has(user.id)) {
        return false;
      }
      ids.add(user.id);
      return true;
    });
  return users.map((user) => {
    return {
      ...user,
      roles: groups
        .filter(isGroupWithRole)
        .filter((group) => group.users.some((u) => u.id === user.id))
        .map((group) => group.profile.role),
    };
  });
};

export const RolesTable = <T extends RolesTableUser>({
  users,
  roles,
  isFetching,
  additionalColumns,
}: RolesTableProps<T>) => {
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const columnHelper = createColumnHelper<T>();
  const columns = useMemo(() => {
    return [
      columnHelper.accessor((item) => item.email, {
        header: t('columns.email'),
      }),
      columnHelper.accessor((item) => item.firstName, {
        header: t('columns.firstName'),
      }),
      columnHelper.accessor((item) => item.lastName, {
        header: t('columns.lastName'),
      }),
      ...roles.map(
        (role: GroupProfileRoleEnum | ProjectUsersInnerRolesEnum) => {
          return columnHelper.display({
            id: role,
            header: t(`columns.${role}`),
            cell: (context) => {
              return context.row.original.roles.includes(role) && <DoneIcon />;
            },
          });
        },
      ),
      ...(additionalColumns ?? []),
    ];
  }, [additionalColumns, columnHelper, roles, t]);

  return (
    <EnhancedTable
      itemList={users}
      columns={columns}
      isFetching={isFetching}
      isSubmitting={false}
      emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.user_plural`),
      })}
    />
  );
};
