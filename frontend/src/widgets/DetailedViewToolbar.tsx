import {
  Button,
  ButtonBox,
  DeleteDialog,
  useDeleteDialog,
} from '@biomedit/next-widgets';
import React, {
  type PropsWithChildren,
  type ReactElement,
  type ReactNode,
} from 'react';

export type DetailedViewToolbarProps = PropsWithChildren & {
  additionalButtons?: Array<ReactNode>;
  canDelete: boolean;
  canEdit: boolean;
  deleteConfirmationText?: string;
  entityName?: string;
  entityType: string;
  isSubmitting?: boolean;
  itemId?: number;
  onDeleteConfirmation?: () => void;
  onEditClick?: () => void;
};

export const DetailedViewToolbar = ({
  additionalButtons,
  canDelete,
  canEdit,
  children,
  itemId,
  isSubmitting = false,
  deleteConfirmationText,
  onEditClick,
  onDeleteConfirmation,
  entityType,
  entityName,
}: DetailedViewToolbarProps): ReactElement => {
  const { handleDeleteOpen, handleDeleteClose, deleteOpen } = useDeleteDialog({
    deleteItem: onDeleteConfirmation,
  });

  return (
    <>
      <DeleteDialog
        open={deleteOpen}
        itemName={entityType}
        confirmationText={deleteConfirmationText}
        isSubmitting={isSubmitting}
        onDelete={(isConfirmed: boolean) =>
          handleDeleteClose && handleDeleteClose(isConfirmed)
        }
      />
      {children}
      <ButtonBox sx={{ display: 'flex', flexWrap: 'nowrap' }}>
        {canEdit && (
          <Button
            key={`edit-button-${itemId}`}
            variant="contained"
            aria-label={`Edit ${entityType}`}
            onClick={onEditClick}
            title={`Edit ${entityType} ${entityName}`}
            style={{ marginRight: '8px', boxShadow: 'none' }}
          >
            {`Edit ${entityType}`}
          </Button>
        )}
        {canDelete && (
          <Button
            key={`delete-button-${itemId}`}
            variant="outlined"
            aria-label={`Delete ${entityType} ${entityName}`}
            onClick={() => handleDeleteOpen(itemId, deleteConfirmationText)}
            title={`Delete ${entityType} ${entityName}`}
            style={{ marginRight: '8px' }}
          >
            Delete
          </Button>
        )}
        {/* Render additional buttons if present */}
        {additionalButtons ?? []}
      </ButtonBox>
    </>
  );
};
