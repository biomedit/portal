import { Box, Typography } from '@mui/material';
import type {
  DataTransferDataRecipientsInner,
  ProjectUsersInner,
} from '../api/generated';
import {
  EmailLink,
  type FormattedItemField,
  Tooltip,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { getFullName } from '../utils';
import { I18nNamespace } from '../i18n';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { useTranslation } from 'next-i18next';
import VerifiedIcon from '@mui/icons-material/Verified';

export function addMultiValueField(
  fieldName: string,
  fieldValues: Array<ReactElement | string> | undefined,
  infoBox: FormattedItemField[],
) {
  if (!!fieldValues && fieldValues.length) {
    infoBox.push({
      caption: fieldName,
      component: (
        <div>
          {fieldValues.map((field) => {
            const key = typeof field === 'string' ? field : field.key;
            return (
              <Typography key={key} variant={'body1'}>
                {field}
              </Typography>
            );
          })}
        </div>
      ),
    });
  }
}

export type UserType =
  | ProjectUsersInner
  | (DataTransferDataRecipientsInner & {
      authorized?: boolean;
    });
export type RoleMap = { [name: string]: UserType[] };

export function useUserRoleEntries<T extends RoleMap>(
  roles: T,
  translation: I18nNamespace,
): FormattedItemField[] {
  const { t } = useTranslation([translation]);

  return useMemo((): FormattedItemField[] => {
    const fields: FormattedItemField[] = [];
    const userNameAndEmailLink = (role: string, u: UserType) => (
      // `email` is usually specified
      <Box component="span" key={role + '-' + u.username}>
        {u?.firstName || u?.lastName ? getFullName(u) : u.username}
        <EmailLink email={u.email ?? ''}>
          <MailOutlineIcon sx={{ marginLeft: 1, verticalAlign: 'bottom' }} />
        </EmailLink>
        {u.authorized && (
          <Tooltip title={t(`${translation}:authorizedTooltip`)}>
            <VerifiedIcon
              color={'success'}
              sx={{ verticalAlign: 'bottom' }}
              aria-label={t(`${translation}:authorizedTooltip`)}
            />
          </Tooltip>
        )}
      </Box>
    );
    for (const role in roles) {
      const users = roles[role];
      if (role && users && users.length > 0) {
        addMultiValueField(
          t(`${translation}:role.${role}`),
          users.map((user) => userNameAndEmailLink(role, user)),
          fields,
        );
      }
    }
    return fields;
  }, [t, translation, roles]);
}
