import {
  ipAddress,
  LabelledArrayField,
  maxValue,
  minValue,
  MultilineField,
  required,
} from '@biomedit/next-widgets';
import React, { type ReactElement } from 'react';
import { I18nNamespace } from '../i18n';
import type { NodeIpAddressRangesInner } from '../api/generated';
import { useTranslation } from 'next-i18next';

export const arrayName = 'ipAddressRanges';

export const IpRangeList = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.IP_RANGE_LIST);

  const renderChild = (field: Record<string, string>, index: number) => (
    <>
      <LabelledArrayField
        field={field}
        index={index}
        type="text"
        label={t('captions.ipAddress')}
        validations={[required, ipAddress]}
        arrayName={arrayName}
        fieldName="ipAddress"
        sx={{ width: '30%', marginRight: 1 }}
      />
      <LabelledArrayField
        field={field}
        index={index}
        type="number"
        label={t('captions.mask')}
        validations={[required, minValue(0), maxValue(32)]}
        arrayName={arrayName}
        fieldName="mask"
        sx={{ width: '30%' }}
      />
    </>
  );

  return <MultilineField arrayName={arrayName} renderChild={renderChild} />;
};

export const IpRangeSummary = (
  ipAddressRanges: NodeIpAddressRangesInner[],
): ReactElement[] => {
  return ipAddressRanges.map((range) => {
    const ipAddressRange = `${range.ipAddress}/${range.mask}`;
    return <div key={ipAddressRange}>{ipAddressRange}</div>;
  });
};
