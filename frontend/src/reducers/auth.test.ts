import {
  createAuthState,
  createDataProviderAdminAuthState,
  createNodeAdminAuthState,
  createProjectLeaderAuthState,
  createProjectUserAuthState,
  createStaffAuthState,
} from '../../factories';
import { pagePermissions } from './auth';

describe('pagePermissions', () => {
  describe('project', () => {
    it.each`
      projectPermissions                                      | authState                             | role
      ${{ del: true, add: true, edit: true, view: true }}     | ${createStaffAuthState()}             | ${'staff'}
      ${{ del: false, add: true, edit: true, view: true }}    | ${createNodeAdminAuthState()}         | ${'node admin'}
      ${{ del: false, add: false, edit: true, view: true }}   | ${createProjectLeaderAuthState()}     | ${'project leader'}
      ${{ del: false, add: false, edit: false, view: true }}  | ${createProjectUserAuthState()}       | ${'project user'}
      ${{ del: false, add: false, edit: false, view: false }} | ${createDataProviderAdminAuthState()} | ${'data provider personnel'}
      ${{ del: false, add: false, edit: false, view: false }} | ${createAuthState()}                  | ${'regular user'}
    `(
      'Should return $projectPermissions for $role',
      ({ projectPermissions, authState }) => {
        expect(pagePermissions(authState.user.permissions).project).toEqual(
          projectPermissions,
        );
      },
    );
  });
});
