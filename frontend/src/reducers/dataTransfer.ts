import {
  ADD_DATA_TRANSFER,
  DELETE_DATA_TRANSFER,
  LOAD_DATA_TRANSFERS,
  RETRIEVE_DATA_TRANSFER,
  UPDATE_APPROVAL,
  UPDATE_DATA_TRANSFER,
} from '../actions/actionTypes';
import type { ApiAction, BaseReducerState } from '@biomedit/next-widgets';
import type {
  DataTransfer,
  DataTransferLite,
  DataTransferLitePackagesInner,
  DataTransferPackagesInner,
} from '../api/generated';
import {
  onAddSuccess,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onRetrieveSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';
import type { IdRequired } from '../global';
import { produce } from 'immer';

type ExpectedDataTransferLite = IdRequired<
  DataTransferLite & {
    packages: Array<IdRequired<DataTransferLitePackagesInner>>;
  }
>;

export type ExpectedDataTransfer = IdRequired<
  DataTransfer & {
    packages: Array<IdRequired<DataTransferPackagesInner>>;
  }
>;

export type DataTransferState = BaseReducerState<
  ExpectedDataTransferLite | ExpectedDataTransfer
>;

export const initialDataTransferState: DataTransferState = {
  isFetching: false,
  isSubmitting: false,
  itemList: [],
};

export function dataTransfers(
  state: DataTransferState = initialDataTransferState,
  action:
    | ApiAction<typeof ADD_DATA_TRANSFER>
    | ApiAction<typeof DELETE_DATA_TRANSFER>
    | ApiAction<typeof LOAD_DATA_TRANSFERS>
    | ApiAction<typeof RETRIEVE_DATA_TRANSFER>
    | ApiAction<typeof UPDATE_APPROVAL>
    | ApiAction<typeof UPDATE_DATA_TRANSFER>,
): DataTransferState {
  return produce(state, (draft: DataTransferState) => {
    switch (action.type) {
      case LOAD_DATA_TRANSFERS.request:
      case RETRIEVE_DATA_TRANSFER.request:
        onFetchRequest(draft);
        break;
      case LOAD_DATA_TRANSFERS.success:
        onLoadSuccess(draft, action.response as ExpectedDataTransfer[]);
        break;
      case LOAD_DATA_TRANSFERS.failure:
      case RETRIEVE_DATA_TRANSFER.failure:
        onFetchFailure(draft);
        break;
      case ADD_DATA_TRANSFER.request:
      case UPDATE_DATA_TRANSFER.request:
      case DELETE_DATA_TRANSFER.request:
      case UPDATE_APPROVAL.request:
        onSubmitRequest(draft);
        break;
      case UPDATE_DATA_TRANSFER.success:
      case UPDATE_APPROVAL.success: {
        onUpdateSuccess(draft, action.response as ExpectedDataTransfer);
        break;
      }
      case ADD_DATA_TRANSFER.failure:
      case UPDATE_DATA_TRANSFER.failure:
      case DELETE_DATA_TRANSFER.failure:
      case UPDATE_APPROVAL.failure:
        onSubmitFailure(draft);
        break;
      case DELETE_DATA_TRANSFER.success: {
        onDeleteSuccess(draft, action?.requestArgs?.id);
        break;
      }
      case ADD_DATA_TRANSFER.success:
        onAddSuccess(draft, action.response as ExpectedDataTransfer);
        break;
      case RETRIEVE_DATA_TRANSFER.success:
        onRetrieveSuccess(draft, action.response as ExpectedDataTransfer);
    }
  });
}
