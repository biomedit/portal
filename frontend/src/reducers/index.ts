import {
  ADD_DATA_PROVIDER,
  ADD_FLAG,
  ADD_LOCAL_USER,
  ADD_NODE,
  ADD_TERMS_OF_USE,
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_DATA_PROVIDERS,
  LOAD_FEED,
  LOAD_FLAGS,
  LOAD_IDENTITY_PROVIDERS,
  LOAD_MEMBER_SERVICES,
  LOAD_NODES,
  LOAD_OAUTH2_CLIENTS,
  LOAD_PERMISSIONS,
  LOAD_PROJECT_USER_ROLE_HISTORY,
  LOAD_QUICK_ACCESSES,
  LOAD_TERMINOLOGIES,
  LOAD_TERMS_OF_USE,
  LOAD_USERS,
  RETRIEVE_DATA_PROVIDER,
  RETRIEVE_NODE,
  RETRIEVE_TERMS_OF_USE,
  RETRIEVE_USER,
  UPDATE_APPROVAL,
  UPDATE_DATA_PROVIDER,
  UPDATE_FLAG,
  UPDATE_NODE,
  UPDATE_USER,
} from '../actions/actionTypes';
import type {
  Approval,
  CustomAffiliation,
  DataProvider,
  Feed,
  Flag,
  IdentityProvider,
  Node,
  OAuth2Client,
  Permission,
  ProjectUserRoleHistory,
  QuickAccessTile,
  Service,
  Terminology,
  TerminologyVersionsInner,
  TermsOfUse,
  TermsOfUseUsersInner,
  User,
} from '../api/generated';
import { reducer, toast } from '@biomedit/next-widgets';
import { auth } from './auth';
import { combineReducers } from 'redux';
import { dataTransfers } from './dataTransfer';
import { groupReducer } from './group';
import type { IdRequired } from '../global';
import { pgpKeyInfo } from './pgpKeyInfo';
import { projectReducer } from './project';
import { sshPublicKey } from './sshPublicKey';

export const reducers = combineReducers({
  auth,
  toast,
  project: projectReducer,
  dataTransfers,
  groups: groupReducer,
  pgpKeyInfo: pgpKeyInfo,
  sshPublicKey,
  users: reducer<IdRequired<User>>({
    load: LOAD_USERS,
    update: UPDATE_USER,
    add: ADD_LOCAL_USER,
    retrieve: RETRIEVE_USER,
  }),
  clients: reducer<IdRequired<OAuth2Client>>({
    load: LOAD_OAUTH2_CLIENTS,
  }),
  nodes: reducer<IdRequired<Node>>({
    load: LOAD_NODES,
    update: UPDATE_NODE,
    add: ADD_NODE,
    retrieve: RETRIEVE_NODE,
  }),
  dataProvider: reducer<IdRequired<DataProvider>>({
    load: LOAD_DATA_PROVIDERS,
    update: UPDATE_DATA_PROVIDER,
    add: ADD_DATA_PROVIDER,
    retrieve: RETRIEVE_DATA_PROVIDER,
  }),
  identityProviders: reducer<IdRequired<IdentityProvider>>({
    load: LOAD_IDENTITY_PROVIDERS,
  }),
  feed: reducer<IdRequired<Feed>>({ load: LOAD_FEED }),
  flags: reducer<IdRequired<Flag>>({
    load: LOAD_FLAGS,
    update: UPDATE_FLAG,
    add: ADD_FLAG,
  }),
  permissions: reducer<IdRequired<Permission>>({
    load: LOAD_PERMISSIONS,
  }),
  quickAccesses: reducer<IdRequired<QuickAccessTile>>({
    load: LOAD_QUICK_ACCESSES,
  }),
  approval: reducer<IdRequired<Approval>>({
    update: UPDATE_APPROVAL,
  }),
  customAffiliation: reducer<IdRequired<CustomAffiliation>>({
    load: LOAD_CUSTOM_AFFILIATIONS,
  }),
  memberServices: reducer<IdRequired<Service>>({
    load: LOAD_MEMBER_SERVICES,
  }),
  terminologies: reducer<
    IdRequired<Terminology> & {
      versions: Array<IdRequired<TerminologyVersionsInner>>;
    }
  >({
    load: LOAD_TERMINOLOGIES,
  }),
  termsOfUse: reducer<
    IdRequired<TermsOfUse & { users: Array<TermsOfUseUsersInner> }>
  >({
    load: LOAD_TERMS_OF_USE,
    retrieve: RETRIEVE_TERMS_OF_USE,
    add: ADD_TERMS_OF_USE,
  }),
  projectUserRoleHistory: reducer<IdRequired<ProjectUserRoleHistory>>({
    load: LOAD_PROJECT_USER_ROLE_HISTORY,
  }),
});
