import {
  ADD_PROJECT,
  CHECK_AUTH,
  LOGIN,
  type LoginAction,
  UPDATE_PROJECT,
  UPDATE_USER_PROFILE,
} from '../actions/actionTypes';
import {
  type ApiAction,
  type DeepWriteable,
  isSerializedResponse,
  type SerializedResponse,
} from '@biomedit/next-widgets';
import { noPermissions, type PagePermissions } from '../permissions';
import type { Project, Userinfo, UserinfoPermissions } from '../api/generated';
import { produce } from 'immer';
import { StatusCodes } from 'http-status-codes';

export type AuthState = {
  invalidMsg: string | null;
  isAuthenticated: boolean;
  isExistingUsername: boolean;
  isFetching: boolean;
  isSubmitting: boolean;
  pagePermissions: PagePermissions;
  user: Userinfo | null;
};

export const initialState: AuthState = {
  isAuthenticated: false,
  isFetching: false,
  isSubmitting: false,
  isExistingUsername: false,
  invalidMsg: null,
  user: null,
  pagePermissions: noPermissions,
};

export function auth(
  state: AuthState = initialState,
  action:
    | ApiAction<typeof ADD_PROJECT>
    | ApiAction<typeof CHECK_AUTH>
    | ApiAction<typeof UPDATE_PROJECT>
    | ApiAction<typeof UPDATE_USER_PROFILE>
    | LoginAction,
): AuthState {
  return produce(state, (draft: AuthState) => {
    switch (action.type) {
      case CHECK_AUTH.request:
        draft.isFetching = true;
        break;
      case LOGIN:
        draft.isFetching = false;
        draft.user = action.response;
        if (action.response.permissions) {
          draft.pagePermissions = pagePermissions(action.response.permissions);
        }
        draft.isAuthenticated = true;
        break;
      case CHECK_AUTH.failure:
        draft.isFetching = false;
        draft.isAuthenticated = false;
        break;
      case UPDATE_USER_PROFILE.request:
        draft.isSubmitting = true;
        draft.invalidMsg = null;
        draft.isExistingUsername = false;
        break;
      case UPDATE_USER_PROFILE.success:
        draft.isSubmitting = false;
        if (action.response && draft.user) {
          // action.response is of type User (NOT Userinfo), so we need to merge
          // because User doesn't contain as much data as Userinfo
          draft.user = Object.assign({}, draft.user, action.response);
        }
        break;
      case UPDATE_USER_PROFILE.failure: {
        draft.isSubmitting = false;
        if (isSerializedResponse(action.error)) {
          const serializedResponse = action.error as SerializedResponse;
          const status = serializedResponse.status;
          if (status === StatusCodes.CONFLICT) {
            draft.isExistingUsername = true;
          } else if (status === StatusCodes.UNPROCESSABLE_ENTITY) {
            draft.invalidMsg = (
              serializedResponse.body as Record<string, string>
            ).detail;
          }
        }
        break;
      }
      case ADD_PROJECT.success:
      case UPDATE_PROJECT.success:
        if (action.response) {
          updateHasProjects(draft.user, action.response);
        }
        break;
      case ADD_PROJECT.failure:
      case ADD_PROJECT.request:
      case CHECK_AUTH.success:
      case UPDATE_PROJECT.failure:
      case UPDATE_PROJECT.request:
        break;
    }
  });
}

export function pagePermissions(roles: UserinfoPermissions): PagePermissions {
  return {
    project: {
      del: !!roles.staff,
      add: !!roles.staff || !!roles.nodeAdmin,
      edit: !!(roles.manager || roles.staff || roles.nodeAdmin),
      view: !!(
        roles.hasProjects ||
        roles.staff ||
        roles.nodeAdmin ||
        roles.nodeViewer
      ),
    },
    dataTransfer: {
      hasProjects: !!roles.hasProjects,
      staff: !!roles.staff,
      dataProviderAdmin: !!roles.dataProviderAdmin,
      dataProviderViewer: !!roles.dataProviderViewer,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
      legalApprover: !!roles.legalApprover,
      dataSpecificationApprover: !!roles.dataSpecificationApprover,
    },
    user: {
      staff: !!roles.staff,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
    },
    node: {
      staff: !!roles.staff,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
    },
    dataProvider: {
      staff: !!roles.staff,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
      dataProviderAdmin: !!roles.dataProviderAdmin,
      dataProviderViewer: !!roles.dataProviderViewer,
    },
    group: {
      staff: !!roles.staff,
      groupManager: !!roles.groupManager,
    },
    flag: {
      staff: !!roles.staff,
    },
    openPgpKey: {
      staff: !!roles.staff,
    },
    termsOfUse: {
      staff: !!roles.staff,
      nodeAdmin: !!roles.nodeAdmin,
      nodeViewer: !!roles.nodeViewer,
    },
  };
}

/**
 * Sets {@code hasProjects} to {@code true}, if the currently logged in user
 * has {@code hasProjects = false} and is in the users of a created or modified project.
 *
 * {@code hasProjects} is only updated when the user logs in, which means when the user
 * doesn't have any projects after login, but does after adding themselves to the
 * users of a project, it would not be changed automatically, since no login is performed
 * again.
 */
function updateHasProjects(
  user: DeepWriteable<Userinfo> | null,
  project: Project,
) {
  if (
    user?.permissions &&
    !user?.permissions?.hasProjects &&
    project.users?.some(
      (projectUser) =>
        projectUser?.username === user?.username && projectUser?.roles?.length,
    )
  ) {
    user.permissions.hasProjects = true;
  }
}
