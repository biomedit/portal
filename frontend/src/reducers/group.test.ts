import type { Action, Reducer } from 'redux';
import {
  DELETE_GROUP,
  LOAD_GROUPS,
  UPDATE_GROUP,
} from '../actions/actionTypes';
import {
  groupReducer,
  type GroupState,
  initialGroupState,
  withUpdatedPermissionsObject,
} from './group';
import { makeMockStore, requestAction } from '@biomedit/next-widgets';
import { createGroup } from '../../factories';
import type { Group } from '../api/generated';
import { mockSuccessfulApiCall } from '../testUtils';
import { rootSaga } from '../actions/sagas';
import type { RootState } from '../store';
import type SagaTester from 'redux-saga-tester';

describe('projectReducer', () => {
  let store: SagaTester<Partial<RootState>>;

  beforeEach(() => {
    store = makeMockStore(
      { groups: initialGroupState },
      {
        groups: groupReducer as Reducer<GroupState, Action>,
      },
      rootSaga,
    );
  });

  it('should add groups to `itemList` when `LOAD_GROUPS` is dispatched', async () => {
    // given
    const group = createGroup();

    mockSuccessfulApiCall<Group[]>('listGroups', [group, createGroup()]);

    // when
    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // then
    const itemList = store.getState().groups?.itemList;

    expect(itemList).toHaveLength(2);
    // @ts-expect-error We already now from the previous expect that the list is not empty
    expect(itemList[0]).toMatchObject(group);
  });

  it('should set the store to initial state when `LOAD_GROUPS` returns nothing', async () => {
    // given
    mockSuccessfulApiCall('listGroups', []);

    // when
    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // then
    const groupState = store.getState().groups;
    expect(groupState).toMatchObject(initialGroupState);
  });

  it('should update group in `itemList` when `UPDATE_GROUP` is dispatched', async () => {
    // given
    const updatedItemId = 1;
    const group = createGroup({ id: updatedItemId });
    const updatedItemName = 'New name';
    const updatedItem = { ...group, name: updatedItemName };

    mockSuccessfulApiCall<Group[]>('listGroups', [group]);
    mockSuccessfulApiCall<Group>('updateGroup', updatedItem);

    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // when
    store.dispatch(
      requestAction(UPDATE_GROUP, {
        id: updatedItemId.toString(),
        group: updatedItem,
      }),
    );
    await store.waitFor(UPDATE_GROUP.success);

    // then
    const itemList = store.getState().groups?.itemList;
    expect(itemList).toHaveLength(1);
    // @ts-expect-error We already now from the previous expect that the list is not empty
    expect(itemList[0].name).toMatch(updatedItem.name);
  });

  it('should remove group from `itemList` when `DELETE_GROUP` is dispatched', async () => {
    // given
    const deleteItemId = 1;

    mockSuccessfulApiCall('destroyGroup', null);
    mockSuccessfulApiCall<Group[]>('listGroups', [
      createGroup({ id: deleteItemId }),
    ]);

    store.dispatch(requestAction(LOAD_GROUPS));
    await store.waitFor(LOAD_GROUPS.success);

    // when
    store.dispatch(
      requestAction(DELETE_GROUP, { id: deleteItemId.toString() }),
    );
    await store.waitFor(DELETE_GROUP.success);

    // then
    const itemList = store.getState().groups?.itemList;
    expect(itemList).toHaveLength(0);
  });
});

describe('withUpdatedPermissionsObject', () => {
  const group = createGroup({
    id: 1,
    permissionsObject: [
      { permission: 10, objects: [1, 2] },
      { permission: 12, objects: [2] },
    ],
  });

  it.each`
    deletedItemId | expected
    ${1}          | ${[{ permission: 10, objects: [2] }, { permission: 12, objects: [2] }]}
    ${2}          | ${[{ permission: 10, objects: [1] }]}
    ${3}          | ${[{ permission: 10, objects: [1, 2] }, { permission: 12, objects: [2] }]}
  `(
    'should correctly update the permissions object ' +
      'when the deleted item has id = $deletedItemId',
    async ({ deletedItemId, expected }) => {
      const { permissionsObject } =
        withUpdatedPermissionsObject(deletedItemId)(group);
      expect(permissionsObject).toMatchObject(expected);
    },
  );
});
