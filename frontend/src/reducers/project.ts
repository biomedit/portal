import {
  ADD_PROJECT,
  ARCHIVE_PROJECT,
  DELETE_PROJECT,
  LOAD_PROJECTS,
  RETRIEVE_PROJECT,
  UNARCHIVE_PROJECT,
  UPDATE_PROJECT,
} from '../actions/actionTypes';
import type { ApiAction, BaseReducerState } from '@biomedit/next-widgets';
import {
  onAddSuccess,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onRetrieveSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';
import type {
  Project,
  ProjectResourcesInner,
  ProjectUsersInner,
} from '../api/generated';
import type { IdRequired } from '../global';
import { produce } from 'immer';

export type ProjectState = BaseReducerState<
  IdRequired<
    Project & {
      resources: Array<IdRequired<ProjectResourcesInner>>;
      users: Array<IdRequired<ProjectUsersInner>>;
    }
  >
>;

export const initialProjectState: ProjectState = {
  isFetching: false,
  isSubmitting: false,
  itemList: [],
};

export function projectReducer(
  state: ProjectState = initialProjectState,
  action:
    | ApiAction<typeof ADD_PROJECT>
    | ApiAction<typeof ARCHIVE_PROJECT>
    | ApiAction<typeof DELETE_PROJECT>
    | ApiAction<typeof LOAD_PROJECTS>
    | ApiAction<typeof RETRIEVE_PROJECT>
    | ApiAction<typeof UNARCHIVE_PROJECT>
    | ApiAction<typeof UPDATE_PROJECT>,
): ProjectState {
  return produce(state, (draft: ProjectState) => {
    switch (action.type) {
      case LOAD_PROJECTS.request:
      case RETRIEVE_PROJECT.request:
        onFetchRequest(draft);
        break;
      case LOAD_PROJECTS.success: {
        onLoadSuccess(draft, action.response);
        break;
      }
      case LOAD_PROJECTS.failure:
      case RETRIEVE_PROJECT.failure:
        onFetchFailure(draft);
        break;
      case ARCHIVE_PROJECT.success:
      case UPDATE_PROJECT.success:
      case UNARCHIVE_PROJECT.success:
        onUpdateSuccess(draft, action.response);
        break;
      case UPDATE_PROJECT.failure:
      case ADD_PROJECT.failure:
      case DELETE_PROJECT.failure:
      case ARCHIVE_PROJECT.failure:
      case UNARCHIVE_PROJECT.failure:
        onSubmitFailure(draft);
        break;
      case UPDATE_PROJECT.request:
      case ADD_PROJECT.request:
      case DELETE_PROJECT.request:
      case ARCHIVE_PROJECT.request:
      case UNARCHIVE_PROJECT.request:
        onSubmitRequest(draft);
        break;
      case ADD_PROJECT.success:
        onAddSuccess(draft, action.response);
        break;
      case DELETE_PROJECT.success:
        onDeleteSuccess(draft, action?.requestArgs?.id);
        break;
      case RETRIEVE_PROJECT.success:
        onRetrieveSuccess(draft, action.response);
    }
  });
}
