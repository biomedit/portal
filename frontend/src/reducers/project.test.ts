import type { Action, Reducer } from 'redux';
import {
  initialProjectState,
  projectReducer,
  type ProjectState,
} from './project';
import { makeMockStore, requestAction } from '@biomedit/next-widgets';
import { createProject } from '../../factories';
import { LOAD_PROJECTS } from '../actions/actionTypes';
import { mockSuccessfulApiCall } from '../testUtils';
import { rootSaga } from '../actions/sagas';
import type { RootState } from '../store';
import type SagaTester from 'redux-saga-tester';

describe('projectReducer', () => {
  let store: SagaTester<Partial<RootState>>;

  beforeEach(() => {
    store = makeMockStore(
      { project: initialProjectState },
      { project: projectReducer as Reducer<ProjectState, Action> },
      rootSaga,
    );
  });

  it('should add projects to `itemList` when `LOAD_PROJECTS` is dispatched', async () => {
    // given
    const project = createProject({ users: [] });

    mockSuccessfulApiCall('listProjects', [project, createProject()]);

    // when
    store.dispatch(requestAction(LOAD_PROJECTS));
    await store.waitFor(LOAD_PROJECTS.success);

    // then
    const projectState = store.getState().project;
    const projectListItems = projectState?.itemList;

    expect(projectListItems).toHaveLength(2);

    // @ts-expect-error We already now from the previous expect that the list is not empty
    expect(projectListItems[0]).toMatchObject(project);
  });
});
