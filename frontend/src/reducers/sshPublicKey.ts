import {
  ADD_SSH_PUBLIC_KEY,
  DELETE_SSH_PUBLIC_KEY,
  LOAD_SSH_PUBLIC_KEYS,
  RESET_SSH_INVALID_MSG,
} from '../actions/actionTypes';
import {
  type ApiAction,
  type BaseReducerState,
  isSerializedResponse,
  type SerializedResponse,
} from '@biomedit/next-widgets';
import {
  onAddOrReplaceSSHPublicKeySuccess,
  onDeleteSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
} from './utils';
import type { Action } from 'redux';
import type { IdRequired } from '../global';
import { produce } from 'immer';
import type { SSHPublicKey } from '../api/generated';
import { StatusCodes } from 'http-status-codes';

export type SSHPublicKeyState = BaseReducerState<IdRequired<SSHPublicKey>> & {
  invalidMsg: string | null;
};

export const initialSSHPublicKeyState: SSHPublicKeyState = {
  isFetching: false,
  isSubmitting: false,
  invalidMsg: null,
  itemList: [],
};

export function sshPublicKey(
  state: SSHPublicKeyState = initialSSHPublicKeyState,
  action:
    | Action<typeof RESET_SSH_INVALID_MSG>
    | ApiAction<typeof ADD_SSH_PUBLIC_KEY>
    | ApiAction<typeof DELETE_SSH_PUBLIC_KEY>
    | ApiAction<typeof LOAD_SSH_PUBLIC_KEYS>,
): SSHPublicKeyState {
  return produce(state, (draft: SSHPublicKeyState) => {
    switch (action.type) {
      case LOAD_SSH_PUBLIC_KEYS.request:
        onFetchRequest(draft);
        break;
      case LOAD_SSH_PUBLIC_KEYS.success:
        onLoadSuccess(draft, action.response);
        draft.invalidMsg = null;
        break;
      case LOAD_SSH_PUBLIC_KEYS.failure:
        onFetchFailure(draft);
        break;
      case ADD_SSH_PUBLIC_KEY.request:
      case DELETE_SSH_PUBLIC_KEY.request:
        draft.invalidMsg = null;
        onSubmitRequest(draft);
        break;
      case ADD_SSH_PUBLIC_KEY.success:
        onAddOrReplaceSSHPublicKeySuccess(draft, action.response);
        break;
      case ADD_SSH_PUBLIC_KEY.failure:
        onSubmitFailure(draft);
        if (isSerializedResponse(action.error)) {
          const serializedResponse = action.error as SerializedResponse;
          const status = serializedResponse.status;
          if (status === StatusCodes.UNPROCESSABLE_ENTITY) {
            draft.invalidMsg = (
              serializedResponse.body as Record<string, string>
            ).detail;
          }
        }
        break;
      case DELETE_SSH_PUBLIC_KEY.failure:
        onSubmitFailure(draft);
        break;
      case DELETE_SSH_PUBLIC_KEY.success: {
        onDeleteSuccess(draft, action?.requestArgs?.id);
        break;
      }
      case RESET_SSH_INVALID_MSG:
        draft.invalidMsg = null;
        break;
    }
  });
}
