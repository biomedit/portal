import {
  ADD_PGP_KEY_INFO,
  LOAD_PGP_KEY_INFOS,
  RETIRE_PGP_KEY_INFO,
  UPDATE_PGP_KEY_INFO,
} from '../actions/actionTypes';
import {
  type ApiAction,
  type BaseReducerState,
  initialBaseReducerState,
} from '@biomedit/next-widgets';
import {
  onAddSuccess,
  onFetchFailure,
  onFetchRequest,
  onLoadSuccess,
  onSubmitFailure,
  onSubmitRequest,
  onUpdateSuccess,
} from './utils';
import type { IdRequired } from '../global';
import type { PgpKeyInfo } from '../api/generated';
import { produce } from 'immer';

export type PgpKeyInfoState = BaseReducerState<IdRequired<PgpKeyInfo>>;

export function pgpKeyInfo(
  state: PgpKeyInfoState = initialBaseReducerState<IdRequired<PgpKeyInfo>>(),
  action:
    | ApiAction<typeof ADD_PGP_KEY_INFO>
    | ApiAction<typeof LOAD_PGP_KEY_INFOS>
    | ApiAction<typeof RETIRE_PGP_KEY_INFO>
    | ApiAction<typeof UPDATE_PGP_KEY_INFO>,
): PgpKeyInfoState {
  return produce(state, (draft: PgpKeyInfoState) => {
    switch (action.type) {
      case LOAD_PGP_KEY_INFOS.request:
        onFetchRequest(draft);
        break;
      case LOAD_PGP_KEY_INFOS.success:
        onLoadSuccess(draft, action.response);
        break;
      case LOAD_PGP_KEY_INFOS.failure:
        onFetchFailure(draft);
        break;
      case UPDATE_PGP_KEY_INFO.request:
      case ADD_PGP_KEY_INFO.request:
      case RETIRE_PGP_KEY_INFO.request:
        onSubmitRequest(draft);
        break;
      case ADD_PGP_KEY_INFO.success:
        onAddSuccess(draft, action.response);
        break;
      case UPDATE_PGP_KEY_INFO.success:
      case RETIRE_PGP_KEY_INFO.success:
        onUpdateSuccess(draft, action.response);
        break;
      case UPDATE_PGP_KEY_INFO.failure:
      case ADD_PGP_KEY_INFO.failure:
      case RETIRE_PGP_KEY_INFO.failure:
        onSubmitFailure(draft);
        break;
    }
  });
}
