export interface PagePermissions {
  dataProvider: DataProviderPermissions;
  dataTransfer: DataTransferPermissions;
  flag: FlagPermissions;
  group: GroupPermissions;
  node: NodePermissions;
  openPgpKey: OpenPgpKeyPermissions;
  project: ProjectPermissions;
  termsOfUse: TermsOfUsePermission;
  user: UserPermissions;
}

export interface ProjectPermissions {
  add: boolean;
  del: boolean;
  edit: boolean;
  view: boolean;
}

export interface TermsOfUsePermission {
  nodeAdmin: boolean;
  nodeViewer: boolean;
  staff: boolean;
}

export interface DataTransferPermissions {
  dataProviderAdmin: boolean;
  dataProviderViewer: boolean;
  dataSpecificationApprover: boolean;
  hasProjects: boolean;
  legalApprover: boolean;
  nodeAdmin: boolean;
  nodeViewer: boolean;
  staff: boolean;
}

export interface UserPermissions {
  nodeAdmin: boolean;
  nodeViewer: boolean;
  staff: boolean;
}

export interface NodePermissions {
  nodeAdmin: boolean;
  nodeViewer: boolean;
  staff: boolean;
}

export interface DataProviderPermissions {
  dataProviderAdmin: boolean;
  dataProviderViewer: boolean;
  nodeAdmin: boolean;
  nodeViewer: boolean;
  staff: boolean;
}

export interface GroupPermissions {
  groupManager: boolean;
  staff: boolean;
}

export interface FlagPermissions {
  staff: boolean;
}

export interface OpenPgpKeyPermissions {
  staff: boolean;
}

export const noUserPermissions: UserPermissions = {
  staff: false,
  nodeAdmin: false,
  nodeViewer: false,
};

export const noNodePermissions: NodePermissions = {
  staff: false,
  nodeAdmin: false,
  nodeViewer: false,
};

export const noDataProviderPermissions: DataProviderPermissions = {
  staff: false,
  nodeAdmin: false,
  nodeViewer: false,
  dataProviderAdmin: false,
  dataProviderViewer: false,
};

export const noGroupPermissions: GroupPermissions = {
  staff: false,
  groupManager: false,
};

export const noFlagPermissions: FlagPermissions = {
  staff: false,
};

export const noOpenPgpKeyPermissions: OpenPgpKeyPermissions = {
  staff: false,
};

export const noProjectPermissions: ProjectPermissions = {
  del: false,
  add: false,
  edit: false,
  view: false,
};

export const noDataTransferPermissions: DataTransferPermissions = {
  hasProjects: false,
  staff: false,
  dataProviderAdmin: false,
  dataProviderViewer: false,
  nodeViewer: false,
  nodeAdmin: false,
  legalApprover: false,
  dataSpecificationApprover: false,
};

export const noTermsOfUsePermission: TermsOfUsePermission = {
  staff: false,
  nodeAdmin: false,
  nodeViewer: false,
};

export const noPermissions: PagePermissions = {
  project: noProjectPermissions,
  dataTransfer: noDataTransferPermissions,
  user: noUserPermissions,
  node: noNodePermissions,
  dataProvider: noDataProviderPermissions,
  group: noGroupPermissions,
  flag: noFlagPermissions,
  openPgpKey: noOpenPgpKeyPermissions,
  termsOfUse: noTermsOfUsePermission,
};
