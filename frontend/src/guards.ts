import type {
  DataTransferDataProviderApprovalsInner,
  DataTransferGroupApprovalsInner,
  DataTransferNodeApprovalsInner,
} from './api/generated';
import type { DataTransferApproval } from './global';

export function isGroupApproval(
  approval: DataTransferApproval,
): approval is DataTransferGroupApprovalsInner {
  return !!(approval as DataTransferGroupApprovalsInner).group;
}

export function isNodeApproval(
  approval: DataTransferApproval,
): approval is DataTransferNodeApprovalsInner {
  return !!(approval as DataTransferNodeApprovalsInner).node;
}

export function isDataProviderApproval(
  approval: DataTransferApproval,
): approval is DataTransferDataProviderApprovalsInner {
  return !!(approval as DataTransferDataProviderApprovalsInner).dataProvider;
}
