import { BackendApi } from './api/generated';
import fs from 'fs';
import path from 'path';

/**
 * Returns the contents of of the file {@code filename} in the folder `test-data` as a string.
 * @param filename of the file of which to return the content as string
 */
export function getTestFileAsString(filename: string): string {
  let p = filename;
  try {
    const srcPath = path.resolve(__dirname);
    p = `${srcPath}/../test-data/${filename}`;
    return fs.readFileSync(p, 'utf8');
  } catch (_e) {
    throw new Error(`Test file with filename: ${p} not found!`);
  }
}

export function mockSuccessfulApiCall<ResponseType>(
  target: keyof BackendApi,
  response: ResponseType,
) {
  return jest.spyOn(BackendApi.prototype, target).mockResolvedValue(response);
}

export function mockFailedApiCall(target: keyof BackendApi, reason?: string) {
  return jest
    .spyOn(BackendApi.prototype, target)
    .mockRejectedValue(new Error(reason));
}
