/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface TermsOfUseAcceptance
 */
export interface TermsOfUseAcceptance {
    /**
     * Which user this acceptance belongs to.
     * @type {number}
     * @memberof TermsOfUseAcceptance
     */
    user: number;
    /**
     * Which terms of use were accepted.
     * @type {number}
     * @memberof TermsOfUseAcceptance
     */
    termsOfUse: number;
}

/**
 * Check if a given object implements the TermsOfUseAcceptance interface.
 */
export function instanceOfTermsOfUseAcceptance(value: object): value is TermsOfUseAcceptance {
    if (!('user' in value) || value['user'] === undefined) return false;
    if (!('termsOfUse' in value) || value['termsOfUse'] === undefined) return false;
    return true;
}

export function TermsOfUseAcceptanceFromJSON(json: any): TermsOfUseAcceptance {
    return TermsOfUseAcceptanceFromJSONTyped(json, false);
}

export function TermsOfUseAcceptanceFromJSONTyped(json: any, ignoreDiscriminator: boolean): TermsOfUseAcceptance {
    if (json == null) {
        return json;
    }
    return {
        
        'user': json['user'],
        'termsOfUse': json['terms_of_use'],
    };
}

export function TermsOfUseAcceptanceToJSON(value?: TermsOfUseAcceptance | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'user': value['user'],
        'terms_of_use': value['termsOfUse'],
    };
}

