/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface OAuth2Client
 */
export interface OAuth2Client {
    /**
     * 
     * @type {number}
     * @memberof OAuth2Client
     */
    readonly id?: number;
    /**
     * Helps to uniquely identify the client by name in the list of all clients. It is NOT an unique identifier.
     * @type {string}
     * @memberof OAuth2Client
     */
    readonly clientName?: string;
    /**
     * Redirection URI for use in redirect-based flows such as the authorization code and implicitflows (typically 'http://localhost:9001/oauth_callback' using a minio local instance).
     * @type {string}
     * @memberof OAuth2Client
     */
    readonly redirectUri?: string;
}

/**
 * Check if a given object implements the OAuth2Client interface.
 */
export function instanceOfOAuth2Client(value: object): value is OAuth2Client {
    return true;
}

export function OAuth2ClientFromJSON(json: any): OAuth2Client {
    return OAuth2ClientFromJSONTyped(json, false);
}

export function OAuth2ClientFromJSONTyped(json: any, ignoreDiscriminator: boolean): OAuth2Client {
    if (json == null) {
        return json;
    }
    return {
        
        'id': json['id'] == null ? undefined : json['id'],
        'clientName': json['client_name'] == null ? undefined : json['client_name'],
        'redirectUri': json['redirect_uri'] == null ? undefined : json['redirect_uri'],
    };
}

export function OAuth2ClientToJSON(value?: Omit<OAuth2Client, 'id'|'client_name'|'redirect_uri'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
    };
}

