/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { UserLiteOpenPgpKeysInner } from './UserLiteOpenPgpKeysInner';
import {
    UserLiteOpenPgpKeysInnerFromJSON,
    UserLiteOpenPgpKeysInnerFromJSONTyped,
    UserLiteOpenPgpKeysInnerToJSON,
} from './UserLiteOpenPgpKeysInner';

/**
 * 
 * @export
 * @interface UserLite
 */
export interface UserLite {
    /**
     * 
     * @type {string}
     * @memberof UserLite
     */
    readonly firstName?: string;
    /**
     * 
     * @type {Array<string>}
     * @memberof UserLite
     */
    groups?: Array<string>;
    /**
     * 
     * @type {number}
     * @memberof UserLite
     */
    readonly id?: number;
    /**
     * Designates whether this user should be treated as active. Unselect this instead of deleting accounts.
     * @type {boolean}
     * @memberof UserLite
     */
    readonly isActive?: boolean;
    /**
     * 
     * @type {Date}
     * @memberof UserLite
     */
    readonly lastLogin?: Date | null;
    /**
     * 
     * @type {string}
     * @memberof UserLite
     */
    readonly lastName?: string;
    /**
     * 
     * @type {string}
     * @memberof UserLite
     */
    readonly email?: string | null;
    /**
     * 
     * @type {string}
     * @memberof UserLite
     */
    username?: string;
    /**
     * User's display name
     * @type {string}
     * @memberof UserLite
     */
    readonly displayName?: string;
    /**
     * 
     * @type {Array<UserLiteOpenPgpKeysInner>}
     * @memberof UserLite
     */
    readonly openPgpKeys?: Array<UserLiteOpenPgpKeysInner>;
}

/**
 * Check if a given object implements the UserLite interface.
 */
export function instanceOfUserLite(value: object): value is UserLite {
    return true;
}

export function UserLiteFromJSON(json: any): UserLite {
    return UserLiteFromJSONTyped(json, false);
}

export function UserLiteFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserLite {
    if (json == null) {
        return json;
    }
    return {
        
        'firstName': json['first_name'] == null ? undefined : json['first_name'],
        'groups': json['groups'] == null ? undefined : json['groups'],
        'id': json['id'] == null ? undefined : json['id'],
        'isActive': json['is_active'] == null ? undefined : json['is_active'],
        'lastLogin': json['last_login'] == null ? undefined : (new Date(json['last_login'])),
        'lastName': json['last_name'] == null ? undefined : json['last_name'],
        'email': json['email'] == null ? undefined : json['email'],
        'username': json['username'] == null ? undefined : json['username'],
        'displayName': json['display_name'] == null ? undefined : json['display_name'],
        'openPgpKeys': json['open_pgp_keys'] == null ? undefined : ((json['open_pgp_keys'] as Array<any>).map(UserLiteOpenPgpKeysInnerFromJSON)),
    };
}

export function UserLiteToJSON(value?: Omit<UserLite, 'first_name'|'id'|'is_active'|'last_login'|'last_name'|'email'|'display_name'|'open_pgp_keys'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'groups': value['groups'],
        'username': value['username'],
    };
}

