/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { DataPackageTraceInner } from './DataPackageTraceInner';
import {
    DataPackageTraceInnerFromJSON,
    DataPackageTraceInnerFromJSONTyped,
    DataPackageTraceInnerToJSON,
} from './DataPackageTraceInner';

/**
 * 
 * @export
 * @interface DataPackage
 */
export interface DataPackage {
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly metadataHash?: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly dataTransfer?: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly purpose?: DataPackagePurposeEnum;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly projectCode?: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    fileName: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    metadata: string;
    /**
     * 
     * @type {number}
     * @memberof DataPackage
     */
    readonly id?: number;
    /**
     * 
     * @type {Array<DataPackageTraceInner>}
     * @memberof DataPackage
     */
    readonly trace?: Array<DataPackageTraceInner>;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly destinationNode?: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly dataProvider?: string;
    /**
     * 
     * @type {number}
     * @memberof DataPackage
     */
    readonly fileSize?: number | null;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly senderOpenPgpKeyInfo?: string;
    /**
     * 
     * @type {string}
     * @memberof DataPackage
     */
    readonly recipientsOpenPgpKeyInfo?: string;
}


/**
 * @export
 */
export const DataPackagePurposeEnum = {
    PRODUCTION: 'PRODUCTION',
    TEST: 'TEST'
} as const;
export type DataPackagePurposeEnum = typeof DataPackagePurposeEnum[keyof typeof DataPackagePurposeEnum];


/**
 * Check if a given object implements the DataPackage interface.
 */
export function instanceOfDataPackage(value: object): value is DataPackage {
    if (!('fileName' in value) || value['fileName'] === undefined) return false;
    if (!('metadata' in value) || value['metadata'] === undefined) return false;
    return true;
}

export function DataPackageFromJSON(json: any): DataPackage {
    return DataPackageFromJSONTyped(json, false);
}

export function DataPackageFromJSONTyped(json: any, ignoreDiscriminator: boolean): DataPackage {
    if (json == null) {
        return json;
    }
    return {
        
        'metadataHash': json['metadata_hash'] == null ? undefined : json['metadata_hash'],
        'dataTransfer': json['data_transfer'] == null ? undefined : json['data_transfer'],
        'purpose': json['purpose'] == null ? undefined : json['purpose'],
        'projectCode': json['project_code'] == null ? undefined : json['project_code'],
        'fileName': json['file_name'],
        'metadata': json['metadata'],
        'id': json['id'] == null ? undefined : json['id'],
        'trace': json['trace'] == null ? undefined : ((json['trace'] as Array<any>).map(DataPackageTraceInnerFromJSON)),
        'destinationNode': json['destination_node'] == null ? undefined : json['destination_node'],
        'dataProvider': json['data_provider'] == null ? undefined : json['data_provider'],
        'fileSize': json['file_size'] == null ? undefined : json['file_size'],
        'senderOpenPgpKeyInfo': json['sender_open_pgp_key_info'] == null ? undefined : json['sender_open_pgp_key_info'],
        'recipientsOpenPgpKeyInfo': json['recipients_open_pgp_key_info'] == null ? undefined : json['recipients_open_pgp_key_info'],
    };
}

export function DataPackageToJSON(value?: Omit<DataPackage, 'metadata_hash'|'data_transfer'|'purpose'|'project_code'|'id'|'trace'|'destination_node'|'data_provider'|'file_size'|'sender_open_pgp_key_info'|'recipients_open_pgp_key_info'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'file_name': value['fileName'],
        'metadata': value['metadata'],
    };
}

