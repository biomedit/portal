/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface PgpKeyMetadata
 */
export interface PgpKeyMetadata {
    /**
     * 
     * @type {string}
     * @memberof PgpKeyMetadata
     */
    fingerprint: string;
    /**
     * 
     * @type {string}
     * @memberof PgpKeyMetadata
     */
    readonly userId?: string;
    /**
     * 
     * @type {string}
     * @memberof PgpKeyMetadata
     */
    readonly email?: string;
    /**
     * 
     * @type {string}
     * @memberof PgpKeyMetadata
     */
    readonly status?: PgpKeyMetadataStatusEnum;
    /**
     * 
     * @type {string}
     * @memberof PgpKeyMetadata
     */
    readonly statusInfo?: string | null;
    /**
     * 
     * @type {string}
     * @memberof PgpKeyMetadata
     */
    readonly error?: string;
}


/**
 * @export
 */
export const PgpKeyMetadataStatusEnum = {
    EXPIRED: 'EXPIRED',
    INVALID: 'INVALID',
    NONVERIFIED: 'NONVERIFIED',
    REVOKED: 'REVOKED',
    VERIFIED: 'VERIFIED'
} as const;
export type PgpKeyMetadataStatusEnum = typeof PgpKeyMetadataStatusEnum[keyof typeof PgpKeyMetadataStatusEnum];


/**
 * Check if a given object implements the PgpKeyMetadata interface.
 */
export function instanceOfPgpKeyMetadata(value: object): value is PgpKeyMetadata {
    if (!('fingerprint' in value) || value['fingerprint'] === undefined) return false;
    return true;
}

export function PgpKeyMetadataFromJSON(json: any): PgpKeyMetadata {
    return PgpKeyMetadataFromJSONTyped(json, false);
}

export function PgpKeyMetadataFromJSONTyped(json: any, ignoreDiscriminator: boolean): PgpKeyMetadata {
    if (json == null) {
        return json;
    }
    return {
        
        'fingerprint': json['fingerprint'],
        'userId': json['user_id'] == null ? undefined : json['user_id'],
        'email': json['email'] == null ? undefined : json['email'],
        'status': json['status'] == null ? undefined : json['status'],
        'statusInfo': json['status_info'] == null ? undefined : json['status_info'],
        'error': json['error'] == null ? undefined : json['error'],
    };
}

export function PgpKeyMetadataToJSON(value?: Omit<PgpKeyMetadata, 'user_id'|'email'|'status'|'status_info'|'error'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'fingerprint': value['fingerprint'],
    };
}

