/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { DataTransferDataRecipientsInner } from './DataTransferDataRecipientsInner';
import {
    DataTransferDataRecipientsInnerFromJSON,
    DataTransferDataRecipientsInnerFromJSONTyped,
    DataTransferDataRecipientsInnerToJSON,
} from './DataTransferDataRecipientsInner';
import type { GroupProfile } from './GroupProfile';
import {
    GroupProfileFromJSON,
    GroupProfileFromJSONTyped,
    GroupProfileToJSON,
} from './GroupProfile';
import type { GroupPermissionsObjectInner } from './GroupPermissionsObjectInner';
import {
    GroupPermissionsObjectInnerFromJSON,
    GroupPermissionsObjectInnerFromJSONTyped,
    GroupPermissionsObjectInnerToJSON,
} from './GroupPermissionsObjectInner';

/**
 * 
 * @export
 * @interface Group
 */
export interface Group {
    /**
     * 
     * @type {number}
     * @memberof Group
     */
    readonly id?: number;
    /**
     * 
     * @type {string}
     * @memberof Group
     */
    name: string;
    /**
     * 
     * @type {GroupProfile}
     * @memberof Group
     */
    profile?: GroupProfile;
    /**
     * Users associated with this group
     * @type {Array<DataTransferDataRecipientsInner>}
     * @memberof Group
     */
    users: Array<DataTransferDataRecipientsInner>;
    /**
     * 
     * @type {Array<number>}
     * @memberof Group
     */
    permissions?: Array<number>;
    /**
     * 
     * @type {Array<GroupPermissionsObjectInner>}
     * @memberof Group
     */
    permissionsObject?: Array<GroupPermissionsObjectInner>;
}

/**
 * Check if a given object implements the Group interface.
 */
export function instanceOfGroup(value: object): value is Group {
    if (!('name' in value) || value['name'] === undefined) return false;
    if (!('users' in value) || value['users'] === undefined) return false;
    return true;
}

export function GroupFromJSON(json: any): Group {
    return GroupFromJSONTyped(json, false);
}

export function GroupFromJSONTyped(json: any, ignoreDiscriminator: boolean): Group {
    if (json == null) {
        return json;
    }
    return {
        
        'id': json['id'] == null ? undefined : json['id'],
        'name': json['name'],
        'profile': json['profile'] == null ? undefined : GroupProfileFromJSON(json['profile']),
        'users': ((json['users'] as Array<any>).map(DataTransferDataRecipientsInnerFromJSON)),
        'permissions': json['permissions'] == null ? undefined : json['permissions'],
        'permissionsObject': json['permissions_object'] == null ? undefined : ((json['permissions_object'] as Array<any>).map(GroupPermissionsObjectInnerFromJSON)),
    };
}

export function GroupToJSON(value?: Omit<Group, 'id'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'name': value['name'],
        'profile': GroupProfileToJSON(value['profile']),
        'users': ((value['users'] as Array<any>).map(DataTransferDataRecipientsInnerToJSON)),
        'permissions': value['permissions'],
        'permissions_object': value['permissionsObject'] == null ? undefined : ((value['permissionsObject'] as Array<any>).map(GroupPermissionsObjectInnerToJSON)),
    };
}

