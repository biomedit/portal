/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface QuickAccessTile
 */
export interface QuickAccessTile {
    /**
     * 
     * @type {number}
     * @memberof QuickAccessTile
     */
    readonly id?: number;
    /**
     * 
     * @type {string}
     * @memberof QuickAccessTile
     */
    title: string;
    /**
     * 
     * @type {string}
     * @memberof QuickAccessTile
     */
    description?: string;
    /**
     * 
     * @type {string}
     * @memberof QuickAccessTile
     */
    url: string;
    /**
     * 
     * @type {Blob}
     * @memberof QuickAccessTile
     */
    image: Blob;
    /**
     * The order of this quick access tile (for sorting).
     * @type {number}
     * @memberof QuickAccessTile
     */
    order?: number | null;
}

/**
 * Check if a given object implements the QuickAccessTile interface.
 */
export function instanceOfQuickAccessTile(value: object): value is QuickAccessTile {
    if (!('title' in value) || value['title'] === undefined) return false;
    if (!('url' in value) || value['url'] === undefined) return false;
    if (!('image' in value) || value['image'] === undefined) return false;
    return true;
}

export function QuickAccessTileFromJSON(json: any): QuickAccessTile {
    return QuickAccessTileFromJSONTyped(json, false);
}

export function QuickAccessTileFromJSONTyped(json: any, ignoreDiscriminator: boolean): QuickAccessTile {
    if (json == null) {
        return json;
    }
    return {
        
        'id': json['id'] == null ? undefined : json['id'],
        'title': json['title'],
        'description': json['description'] == null ? undefined : json['description'],
        'url': json['url'],
        'image': json['image'],
        'order': json['order'] == null ? undefined : json['order'],
    };
}

export function QuickAccessTileToJSON(value?: Omit<QuickAccessTile, 'id'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'title': value['title'],
        'description': value['description'],
        'url': value['url'],
        'image': value['image'],
        'order': value['order'],
    };
}

