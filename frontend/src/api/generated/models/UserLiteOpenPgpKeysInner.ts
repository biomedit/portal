/* tslint:disable */
/* eslint-disable */
/**
 * Portal API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UserLiteOpenPgpKeysInner
 */
export interface UserLiteOpenPgpKeysInner {
    /**
     * 
     * @type {string}
     * @memberof UserLiteOpenPgpKeysInner
     */
    fingerprint: string;
    /**
     * 
     * @type {string}
     * @memberof UserLiteOpenPgpKeysInner
     */
    status?: UserLiteOpenPgpKeysInnerStatusEnum;
    /**
     * 
     * @type {number}
     * @memberof UserLiteOpenPgpKeysInner
     */
    readonly id?: number;
    /**
     * 
     * @type {string}
     * @memberof UserLiteOpenPgpKeysInner
     */
    readonly keyUserId?: string;
    /**
     * 
     * @type {string}
     * @memberof UserLiteOpenPgpKeysInner
     */
    readonly keyEmail?: string;
}


/**
 * @export
 */
export const UserLiteOpenPgpKeysInnerStatusEnum = {
    APPROVED: 'APPROVED',
    APPROVAL_REVOKED: 'APPROVAL_REVOKED',
    DELETED: 'DELETED',
    KEY_REVOKED: 'KEY_REVOKED',
    PENDING: 'PENDING',
    REJECTED: 'REJECTED'
} as const;
export type UserLiteOpenPgpKeysInnerStatusEnum = typeof UserLiteOpenPgpKeysInnerStatusEnum[keyof typeof UserLiteOpenPgpKeysInnerStatusEnum];


/**
 * Check if a given object implements the UserLiteOpenPgpKeysInner interface.
 */
export function instanceOfUserLiteOpenPgpKeysInner(value: object): value is UserLiteOpenPgpKeysInner {
    if (!('fingerprint' in value) || value['fingerprint'] === undefined) return false;
    return true;
}

export function UserLiteOpenPgpKeysInnerFromJSON(json: any): UserLiteOpenPgpKeysInner {
    return UserLiteOpenPgpKeysInnerFromJSONTyped(json, false);
}

export function UserLiteOpenPgpKeysInnerFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserLiteOpenPgpKeysInner {
    if (json == null) {
        return json;
    }
    return {
        
        'fingerprint': json['fingerprint'],
        'status': json['status'] == null ? undefined : json['status'],
        'id': json['id'] == null ? undefined : json['id'],
        'keyUserId': json['key_user_id'] == null ? undefined : json['key_user_id'],
        'keyEmail': json['key_email'] == null ? undefined : json['key_email'],
    };
}

export function UserLiteOpenPgpKeysInnerToJSON(value?: Omit<UserLiteOpenPgpKeysInner, 'id'|'key_user_id'|'key_email'> | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'fingerprint': value['fingerprint'],
        'status': value['status'],
    };
}

