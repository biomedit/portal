import { apiHost, csrfHeader } from '@biomedit/next-widgets';
import { BackendApi, Configuration } from './generated';

export const backend = apiHost() + 'backend/';

export const loginRedirect = apiHost() + 'auth/oidc/authenticate/';
export const logoutUrl = apiHost() + 'auth/oidc/logout/';

const configuration = new Configuration({
  basePath: apiHost(false),
  credentials: 'include',
  headers: csrfHeader(),
});

export const generatedBackendApi = new BackendApi(configuration);
