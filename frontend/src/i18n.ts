/**
 * Reflects the filenames in `public/locales/en`. Make sure to add an entry here
 * for every new file and change the value when changing the filenames. Also
 * make sure to add an entry inside getStaticProps of index.tsx where you intend
 * to use the namespace.
 */
export enum I18nNamespace {
  BOOLEAN = 'boolean',
  COMMON = 'common',
  CONTACT_US = 'contactUs',
  DATA_PROVIDER = 'dataProvider',
  DATA_TRANSFER = 'dataTransfer',
  DATA_TRANSFER_APPROVALS = 'dataTransferApprovals',
  FEED_LIST = 'feedList',
  FLAG_LIST = 'flagList',
  GROUP_LIST = 'groupList',
  GROUP_MANAGE_FORM = 'groupManageForm',
  HOME = 'home',
  IP_RANGE_LIST = 'ipRangeList',
  LIST = 'list',
  NODE = 'node',
  PGP_KEY_INFO_LIST = 'pgpKeyInfoList',
  PGP_KEY_INFO_MANAGE_FORM = 'pgpKeyInfoManageForm',
  PROJECT_FORM = 'projectForm',
  PROJECT_LIST = 'projectList',
  PROJECT_RESOURCE_LIST = 'projectResourceList',
  PROJECT_USER_LIST = 'projectUserList',
  PROJECT_USER_ROLE_HISTORY = 'projectUserRoleHistory',
  SERVICE_LIST = 'serviceList',
  TERMINOLOGY_LIST = 'terminologyList',
  TERMINOLOGY_VERSION_LIST = 'terminologyVersionList',
  TERMS_OF_USE = 'termsOfUse',
  USER_INFO = 'userInfo',
  USER_LIST = 'userList',
  USER_MANAGE_FORM = 'userManageForm',
  USER_TEXT_FIELD = 'userTextField',
}
