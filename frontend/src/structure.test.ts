import {
  filterByPermission,
  type PortalStructureItem,
  toInitials,
} from './structure';
import { createPagePermissions } from '../factories';

describe('toInitials', () => {
  it.each`
    user                                              | isChristmas | expectedText
    ${{ firstName: 'Chuck', lastName: 'Norris' }}     | ${false}    | ${'CN'}
    ${{ profile: { localUsername: 'chuck_norris' } }} | ${false}    | ${'ch'}
    ${{ username: 'chuck_norris' }}                   | ${false}    | ${'ch'}
    ${{}}                                             | ${false}    | ${'U'}
    ${{ firstName: 'Chuck', lastName: 'Norris' }}     | ${true}     | ${'🎅'}
  `(
    'Should return $expectedText if user is $user and isChristmas is $isChristmas',
    async ({ user, isChristmas, expectedText }) => {
      const result = toInitials(user, isChristmas);
      expect(result).toBe(expectedText);
    },
  );
});

describe('filterByPermission', () => {
  const privilegedItems: PortalStructureItem[] = [
    {
      title: 'With Permission',
      menu: { link: '/withPermission' },
      permission: 'user',
    },
  ];

  const unprivilegedItems: PortalStructureItem[] = [
    {
      title: 'No Permission',
      menu: { link: '/noPermission' },
    },
  ];

  const allItems = [...privilegedItems, ...unprivilegedItems];

  it.each`
    permissions                   | expectedMenuItems
    ${{ user: { staff: false } }} | ${[...unprivilegedItems]}
    ${{ user: { staff: true } }}  | ${allItems}
  `(
    'Should return $expectedMenuItems if permissions are $permissions',
    ({ permissions, expectedMenuItems }) => {
      expect(filterByPermission(allItems, permissions)).toEqual(
        expectedMenuItems,
      );
    },
  );

  it.each`
    subItems           | expectedMenuItems
    ${allItems}        | ${[{ title: 'Collapsible', subItems: unprivilegedItems }]}
    ${privilegedItems} | ${[]}
  `(
    'For unprivileged users, it should return $expectedMenuItems when subItems are $subItems',
    ({ subItems, expectedMenuItems }) => {
      expect(
        filterByPermission(
          [
            {
              title: 'Collapsible',
              subItems: subItems,
            },
          ],
          createPagePermissions(),
        ),
      ).toEqual(expectedMenuItems);
    },
  );
});
