// eslint-disable-rule no-unused-vars
import {
  ADD_DATA_PROVIDER,
  ADD_DATA_TRANSFER,
  ADD_FLAG,
  ADD_GROUP,
  ADD_LOCAL_USER,
  ADD_NODE,
  ADD_PGP_KEY_INFO,
  ADD_PROJECT,
  ADD_SSH_PUBLIC_KEY,
  ADD_TERMS_OF_USE,
  ARCHIVE_PROJECT,
  CALL,
  CHECK_AUTH,
  DELETE_DATA_TRANSFER,
  DELETE_GROUP,
  DELETE_PROJECT,
  DELETE_SSH_PUBLIC_KEY,
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_DATA_PROVIDERS,
  LOAD_DATA_TRANSFERS,
  LOAD_FEED,
  LOAD_FLAGS,
  LOAD_GROUPS,
  LOAD_IDENTITY_PROVIDERS,
  LOAD_MEMBER_SERVICES,
  LOAD_NODES,
  LOAD_OAUTH2_CLIENTS,
  LOAD_PERMISSIONS,
  LOAD_PGP_KEY_INFOS,
  LOAD_PROJECT_USER_ROLE_HISTORY,
  LOAD_PROJECTS,
  LOAD_QUICK_ACCESSES,
  LOAD_SSH_PUBLIC_KEYS,
  LOAD_TERMINOLOGIES,
  LOAD_TERMS_OF_USE,
  LOAD_USERS,
  loginAction,
  REDIRECT,
  RESET_SSH_INVALID_MSG,
  resetSshInvalidMsgAction,
  RETIRE_PGP_KEY_INFO,
  RETRIEVE_DATA_PROVIDER,
  RETRIEVE_DATA_TRANSFER,
  RETRIEVE_NODE,
  RETRIEVE_PROJECT,
  RETRIEVE_TERMS_OF_USE,
  RETRIEVE_USER,
  UNARCHIVE_PROJECT,
  UPDATE_APPROVAL,
  UPDATE_DATA_PROVIDER,
  UPDATE_DATA_TRANSFER,
  UPDATE_FLAG,
  UPDATE_GROUP,
  UPDATE_NODE,
  UPDATE_PGP_KEY_INFO,
  UPDATE_PROJECT,
  UPDATE_USER,
  UPDATE_USER_PROFILE,
} from './actionTypes';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { generatedBackendApi, loginRedirect } from '../api/api';
import { redirect, takeApiFactory } from '@biomedit/next-widgets';
import type { Action } from 'redux';
import { StatusCodes } from 'http-status-codes';
import type { Userinfo } from '../api/generated';

interface CallbackAction extends Action {
  callback: () => void;
}

export function* rootSaga() {
  const takeApi = takeApiFactory(generatedBackendApi, loginRedirect);
  yield all<Action>([
    yield takeLatest(CHECK_AUTH.success, login),
    yield takeLatest(REDIRECT, redirect),
    yield takeLatest(CALL, function* ({ callback }: CallbackAction) {
      yield call(callback);
    }),
    yield takeApi.latest(LOAD_PROJECTS),
    yield takeApi.latest(RETRIEVE_PROJECT, [StatusCodes.NOT_FOUND]),
    yield takeApi.latest(UPDATE_PROJECT),
    yield takeApi.latest(ADD_PROJECT),
    yield takeApi.latest(DELETE_PROJECT),
    yield takeApi.latest(ARCHIVE_PROJECT),
    yield takeApi.latest(UNARCHIVE_PROJECT),
    yield takeApi.latest(LOAD_FEED),
    yield takeApi.latest(LOAD_USERS),
    yield takeApi.latest(LOAD_CUSTOM_AFFILIATIONS),
    yield takeApi.latest(LOAD_IDENTITY_PROVIDERS),
    yield takeApi.latest(CHECK_AUTH),
    yield takeApi.latest(UPDATE_USER_PROFILE, [
      StatusCodes.CONFLICT,
      StatusCodes.UNPROCESSABLE_ENTITY,
    ]),
    yield takeApi.latest(ADD_LOCAL_USER),
    yield takeApi.latest(UPDATE_USER),
    yield takeApi.latest(LOAD_NODES),
    yield takeApi.latest(ADD_NODE),
    yield takeApi.latest(UPDATE_NODE),
    yield takeApi.latest(LOAD_FLAGS),
    yield takeApi.latest(ADD_FLAG),
    yield takeApi.latest(UPDATE_FLAG),
    yield takeApi.latest(LOAD_GROUPS),
    yield takeApi.latest(ADD_GROUP),
    yield takeApi.latest(UPDATE_GROUP),
    yield takeApi.latest(DELETE_GROUP),
    yield takeApi.latest(LOAD_PERMISSIONS),
    yield takeApi.leading(LOAD_DATA_PROVIDERS),
    yield takeApi.latest(ADD_DATA_PROVIDER),
    yield takeApi.latest(UPDATE_DATA_PROVIDER),
    yield takeApi.latest(ADD_DATA_TRANSFER),
    yield takeApi.latest(DELETE_DATA_TRANSFER),
    yield takeApi.latest(UPDATE_DATA_TRANSFER),
    yield takeApi.latest(LOAD_DATA_TRANSFERS),
    yield takeApi.latest(LOAD_QUICK_ACCESSES),
    yield takeApi.latest(LOAD_OAUTH2_CLIENTS),
    yield takeApi.latest(UPDATE_APPROVAL),
    yield takeApi.latest(LOAD_PGP_KEY_INFOS),
    yield takeApi.latest(ADD_PGP_KEY_INFO),
    yield takeApi.latest(UPDATE_PGP_KEY_INFO),
    yield takeApi.latest(RETIRE_PGP_KEY_INFO),
    yield takeApi.latest(LOAD_SSH_PUBLIC_KEYS),
    yield takeApi.latest(DELETE_SSH_PUBLIC_KEY),
    yield takeApi.latest(ADD_SSH_PUBLIC_KEY, [
      StatusCodes.UNPROCESSABLE_ENTITY,
    ]),
    yield takeLatest(RESET_SSH_INVALID_MSG, resetSshInvalidMsgAction),
    yield takeApi.latest(RETRIEVE_DATA_TRANSFER, [StatusCodes.NOT_FOUND]),
    yield takeApi.latest(RETRIEVE_DATA_PROVIDER, [StatusCodes.NOT_FOUND]),
    yield takeApi.latest(RETRIEVE_NODE, [StatusCodes.NOT_FOUND]),
    yield takeApi.latest(RETRIEVE_USER, [StatusCodes.NOT_FOUND]),
    yield takeApi.latest(LOAD_MEMBER_SERVICES),
    yield takeApi.latest(LOAD_TERMS_OF_USE),
    yield takeApi.latest(RETRIEVE_TERMS_OF_USE, [StatusCodes.NOT_FOUND]),
    yield takeApi.latest(ADD_TERMS_OF_USE),
    yield takeApi.latest(LOAD_TERMINOLOGIES),
    yield takeApi.latest(LOAD_PROJECT_USER_ROLE_HISTORY),
  ]);
}

interface ResponseAction extends Action {
  response: Userinfo;
}

function* login(res: ResponseAction) {
  const { response } = res;
  if (response !== undefined) {
    yield put(loginAction(response));
  }
}
