import React, { type ReactElement } from 'react';
import { ButtonGroup } from '@mui/material';

export const DownloadButtonGroupField = (
  downloadButtons: ReactElement[],
): ReactElement => (
  <ButtonGroup variant="outlined" color="primary">
    {downloadButtons}
  </ButtonGroup>
);
