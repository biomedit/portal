import {
  Form,
  getCsrfToken,
  HiddenField,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import React, { type ReactElement, type ReactNode } from 'react';
import type {
  TerminologyVersionsInnerFilesInner,
  VersionFileDownload,
} from '../../../api/generated';
import type { ButtonProps } from '@mui/material';
import { DownloadButton } from './DownloadButton';
import { downloadFile } from '../../../utils';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../../api/api';

type DownloadButtonFormProps = {
  filename: string;
  versionFile: TerminologyVersionsInnerFilesInner;
  children?: ReactNode;
} & ButtonProps;

export const DownloadButtonForm = ({
  filename,
  versionFile,
  children,
  ...buttonProps
}: DownloadButtonFormProps): ReactElement => {
  const form = useEnhancedForm({
    defaultValues: versionFile,
  });

  return (
    <FormProvider {...form}>
      <Form<VersionFileDownload>
        onSubmit={(versionFileDownload) => {
          downloadFile(
            filename,
            generatedBackendApi.createVersionFile({
              versionFileDownload,
            }),
            versionFile.format,
          );
        }}
      >
        <HiddenField name="id" initialValues={{ id: versionFile.id }} />
        <HiddenField
          name="csrfmiddlewaretoken"
          initialValues={{ csrfmiddlewaretoken: getCsrfToken() }}
        />
        <DownloadButton
          versionFile={versionFile}
          color="primary"
          {...buttonProps}
        >
          {children}
        </DownloadButton>
      </Form>
    </FormProvider>
  );
};
