import {
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  FormControlLabel,
} from '@mui/material';
import React, { type ReactElement, useState } from 'react';
import { DownloadButtonForm } from './DownloadButtonForm';
import { I18nNamespace } from '../../../i18n';
import { Markdown } from '@biomedit/next-widgets';
import type { TerminologyVersionsInnerFilesInner } from '../../../api/generated';
import { useTranslation } from 'next-i18next';

type DownloadConfirmationDialogProps = {
  filename: string;
  versionFile: TerminologyVersionsInnerFilesInner;
  onClose: () => void;
  open?: boolean;
};

export function DownloadConfirmationDialog({
  filename,
  onClose,
  open,
  versionFile,
}: DownloadConfirmationDialogProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.TERMINOLOGY_VERSION_LIST);
  const [checked, setChecked] = useState<boolean>(false);
  return (
    <Dialog onClose={onClose} open={!!open}>
      <DialogContent>
        <FormControlLabel
          sx={{ paddingBottom: 2 }}
          control={
            <Checkbox
              onChange={(event) => setChecked(event.target.checked)}
              autoFocus
            />
          }
          label={
            <Markdown
              text={versionFile?.confirmationCheckboxText as string}
              openLinksInNewTab={true}
            />
          }
        />
        <DialogContentText sx={{ paddingBottom: 1 }}>
          <Markdown
            text={versionFile?.confirmationDescription as string}
            openLinksInNewTab={true}
          />
        </DialogContentText>
        <DialogActions>
          <DownloadButtonForm
            filename={filename}
            versionFile={versionFile}
            onClick={onClose}
            disabled={!checked}
          >
            {t('download.button')}
          </DownloadButtonForm>
        </DialogActions>
      </DialogContent>
    </Dialog>
  );
}
