import React, { type ReactElement, useCallback, useState } from 'react';
import { DownloadButton } from './DownloadButton';
import { DownloadButtonForm } from './DownloadButtonForm';
import { DownloadConfirmationDialog } from './DownloadConfirmationDialog';
import type { TerminologyVersionsInnerFilesInner } from '../../../api/generated';

type DownloadButtonFieldProps = {
  filename: string;
  versionFile: TerminologyVersionsInnerFilesInner;
};

export function DownloadButtonField({
  filename,
  versionFile,
}: DownloadButtonFieldProps): ReactElement {
  const [open, setOpen] = useState<boolean>(false);
  const closeDialog = useCallback(() => setOpen(false), []);
  const openDialog = useCallback(() => setOpen(true), []);
  if (versionFile?.confirmationCheckboxText?.length) {
    return (
      <>
        <DownloadButton versionFile={versionFile} onClick={openDialog} />
        <DownloadConfirmationDialog
          filename={filename}
          versionFile={versionFile}
          onClose={closeDialog}
          open={open}
        />
      </>
    );
  } else {
    return <DownloadButtonForm filename={filename} versionFile={versionFile} />;
  }
}
