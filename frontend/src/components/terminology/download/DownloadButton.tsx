import React, { type ReactElement, type ReactNode } from 'react';
import { Button } from '@biomedit/next-widgets';
import type { ButtonProps } from '@mui/material';
import { I18nNamespace } from '../../../i18n';
import type { TerminologyVersionsInnerFilesInner } from '../../../api/generated';
import { useTranslation } from 'next-i18next';

type DownloadButtonProps = {
  versionFile: TerminologyVersionsInnerFilesInner;
  children?: ReactNode;
} & ButtonProps;

export function DownloadButton({
  versionFile,
  children,
  ...buttonProps
}: DownloadButtonProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.TERMINOLOGY_VERSION_LIST);
  return (
    <Button
      title={
        t('download.title', {
          name: versionFile.format,
        }) as string
      }
      type="submit"
      {...buttonProps}
    >
      {children ?? versionFile.format}
    </Button>
  );
}
