import {
  BackendApi,
  TerminologyVersionsInnerStatusEnum,
} from '../../api/generated';
import {
  createBatch,
  createTerminology,
  createVersion,
} from '../../../factories';
import { fireEvent, render, screen } from '@testing-library/react';
import { makeStore } from '../../store';
import { Provider } from 'react-redux';
import React from 'react';
import { TerminologyList } from './TerminologyList';

describe('TerminologyList', () => {
  describe('No terminology', () => {
    jest.spyOn(BackendApi.prototype, 'listTerminologies').mockResolvedValue([]);

    it('should display empty list', async () => {
      render(
        <Provider store={makeStore()}>
          <TerminologyList />
        </Provider>,
      );
      // Empty message (NOT i18n'ed)
      await screen.findByText('list:emptyMessage');
    });
  });

  describe('Some terminologies', () => {
    const terminology = createTerminology({
      versions: [
        ...createBatch(createVersion, 10, {
          status: TerminologyVersionsInnerStatusEnum.ACTIVE,
        }),
        ...createBatch(createVersion, 8, {
          status: TerminologyVersionsInnerStatusEnum.ARCHIVED,
        }),
      ],
    });

    beforeAll(() => {
      jest
        .spyOn(BackendApi.prototype, 'listTerminologies')
        .mockResolvedValue([terminology]);
    });

    it('should display one terminology', async () => {
      render(
        <Provider store={makeStore()}>
          <TerminologyList />
        </Provider>,
      );
      const title = `${terminology.code} - ${terminology.name}`;
      // Open the accordion
      fireEvent.click(await screen.findByText(title));
      // The terminology has 18 versions
      expect(await screen.findAllByRole('row')).toHaveLength(18);
      const firstPage = '1–10 of 18';
      const secondPage = '11–18 of 18';
      await screen.findByText(firstPage);
      expect(screen.queryByText(secondPage)).toBeNull();
      expect(screen.queryByText('ARCHIVED')).toBeNull();
      // Find pagination next page button
      fireEvent.click(await screen.findByLabelText('next page'));
      await screen.findByText(secondPage);
      expect(await screen.findAllByText('ARCHIVED')).toHaveLength(8);
    });
  });
});
