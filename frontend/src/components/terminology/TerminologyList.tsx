import {
  Field,
  type ListModel,
  ListPage,
  Markdown,
  requestAction,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useCallback, useMemo } from 'react';
import { I18nNamespace } from '../../i18n';
import { LOAD_TERMINOLOGIES } from '../../actions/actionTypes';
import type { RootState } from '../../store';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { VersionTable } from './VersionTable';

const StyledDiv = styled('div')(() => ({
  p: { display: 'inline' },
}));

const TerminologyItem = (text: string): ReactElement => {
  return (
    <StyledDiv>
      <Markdown text={text} openLinksInNewTab={true} />
    </StyledDiv>
  );
};

export function TerminologyList(): ReactElement {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.LIST,
    I18nNamespace.TERMINOLOGY_LIST,
  ]);

  const { itemList, isFetching } = useSelector(
    (state: RootState) => state.terminologies,
  );

  const dispatch = useReduxDispatch();

  const loadItems = useCallback(() => {
    dispatch(requestAction(LOAD_TERMINOLOGIES));
  }, [dispatch]);

  const terminologyModel: ListModel<Unpacked<typeof itemList>> = useMemo(() => {
    const fields: (
      | 'description'
      | 'contact'
      | 'copyright'
      | 'documentation'
    )[] = ['description', 'contact', 'copyright', 'documentation'];
    return {
      getCaption: (terminology) => terminology.code + ' - ' + terminology.name,
      fields: fields
        .map((prop) => {
          return Field({
            caption: t(
              `${I18nNamespace.TERMINOLOGY_LIST}:model.${prop}`,
            ) as string,
            getProperty: (terminology: Unpacked<typeof itemList>) =>
              terminology[prop] ??
              t(`${I18nNamespace.COMMON}:noValuePlaceholder`),
            key: prop,
            render: TerminologyItem,
          });
        })
        .concat(
          Field({
            getProperty: (terminology: Unpacked<typeof itemList>) =>
              terminology.versions,
            key: 'versions',
            render: VersionTable,
            hideIf: (terminology) => !terminology.versions.length,
          }),
        ),
    };
  }, [t]);

  return (
    <ListPage
      model={terminologyModel}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.terminology_plural`),
        }) as string
      }
      itemList={itemList}
      loadItems={loadItems}
      isFetching={isFetching}
      isSubmitting={false}
    />
  );
}
