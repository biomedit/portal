import { ColoredStatus, formatDate, type Status } from '@biomedit/next-widgets';
import React, { useMemo } from 'react';
import {
  type TerminologyVersionsInner,
  TerminologyVersionsInnerStatusEnum,
} from '../../api/generated';
import { createColumnHelper } from '@tanstack/react-table';
import { DownloadButtonField } from './download/DownloadButtonField';
import { DownloadButtonGroupField } from './download/DownloadButtonGroupField';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { useTranslation } from 'next-i18next';

export const useVersionTableStatuses = () =>
  useMemo<Status<TerminologyVersionsInnerStatusEnum>[]>(
    () => [
      {
        color: 'success',
        value: TerminologyVersionsInnerStatusEnum.ACTIVE,
      },
      {
        color: 'warning',
        value: TerminologyVersionsInnerStatusEnum.ARCHIVED,
      },
    ],
    [],
  );

export const useResourceColumns = () => {
  const columnHelper =
    createColumnHelper<IdRequired<TerminologyVersionsInner>>();
  const { t } = useTranslation(I18nNamespace.TERMINOLOGY_VERSION_LIST);
  const statuses = useVersionTableStatuses();

  return useMemo(() => {
    return [
      columnHelper.accessor('name', { header: t('columns.name') }),
      columnHelper.accessor('released', {
        header: t('columns.released'),
        cell: (props) => formatDate(props.getValue(), false),
        sortingFn: 'datetime',
      }),
      columnHelper.accessor('status', {
        header: t('columns.status'),
        cell: (props) =>
          ColoredStatus<TerminologyVersionsInnerStatusEnum>({
            value: props.getValue(),
            statuses,
          }),
      }),
      columnHelper.display({
        id: 'files',
        header: t('columns.file'),
        cell: ({ row: { original } }) => {
          if (original.files.length) {
            return DownloadButtonGroupField(
              original.files.map((file) => (
                <DownloadButtonField
                  filename={file.filename ?? original.name}
                  key={'download-button-field-' + file.id}
                  versionFile={file}
                />
              )),
            );
          }
          return null;
        },
      }),
    ];
  }, [t, columnHelper, statuses]);
};
