import React, { type ReactElement } from 'react';
import { EnhancedTable } from '@biomedit/next-widgets';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { TerminologyVersionsInner } from '../../api/generated';
import { useResourceColumns } from './VersionTableHooks';
import { useTranslation } from 'next-i18next';

export const VersionTable = (
  versions: IdRequired<TerminologyVersionsInner>[],
): ReactElement => {
  const { t } = useTranslation(I18nNamespace.TERMINOLOGY_VERSION_LIST);
  const columns = useResourceColumns();

  return (
    <EnhancedTable
      title={t('title')}
      itemList={versions}
      columns={columns}
      isFetching={false}
      isSubmitting={false}
      canFilter={false}
      pagination={true}
      inline
    />
  );
};
