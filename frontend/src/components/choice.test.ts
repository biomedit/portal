import { displayName } from './choice';

describe('displayName', () => {
  it.each`
    user | expected
    ${{
  profile: { displayName: 'Luke Skywalker' },
}} | ${'Luke Skywalker'}
    ${{
  profile: { displayName: '' },
  username: 'AnonymousUser',
}} | ${'AnonymousUser'}
    ${{
  firstName: '',
  lastName: undefined,
  username: 'AnonymousUser',
  email: 'anonymous_user@localhost',
}} | ${'AnonymousUser (anonymous_user@localhost)'}
    ${{
  firstName: 'Luke',
  lastName: '',
  username: 'AnonymousUser',
  email: 'luke@localhost',
}} | ${'Luke (luke@localhost)'}
    ${{
  firstName: null,
  lastName: 'Skywalker',
  username: 'AnonymousUser',
  email: undefined,
}} | ${'Skywalker'}
    ${{
  firstName: 'Luke',
  lastName: 'Skywalker',
  username: 'AnonymousUser',
  email: '',
}} | ${'Luke Skywalker'}
    ${{
  firstName: '',
  lastName: null,
  username: 'AnonymousUser',
  email: undefined,
}} | ${'AnonymousUser'}
  `("Should return '$expected' if user is '$user'", ({ user, expected }) => {
    expect(displayName(user)).toStrictEqual(expected);
  });
});
