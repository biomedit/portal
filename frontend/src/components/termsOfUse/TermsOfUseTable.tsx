import {
  authenticatedUser,
  canAddTermsOfUse,
  canExportTermsOfUse,
} from '../selectors';
import {
  EnhancedTable,
  formatDate,
  getFilenameWithTimestamp,
  requestAction,
  type TabItem,
  TabPane,
  Tooltip,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  LOAD_NODES,
  LOAD_TERMS_OF_USE,
  RETRIEVE_USER,
} from '../../actions/actionTypes';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Button } from '@mui/material';
import { createColumnHelper } from '@tanstack/react-table';
import { Done } from '@mui/icons-material';
import { downloadFile } from '../../utils';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { RootState } from '../../store';
import type { TermsOfUse } from '../../api/generated';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export const TermsOfUseTable = () => {
  const { t } = useTranslation([
    I18nNamespace.TERMS_OF_USE,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);
  const router = useRouter();
  const dispatch = useReduxDispatch();
  const { itemList: termsOfUse, isFetching: isFetchingTermsOfUse } =
    useSelector((state: RootState) => state.termsOfUse);
  const { itemList: nodes, isFetching: isFetchingNodes } = useSelector(
    (state: RootState) => state.nodes,
  );
  const isFetching = useMemo(
    () => isFetchingTermsOfUse || isFetchingNodes,
    [isFetchingNodes, isFetchingTermsOfUse],
  );
  const canAdd = useSelector(canAddTermsOfUse);
  const canExport = useSelector(canExportTermsOfUse);
  const currentUser = useSelector((state: RootState) => state.auth.user);
  const user = useSelector(authenticatedUser);
  const columnHelper = createColumnHelper<IdRequired<TermsOfUse>>();
  const columns = useMemo(
    () => [
      columnHelper.accessor('version', {
        header: t('columns.version'),
      }),
      columnHelper.accessor('versionType', {
        header: t('columns.versionType'),
      }),
      columnHelper.accessor('created', {
        header: t('columns.created'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.getValue(), true),
      }),
      columnHelper.display({
        header: t('columns.acceptedAt'),
        sortingFn: 'datetime',
        cell: (context) =>
          formatDate(
            user?.termsOfUse?.find(
              (acceptance) => acceptance.id === context.row.original.id,
            )?.acceptedAt,
            true,
          ),
      }),
    ],
    [columnHelper, t, user?.termsOfUse],
  );

  const onExport = useCallback(
    (nodeId: number) => {
      setExporting(true);
      const nodeCode = nodes.find((node) => node.id === nodeId)?.code || '';
      downloadFile(
        getFilenameWithTimestamp('terms_of_use_node_' + nodeCode, '.csv'),
        generatedBackendApi.retrieveUsersTermsOfUseExport({
          nodeId: nodeId.toString(),
        }),
        'text/csv',
        () => setExporting(false),
      );
    },
    [nodes],
  );

  const [exporting, setExporting] = useState<boolean>(false);
  const tabModel: TabItem[] = useMemo(() => {
    const userHasAccepted = (terms: TermsOfUse) =>
      terms.id &&
      (user && user.termsOfUse
        ? user.termsOfUse.map((terms) => terms.id)
        : []
      ).includes(terms.id);

    const approveTermsOfUseButtonFactory = (item?: IdRequired<TermsOfUse>) =>
      item &&
      (userHasAccepted(item) ? (
        <Tooltip title={'You have already accepted these terms of use'}>
          <Done />
        </Tooltip>
      ) : (
        <Tooltip title={`Read and accept terms of use`}>
          <Button
            variant="outlined"
            size="small"
            sx={{ boxShadow: 'none' }}
            onClick={() => router.push(`/terms-of-use/${item.id}/accept`)}
            aria-label={'readAndAcceptTermsOfUseButton'}
          >
            {'Read and accept'}
          </Button>
        </Tooltip>
      ));

    return nodes.map((node) => ({
      id: node.code,
      label: node.name,
      content: (
        <EnhancedTable
          itemList={termsOfUse
            .filter((item) => item.node === node.id)
            .toSorted((a, b) => {
              // At runtime, created will never be undefined
              if (!a.created || !b.created) {
                return 0;
              }

              return a.created < b.created ? 1 : -1;
            })}
          columns={columns}
          isFetching={isFetching}
          isSubmitting={false}
          addButtonLabel={t(`${I18nNamespace.COMMON}:models.termsOfUse`)}
          canAdd={canAdd}
          onAdd={() => router.push(`/terms-of-use/new`)}
          onRowClick={(item) => router.push(`/terms-of-use/${item.id}`)}
          emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.termsOfUse`),
          })}
          canExport={canExport}
          isExporting={exporting}
          onExport={() => onExport(node.id)}
          inline={true}
          additionalActionButtons={[approveTermsOfUseButtonFactory]}
        />
      ),
    }));
  }, [
    canAdd,
    canExport,
    columns,
    exporting,
    isFetching,
    nodes,
    onExport,
    router,
    t,
    termsOfUse,
    user,
  ]);

  useEffect(() => {
    dispatch(requestAction(LOAD_TERMS_OF_USE));
    dispatch(requestAction(LOAD_NODES));
  }, [dispatch]);

  useEffect(() => {
    if (currentUser) {
      dispatch(requestAction(RETRIEVE_USER, { id: String(currentUser.id) }));
    }
  }, [currentUser, dispatch]);

  return (
    <TabPane
      id="TermsOfUseTable"
      label="Switch between different nodes."
      model={tabModel}
    />
  );
};
