import { ADD_TERMS_OF_USE, LOAD_NODES } from '../../actions/actionTypes';
import {
  ConfirmLabel,
  FixedChildrenHeight,
  LabelledField,
  LoadingButton,
  requestAction,
  required,
  SelectField,
  useChoices,
  useEnhancedForm,
  useEnumChoices,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { Form, FormProvider } from 'react-hook-form';
import React, { useEffect } from 'react';
import {
  type TermsOfUse,
  TermsOfUseVersionTypeEnum,
} from '../../api/generated';
import type { RootState } from '../../store';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

export const TermsOfUseForm = () => {
  const router = useRouter();
  const form = useEnhancedForm<TermsOfUse>();
  const dispatch = useReduxDispatch();
  const { itemList } = useSelector((state: RootState) => state.nodes);
  const { isSubmitting } = useSelector((state: RootState) => state.termsOfUse);
  const versionTypeChoices = useEnumChoices(TermsOfUseVersionTypeEnum);
  const nodeChoices = useChoices(itemList, 'id', 'name');
  useEffect(() => {
    dispatch(requestAction(LOAD_NODES));
  }, [dispatch]);

  return (
    <FormProvider {...form}>
      <Form<TermsOfUse>
        onSubmit={({ data }) => {
          dispatch(
            requestAction(
              ADD_TERMS_OF_USE,
              { termsOfUse: data },
              {
                type: 'CALL',
                callback: () => router.push(`/terms-of-use`),
              },
            ),
          );
        }}
      >
        <FixedChildrenHeight>
          <SelectField
            name="node"
            choices={nodeChoices}
            label={'Node'}
            required
          />
          <LabelledField
            name="version"
            type="text"
            label={'Version'}
            validations={[required]}
          />
          <SelectField
            name="versionType"
            label={'Version Type'}
            choices={versionTypeChoices}
            required
          />
          <LabelledField
            multiline
            name="text"
            type="text"
            label={'Text'}
            rows={10}
            validations={[required]}
          />
          <LoadingButton
            sx={{ mt: 5 }}
            type="submit"
            color="primary"
            variant="outlined"
            loading={isSubmitting}
          >
            {ConfirmLabel.CONFIRM}
          </LoadingButton>
        </FixedChildrenHeight>
      </Form>
    </FormProvider>
  );
};
