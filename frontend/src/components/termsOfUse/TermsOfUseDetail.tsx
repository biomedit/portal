import {
  Description,
  EnhancedTable,
  Field,
  formatDate,
  formatItemFields,
  type FormattedItemField,
  Markdown,
  type TabItem,
  TabPane,
} from '@biomedit/next-widgets';
import React, { useMemo } from 'react';
import type { TermsOfUse, TermsOfUseUsersInner } from '../../api/generated';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { RootState } from '../../store';
import { termsOfUseNodeName } from './utils';
import { Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

type TermsOfUseDetailProps = {
  termsOfUse?: IdRequired<
    TermsOfUse & { users?: Array<IdRequired<TermsOfUseUsersInner>> }
  >;
};

export const TermsOfUseDetail = ({ termsOfUse }: TermsOfUseDetailProps) => {
  const { t } = useTranslation([
    I18nNamespace.TERMS_OF_USE,
    I18nNamespace.USER_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);
  const { itemList: nodes } = useSelector((state: RootState) => state.nodes);
  const detailEntries: FormattedItemField[] | null = useMemo(
    () =>
      termsOfUse
        ? formatItemFields<TermsOfUse>(
            {
              fields: [
                Field({
                  caption: t(`columns.node`),
                  getProperty: (termsOfUse) =>
                    termsOfUseNodeName(termsOfUse, nodes),
                  key: 'version',
                }),
                Field({
                  caption: t(`columns.version`),
                  getProperty: (termsOfUse: TermsOfUse) => termsOfUse.version,
                  key: 'version',
                }),
                Field({
                  caption: t(`columns.versionType`),
                  getProperty: (termsOfUse: TermsOfUse) =>
                    termsOfUse.versionType,
                  key: 'versionType',
                }),
                Field({
                  caption: t(`columns.created`),
                  getProperty: (termsOfUse: TermsOfUse) => termsOfUse.created,
                  key: 'created',
                }),
                Field({
                  caption: t(`columns.statement`),
                  getProperty: (termsOfUse: TermsOfUse) => (
                    <Markdown text={termsOfUse?.text ?? ''} />
                  ),
                  key: 'statement',
                }),
              ],
            },
            termsOfUse,
          )
        : null,
    [nodes, t, termsOfUse],
  );

  const columnHelper = createColumnHelper<IdRequired<TermsOfUseUsersInner>>();
  const columns = useMemo(
    () => [
      columnHelper.accessor('firstName', {
        header: t(`${I18nNamespace.USER_LIST}:columns.firstName`),
      }),
      columnHelper.accessor('lastName', {
        header: t(`${I18nNamespace.USER_LIST}:columns.lastName`),
      }),
      columnHelper.accessor('email', {
        header: t(`${I18nNamespace.USER_LIST}:columns.email`),
      }),
      columnHelper.accessor('acceptedAt', {
        header: t(`${I18nNamespace.TERMS_OF_USE}:columns.acceptedAt`),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.getValue(), true),
      }),
    ],
    [columnHelper, t],
  );

  const tabModel: TabItem[] = useMemo(() => {
    const model = [
      {
        id: 'overview',
        label: 'Overview',
        content: <Description entries={detailEntries} labelWidth="25%" />,
      },
    ];
    // `users` is removed from the model if current user is not allowed to access them.
    if (termsOfUse?.users) {
      model.push({
        id: 'users',
        label: 'Users',
        content: (
          <EnhancedTable
            itemList={termsOfUse.users}
            columns={columns}
            isFetching={!termsOfUse}
            isSubmitting={false}
            emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
              model: t(`${I18nNamespace.COMMON}:models.user_plural`),
            })}
          />
        ),
      });
    }
    return model;
  }, [columns, detailEntries, t, termsOfUse]);

  return termsOfUse ? (
    <TabPane
      id="TermsOfUseDetail"
      label="Switch between details, text and users of the terms of use."
      model={tabModel}
      panelBoxProps={{ sx: { height: '50vh' } }}
    />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.termsOfUse`),
      })}
    </Typography>
  );
};
