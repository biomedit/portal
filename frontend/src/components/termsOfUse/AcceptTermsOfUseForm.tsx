import {
  CheckboxField,
  ConfirmLabel,
  HiddenField,
  LoadingButton,
  Markdown,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { Form, FormProvider } from 'react-hook-form';
import React, { useState } from 'react';
import type { TermsOfUse, TermsOfUseAcceptance } from '../../api/generated';
import { Box } from '@mui/material';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../../src/i18n';
import type { RootState } from '../../store';
import { Typography } from '@mui/material';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export interface AcceptTermsOfUseFormProps {
  termsOfUse?: TermsOfUse;
}

export const AcceptTermsOfUseForm = ({
  termsOfUse,
}: AcceptTermsOfUseFormProps) => {
  const router = useRouter();
  const form = useEnhancedForm<TermsOfUseAcceptance>();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const { t } = useTranslation(I18nNamespace.COMMON);
  const currentUser = useSelector((state: RootState) => state.auth.user);

  return termsOfUse ? (
    <FormProvider {...form}>
      <Form<TermsOfUseAcceptance>
        onSubmit={async ({ data }) => {
          setIsSubmitting(true);
          generatedBackendApi
            .createTermsOfUseAcceptance({
              termsOfUseAcceptance: data,
            })
            .then(() => router.push('/terms-of-use'))
            .finally(() => setIsSubmitting(false));
        }}
      >
        <Markdown text={termsOfUse.text} />
        <HiddenField name="user" initialValues={{ user: currentUser?.id }} />
        <HiddenField
          name="termsOfUse"
          initialValues={{ termsOfUse: termsOfUse.id }}
        />
        <CheckboxField
          name="acceptTerms"
          label={t(`${I18nNamespace.COMMON}:acceptTermsOfUse`)}
          required
        />
        <Box sx={{ display: 'flex', justifyContent: 'flex-start' }}>
          <LoadingButton
            sx={{ mt: 5, float: 'right' }}
            type="submit"
            color="primary"
            variant="outlined"
            loading={isSubmitting}
          >
            {ConfirmLabel.CONFIRM}
          </LoadingButton>
        </Box>
      </Form>
    </FormProvider>
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.termsOfUse`),
      })}
    </Typography>
  );
};
