import { termsOfUseNodeName } from './utils';

describe('termsOfUseNodeName', () => {
  it.each`
    termsOfUse     | nodes                          | expected    | description
    ${{ node: 1 }} | ${[{ id: 1, name: 'Node 1' }]} | ${'Node 1'} | ${'the correct node is available'}
    ${{ node: 1 }} | ${[{ id: 2, name: 'Node 2' }]} | ${'1'}      | ${'the correct node is NOT available'}
    ${{ node: 1 }} | ${[]}                          | ${'1'}      | ${'the nodes are not available'}
  `(
    'should return $expected when $description',
    ({ termsOfUse, nodes, expected }) => {
      expect(termsOfUseNodeName(termsOfUse, nodes)).toBe(expected);
    },
  );
});
