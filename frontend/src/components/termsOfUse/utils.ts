import type { Node, TermsOfUse } from '../../api/generated';

export const termsOfUseNodeName = (
  termsOfUse: TermsOfUse,
  nodes: Node[],
): string => {
  const nodeId = termsOfUse.node;
  const node: Node | undefined = nodes.find((node) => node.id == nodeId);
  return node?.name ?? nodeId.toString();
};
