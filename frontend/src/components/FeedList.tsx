import {
  Markdown,
  requestAction,
  Timeline,
  type TimelineElement,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import type { Feed } from '../api/generated';
import { I18nNamespace } from '../i18n';
import type { IdRequired } from '../global';
import { LOAD_FEED } from '../actions/actionTypes';
import type { RootState } from '../store';
import { Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const FeedList = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.FEED_LIST);

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_FEED));
  }, [dispatch]);
  const feed = useSelector<RootState, Array<IdRequired<Feed>>>(
    (state) => state.feed.itemList,
  );

  const elements: TimelineElement[] = useMemo(
    () =>
      feed.map((feed) => ({
        icon: feed.label,
        title: feed.title ? <Markdown text={feed.title} /> : undefined,
        message: feed.message ? <Markdown text={feed.message} /> : undefined,
        date: feed.fromDate,
        titleProps: { variant: 'body1' },
        messageProps: { color: 'textSecondary', variant: 'body2' },
      })),
    [feed],
  );

  if (!feed.length) {
    return (
      <Typography variant="body2" sx={{ textAlign: 'center' }}>
        {t('emptyMessage')}
      </Typography>
    );
  }

  return (
    <Timeline
      elements={elements}
      sx={{
        padding: 0,
        margin: 0,
      }}
    />
  );
};
