import { render, screen } from '@testing-library/react';
import { createUser } from '../../factories';
import { FormProvider } from 'react-hook-form';
import { makeStore } from '../store';
import { mockSuccessfulApiCall } from '../testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import { useEnhancedForm } from '@biomedit/next-widgets';
import type { User } from '../api/generated';
import userEvent from '@testing-library/user-event';
import { UserTextField } from './UserTextField';

describe('UserTextField', () => {
  function TestUserTextField({ validateUser = undefined }) {
    const form = useEnhancedForm();
    return (
      <Provider store={makeStore()}>
        <FormProvider {...form}>
          <UserTextField
            name="someUserField"
            validateUser={validateUser}
            addUser={() => undefined}
          />
        </FormProvider>
      </Provider>
    );
  }

  describe('validateUser', () => {
    const validateUser = (u: User) => u.username === 'valid';

    it.each`
      targetUser                             | validateUser    | expected
      ${createUser({ username: 'valid' })}   | ${validateUser} | ${false}
      ${createUser({ username: 'invalid' })} | ${validateUser} | ${true}
      ${createUser({ username: 'invalid' })} | ${undefined}    | ${false}
    `(
      'Should show the error message ($expected) when the user is $targetUser.username',
      async ({ targetUser, validateUser, expected }) => {
        const user = userEvent.setup();
        mockSuccessfulApiCall('listUserLites', [targetUser]);

        render(<TestUserTextField validateUser={validateUser} />);

        await user.click(await screen.findByRole('textbox'));
        await user.keyboard(targetUser.email);
        await user.click(
          await screen.findByLabelText('Add common:models.user'),
        );

        if (expected) {
          await screen.findByText('invalidUser');
        } else {
          expect(screen.queryByText('invalidUser')).not.toBeInTheDocument();
        }
      },
    );
  });
});
