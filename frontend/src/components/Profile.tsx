import {
  CALL,
  LOAD_CUSTOM_AFFILIATIONS,
  UPDATE_USER_PROFILE,
} from '../actions/actionTypes';
import type { CustomAffiliation, User, Userinfo } from '../api/generated';
import {
  Description,
  Form,
  type FormattedItemField,
  LoadingButton,
  requestAction,
  SelectField,
  type TabItem,
  TabPane,
  ToastBarSnackBar,
  ToastSeverity,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  emailChoices,
  getAffiliation,
  getFullName,
  isUserInfo,
} from '../utils';
import React, {
  type ReactElement,
  type ReactNode,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { addMultiValueField } from '../widgets/UserRoleEntries';
import type { AuthState } from '../reducers/auth';
import Box from '@mui/material/Box';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../i18n';
import { PgpKeyInfoList } from './pgpKeyInfo/PgpKeyInfoList';
import type { RootState } from '../store';
import { SSHPublicKeyList } from './SSHPublicKeyList';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const emailFieldCaption = `${I18nNamespace.COMMON}:email`;
export const flagFieldCaption = `${I18nNamespace.COMMON}:models.flag_plural`;
export const groupFieldCaption = `${I18nNamespace.COMMON}:models.group_plural`;
export const affiliationFieldCaption = `${I18nNamespace.USER_INFO}:affiliation_plural`;
export const displayNameFieldCaption = `${I18nNamespace.USER_INFO}:displayName`;
export const usernameFieldCaption = `${I18nNamespace.USER_INFO}:username`;
export const ipAddressFieldCaption = `${I18nNamespace.USER_INFO}:ipAddress`;
export const localUsernameFieldCaption = `${I18nNamespace.USER_INFO}:localUsername`;

const useEmailField = (userInfo: User | Userinfo | null) => {
  const { t } = useTranslation([I18nNamespace.COMMON]);

  const dispatch = useReduxDispatch();
  const { isSubmitting } = useSelector<RootState, AuthState>(
    (state) => state.auth,
  );

  function submit(user: Userinfo) {
    dispatch(
      requestAction(
        UPDATE_USER_PROFILE,
        {
          user: { email: user.email },
          id: String(userInfo?.id),
        },
        { type: CALL, callback: () => setToastBarOpen(true) },
      ),
    );
  }

  const formId = 'email-form';
  const form = useEnhancedForm<Userinfo>({ defaultValues: userInfo ?? {} });
  const [toastBarOpen, setToastBarOpen] = useState(false);
  const choices = useMemo(() => emailChoices(userInfo), [userInfo]);

  return (
    <FormProvider {...form}>
      <Form<Userinfo> id={formId} onSubmit={submit}>
        <ToastBarSnackBar
          severity={ToastSeverity.SUCCESS}
          open={toastBarOpen}
          onClose={() => setToastBarOpen(false)}
          autoHideDuration={6000}
        >
          <span>{'Email updated'}</span>
        </ToastBarSnackBar>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'start',
            gap: 1,
            width: '400px',
          }}
        >
          <SelectField
            name={'email'}
            label={t('email')}
            choices={choices}
            required={false}
            fullWidth
          />
          <LoadingButton
            type="submit"
            form={formId}
            variant="outlined"
            loading={isSubmitting}
            sx={{
              height: '40px',
            }}
          >
            {'save'}
          </LoadingButton>
        </Box>
      </Form>
    </FormProvider>
  );
};

export function useUserInfoFields(
  userInfo: User | Userinfo | null,
  customAffiliations: CustomAffiliation[] = [],
  emailField: ReactNode = null,
): FormattedItemField[] | null {
  const { t } = useTranslation([I18nNamespace.COMMON, I18nNamespace.USER_LIST]);
  const affiliation = useMemo(() => {
    return (
      customAffiliations.find(
        (customAffiliation) =>
          customAffiliation.id === userInfo?.profile?.customAffiliation,
      )?.code || getAffiliation(userInfo)
    );
  }, [customAffiliations, userInfo]);

  if (!userInfo) {
    return [];
  }

  const displayName = getFullName(userInfo);

  const fields: FormattedItemField[] = [
    { caption: t(displayNameFieldCaption), component: displayName },
    {
      caption: t(usernameFieldCaption),
      component: userInfo?.username,
    },
    {
      caption: t(localUsernameFieldCaption),
      component: userInfo?.profile?.localUsername ?? '',
    },
    {
      caption: t(emailFieldCaption),
      component: emailField ?? userInfo.email,
    },
    { caption: t(affiliationFieldCaption), component: affiliation },
  ];

  if (isUserInfo(userInfo)) {
    fields.push({
      caption: t(ipAddressFieldCaption),
      component: userInfo.ipAddress,
    });

    // Add "Groups" and "Flags" fields to the Userinfo box that lists all the
    // groups a user belongs to, and all flags that are active for the user.
    addMultiValueField(t(groupFieldCaption), userInfo.groups, fields);
    addMultiValueField(t(flagFieldCaption), userInfo.flags, fields);
  }

  return userInfo
    ? fields.filter(({ component }) => component !== undefined)
    : null;
}

export const Profile = () => {
  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
  }, [dispatch]);

  const { user } = useSelector<RootState, AuthState>((state) => state.auth);

  const customAffiliations = useSelector<RootState, Array<CustomAffiliation>>(
    (state) => state.customAffiliation.itemList,
  );

  const emailField = useEmailField(user);
  const userInfoEntries = useUserInfoFields(
    user,
    customAffiliations,
    emailField,
  );

  return <Description entries={userInfoEntries} labelWidth="30%" />;
};

type ProfileTabsProps = {
  tabCode: string;
};

export const ProfileTabs = ({ tabCode }: ProfileTabsProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);

  const tabItems: TabItem[] = useMemo(
    () => [
      {
        id: 'profile',
        label: t('userProfileDialog.userProfileTab'),
        content: <Profile />,
      },
      {
        id: 'openPgpKeys',
        label: t('models.openPgpKey_plural'),
        content: <PgpKeyInfoList />,
      },
      {
        id: 'sshKeys',
        label: t('models.sshKey_plural'),
        content: <SSHPublicKeyList />,
      },
    ],
    [t],
  );

  return (
    <TabPane
      id="UserProfile"
      label={t('label')}
      model={tabItems}
      selected={tabCode}
    />
  );
};
