import {
  DELETE_SSH_PUBLIC_KEY,
  LOAD_SSH_PUBLIC_KEYS,
} from '../actions/actionTypes';
import {
  EnhancedTable,
  requestAction,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
} from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../i18n';
import type { RootState } from '../store';
import { Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { useSSHPublicKeyForm } from './SSHPublicKeyManageForm';
import { useTranslation } from 'next-i18next';

export const SSHPublicKeyList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
    I18nNamespace.PGP_KEY_INFO_LIST,
  ]);

  const columns = useMemo(() => {
    const columnHelper = createColumnHelper<Unpacked<typeof itemList>>();
    return [
      columnHelper.accessor('projectName', {
        header: 'Project',
      }),
      columnHelper.accessor('key', {
        header: 'Key',
        cell: (props) => {
          return (
            <Typography
              sx={{ wordBreak: 'break-all' }}
              display="inline"
              variant="body2"
            >
              {props.getValue()}
            </Typography>
          );
        },
      }),
    ];
  }, []);

  const { isFetching, isSubmitting, itemList } = useSelector(
    (state: RootState) => state.sshPublicKey,
  );
  const dispatch = useReduxDispatch();
  useEffect(() => {
    dispatch(requestAction(LOAD_SSH_PUBLIC_KEYS));
  }, [dispatch]);

  const { openFormDialog, sshPublicKeyForm } = useSSHPublicKeyForm();

  const deleteItem = useCallback(
    (id: number) => {
      dispatch(requestAction(DELETE_SSH_PUBLIC_KEY, { id: String(id) }));
    },
    [dispatch],
  );

  return (
    <>
      <EnhancedTable
        itemList={itemList}
        columns={columns}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.sshKey_plural`),
          }) as string
        }
        canAdd={true}
        canEdit={false}
        onAdd={openFormDialog}
        form={sshPublicKeyForm}
        addButtonLabel={t(`${I18nNamespace.COMMON}:models.sshKey`) as string}
        canDelete={true}
        onDelete={deleteItem}
        inline
      />
    </>
  );
};
