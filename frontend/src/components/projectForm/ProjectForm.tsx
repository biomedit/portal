import {
  ADD_PROJECT,
  CALL,
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_GROUPS,
  LOAD_IDENTITY_PROVIDERS,
  LOAD_NODES,
  updateProjectAction,
} from '../../actions/actionTypes';
import { arrayName, IpRangeList } from '../../widgets/IpRangeList';
import {
  AutocompleteField,
  type BaseReducerState,
  CheckboxField,
  ConfirmLabel,
  email,
  HiddenField,
  idValidations,
  LabelledField,
  LoadingButton,
  nameValidations,
  requestAction,
  SelectField,
  useChoices,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { Box, CircularProgress } from '@mui/material';
import { Form, FormProvider } from 'react-hook-form';
import {
  type Group,
  GroupProfileRoleEnum,
  type IdentityProvider,
  type Project,
  type ProjectPermissionsEdit,
  ProjectUsersInnerRolesEnum,
  type UniqueProject,
  type User,
} from '../../api/generated';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react';
import type { ConvertedProjectUser } from './RoleTableCell';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { Overwrite } from 'utility-types';
import { projectUserRoles } from './UsersListHooks';
import { reduce } from 'lodash';
import type { RootState } from '../../store';
import { useNodeChoices } from '../choice';
import { useRouter } from 'next/router';
import { UsersList } from './UsersList';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export enum ProjectFormMode {
  Edit,
  Create,
}

type ConvertedProject = Overwrite<
  Project,
  {
    users: ConvertedProjectUser[];
  }
>;

export function preProcessProject(project: ConvertedProject): Project {
  // This is to convert `{role1: true, role2: false}` into `[role1]`
  const { users = [], ...rest } = project;
  return {
    ...rest,
    users: users.map(({ roles, ...userContent }) => ({
      roles: reduce(
        roles,
        (result, value, key) => {
          if (value) {
            result.push(key as ProjectUsersInnerRolesEnum);
          }
          return result;
        },
        [] as ProjectUsersInnerRolesEnum[],
      ),
      ...userContent,
    })),
  };
}

export function convertPermissions(
  project: Project | undefined,
): ConvertedProject | undefined {
  if (!project) {
    return undefined;
  }

  // This is to convert `[perm1, perm2]` into `{perm1: true, perm2: true}`
  const { users, ...rest } = project;
  return {
    ...rest,
    users: (users ?? []).map(({ roles, ...userContent }) => ({
      roles: Object.fromEntries(roles.map((r) => [r, true])),
      ...userContent,
    })),
  };
}

interface ProjectFormProps {
  mode: ProjectFormMode;
  project?: Project;
}

export const nameLabel = 'captions.name';
export const projectCodeLabel = 'captions.code';
export const destinationLabel = 'captions.destination';
export const legalSupportContactLabel = 'captions.legalSupportContact';
export const legalApprovalGroupLabel = 'captions.legalApprovalGroup';
export const dataSpecificationApprovalGroupLabel =
  'captions.dataSpecificationApprovalGroup';
export const expirationDateLabel = 'captions.expirationDate';
export const sshSupportLabel = 'captions.sshSupport';
export const resourcesSupportLabel = 'captions.resourcesSupport';
export const servicesSupportLabel = 'captions.servicesSupport';
export const identityProviderLabel = 'captions.identityProvider';
export const additionalInfoLabel = 'captions.additionalInfo';

export function hasValidRoles(project: ConvertedProject): boolean {
  const users = project?.users ?? [];
  const all_users_have_a_role = users.every((u) =>
    Object.values(u.roles).some((role) => role),
  );
  const project_has_project_leader = users.some(
    (u) => u.roles[ProjectUsersInnerRolesEnum.PL],
  );
  const norole_is_not_mutually_exclusive = users.some(
    (u) =>
      u.roles[ProjectUsersInnerRolesEnum.NO_ROLE] &&
      Object.values(u.roles).filter((role) => role).length > 1,
  );
  return (
    all_users_have_a_role &&
    project_has_project_leader &&
    !norole_is_not_mutually_exclusive
  );
}

export function ProjectForm({ project, mode }: ProjectFormProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.PROJECT_FORM);
  const router = useRouter();

  const dispatch = useReduxDispatch();

  const initialValues = convertPermissions(project);

  const { isSubmitting, isFetching: isFetchingProject } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Project>>
  >((state) => state.project);

  const { itemList: nodes, isFetching: isFetchingNodes } = useSelector(
    (state: RootState) => state.nodes,
  );

  const {
    itemList: identityProviders,
    isFetching: isFetchingIdentityProviders,
  } = useSelector<RootState, BaseReducerState<IdRequired<IdentityProvider>>>(
    (state) => state.identityProviders,
  );

  const users = useSelector<RootState, Array<IdRequired<User>>>(
    (state) => state.users.itemList,
  );
  const nodeAdminNodes = useSelector(
    (state: RootState) => state.auth.user?.manages?.nodeAdmin,
  );

  const { itemList: groups, isFetching: isFetchingGroups } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Group>>
  >((state) => state.groups);

  const legalApprovalGroups = groups.filter(
    (group: Group) => group.profile?.role === GroupProfileRoleEnum.ELSI,
  );

  const dataSpecificationApprovalGroups = groups.filter(
    (group: Group) => group.profile?.role === GroupProfileRoleEnum.FAIR,
  );

  const isDisabled = useCallback(
    (fieldName: keyof ProjectPermissionsEdit): boolean => {
      switch (mode) {
        case ProjectFormMode.Create:
          return false;
        case ProjectFormMode.Edit:
          return !project?.permissions?.edit?.[fieldName];
      }
    },
    [mode, project?.permissions?.edit],
  );
  const isNameDisabled = useMemo(() => isDisabled('name'), [isDisabled]);
  const isDestDisabled = useMemo(() => isDisabled('destination'), [isDisabled]);
  const isLegalSupportContactDisabled = useMemo(
    () => isDisabled('legalSupportContact'),
    [isDisabled],
  );
  const isIpEnabled = useMemo(() => !isDisabled(arrayName), [isDisabled]);
  const isLegalApprovalGroupDisabled = useMemo(
    () => isDisabled('legalApprovalGroup'),
    [isDisabled],
  );
  const isDataSpecificationApprovalGroupDisabled = useMemo(
    () => isDisabled('dataSpecificationApprovalGroup'),
    [isDisabled],
  );
  const isExpirationDateDisabled = useMemo(
    () => isDisabled('expirationDate'),
    [isDisabled],
  );
  const isSSHSuportDisabled = useMemo(
    () => isDisabled('sshSupport'),
    [isDisabled],
  );
  const isResourcesSuportDisabled = useMemo(
    () => isDisabled('resourcesSupport'),
    [isDisabled],
  );
  const isServicesSuportDisabled = useMemo(
    () => isDisabled('servicesSupport'),
    [isDisabled],
  );
  const isIdentityProviderDisabled = useMemo(
    () => isDisabled('identityProvider'),
    [isDisabled],
  );
  const isAdditionalInfoDisabled = useMemo(
    () => isDisabled('additionalInfo'),
    [isDisabled],
  );

  const nodeChoices = useNodeChoices(
    nodeAdminNodes?.length && !isDestDisabled ? nodeAdminNodes : nodes,
  );

  const legalApprovalGroupChoices = useChoices(
    legalApprovalGroups,
    'id',
    'name',
  );

  const dataSpecificationApprovalGroupChoices = useChoices(
    dataSpecificationApprovalGroups,
    'id',
    'name',
  );

  const form = useEnhancedForm<Project | ConvertedProject>();

  // Ref to track if form reset has already been called
  const hasReset = useRef(false);

  useEffect(() => {
    if (initialValues && !hasReset.current) {
      form.reset(initialValues);
      hasReset.current = true;
    }
  }, [initialValues, form]);

  const identityProviderChoices = useChoices(identityProviders, 'id', 'name');

  const { setError } = form;

  const handleSubmit = ({ data }: { data: ConvertedProject }) => {
    if (
      // Otherwise it is a `dayjs` object
      typeof data.expirationDate === 'string' &&
      (data.expirationDate as string).trim() === ''
    ) {
      data.expirationDate = null;
    }

    // Dispatch if all users have a role AND we have at least one PL
    if (hasValidRoles(data)) {
      const processedProject = preProcessProject(data);
      if (mode === ProjectFormMode.Edit) {
        dispatch(updateProjectAction(processedProject));
        router.push(`/projects/${data.id}`);
      } else {
        dispatch(
          requestAction(
            ADD_PROJECT,
            { project: processedProject },
            {
              type: CALL,
              callback: () => router.push('/projects/'),
            },
          ),
        );
      }
    } else {
      setError('users', {
        message: t('invalidRolesError') as string,
      });
    }
  };

  useEffect(() => {
    dispatch(requestAction(LOAD_NODES));
    dispatch(requestAction(LOAD_GROUPS));
    dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
    dispatch(requestAction(LOAD_IDENTITY_PROVIDERS, { ordering: 'name' }));
  }, [dispatch]);

  if (
    mode === ProjectFormMode.Edit &&
    (isFetchingProject || !initialValues || !users)
  ) {
    return <CircularProgress />;
  }

  const nameValue = form.getValues().name;
  const projectCodeValue = form.getValues().code;

  const uniqueCheck = async (value: Omit<UniqueProject, 'gid' | 'id'>) =>
    generatedBackendApi.uniqueProject({ uniqueProject: value });

  return (
    <FormProvider {...form}>
      <Form<ConvertedProject> onSubmit={handleSubmit} noValidate>
        <HiddenField name="id" initialValues={initialValues} />
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            gap: 2,
            '&> *': {
              minHeight: '4rem',
            },
          }}
        >
          <LabelledField
            name="name"
            type="text"
            unique={uniqueCheck}
            label={t(nameLabel)}
            validations={nameValidations(
              t(projectCodeLabel) as string,
              projectCodeValue,
            )}
            initialValues={initialValues}
            disabled={isNameDisabled}
            fullWidth
          />
          <Box sx={{ display: 'flex', gap: 1, alignContent: 'stretch' }}>
            <LabelledField
              name="code"
              type="text"
              unique={uniqueCheck}
              label={t(projectCodeLabel)}
              initialValues={initialValues}
              validations={idValidations(t(nameLabel) as string, nameValue)}
              disabled={mode === ProjectFormMode.Edit}
            />
            <SelectField
              name="destination"
              label={t(destinationLabel)}
              initialValues={initialValues}
              isLoading={isFetchingNodes}
              required
              choices={nodeChoices}
              disabled={isDestDisabled}
            />
            <LabelledField
              name="legalSupportContact"
              type="email"
              label={t(legalSupportContactLabel)}
              initialValues={initialValues}
              validations={[email]}
              disabled={isLegalSupportContactDisabled}
              fullWidth
            />
          </Box>
          <LabelledField
            name="expirationDate"
            type="date"
            label={t(expirationDateLabel)}
            initialValues={initialValues}
            slotProps={{ inputLabel: { shrink: true } }}
            disabled={isExpirationDateDisabled}
            format="DD.MM.YYYY"
            fullWidth
          />

          <Box sx={{ display: 'flex', gap: 1, alignContent: 'stretch' }}>
            <AutocompleteField
              name="identityProvider"
              label={t(identityProviderLabel)}
              choices={identityProviderChoices}
              initialValues={initialValues}
              disabled={isIdentityProviderDisabled}
              isLoading={isFetchingIdentityProviders}
              width="100%"
            />
            <AutocompleteField
              name="legalApprovalGroup"
              label={t(legalApprovalGroupLabel)}
              initialValues={initialValues}
              isLoading={isFetchingGroups}
              choices={legalApprovalGroupChoices}
              disabled={isLegalApprovalGroupDisabled}
              width="100%"
            />
            <AutocompleteField
              name="dataSpecificationApprovalGroup"
              label={t(dataSpecificationApprovalGroupLabel)}
              initialValues={initialValues}
              isLoading={isFetchingGroups}
              choices={dataSpecificationApprovalGroupChoices}
              disabled={isDataSpecificationApprovalGroupDisabled}
              width="100%"
            />
          </Box>
          <Box>
            <CheckboxField
              name="sshSupport"
              label={t(sshSupportLabel)}
              initialValues={initialValues}
              disabled={isSSHSuportDisabled}
            />
            <CheckboxField
              name="resourcesSupport"
              label={t(resourcesSupportLabel)}
              initialValues={initialValues}
              disabled={isResourcesSuportDisabled}
            />
            <CheckboxField
              name="servicesSupport"
              label={t(servicesSupportLabel)}
              initialValues={initialValues}
              disabled={isServicesSuportDisabled}
            />
          </Box>
          <LabelledField
            name="additionalInfo"
            label={t(additionalInfoLabel)}
            fullWidth
            multiline
            rows={3}
            sx={{ marginBottom: 2 }}
            disabled={isAdditionalInfoDisabled}
          />
        </Box>
        <h4>{t('subtitles.users')}</h4>
        <UsersList
          permissions={
            mode === ProjectFormMode.Edit
              ? project?.permissions?.edit?.users
              : projectUserRoles
          }
        />
        {isIpEnabled && (
          <Box>
            <h4>{t('subtitles.ipRanges')}</h4>
            <IpRangeList />
          </Box>
        )}
        <LoadingButton
          sx={{ mt: 5 }}
          type="submit"
          color="primary"
          variant="outlined"
          loading={isSubmitting}
        >
          {ConfirmLabel.SAVE}
        </LoadingButton>
      </Form>
    </FormProvider>
  );
}
