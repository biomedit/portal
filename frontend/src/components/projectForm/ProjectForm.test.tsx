import {
  ConfirmLabel,
  mockConsoleError,
  mockConsoleWarn,
  setInputValue,
} from '@biomedit/next-widgets';
import {
  convertPermissions,
  destinationLabel,
  hasValidRoles,
  nameLabel,
  preProcessProject,
  projectCodeLabel,
  ProjectForm,
  ProjectFormMode,
} from './ProjectForm';
import {
  createBatch,
  createCustomAffiliation,
  createIdentityProvider,
  createNode,
  createNodeAdminAuthState,
  createProject,
  createProjectPermissions,
  createStaffAuthState,
} from '../../../factories';
import {
  type CustomAffiliation,
  type Node,
  type ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { getInitialState, makeStore } from '../../store';
import type { AuthState } from '../../reducers/auth';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
  }),
}));

const users = [
  {
    username: '63457666153@eduid.ch',
    email: 'chuck.norris@roundhouse.kick',
    firstName: 'Chuck',
    lastName: 'Norris',
    displayName: 'Chuck Norris (chuck.norris@roundhouse.kick)',
    affiliation: '',
    affiliationId: '',
    id: 2,
    roles: [ProjectUsersInnerRolesEnum.PL],
  },
];
describe('ProjectForm', () => {
  describe('preProcessProject', () => {
    it('should transform object with permissions into array of permission keys', () => {
      expect(
        preProcessProject({
          code: '',
          name: '',
          users: [
            {
              roles: {
                PL: true,
                PM: true,
                DM: false,
                USER: true,
              },
            },
          ],
        }),
      ).toEqual({
        code: '',
        name: '',
        users: [
          {
            roles: ['PL', 'PM', 'USER'],
          },
        ],
      });
    });
  });

  describe('Component', () => {
    const enterText = (
      inputs: HTMLInputElement[],
      name: string,
      value: string,
    ) => {
      const nameField = inputs.find((el) => el.name === name);
      if (!nameField) {
        throw new Error(`No input field could be found for '${name}'`);
      }
      setInputValue(nameField, value);
    };

    const node1Name = 'Node';
    const node1Code = 'node';
    const node2Name = 'Other';
    const userNode = createNode({ code: node1Code, name: node1Name });

    const nodes = [
      createNode({ code: node1Code, name: node1Name }),
      createNode({ code: 'other', name: node2Name }),
    ];

    const identityProvider = createIdentityProvider();

    const renderForm = (
      authState: Record<'auth', AuthState>,
      nodeName: string,
      destinationPermission = true,
      users: Array<ProjectUsersInner> = [],
      mode = ProjectFormMode.Create,
      expirationDate: Date | undefined = undefined,
    ) =>
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            ...authState,
            nodes: { itemList: nodes, isFetching: false, isSubmitting: false },
          })}
        >
          <ProjectForm
            project={createProject({
              id: mode === ProjectFormMode.Edit ? 1 : undefined,
              destination: nodes.find((node) => node.name === nodeName)?.code,
              users,
              permissions: createProjectPermissions({
                edit: {
                  destination: destinationPermission,
                  expirationDate: true,
                },
              }),
              identityProvider: identityProvider.id,
              expirationDate,
            })}
            mode={mode}
          />
        </Provider>,
      );

    beforeEach(() => {
      mockSuccessfulApiCall<Node[]>('listNodes', nodes);
      mockSuccessfulApiCall('listUserLites', []);
      mockSuccessfulApiCall('listGroups', []);
      mockSuccessfulApiCall('listIdentityProviders', [identityProvider]);
      mockSuccessfulApiCall<CustomAffiliation[]>(
        'listCustomAffiliations',
        createBatch(createCustomAffiliation, 2),
      );
    });

    it('should display an error when ID and name are identical (case-sensitive)', async () => {
      renderForm({ auth: createStaffAuthState() }, node1Name);
      const inputs = screen.getAllByRole('textbox') as HTMLInputElement[];
      const sameValue = 'du';
      enterText(inputs, 'name', sameValue);
      enterText(inputs, 'code', sameValue);
      // Ensure that the node is already selected
      await screen.findByText(node1Name);
      fireEvent.click(screen.getByText(ConfirmLabel.SAVE));
      await screen.findByText(`Must not be the same as ${nameLabel}`);
      await screen.findByText(`Must not be the same as ${projectCodeLabel}`);
    }, 10000);

    it.each`
      authState                               | selectableNodes           | nonSelectableNodes | description
      ${createStaffAuthState()}               | ${[node1Name, node2Name]} | ${[]}              | ${'allow destination to be any of the nodes for staff'}
      ${createNodeAdminAuthState([userNode])} | ${[node1Name]}            | ${[node2Name]}     | ${'only allow destination to be one of the nodes the user is node admin of'}
    `(
      'should $description',
      async ({ authState, selectableNodes, nonSelectableNodes }) => {
        const consoleSpy = mockConsoleWarn();

        for (const selectableNode of selectableNodes) {
          const { findByText } = renderForm(
            { auth: authState },
            selectableNode,
          );
          await findByText(selectableNode);
          expect(consoleSpy).not.toHaveBeenCalled();
        }

        for (const nonSelectableNode of nonSelectableNodes) {
          const { queryByText, findAllByText } = renderForm(
            { auth: authState },
            nonSelectableNode,
          );
          await findAllByText(destinationLabel);
          expect(queryByText(nonSelectableNode)).not.toBeInTheDocument();
        }

        if (nonSelectableNodes.length) {
          await waitFor(() => {
            const warnings = consoleSpy.mock.calls;
            const expectedWarning =
              'MUI: You have provided an out-of-range value';
            for (const warning of warnings) {
              expect(warning[0]).toContain(expectedWarning);
            }
          });
        } else {
          expect(consoleSpy).not.toHaveBeenCalled();
        }
      },
    );

    it('should show destination node when editing a project the node admin is NOT associated with but is project leader of', async () => {
      const { findByText } = renderForm(
        { auth: createNodeAdminAuthState() },
        node2Name,
        false,
        users,
        ProjectFormMode.Edit,
      );
      await findByText(node2Name);
    });

    it('should display the right expiration date, which can be cleared', async () => {
      renderForm(
        { auth: createStaffAuthState() },
        node2Name,
        true,
        users,
        ProjectFormMode.Edit,
        new Date('2023-03-03'),
      );
      mockConsoleError();
      const expirationDate = screen.getByRole('textbox', {
        name: 'captions.expirationDate',
      });
      expect(expirationDate).not.toHaveDisplayValue(['']);
      // 'Choose date' button to open date picker should be available
      const openCalendar = screen.getByRole('button', {
        name: /Mar 3, 2023/,
      });
      fireEvent.click(openCalendar);
      // Ensure calendar is open
      const clearButton = screen.getByRole('button', {
        name: 'Clear',
      });
      fireEvent.click(clearButton);
      // Displayed value has been reset
      expect(expirationDate).toHaveDisplayValue(['']);
    });

    it.each`
      users                                                                                         | expected | description
      ${[{ roles: [ProjectUsersInnerRolesEnum.PM] }, { roles: [ProjectUsersInnerRolesEnum.USER] }]} | ${false} | ${'PL is missing'}
      ${[{ roles: [ProjectUsersInnerRolesEnum.PL] }, { roles: [] }]}                                | ${false} | ${'a user without a role'}
      ${[{ roles: [ProjectUsersInnerRolesEnum.PL] }, { roles: [ProjectUsersInnerRolesEnum.USER] }]} | ${true}  | ${'all users have a role, including one PL'}
      ${[{ roles: [ProjectUsersInnerRolesEnum.PL, ProjectUsersInnerRolesEnum.NO_ROLE] }]}           | ${false} | ${'a user with no_role cannot have another role assigend'}
    `('should return $expected when $description', ({ users, expected }) => {
      // @ts-expect-error not relevant for logic to be tested
      const result = hasValidRoles(convertPermissions({ users: users }));
      expect(result).toBe(expected);
    });
  });
});
