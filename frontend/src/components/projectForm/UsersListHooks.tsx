import {
  affiliationFieldCaption,
  displayNameFieldCaption,
  emailFieldCaption,
} from '../Profile';
import {
  type CustomAffiliation,
  ProjectUsersInnerRolesEnum,
  type User,
} from '../../api/generated';
import type { FormattedItemField } from '@biomedit/next-widgets';
import { getFullName } from '../../utils';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const projectUserRoles = Object.values(ProjectUsersInnerRolesEnum);

export const useTooltips = (): Record<ProjectUsersInnerRolesEnum, string> => {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  return useMemo(
    () =>
      projectUserRoles.reduce(
        (acc, role) => {
          acc[role] =
            t(`roleShortTitle.${role}`) + ': ' + t(`roleDescription.${role}`);
          return acc;
        },
        {} as Record<ProjectUsersInnerRolesEnum, string>,
      ),
    [t],
  );
};

export const useUserEntries = (user: User | null): FormattedItemField[] => {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  const customAffiliations = useSelector<RootState, CustomAffiliation[]>(
    (state) => state.customAffiliation.itemList,
  );

  return useMemo(() => {
    const affiliation =
      user?.profile?.affiliation ||
      customAffiliations.find(
        (customAffiliation) =>
          customAffiliation.id === user?.profile?.customAffiliation,
      )?.code;
    return [
      { caption: t(displayNameFieldCaption), component: getFullName(user) },
      {
        caption: t(emailFieldCaption),
        component: user?.email,
      },
      {
        caption: t(affiliationFieldCaption),
        component: affiliation
          ? affiliation
              .split(',')
              .map((aff) => aff.trim())
              .join('\n')
          : '-',
      },
    ];
  }, [customAffiliations, t, user]);
};
