import {
  type BaseReducerState,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TooltipText,
  UserChip,
} from '@biomedit/next-widgets';
import { Controller, useFieldArray, useFormContext } from 'react-hook-form';
import { type ConvertedProjectUser, RoleTableCell } from './RoleTableCell';
import {
  type CustomAffiliation,
  ProjectPermissionsEditUsersEnum,
  ProjectUsersInnerRolesEnum,
  type User,
} from '../../api/generated';
import { projectUserRoles, useTooltips } from './UsersListHooks';
import React, { type ReactElement } from 'react';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { RootState } from '../../store';
import { UserTextField } from '../UserTextField';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

interface FormValues {
  users: Array<ConvertedProjectUser>;
}

type UserListProps = {
  permissions?: Array<ProjectPermissionsEditUsersEnum>;
};

const isRoleSelected = (
  field: ConvertedProjectUser,
  role: ProjectUsersInnerRolesEnum,
): boolean => !!field?.roles?.[role];

export const UsersList = ({ permissions }: UserListProps): ReactElement => {
  const arrayName = 'users';

  const { control, clearErrors } = useFormContext<FormValues>();
  const { itemList: customAffiliations } = useSelector<
    RootState,
    BaseReducerState<IdRequired<CustomAffiliation>>
  >((state) => state.customAffiliation);
  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
    keyName: 'key',
  });

  function hasRole(
    role: ProjectPermissionsEditUsersEnum | ProjectUsersInnerRolesEnum,
  ): boolean {
    return !!permissions?.includes(role as ProjectPermissionsEditUsersEnum);
  }
  const { t } = useTranslation([I18nNamespace.PROJECT_FORM]);

  const tooltips = useTooltips();
  return (
    <div>
      <UserTextField
        addUser={(user: User) => {
          const affiliation = user?.profile?.affiliation;
          const projectUser: ConvertedProjectUser = {
            affiliation: affiliation
              ? affiliation
              : customAffiliations.find(
                  (customAffiliation) =>
                    customAffiliation.id === user?.profile?.customAffiliation,
                )?.code,
            // initialize checkboxes to be empty
            roles: Object.fromEntries(
              Object.values(ProjectUsersInnerRolesEnum).map((role) => [
                role,
                false,
              ]),
            ),
            ...user,
          };
          if (!fields.some((field) => String(field.id) === String(user.id))) {
            append(projectUser);
          }
        }}
        name={arrayName}
      />
      {fields.length > 0 && (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>User</TableCell>
              {projectUserRoles.map((permission) => (
                <TableCell key={permission}>
                  <TooltipText text={permission} title={tooltips[permission]} />
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {fields.map((field, index) => {
              const displayName = field?.['displayName']
                ? field?.['displayName']
                : field?.['username'];

              //
              // Make sure only users can be deleted which only have selected
              // roles the current user is allowed to change.
              //
              // Example: if the current user is permissions manager, they
              // cannot change whether users are project leaders. This means
              // they should also not be able to delete any users from the list
              // which are project leaders, else they would also remove the
              // project leader's roles by doing so.
              //
              const canDelete = projectUserRoles.every((role) =>
                isRoleSelected(field, role) ? hasRole(role) : true,
              );
              const has_no_role = field.roles['NO_ROLE'];
              const has_other_role = Object.entries(field.roles).some(
                ([key, value]) => key !== 'NO_ROLE' && value === true,
              );

              const onDelete = canDelete
                ? () => {
                    clearErrors('users');
                    remove(index);
                  }
                : undefined;

              return (
                <TableRow key={field.key}>
                  <TableCell>
                    <Controller
                      control={control}
                      name={`${arrayName}.${index}.displayName`}
                      defaultValue={displayName}
                      render={() => (
                        <UserChip
                          user={displayName}
                          onDelete={onDelete}
                          {...(has_no_role && has_other_role
                            ? {
                                color: 'error',
                                tooltip: t('noRoleIsMutuallyExclusiveError'),
                              }
                            : {})}
                        />
                      )}
                    />
                  </TableCell>
                  {projectUserRoles.map((role) => {
                    return (
                      <RoleTableCell
                        name={arrayName}
                        key={role}
                        index={index}
                        field={field}
                        disabled={!hasRole(role)}
                        fieldName={`roles.${role}`}
                      />
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )}
    </div>
  );
};
