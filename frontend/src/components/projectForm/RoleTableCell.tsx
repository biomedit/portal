import type { $Values, Overwrite } from 'utility-types';
import {
  CheckboxArrayField,
  type EnhancedArrayFieldProps,
  HiddenArrayField,
  TableCell,
} from '@biomedit/next-widgets';
import {
  type ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import React, { type ReactElement } from 'react';
import type { FieldPath } from 'react-hook-form/dist/types/path';
import type { FieldValues } from 'react-hook-form/dist/types';
import { I18nNamespace } from '../../i18n';
import { useFormContext } from 'react-hook-form';
import { useTranslation } from 'next-i18next';

export type ConvertedProjectUser = Overwrite<
  ProjectUsersInner,
  {
    roles: Partial<Record<ProjectUsersInnerRolesEnum, boolean>>;
  }
>;

type RoleTableCellProps<T extends FieldValues> = {
  disabled: boolean;
  field: $Values<Pick<EnhancedArrayFieldProps, 'field'>>;
  fieldName: string;
  index: number;
  name: FieldPath<T>;
};

export function RoleTableCell<T extends FieldValues>({
  name,
  field,
  index,
  disabled,
  fieldName,
}: RoleTableCellProps<T>): ReactElement {
  const { t } = useTranslation([I18nNamespace.PROJECT_USER_LIST]);
  const { clearErrors, getValues } = useFormContext<T>();

  const onChange = () => {
    const projectLeader = getValues()[name].find(
      (user: ConvertedProjectUser) =>
        user.roles?.[ProjectUsersInnerRolesEnum.PL],
    );
    if (projectLeader) {
      clearErrors(name);
    }
  };

  return (
    <TableCell>
      <HiddenArrayField
        arrayName={name}
        fieldName="id"
        index={index}
        field={field}
      />
      <CheckboxArrayField
        arrayName={name}
        fieldName={fieldName}
        index={index}
        field={field}
        disabled={disabled}
        disabledTitle={t('disabledTitle')}
        onChange={onChange}
      />
    </TableCell>
  );
}
