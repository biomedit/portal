import {
  affiliationConsentRequired,
  privacyLink,
  termsOfUseEnabled,
} from '../config';
import {
  CheckboxField,
  FormDialog,
  LabelledField,
  requestAction,
  SafeLink,
  SelectField,
  ToastBarSnackBar,
  ToastSeverity,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  LOAD_NODES,
  LOAD_TERMS_OF_USE,
  RETRIEVE_USER,
  UPDATE_USER_PROFILE,
} from '../actions/actionTypes';
import React, { type ReactElement, useEffect, useMemo, useState } from 'react';
import {
  type TermsOfUseLite,
  TermsOfUseVersionTypeEnum,
  type User,
} from '../../src/api/generated';
import { Trans, useTranslation } from 'next-i18next';
import Alert from '@mui/material/Alert';
import { authenticatedUser } from './selectors';
import type { AuthState } from '../reducers/auth';
import { emailChoices } from '../utils';
import { FormFieldsContainer } from './FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../i18n';
import type { IdRequired } from '../global';
import type { RootState } from '../store';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';

interface TermsOfUseWarningShowWarning {
  showNewVersionTermsOfUse: true;
  userNodeID: number;
  mostRecentTermsOfUseID: number;
}

interface TermsOfUseWarningDoNotShowWarning {
  showNewVersionTermsOfUse: false;
  userNodeID: undefined;
  mostRecentTermsOfUseID: undefined;
}

export type TermsOfUseWarningReturnType =
  | TermsOfUseWarningShowWarning
  | TermsOfUseWarningDoNotShowWarning;

export const termsOfUseWarning = (
  user: User | undefined,
  termsOfUseList: IdRequired<TermsOfUseLite>[] | undefined,
): TermsOfUseWarningReturnType => {
  let showNewVersionResult = false;
  let userNodeIDResult: number | undefined = undefined;
  let mostRecentTermsOfUseID: number | undefined = undefined;

  const termsOfUseAcceptedList = user?.termsOfUse;

  if (!termsOfUseAcceptedList || !termsOfUseList) {
    return {
      showNewVersionTermsOfUse: showNewVersionResult,
      userNodeID: userNodeIDResult,
      mostRecentTermsOfUseID,
    };
  }

  const sortedTermsOfUseAcceptedList = termsOfUseAcceptedList.toSorted(
    (a, b) => b.id - a.id,
  );

  const sortedTermsOfUseList = termsOfUseList.toSorted((a, b) => b.id - a.id);

  // Get unique node IDs
  const nodeIDs = Array.from(
    new Set(sortedTermsOfUseList.map((item) => item.node)),
  );

  // Filter termsOfUseList for each node ID
  const termsOfUseListsByNode: TermsOfUseLite[][] = nodeIDs.map((nodeID) =>
    sortedTermsOfUseList.filter((item) => item.node === nodeID),
  );

  // Iterate over the list of lists
  termsOfUseListsByNode.forEach((list: TermsOfUseLite[]) => {
    // Get the TOU acceptance list by node
    const filteredTermsOfUseAcceptedList = sortedTermsOfUseAcceptedList.filter(
      (item) => list.some((listItem) => listItem.id === item.id),
    );

    const mostRecentMajorTermsOfUse = list.find(
      (item) => item.versionType === TermsOfUseVersionTypeEnum.MAJOR,
    );

    const mostRecentTOUAccepted =
      filteredTermsOfUseAcceptedList &&
      filteredTermsOfUseAcceptedList.length > 0
        ? filteredTermsOfUseAcceptedList[0]
        : undefined;

    if (
      mostRecentTOUAccepted &&
      mostRecentMajorTermsOfUse?.id &&
      mostRecentTOUAccepted.id < mostRecentMajorTermsOfUse.id
    ) {
      showNewVersionResult = true;
      userNodeIDResult = list[0].node;
      mostRecentTermsOfUseID = list[0].id;
    }
  });

  return {
    showNewVersionTermsOfUse: showNewVersionResult,
    userNodeID: userNodeIDResult,
    mostRecentTermsOfUseID,
  };
};

type FormData = {
  affiliationConsent: boolean;
  email: string;
  localUsername: string;
};

const StyledCheckboxField = styled(CheckboxField)(({ theme }) => ({
  marginTop: theme.spacing(1),
}));

export const dialogTitle = 'userProfileDialog.title';
export const usernameLabel = 'userProfileDialog.usernameLabel';
export const affiliationConsentLabel =
  'userProfileDialog.affiliationConsentLabel';
export const privacyPolicyLabel = 'userProfileDialog.privacyPolicyLabel';
export const emailLabel = 'userProfileDialog.emailLabel';

export const UserProfileDialog = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const dispatch = useReduxDispatch();

  const {
    user: userInfo,
    isSubmitting,
    isExistingUsername,
    invalidMsg,
  } = useSelector<RootState, AuthState>((state) => state.auth);

  const form = useEnhancedForm<FormData>();

  const [invalidUsername, setInvalidUsername] = useState<string | undefined>();

  const usernameMissing = useMemo(
    () => !userInfo?.profile?.localUsername,
    [userInfo],
  );
  const affiliationConsentMissing = useMemo(
    () => affiliationConsentRequired && !userInfo?.profile?.affiliationConsent,
    [userInfo],
  );
  const emailMissing = useMemo(() => !userInfo?.email, [userInfo]);

  const currentUser = useSelector((state: RootState) => state.auth.user);
  const user = useSelector(authenticatedUser);

  const { itemList: nodes } = useSelector((state: RootState) => state.nodes);

  useEffect(() => {
    dispatch(requestAction(LOAD_TERMS_OF_USE));
    dispatch(requestAction(LOAD_NODES));
  }, [dispatch]);

  useEffect(() => {
    if (currentUser) {
      dispatch(requestAction(RETRIEVE_USER, { id: String(currentUser.id) }));
    }
  }, [currentUser, dispatch]);

  const { itemList: termsOfUseList } = useSelector(
    (state: RootState) => state.termsOfUse,
  );

  const { showNewVersionTermsOfUse, userNodeID, mostRecentTermsOfUseID } =
    useMemo(
      () => termsOfUseWarning(user, termsOfUseList),
      [user, termsOfUseList],
    );

  const usernameField = 'localUsername';
  const affiliationConsentField = 'affiliationConsent';
  const privacyPolicyField = 'privacyPolicy';
  const emailField = 'email';

  const watchUsername = form.watch(usernameField);

  const submit = ({ localUsername, affiliationConsent, email }: FormData) => {
    setInvalidUsername(undefined);
    if (localUsername || affiliationConsent || email) {
      dispatch(
        requestAction(UPDATE_USER_PROFILE, {
          user: {
            profile: {
              localUsername,
              affiliationConsent,
            },
            email,
          },
          id: String(userInfo?.id),
        }),
      );
    }
  };

  const fieldError = useMemo(() => {
    // `invalidUsername` NOT set (is reset in submit call)
    if (!invalidUsername && (isExistingUsername || !!invalidMsg)) {
      setInvalidUsername(watchUsername);
    }
    // Input has changed and does NOT equal `invalidUsername`
    if (invalidUsername && watchUsername !== invalidUsername) {
      return;
    }
    if (isExistingUsername) {
      return {
        type: 'validate',
        message: t('userProfileDialog.validationUsernameAlreadyExists'),
      };
    }
    if (invalidMsg) {
      return {
        type: 'validate',
        message: invalidMsg,
      };
    }
    return;
  }, [t, invalidMsg, isExistingUsername, watchUsername, invalidUsername]);

  const choices = useMemo(() => emailChoices(userInfo), [userInfo]);

  const [toastBarOpen, setToastBarOpen] = useState(false);

  useEffect(() => {
    if (showNewVersionTermsOfUse) {
      setToastBarOpen(true);
    } else {
      setToastBarOpen(false);
    }
  }, [showNewVersionTermsOfUse]);

  const userNode = nodes.find((node) => node.id === userNodeID)?.name;

  return (
    <>
      {termsOfUseEnabled && (
        <ToastBarSnackBar
          severity={ToastSeverity.INFO}
          open={toastBarOpen}
          withCloseButton={true}
          onClose={() => setToastBarOpen(false)}
        >
          <span>
            {t('userProfileDialog.newTermsOfUseAlert', { userNode }) + ' '}
            <a
              href={`/terms-of-use/${mostRecentTermsOfUseID}/accept`}
              style={{ color: 'white', textDecoration: 'underline' }}
            >
              {t('userProfileDialog.clickHere')}.
            </a>
          </span>
        </ToastBarSnackBar>
      )}
      <FormProvider {...form}>
        <FormDialog<FormData>
          title={t(dialogTitle)}
          open={usernameMissing || affiliationConsentMissing || emailMissing}
          onSubmit={submit}
          isSubmitting={isSubmitting}
        >
          <FormFieldsContainer>
            {usernameMissing && (
              <>
                <Alert
                  severity="warning"
                  sx={{ marginBottom: 2, marginRight: 0 }}
                >
                  {t('userProfileDialog.warning')}
                </Alert>
                <LabelledField
                  type="text"
                  name={usernameField}
                  label={t(usernameLabel)}
                  fieldError={fieldError}
                  required={usernameMissing}
                  autofocus
                  fullWidth
                />
              </>
            )}
            {emailMissing && !!userInfo?.profile?.emails && (
              <SelectField
                name={emailField}
                label={t(emailLabel)}
                choices={choices}
                populateIfSingleChoice
                required={emailMissing && !!userInfo?.profile?.emails}
                helperText={t('userProfileDialog.emailHelperText')}
                fullWidth
              />
            )}
            {affiliationConsentMissing && (
              <StyledCheckboxField
                name={affiliationConsentField}
                label={t(affiliationConsentLabel)}
                required={affiliationConsentMissing}
              />
            )}
            {privacyLink && (
              <StyledCheckboxField
                name={privacyPolicyField}
                label={
                  <Trans
                    i18nKey={`${I18nNamespace.COMMON}:${privacyPolicyLabel}`}
                    components={{
                      privacyPolicy: <SafeLink href={privacyLink} />,
                    }}
                  />
                }
                required={true}
              />
            )}
          </FormFieldsContainer>
        </FormDialog>
      </FormProvider>
    </>
  );
};
