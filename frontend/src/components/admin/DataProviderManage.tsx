import {
  type BaseReducerState,
  ColoredStatus,
  EnhancedTable,
  requestAction,
  type Status,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { LOAD_DATA_PROVIDERS, LOAD_NODES } from '../../actions/actionTypes';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import type { AuthState } from '../../reducers/auth';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { isStaff } from '../selectors';
import type { Node } from '../../api/generated';
import type { RootState } from '../../store';
import { useDataProviderForm } from './DataProviderManageForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

enum DataProviderStatusEnum {
  DISABLED = 'DISABLED',
  ENABLED = 'ENABLED',
}

export function getDataProviderColoredStatus(enabled: boolean | undefined) {
  const statuses: Status<DataProviderStatusEnum>[] = [
    {
      color: 'success',
      value: DataProviderStatusEnum.ENABLED,
    },
    {
      color: 'error',
      value: DataProviderStatusEnum.DISABLED,
    },
  ];
  return ColoredStatus<DataProviderStatusEnum>({
    value: enabled
      ? DataProviderStatusEnum.ENABLED
      : DataProviderStatusEnum.DISABLED,
    statuses,
  });
}

export const DataProviderList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_PROVIDER,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const {
    isFetching,
    isSubmitting,
    itemList: dataProviders,
  } = useSelector((state: RootState) => state.dataProvider);
  const staff = useSelector(isStaff);
  const auth = useSelector<RootState, AuthState>((state) => state.auth);
  const staffOrNodePersonnel = auth.pagePermissions.node;
  const username = auth.user?.username;

  const dispatch = useReduxDispatch();

  const { itemList: nodes, isFetching: isFetchingNodes } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Node>>
  >((state) => state.nodes);

  useEffect(() => {
    if (staffOrNodePersonnel) {
      dispatch(requestAction(LOAD_DATA_PROVIDERS));
    } else {
      dispatch(requestAction(LOAD_DATA_PROVIDERS, { username }));
    }
    dispatch(requestAction(LOAD_NODES));
  }, [dispatch, username, staffOrNodePersonnel]);

  const { openFormDialog, dataProviderForm } = useDataProviderForm();
  const columnHelper = createColumnHelper<Unpacked<typeof dataProviders>>();
  const columns = useMemo(() => {
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('name', {
        header: t('columns.name'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('nodes', {
        header: t('columns.nodes'),
        cell: (props) =>
          nodes
            .filter((node) => props.getValue().includes(node.code))
            .map((node) => `${node.name} (${node.code})`)
            .sort()
            .join(', '),
      }),
      columnHelper.accessor('enabled', {
        header: t('columns.enabled'),
        cell: (props) => getDataProviderColoredStatus(props.getValue()),
      }),
    ];
  }, [columnHelper, t, nodes]);

  const router = useRouter();

  return (
    <EnhancedTable
      itemList={dataProviders}
      columns={columns}
      canAdd={staff}
      onAdd={openFormDialog}
      onRowClick={(item) => router.push(`/data-providers/${item.id}`)}
      addButtonLabel={t(`${I18nNamespace.DATA_PROVIDER}:addButton`) as string}
      form={dataProviderForm}
      isFetching={isFetching || isFetchingNodes}
      isSubmitting={isSubmitting}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.dataProvider_plural`),
        }) as string
      }
      inline
    />
  );
};
