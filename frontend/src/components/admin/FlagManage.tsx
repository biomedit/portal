import {
  EnhancedTable,
  requestAction,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import { isStaff } from '../selectors';
import { LOAD_FLAGS } from '../../actions/actionTypes';
import type { RootState } from '../../store';
import { truncate } from 'lodash';
import { useFlagForm } from './FlagManageForm';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const FlagManageList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.FLAG_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const {
    isFetching,
    isSubmitting,
    itemList: flags,
  } = useSelector((state: RootState) => state.flags);
  const staff = useSelector(isStaff);
  const dispatch = useReduxDispatch();

  const columnHelper = createColumnHelper<Unpacked<typeof flags>>();
  const columns = useMemo(() => {
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('description', {
        header: t('columns.description'),
        cell: (props) =>
          truncate(props.getValue(), {
            length: 100,
            separator: ' ',
          }),
      }),
      columnHelper.accessor('users', {
        header: t('columns.users'),
        cell: (props) => props.getValue()?.length,
      }),
    ];
  }, [t, columnHelper]);

  useEffect(() => {
    dispatch(requestAction(LOAD_FLAGS));
  }, [dispatch]);

  const { openFormDialog, flagForm } = useFlagForm();

  return (
    <EnhancedTable
      itemList={flags}
      columns={columns}
      canAdd={staff}
      canEdit={staff}
      canDelete={false} // it's forbidden to delete flags
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      form={flagForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      addButtonLabel={t(`${I18nNamespace.FLAG_LIST}:addButton`) as string}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.flag_plural`),
        }) as string
      }
      inline
    />
  );
};
