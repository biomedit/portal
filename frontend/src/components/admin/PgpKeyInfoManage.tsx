import {
  EnhancedTable,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect } from 'react';
import { I18nNamespace } from '../../i18n';
import { isStaff } from '../selectors';
import { LOAD_PGP_KEY_INFOS } from '../../actions/actionTypes';
import type { PgpKeyInfoState } from '../../reducers/pgpKeyInfo';
import type { RootState } from '../../store';
import { usePgpKeyInfoColumns } from '../pgpKeyInfo/PgpKeyInfoHooks';
import { usePgpKeyInfoForm } from './PgpKeyInfoManageForm';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const PgpKeyInfoList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.PGP_KEY_INFO_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);
  const { isFetching, isSubmitting, itemList } = useSelector<
    RootState,
    PgpKeyInfoState
  >((state) => state.pgpKeyInfo);
  const staff = useSelector(isStaff);
  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS));
  }, [dispatch]);

  const { openFormDialog, pgpKeyInfoForm } = usePgpKeyInfoForm();

  const columns = usePgpKeyInfoColumns();
  return (
    <EnhancedTable
      itemList={itemList}
      columns={columns}
      canAdd={false} // it's forbidden to add keys
      canEdit={staff}
      canDelete={false} // it's forbidden to delete keys
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      form={pgpKeyInfoForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.openPgpKey_plural`),
        }) as string
      }
      inline
    />
  );
};
