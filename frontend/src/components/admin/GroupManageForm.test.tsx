import {
  BackendApi,
  type DataTransferDataRecipientsInner,
} from '../../api/generated';
import {
  createBatch,
  createGroup,
  createObjectByPermission,
  createPermission,
  createPermissionObject,
  createSnakeCased,
  createStaffAuthState,
  createUser,
} from '../../../factories';
import { getInitialState, makeStore } from '../../store';
import { render, screen, waitFor } from '@testing-library/react';
import { GroupForm } from './GroupManageForm';
import { mockConsoleError } from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import React from 'react';

describe('GroupManageForm', () => {
  const viewNodePermissionId = 44;
  const userId = 3;
  const groupId = 5;
  const timeout = 10000;

  const permissionName = 'Can view node';
  const permissionObjectName = 'Node 1 (node_1)';
  const user = createSnakeCased(createUser)({
    id: userId,
  }) as DataTransferDataRecipientsInner;

  // TODO 2024-04-30, Christian Ribeaud: See https://gitlab.com/biomedit/portal/-/merge_requests/1157.
  let consoleError: jest.SpyInstance;
  beforeEach(() => (consoleError = mockConsoleError()));
  afterEach(() => consoleError.mockRestore());

  it.each`
    delayListPerm | delayListObjectByPerm
    ${0}          | ${0}
    ${500}        | ${500}
    ${2000}       | ${2000}
    ${2000}       | ${0}
    ${0}          | ${2000}
  `(
    'should contain both permission and object when ' +
      'call to list permissions is delayed by $delayListPerm ms and ' +
      'call to list object by permission is delayed by $delayListObjectByPerm ms',
    async ({ delayListPerm, delayListObjectByPerm }) => {
      const permission = createPermission({
        id: viewNodePermissionId,
        name: permissionName,
      });
      const permissions = [permission, ...createBatch(createPermission, 9)];

      const permissionsApiCall = jest
        .spyOn(BackendApi.prototype, 'listPermissions')
        .mockImplementation(async () => {
          await Promise.resolve(setTimeout(() => null, delayListPerm));
          return Promise.resolve(permissions);
        });
      permissionsApiCall.mockClear();

      const objectByPermissionApiCall = jest
        .spyOn(BackendApi.prototype, 'listObjectByPermissionViewSets')
        // @ts-expect-error the backend endpoint is malformed - should return a
        // list but returns an object instead
        .mockImplementation(async () => {
          await Promise.resolve(setTimeout(() => null, delayListObjectByPerm));
          return Promise.resolve(
            createObjectByPermission(permission.id, [
              createPermissionObject({
                id: 1,
                name: permissionObjectName,
              }),
            ]),
          );
        });
      objectByPermissionApiCall.mockClear();

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createStaffAuthState(),
          })}
        >
          <GroupForm
            group={createGroup({
              permissionsObject: [
                {
                  permission: viewNodePermissionId,
                  objects: [1],
                },
              ],
              users: [user],
              id: groupId,
            })}
            onClose={jest.fn()}
          />
        </Provider>,
      );
      // Wait for all API calls to be finished (no more spinners are visible,
      // text-/combobox all visible)
      await waitFor(
        () => {
          const boxes = screen.getAllByRole('combobox');
          expect(boxes).toHaveLength(4);
          expect(screen.getAllByRole('textbox')).toHaveLength(3);
          expect(boxes[2]).toHaveValue(permissionName);
        },
        { timeout },
      );
      await screen.findByText(permissionObjectName);

      expect(permissionsApiCall).toHaveBeenCalledTimes(1);
      expect(objectByPermissionApiCall).toHaveBeenCalledTimes(1);
    },
    timeout,
  );
});
