import {
  createAuthState,
  createNodeAdminAuthState,
  createStaffAuthState,
} from '../../../factories';
import { getInitialState, makeStore } from '../../store';
import { render, screen } from '@testing-library/react';
import { mockSuccessfulApiCall } from '../../testUtils';
import { NodeForm } from './NodeManageForm';
import { Provider } from 'react-redux';
import React from 'react';

describe('NodeManageForm', () => {
  beforeEach(() => {
    mockSuccessfulApiCall('uniqueNode', {});
    mockSuccessfulApiCall('listOAuth2Clients', []);
  });

  it.each`
    authState                     | enabled  | userType
    ${createStaffAuthState()}     | ${true}  | ${'Portal admin'}
    ${createNodeAdminAuthState()} | ${true}  | ${'node admin'}
    ${createAuthState()}          | ${false} | ${'regular user'}
  `(
    'Should allow ($enabled) to edit the OAuth2 client field if the user is a $userType',
    async ({ authState, enabled }) => {
      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: authState,
          })}
        >
          <NodeForm node={{}} onClose={jest.fn()} />
        </Provider>,
      );

      const clientField = await screen.findByLabelText(
        'node:columns.oauth2Client',
      );
      expect(clientField).toBeInTheDocument();
      if (!enabled) {
        expect(clientField).toHaveAttribute('aria-disabled');
      } else {
        expect(clientField).not.toHaveAttribute('aria-disabled');
      }
    },
  );
});
