import {
  type BaseReducerState,
  Description,
  Field,
  formatItemFields,
  type FormattedItemField,
  type ListModel,
  requestAction,
  type TabItem,
  TabPane,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  type DataProvider,
  type Group,
  GroupProfileRoleEnum,
  type Node,
} from '../../api/generated';
import {
  LOAD_DATA_TRANSFERS,
  LOAD_GROUPS,
  LOAD_NODES,
} from '../../actions/actionTypes';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { RolesTable, useGroups } from '../../widgets/RolesTable';
import { DataTransferTable } from '../dataTransfer/DataTransferTable';
import { getDataProviderColoredStatus } from './DataProviderManage';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { RootState } from '../../store';
import { Typography } from '@mui/material';
import { useDPDataTransfers } from '../dataTransfer/DataTransferHooks';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export type DataProviderDetailProps = {
  dataProvider: DataProvider | undefined;
};

export const DataProviderDetail = ({
  dataProvider,
}: DataProviderDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.DATA_PROVIDER]);
  const { t: emptyMessageTFunction } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.LIST,
  ]);
  const { itemList: groups, isFetching } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Group>>
  >((store) => store.groups);

  const { itemList: nodes } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Node>>
  >((state) => state.nodes);

  const dispatch = useReduxDispatch();

  useEffect(() => {
    if (dataProvider?.id) {
      dispatch(
        requestAction(LOAD_GROUPS, {
          roleEntityId: String(dataProvider.id),
          roleEntityType: 'dataprovider',
        }),
      );
      dispatch(requestAction(LOAD_DATA_TRANSFERS));
    }
    if (!nodes.length) {
      dispatch(requestAction(LOAD_NODES));
    }
  }, [dataProvider, dispatch, nodes.length]);

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<DataProvider> = {
      fields: [
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.code`) as string,
          getProperty: (dp: DataProvider) => dp.code,
          key: 'code',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.name`) as string,
          getProperty: (dp: DataProvider) => dp.name,
          key: 'name',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_PROVIDER}:columns.nodes`) as string,
          getProperty: (dp: DataProvider) =>
            nodes
              .filter((node) => dp.nodes.includes(node.code))
              .map((node) => `${node.name} (${node.code})`)
              .sort()
              .join(', '),
          key: 'nodes',
          hideIf: (dp: DataProvider) => dp.nodes.length === 0,
        }),
        Field({
          caption: t(
            `${I18nNamespace.DATA_PROVIDER}:columns.enabled`,
          ) as string,
          getProperty: (dp: DataProvider) =>
            getDataProviderColoredStatus(dp.enabled),
          key: 'enabled',
        }),
      ],
    };
    return dataProvider
      ? formatItemFields<DataProvider>(model, dataProvider)
      : null;
  }, [t, dataProvider, nodes]);

  const users = useGroups(groups);
  const dataTransfers = useDPDataTransfers(dataProvider);
  const tabModel: TabItem[] = useMemo(() => {
    return [
      {
        id: 'details',
        label: 'Details',
        content: <Description entries={detailEntries} labelWidth={300} />,
      },
      {
        id: 'users',
        label: 'Users',
        content: (
          <RolesTable
            roles={[
              GroupProfileRoleEnum.DPC,
              GroupProfileRoleEnum.DPDE,
              GroupProfileRoleEnum.DPM,
              GroupProfileRoleEnum.DPSO,
              GroupProfileRoleEnum.DPTA,
              GroupProfileRoleEnum.DPW,
            ]}
            isFetching={isFetching}
            users={users}
          />
        ),
      },
      {
        id: 'dataTransfers',
        label: 'Data Transfers',
        content: (
          <DataTransferTable
            dataTransfers={dataTransfers ?? []}
            canAdd={false}
          />
        ),
      },
    ];
  }, [dataTransfers, detailEntries, isFetching, users]);

  return dataProvider ? (
    <TabPane
      id="DataProviderDetail"
      label="Switch between details, users, and data transfers."
      model={tabModel}
      panelBoxProps={{ sx: { height: '50vh' } }}
    />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {emptyMessageTFunction(`${I18nNamespace.LIST}:emptyMessage`, {
        model: emptyMessageTFunction(
          `${I18nNamespace.COMMON}:models.dataProvider`,
        ),
      })}
    </Typography>
  );
};
