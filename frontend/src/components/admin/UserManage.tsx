import {
  type BaseReducerState,
  ColoredStatus,
  EnhancedTable,
  formatDate,
  getFilenameWithTimestamp,
  requestAction,
  type Status,
  ToastBarSnackBar,
  ToastSeverity,
  type Unpacked,
  useFormDialog,
  useLocaleSortingFn,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { type FormAddLocalUser, UserAddLocalForm } from './UserAddLocalForm';
import { type FormUser, UserForm } from './UserManageForm';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { Trans, useTranslation } from 'next-i18next';
import { createColumnHelper } from '@tanstack/react-table';
import { downloadFile } from '../../utils';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { isStaff } from '../selectors';
import { LOAD_USERS } from '../../actions/actionTypes';
import type { RootState } from '../../store';
import type { User } from '../../api/generated';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

export const UserList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
    I18nNamespace.BOOLEAN,
  ]);

  const {
    itemList: users,
    isFetching,
    isSubmitting,
  } = useSelector<RootState, BaseReducerState<IdRequired<User>>>(
    (state) => state.users,
  );

  const staff = useSelector(isStaff);

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_USERS, { includeInactive: true }));
  }, [dispatch]);

  const { closeFormDialog, data, openFormDialog } =
    useFormDialog<FormUser>(undefined);
  const {
    closeFormDialog: closeAddLocalFormDialog,
    data: addLocalData,
    openFormDialog: openAddLocalFormDialog,
  } = useFormDialog<FormAddLocalUser>({});
  const userForm = useMemo(() => {
    if (data) {
      return <UserForm user={data} onClose={closeFormDialog} />;
    }
    if (addLocalData) {
      return (
        <UserAddLocalForm
          user={addLocalData}
          onClose={closeAddLocalFormDialog}
        />
      );
    }
    return;
  }, [data, closeFormDialog, addLocalData, closeAddLocalFormDialog]);

  const localeSortingFn = useLocaleSortingFn<Unpacked<typeof users>>();

  const columns = useMemo(() => {
    const columnHelper = createColumnHelper<Unpacked<typeof users>>();
    const statuses: Status<boolean>[] = [
      {
        color: 'success',
        value: true,
        text: t(`${I18nNamespace.BOOLEAN}:true`),
      },
      {
        color: 'error',
        value: false,
        text: t(`${I18nNamespace.BOOLEAN}:false`),
      },
    ];
    return [
      columnHelper.accessor('firstName', {
        header: t('columns.firstName'),
        sortingFn: localeSortingFn,
      }),
      columnHelper.accessor('lastName', {
        header: t('columns.lastName'),
        sortingFn: localeSortingFn,
      }),
      columnHelper.accessor('email', {
        header: t('columns.email'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('username', {
        header: t('columns.username'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('openPgpKeys', {
        header: t('columns.pgpKeyFingerprint'),
        sortingFn: 'alphanumeric',
        cell: (props) =>
          props.getValue()?.filter((pgpkey) => pgpkey.status === 'APPROVED')[0]
            ?.fingerprint,
      }),
      // lastLogin might be null, and if that happens datetime sorting won't work
      columnHelper.accessor((user) => user.lastLogin ?? new Date(0), {
        id: 'lastLogin',
        header: t('columns.lastLogin'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.row.original.lastLogin, true) ?? '',
      }),
      columnHelper.accessor(
        (value) => {
          return t(`${I18nNamespace.BOOLEAN}:${value.isActive}`);
        },
        {
          id: 'isActive',
          header: t('columns.isActive'),
          sortingFn: 'alphanumeric',
          cell: ({ row: { original } }) => {
            return ColoredStatus<boolean>({
              value: original.isActive,
              statuses,
            });
          },
        },
      ),
    ];
  }, [t, localeSortingFn]);

  const onExport = useCallback(() => {
    setExporting(true);
    downloadFile(
      getFilenameWithTimestamp('users', '.csv'),
      generatedBackendApi.listUsersExports(),
      'text/csv',
      () => setExporting(false),
    );
  }, []);

  const [exporting, setExporting] = useState<boolean>(false);

  const router = useRouter();

  return (
    <>
      <ToastBarSnackBar
        severity={ToastSeverity.SUCCESS}
        open={exporting}
        autoHideDuration={6000}
      >
        <span>
          <Trans
            i18nKey={t(`${I18nNamespace.USER_LIST}:exporting`)}
            components={{
              break: <br />,
            }}
          />
        </span>
      </ToastBarSnackBar>
      <EnhancedTable
        itemList={users}
        columns={columns}
        canAdd={staff}
        canDelete={false} // it's forbidden to delete users
        onAdd={openAddLocalFormDialog}
        onRowClick={(item) => router.push(`/users/${item.id}`)}
        onEdit={openFormDialog}
        canExport={true}
        isExporting={exporting}
        onExport={onExport}
        form={userForm}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.user_plural`),
        })}
        addButtonLabel={t(`${I18nNamespace.USER_LIST}:addButton`)}
        inline
      />
    </>
  );
};
