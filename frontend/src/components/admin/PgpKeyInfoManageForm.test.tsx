import { allowedPgpKeyInfoStatusEnumSubset } from './PgpKeyInfoManageForm';
import { PgpKeyInfoStatusEnum } from '../../api/generated';

describe('allowedPgpKeyInfoStatusEnumSubset', () => {
  const allowedEnumSubsetFromPending = {
    [PgpKeyInfoStatusEnum.APPROVED]: PgpKeyInfoStatusEnum.APPROVED,
    [PgpKeyInfoStatusEnum.PENDING]: PgpKeyInfoStatusEnum.PENDING,
    [PgpKeyInfoStatusEnum.REJECTED]: PgpKeyInfoStatusEnum.REJECTED,
  };
  const allowedEnumSubsetFromApproved = {
    [PgpKeyInfoStatusEnum.APPROVAL_REVOKED]:
      PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
    [PgpKeyInfoStatusEnum.APPROVED]: PgpKeyInfoStatusEnum.APPROVED,
  };

  it.each`
    currentStatus                            | expectedEnumSubset
    ${PgpKeyInfoStatusEnum.PENDING}          | ${allowedEnumSubsetFromPending}
    ${PgpKeyInfoStatusEnum.APPROVED}         | ${allowedEnumSubsetFromApproved}
    ${PgpKeyInfoStatusEnum.APPROVAL_REVOKED} | ${{ [PgpKeyInfoStatusEnum.APPROVAL_REVOKED]: PgpKeyInfoStatusEnum.APPROVAL_REVOKED }}
  `(
    'Should return $expectedEnumSubset if current status is $currentStatus',
    function ({ currentStatus, expectedEnumSubset }) {
      expect(allowedPgpKeyInfoStatusEnumSubset(currentStatus)).toStrictEqual(
        expectedEnumSubset,
      );
    },
  );
});
