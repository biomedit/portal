import { getInitialState, makeStore } from '../../store';
import { render, screen } from '@testing-library/react';
import { createStaffAuthState } from '../../../factories';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import type { User } from '../../api/generated';
import { UserForm } from './UserManageForm';

describe('UserManageForm', () => {
  const staffAuthState = createStaffAuthState();
  const differentUser: User = {
    ...staffAuthState.user,
    username: 'differentUser',
  };

  const flagsApiCall = mockSuccessfulApiCall('listFlags', []);
  const groupsApiCall = mockSuccessfulApiCall('listGroups', []);
  const customAffiliationsApiCall = mockSuccessfulApiCall(
    'listCustomAffiliations',
    [],
  );

  beforeEach(() => {
    flagsApiCall.mockClear();
    groupsApiCall.mockClear();
    customAffiliationsApiCall.mockClear();
  });

  it.each`
    editedUser             | canDisable | description
    ${staffAuthState.user} | ${false}   | ${'prevent the currently logged in user from inactivating themself'}
    ${differentUser}       | ${true}    | ${'allow the currently logged in user to inactivate a different user'}
  `('should $description', async ({ editedUser, canDisable }) => {
    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: staffAuthState,
        })}
      >
        <UserForm user={editedUser} onClose={jest.fn()} />
      </Provider>,
    );

    const isActiveCheckbox = await screen.findByRole('checkbox');
    expect(isActiveCheckbox).toBeInTheDocument();
    if (canDisable) {
      expect(isActiveCheckbox).toBeEnabled();
    } else {
      expect(isActiveCheckbox).toBeDisabled();
    }

    expect(flagsApiCall).toHaveBeenCalledTimes(1);
    expect(groupsApiCall).toHaveBeenCalledTimes(1);
    expect(customAffiliationsApiCall).toHaveBeenCalledTimes(1);
  });
});
