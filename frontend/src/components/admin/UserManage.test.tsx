import { createBatch, createUser } from '../../../factories';
import { fireEvent, render, screen } from '@testing-library/react';
import { makeStore } from '../../store';
import { mockSuccessfulApiCall } from '../../testUtils';
import { mockTimeZone } from '@biomedit/next-widgets';
import { Provider } from 'react-redux';
import React from 'react';
import type { User } from '../../api/generated';
import { UserList } from './UserManage';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
    query: { id: '1' },
    push: jest.fn(),
  }),
}));

const assertHasTextContents = async (
  textContents: string[],
  identifier: string,
  order: number[],
) => {
  // cell_${i}_${identifier}
  const cells = await screen.findAllByTestId(`_${identifier}`, {
    exact: false,
  });
  for (let i = 0; i < order.length; i++) {
    // Cannot use 'toHaveTextContent' with empty value
    expect(cells[i].textContent).toBe(textContents[order[i]]);
  }
};

describe('UserList.fetchUsers', () => {
  it('should display empty list', async () => {
    mockSuccessfulApiCall('listUserLites', []);
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>,
    );
    // Promise is rejected if NOT found. Empty message (NOT i18n'ed) expected.
    await screen.findByText('list:emptyMessage');
  });

  it('should list the users', async () => {
    mockSuccessfulApiCall<User[]>('listUserLites', createBatch(createUser, 6));

    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>,
    );
    // Table header (1x), plus user rows (6x), plus table footer (1x)
    expect(await screen.findAllByRole('row')).toHaveLength(8);
  });

  it('should correctly sort the last login values', async () => {
    mockTimeZone('UTC');
    const expected = [
      '2019-05-02 14:46',
      '2020-07-09 12:22',
      '2021-01-03 08:20',
      '2019-12-03 10:20',
      '',
      '',
    ];
    const users = expected.map((lastLogin) => {
      const [year, month, day, hour, minute] = lastLogin
        .split(/[- :]/)
        .map((token) => parseInt(token));
      return createUser({
        lastLogin: year
          ? new Date(year, month - 1, day, hour, minute)
          : undefined,
      });
    });

    mockSuccessfulApiCall<User[]>('listUserLites', users);
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>,
    );
    const identifier = 'lastLogin';
    const sorted = [2, 1, 3, 0, 5, 4];
    await assertHasTextContents(expected, identifier, [0, 1, 2, 3, 4, 5]);
    // Sort asc
    const lastLoginHeader = await screen.findByText('columns.lastLogin');
    fireEvent.click(lastLoginHeader);
    await assertHasTextContents(expected, identifier, sorted);
    // Sort desc
    fireEvent.click(lastLoginHeader);
    await assertHasTextContents(expected, identifier, sorted.reverse());
  });

  it('should correctly sort the last names', async () => {
    const expected = [
      'kerluke',
      'Rowan',
      'norris',
      'österle',
      'schulz',
      'lagaffe',
    ];

    const users = expected.map((lastName) =>
      createUser({ lastName: lastName }),
    );
    mockSuccessfulApiCall<User[]>('listUserLites', users);
    render(
      <Provider store={makeStore()}>
        <UserList />
      </Provider>,
    );
    const identifier = 'lastName';
    const sorted = [0, 5, 2, 3, 1, 4];
    await assertHasTextContents(expected, identifier, [0, 1, 2, 3, 4, 5]);
    // Sort asc
    const lastNameHeader = await screen.findByText('columns.lastName');
    fireEvent.click(lastNameHeader);
    await assertHasTextContents(expected, identifier, sorted);
    // Sort desc
    fireEvent.click(lastNameHeader);
    await assertHasTextContents(expected, identifier, sorted.reverse());
  });
});
