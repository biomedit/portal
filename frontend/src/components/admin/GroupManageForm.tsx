import {
  ADD_GROUP,
  CALL,
  LOAD_PERMISSIONS,
  UPDATE_GROUP,
} from '../../actions/actionTypes';
import {
  AutocompleteField,
  type BaseReducerState,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  LabelledField,
  nameValidations,
  requestAction,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  type Group,
  GroupProfileRoleEnum,
  type Permission,
  type UniqueGroup,
} from '../../api/generated';
import {
  MultipleUsersTextField,
  useMultipleUsersTextField,
} from '../UserTextField';
import React, { type ReactElement, useEffect } from 'react';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { isStaff } from '../selectors';
import { PermissionsObjectList } from './PermissionsObjectList';
import type { RootState } from '../../store';
import { usePermissionChoices } from '../choice';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

type GroupFormProps = {
  group: Partial<Group>;
  onClose: () => void;
};

export function useGroupForm() {
  const newGroup = {
    name: undefined,
    users: [],
    permissions: [],
    permissionsObject: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Group>>(newGroup);
  const groupForm = data && (
    <GroupForm group={data} onClose={closeFormDialog} />
  );
  return {
    groupForm,
    ...rest,
  };
}

export const GroupForm = ({ group, onClose }: GroupFormProps): ReactElement => {
  const usersFieldName = 'users';
  const dispatch = useReduxDispatch();
  const { t } = useTranslation([
    I18nNamespace.GROUP_LIST,
    I18nNamespace.GROUP_MANAGE_FORM,
  ]);

  const isEdit = !!group.id;

  const { isFetching: isFetchingPermissions, itemList: permissions } =
    useSelector<RootState, BaseReducerState<IdRequired<Permission>>>(
      (state) => state.permissions,
    );

  const staff = useSelector(isStaff);
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.groups.isSubmitting,
  );
  const permissionChoices = usePermissionChoices(permissions);

  const form = useEnhancedForm<Group>({ defaultValues: group });

  function submit(activeGroup: Group) {
    const onSuccessAction = { type: CALL, callback: onClose };
    activeGroup.users = users;
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_GROUP,
          {
            id: String(activeGroup.id),
            group: activeGroup,
          },
          onSuccessAction,
        ),
      );
    } else {
      dispatch(
        requestAction(ADD_GROUP, { group: activeGroup }, onSuccessAction),
      );
    }
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_PERMISSIONS));
  }, [dispatch, group.id]);

  const uniqueCheck = async (value: Omit<UniqueGroup, 'id'>) =>
    generatedBackendApi.uniqueGroup({ uniqueGroup: value });

  const { users, deleteUser, addUser } = useMultipleUsersTextField({
    initialUsers: group.users ?? [],
  });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={t(
          `${I18nNamespace.GROUP_MANAGE_FORM}:title.${
            isEdit ? 'edit' : 'create'
          }`,
        )}
        open={!!group}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: group.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.name`)}
            validations={nameValidations()}
            unique={uniqueCheck}
            initialValues={group}
            fullWidth
            disabled={!staff}
          />
          <LabelledField
            name="profile.description"
            type="text"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.description`)}
            initialValues={group}
            fullWidth
            disabled={!staff}
            multiline
            rows={3}
            sx={{ marginBottom: 5 }}
          />
          <AutocompleteField
            name="profile.role"
            label={t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.role`)}
            initialValues={group}
            disabled={!staff || isEdit}
            width="100%"
            choices={useEnumChoices(GroupProfileRoleEnum).map((choice) => ({
              ...choice,
              label: t(`${I18nNamespace.GROUP_LIST}:roles.${choice.label}`),
            }))}
          />
          <MultipleUsersTextField
            addUser={addUser}
            deleteUser={deleteUser}
            legend={t(`${I18nNamespace.COMMON}:models.user_plural`)}
            name={usersFieldName}
            users={users}
          />
          <AutocompleteField
            name="permissions"
            label={t(
              `${I18nNamespace.GROUP_MANAGE_FORM}:captions.modelPermissions`,
            )}
            initialValues={group}
            choices={permissionChoices}
            isLoading={isFetchingPermissions}
            multiple
            width="100%"
            disabled={!staff}
          />
        </FormFieldsContainer>
        <h4>
          {t(`${I18nNamespace.GROUP_MANAGE_FORM}:captions.objectPermissions`)}
        </h4>
        <PermissionsObjectList />
      </FormDialog>
    </FormProvider>
  );
};
