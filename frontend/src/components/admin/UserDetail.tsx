import type { CustomAffiliation, User } from '../../api/generated';
import {
  Description,
  formatDate,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_FLAGS,
  LOAD_GROUPS,
} from '../../actions/actionTypes';
import React, { type ReactElement, useEffect, useMemo, useState } from 'react';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { useUserInfoFields } from '../Profile';

export type UserDetailProps = {
  user: User | undefined;
};

export const UserDetail = ({ user }: UserDetailProps): ReactElement => {
  const dispatch = useReduxDispatch();

  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.USER_MANAGE_FORM,
  ]);
  const { t: emptyMessageTFunction } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.LIST,
  ]);
  const customAffiliations = useSelector<RootState, Array<CustomAffiliation>>(
    (state) => state.customAffiliation.itemList,
  );
  const noProjectsPlaceholder = t(
    `${I18nNamespace.USER_MANAGE_FORM}:noProjectsPlaceholder`,
  );
  const [projects, setProjects] = useState<string>(noProjectsPlaceholder);
  const userInfoEntries = useUserInfoFields(user ?? null, customAffiliations);

  useEffect(() => {
    generatedBackendApi
      .listProjects({
        username: user?.username,
        ordering: 'name',
      })
      .then((userProjects) => {
        setProjects(
          userProjects?.length
            ? userProjects.map((project) => project.code).join(', ')
            : noProjectsPlaceholder,
        );
      });
  }, [noProjectsPlaceholder, user?.username, projects]);

  const openPgpKeysList = useMemo(
    () =>
      user?.openPgpKeys?.map((k) => {
        return (
          <div key={k.fingerprint}>
            <span
              style={
                k.status === 'APPROVED'
                  ? { fontFamily: 'monospace' }
                  : { fontFamily: 'monospace', color: 'gray' }
              }
            >
              {k.fingerprint}
            </span>{' '}
            <span>{k.keyUserId}</span>
            <span>{' (' + k.status + ')'}</span>
          </div>
        );
      }),
    [user?.openPgpKeys],
  );

  if (userInfoEntries) {
    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_LIST}:columns.lastLogin`) as string,
      component: formatDate(user?.lastLogin, true),
    });

    userInfoEntries.push({
      caption: t(
        `${I18nNamespace.USER_MANAGE_FORM}:captions.projects`,
      ) as string,
      component: projects,
    });

    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.flags`) as string,
      component: user?.flags?.length
        ? user?.flags?.map((f) => f).join(', ')
        : t(`${I18nNamespace.USER_MANAGE_FORM}:noFlagsPlaceholder`),
    });

    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_MANAGE_FORM}:captions.groups`) as string,
      component: user?.groups?.length
        ? user?.groups?.map((g) => g).join(', ')
        : t(`${I18nNamespace.USER_MANAGE_FORM}:noGroupsPlaceholder`),
    });

    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_LIST}:columns.isActive`) as string,
      component: t(`${I18nNamespace.BOOLEAN}:${user?.isActive}`),
    });

    userInfoEntries.push({
      caption: t(`${I18nNamespace.USER_LIST}:columns.pgpKeys`) as string,
      component: user?.openPgpKeys?.length
        ? openPgpKeysList
        : t(`${I18nNamespace.USER_MANAGE_FORM}:noPgpKeysPlaceholder`),
    });
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_FLAGS));
    dispatch(requestAction(LOAD_GROUPS));
    dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
  }, [dispatch, user?.username]);

  return user ? (
    <Description entries={userInfoEntries} labelWidth={300} />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {emptyMessageTFunction(`${I18nNamespace.LIST}:emptyMessage`, {
        model: emptyMessageTFunction(`${I18nNamespace.COMMON}:models.user`),
      })}
    </Typography>
  );
};
