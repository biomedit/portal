import {
  ADD_NODE,
  CALL,
  LOAD_OAUTH2_CLIENTS,
  UPDATE_NODE,
} from '../../actions/actionTypes';
import {
  type BaseReducerState,
  ConfirmLabel,
  email,
  FormDialog,
  HiddenField,
  httpsUrl,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  required,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { isNodeAdmin, isStaff } from '../selectors';
import {
  type Node,
  NodeNodeStatusEnum,
  type OAuth2Client,
  type UniqueNode,
} from '../../api/generated';
import React, { type ReactElement, useEffect } from 'react';
import { Box } from '@mui/material';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { IpRangeList } from '../../widgets/IpRangeList';
import type { RootState } from '../../store';
import { useOAuth2ClientChoices } from '../choice';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

type NodeFormProps = {
  node: Partial<Node>;
  onClose: () => void;
};

export function useNodeForm() {
  const newNode = {
    code: undefined,
    name: undefined,
    nodeStatus: NodeNodeStatusEnum.ON,
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Node>>(newNode);
  const nodeForm = data && <NodeForm node={data} onClose={closeFormDialog} />;
  return {
    nodeForm,
    ...rest,
  };
}

export const NodeForm = ({ node, onClose }: NodeFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.NODE]);

  const isEdit = !!node.id;

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_OAUTH2_CLIENTS));
  }, [dispatch]);

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.nodes.isSubmitting,
  );

  const staff = useSelector(isStaff);
  const nodeAdmin = useSelector(isNodeAdmin);
  const { itemList: clients } = useSelector<
    RootState,
    BaseReducerState<IdRequired<OAuth2Client>>
  >((store) => store.clients);

  const form = useEnhancedForm<Node>({ defaultValues: node });

  function submit(node: Node) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    if (isEdit) {
      onClose();
      dispatch(requestAction(UPDATE_NODE, { id: String(node.id), node }));
    } else {
      dispatch(
        requestAction(
          ADD_NODE,
          {
            node,
          },
          onSuccessAction,
        ),
      );
    }
  }

  const uniqueCheck = async (value: Omit<UniqueNode, 'id'>) =>
    generatedBackendApi.uniqueNode({ uniqueNode: value });

  const clientsChoices = useOAuth2ClientChoices(clients);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `Edit ${node?.code}` : 'New Node'}
        open={!!node}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: node.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.NODE}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={node}
            fullWidth
            disabled={isEdit}
          />
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.NODE}:columns.name`)}
            validations={nameValidations()}
            unique={uniqueCheck}
            initialValues={node}
            fullWidth
          />
          <SelectField
            name="nodeStatus"
            label={t(`${I18nNamespace.NODE}:columns.nodeStatus`)}
            initialValues={node}
            required
            choices={useEnumChoices(NodeNodeStatusEnum)}
          />
          <LabelledField
            name="ticketingSystemEmail"
            type="email"
            label={t(`${I18nNamespace.NODE}:columns.ticketingSystemEmail`)}
            validations={[required, email]}
            initialValues={node}
            fullWidth
          />
          <LabelledField
            name="objectStorageUrl"
            type="url"
            validations={[httpsUrl]}
            label={t(`${I18nNamespace.NODE}:columns.objectStorageUrl`)}
            initialValues={node}
            fullWidth
            disabled={!(staff || nodeAdmin)}
          />
          <SelectField
            name="oauth2Client"
            label={t(`${I18nNamespace.NODE}:columns.oauth2Client`)}
            initialValues={node}
            fullWidth
            disabled={!(staff || nodeAdmin)}
            choices={clientsChoices}
          />
        </FormFieldsContainer>
        <Box>
          <h4>{t(`${I18nNamespace.NODE}:columns.ipRanges`)}</h4>
          <IpRangeList />
        </Box>
      </FormDialog>
    </FormProvider>
  );
};
