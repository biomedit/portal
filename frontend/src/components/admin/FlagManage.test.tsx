import { render, screen } from '@testing-library/react';
import { createFlag } from '../../../factories';
import { FlagManageList } from './FlagManage';
import { makeStore } from '../../store';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';

describe('FlagManageList.fetchFlags', () => {
  it('should display empty list', async () => {
    mockSuccessfulApiCall('listFlags', []);

    render(
      <Provider store={makeStore()}>
        <FlagManageList />
      </Provider>,
    );
    // Empty message (NOT i18n'ed)
    expect(await screen.findByText('list:emptyMessage')).toBeInTheDocument();
  });

  it('should list the flags', async () => {
    mockSuccessfulApiCall('listFlags', [
      createFlag({ code: 'flag_1' }),
      createFlag({ code: 'flag_2' }),
    ]);

    render(
      <Provider store={makeStore()}>
        <FlagManageList />
      </Provider>,
    );
    // Table header (1x), plus flag rows (2x), plus table footer (1x)
    expect(await screen.findAllByRole('row')).toHaveLength(4);
    expect(await screen.findByText('flag_1')).toBeInTheDocument();
    expect(await screen.findByText('flag_2')).toBeInTheDocument();
  });
});
