import {
  ADD_DATA_PROVIDER,
  CALL,
  LOAD_NODES,
  UPDATE_DATA_PROVIDER,
} from '../../actions/actionTypes';
import {
  type BaseReducerState,
  CheckboxField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  nameValidations,
  requestAction,
  SelectField,
  useEnhancedForm,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import type {
  DataProvider,
  Node,
  UniqueDataProvider,
} from '../../api/generated';
import React, { type ReactElement, useEffect } from 'react';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { isStaff } from '../selectors';
import type { RootState } from '../../store';
import { useNodeChoices } from '../choice';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

type DataProviderFormProps = {
  dataProvider: Partial<DataProvider>;
  onClose: () => void;
};

export function useDataProviderForm() {
  const newDataProvider = {
    code: undefined,
    name: undefined,
    description: '',
    enabled: false,
    nodes: [],
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<DataProvider>>(newDataProvider);
  const dataProviderForm = data && (
    <DataProviderForm dataProvider={data} onClose={closeFormDialog} />
  );
  return {
    dataProviderForm,
    ...rest,
  };
}

export const DataProviderForm = ({
  dataProvider,
  onClose,
}: DataProviderFormProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.DATA_PROVIDER,
  ]);
  const staff = useSelector(isStaff);
  const isEdit = !!dataProvider.id;

  const dispatch = useReduxDispatch();

  const { itemList: nodes, isFetching: isFetchingNodes } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Node>>
  >((state) => state.nodes);
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.dataProvider.isSubmitting,
  );

  const form = useEnhancedForm<DataProvider>();

  function submit(dataProvider: DataProvider) {
    const onSuccessAction = { type: CALL, callback: onClose };
    if (isEdit) {
      onClose();
      dispatch(
        requestAction(UPDATE_DATA_PROVIDER, {
          id: String(dataProvider.id),
          dataProvider,
        }),
      );
    } else {
      dispatch(
        requestAction(
          ADD_DATA_PROVIDER,
          {
            dataProvider,
          },
          onSuccessAction,
        ),
      );
    }
  }

  useEffect(() => {
    if (!nodes.length) {
      dispatch(requestAction(LOAD_NODES));
    }
  }, [dispatch, nodes.length]);

  const uniqueCheck = async (
    value: Omit<UniqueDataProvider, 'id'> | undefined,
  ) => generatedBackendApi.uniqueDataProvider({ uniqueDataProvider: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `Edit ${dataProvider?.name}` : 'New Data Provider'}
        open={!!dataProvider}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
        noValidate
      >
        <HiddenField name="id" initialValues={{ id: dataProvider.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.DATA_PROVIDER}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={dataProvider}
            fullWidth
            disabled={isEdit}
          />
          <LabelledField
            name="name"
            type="text"
            label={t(`${I18nNamespace.DATA_PROVIDER}:columns.name`)}
            initialValues={dataProvider}
            validations={nameValidations()}
            unique={uniqueCheck}
            fullWidth
          />
          <CheckboxField
            name="enabled"
            label="Enabled"
            initialValues={dataProvider}
          />
          <SelectField
            name="nodes"
            multiple
            label={t(`${I18nNamespace.DATA_PROVIDER}:columns.nodes`)}
            initialValues={dataProvider}
            choices={useNodeChoices(nodes)}
            isLoading={isFetchingNodes}
            disabled={!staff}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
