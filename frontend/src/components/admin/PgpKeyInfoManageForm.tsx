import { CALL, UPDATE_PGP_KEY_INFO } from '../../actions/actionTypes';
import {
  ConfirmLabel,
  FormDialog,
  HiddenField,
  requestAction,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { type PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import React, { type ReactElement, useMemo } from 'react';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

const PendingPgpKeyInfoStatusEnum = {
  APPROVED: PgpKeyInfoStatusEnum.APPROVED,
  PENDING: PgpKeyInfoStatusEnum.PENDING,
  REJECTED: PgpKeyInfoStatusEnum.REJECTED,
};

const ApprovedPgpKeyInfoStatusEnum = {
  APPROVED: PgpKeyInfoStatusEnum.APPROVED,
  APPROVAL_REVOKED: PgpKeyInfoStatusEnum.APPROVAL_REVOKED,
};

export const allowedPgpKeyInfoStatusEnumSubset = (
  currentStatus: PgpKeyInfoStatusEnum,
) => {
  switch (currentStatus) {
    case PgpKeyInfoStatusEnum.PENDING:
      return PendingPgpKeyInfoStatusEnum;
    case PgpKeyInfoStatusEnum.APPROVED:
      return ApprovedPgpKeyInfoStatusEnum;
    case PgpKeyInfoStatusEnum.APPROVAL_REVOKED:
    case PgpKeyInfoStatusEnum.DELETED:
    case PgpKeyInfoStatusEnum.KEY_REVOKED:
    case PgpKeyInfoStatusEnum.REJECTED:
      return { [String(currentStatus)]: currentStatus };
  }
};

type PgpKeyInfoManageFormProps = {
  onClose: () => void;
  pgpKeyInfo: Partial<PgpKeyInfo>;
};

export const usePgpKeyInfoForm = () => {
  const newPgpKeyInfo = {
    id: undefined,
    status: PgpKeyInfoStatusEnum.PENDING,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<PgpKeyInfo>>(newPgpKeyInfo);
  const pgpKeyInfoForm = data && (
    <PgpKeyInfoManageForm pgpKeyInfo={data} onClose={closeFormDialog} />
  );
  return {
    pgpKeyInfoForm,
    ...rest,
  };
};

export const PgpKeyInfoManageForm = ({
  pgpKeyInfo,
  onClose,
}: PgpKeyInfoManageFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.PGP_KEY_INFO_LIST]);
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.pgpKeyInfo.isSubmitting,
  );
  const dispatch = useReduxDispatch();
  const form = useEnhancedForm<PgpKeyInfo>();
  const pgpKeyInfoStatusEnumSubset = useMemo(
    () =>
      pgpKeyInfo.status
        ? allowedPgpKeyInfoStatusEnumSubset(pgpKeyInfo.status)
        : {},
    [pgpKeyInfo.status],
  );

  function submit(formPgpKeyInfo: PgpKeyInfo) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    dispatch(
      requestAction(
        UPDATE_PGP_KEY_INFO,
        {
          id: String(pgpKeyInfo.id),
          pgpKeyInfo: formPgpKeyInfo,
        },
        onSuccessAction,
      ),
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={pgpKeyInfo?.keyUserId}
        open={!!pgpKeyInfo}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          <HiddenField name="id" initialValues={{ id: pgpKeyInfo.id }} />
          <SelectField
            name="status"
            label={t(`${I18nNamespace.PGP_KEY_INFO_LIST}:columns.status`)}
            required
            initialValues={{ status: pgpKeyInfo.status }}
            choices={useEnumChoices(pgpKeyInfoStatusEnumSubset)}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
