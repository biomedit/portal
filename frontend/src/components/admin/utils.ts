import {
  type DataTransferDataRecipientsInner,
  type Group,
  GroupProfileRoleEnum,
} from '../../api/generated';

export const groupsToRoleUsers = (
  groups: Group[],
  role: GroupProfileRoleEnum,
): DataTransferDataRecipientsInner[] =>
  groups.find((group) => group.profile?.role == role)?.users || [];
