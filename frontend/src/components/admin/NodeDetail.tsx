import {
  type BaseReducerState,
  Description,
  Field,
  formatItemFields,
  type FormattedItemField,
  type ListModel,
  requestAction,
  type TabItem,
  TabPane,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  GroupProfileRoleEnum,
  type Node,
  type OAuth2Client,
} from '../../api/generated';
import { LOAD_GROUPS, LOAD_OAUTH2_CLIENTS } from '../../actions/actionTypes';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { RolesTable, useGroups } from '../../widgets/RolesTable';
import type { GroupState } from '../../reducers/group';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { IpRangeSummary } from '../../widgets/IpRangeList';
import type { RootState } from '../../store';
import { Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export type NodeDetailProps = {
  node: Node | undefined;
};

export const NodeDetail = ({ node }: NodeDetailProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.NODE]);
  const { itemList: groups, isFetching } = useSelector<RootState, GroupState>(
    (store) => store.groups,
  );
  const { itemList: clients } = useSelector<
    RootState,
    BaseReducerState<IdRequired<OAuth2Client>>
  >((store) => store.clients);
  const dispatch = useReduxDispatch();
  const { t: emptyMessageTFunction } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.LIST,
  ]);

  useEffect(() => {
    if (node?.id) {
      dispatch(
        requestAction(LOAD_GROUPS, {
          roleEntityId: String(node.id),
          roleEntityType: 'node',
        }),
      );
      dispatch(requestAction(LOAD_OAUTH2_CLIENTS));
    }
  }, [node, dispatch]);

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<Node> = {
      fields: [
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.code`),
          getProperty: (node: Node) => node.code,
          key: 'code',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.name`),
          getProperty: (node: Node) => node.name,
          key: 'name',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.nodeStatus`),
          getProperty: (node: Node) => node.nodeStatus,
          key: 'nodeStatus',
        }),
        Field({
          caption: t(
            `${I18nNamespace.NODE}:columns.ticketingSystemEmail`,
          ) as string,
          getProperty: (node: Node) => node.ticketingSystemEmail,
          key: 'ticketingSystemEmail',
        }),
        Field({
          caption: t(
            `${I18nNamespace.NODE}:columns.objectStorageUrl`,
          ) as string,
          getProperty: (node: Node) => node.objectStorageUrl,
          key: 'objectStorageUrl',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.oauth2Client`),
          getProperty: (node: Node) => {
            const client = clients.find(
              (client: OAuth2Client) => client.id === node.oauth2Client,
            );
            return client ? `${client.clientName} (${client.redirectUri})` : '';
          },
          key: 'client',
        }),
        Field({
          caption: t(`${I18nNamespace.NODE}:columns.ipRanges`) as string,
          getProperty: (node: Node) =>
            !node.ipAddressRanges || node.ipAddressRanges.length === 0
              ? t(`${I18nNamespace.COMMON}:noValuePlaceholder`)
              : IpRangeSummary(node.ipAddressRanges),
          key: 'ipAddressRange',
        }),
      ],
    };
    return node ? formatItemFields<Node>(model, node) : null;
  }, [t, node, clients]);

  const users = useGroups(groups);
  const tabModel: TabItem[] = useMemo(() => {
    const tabItems = [
      {
        id: 'details',
        label: 'Details',
        content: <Description entries={detailEntries} labelWidth={300} />,
      },
      {
        id: 'users',
        label: 'Users',
        content: (
          <RolesTable
            roles={[
              GroupProfileRoleEnum.NA,
              GroupProfileRoleEnum.NM,
              GroupProfileRoleEnum.NV,
            ]}
            isFetching={isFetching}
            users={users}
          />
        ),
      },
    ];
    return tabItems;
  }, [detailEntries, isFetching, users]);

  return node ? (
    <TabPane
      id="NodeDetail"
      label="Switch between details and users of the node."
      model={tabModel}
      panelBoxProps={{ sx: { height: '50vh' } }}
    />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {emptyMessageTFunction(`${I18nNamespace.LIST}:emptyMessage`, {
        model: emptyMessageTFunction(`${I18nNamespace.COMMON}:models.node`),
      })}
    </Typography>
  );
};
