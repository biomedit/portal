import {
  ADD_FLAG,
  CALL,
  LOAD_USERS,
  UPDATE_FLAG,
} from '../../actions/actionTypes';
import {
  AutocompleteField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  idValidations,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import type { Flag, UniqueFlag } from '../../api/generated';
import React, { type ReactElement, useEffect } from 'react';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { useUserChoices } from '../choice';

type FlagFormProps = {
  flag: Partial<Flag>;
  onClose: () => void;
};

export const useFlagForm = () => {
  const newFlag = {
    code: undefined,
    description: undefined,
    users: [],
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<Flag>>(newFlag);
  const flagForm = data && (
    <FlagManageForm flag={data} onClose={closeFormDialog} />
  );
  return {
    flagForm,
    ...rest,
  };
};

export const FlagManageForm = ({
  flag,
  onClose,
}: FlagFormProps): ReactElement => {
  const { t } = useTranslation([I18nNamespace.FLAG_LIST]);
  const dispatch = useReduxDispatch();

  const { itemList: users, isFetching: isFetchingUsers } = useSelector(
    (state: RootState) => state.users,
  );
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.flags.isSubmitting,
  );
  const isEdit = !!flag.id;

  const form = useEnhancedForm<Flag>();

  function submit(formFlag: Flag) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    if (isEdit) {
      dispatch(
        requestAction(
          UPDATE_FLAG,
          {
            id: String(flag.id),
            flag: formFlag,
          },
          onSuccessAction,
        ),
      );
    } else {
      dispatch(requestAction(ADD_FLAG, { flag: formFlag }, onSuccessAction));
    }
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_USERS));
  }, [dispatch]);

  const uniqueCheck = async (value: Omit<UniqueFlag, 'id'>) =>
    generatedBackendApi.uniqueFlag({ uniqueFlag: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `Flag: ${flag?.code}` : 'New Flag'}
        open={!!flag}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: flag.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="code"
            type="text"
            label={t(`${I18nNamespace.FLAG_LIST}:columns.code`)}
            validations={idValidations()}
            unique={uniqueCheck}
            initialValues={flag}
            fullWidth
          />
          <LabelledField
            name="description"
            type="text"
            multiline
            validations={required}
            label={t(`${I18nNamespace.FLAG_LIST}:columns.description`)}
            initialValues={flag}
            fullWidth
            style={{ marginBottom: 15 }}
          />
          <AutocompleteField
            name="users"
            label={t(`${I18nNamespace.FLAG_LIST}:columns.users`)}
            initialValues={flag}
            choices={useUserChoices(users)}
            isLoading={isFetchingUsers}
            multiple
            width="100%"
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
