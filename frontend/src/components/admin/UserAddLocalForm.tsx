import { ADD_LOCAL_USER, CALL } from '../../actions/actionTypes';
import {
  ConfirmLabel,
  email,
  FormDialog,
  LabelledField,
  requestAction,
  required,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement } from 'react';
import type { UniqueUser, User } from '../../api/generated';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../api/api';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';

export type FormAddLocalUser = Pick<
  Partial<User>,
  'email' | 'firstName' | 'lastName'
>;

type UserFormProps = {
  onClose: () => void;
  user: FormAddLocalUser;
};

export const UserAddLocalForm = ({
  user,
  onClose,
}: UserFormProps): ReactElement => {
  const dispatch = useReduxDispatch();

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.users.isSubmitting,
  );

  const form = useEnhancedForm<User>();

  function submit(user: User) {
    dispatch(
      requestAction(
        ADD_LOCAL_USER,
        { user },
        { type: CALL, callback: onClose },
      ),
    );
  }

  const uniqueCheck = async (value: Omit<UniqueUser, 'id'>) =>
    generatedBackendApi.uniqueUser({ uniqueUser: value });

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title="Add local user"
        open={!!user}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <FormFieldsContainer>
          <LabelledField
            name="firstName"
            type="text"
            label="First name"
            fullWidth
            validations={required}
          />
          <LabelledField
            name="lastName"
            type="text"
            label="Last name"
            fullWidth
            validations={required}
          />
          <LabelledField
            name="email"
            type="email"
            label="Email address"
            validations={[required, email]}
            unique={uniqueCheck}
            fullWidth
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
