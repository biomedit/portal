import {
  createBatch,
  createDataProvider,
  createDataProviderAdminAuthState,
  createNode,
  createNodeAdminAuthState,
  createStaffAuthState,
} from '../../../factories';
import { getInitialState, makeStore } from '../../store';
import { render, screen } from '@testing-library/react';
import { DataProviderList } from './DataProviderManage';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
  }),
}));

describe('DataProviderList', () => {
  it('should display empty list', async () => {
    mockSuccessfulApiCall('listDataProviders', []);

    render(
      <Provider store={makeStore()}>
        <DataProviderList />
      </Provider>,
    );
    await screen.findByText('list:emptyMessage');
  });

  const nodes = createBatch(createNode, 2);
  const userDataProvider = createDataProvider({
    id: 1,
    code: 'exists',
    nodes: [nodes[0].code],
  });
  const otherDataProvider = createDataProvider({
    id: 2,
    nodes: nodes.map((node) => node.code),
  });
  const dataProviderAuthState = createDataProviderAdminAuthState([
    userDataProvider,
  ]);

  it.each`
    userState                     | userType                 | expectedRows
    ${createStaffAuthState()}     | ${'staff'}               | ${2}
    ${createNodeAdminAuthState()} | ${'node admin'}          | ${2}
    ${dataProviderAuthState}      | ${'data provider admin'} | ${1}
  `(
    'should list $expectedRows data providers if user is $userType',
    async ({ userState, expectedRows }) => {
      mockSuccessfulApiCall(
        'listDataProviders',
        userState.user?.username === dataProviderAuthState.user?.username
          ? [userDataProvider]
          : [userDataProvider, otherDataProvider],
      );
      mockSuccessfulApiCall('listNodes', nodes);

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: userState,
          })}
        >
          <DataProviderList />
        </Provider>,
      );
      // Table header (1x), plus user rows, plus table footer (1x)
      expect(await screen.findAllByRole('row')).toHaveLength(expectedRows + 2);
    },
  );

  afterEach(() => {
    jest.clearAllMocks();
  });
});
