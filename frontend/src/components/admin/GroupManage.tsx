import { DELETE_GROUP, LOAD_GROUPS } from '../../actions/actionTypes';
import {
  EnhancedTable,
  requestAction,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { isGroupManager, isStaff } from '../selectors';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
} from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { useGroupForm } from './GroupManageForm';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const GroupList = (): ReactElement => {
  const { t } = useTranslation([I18nNamespace.GROUP_LIST, I18nNamespace.LIST]);

  const {
    itemList: groups,
    isFetching,
    isSubmitting,
  } = useSelector((state: RootState) => state.groups);
  const groupManager = useSelector(isGroupManager);
  const staff = useSelector(isStaff);

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_GROUPS, { managed: !staff }));
  }, [staff, dispatch]);

  const deleteItem = useCallback(
    (id: number) => {
      dispatch(requestAction(DELETE_GROUP, { id: String(id) }));
    },
    [dispatch],
  );

  const { openFormDialog, groupForm } = useGroupForm();

  const getDeleteConfirmationText = useCallback(
    (item: Unpacked<typeof groups>) => 'Delete ' + item.name,
    [],
  );

  const columnHelper = createColumnHelper<Unpacked<typeof groups>>();
  const columns = useMemo(() => {
    return [
      columnHelper.accessor('name', {
        header: t('columns.name'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('profile.role', {
        header: t('columns.role'),
        sortingFn: 'alphanumeric',
        cell: (props) => {
          return props.getValue()
            ? t(`${I18nNamespace.GROUP_LIST}:roles.${props.getValue()}`)
            : null;
        },
      }),
      columnHelper.accessor('users', {
        header: t('columns.users'),
        cell: (props) => props.getValue().length,
      }),
    ];
  }, [t, columnHelper]);

  return (
    <EnhancedTable
      itemList={groups}
      columns={columns}
      canAdd={staff}
      canEdit={staff || groupManager}
      canDelete={staff}
      onAdd={openFormDialog}
      onEdit={openFormDialog}
      onDelete={deleteItem}
      form={groupForm}
      isFetching={isFetching}
      getDeleteConfirmationText={getDeleteConfirmationText}
      isSubmitting={isSubmitting}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.group_plural`),
        }) as string
      }
      addButtonLabel={t(`${I18nNamespace.GROUP_LIST}:addButton`) as string}
      inline
    />
  );
};
