import {
  ColoredStatus,
  EnhancedTable,
  requestAction,
  type Status,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import { isStaff } from '../selectors';
import { LOAD_NODES } from '../../actions/actionTypes';
import { NodeNodeStatusEnum } from '../../api/generated';
import type { RootState } from '../../store';
import { useNodeForm } from './NodeManageForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const NodeList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.NODE,
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
  ]);

  const {
    itemList: nodes,
    isFetching,
    isSubmitting,
  } = useSelector((state: RootState) => state.nodes);

  const staff = useSelector(isStaff);
  const username = useSelector<RootState, string | undefined>(
    (state) => state.auth.user?.username,
  );

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_NODES, staff ? {} : { username }));
  }, [dispatch, username, staff]);

  const { openFormDialog, nodeForm } = useNodeForm();
  const columnHelper = createColumnHelper<Unpacked<typeof nodes>>();
  const columns = useMemo(() => {
    const statuses: Status<NodeNodeStatusEnum>[] = [
      {
        color: 'success',
        value: NodeNodeStatusEnum.ON,
      },
      {
        color: 'error',
        value: NodeNodeStatusEnum.OFF,
      },
    ];
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('name', {
        header: t('columns.name'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('nodeStatus', {
        header: t('columns.nodeStatus'),
        cell: (props) =>
          ColoredStatus<NodeNodeStatusEnum>({
            value: props.getValue(),
            statuses,
          }),
      }),
    ];
  }, [t, columnHelper]);

  const router = useRouter();

  return (
    <EnhancedTable
      itemList={nodes}
      columns={columns}
      canAdd={staff}
      onEdit={openFormDialog}
      onAdd={openFormDialog}
      onRowClick={(item) => router.push(`/nodes/${item.id}`)}
      form={nodeForm}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      addButtonLabel={t(`${I18nNamespace.NODE}:addButton`) as string}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.node_plural`),
        }) as string
      }
      inline
    />
  );
};
