import { BackendApi, type Group, type Permission } from '../../api/generated';
import {
  createBatch,
  createGroup,
  createObjectByPermission,
  createPermission,
  createPermissionObject,
  createStaffAuthState,
} from '../../../factories';
import { getInitialState, makeStore } from '../../store';
import {
  initialBaseReducerState,
  mockConsoleError,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { render, renderHook, screen, waitFor } from '@testing-library/react';
import { FormProvider } from 'react-hook-form';
import type { IdRequired } from '../../global';
import { PermissionsObjectList } from './PermissionsObjectList';
import { Provider } from 'react-redux';
import React from 'react';

describe('PermissionsObjectList', () => {
  const viewNodePermissionId = 44;
  const permissionObjectName = 'Node 1 (node_1)';

  // TODO 2024-04-30, Christian Ribeaud: See https://gitlab.com/biomedit/portal/-/merge_requests/1157.
  let consoleError: jest.SpyInstance;
  beforeEach(() => (consoleError = mockConsoleError()));
  afterEach(() => consoleError.mockRestore());

  it.each`
    delayListObjectByPerm
    ${0}
    ${500}
    ${2000}
  `(
    'should contain both permission and object when ' +
      'call to list object by permission is delayed by $delayListObjectByPerm ms',
    async ({ delayListObjectByPerm }) => {
      const spy = jest
        .spyOn(BackendApi.prototype, 'listObjectByPermissionViewSets')
        // @ts-expect-error the backend endpoint is malformed - should return a
        // list but returns an object instead
        .mockImplementation(async () => {
          await Promise.resolve(setTimeout(() => null, delayListObjectByPerm));
          return Promise.resolve(
            createObjectByPermission(viewNodePermissionId, [
              createPermissionObject({
                id: 1,
                name: permissionObjectName,
              }),
            ]),
          );
        });
      spy.mockClear();

      const { result } = renderHook(() =>
        useEnhancedForm<Group>({
          defaultValues: createGroup({
            permissionsObject: [
              { permission: viewNodePermissionId, objects: [1] },
            ],
          }),
        }),
      );
      const form = result.current;
      const permissionName = 'Can view node';

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createStaffAuthState(),
            permissions: {
              ...initialBaseReducerState<IdRequired<Permission>>(),
              itemList: [
                createPermission({
                  id: viewNodePermissionId,
                  name: permissionName,
                }),
                ...createBatch(createPermission, 9),
              ],
            },
          })}
        >
          <FormProvider {...form}>
            <PermissionsObjectList />
          </FormProvider>
        </Provider>,
      );

      // wait for all API calls to be finished (no more spinners are visible,
      // comboboxes all visible)
      const comboboxes = await waitFor(
        () => {
          const boxes = screen.getAllByRole('combobox');
          expect(boxes).toHaveLength(2);
          return boxes;
        },
        { timeout: 5000 },
      );

      expect(comboboxes[0]).toHaveValue(permissionName);
      await screen.findByText(permissionObjectName);

      expect(spy).toHaveBeenCalledTimes(1);
    },
  );
});
