import {
  AutocompleteField,
  type BaseReducerState,
  CheckboxField,
  ConfirmLabel,
  FormDialog,
  HiddenField,
  requestAction,
  SelectField,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import type { CustomAffiliation, Flag, Group, User } from '../../api/generated';
import {
  LOAD_CUSTOM_AFFILIATIONS,
  LOAD_FLAGS,
  LOAD_GROUPS,
  UPDATE_USER,
} from '../../actions/actionTypes';
import React, { type ReactElement, useEffect } from 'react';
import {
  useCustomAffiliationChoices,
  useFlagChoices,
  useGroupChoices,
} from '../choice';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { Required } from 'utility-types';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export type FormUser = Required<Partial<User>, 'id'>;

type UserFormProps = {
  onClose: () => void;
  user: FormUser;
};

export const UserForm = ({ user, onClose }: UserFormProps): ReactElement => {
  const dispatch = useReduxDispatch();
  const { t } = useTranslation([
    I18nNamespace.USER_LIST,
    I18nNamespace.USER_MANAGE_FORM,
  ]);

  const { isFetching: isFetchingFlags, itemList: flags } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Flag>>
  >((state) => state.flags);

  const { isFetching: isFetchingGroups, itemList: groups } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Group>>
  >((state) => state.groups);

  const {
    isFetching: isFetchingCustomAffiliations,
    itemList: customAffiliations,
  } = useSelector<RootState, BaseReducerState<IdRequired<CustomAffiliation>>>(
    (state) => state.customAffiliation,
  );

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.users.isSubmitting,
  );
  const currentUsername = useSelector<RootState, string | undefined>(
    (state) => state.auth.user?.username,
  );

  const form = useEnhancedForm<User>();

  function submit(formUser: User) {
    onClose();
    dispatch(
      requestAction(UPDATE_USER, { user: formUser, id: String(formUser.id) }),
    );
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_FLAGS));
    dispatch(requestAction(LOAD_GROUPS));
    dispatch(requestAction(LOAD_CUSTOM_AFFILIATIONS));
  }, [dispatch]);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={t(`${I18nNamespace.USER_MANAGE_FORM}:title`)}
        open={!!user}
        confirmLabel={ConfirmLabel.SAVE}
        onSubmit={submit}
        onClose={onClose}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: user.id }} />
        <FormFieldsContainer>
          <AutocompleteField
            name="profile.customAffiliation"
            label={t(
              `${I18nNamespace.USER_MANAGE_FORM}:captions.customAffiliation`,
            )}
            initialValues={user}
            choices={useCustomAffiliationChoices(customAffiliations)}
            isLoading={isFetchingCustomAffiliations}
            width="100%"
          />
          <SelectField
            name="flags"
            label={t(`${I18nNamespace.USER_MANAGE_FORM}:captions.flags`)}
            initialValues={user}
            multiple
            choices={useFlagChoices(flags)}
            isLoading={isFetchingFlags}
          />
          <SelectField
            name="groups"
            label={t(`${I18nNamespace.USER_MANAGE_FORM}:captions.groups`)}
            initialValues={user}
            multiple
            choices={useGroupChoices(groups)}
            isLoading={isFetchingGroups}
          />
          <CheckboxField
            name="isActive"
            label={t(`${I18nNamespace.USER_LIST}:columns.isActive`)}
            initialValues={user}
            disabled={currentUsername === user.username}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
