import {
  DELETE_DATA_TRANSFER,
  LOAD_DATA_TRANSFERS,
} from '../../actions/actionTypes';
import {
  EnhancedTable,
  getFilenameWithTimestamp,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  useDataTransferColumns,
  useGlobalDataTransferFilter,
} from './DataTransferHooks';
import { downloadFile } from '../../utils';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { useDataTransferForm } from './DataTransferForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const DataTransferOverviewTable = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
    I18nNamespace.DATA_TRANSFER,
  ]);

  const {
    isFetching,
    isSubmitting,
    itemList: dataTransfers,
  } = useSelector((state: RootState) => state.dataTransfers);

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_DATA_TRANSFERS));
  }, [dispatch]);

  const deleteItem = useCallback(
    (id: number) => {
      dispatch(requestAction(DELETE_DATA_TRANSFER, { id: String(id) }));
    },
    [dispatch],
  );

  const { dtrForm } = useDataTransferForm();

  const columns = useDataTransferColumns(true);

  const globalFilterFn = useGlobalDataTransferFilter();

  const [exporting, setExporting] = useState<boolean>(false);

  const onExport = useCallback(() => {
    setExporting(true);
    downloadFile(
      getFilenameWithTimestamp('data_transfers', '.csv'),
      generatedBackendApi.listDataTransfersExports(),
      'text/csv',
      () => setExporting(false),
    );
  }, []);

  const router = useRouter();

  return (
    <>
      <EnhancedTable
        itemList={dataTransfers}
        columns={columns}
        canAdd={false} // it should only be possible to add data transfers from the projects tabs
        canEdit={false} // it should only be possible to edit data transfers from the detailed view
        canDelete={false} // it should only be possible to delete data transfers from the detailed view
        onRowClick={(item) => router.push(`/data-transfers/${item.id}`)}
        form={dtrForm}
        onDelete={deleteItem}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
          }) as string
        }
        globalFilterFn={globalFilterFn}
        globalFilterHelperText={
          t(`${I18nNamespace.DATA_TRANSFER}:globalFilterHelperText`) as string
        }
        canExport={dataTransfers.length > 0}
        isExporting={exporting}
        onExport={onExport}
        inline
      />
    </>
  );
};
