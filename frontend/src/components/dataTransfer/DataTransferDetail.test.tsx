import {
  createDataTransfer,
  createDataTransferDataProviderApproval,
  createDataTransferNodeApproval,
  createStaffAuthState,
} from '../../../factories';
import {
  DataTransferDataProviderApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInnerStatusEnum,
} from '../../api/generated';
import { fireEvent, render, screen } from '@testing-library/react';
import { getInitialState, makeStore } from '../../store';
import { DataTransferDetail } from './DataTransferDetail';
import type { ExpectedDataTransfer } from '../../reducers/dataTransfer';
import { Provider } from 'react-redux';
import React from 'react';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
  }),
}));

const projectName = 'Test';
const id = 2;
const dataProvider = 'dataProvider';

const dataTransferWithoutApprovals = createDataTransfer({
  id: id,
  dataProviderName: dataProvider,
  projectName: projectName,
  nodeApprovals: [],
  dataProviderApprovals: [],
  groupApprovals: [],
});

const dataTransfer = {
  ...dataTransferWithoutApprovals,
  nodeApprovals: [
    createDataTransferNodeApproval({
      status: DataTransferNodeApprovalsInnerStatusEnum.W,
      canApprove: true,
    }),
  ],
  dataProviderApprovals: [
    createDataTransferDataProviderApproval({
      status: DataTransferDataProviderApprovalsInnerStatusEnum.W,
      canApprove: true,
    }),
  ],
};

const createInitialState = (dtr: ExpectedDataTransfer) => {
  return {
    ...getInitialState(),
    auth: createStaffAuthState(),
    ...{
      dataTransfers: {
        isFetching: false,
        isSubmitting: false,
        itemList: [dtr],
      },
    },
  };
};

describe('DataTransferDetail', () => {
  describe('DataTransferDetail', () => {
    it('should display details for a given DTR', async () => {
      const store = makeStore(undefined, createInitialState(dataTransfer));
      render(
        <Provider store={store}>
          <DataTransferDetail dataTransfer={dataTransfer} />
        </Provider>,
      );

      const tabs = await screen.findAllByRole('tab');
      expect(tabs).toHaveLength(3);

      // First tab is selected
      await screen.findByText('columns.projectName');
      await screen.findByText('columns.dataProvider');
      await screen.findByText(dataProvider);
      await screen.findByText('columns.transferredPackages');

      expect(
        screen.queryByText('common:models.node common:models.approval_plural'),
      ).toBeNull();

      // Approvals tab
      fireEvent.click(tabs[2]);
      await screen.findByText(
        'common:models.node common:models.approval_plural',
      );
      // Two buttons to reject
      expect(await screen.findAllByText('actionButtons.reject')).toHaveLength(
        2,
      );
      // Two buttons to approve
      expect(await screen.findAllByText('actionButtons.approve')).toHaveLength(
        2,
      );
    });
  });
});
