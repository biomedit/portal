import React, { type ReactElement } from 'react';
import type { DataTransfer } from '../../api/generated';

export function RequestorField({
  requestorFullName,
  requestorEmail,
  requestorDisplayId,
}: Pick<
  DataTransfer,
  'requestorDisplayId' | 'requestorEmail' | 'requestorFullName'
>): ReactElement {
  return (
    <span>
      {requestorFullName}
      {requestorDisplayId ? ` (${requestorDisplayId})` : ''} ({requestorEmail})
    </span>
  );
}
