import { ButtonGroup, Stack, Typography } from '@mui/material';
import { ColoredStatus, formatDate, Tooltip } from '@biomedit/next-widgets';
import type { DataTransferApproval, DataTransferStatus } from '../../global';
import React, { type ReactElement, useCallback } from 'react';
import { ApprovalInstitutionEnum } from '../../api/generated';
import Button from '@mui/material/Button';
import camelCase from 'lodash/camelCase';
import Grid from '@mui/material/Grid2';
import { I18nNamespace } from '../../i18n';
import { isDataProviderApproval } from '../../guards';
import { useApprovalStatus } from './DataTransferHooks';
import { useTranslation } from 'next-i18next';

export type ApprovalDialogState = {
  action: ApprovalAction;
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
};

export enum ApprovalAction {
  ACCEPT,
  REJECT,
}

type DataTransferApprovalsStackProps = {
  approvalInstitution: ApprovalInstitutionEnum;
  approvals: Array<DataTransferApproval>;
  disableButtons?: boolean;
  setApprovalState: (approvalState: ApprovalDialogState) => void;
  waitingStatus: DataTransferStatus;
};

export const getTitle = (
  approval: DataTransferApproval,
  approvalInstitution: ApprovalInstitutionEnum,
) => {
  const approvalType = camelCase(approvalInstitution);
  const name = approval[approvalType].name;
  if ('code' in approval[approvalType]) {
    return `${name} (${approval[approvalType].code})`;
  }
  return name;
};

export const DataTransferApprovalsStack = ({
  approvals,
  approvalInstitution,
  setApprovalState,
  waitingStatus,
  disableButtons,
}: DataTransferApprovalsStackProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_TRANSFER,
    I18nNamespace.DATA_TRANSFER_APPROVALS,
  ]);

  const sort = useCallback(
    (approvals: DataTransferApproval) =>
      [...approvals].sort((a, b) => {
        const institutionA = a[camelCase(approvalInstitution)];
        const institutionB = b[camelCase(approvalInstitution)];
        return (institutionA ? institutionA.name.toLowerCase() : '') >
          (institutionB ? institutionB.name.toLowerCase() : '')
          ? 1
          : -1;
      }),
    [approvalInstitution],
  );

  const statuses = useApprovalStatus();

  const makeButton = useCallback(
    (approval: DataTransferApproval) => {
      const canTakeAction =
        approval.canApprove && approval.status === waitingStatus;

      return (
        <>
          {canTakeAction && (
            <Tooltip
              title={
                disableButtons && isDataProviderApproval(approval)
                  ? t(
                      'dataTransferApprovals:dataProviderApprovalsButtonTooltip.text',
                    )
                  : ''
              }
            >
              <ButtonGroup
                sx={{
                  maxHeight: '40px',
                  marginTop: '7px',
                }}
                variant="outlined"
                aria-label="outlined primary button group"
              >
                <Button
                  disabled={disableButtons}
                  onClick={() => {
                    setApprovalState({
                      approval,
                      approvalInstitution,
                      action: ApprovalAction.REJECT,
                    });
                  }}
                >
                  {t('actionButtons.reject')}
                </Button>
                <Button
                  disabled={disableButtons}
                  onClick={() =>
                    setApprovalState({
                      approval,
                      approvalInstitution,
                      action: ApprovalAction.ACCEPT,
                    })
                  }
                >
                  {t('actionButtons.approve')}
                </Button>
              </ButtonGroup>
            </Tooltip>
          )}
          {!canTakeAction && (
            <ColoredStatus value={approval.status} statuses={statuses} />
          )}
        </>
      );
    },
    [
      approvalInstitution,
      setApprovalState,
      statuses,
      t,
      waitingStatus,
      disableButtons,
    ],
  );

  return (
    <Stack direction="column" spacing={1}>
      <Typography variant="subtitle1">
        {t(`${I18nNamespace.COMMON}:models.${camelCase(approvalInstitution)}`)}{' '}
        {t(`${I18nNamespace.COMMON}:models.approval_plural`)}
      </Typography>
      {sort(approvals).map((approval) => {
        return (
          <Grid container key={approval.id}>
            <Grid size={{ xs: 2 }}>
              <Typography marginTop={0.75}>
                {getTitle(approval, approvalInstitution)}
              </Typography>
            </Grid>
            <Grid
              size={{ xs: 4 }}
              sx={{ display: 'flex', justifyContent: 'flex-end' }}
            >
              {makeButton(approval)}
            </Grid>
            <Grid size={{ xs: 2 }}>
              {approval.status !== 'W' && (
                <Typography color="darkgray" marginLeft={6} marginTop={0.75}>
                  {formatDate(approval.changeDate, true)}
                </Typography>
              )}
            </Grid>
            <Grid size={{ xs: 4 }}>
              <Typography color="darkgray" marginLeft={6} marginTop={0.75}>
                {approval.rejectionReason}
              </Typography>
            </Grid>
          </Grid>
        );
      })}
    </Stack>
  );
};
