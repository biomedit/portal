import {
  ADD_DATA_TRANSFER,
  CALL,
  LOAD_DATA_PROVIDERS,
  LOAD_PGP_KEY_INFOS,
  LOAD_USERS,
  RETRIEVE_PROJECT,
  UPDATE_DATA_TRANSFER,
} from '../../actions/actionTypes';
import {
  AutocompleteField,
  type BaseReducerState,
  CancelLabel,
  ConfirmLabel,
  Dialog,
  FixedChildrenHeight,
  FormDialog,
  HiddenField,
  httpsUrl,
  LabelledField,
  maxLength,
  requestAction,
  SafeLink,
  SelectField,
  useEnhancedForm,
  useEnumChoices,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { CircularProgress, Typography } from '@mui/material';
import {
  type DataProvider,
  type DataTransfer,
  type DataTransferPermissionsEdit,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
  ListUserLitesRoleEnum,
  type PgpKeyInfo,
  PgpKeyInfoStatusEnum,
  type Project,
  ProjectUsersInnerRolesEnum,
  type User,
  type Userinfo,
} from '../../api/generated';
import {
  MultipleUsersTextField,
  useMultipleUsersTextField,
} from '../UserTextField';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
} from 'react';
import { useDataProviderChoices, useUserChoices } from '../choice';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { isStaff } from '../selectors';
import type { Required } from 'utility-types';
import type { RootState } from '../../store';
import { styled } from '@mui/material/styles';
import { TestId } from '../../testId';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

enum MaxPackages {
  ONE_PACKAGE = '1',
  UNLIMITED = '-1',
}

export type UiDataTransfer = Required<Partial<DataTransfer>, 'project'>;

export type DataTransferFormProps = {
  close: () => void;
  transfer: UiDataTransfer;
};

const StyledTypography = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(1),
}));

export function useDataTransferForm(projectId: number | undefined = undefined) {
  const newDtr =
    projectId !== undefined
      ? {
          project: +projectId,
        }
      : undefined;
  const { closeFormDialog, data, ...rest } =
    useFormDialog<UiDataTransfer>(newDtr);
  const dtrForm = data && (
    <DataTransferForm transfer={data} close={closeFormDialog} />
  );
  return {
    dtrForm,
    ...rest,
  };
}

export const DataTransferForm = ({
  transfer,
  close,
}: DataTransferFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  const requestor = 'requestor';
  const purpose = 'purpose';

  const isEdit = !!transfer.id;
  const dispatch = useReduxDispatch();

  const { isFetching: isFetchingDataProvider, itemList: dataProviders } =
    useSelector<RootState, BaseReducerState<IdRequired<DataProvider>>>(
      (state) => state.dataProvider,
    );
  const { isFetching: isFetchingUsers, itemList: users } = useSelector<
    RootState,
    BaseReducerState<IdRequired<User>>
  >((state) => state.users);
  const { isFetching: isFetchingKeys, itemList: keys } = useSelector<
    RootState,
    BaseReducerState<IdRequired<PgpKeyInfo>>
  >((state) => state.pgpKeyInfo);
  const currentUser = useSelector<RootState, Userinfo | null>(
    (state) => state.auth.user,
  );
  const projects = useSelector((state: RootState) => state.project.itemList);
  const isSubmitting = useSelector(
    (state: RootState) => state.dataTransfers.isSubmitting,
  );

  const staff = useSelector(isStaff);
  const dataSpecificationApprover = useSelector(
    (state: RootState) =>
      state.auth.user?.permissions?.dataSpecificationApprover,
  );
  const isDisabled = useCallback(
    (fieldName: keyof DataTransferPermissionsEdit): boolean =>
      isEdit && !transfer.permissions?.edit?.[fieldName],
    [isEdit, transfer.permissions?.edit],
  );
  const userChoices = useUserChoices(users);
  const dataProviderChoices = useDataProviderChoices(
    dataProviders.filter(
      (dp) =>
        (dp.coordinators?.length && dp.enabled) ||
        dp.code === transfer.dataProvider,
    ),
  );
  const dataTransferPurposeChoices = useEnumChoices(DataTransferPurposeEnum);
  const dataTransferStatusChoices = useEnumChoices(DataTransferStatusEnum);
  const maxPackagesChoices = useEnumChoices(MaxPackages).map((choice) => ({
    ...choice,
    label: t('maxPackages', { context: choice.value }),
  }));
  const form = useEnhancedForm<DataTransfer>({ defaultValues: transfer });

  const { watch, setError } = form;

  function submit(dataTransfer: DataTransfer) {
    // No submission if none of requestor's keys is approved.
    if (staff && !keys.length) {
      setError(requestor, { message: t(`noKeys.${requestor}`) });
      return;
    }
    const onSuccessAction = { type: CALL, callback: close };
    dataTransfer.dataRecipients = dataRecipients;
    if (isEdit) {
      close();
      dispatch(
        requestAction(UPDATE_DATA_TRANSFER, {
          id: String(dataTransfer.id),
          // @ts-expect-error `partialUpdateDataTransfer` expects a full data
          // transfer object, but it really shouldn't. See
          // https://gitlab.com/biomedit/portal/-/issues/1000
          dataTransfer: staff
            ? dataTransfer
            : { dataSpecification: dataTransfer.dataSpecification },
        }),
      );
    } else {
      dispatch(
        requestAction(
          ADD_DATA_TRANSFER,
          {
            dataTransfer,
          },
          onSuccessAction,
        ),
      );
    }
  }

  // This is only relevant for `staff`
  const watchRequestor = watch(requestor);

  useEffect(() => {
    // Load all DMs of this project for staff, otherwise only take current user
    // (which is most probably a DM - the requestor)
    if (staff) {
      dispatch(
        requestAction(LOAD_USERS, {
          projectId: transfer.project,
          role: ListUserLitesRoleEnum.DM,
        }),
      );
    }
    dispatch(requestAction(LOAD_DATA_PROVIDERS));
    if (isEdit) {
      dispatch(
        requestAction(RETRIEVE_PROJECT, { id: String(transfer.project) }),
      );
    }
  }, [transfer.project, dispatch, staff, isEdit]);

  useEffect(() => {
    const requestorUser =
      (staff &&
        watchRequestor &&
        users.find((user) => user.id === watchRequestor)) ||
      currentUser;
    if (requestorUser) {
      dispatch(
        requestAction(LOAD_PGP_KEY_INFOS, {
          status: PgpKeyInfoStatusEnum.APPROVED,
          username: requestorUser.username,
        }),
      );
    }
  }, [dispatch, watchRequestor, users, staff, currentUser]);

  const project: Project | undefined = useMemo(
    () => projects.find((project: Project) => project.id === transfer.project),
    [projects, transfer],
  );

  const watchPurpose = watch(purpose);
  const legalBasisRequired = useMemo(
    () =>
      watchPurpose === DataTransferPurposeEnum.PRODUCTION &&
      !!project?.legalApprovalGroup,
    [watchPurpose, project],
  );
  const dataSpecificationRequired = useMemo(
    () =>
      watchPurpose === DataTransferPurposeEnum.PRODUCTION &&
      !!project?.dataSpecificationApprovalGroup,
    [watchPurpose, project],
  );

  // We do NOT need to filter as this method is only called when the user is a DM
  const checkForKeys = useCallback(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS));
  }, [dispatch]);

  const {
    users: dataRecipients,
    addUser,
    deleteUser,
  } = useMultipleUsersTextField({
    initialUsers: transfer.dataRecipients ?? [],
  });

  const isDataManager = (user: User) =>
    !!project &&
    (project.users ?? [])
      .filter((projectUser) =>
        projectUser.roles.includes(ProjectUsersInnerRolesEnum.DM),
      )
      .some((dataManager) => dataManager.id === user.id);

  return (
    <>
      {!staff && !dataSpecificationApprover && (
        <Dialog
          open={!isFetchingKeys && !keys.length}
          isSubmitting={false}
          fullWidth={true}
          maxWidth={'lg'}
          title={t('noKeys.title')}
          cancelLabel={CancelLabel.CANCEL}
          confirmLabel={t('noKeys.button')}
          confirmButtonProps={{
            onClick: checkForKeys,
            type: 'button',
          }}
          onClose={close}
        >
          <StyledTypography>{t('noKeys.intro')}</StyledTypography>
          <StyledTypography>
            {t('noKeys.upload.helper')}
            <SafeLink href={t('noKeys.upload.link')}>
              {t('noKeys.upload.text')}
            </SafeLink>
          </StyledTypography>
          <StyledTypography>
            {t('noKeys.create.helper')}
            <SafeLink href={t('noKeys.create.link')}>
              {t('noKeys.create.text')}
            </SafeLink>
          </StyledTypography>
        </Dialog>
      )}
      <FormProvider {...form}>
        <FormDialog
          title={
            isEdit ? `Edit Data Transfer ${transfer?.id}` : 'New Data Transfer'
          }
          open={
            !!transfer &&
            (staff ||
              dataSpecificationApprover ||
              (!isFetchingKeys && !!keys.length))
          }
          isSubmitting={isSubmitting}
          onSubmit={submit}
          onClose={close}
          confirmLabel={ConfirmLabel.SAVE}
        >
          <HiddenField
            name="project"
            initialValues={{ project: transfer.project }}
          />
          <HiddenField name="id" initialValues={{ id: transfer.id }} />
          <FixedChildrenHeight sx={{ marginTop: 3 }}>
            <SelectField
              name="maxPackages"
              label="Max Packages"
              initialValues={transfer}
              required
              choices={maxPackagesChoices}
              fullWidth={true}
              disabled={isDisabled('maxPackages')}
            />
            <SelectField
              name="dataProvider"
              label="Data Provider"
              initialValues={transfer}
              required
              isLoading={isFetchingDataProvider}
              choices={dataProviderChoices}
              fullWidth={true}
              helperText={t('helper.dataProvider')}
              populateIfSingleChoice
              disabled={isDisabled('dataProvider')}
            />
            <SelectField
              name={purpose}
              label="Purpose"
              initialValues={transfer}
              required
              choices={dataTransferPurposeChoices}
              disabled={isDisabled('purpose')}
              fullWidth={true}
            />
            {isEdit && (
              <SelectField
                name="status"
                label="Status"
                initialValues={transfer}
                required
                choices={dataTransferStatusChoices}
                fullWidth={true}
                disabled={isDisabled('status')}
              />
            )}
            {staff &&
              (isFetchingUsers ? (
                <CircularProgress />
              ) : (
                // Requestor field only makes sense for staff
                <AutocompleteField
                  name={requestor}
                  label="Requestor"
                  initialValues={transfer}
                  required
                  testId={TestId.REQUESTOR}
                  choices={userChoices}
                  isLoading={isFetchingUsers}
                  width="100%"
                  disabled={isDisabled('requestor')}
                  helperText={t('helper.requestor')}
                />
              ))}
            <MultipleUsersTextField
              addUser={addUser}
              deleteUser={deleteUser}
              validateUser={isDataManager}
              legend={t('dataRecipients')}
              name={'dataRecipients'}
              users={dataRecipients}
              disabled={isDisabled('dataRecipients')}
            />
            <LabelledField
              name="legalBasis"
              type="text"
              label={t('legalBasis')}
              initialValues={transfer}
              // Do NOT use `validations={legalBasisRequired ? required : undefined}`
              // as it does NOT work
              required={legalBasisRequired}
              disabled={
                watchPurpose === DataTransferPurposeEnum.TEST ||
                isDisabled('legalBasis')
              }
              validations={maxLength(128)}
              fullWidth
              helperText={t('legalBasisHelp')}
            />
            <LabelledField
              sx={{ mt: 1 }}
              name="dataSpecification"
              type="url"
              label={t('dataSpecification')}
              initialValues={transfer}
              // Do NOT use `validations={dataSpecificationRequired ? required
              // : undefined}` as it does NOT work
              required={dataSpecificationRequired}
              disabled={
                watchPurpose === DataTransferPurposeEnum.TEST ||
                isDisabled('dataSpecification')
              }
              validations={[httpsUrl]}
              helperText={t('dataSpecificationHelp')}
              fullWidth
            />
          </FixedChildrenHeight>
        </FormDialog>
      </FormProvider>
    </>
  );
};
