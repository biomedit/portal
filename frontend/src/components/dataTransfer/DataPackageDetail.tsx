import { CircularProgress, Typography } from '@mui/material';
import {
  DataPackageTraceInnerStatusEnum,
  type DataTransferPackagesInner,
} from '../../api/generated';
import {
  Description,
  Field,
  formatDate,
  formatItemFields,
  type FormattedItemField,
  type ListModel,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export type DataPackageDetailProps = {
  dataPackage: DataTransferPackagesInner | undefined;
};

export const DataPackageDetail = ({
  dataPackage,
}: DataPackageDetailProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.DATA_TRANSFER,
    I18nNamespace.LIST,
  ]);

  const { isFetching } = useSelector((state: RootState) => state.dataTransfers);

  const getTraceDatetime = (
    pkg: DataTransferPackagesInner,
    event: DataPackageTraceInnerStatusEnum,
  ) =>
    formatDate(
      pkg.trace?.find((trace) => trace.status === event)?.timestamp,
      true,
      true,
    );

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<DataTransferPackagesInner> = {
      fields: [
        Field({
          caption: t(`${I18nNamespace.DATA_TRANSFER}:columns.packageName`),
          getProperty: (pkg: DataTransferPackagesInner) => pkg.fileName,
          key: 'packageName',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_TRANSFER}:columns.sender`),
          getProperty: (pkg: DataTransferPackagesInner) =>
            pkg.senderOpenPgpKeyInfo,
          key: 'sender',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_TRANSFER}:columns.recipients`),
          getProperty: (pkg: DataTransferPackagesInner) =>
            pkg.recipientsOpenPgpKeyInfo,
          key: 'recipients',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_TRANSFER}:columns.timeSent`),
          getProperty: (pkg: DataTransferPackagesInner) =>
            getTraceDatetime(pkg, DataPackageTraceInnerStatusEnum.ENTER),
          key: 'sent',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_TRANSFER}:columns.timeArrived`),
          getProperty: (pkg: DataTransferPackagesInner) =>
            getTraceDatetime(pkg, DataPackageTraceInnerStatusEnum.PROCESSED),
          key: 'arrived',
        }),
        Field({
          caption: t(`${I18nNamespace.DATA_TRANSFER}:columns.timeDeleted`),
          getProperty: (pkg: DataTransferPackagesInner) =>
            getTraceDatetime(pkg, DataPackageTraceInnerStatusEnum.DELETED),
          key: 'deleted',
        }),
      ],
    };
    return dataPackage
      ? formatItemFields<DataTransferPackagesInner>(model, dataPackage)
      : null;
  }, [dataPackage, t]);

  if (isFetching) {
    return <CircularProgress />;
  }

  return dataPackage ? (
    <Description entries={detailEntries} labelWidth={''} />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.dataPackage`),
      })}
    </Typography>
  );
};
