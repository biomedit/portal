import {
  type Approval,
  ApprovalInstitutionEnum,
  ApprovalStatusEnum,
  type DataTransfer,
  DataTransferGroupApprovalsInnerGroupProfileRoleEnum,
  DataTransferPurposeEnum,
} from '../../api/generated';
import { CALL, UPDATE_APPROVAL } from '../../actions/actionTypes';
import {
  CheckboxField,
  FormDialog,
  HiddenField,
  requestAction,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  isDataProviderApproval,
  isGroupApproval,
  isNodeApproval,
} from '../../guards';
import React, {
  type ReactElement,
  useCallback,
  useMemo,
  useState,
} from 'react';
import { Stack, Typography } from '@mui/material';
import camelCase from 'lodash/camelCase';
import type { DataTransferApproval } from '../../global';
import { FormProvider } from 'react-hook-form';
import { getTitle } from './DataTransferApprovalsStack';
import { I18nNamespace } from '../../i18n';
import type { RootState } from '../../store';
import TextField from '@mui/material/TextField';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

type DataTransferApprovalFormProps = {
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
  dtr: DataTransfer;
  isRejectionForm: boolean;
  onClose: () => void;
  open: boolean;
};

type TitleProps = {
  approval: DataTransferApproval;
  approvalInstitution: ApprovalInstitutionEnum;
  dtr: DataTransfer;
};

export const Title = ({
  dtr,
  approval,
  approvalInstitution,
}: TitleProps): ReactElement => {
  return (
    <>
      <Typography variant="h5" display={'inline'}>
        {`${dtr.id} - ${dtr.projectName}`}
      </Typography>
      <Typography display={'inline'} pl={5}>
        {getTitle(approval, approvalInstitution)}
      </Typography>
    </>
  );
};

export const DataTransferApprovalForm = ({
  dtr,
  approval,
  approvalInstitution,
  onClose,
  open,
  isRejectionForm,
}: DataTransferApprovalFormProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.DATA_TRANSFER_APPROVALS,
    I18nNamespace.DATA_TRANSFER,
  ]);
  const dispatch = useReduxDispatch();
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.dataTransfers.isSubmitting,
  );
  const form = useEnhancedForm<Approval>();
  const [rejectionReason, setRejectionReason] = useState<string>();

  const submit = useCallback(
    (updatedApproval: Approval) => {
      const onSuccessAction = {
        type: CALL,
        callback: onClose,
      };
      dispatch(
        requestAction(
          UPDATE_APPROVAL,
          {
            id: String(updatedApproval.id),
            approval: updatedApproval,
          },
          onSuccessAction,
        ),
      );
    },
    [onClose, dispatch],
  );

  const reject = useCallback(
    (approval: DataTransferApproval) => {
      const onSuccessAction = {
        type: CALL,
        callback: onClose,
      };
      dispatch(
        requestAction(
          UPDATE_APPROVAL,
          {
            id: approval?.id,
            approval: {
              institution: approvalInstitution,
              status: ApprovalStatusEnum.R,
              rejectionReason: rejectionReason,
            },
          },
          onSuccessAction,
        ),
      );
    },
    [dispatch, approvalInstitution, onClose, rejectionReason],
  );

  const checkboxes = useMemo(() => {
    const names: string[] = [];

    const isDataSpecificationApproval =
      isGroupApproval(approval) &&
      approval.group?.profile?.role ==
        DataTransferGroupApprovalsInnerGroupProfileRoleEnum.FAIR;

    if (!isGroupApproval(approval)) {
      names.push('technicalMeasuresInPlace');
    }

    if (
      dtr.purpose === DataTransferPurposeEnum.PRODUCTION &&
      !isDataSpecificationApproval
    ) {
      names.push('existingLegalBasis');
    }

    if (isNodeApproval(approval)) {
      names.push('projectSpaceReady');
    }

    if (
      dtr.purpose === DataTransferPurposeEnum.PRODUCTION &&
      isDataProviderApproval(approval)
    ) {
      names.push('dataDeliveryApproved');
    }

    return names;
  }, [dtr.purpose, approval]);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'md'}
        title={
          <Title
            dtr={dtr}
            approval={approval}
            approvalInstitution={approvalInstitution}
          />
        }
        onSubmit={!isRejectionForm ? submit : () => reject(approval)}
        onClose={() => {
          onClose();
          form.reset();
        }}
        open={open}
        confirmLabel={
          (!isRejectionForm
            ? t(`${I18nNamespace.DATA_TRANSFER}:actionButtons.approve`)
            : t(
                `${I18nNamespace.DATA_TRANSFER}:actionButtons.reject`,
              )) as string
        }
        isSubmitting={isSubmitting}
      >
        {!isRejectionForm && (
          <>
            <HiddenField name="id" initialValues={{ id: approval.id }} />
            <HiddenField
              name="status"
              initialValues={{ status: ApprovalStatusEnum.A }}
            />
            <HiddenField
              name="institution"
              initialValues={{
                institution: approvalInstitution,
              }}
            />
            {checkboxes.length === 0 ? (
              <Typography variant="body1">
                {t(
                  `${I18nNamespace.DATA_TRANSFER_APPROVALS}:declarations.empty`,
                )}
              </Typography>
            ) : (
              <>
                <Typography variant="h5">
                  {t(
                    `${I18nNamespace.DATA_TRANSFER_APPROVALS}:declarations.title`,
                  )}
                </Typography>
                <Stack>
                  {checkboxes.map((name) => {
                    return (
                      <CheckboxField
                        key={name}
                        name={name}
                        label={t(
                          `${
                            I18nNamespace.DATA_TRANSFER_APPROVALS
                          }:declarations.${camelCase(
                            approvalInstitution,
                          )}.${name}`,
                        )}
                        required
                      />
                    );
                  })}
                </Stack>
              </>
            )}
          </>
        )}

        {isRejectionForm && (
          <>
            <Typography variant="h5">
              {t(
                `${I18nNamespace.DATA_TRANSFER_APPROVALS}:rejectDataTransfer.title`,
              )}
            </Typography>
            <br />
            <Typography>
              {t(
                `${I18nNamespace.DATA_TRANSFER_APPROVALS}:rejectDataTransfer.text`,
              )}
            </Typography>
            <TextField
              autoFocus={true}
              onChange={(event) => {
                setRejectionReason(event.target.value);
              }}
              required
              size="small"
              variant="outlined"
              fullWidth
              slotProps={{
                htmlInput: { maxLength: 512 },
              }}
            />
          </>
        )}
      </FormDialog>
    </FormProvider>
  );
};
