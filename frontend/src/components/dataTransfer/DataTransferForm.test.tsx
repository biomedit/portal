import {
  createDataTransfer,
  createDataTransferPermissions,
  createDataTransferPermissionsEdit,
  createProject,
  createProjectUser,
  createStaffAuthState,
  createUser,
} from '../../../factories';
import {
  DataTransferPurposeEnum,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import { getInitialState, makeStore } from '../../store';
import { render, screen } from '@testing-library/react';
import { DataTransferForm } from './DataTransferForm';
import { mockConsoleWarn } from '@biomedit/next-widgets';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

describe('DataTransferForm', () => {
  it.each`
    purpose                               | hasApprovalGroups | required | disabled
    ${DataTransferPurposeEnum.PRODUCTION} | ${true}           | ${true}  | ${false}
    ${DataTransferPurposeEnum.PRODUCTION} | ${false}          | ${false} | ${false}
    ${DataTransferPurposeEnum.TEST}       | ${true}           | ${false} | ${true}
  `(
    `Should require ($required) and disable ($disabled) the data specification and legal basis fields if:
    1) The purpose is $purpose;
    2) The project has ($hasApprovalGroups) approval groups.`,
    async ({ purpose, hasApprovalGroups, required, disabled }) => {
      // Using the same test for both fields because they are supposed to behave
      // the same.

      const project = createProject({
        dataSpecificationApprovalGroup: hasApprovalGroups ? 1 : undefined,
        legalApprovalGroup: hasApprovalGroups ? 1 : undefined,
      });
      const dataTransfer = createDataTransfer({
        purpose,
        project: project.id,
        permissions: createDataTransferPermissions({
          edit: createDataTransferPermissionsEdit({
            dataSpecification: true,
            legalBasis: true,
          }),
        }),
      });
      mockSuccessfulApiCall('retrieveProject', project);
      mockConsoleWarn();

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createStaffAuthState(),
          })}
        >
          <DataTransferForm transfer={dataTransfer} close={() => null} />
        </Provider>,
      );

      const dataSpecificationField = await screen.findByLabelText(
        `dataSpecification${required ? ' *' : ''}`,
      );
      const legalBasisField = await screen.findByLabelText(
        `legalBasis${required ? ' *' : ''}`,
      );

      for (const field of [dataSpecificationField, legalBasisField]) {
        if (disabled) {
          expect(field).toBeDisabled();
        } else {
          expect(field).not.toBeDisabled();
        }

        if (required) {
          expect(field).toBeRequired();
        } else {
          expect(field).not.toBeRequired();
        }
      }
    },
  );

  describe('dataRecipients', () => {
    const dataManager = createUser();

    it.each`
      targetUser      | userType          | invalid
      ${dataManager}  | ${'data manager'} | ${false}
      ${createUser()} | ${'regular user'} | ${true}
    `(
      `Should show invalidUser ($invalid) when the target user is a $userType`,
      async ({ targetUser, invalid }) => {
        const user = userEvent.setup();
        mockSuccessfulApiCall('listUserLites', [targetUser]);

        const project = createProject({
          users: [
            createProjectUser({
              ...dataManager,
              roles: [ProjectUsersInnerRolesEnum.DM],
            }),
          ],
        });
        const dataTransfer = createDataTransfer({
          project: project.id,
          permissions: createDataTransferPermissions({
            edit: createDataTransferPermissionsEdit({
              dataRecipients: true,
            }),
          }),
        });
        mockSuccessfulApiCall('retrieveProject', project);
        mockConsoleWarn();

        render(
          <Provider
            store={makeStore(undefined, {
              ...getInitialState(),
              auth: createStaffAuthState(),
            })}
          >
            <DataTransferForm transfer={dataTransfer} close={() => null} />
          </Provider>,
        );

        await user.click(screen.getByPlaceholderText('Enter email address'));
        await user.keyboard(targetUser.email);
        await user.click(
          await screen.findByLabelText('Add common:models.user'),
        );

        if (invalid) {
          await screen.findByText('invalidUser');
        } else {
          expect(screen.queryByText('invalidUser')).not.toBeInTheDocument();
        }
      },
    );
  });
});
