import {
  ColoredStatus,
  formatDate,
  type Status,
  useLocaleSortingFn,
} from '@biomedit/next-widgets';
import { createColumnHelper, type FilterFn } from '@tanstack/react-table';
import {
  type DataProvider,
  type DataTransferLite,
  DataTransferStatusEnum,
  type Project,
} from '../../api/generated';
import { createSelector } from 'reselect';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { RootState } from '../../store';
import { useMemo } from 'react';
import { useParamSelector } from '../selectors';
import { useTranslation } from 'next-i18next';

/**
 * '-1' (standing for 'unlimited'/'infinity') gets converted into 'Number.MAX_SAFE_INTEGER'
 * for sorting purposes.
 * We cannot use 'infinity'. Otherwise, addition (in sorting) will not work.
 */
export const useApprovalStatus = () => {
  const { t } = useTranslation([I18nNamespace.DATA_TRANSFER]);

  return useMemo<Status<string>[]>(
    () => [
      {
        color: 'info',
        text: t(`actionButtons.W`).toUpperCase(),
        value: 'W',
      },
      {
        color: 'success',
        text: t(`actionButtons.A`).toUpperCase(),
        value: 'A',
      },
      {
        color: 'error',
        text: t(`actionButtons.R`).toUpperCase(),
        value: 'R',
      },
      {
        text: t(`actionButtons.B`).toUpperCase(),
        value: 'B',
      },
    ],
    [t],
  );
};

export const useDataTransferStatus = () =>
  useMemo<Status<DataTransferStatusEnum>[]>(
    () => [
      {
        color: 'info',
        value: DataTransferStatusEnum.INITIAL,
      },
      {
        color: 'success',
        value: DataTransferStatusEnum.AUTHORIZED,
      },
      {
        value: DataTransferStatusEnum.EXPIRED,
      },
      {
        color: 'error',
        value: DataTransferStatusEnum.UNAUTHORIZED,
      },
    ],
    [],
  );

export const useDataTransferColumns = (withProjectName: boolean) => {
  const columnHelper = createColumnHelper<IdRequired<DataTransferLite>>();
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  const statuses = useDataTransferStatus();
  const localeSortingFn = useLocaleSortingFn<IdRequired<DataTransferLite>>();

  return useMemo(() => {
    const columns = [
      columnHelper.accessor('id', {
        header: t('columns.transferId'),
      }),
      columnHelper.accessor('dataProviderName', {
        header: t('columns.dataProvider'),
        sortingFn: 'alphanumeric',
      }),
      columnHelper.accessor('requestorFullName', {
        header: t('columns.requestor'),
        sortingFn: localeSortingFn,
      }),
      columnHelper.accessor('purpose', {
        header: t('columns.purpose'),
      }),
      columnHelper.accessor('changeDate', {
        header: t('columns.changeDate'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.getValue(), true) ?? '',
      }),
      columnHelper.accessor('status', {
        header: t('columns.status'),
        cell: (props) =>
          ColoredStatus({
            value: props.getValue(),
            statuses: statuses,
          }),
      }),
    ];

    const projectNameColumns = withProjectName
      ? [
          columnHelper.accessor('projectName', {
            header: t('columns.projectName'),
            sortingFn: 'alphanumeric',
          }),
        ]
      : [];

    return [...projectNameColumns, ...columns];
  }, [t, withProjectName, columnHelper, statuses, localeSortingFn]);
};
export const useGlobalDataTransferFilter =
  (): FilterFn<IdRequired<DataTransferLite>> =>
  (row, columnId, filterValue) => {
    const filterOnColumns = () => {
      switch (columnId) {
        case 'id':
          return row.getValue(columnId) == filterValue;
        default:
          return String(row.getValue(columnId))
            .toLowerCase()
            .includes(String(filterValue).toLowerCase());
      }
    };

    const filterOnDataPackage = () =>
      row.original.packages?.some((dataPackage) =>
        String(dataPackage.fileName)
          .toLowerCase()
          .includes(String(filterValue).toLowerCase()),
      ) || false;

    return filterOnColumns() || filterOnDataPackage();
  };

const getDataTransfers = (state: RootState) => state.dataTransfers.itemList;
const getDtrIdParam = (_: RootState, dtrId: number): number => dtrId;
const getProjectParam = (_: RootState, prj?: Project): Project | undefined =>
  prj;

const findDataTransferById = createSelector(
  getDataTransfers,
  getDtrIdParam,
  (dataTransfers, dtrId: number) =>
    dataTransfers.find((dataTransfer) => dataTransfer.id === dtrId),
);
const filterDataTransfersByProject = createSelector(
  getDataTransfers,
  getProjectParam,
  (dataTransfers, prj) =>
    dataTransfers.filter((dtr) => prj && dtr.projectName === prj.name),
);

export const useDataTransfer = (dtrId: number) => {
  return useParamSelector(findDataTransferById, dtrId);
};

export const useProjectDataTransfers = (project?: Project) => {
  return useParamSelector(filterDataTransfersByProject, project);
};

const getDataProviderParam = (
  _: RootState,
  dp?: DataProvider,
): DataProvider | undefined => dp;

const filterDataTransfersByDP = createSelector(
  getDataTransfers,
  getDataProviderParam,
  (dataTransfers, dp?: DataProvider) =>
    dataTransfers.filter((dtr) => dp && dtr.dataProviderName === dp.name),
);

export const useDPDataTransfers = (dataProvider?: DataProvider) => {
  return useParamSelector(filterDataTransfersByDP, dataProvider);
};
