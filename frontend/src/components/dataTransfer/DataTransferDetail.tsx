import { type CellContext, createColumnHelper } from '@tanstack/react-table';
import { CircularProgress, styled, Typography } from '@mui/material';
import {
  ColoredStatus,
  Description,
  EnhancedTable,
  Field,
  formatDate,
  formatItemFields,
  type FormattedItemField,
  type ListModel,
  SafeLink,
  type TabItem,
  TabPane,
} from '@biomedit/next-widgets';
import {
  type DataPackage,
  DataPackageTraceInnerStatusEnum,
  type DataTransfer,
  type DataTransferPackagesInner,
  DataTransferPurposeEnum,
} from '../../api/generated';
import React, { type ReactElement, useMemo } from 'react';
import { DataTransferApprovals } from './DataTransferApprovals';
import type { ExpectedDataTransfer } from '../../reducers/dataTransfer';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { RequestorField } from './RequestorField';
import type { RootState } from '../../store';
import { useDataTransferStatus } from './DataTransferHooks';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import { useUserRoleEntries } from '../../widgets/UserRoleEntries';

const StyledColoredStatus = styled(ColoredStatus)(({ theme }) => ({
  paddingLeft: theme.spacing(5),
  display: 'inline',
}));

export type DataTransferDetailProps = {
  dataTransfer?: ExpectedDataTransfer & {
    packages: IdRequired<DataTransferPackagesInner>[];
  };
};

export const DataTransferDetail = ({
  dataTransfer,
}: DataTransferDetailProps): ReactElement => {
  const router = useRouter();
  const { t } = useTranslation(I18nNamespace.DATA_TRANSFER);
  const { t: emptyMessageTFunction } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.LIST,
  ]);

  const statuses = useDataTransferStatus();

  const { isFetching } = useSelector((state: RootState) => state.dataTransfers);

  const roles = useMemo(() => {
    return {
      dataRecipients: dataTransfer?.dataRecipients ?? [],
    };
  }, [dataTransfer]);

  const userRoleEntries = useUserRoleEntries(
    roles,
    I18nNamespace.DATA_TRANSFER,
  );

  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<DataTransfer> = {
      fields: [
        Field({
          caption: t('columns.projectName'),
          getProperty: (dtr: DataTransfer) => dtr.projectName,
          key: 'projectName',
        }),
        Field({
          caption: t('columns.transferId'),
          getProperty: (dtr: DataTransfer) => dtr.id,
          key: 'transferId',
        }),
        Field({
          caption: t('columns.dataProvider'),
          getProperty: (dtr: DataTransfer) => dtr.dataProviderName,
          key: 'dataProviderName',
        }),
        Field({
          caption: t('columns.status'),
          getProperty: (dtr: DataTransfer) => (
            <StyledColoredStatus value={dtr.status} statuses={statuses} />
          ),
          key: 'status',
        }),
        Field({
          caption: t('columns.maxPackages'),
          getProperty: (dtr: DataTransfer) =>
            t('maxPackages', { context: String(dtr.maxPackages) }),
          key: 'maxPackages',
        }),
        Field({
          caption: t('columns.transferredPackages'),
          getProperty: (dtr: DataTransfer) => dtr.packages?.length,
          key: 'packageCount',
        }),
        Field({
          caption: t('columns.requestor'),
          getProperty: (dtr: DataTransfer) => dtr,
          render: RequestorField,
          key: 'requestor',
        }),
        Field({
          caption: t('columns.dataSpecification'),
          getProperty: (dtr: DataTransfer) => dtr.dataSpecification,
          key: 'dataSpecification',
          render: (dataSpecification) =>
            dataSpecification ? (
              <SafeLink href={dataSpecification}>{dataSpecification}</SafeLink>
            ) : (
              t('common:noValuePlaceholder')
            ),
          hideIf: (dtr: DataTransfer) =>
            dtr.purpose === DataTransferPurposeEnum.TEST,
        }),
        Field({
          caption: t('columns.purpose'),
          getProperty: (dtr: DataTransfer) => dtr.purpose,
          key: 'purpose',
        }),
        Field({
          caption: t('columns.legalBasis'),
          getProperty: (dtr: DataTransfer) => dtr.legalBasis,
          key: 'legalBasis',
          render: (legalBasis) => legalBasis || t('common:noValuePlaceholder'),
          hideIf: (dtr: DataTransfer) =>
            dtr.purpose === DataTransferPurposeEnum.TEST,
        }),
      ],
    };
    return dataTransfer
      ? formatItemFields<DataTransfer>(model, dataTransfer)
      : null;
  }, [t, dataTransfer, statuses]);

  const columnHelper = createColumnHelper<IdRequired<DataPackage>>();

  const columns = useMemo(() => {
    const accessorByStatus = (status: string) => (pkg: DataPackage) =>
      pkg.trace?.find((item) => item.status === status)?.timestamp;
    const cell = (
      props: CellContext<IdRequired<DataPackage>, Date | undefined>,
    ) => formatDate(props.getValue(), true);

    return [
      columnHelper.accessor('fileName', {
        header: t('columns.packageName'),
      }),
      columnHelper.accessor('senderOpenPgpKeyInfo', {
        header: t('columns.sender'),
      }),
      ...[
        [DataPackageTraceInnerStatusEnum.ENTER, 'timeSent'],
        [DataPackageTraceInnerStatusEnum.PROCESSED, 'timeArrived'],
        [DataPackageTraceInnerStatusEnum.DELETED, 'timeDeleted'],
      ].map(([status, translation]) =>
        columnHelper.accessor(accessorByStatus(status), {
          header: t(`columns.${translation}`),
          sortingFn: 'datetime',
          cell,
        }),
      ),
    ];
  }, [t, columnHelper]);

  const tabModel: TabItem[] = useMemo(() => {
    const tabItems = [
      {
        id: 'details',
        label: 'Details',
        content: (
          <>
            <Description entries={detailEntries} labelWidth={300} />
            <Description entries={userRoleEntries} labelWidth={300} />
          </>
        ),
      },
      {
        id: 'log',
        label: 'Log',
        content: (
          <EnhancedTable
            itemList={dataTransfer?.packages ?? []}
            columns={columns}
            emptyMessage={
              "This data transfer doesn't have any data packages yet."
            }
            canAdd={false}
            canDelete={false}
            canEdit={false}
            isFetching={false}
            isSubmitting={false}
            onRowClick={(item: DataPackage) =>
              router.push(
                `/data-transfers/${dataTransfer?.id}/data-packages/${item.id}`,
              )
            }
          />
        ),
      },
    ];

    tabItems.push({
      id: 'approvals',
      label: t(`${I18nNamespace.COMMON}:models.approval_plural`),
      content:
        dataTransfer?.nodeApprovals?.length ||
        dataTransfer?.dataProviderApprovals?.length ? (
          <DataTransferApprovals dtr={dataTransfer} />
        ) : (
          <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
            {emptyMessageTFunction(`${I18nNamespace.LIST}:emptyMessage`, {
              model: emptyMessageTFunction(
                `${I18nNamespace.COMMON}:models.approval_plural`,
              ),
            })}
          </Typography>
        ),
    });

    return tabItems;
  }, [
    detailEntries,
    dataTransfer,
    t,
    userRoleEntries,
    emptyMessageTFunction,
    columns,
    router,
  ]);

  if (isFetching) {
    return <CircularProgress />;
  }

  return dataTransfer ? (
    <TabPane
      id="DataTransferDetail"
      label="Switch between details of the data transfer and the log of data packages."
      model={tabModel}
      panelBoxProps={{ sx: { height: '50vh' } }}
    />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {emptyMessageTFunction(`${I18nNamespace.LIST}:emptyMessage`, {
        model: emptyMessageTFunction(
          `${I18nNamespace.COMMON}:models.dataTransfer`,
        ),
      })}
    </Typography>
  );
};
