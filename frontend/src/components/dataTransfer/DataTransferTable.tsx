import React, { type ReactElement } from 'react';
import {
  useDataTransferColumns,
  useGlobalDataTransferFilter,
} from './DataTransferHooks';
import type { DataTransferLite } from '../../api/generated';
import { EnhancedTable } from '@biomedit/next-widgets';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { RootState } from '../../../src/store';
import { useDataTransferForm } from './DataTransferForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

type DataTransferTableProps = {
  canAdd: boolean;
  dataTransfers: IdRequired<DataTransferLite>[];
  projectId?: number;
};

export const DataTransferTable = ({
  dataTransfers,
  canAdd,
  projectId,
}: DataTransferTableProps): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.DATA_TRANSFER,
  ]);

  const { isFetching, isSubmitting } = useSelector(
    (state: RootState) => state.dataTransfers,
  );

  const { openFormDialog, dtrForm } = useDataTransferForm(projectId);
  const columns = useDataTransferColumns(false);
  const globalFilterFn = useGlobalDataTransferFilter();
  const router = useRouter();

  return (
    <EnhancedTable
      itemList={dataTransfers}
      columns={columns}
      canAdd={canAdd}
      canEdit={false} // it should only be possible to edit data transfers from the detailed view
      canDelete={false} // it should only be possible to delete data transfers from the detailed view
      onAdd={openFormDialog}
      onRowClick={(item) => router.push(`/data-transfers/${item.id}`)}
      form={dtrForm}
      addButtonLabel={t(`${I18nNamespace.COMMON}:models.dataTransfer`)}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
      globalFilterHelperText={
        t(`${I18nNamespace.DATA_TRANSFER}:globalFilterHelperText`) as string
      }
      globalFilterFn={globalFilterFn}
      inline
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
        }) as string
      }
    />
  );
};
