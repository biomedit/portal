import {
  ApprovalAction,
  type ApprovalDialogState,
  DataTransferApprovalsStack,
} from './DataTransferApprovalsStack';
import {
  ApprovalInstitutionEnum,
  type DataTransfer,
  DataTransferDataProviderApprovalsInnerStatusEnum,
  DataTransferGroupApprovalsInnerStatusEnum,
  DataTransferNodeApprovalsInnerStatusEnum,
} from '../../api/generated';
import React, { type ReactElement, useMemo } from 'react';
import { DataTransferApprovalForm } from './DataTransferApprovalForm';
import { Stack } from '@mui/material';
import { useDialogState } from '@biomedit/next-widgets';

type DataTransferApprovalsProps = {
  dtr: DataTransfer;
};

export const DataTransferApprovals = ({
  dtr,
}: DataTransferApprovalsProps): ReactElement => {
  const { item, setItem, open, onClose } =
    useDialogState<ApprovalDialogState>();

  const allNonDPApprovalsApproved = useMemo(() => {
    const nonDPApprovals = [
      ...(dtr.nodeApprovals || []),
      ...(dtr.groupApprovals || []),
    ];
    return nonDPApprovals.every((approval) => approval.status === 'A');
  }, [dtr.groupApprovals, dtr.nodeApprovals]);

  return (
    <>
      {dtr && item && (
        <DataTransferApprovalForm
          dtr={dtr}
          approval={item.approval}
          approvalInstitution={item.approvalInstitution}
          onClose={onClose}
          open={open}
          isRejectionForm={item.action === ApprovalAction.REJECT}
        />
      )}

      <Stack direction="column" spacing={5}>
        {!!dtr.nodeApprovals?.length && (
          <DataTransferApprovalsStack
            approvals={dtr.nodeApprovals}
            approvalInstitution={ApprovalInstitutionEnum.node}
            setApprovalState={setItem}
            waitingStatus={DataTransferNodeApprovalsInnerStatusEnum.W}
          />
        )}
        {!!dtr.dataProviderApprovals?.length && (
          <DataTransferApprovalsStack
            approvals={dtr.dataProviderApprovals}
            approvalInstitution={ApprovalInstitutionEnum.data_provider}
            setApprovalState={setItem}
            waitingStatus={DataTransferDataProviderApprovalsInnerStatusEnum.W}
            disableButtons={!allNonDPApprovalsApproved}
          />
        )}
        {!!dtr.groupApprovals?.length && (
          <DataTransferApprovalsStack
            approvals={dtr.groupApprovals}
            approvalInstitution={ApprovalInstitutionEnum.group}
            setApprovalState={setItem}
            waitingStatus={DataTransferGroupApprovalsInnerStatusEnum.W}
          />
        )}
      </Stack>
    </>
  );
};
