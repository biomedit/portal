import { render, screen } from '@testing-library/react';
import { DataTransferOverviewTable } from './DataTransferOverviewTable';
import { makeStore } from '../../store';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
  }),
}));

describe('DataTransferOverviewTable', () => {
  describe('No data transfers', () => {
    beforeAll(() => {
      mockSuccessfulApiCall('listDataTransferLites', []);
    });

    it('should display empty list', async () => {
      render(
        <Provider store={makeStore()}>
          <DataTransferOverviewTable />
        </Provider>,
      );
      // Empty message (NOT i18n'ed)
      await screen.findByText('list:emptyMessage');
    });
  });
});
