import {
  ApprovalInstitutionEnum,
  DataTransferGroupApprovalsInnerGroupProfileRoleEnum,
  DataTransferPurposeEnum,
  DataTransferStatusEnum,
} from '../../api/generated';
import {
  createDataTransfer,
  createDataTransferDataProviderApproval,
  createDataTransferGroupApproval,
  createDataTransferNodeApproval,
  createGroup,
} from '../../../factories';
import { DataTransferApprovalForm, Title } from './DataTransferApprovalForm';
import { getInitialState, makeStore } from '../../store';
import { render, screen } from '@testing-library/react';
import camelCase from 'lodash/camelCase';
import { Provider } from 'react-redux';
import React from 'react';

const groupApproval = createDataTransferGroupApproval();
const nodeApproval = createDataTransferNodeApproval();
const dpApproval = createDataTransferDataProviderApproval();
const dtrId = 44;
const projectName = 'Project Number 1';
const dtr = createDataTransfer({ id: dtrId, projectName: projectName });

describe('Title', function () {
  it.each`
    approval        | institution                                                              | approvalInstitution
    ${nodeApproval} | ${`${nodeApproval.node?.name} (${nodeApproval.node?.code})`}             | ${ApprovalInstitutionEnum.node}
    ${dpApproval}   | ${`${dpApproval.dataProvider?.name} (${dpApproval.dataProvider?.code})`} | ${ApprovalInstitutionEnum.data_provider}
  `(
    'Should show correct information when approval is $approval',
    async function ({ approval, institution, approvalInstitution }) {
      const dtrInformation = `${dtrId} - ${projectName}`;

      render(
        <Title
          dtr={dtr}
          approval={approval}
          approvalInstitution={approvalInstitution}
        />,
      );

      expect(await screen.findByText(dtrInformation)).toBeInTheDocument();
      expect(await screen.findByText(institution)).toBeInTheDocument();
    },
  );

  describe('DataTransferApprovalForm', function () {
    it.each`
      approval         | approvalInstitution
      ${nodeApproval}  | ${ApprovalInstitutionEnum.node}
      ${dpApproval}    | ${ApprovalInstitutionEnum.data_provider}
      ${groupApproval} | ${ApprovalInstitutionEnum.group}
    `(
      'Should show "technicalMeasuresInPlace" checkbox for non-group approval (current: $approvalInstitution)',
      async function ({ approval, approvalInstitution }) {
        const technicalMeasuresInPlace = `dataTransferApprovals:declarations.${camelCase(
          approvalInstitution,
        )}.technicalMeasuresInPlace`;

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={approval}
              approvalInstitution={approvalInstitution}
              dtr={dtr}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>,
        );

        if (approvalInstitution === 'group') {
          expect(
            screen.queryByRole(technicalMeasuresInPlace),
          ).not.toBeInTheDocument();
        } else {
          expect(
            await screen.findByText(technicalMeasuresInPlace),
          ).toBeInTheDocument();
        }
      },
    );

    it.each`
      dtrPurpose      | expected
      ${'PRODUCTION'} | ${true}
      ${'TEST'}       | ${false}
    `(
      'Should show "existingLegalBasis" checkbox $expected if purpose is $dtrPurpose',
      async function ({ dtrPurpose, expected }) {
        const existingLegalBasis =
          'dataTransferApprovals:declarations.node.existingLegalBasis';

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={nodeApproval}
              approvalInstitution={ApprovalInstitutionEnum.node}
              dtr={{ ...dtr, purpose: dtrPurpose }}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>,
        );

        if (expected) {
          expect(
            await screen.findByText(existingLegalBasis),
          ).toBeInTheDocument();
        } else {
          expect(
            screen.queryByText(existingLegalBasis),
          ).not.toBeInTheDocument();
        }
      },
    );

    it.each`
      role                                                        | expected
      ${DataTransferGroupApprovalsInnerGroupProfileRoleEnum.ELSI} | ${true}
      ${DataTransferGroupApprovalsInnerGroupProfileRoleEnum.FAIR} | ${false}
    `(
      'Should show "existingLegalBasis" checkbox $expected if group role is $role',
      async function ({ role, expected }) {
        const existingLegalBasis =
          'dataTransferApprovals:declarations.group.existingLegalBasis';

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={createDataTransferGroupApproval({
                group: createGroup({
                  profile: {
                    role,
                  },
                }),
              })}
              approvalInstitution={ApprovalInstitutionEnum.group}
              dtr={createDataTransfer({
                purpose: DataTransferPurposeEnum.PRODUCTION,
              })}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>,
        );

        if (expected) {
          expect(
            await screen.findByText(existingLegalBasis),
          ).toBeInTheDocument();
        } else {
          expect(
            screen.queryByText(existingLegalBasis),
          ).not.toBeInTheDocument();
        }
      },
    );

    it.each`
      approval         | purpose                               | approvalInstitution                      | expected
      ${nodeApproval}  | ${DataTransferPurposeEnum.PRODUCTION} | ${ApprovalInstitutionEnum.node}          | ${false}
      ${dpApproval}    | ${DataTransferPurposeEnum.TEST}       | ${ApprovalInstitutionEnum.data_provider} | ${false}
      ${dpApproval}    | ${DataTransferPurposeEnum.PRODUCTION} | ${ApprovalInstitutionEnum.data_provider} | ${true}
      ${groupApproval} | ${DataTransferPurposeEnum.PRODUCTION} | ${ApprovalInstitutionEnum.group}         | ${false}
    `(
      `Should show "dataDeliveryApproved" ($expected) checkbox if:
      1) the approval institution is $approvalInstitution
      2) the purpose is $purpose`,
      async function ({ approval, purpose, approvalInstitution, expected }) {
        const dataDeliveryApproved = `dataTransferApprovals:declarations.${camelCase(
          approvalInstitution,
        )}.dataDeliveryApproved`;

        render(
          <Provider store={makeStore()}>
            <DataTransferApprovalForm
              approval={approval}
              approvalInstitution={approvalInstitution}
              dtr={createDataTransfer({ purpose })}
              onClose={() => {
                return;
              }}
              open={true}
              isRejectionForm={false}
            />
          </Provider>,
        );

        if (expected) {
          expect(
            await screen.findByText(dataDeliveryApproved),
          ).toBeInTheDocument();
        } else {
          expect(
            screen.queryByText(dataDeliveryApproved),
          ).not.toBeInTheDocument();
        }
      },
    );
  });
});

describe('RejectionForm', function () {
  it.each`
    isRejectionForm | approval         | approvalInstitution                      | expected                                           | len
    ${false}        | ${dpApproval}    | ${ApprovalInstitutionEnum.data_provider} | ${'dataTransfer:actionButtons.approve'}            | ${3}
    ${false}        | ${nodeApproval}  | ${ApprovalInstitutionEnum.node}          | ${'dataTransfer:actionButtons.approve'}            | ${3}
    ${false}        | ${groupApproval} | ${ApprovalInstitutionEnum.group}         | ${'dataTransfer:actionButtons.approve'}            | ${1}
    ${true}         | ${dpApproval}    | ${ApprovalInstitutionEnum.data_provider} | ${'dataTransferApprovals:rejectDataTransfer.text'} | ${0}
    ${true}         | ${nodeApproval}  | ${ApprovalInstitutionEnum.node}          | ${'dataTransferApprovals:rejectDataTransfer.text'} | ${0}
    ${true}         | ${groupApproval} | ${ApprovalInstitutionEnum.group}         | ${'dataTransferApprovals:rejectDataTransfer.text'} | ${0}
  `(
    'Should show the form (rejection=$isRejectionForm) for $approvalType',
    async function ({
      isRejectionForm,
      approval,
      approvalInstitution,
      expected,
      len,
    }) {
      render(
        <Provider store={makeStore(undefined, getInitialState())}>
          <DataTransferApprovalForm
            dtr={{
              ...dtr,
              purpose: DataTransferPurposeEnum.PRODUCTION,
              status: DataTransferStatusEnum.EXPIRED,
            }}
            approval={approval}
            onClose={() => undefined}
            open={true}
            approvalInstitution={approvalInstitution}
            isRejectionForm={isRejectionForm}
          />
        </Provider>,
      );
      if (len > 0) {
        const checkboxes = await screen.findAllByRole('checkbox');
        expect(checkboxes).toHaveLength(len);
      } else {
        expect(screen.queryByRole('checkbox')).not.toBeInTheDocument();
        const textboxes = await screen.findAllByRole('textbox');
        expect(textboxes).toHaveLength(1);
      }
      expect(await screen.findByText(expected)).toBeInTheDocument();
    },
  );
});
