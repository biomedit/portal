import { type DefaultRootState, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import type { RootState } from '../store';
import type { User } from '../api/generated';

export const isStaff = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.staff;

export const isNodeAdmin = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.nodeAdmin;

export const isNodeViewer = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.nodeViewer;

export const isDataProviderAdmin = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataProviderAdmin;

export const isDataProviderViewer = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataProviderViewer;

export const isGroupManager = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.groupManager;

export const isDataManager = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.dataManager;

export const isProjectManager = (state: DefaultRootState): boolean =>
  !!state.auth.user?.permissions?.manager;

export const canAddDataTransfer = createSelector(
  isStaff,
  isDataManager,
  (staff, dataManager) => staff || dataManager,
);

export const canAddTermsOfUse = createSelector(
  isStaff,
  isNodeAdmin,
  (staff, nodeAdmin) => staff || nodeAdmin,
);

export const canExportTermsOfUse = createSelector(
  isStaff,
  isNodeAdmin,
  isNodeViewer,
  (staff, nodeAdmin, nodeViewer) => staff || nodeAdmin || nodeViewer,
);

export const canAddProjects = (state: DefaultRootState): boolean =>
  state.auth.pagePermissions.project.add;

export function useParamSelector<T, S>(
  selector: (state: RootState, ...params: S[]) => T,
  ...params: S[]
): T | null {
  return useSelector<RootState, T>((state) => selector(state, ...params));
}

export const authenticatedUser = (state: RootState): User | undefined =>
  state.users.itemList.find((user) => user.id === state.auth.user?.id);

export const canSeeProjectUserRoleHistory = createSelector(
  isStaff,
  isNodeAdmin,
  isNodeViewer,
  isProjectManager,
  (staff, nodeAdmin, nodeViewer, projectManager) =>
    staff || nodeAdmin || nodeViewer || projectManager,
);
