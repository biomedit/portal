import { FixedChildrenHeight } from '@biomedit/next-widgets';
import { styled } from '@mui/material/styles';

export const FormFieldsContainer = styled(FixedChildrenHeight)(({ theme }) => ({
  marginTop: theme.spacing(1),
}));
