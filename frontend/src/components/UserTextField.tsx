import {
  AddIcon,
  Description,
  Dialog,
  useDialogState,
} from '@biomedit/next-widgets';
import {
  Box,
  Button,
  Chip,
  Stack,
  styled,
  TextField,
  type TextFieldProps,
} from '@mui/material';
import React, { useState } from 'react';
import defaultTo from 'lodash/defaultTo';
import type { FieldPath } from 'react-hook-form/dist/types/path';
import type { FieldValues } from 'react-hook-form/dist/types';
import { generatedBackendApi } from '../api/api';
import { I18nNamespace } from '../i18n';
import isemail from 'isemail';
import { useFormContext } from 'react-hook-form';
import type { User } from '../api/generated';
import { useTranslation } from 'next-i18next';
import { useUserEntries } from './projectForm/UsersListHooks';

const StyledFieldset = styled('fieldset')(({ theme }) => ({
  width: '100%',
  marginBottom: theme.spacing(5),
  marginInlineStart: 0,
  borderRadius: theme.shape.borderRadius,
  border: '1px',
  borderColor: theme.palette.grey[400],
  borderStyle: 'solid',
  fontSize: '0.75rem',
  minHeight: theme.spacing(17),
}));

const StyledLegend = styled('legend')(({ theme }) => ({
  paddingInlineStart: '0.3rem',
  paddingInlineEnd: '0.3rem',
  color: theme.palette.text.secondary,
}));

type UserTextFieldProps<T extends FieldValues> = Omit<
  TextFieldProps,
  'error' | 'helperText' | 'type' | 'value'
> & {
  addUser: (user: User) => void;
  name: FieldPath<T>;
  validateUser?: (user: User) => boolean;
};

export function UserTextField<T extends FieldValues>({
  name,
  addUser,
  validateUser,
  ...props
}: UserTextFieldProps<T>) {
  const { t } = useTranslation([I18nNamespace.USER_TEXT_FIELD]);
  const [inputValue, setInputValue] = useState<string>('');
  const {
    item: user,
    setItem: setUser,
    onClose,
    open,
  } = useDialogState<User | null>();

  const {
    clearErrors,
    setError,
    formState: { errors },
  } = useFormContext<T>();

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    clearErrors(name);
    setInputValue(defaultTo(event.target.value, ''));
  };

  const userEntries = useUserEntries(user);

  const fetchUser = (email: string): void => {
    if (!isemail.validate(email, { minDomainAtoms: 2 })) {
      setError(name, {
        message: t('invalidEmail'),
      });
      return;
    }
    generatedBackendApi
      .listUserLites({ email })
      .then((response) => {
        if (response.length === 0) {
          setError(name, {
            message: t('userNotFound'),
          });
          return;
        }
        if (validateUser && !validateUser(response[0])) {
          setError(name, { message: t('invalidUser') });
          return;
        }
        setUser(response[0]);
      })
      .catch((error) => {
        setError(name, {
          message: error.message,
        });
      });
  };

  const AddUserButton = () => {
    return (
      <Button
        sx={(theme) => ({
          '&:disabled': {
            background: theme.palette.success.main,
            color: theme.palette.success.contrastText,
            opacity: 0.3,
          },
          height: '2.55rem',
        })}
        variant="contained"
        color="success"
        startIcon={<AddIcon />}
        aria-label={'Add ' + t(`${I18nNamespace.COMMON}:models.user`)}
        title={'Add ' + t(`${I18nNamespace.COMMON}:models.user`)}
        onClick={() => {
          fetchUser(inputValue);
        }}
        disabled={inputValue.length === 0}
      >
        {t(`${I18nNamespace.COMMON}:models.user`)}
      </Button>
    );
  };

  return (
    <>
      {user && (
        <Dialog
          title={'User'}
          isSubmitting={false}
          open={open}
          confirmLabel="Add"
          confirmButtonProps={{
            onClick: () => {
              addUser(user);
              setUser(null);
              setInputValue('');
            },
            type: 'button',
          }}
          text={t('addUserConfirmationText')}
          onClose={onClose}
          maxWidth={false}
        >
          <Description
            entries={userEntries}
            labelWidth="30%"
            styleRules={{
              caption: {
                padding: '.7rem 0',
                paddingRight: '.5rem',
              },
              value: {
                padding: '.7rem 0',
              },
              table: {
                marginTop: '1rem',
              },
            }}
          />
        </Dialog>
      )}
      <Box
        sx={{
          display: 'flex',
          gap: '.2rem',
          marginBottom: '.7rem',
          alignItems: 'flex-start',
        }}
      >
        <TextField
          size="small"
          fullWidth
          placeholder="Enter email address"
          error={!!errors?.[name]}
          helperText={errors?.[name]?.message as string}
          value={inputValue}
          type="email"
          onKeyDown={(event) => {
            if (event.key === 'Enter') {
              fetchUser(inputValue);
              event.preventDefault();
            }
          }}
          onChange={onChange}
          {...props}
        />
        <AddUserButton />
      </Box>
    </>
  );
}

type MultipleUsersTextFieldUser = Pick<User, 'displayName' | 'id'>;

type MultipleUsersTextFieldProps<
  T extends MultipleUsersTextFieldUser,
  S extends FieldValues,
> = UserTextFieldProps<S> & {
  deleteUser: (user: User) => () => void;
  legend: string;
  users: T[];
};

export function MultipleUsersTextField<
  T extends MultipleUsersTextFieldUser,
  S extends FieldValues,
>({
  deleteUser,
  legend,
  users,
  disabled,
  ...rest
}: MultipleUsersTextFieldProps<T, S>) {
  return (
    <StyledFieldset>
      <StyledLegend>{legend}</StyledLegend>
      <UserTextField disabled={disabled} {...rest} />
      <Stack direction="row" spacing={1} useFlexGap flexWrap="wrap">
        {users.map((user) => (
          <Chip
            key={user.id}
            label={user.displayName}
            onDelete={deleteUser(user)}
            disabled={disabled}
          />
        ))}
      </Stack>
    </StyledFieldset>
  );
}

export function useMultipleUsersTextField<
  T extends MultipleUsersTextFieldUser,
>({
  initialUsers,
}: {
  initialUsers: T[];
}): {
  addUser: (user: User) => void;
  deleteUser: (user: User) => () => void;
  users: T[];
} {
  const [users, setUsers] = useState(initialUsers || []);
  const deleteUser = (user: User) => () =>
    setUsers(users?.filter((dataRecipient) => dataRecipient.id !== user.id));
  const addUser = (user: User) => {
    if (!users.some((dataRecipient) => dataRecipient.id === user.id)) {
      setUsers([...users, user as T]);
    }
  };

  return { users, addUser, deleteUser };
}
