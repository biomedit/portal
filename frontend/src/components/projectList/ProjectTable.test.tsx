import {
  createAuthState,
  createPagePermissions,
  createProjectPagePermissions,
} from '../../../factories';
import { getInitialState, makeStore } from '../../store';
import { render, screen } from '@testing-library/react';
import { ProjectTable } from './ProjectTable';
import { Provider } from 'react-redux';
import React from 'react';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
  }),
}));

describe('ProjectTable', () => {
  it('Should show the add button only to staff users', async () => {
    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createAuthState({
            pagePermissions: createPagePermissions({
              project: createProjectPagePermissions({ add: true }),
            }),
          }),
        })}
      >
        <ProjectTable />
      </Provider>,
    );
    await screen.findByLabelText('Add Project');
  });

  it('Should not show the add button only to non staff users', async () => {
    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createAuthState({
            pagePermissions: createPagePermissions({
              project: createProjectPagePermissions({ add: false }),
            }),
          }),
        })}
      >
        <ProjectTable />
      </Provider>,
    );
    expect(screen.queryByLabelText('Add Project')).not.toBeInTheDocument();
  });
});
