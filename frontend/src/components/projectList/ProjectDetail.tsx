import { Box, CircularProgress, Typography } from '@mui/material';
import { canAddDataTransfer, canSeeProjectUserRoleHistory } from '../selectors';
import {
  Description,
  Field,
  formatDate,
  formatItemFields,
  type FormattedItemField,
  isIpInRange,
  type ListModel,
  Markdown,
  requestAction,
  type TabItem,
  TabPane,
  Tooltip,
  useReduxDispatch,
  Warning,
} from '@biomedit/next-widgets';
import {
  type IdentityProvider,
  type NodeIpAddressRangesInner,
  type Project,
  type ProjectResourcesInner,
  type ProjectUsersInner,
  ProjectUsersInnerRolesEnum,
} from '../../api/generated';
import {
  LOAD_DATA_TRANSFERS,
  LOAD_GROUPS,
  LOAD_IDENTITY_PROVIDERS,
  LOAD_MEMBER_SERVICES,
  LOAD_NODES,
} from '../../actions/actionTypes';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { DataTransferTable } from '../dataTransfer/DataTransferTable';
import { EmailLink } from '@biomedit/next-widgets';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { IpRangeSummary } from '../../widgets/IpRangeList';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { MemberServicesTable } from './MemberServicesTable';
import { ResourceTable } from './ResourceTable';
import { RolesTable } from '../../widgets/RolesTable';
import type { RootState } from '../../store';
import { termsOfUseEnabled } from '../../config';
import { useProjectDataTransfers } from '../dataTransfer/DataTransferHooks';
import { UserRoleHistory } from './UserRoleHistory';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';
import VerifiedIcon from '@mui/icons-material/Verified';

export type ProjectDetailProps = {
  project:
    | (Project & {
        resources: Array<IdRequired<ProjectResourcesInner>>;
        users: Array<IdRequired<ProjectUsersInner>>;
      })
    | undefined;
};

export function isIpInProjectRanges(
  ip: string | undefined,
  ipAddressRanges: Array<NodeIpAddressRangesInner> | undefined,
): boolean {
  return (
    !ip ||
    !ipAddressRanges ||
    ipAddressRanges.length === 0 ||
    ipAddressRanges.some(({ ipAddress, mask }) =>
      isIpInRange(ip, ipAddress, mask),
    )
  );
}

function generateWarningMessage(
  prj: Project,
  ipAddress: string | undefined,
): string | null {
  if (isIpInProjectRanges(ipAddress, prj.ipAddressRanges)) {
    return null;
  }
  return `Your IP address (${ipAddress}) is not within the valid IP address ranges of the project`;
}

export const ProjectDetail = ({
  project,
}: ProjectDetailProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.PROJECT_FORM);
  const { t: emptyMessageTFunction } = useTranslation([
    I18nNamespace.COMMON,
    I18nNamespace.LIST,
  ]);
  const { t: tAdditionalColumns } = useTranslation([
    I18nNamespace.PROJECT_LIST,
    I18nNamespace.USER_LIST,
  ]);
  const currentUser = useSelector((state: RootState) => state.auth.user);
  const canSeeUserRoleHistory = useSelector(canSeeProjectUserRoleHistory);
  const dispatch = useReduxDispatch();
  useEffect(() => {
    dispatch(requestAction(LOAD_NODES));
    // Load data transfers here instead of in `DataTransferTable.tsx`.
    // Otherwise, the data transfers load every time we open `ProjectForm.tsx`.
    dispatch(requestAction(LOAD_DATA_TRANSFERS));
    dispatch(requestAction(LOAD_GROUPS));
    dispatch(requestAction(LOAD_IDENTITY_PROVIDERS, { ordering: 'name' }));
  }, [dispatch]);

  useEffect(() => {
    if (project && project.id && currentUser) {
      dispatch(
        requestAction(LOAD_MEMBER_SERVICES, {
          projectPk: project.id.toString(),
          userPk: currentUser.id.toString(),
        }),
      );
    }
  }, [dispatch, project, currentUser]);

  const identityProviders = useSelector<
    RootState,
    Array<IdRequired<IdentityProvider>>
  >((state) => state.identityProviders.itemList);
  const nodes = useSelector((state: RootState) => state.nodes.itemList);
  const groups = useSelector((state: RootState) => state.groups.itemList);
  const services = useSelector(
    (state: RootState) => state.memberServices.itemList,
  );
  const { isFetching } = useSelector((state: RootState) => state.project);
  const ipAddress = useSelector(
    (state: RootState) => state.auth?.user?.ipAddress,
  );
  const detailEntries: FormattedItemField[] | null = useMemo(() => {
    const model: ListModel<Project> = {
      fields: [
        Field({
          caption: t('captions.code') as string,
          getProperty: (prj: Project) => prj.code,
          key: 'code',
        }),
        Field({
          caption: t('captions.destination') as string,
          getProperty: (prj: Project) => {
            const node = nodes.find((node) => node.code === prj.destination);
            return node ? `${node.name} (${node.code})` : prj.destination;
          },
          key: 'destination',
        }),
        Field({
          caption: t('captions.archived') as string,
          getProperty: (prj: Project) => (prj.archived ? 'Yes' : 'No'),
          key: 'archived',
        }),
        Field({
          caption: t('captions.expirationDate') as string,
          getProperty: (prj: Project) =>
            prj.expirationDate
              ? formatDate(prj.expirationDate, false, true)
              : t(`${I18nNamespace.COMMON}:noValuePlaceholder`),
          key: 'expirationDate',
        }),
        Field({
          caption: t('captions.legalApprovalGroup') as string,
          getProperty: (prj: Project) => {
            const group = groups.find(
              (group) => group.id == prj.legalApprovalGroup,
            );
            return (
              group?.name || t(`${I18nNamespace.COMMON}:noValuePlaceholder`)
            );
          },
          key: 'legalApprovalGroup',
        }),
        Field({
          caption: t('captions.dataSpecificationApprovalGroup') as string,
          getProperty: (prj: Project) => {
            const group = groups.find(
              (group) => group.id == prj.dataSpecificationApprovalGroup,
            );
            return (
              group?.name || t(`${I18nNamespace.COMMON}:noValuePlaceholder`)
            );
          },
          key: 'dataSpecificationApprovalGroup',
        }),
        Field({
          caption: t('captions.legalSupportContact') as string,
          getProperty: (prj: Project) => prj.legalSupportContact,
          key: 'legalSupportContact',
          render: (contact) => (
            <Box component="span" key={contact}>
              {contact ? (
                <EmailLink email={contact}>
                  {contact}
                  <MailOutlineIcon
                    sx={{ marginLeft: 1, verticalAlign: 'bottom' }}
                  />
                </EmailLink>
              ) : (
                t(`${I18nNamespace.COMMON}:noValuePlaceholder`)
              )}
            </Box>
          ),
        }),
        Field({
          caption: t('captions.sshSupport') as string,
          getProperty: (prj: Project) => (prj.sshSupport ? 'Yes' : 'No'),
          key: 'sshSupport',
        }),
        Field({
          caption: t('captions.resourcesSupport'),
          getProperty: (prj: Project) => (prj.resourcesSupport ? 'Yes' : 'No'),
          key: 'resourcesSupport',
        }),
        Field({
          caption: t('captions.servicesSupport'),
          getProperty: (prj: Project) => (prj.servicesSupport ? 'Yes' : 'No'),
          key: 'servicesSupport',
        }),
        Field({
          caption: t('captions.identityProvider') as string,
          getProperty: (prj: Project) =>
            prj.identityProvider === undefined
              ? t(`${I18nNamespace.COMMON}:noValuePlaceholder`)
              : identityProviders.find((idp) => idp.id === prj.identityProvider)
                  ?.name,
          key: 'identityProvider',
        }),
        Field({
          caption: t('subtitles.ipRanges') as string,
          getProperty: (prj: Project) => {
            if (
              prj.ipAddressRanges == undefined ||
              prj.ipAddressRanges.length === 0
            ) {
              return t(`${I18nNamespace.COMMON}:noValuePlaceholder`);
            }
            const ipAddressRangesList = IpRangeSummary(prj.ipAddressRanges);
            const warningMsg = generateWarningMessage(prj, ipAddress);

            if (warningMsg) {
              ipAddressRangesList.push(
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    marginLeft: '-15px',
                  }}
                >
                  <Warning tooltip="" />
                  <div style={{ marginLeft: '8px' }}>{warningMsg}</div>
                </div>,
              );
            }
            return ipAddressRangesList;
          },
          key: 'ipAddressRange',
        }),
      ],
    };
    return project ? formatItemFields<Project>(model, project) : null;
  }, [t, project, nodes, groups, ipAddress, identityProviders]);

  const canAddDtr = useSelector(canAddDataTransfer);
  const dataTransfers = useProjectDataTransfers(project);
  const dataTransferTable = useMemo(
    () => (
      <DataTransferTable
        dataTransfers={dataTransfers ?? []}
        canAdd={canAddDtr && !project?.archived}
        projectId={project?.id}
      />
    ),
    [canAddDtr, dataTransfers, project?.archived, project?.id],
  );
  const resourceTable = ResourceTable(project);
  const memberServicesTable = MemberServicesTable(services);

  const tabModel: TabItem[] = useMemo(() => {
    const columnHelper = createColumnHelper<IdRequired<ProjectUsersInner>>();
    const additionalColumns = [
      columnHelper.display({
        header: tAdditionalColumns(
          `${I18nNamespace.USER_LIST}:columns.authorized`,
        ),
        cell: (context) =>
          context.row.original.authorized && (
            <Tooltip title={tAdditionalColumns('authorizedTooltip')}>
              <VerifiedIcon
                color={'success'}
                sx={{ verticalAlign: 'bottom' }}
                aria-label={tAdditionalColumns('authorizedTooltip')}
              />
            </Tooltip>
          ),
      }),
    ];

    if (termsOfUseEnabled) {
      additionalColumns.push(
        columnHelper.display({
          header: tAdditionalColumns(
            `${I18nNamespace.USER_LIST}:columns.termsOfUseAccepted`,
          ),
          cell: (context) => context.row.original.termsOfUseAccepted,
        }),
      );
    }

    const tabs = [
      {
        id: 'details',
        label: 'Details',
        content: <Description entries={detailEntries} labelWidth="25%" />,
      },
      {
        id: 'dataTransfers',
        label: 'Data Transfers',
        content: dataTransferTable,
      },

      {
        id: 'users',
        label: 'Users',
        content: (
          <RolesTable
            users={project?.users ?? []}
            roles={Object.values(ProjectUsersInnerRolesEnum)}
            isFetching={isFetching}
            additionalColumns={additionalColumns}
          />
        ),
      },

      {
        id: 'additionalInfo',
        label: 'Additional Information',
        content:
          project?.additionalInfo?.trim() !== '' ? (
            <Markdown text={project?.additionalInfo || ''} />
          ) : (
            <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
              {t(`${I18nNamespace.COMMON}:noAdditionalInformation`)}
            </Typography>
          ),
      },
    ];

    if (project?.resourcesSupport) {
      tabs.push({
        id: 'resources',
        label: 'Resources',
        content: resourceTable,
      });
    }

    if (project?.servicesSupport) {
      tabs.push({
        id: 'services',
        label: 'Services',
        content: memberServicesTable,
      });
    }

    if (project && canSeeUserRoleHistory) {
      tabs.push({
        id: 'userRoleHistory',
        label: 'User Role History',
        content: <UserRoleHistory projectCode={project.code} />,
      });
    }

    return tabs;
  }, [
    canSeeUserRoleHistory,
    dataTransferTable,
    detailEntries,
    isFetching,
    memberServicesTable,
    project,
    resourceTable,
    t,
    tAdditionalColumns,
  ]);

  if (isFetching) {
    return <CircularProgress />;
  }

  return project ? (
    <TabPane
      id="ProjectDetail"
      label="Switch between details of the project."
      model={tabModel}
      panelBoxProps={{ sx: { height: '50vh' } }}
    />
  ) : (
    <Typography variant="body2" component="pre" sx={{ paddingTop: 2 }}>
      {emptyMessageTFunction(`${I18nNamespace.LIST}:emptyMessage`, {
        model: emptyMessageTFunction(`${I18nNamespace.COMMON}:models.project`),
      })}
    </Typography>
  );
};
