import { EnhancedTable, useReduxDispatch } from '@biomedit/next-widgets';
import type { Project, ProjectResourcesInner } from '../../api/generated';
import React, { type ReactElement, useCallback } from 'react';
import { useResourceColumns, useResourceForm } from './ResourceHooks';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { produce } from 'immer';
import type { RootState } from '../../store';
import { updateProjectAction } from '../../actions/actionTypes';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const resourceTableTitle = `${I18nNamespace.COMMON}:models.resource_plural`;
export const resourceTableAddButtonLabel = `${I18nNamespace.PROJECT_RESOURCE_LIST}:addButton`;

export const ResourceTable = (
  project?: Project & { resources: Array<IdRequired<ProjectResourcesInner>> },
): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.PROJECT_RESOURCE_LIST,
    I18nNamespace.COMMON,
  ]);

  const { isSubmitting, isFetching } = useSelector(
    (state: RootState) => state.project,
  );

  const canAddResource = project?.permissions?.edit?.resources;

  const dispatch = useReduxDispatch();
  const deleteItem = useCallback(
    (id: number) => {
      if (!project) {
        return;
      }
      const newProject: Project = produce<Project>(project, (draft) => {
        const resources = draft.resources ?? [];
        const index = resources.findIndex((resource) => resource.id == id);
        if (index > -1) {
          resources.splice(index, 1);
        }
      });
      dispatch(updateProjectAction(newProject));
    },
    [project, dispatch],
  );

  const { openFormDialog, resourceForm } = useResourceForm(project);
  const columns = useResourceColumns();

  const getDeleteConfirmationText = useCallback(
    (item: IdRequired<ProjectResourcesInner>) => 'Delete ' + item.name,
    [],
  );

  return (
    <>
      <EnhancedTable
        itemList={project?.resources ?? []}
        columns={columns}
        canAdd={canAddResource}
        canEdit={canAddResource}
        canDelete={canAddResource}
        onAdd={openFormDialog}
        onEdit={openFormDialog}
        form={resourceForm}
        onDelete={deleteItem}
        addButtonLabel={t(resourceTableAddButtonLabel) as string}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        canFilter={false}
        pagination={false}
        getDeleteConfirmationText={getDeleteConfirmationText}
        inline
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.resource_plural`),
          }) as string
        }
      />
    </>
  );
};
