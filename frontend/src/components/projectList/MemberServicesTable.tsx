import { Box, Divider, Typography } from '@mui/material';
import { EnhancedTable, formatDate, Markdown } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo, useState } from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import type { Service } from '../../api/generated';
import { useTranslation } from 'next-i18next';

export const MemberServicesTable = (
  memberServices: Array<IdRequired<Service>>,
): ReactElement => {
  const { t } = useTranslation([I18nNamespace.SERVICE_LIST]);

  const columnHelper = createColumnHelper<IdRequired<Service>>();

  const columns = useMemo(() => {
    return [
      columnHelper.accessor('name', {
        header: t('columns.name'),
      }),
      columnHelper.accessor('state', {
        header: t('columns.state'),
      }),
      columnHelper.accessor('action', {
        header: t('columns.action'),
        cell: (props) =>
          props.row.original.action === 'NONE' ? '' : props.row.original.action,
      }),
      columnHelper.accessor('created', {
        header: t('columns.created'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.row.original.created, true) ?? '',
      }),
      columnHelper.accessor('changed', {
        header: t('columns.changed'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.row.original.changed, true) ?? '',
      }),
    ];
  }, [t, columnHelper]);

  const [serviceTitle, setServiceTitle] = useState('');
  const [serviceDescription, setServiceDescription] = useState('');
  const [serviceDetails, setServiceDetails] = useState('');

  return (
    <div>
      <EnhancedTable<IdRequired<Service>>
        itemList={memberServices}
        columns={columns}
        canAdd={false}
        canEdit={false}
        canDelete={false}
        form={<div />}
        isFetching={false}
        isSubmitting={false}
        onRowClick={(service) => {
          setServiceTitle(service.name);
          setServiceDescription(service.description ?? '');
          setServiceDetails(service.data ?? '');
        }}
        inline
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.service_plural`),
          }) as string
        }
      />
      <Box
        sx={{
          display: serviceTitle ? 'block' : 'none',
          border: '1px solid',
          borderColor: 'grey.300',
          borderRadius: 2,
          padding: 2,
        }}
      >
        <Typography variant="h4" gutterBottom>
          {serviceTitle}
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          {serviceDescription}
        </Typography>
        <Divider
          sx={{ display: serviceDetails ? 'block' : 'none', marginY: 2 }}
        />
        <Typography variant="body1">
          <Markdown text={serviceDetails} />
        </Typography>
      </Box>
    </div>
  );
};
