import { isClient, Tooltip } from '@biomedit/next-widgets';
import React, { type ReactElement, useState } from 'react';
import Link from '@mui/material/Link';
import type { ProjectResourcesInner } from '../../api/generated';
import { SSHDialog } from '../sshDialog';

const preventDefault = (event: React.MouseEvent) => event.preventDefault();

type ResourceLinkProps = {
  resource: ProjectResourcesInner;
  href?: string;
  onClick?: (event: React.MouseEvent) => void;
};

const ResourceLink = ({
  resource,
  href = undefined,
  onClick = undefined,
  ...other
}: Readonly<ResourceLinkProps>): ReactElement => {
  href = href || resource.location;
  return (
    <Tooltip title={resource.location}>
      <Link href={href} onClick={onClick} {...other}>
        {resource.name}
      </Link>
    </Tooltip>
  );
};

const DefaultComponent = ({
  resource,
}: {
  resource: ProjectResourcesInner;
}): ReactElement => {
  const other = isClient()
    ? { href: resource.location, rel: 'noopener noreferrer', target: '_blank' }
    : { href: '#', onClick: preventDefault };
  return <ResourceLink resource={resource} {...other} />;
};

interface SSHFieldProps {
  resource: ProjectResourcesInner;
  protocol: string | null;
}

const SSHField = ({ resource, protocol }: SSHFieldProps): ReactElement => {
  const path = protocol && resource.location.substring(protocol.length + 3);
  const [open, setOpen] = useState<boolean>(false);
  return (
    <>
      <ResourceLink
        resource={resource}
        onClick={() => setOpen(true)}
        href="#"
      />
      <SSHDialog
        open={open}
        onClose={() => setOpen(false)}
        command={protocol + ' ' + path}
      />
    </>
  );
};

const componentByProtocol = new Map([
  ['ssh', SSHField],
  ['sftp', SSHField],
]);

export const ResourceField = (
  resource: ProjectResourcesInner,
): ReactElement => {
  const matches = resource.location?.match(/[a-z]*(?=:\/\/)/);
  const protocol = matches && matches[0];
  const Component =
    (protocol && componentByProtocol.get(protocol)) || DefaultComponent;
  return <Component {...{ protocol, resource }} />;
};
