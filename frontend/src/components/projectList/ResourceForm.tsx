import { CALL, updateProjectAction } from '../../actions/actionTypes';
import {
  ConfirmLabel,
  email,
  FormDialog,
  HiddenField,
  LabelledField,
  required,
  url,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import type { Project, ProjectResourcesInner } from '../../api/generated';
import React, { type ReactElement } from 'react';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../../i18n';
import { produce } from 'immer';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

type ResourceFormProps = {
  onClose: () => void;
  project: Project;
  resource: Partial<ProjectResourcesInner>;
};

export const ResourceForm = ({
  project,
  resource,
  onClose,
}: Readonly<ResourceFormProps>): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.PROJECT_RESOURCE_LIST,
    I18nNamespace.COMMON,
  ]);
  const dispatch = useReduxDispatch();

  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.project.isSubmitting,
  );
  const isEdit = !!resource.id;

  const form = useEnhancedForm<ProjectResourcesInner>();

  function submit(resourceForm: ProjectResourcesInner) {
    const newProject = produce(project, (draft) => {
      const resources = draft.resources ?? [];
      if (isEdit) {
        const index = resources.findIndex(
          (resource) => resource.id == resourceForm.id,
        );
        if (index > -1) {
          resources[index] = resourceForm;
        }
      } else {
        resources.push(resourceForm);
      }
    });

    dispatch(
      updateProjectAction(newProject, {
        type: CALL,
        callback: onClose,
      }),
    );
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={isEdit ? `${t('columns.name')}: ${resource?.name}` : 'New '}
        open={!!resource}
        onSubmit={submit}
        onClose={onClose}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
      >
        <HiddenField name="id" initialValues={{ id: resource.id }} />
        <FormFieldsContainer>
          <LabelledField
            name="name"
            type="text"
            validations={required}
            label={t(`${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.name`)}
            initialValues={resource}
            fullWidth
          />
          <LabelledField
            name="location"
            type="url"
            validations={[required, url]}
            label={t(`${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.location`)}
            initialValues={resource}
            fullWidth
          />
          <LabelledField
            name="contact"
            type="text"
            validations={email}
            label={t(`${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.contact`)}
            initialValues={resource}
            fullWidth
          />
          <LabelledField
            name="description"
            type="text"
            label={t(
              `${I18nNamespace.PROJECT_RESOURCE_LIST}:columns.description`,
            )}
            initialValues={resource}
            fullWidth
            multiline
            rows={4}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
