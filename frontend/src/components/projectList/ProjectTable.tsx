import {
  EnhancedTable,
  formatDate,
  requestAction,
  type TabItem,
  TabPane,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { canAddProjects } from '../selectors';
import { createColumnHelper } from '@tanstack/react-table';
import DoneIcon from '@mui/icons-material/Done';
import { I18nNamespace } from '../../i18n';
import { LOAD_PROJECTS } from '../../actions/actionTypes';
import type { RootState } from '../../store';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export const ProjectTable = (): ReactElement => {
  const {
    itemList: projects,
    isFetching,
    isSubmitting,
  } = useSelector((state: RootState) => state.project);

  const canAdd = useSelector(canAddProjects);
  const columnHelper = createColumnHelper<Unpacked<typeof projects>>();
  const { t } = useTranslation(I18nNamespace.PROJECT_LIST);

  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(requestAction(LOAD_PROJECTS));
  }, [dispatch]);

  const columns = useMemo(() => {
    return [
      columnHelper.accessor('code', {
        header: t('columns.code'),
      }),
      columnHelper.accessor('name', {
        header: t('columns.name'),
      }),
      columnHelper.accessor('archived', {
        header: t('columns.archived'),
        cell: (props) => props.getValue() && <DoneIcon />,
      }),
      columnHelper.accessor('expirationDate', {
        header: t('columns.expirationDate'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.getValue(), true) ?? '',
      }),
      columnHelper.accessor('created', {
        header: t('columns.dateCreated'),
        sortingFn: 'datetime',
        cell: (props) => formatDate(props.getValue(), true) ?? '',
      }),
      columnHelper.accessor('destination', {
        header: t('columns.destination'),
      }),
      columnHelper.accessor('dataTransferCount', {
        header: t('columns.numDataTransfers'),
      }),
    ];
  }, [t, columnHelper]);

  const router = useRouter();

  const tabModel: TabItem[] = useMemo(() => {
    const tableProps = {
      addButtonLabel: 'Project',
      canAdd,
      columns,
      inline: true,
      itemList: projects,
      isFetching,
      isSubmitting,
      onAdd: () => router.push('/projects/new'),
      onRowClick: (item: Unpacked<typeof projects>) =>
        router.push(`/projects/${item.id}`),
      emptyMessage: t(`${I18nNamespace.LIST}:emptyMessage`, {
        model: t(`${I18nNamespace.COMMON}:models.project_plural`),
      }),
    };

    return [
      {
        id: 'all',
        label: 'All',
        content: <EnhancedTable {...tableProps} />,
      },
      {
        id: 'user',
        label: 'My Projects',
        content: (
          <EnhancedTable
            {...tableProps}
            itemList={projects.filter((prj) => prj.userHasRole)}
          />
        ),
      },
      {
        id: 'archived',
        label: 'Archived',
        content: (
          <EnhancedTable
            {...tableProps}
            itemList={projects.filter((prj) => prj.archived)}
          />
        ),
      },
    ];
  }, [canAdd, columns, isFetching, isSubmitting, projects, router, t]);

  return (
    <TabPane
      id="ProjectTable"
      label="Switch between different views for projects."
      model={tabModel}
    />
  );
};
