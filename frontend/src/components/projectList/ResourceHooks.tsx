import { EmailLink, useFormDialog } from '@biomedit/next-widgets';
import type { Project, ProjectResourcesInner } from '../../api/generated';
import React, { useMemo } from 'react';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import type { IdRequired } from '../../global';
import { ResourceField } from './ResourceField';
import { ResourceForm } from './ResourceForm';
import { useTranslation } from 'next-i18next';

export const useResourceColumns = () => {
  const columnHelper = createColumnHelper<IdRequired<ProjectResourcesInner>>();
  const { t } = useTranslation(I18nNamespace.PROJECT_RESOURCE_LIST);
  return useMemo(() => {
    return [
      columnHelper.accessor('name', {
        header: t('columns.name'),
        cell: ({ row: { original } }) => ResourceField(original),
      }),
      columnHelper.accessor('description', {
        header: t('columns.description'),
      }),
      columnHelper.accessor('contact', {
        header: t('columns.contact'),
        cell: (props) => {
          const email = props.getValue();
          return email ? EmailLink({ email }) : null;
        },
      }),
    ];
  }, [t, columnHelper]);
};

export const useResourceForm = (project?: Project) => {
  const newResource = {
    name: undefined,
    location: undefined,
    description: undefined,
    contact: undefined,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<ProjectResourcesInner>>(newResource);
  const resourceForm = data && project && (
    <ResourceForm project={project} resource={data} onClose={closeFormDialog} />
  );
  return {
    resourceForm,
    ...rest,
  };
};
