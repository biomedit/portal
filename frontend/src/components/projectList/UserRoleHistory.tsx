import {
  EnhancedTable,
  formatDate,
  requestAction,
  type Unpacked,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { createColumnHelper } from '@tanstack/react-table';
import { I18nNamespace } from '../../i18n';
import { LOAD_PROJECT_USER_ROLE_HISTORY } from '../../actions/actionTypes';
import React from 'react';
import type { RootState } from '../../store';
import { snakeCase } from 'lodash';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

interface UserRoleHistoryProps {
  projectCode: string;
}

export const UserRoleHistory = ({ projectCode }: UserRoleHistoryProps) => {
  const { t } = useTranslation();
  const dispatch = useReduxDispatch();
  const columnHelper = createColumnHelper<Unpacked<typeof itemList>>();

  useEffect(() => {
    dispatch(requestAction(LOAD_PROJECT_USER_ROLE_HISTORY));
  }, [dispatch]);

  const { itemList, isFetching } = useSelector(
    (state: RootState) => state.projectUserRoleHistory,
  );

  const columns = [
    columnHelper.accessor('user', {
      header: t(`${I18nNamespace.PROJECT_USER_ROLE_HISTORY}:columns.user`),
      sortingFn: 'alphanumeric',
    }),
    columnHelper.accessor('permission', {
      header: t(
        `${I18nNamespace.PROJECT_USER_ROLE_HISTORY}:columns.permission`,
      ),
      sortingFn: 'alphanumeric',
      cell: (row) =>
        t(
          `${I18nNamespace.PROJECT_USER_LIST}:roleShortTitle.${snakeCase(row.getValue()).toUpperCase()}`,
        ),
    }),
    columnHelper.accessor('enabled', {
      header: t(`${I18nNamespace.PROJECT_USER_ROLE_HISTORY}:columns.enabled`),
      cell: (row) => (row.getValue() ? 'Added' : 'Removed'),
      sortingFn: 'alphanumeric',
    }),
    columnHelper.accessor('timestamp', {
      header: t(`${I18nNamespace.PROJECT_USER_ROLE_HISTORY}:columns.timestamp`),
      sortingFn: 'datetime',
      cell: (props) => formatDate(props.getValue(), true),
    }),
    columnHelper.accessor('changedBy', {
      header: t(`${I18nNamespace.PROJECT_USER_ROLE_HISTORY}:columns.changedBy`),
      sortingFn: 'alphanumeric',
    }),
  ];

  return (
    <EnhancedTable
      itemList={itemList.filter((item) => item.project === projectCode)}
      columns={columns}
      isFetching={isFetching}
      isSubmitting={false}
      emptyMessage={
        t(`${I18nNamespace.LIST}:emptyMessage`, {
          model: t(`${I18nNamespace.COMMON}:models.history`),
        }) as string
      }
    />
  );
};
