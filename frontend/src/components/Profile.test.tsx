import {
  affiliationFieldCaption,
  displayNameFieldCaption,
  emailFieldCaption,
  ipAddressFieldCaption,
  localUsernameFieldCaption,
  Profile,
  usernameFieldCaption,
  useUserInfoFields,
} from './Profile';
import type { CustomAffiliation, Userinfo } from '../api/generated';
import { getInitialState, makeStore, type RootState } from '../store';
import { render, renderHook, screen } from '@testing-library/react';
import { initialState } from '../reducers/auth';
import { mockSuccessfulApiCall } from '../testUtils';
import { Provider } from 'react-redux';
import React from 'react';

describe('Profile', () => {
  const ip = '127.0.0.1';

  const getUser = (): Userinfo => {
    return {
      username: '63457666153@eduid.ch',
      email: 'chuck.norris@roundhouse.kick',
      firstName: 'Chuck',
      lastName: 'Norris',
      profile: {
        affiliation: '',
        localUsername: 'cnorris',
        displayName: 'Chuck Norris (chuck.norris@roundhouse.kick)',
        displayId: 'ID: 63457666153',
        uid: 1000000,
        gid: 1000000,
        namespace: 'ch',
        displayLocalUsername: 'ch_cnorris',
      },
      flags: [],
      id: 2,
      ipAddress: ip,
      permissions: {
        manager: true,
        staff: true,
        dataManager: true,
        groupManager: true,
        nodeAdmin: false,
        nodeViewer: false,
        dataProviderAdmin: false,
        dataProviderViewer: false,
        hasProjects: true,
      },
    };
  };

  let user: Userinfo;
  const customAffiliations: CustomAffiliation[] = [
    {
      id: 1,
      code: 'ABC',
      name: 'Custom Affiliation 1',
    },
    {
      id: 2,
      code: 'XYZ',
      name: 'Custom Affiliation 2',
    },
  ];

  beforeEach(() => {
    user = getUser();
  });

  describe('useUserInfoFields', () => {
    const basicUserInfoFieldAmount = 6;

    it('should contain field captions in the right order', () => {
      const { result } = renderHook(() =>
        useUserInfoFields(user, customAffiliations),
      );
      expect(result.current?.map((field) => field.caption)).toStrictEqual([
        displayNameFieldCaption,
        usernameFieldCaption,
        localUsernameFieldCaption,
        emailFieldCaption,
        affiliationFieldCaption,
        ipAddressFieldCaption,
      ]);
    });

    it('should contain the correct field values', () => {
      const { result } = renderHook(() =>
        useUserInfoFields(user, customAffiliations),
      );
      expect(result.current?.map((field) => field.component)).toStrictEqual([
        'Chuck Norris',
        '63457666153@eduid.ch',
        'cnorris',
        'chuck.norris@roundhouse.kick',
        '-',
        '127.0.0.1',
      ]);
    });

    it('should return only basic user info fields if "manages" is "undefined"', () => {
      const { result } = renderHook(() => useUserInfoFields(user));
      expect(result.current).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should return only basic user info fields if "manages" is an empty object', () => {
      user.manages = {};
      const { result } = renderHook(() => useUserInfoFields(user));
      expect(result.current).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should return only basic user info fields if "manages" is an object with only empty arrays', () => {
      const { result } = renderHook(() => useUserInfoFields(user));
      expect(result.current).toHaveLength(basicUserInfoFieldAmount);
    });

    it('should also return one flag field if user has flags', () => {
      const { result } = renderHook(() =>
        useUserInfoFields({
          ...user,
          flags: ['funwithflags', 'anotheflag'],
        }),
      );
      expect(result.current).toHaveLength(basicUserInfoFieldAmount + 1);
    });
  });

  describe('Component', () => {
    let state: RootState;

    const apiCall = mockSuccessfulApiCall('listCustomAffiliations', []);

    beforeEach(() => {
      apiCall.mockClear();
      state = {
        ...getInitialState(),
        auth: {
          ...initialState,
          user: getUser(),
        },
      };
    });

    it('should display caption and component of fields', async () => {
      render(
        <Provider store={makeStore(undefined, state as RootState)}>
          <Profile />
        </Provider>,
      );

      expect(apiCall).toHaveBeenCalledTimes(1);
      await screen.findByText(ipAddressFieldCaption);
      await screen.findByText(ip);
    });
  });
});
