import { createAuthState, createPgpKeyInfo } from '../../../factories';
import { getInitialState, makeStore } from '../../store';
import { hasActiveKey, isActive, PgpKeyInfoList } from './PgpKeyInfoList';
import { type PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import { render, screen } from '@testing-library/react';
import { mockSuccessfulApiCall } from '../../testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

const prettify = (key: PgpKeyInfo) => key.id;

describe('isActive', function () {
  it.each`
    status                                   | expected
    ${PgpKeyInfoStatusEnum.APPROVAL_REVOKED} | ${false}
    ${PgpKeyInfoStatusEnum.KEY_REVOKED}      | ${false}
    ${PgpKeyInfoStatusEnum.APPROVED}         | ${true}
    ${PgpKeyInfoStatusEnum.REJECTED}         | ${false}
    ${PgpKeyInfoStatusEnum.PENDING}          | ${true}
    ${PgpKeyInfoStatusEnum.DELETED}          | ${false}
  `('Should evaluate status $status to $expected', ({ status, expected }) => {
    expect(isActive(status)).toBe(expected);
  });
});

const activeKey = createPgpKeyInfo({ status: PgpKeyInfoStatusEnum.PENDING });
const inactiveKey = createPgpKeyInfo({
  status: PgpKeyInfoStatusEnum.REJECTED,
});
const withActiveKey = [activeKey, inactiveKey];
const withoutActiveKey = [inactiveKey];

describe('hasActiveKey', function () {
  it.each`
    itemList            | expected | humanReadableItemList
    ${[]}               | ${false} | ${[]}
    ${withActiveKey}    | ${true}  | ${withActiveKey.map(prettify)}
    ${withoutActiveKey} | ${false} | ${withoutActiveKey.map(prettify)}
  `(
    'Should evaluate itemList $humanReadableItemList to $expected',
    ({ itemList, expected }) => {
      expect(hasActiveKey(itemList)).toBe(expected);
    },
  );
});

describe('PgpKeyInfoList', function () {
  const retireKeyButtonLabel = 'retireKeyButton';

  it.each`
    itemList            | expected | humanReadableItemList
    ${[]}               | ${true}  | ${[]}
    ${withActiveKey}    | ${false} | ${withActiveKey.map(prettify)}
    ${withoutActiveKey} | ${true}  | ${withoutActiveKey.map(prettify)}
  `(
    'Should show ($expected) the add button when itemList is $humanReadableItemList',
    async ({ itemList, expected }) => {
      // given
      mockSuccessfulApiCall<PgpKeyInfo[]>('listPgpKeyInfos', itemList);
      const addButtonLabel = 'Add common:models.openPgpKey';

      // when
      const { unmount } = render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createAuthState(),
          })}
        >
          <PgpKeyInfoList />
        </Provider>,
      );

      // then
      if (expected) {
        expect(
          await screen.findByLabelText(addButtonLabel),
        ).toBeInTheDocument();
      } else {
        expect(screen.queryByLabelText(addButtonLabel)).not.toBeInTheDocument();
      }

      unmount();
    },
  );

  it.each`
    itemList            | expected | humanReadableItemList
    ${withActiveKey}    | ${true}  | ${withActiveKey.map(prettify)}
    ${withoutActiveKey} | ${false} | ${withoutActiveKey.map(prettify)}
  `(
    'Should show ($expected) the retireKeyButton when itemList is $humanReadableItemList',
    async function ({ itemList, expected }) {
      // given
      mockSuccessfulApiCall<PgpKeyInfo[]>('listPgpKeyInfos', itemList);

      // when
      const { unmount } = render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createAuthState(),
          })}
        >
          <PgpKeyInfoList />
        </Provider>,
      );

      // then
      if (expected) {
        expect(
          await screen.findByLabelText(retireKeyButtonLabel),
        ).toBeInTheDocument();
      } else {
        expect(
          screen.queryByLabelText(retireKeyButtonLabel),
        ).not.toBeInTheDocument();
      }

      unmount();
    },
  );

  it('Should show the dialog when clicking the retireKeyButton', async function () {
    // given
    mockSuccessfulApiCall<PgpKeyInfo[]>('listPgpKeyInfos', withActiveKey);
    const user = userEvent.setup();

    // when
    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createAuthState(),
        })}
      >
        <PgpKeyInfoList />
      </Provider>,
    );
    const retireKeyButton = await screen.findByLabelText(retireKeyButtonLabel);
    await user.click(retireKeyButton);

    // then
    expect(
      await screen.findByText('pgpKeyInfoList:dialogs.retireKey.text'),
    ).toBeInTheDocument();
  });
});
