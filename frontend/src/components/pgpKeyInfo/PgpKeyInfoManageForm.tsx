import { ADD_PGP_KEY_INFO, CALL } from '../../actions/actionTypes';
import { Alert, type AlertColor, Box, Typography } from '@mui/material';
import {
  FabButton,
  FormDialog,
  LabelledField,
  mustBeOneOf,
  pgpFingerprint,
  requestAction,
  required,
  SafeLink,
  useEnhancedForm,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  type PgpKeyInfo,
  type PgpKeyMetadata,
  PgpKeyMetadataStatusEnum,
  type UniquePgpKeyInfo,
} from '../../api/generated';
import React, { type ReactElement, useMemo, useState } from 'react';
import { Trans, useTranslation } from 'next-i18next';
import type { AuthState } from '../../reducers/auth';
import { FormFieldsContainer } from '../FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../../api/api';
import { I18nNamespace } from '../../i18n';
import { keyServer } from '../../config';
import type { RootState } from '../../store';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';

type KeyMetadata = {
  error?: string;
  keyEmail?: string;
  keyStatus?: PgpKeyMetadataStatusEnum;
  keyUserId?: string;
};

export type PgpKeyInfoManageFormProps = {
  onClose: () => void;
  pgpKeyInfo: Partial<PgpKeyInfo>;
};

type HelperTextEnum =
  | 'EXPIRED'
  | 'INITIAL'
  | 'INVALID'
  | 'NOT_FOUND'
  | 'NOT_VERIFIED'
  | 'REVOKED'
  | 'VERIFIED';

type HelperText = {
  severity?: AlertColor;
  text: ReactElement | string;
};

function useHelperTexts(): Record<HelperTextEnum, HelperText> {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_MANAGE_FORM);

  return {
    INITIAL: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.initial`),
      severity: 'info',
    },
    VERIFIED: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyVerified`),
      severity: 'warning',
    },
    NOT_VERIFIED: {
      text: (
        <Trans
          i18nKey={`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyNotVerified`}
          components={{
            keyServer: keyServer ? (
              <SafeLink href={keyServer} />
            ) : (
              <Typography variant="body2" display="inline" />
            ),
          }}
        />
      ),
      severity: 'error',
    },
    REVOKED: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyRevoked`),
      severity: 'error',
    },
    EXPIRED: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyExpired`),
      severity: 'error',
    },
    NOT_FOUND: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyNotFound`),
      severity: 'error',
    },
    INVALID: {
      text: t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:helpers.keyInvalid`),
      severity: 'error',
    },
  };
}

const StyledFabButton = styled(FabButton)(({ theme }) => ({
  marginLeft: theme.spacing(1),
}));

export const PgpKeyInfoManageForm = ({
  pgpKeyInfo,
  onClose,
}: PgpKeyInfoManageFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.PGP_KEY_INFO_MANAGE_FORM);
  const form = useEnhancedForm<PgpKeyInfo>({ mode: 'onChange' });
  const isSubmitting = useSelector<RootState, boolean>(
    (state) => state.pgpKeyInfo.isSubmitting,
  );
  const dispatch = useReduxDispatch();
  const [keyMetadata, setKeyMetadata] = useState<KeyMetadata>();
  const { user } = useSelector<RootState, AuthState>((state) => state.auth);
  const userEmails = useMemo(() => {
    if (!user) {
      return [];
    }
    if (user.profile?.emails) {
      return user.profile.emails.split(',');
    }
    if (user.email) {
      return [user.email];
    }
    return [];
  }, [user]);
  const fingerprintFieldName = 'fingerprint';
  const helperTexts = useHelperTexts();
  const [helperText, setHelperText] = useState<HelperText>(helperTexts.INITIAL);
  const watchFingerprint = form.watch(fingerprintFieldName);
  const [isFetchingKeyMetadata, setIsFetchingKeyMetadata] =
    useState<boolean>(false);
  const uniqueCheck = async (value: Omit<UniquePgpKeyInfo, 'id'>) =>
    generatedBackendApi.uniquePgpKeyInfo({
      uniquePgpKeyInfo: { fingerprint: value.fingerprint },
    });

  function submit(pgpKeyInfo: PgpKeyInfo) {
    const onSuccessAction = {
      type: CALL,
      callback: onClose,
    };
    dispatch(
      requestAction(
        ADD_PGP_KEY_INFO,
        {
          pgpKeyInfo,
        },
        onSuccessAction,
      ),
    );
  }

  async function onSearchClick() {
    form.clearErrors('keyEmail');
    const { fingerprint: formFingerprint } = form.getValues();
    setIsFetchingKeyMetadata(true);
    generatedBackendApi
      .metadataPgpKeyMetadata({
        pgpKeyMetadata: { fingerprint: formFingerprint },
      })
      .then(
        ({
          fingerprint,
          email,
          userId,
          status,
          statusInfo,
          error,
        }: PgpKeyMetadata) => {
          const keyMetadata: KeyMetadata = {
            keyEmail: email,
            keyUserId: userId,
            keyStatus: status,
          };
          setKeyMetadata(keyMetadata);
          if (error) {
            setHelperText({ text: error, severity: 'error' });
          } else if (!fingerprint) {
            setHelperText(helperTexts.NOT_FOUND);
          } else {
            switch (status) {
              case PgpKeyMetadataStatusEnum.VERIFIED:
                setHelperText(helperTexts.VERIFIED);
                break;
              case PgpKeyMetadataStatusEnum.NONVERIFIED:
                setHelperText(helperTexts.NOT_VERIFIED);
                break;
              case PgpKeyMetadataStatusEnum.EXPIRED:
                setHelperText(helperTexts.EXPIRED);
                break;
              case PgpKeyMetadataStatusEnum.REVOKED:
                setHelperText(helperTexts.REVOKED);
                break;
              case PgpKeyMetadataStatusEnum.INVALID:
              case undefined:
                setHelperText({
                  text: statusInfo
                    ? `${helperTexts.INVALID.text} ${statusInfo}`
                    : helperTexts.INVALID.text,
                  severity: helperTexts.INVALID.severity,
                });
            }
          }
        },
      )
      .finally(() => setIsFetchingKeyMetadata(false));
  }

  return (
    <FormProvider {...form}>
      <FormDialog
        open={!!pgpKeyInfo}
        title={t(`${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:title`)}
        onSubmit={submit}
        isSubmitting={isSubmitting || isFetchingKeyMetadata}
        onClose={onClose}
        confirmButtonProps={{
          disabled:
            !keyMetadata ||
            !!keyMetadata.error ||
            keyMetadata.keyStatus !== PgpKeyMetadataStatusEnum.VERIFIED,
        }}
        fullWidth={true}
      >
        <FormFieldsContainer>
          <Alert
            severity={helperText.severity}
            sx={{ marginBottom: 3, marginRight: 0 }}
          >
            {helperText.text}
          </Alert>
          <Box sx={{ display: 'flex', marginRight: 0 }}>
            <LabelledField
              name={fingerprintFieldName}
              type="text"
              label={t(
                `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.${fingerprintFieldName}`,
              )}
              fullWidth
              validations={[required, pgpFingerprint]}
              unique={uniqueCheck}
              autofocus={true}
            />
            <StyledFabButton
              icon={'search'}
              onClick={onSearchClick}
              size={'small'}
              aria-label={
                t(
                  `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:buttons.search`,
                ) as string
              }
              disabled={
                !watchFingerprint ||
                watchFingerprint.length === 0 ||
                !!form.getFieldState(fingerprintFieldName).error ||
                isFetchingKeyMetadata
              }
            />
          </Box>
          <LabelledField
            name="keyUserId"
            type="text"
            label={t(
              `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.keyUserId`,
            )}
            validations={[required]}
            fullWidth
            disabled
            // Both needed as `initialValues` wouldn't show up in the UI and
            // value wouldn't be submitted when confirming
            initialValues={keyMetadata}
            value={keyMetadata?.keyUserId ?? ''}
          />
          <LabelledField
            name="keyEmail"
            type="text"
            label={t(
              `${I18nNamespace.PGP_KEY_INFO_MANAGE_FORM}:captions.keyEmail`,
            )}
            fullWidth
            disabled
            // Both needed as `initialValues` wouldn't show up in the UI and
            // value wouldn't be submitted when confirming
            initialValues={keyMetadata}
            value={keyMetadata?.keyEmail ?? ''}
            validations={[required, mustBeOneOf(userEmails)]}
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
