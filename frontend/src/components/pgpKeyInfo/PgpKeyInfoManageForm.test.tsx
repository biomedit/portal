import { ConfirmLabel, mockConsoleError } from '@biomedit/next-widgets';
import {
  type PgpKeyInfo,
  type PgpKeyMetadata,
  PgpKeyMetadataStatusEnum,
} from '../../api/generated';
import { render, screen } from '@testing-library/react';
import { makeStore } from '../../store';
import { mockSuccessfulApiCall } from '../../testUtils';
import { PgpKeyInfoManageForm } from './PgpKeyInfoManageForm';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

describe('PgpKeyInfoManageForm', function () {
  function getSearchButton() {
    return screen.findByLabelText('pgpKeyInfoManageForm:buttons.search');
  }

  function getFingerprintLabelledField() {
    return screen.findByLabelText('pgpKeyInfoManageForm:captions.fingerprint');
  }

  const testEmail = 'chuck.norris@roundhouse.kick';
  const testUserId = 'Chuck Norris';
  const testFingerprint = '0123456789ABCDEF0123456789ABCDEF01234567';
  const keyMetadata = {
    email: testEmail,
    user_id: testUserId,
    fingerprint: testFingerprint,
  };

  let consoleMock: jest.SpyInstance;

  beforeEach(() => {
    // Because we are using `value` (controlled) for `LabelledField`
    // (which should keep uncontrolled)
    consoleMock = mockConsoleError();
    const initialValues: Partial<PgpKeyInfo> = { fingerprint: undefined };
    render(
      <Provider store={makeStore()}>
        <PgpKeyInfoManageForm
          pgpKeyInfo={initialValues}
          onClose={() => {
            return;
          }}
        />
      </Provider>,
    );
  });

  afterEach(() => {
    consoleMock.mockRestore();
  });

  it('Should show the initialHelperText at first', async function () {
    expect(
      await screen.findByText('pgpKeyInfoManageForm:helpers.initial'),
    ).toBeInTheDocument();
  });

  describe('Search button', function () {
    it.each`
      fingerprint                                           | enabled
      ${''}                                                 | ${false}
      ${'0123456789ABCDEF'}                                 | ${false}
      ${'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'} | ${false}
      ${'malformedMalformedMalformedMalformed0000'}         | ${false}
      ${testFingerprint}                                    | ${true}
    `(
      'Should allow ($enabled) clicking the Search button if the input is $fingerprint',
      async function ({ fingerprint, enabled }) {
        mockSuccessfulApiCall('uniquePgpKeyInfo', {});

        const user = userEvent.setup();
        const fingerprintLabelledField = await getFingerprintLabelledField();
        const searchButton = await getSearchButton();
        const otherLabelledField = await screen.findByLabelText(
          'pgpKeyInfoManageForm:captions.keyUserId',
        );

        // Search button should always be disabled at first
        expect(searchButton).toBeDisabled();

        await user.click(fingerprintLabelledField);
        if (fingerprint) {
          await user.keyboard(fingerprint);
        }

        // Trigger validations
        await user.click(otherLabelledField);

        if (enabled) {
          expect(searchButton).not.toBeDisabled();
        } else {
          expect(searchButton).toBeDisabled();
        }
      },
    );

    it.each`
      response | helperText
      ${{
  ...keyMetadata,
  status: PgpKeyMetadataStatusEnum.EXPIRED,
}} | ${'pgpKeyInfoManageForm:helpers.keyExpired'}
      ${{
  ...keyMetadata,
  status: PgpKeyMetadataStatusEnum.VERIFIED,
}} | ${'pgpKeyInfoManageForm:helpers.keyVerified'}
      ${{
  fingerprint: keyMetadata.fingerprint,
  status: PgpKeyMetadataStatusEnum.NONVERIFIED,
}} | ${'pgpKeyInfoManageForm:helpers.keyNotVerified'}
      ${{
  fingerprint: keyMetadata.fingerprint,
  status: PgpKeyMetadataStatusEnum.REVOKED,
}} | ${'pgpKeyInfoManageForm:helpers.keyRevoked'}
      ${{}}    | ${'pgpKeyInfoManageForm:helpers.keyNotFound'}
    `(
      'Should show $helperText if response is $response',
      async ({ response, helperText }) => {
        mockSuccessfulApiCall('uniquePgpKeyInfo', {});
        mockSuccessfulApiCall<PgpKeyMetadata>(
          'metadataPgpKeyMetadata',
          response,
        );

        const user = userEvent.setup();

        await user.click(await getFingerprintLabelledField());
        await user.keyboard(testFingerprint);
        await user.click(await getSearchButton());

        expect(await screen.findByText(helperText)).toBeInTheDocument();
      },
    );

    it('Empty key user ID is not tolerable', async function () {
      mockSuccessfulApiCall('uniquePgpKeyInfo', {});
      mockSuccessfulApiCall<PgpKeyMetadata>('metadataPgpKeyMetadata', {
        ...keyMetadata,
        userId: '',
        status: PgpKeyMetadataStatusEnum.VERIFIED,
      });

      const user = userEvent.setup();
      const keyEmailField = await screen.findByLabelText<HTMLInputElement>(
        'pgpKeyInfoManageForm:captions.keyEmail',
      );
      const keyUserIdField = await screen.findByLabelText<HTMLInputElement>(
        'pgpKeyInfoManageForm:captions.keyUserId',
      );

      await user.click(await getFingerprintLabelledField());
      await user.keyboard(testFingerprint);
      await user.click(await getSearchButton());

      expect(keyUserIdField.value).toBe('');
      expect(keyEmailField.value).toBe(testEmail);

      expect(screen.queryByText('Required')).not.toBeInTheDocument();
      user.click(screen.getByText(ConfirmLabel.CONFIRM));
      await screen.findByText('Required');
    });
  });
});
