import {
  CALL,
  LOAD_PGP_KEY_INFOS,
  RETIRE_PGP_KEY_INFO,
} from '../../actions/actionTypes';
import {
  ConfirmDialog,
  EnhancedTable,
  requestAction,
  Tooltip,
  type Unpacked,
  useDialogState,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { type PgpKeyInfo, PgpKeyInfoStatusEnum } from '../../api/generated';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
} from 'react';
import {
  usePgpKeyInfoColumns,
  usePgpKeyInfoManageForm,
} from './PgpKeyInfoHooks';
import { I18nNamespace } from '../../i18n';
import IconButton from '@mui/material/IconButton';
import KeyOff from '@mui/icons-material/KeyOff';
import type { RootState } from '../../store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export function isActive(status?: PgpKeyInfoStatusEnum): boolean {
  return (
    !!status &&
    (PgpKeyInfoStatusEnum.PENDING === status ||
      PgpKeyInfoStatusEnum.APPROVED === status)
  );
}

export function hasActiveKey(itemList: PgpKeyInfo[]): boolean {
  return itemList.some(({ status }: PgpKeyInfo) => status && isActive(status));
}

export const PgpKeyInfoList = (): ReactElement => {
  const { t } = useTranslation([
    I18nNamespace.LIST,
    I18nNamespace.COMMON,
    I18nNamespace.PGP_KEY_INFO_LIST,
  ]);
  const columns = usePgpKeyInfoColumns();
  const username = useSelector<RootState, string | undefined>(
    (state) => state.auth.user?.username,
  );
  const { isFetching, isSubmitting, itemList } = useSelector(
    (state: RootState) => state.pgpKeyInfo,
  );
  const canAdd: boolean = useMemo(() => !hasActiveKey(itemList), [itemList]);
  const dispatch = useReduxDispatch();
  useEffect(() => {
    dispatch(requestAction(LOAD_PGP_KEY_INFOS, { username }));
  }, [dispatch, username]);

  const { openFormDialog, pgpKeyInfoManageForm } = usePgpKeyInfoManageForm();

  const { item, setItem, onClose, open } =
    useDialogState<Unpacked<typeof itemList>>();

  const retireKeyButtonLabel = 'retireKeyButton';
  const retireKeyTitle = 'Retire Key';
  const retireKeyButtonFactory = useCallback(
    (item?: Unpacked<typeof itemList>) =>
      item && isActive(item.status) ? (
        <Tooltip key={retireKeyButtonLabel + item.id} title={retireKeyTitle}>
          <IconButton
            size={'large'}
            onClick={() => setItem(item)}
            aria-label={retireKeyButtonLabel}
          >
            <KeyOff />
          </IconButton>
        </Tooltip>
      ) : null,
    [retireKeyButtonLabel, setItem],
  );

  const onRetireButtonClick = (item: Unpacked<typeof itemList>) => {
    dispatch(
      requestAction(
        RETIRE_PGP_KEY_INFO,
        {
          id: String(item.id),
        },
        { type: CALL, callback: onClose },
      ),
    );
  };

  return (
    <>
      {item && (
        <ConfirmDialog
          title={retireKeyTitle}
          open={open}
          onConfirm={(isConfirmed) =>
            isConfirmed ? onRetireButtonClick(item) : onClose()
          }
          isSubmitting={isSubmitting}
          text={t(`${I18nNamespace.PGP_KEY_INFO_LIST}:dialogs.retireKey.text`)}
        />
      )}
      <EnhancedTable
        itemList={itemList}
        columns={columns}
        isFetching={isFetching}
        isSubmitting={isSubmitting}
        emptyMessage={
          t(`${I18nNamespace.LIST}:emptyMessage`, {
            model: t(`${I18nNamespace.COMMON}:models.openPgpKey_plural`),
          }) as string
        }
        canAdd={canAdd}
        onAdd={openFormDialog}
        addButtonLabel={
          t(`${I18nNamespace.COMMON}:models.openPgpKey`) as string
        }
        form={pgpKeyInfoManageForm}
        canDelete={false}
        inline
        additionalActionButtons={[retireKeyButtonFactory]}
      />
    </>
  );
};
