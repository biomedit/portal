import { fillTextboxes, type TextboxField } from '@biomedit/next-widgets';
import { fireEvent, render, screen } from '@testing-library/react';
import { getInitialState, makeStore } from '../store';
import type { $Keys } from 'utility-types';
import type { Contact } from '../api/generated';
import { ContactUs } from './ContactUs';
import { createStaffAuthState } from '../../factories';
import { mockSuccessfulApiCall } from '../testUtils';
import { Provider } from 'react-redux';
import React from 'react';

describe('ContactUs', () => {
  describe('Component', () => {
    const staffAuthState = createStaffAuthState();
    const firstName = staffAuthState.user?.firstName;
    const lastName = staffAuthState.user?.lastName;
    const email = staffAuthState.user?.email;

    const subject = 'Test Subject';
    const message =
      'Chuck Norris is not a test subject.\nChuck Norris is the test.';

    const getExpectedBody = (authenticated: boolean, sendCopy: boolean) => {
      const body: Contact = {
        subject,
        message,
        sendCopy,
      };
      if (!authenticated) {
        body['firstName'] = firstName;
        body['lastName'] = lastName;
        body['email'] = email ?? undefined;
      }
      return body;
    };

    type Field = {
      name: $Keys<Contact>;
      value: string;
    };

    async function assertSubmitSuccess(spy: jest.SpyInstance) {
      const submitBtn = await screen.findByText('submit');
      fireEvent.click(submitBtn);

      await screen.findByText('success.title');
      await screen.findByText('success.message');

      expect(spy).toHaveBeenCalledTimes(1);
    }

    describe('Authenticated', () => {
      beforeEach(() => {
        render(
          <Provider
            store={makeStore(undefined, {
              ...getInitialState(),
              auth: staffAuthState,
            })}
          >
            <ContactUs />
          </Provider>,
        );
      });

      it('should submit form and see a success confirmation', async () => {
        const apiCall = mockSuccessfulApiCall(
          'createContact',
          getExpectedBody(true, true),
        );
        apiCall.mockClear();

        const textboxes = await screen.findAllByRole('textbox');
        expect(textboxes).toHaveLength(2);

        // fill out text fields
        const fields: Field[] = [
          { name: 'subject', value: subject },
          { name: 'message', value: message },
        ];

        fillTextboxes<Contact>(textboxes, fields);

        // check sendCopy checkbox
        const sendCopy = await screen.findByRole('checkbox');
        expect(sendCopy).toHaveAttribute('name', 'sendCopy');
        fireEvent.click(sendCopy);

        await assertSubmitSuccess(apiCall);
      });
    });

    describe('Unauthenticated', () => {
      beforeEach(() => {
        render(
          <Provider store={makeStore()}>
            <ContactUs />
          </Provider>,
        );
      });

      it('should submit form and see a success confirmation', async () => {
        const apiCall = mockSuccessfulApiCall(
          'createContact',
          getExpectedBody(false, false),
        );
        apiCall.mockClear();

        const textboxes = await screen.findAllByRole('textbox');
        expect(textboxes).toHaveLength(5);

        // fill out text fields
        const fields: TextboxField<Contact>[] =
          firstName && lastName && email
            ? [
                { name: 'firstName', value: firstName },
                { name: 'lastName', value: lastName },
                { name: 'email', value: email },
                { name: 'subject', value: subject },
                { name: 'message', value: message },
              ]
            : [];

        fillTextboxes<Contact>(textboxes, fields);

        await assertSubmitSuccess(apiCall);
      });
    });
  });
});
