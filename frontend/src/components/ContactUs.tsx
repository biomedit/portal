import { Box, Typography } from '@mui/material';
import {
  CheckboxField,
  Form,
  LabelledField,
  LoadingButton,
  required,
  useEnhancedForm,
} from '@biomedit/next-widgets';
import { contactName, contactShortName } from '../config';
import React, { type ReactElement, useState } from 'react';
import type { Contact } from '../api/generated';
import { FormProvider } from 'react-hook-form';
import { generatedBackendApi } from '../api/api';
import { I18nNamespace } from '../i18n';
import type { RootState } from '../store';
import { styled } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

const StyledLabelledField = styled(LabelledField)(({ theme }) => ({
  marginTop: theme.spacing(1),
  marginBottom: theme.spacing(0.5),
}));

const formId = 'contact-form';

export const ContactUs = (): ReactElement => {
  const [success, setSuccess] = useState<boolean>(false);

  const { t } = useTranslation(I18nNamespace.CONTACT_US);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const isAuthenticated = useSelector<RootState, boolean>(
    (state) => state.auth.isAuthenticated,
  );

  const form = useEnhancedForm<Contact>();

  function handleSuccess() {
    setSuccess(true);
    form.reset();
  }

  async function submit(contact: Contact) {
    setIsSubmitting(true);
    await generatedBackendApi
      .createContact({ contact })
      .then(handleSuccess)
      .finally(() => {
        setIsSubmitting(false);
      });
  }

  if (success) {
    return (
      <>
        <Typography variant={'h4'}>{t('success.title')}</Typography>
        <br />
        <Typography variant={'body1'}>{t('success.message')}</Typography>
      </>
    );
  }

  return (
    <>
      <Typography variant="body2">
        {t('description', { contactName, contactShortName })}
      </Typography>
      <FormProvider {...form}>
        <Form<Contact> id={formId} onSubmit={submit}>
          <Box>
            {!isAuthenticated && (
              <>
                <StyledLabelledField
                  name="firstName"
                  type="text"
                  label={t('captions.firstName')}
                  validations={required}
                  fullWidth
                  variant="outlined"
                />
                <StyledLabelledField
                  name="lastName"
                  type="text"
                  label={t('captions.lastName')}
                  validations={required}
                  fullWidth
                  variant="outlined"
                />
                <StyledLabelledField
                  name="email"
                  type="text"
                  label={t('captions.email')}
                  validations={required}
                  fullWidth
                  variant="outlined"
                />
              </>
            )}
            <StyledLabelledField
              name="subject"
              type="text"
              label={t('captions.subject')}
              validations={required}
              fullWidth
              variant="outlined"
            />
            <StyledLabelledField
              name="message"
              type="text"
              label={t('captions.message')}
              validations={required}
              fullWidth
              variant="outlined"
              multiline
              rows={4}
            />
            <CheckboxField name="sendCopy" label={t('captions.sendCopy')} />
          </Box>
        </Form>
        <LoadingButton
          type="submit"
          form={formId}
          color="primary"
          variant="outlined"
          loading={isSubmitting}
        >
          {t('submit')}
        </LoadingButton>
      </FormProvider>
    </>
  );
};
