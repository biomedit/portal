import {
  ADD_SSH_PUBLIC_KEY,
  CALL,
  LOAD_PROJECTS,
  resetSshInvalidMsgAction,
} from '../actions/actionTypes';
import {
  type BaseReducerState,
  ConfirmLabel,
  FormDialog,
  LabelledField,
  requestAction,
  required,
  SelectField,
  useEnhancedForm,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import type { Project, SSHPublicKey } from '../api/generated';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { FormFieldsContainer } from './FormFieldsContainer';
import { FormProvider } from 'react-hook-form';
import { I18nNamespace } from '../i18n';
import type { IdRequired } from '../global';
import type { RootState } from '../store';
import type { SSHPublicKeyState } from '../reducers/sshPublicKey';
import { useMySshSupportedProjectChoices } from './choice';
import { useSelector } from 'react-redux';
import { useTranslation } from 'next-i18next';

export function useSSHPublicKeyForm() {
  const newSSHPublicKey = {
    key: undefined,
    project: undefined,
  };
  const { closeFormDialog, data, ...rest } =
    useFormDialog<Partial<SSHPublicKey>>(newSSHPublicKey);
  const sshPublicKeyForm = data && (
    <SSHPublicKeyForm sshPublicKey={data} onClose={closeFormDialog} />
  );
  return {
    sshPublicKeyForm,
    ...rest,
  };
}

interface SSHPublicKeyFormProps {
  sshPublicKey: Partial<SSHPublicKey>;
  onClose: () => void;
}

export const SSHPublicKeyForm = ({
  sshPublicKey,
  onClose,
}: SSHPublicKeyFormProps): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);

  const dispatch = useReduxDispatch();

  const { itemList: projects, isFetching: isFetchingProjects } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Project>>
  >((state) => state.project);

  const { isSubmitting, invalidMsg } = useSelector<
    RootState,
    SSHPublicKeyState
  >((state) => state.sshPublicKey);

  const [invalidKey, setInvalidKey] = useState<string>();

  const form = useEnhancedForm<SSHPublicKey>();

  const keyField = 'key';
  const watchKey = form.watch(keyField);

  const fieldError = useMemo(() => {
    // `invalidKey` NOT set yet, BUT we got an invalid message from the server
    if (!invalidKey && !!invalidMsg) {
      setInvalidKey(watchKey);
    }
    // Input has changed and does NOT equal `invalidKey`
    if (invalidKey && watchKey !== invalidKey) {
      return;
    }
    if (invalidMsg) {
      return {
        type: 'validate',
        message: invalidMsg,
      };
    }
    return;
  }, [invalidMsg, invalidKey, watchKey]);

  const closeForm = useCallback(() => {
    setInvalidKey(undefined);
    dispatch(resetSshInvalidMsgAction());
    onClose();
  }, [dispatch, onClose]);

  function submit(sshPublicKey: SSHPublicKey) {
    const onSuccessAction = { type: CALL, callback: closeForm };
    // cast the userId to a number, because the hidden field returns a string
    dispatch(
      requestAction(
        ADD_SSH_PUBLIC_KEY,
        {
          sSHPublicKey: sshPublicKey,
        },
        onSuccessAction,
      ),
    );
  }

  useEffect(() => {
    dispatch(requestAction(LOAD_PROJECTS));
  }, [dispatch]);

  return (
    <FormProvider {...form}>
      <FormDialog
        fullWidth={true}
        maxWidth={'lg'}
        title={'SSH Public Key'}
        open={!!sshPublicKey}
        onSubmit={submit}
        onClose={closeForm}
        confirmLabel={ConfirmLabel.SAVE}
        isSubmitting={isSubmitting}
        noValidate
      >
        <FormFieldsContainer>
          <SelectField
            name="project"
            label={t('models.project')}
            isLoading={isFetchingProjects}
            required
            choices={useMySshSupportedProjectChoices(projects)}
          />
          <LabelledField
            name={keyField}
            type="text"
            label={t('models.sshKey')}
            fieldError={fieldError}
            validations={required}
            multiline
            rows={4}
            fullWidth
          />
        </FormFieldsContainer>
      </FormDialog>
    </FormProvider>
  );
};
