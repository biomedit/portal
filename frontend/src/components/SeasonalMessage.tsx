import {
  type EffectRef,
  EffectsOverlay,
} from '../widgets/effects/EffectsOverlay';
import { isBetweenIgnoreYear, isEaster } from '@biomedit/next-widgets';
import React, { type ReactElement, useCallback, useMemo, useRef } from 'react';
import { EffectCommand } from '../widgets/effects';
import { I18nNamespace } from '../i18n';
import { styled } from '@mui/material/styles';
import { useTranslation } from 'next-i18next';

const EasterEgg = styled('span')(() => ({
  cursor: 'pointer',
}));

declare type MessageProps = {
  easterEggEffect: EffectCommand;
  easterEggEmoji: string;
  message: string;
};

function Message({
  message,
  easterEggEmoji,
  easterEggEffect,
}: MessageProps): ReactElement {
  const { t } = useTranslation(I18nNamespace.HOME);
  const effectRef = useRef<EffectRef>(null);

  const onEasterEggClick = useCallback(() => {
    effectRef.current?.start(easterEggEffect);
  }, [easterEggEffect]);

  return (
    <>
      <EffectsOverlay ref={effectRef} />
      <h3>
        {t(message)}
        <>
          {' '}
          <EasterEgg onClick={onEasterEggClick}>{t(easterEggEmoji)}</EasterEgg>
        </>
      </h3>
    </>
  );
}

export const SeasonalMessage = (): ReactElement => {
  const christmas = useMemo(() => {
    return isBetweenIgnoreYear(12, 1, 12, 31);
  }, []);

  const easter = useMemo(() => {
    return isEaster();
  }, []);

  const newYear = useMemo(() => {
    return isBetweenIgnoreYear(1, 1, 1, 6);
  }, []);

  const nationalDay = useMemo(() => {
    return isBetweenIgnoreYear(8, 1, 8, 1);
  }, []);

  const halloween = useMemo(() => {
    return isBetweenIgnoreYear(10, 25, 10, 31);
  }, []);

  const valentinesDay = useMemo(() => {
    return isBetweenIgnoreYear(2, 14, 2, 14);
  }, []);

  const singlesAwarenessDay = useMemo(() => {
    return isBetweenIgnoreYear(2, 15, 2, 15);
  }, []);

  return (
    <>
      {easter && (
        <Message
          message={'seasonal.easter'}
          easterEggEmoji={'seasonal.easterEgg'}
          easterEggEffect={EffectCommand.CONFETTI}
        />
      )}
      {nationalDay && (
        <Message
          message={'seasonal.nationalDay'}
          easterEggEmoji={'seasonal.fireworks'}
          easterEggEffect={EffectCommand.FIREWORKS}
        />
      )}
      {halloween && (
        <Message
          message={'seasonal.halloween'}
          easterEggEmoji={'seasonal.ghost'}
          easterEggEffect={EffectCommand.SPACEINVADERS}
        />
      )}
      {christmas && (
        <Message
          message={'seasonal.christmas'}
          easterEggEmoji={'seasonal.gift'}
          easterEggEffect={EffectCommand.SNOWFALL}
        />
      )}
      {newYear && (
        <Message
          message={'seasonal.newYear'}
          easterEggEmoji={'seasonal.fireworks'}
          easterEggEffect={EffectCommand.FIREWORKS}
        />
      )}
      {valentinesDay && (
        <Message
          message={'seasonal.valentinesDay'}
          easterEggEmoji={'seasonal.hearts'}
          easterEggEffect={EffectCommand.HEARTS}
        />
      )}
      {singlesAwarenessDay && (
        <Message
          message={'seasonal.singlesAwarenessDay'}
          easterEggEmoji={'seasonal.cloudWithRain'}
          easterEggEffect={EffectCommand.RAINFALL}
        />
      )}
    </>
  );
};
