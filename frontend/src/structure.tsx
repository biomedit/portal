import {
  AdminPanelSettings,
  Apartment,
  Feedback,
  GridOn,
  Handshake,
  Home,
  Hub,
  Logout,
  Work,
} from '@mui/icons-material';
import {
  type CollapsibleStructureItem,
  isBetweenIgnoreYear,
  isCollapsibleStructureItem,
  type StructureItem,
  type SubItem,
} from '@biomedit/next-widgets';
import React, { useMemo } from 'react';
import type { $Keys } from 'utility-types';
import { Avatar } from '@mui/material';
import { getFullName } from './utils';
import { logoutUrl } from './api/api';
import type { PagePermissions } from './permissions';
import { termsOfUseEnabled } from './config';
import type { Userinfo } from './api/generated';

export type PortalStructureItem = StructureItem & {
  permission?: $Keys<PagePermissions>;
};

export type PortalSubItem = SubItem & { permission?: $Keys<PagePermissions> };

export type PortalCollapsibleStructureItem =
  CollapsibleStructureItem<PortalStructureItem> & {
    permission?: $Keys<PagePermissions>;
  };

export function toInitials(user: Userinfo | null, isChristmas: boolean) {
  function userToText(): string | undefined {
    if (isChristmas) {
      return '🎅';
    }
    if (user?.firstName && user?.lastName) {
      return getFullName(user);
    }
    if (user?.profile?.localUsername) {
      return user.profile.localUsername;
    }
    if (user?.username) {
      return user?.username;
    }
    return undefined;
  }

  const text = userToText();

  const parts = (text && text?.length ? text : 'U').split(' ');
  if (parts.length > 1) {
    return parts[0][0] + parts[1][0];
  }
  return parts[0].substring(0, 2);
}

export const filterByPermission = (
  structureItems: (PortalCollapsibleStructureItem | PortalStructureItem)[],
  pagePermissions: PagePermissions,
): (PortalStructureItem | PortalCollapsibleStructureItem)[] => {
  return structureItems
    .map((item) => {
      return isCollapsibleStructureItem(item)
        ? {
            ...item,
            subItems: filterByPermission(item.subItems, pagePermissions),
          }
        : item;
    })
    .filter((item) =>
      isCollapsibleStructureItem(item)
        ? item.subItems.length
        : !item.permission ||
          Object.values(pagePermissions[item.permission]).some(
            (permission) => permission,
          ),
    );
};

export const useStructureItems = (
  pagePermissions: PagePermissions,
  user: Userinfo | null,
): (CollapsibleStructureItem<PortalSubItem> | StructureItem)[] => {
  const isChristmas = useMemo(() => isBetweenIgnoreYear(12, 1, 12, 31), []);

  const structureItems: (
    | PortalCollapsibleStructureItem
    | PortalStructureItem
  )[] = useMemo(() => {
    const administrationItems: PortalSubItem[] = [
      {
        title: 'Users',
        menu: { link: '/users' },
        permission: 'user',
      },
      {
        title: 'Groups',
        menu: { link: '/groups' },
        permission: 'group',
      },
      {
        title: 'Flags',
        menu: { link: '/flags' },
        permission: 'flag',
      },
      {
        title: 'OpenPGP Keys',
        menu: { link: '/open-pgp-keys' },
        permission: 'openPgpKey',
      },
    ];

    const structureItems: (
      | PortalCollapsibleStructureItem
      | PortalStructureItem
    )[] = [
      {
        title: 'Home',
        menu: { link: '/', icon: <Home /> },
      },
      {
        title: 'Profile',
        menu: {
          link: '/profile',
          icon: (
            <Avatar
              sx={{
                width: 24,
                height: 24,
                fontSize: 12,
              }}
            >
              {toInitials(user, isChristmas)}
            </Avatar>
          ),
        },
      },
      {
        title: 'Admin',
        menu: { icon: <AdminPanelSettings /> },
        subItems: administrationItems,
      },
      {
        title: 'Nodes',
        menu: { link: '/nodes', icon: <Hub /> },
        permission: 'node',
      },
      {
        title: 'Data Providers',
        menu: { link: '/data-providers', icon: <Apartment /> },
        permission: 'dataProvider',
      },
      {
        title: 'Projects',
        menu: { link: '/projects', icon: <GridOn /> },
        permission: 'project',
      },
      {
        title: 'Data Transfers',
        menu: { link: '/data-transfers', icon: <Work /> },
        permission: 'dataTransfer',
      },
      ...(termsOfUseEnabled
        ? ([
            {
              title: 'Terms of Use',
              menu: { link: '/terms-of-use', icon: <Handshake /> },
            },
          ] as PortalStructureItem[])
        : []),
      {
        title: 'Contact Us',
        menu: { link: '/contact', icon: <Feedback /> },
      },
      {
        title: 'Logout',
        menu: { link: logoutUrl, icon: <Logout /> },
      },
    ];

    return structureItems;
  }, [isChristmas, user]);

  return useMemo(
    () => filterByPermission(structureItems, pagePermissions),
    [pagePermissions, structureItems],
  );
};
