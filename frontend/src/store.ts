import {
  applyMiddleware,
  legacy_createStore,
  type Middleware,
  type Reducer,
  type Store,
} from 'redux';
import { type Context, createWrapper } from 'next-redux-wrapper';
import { composeWithDevToolsLogOnlyInProduction } from '@redux-devtools/extension';
import createSagaMiddleware from 'redux-saga';
import { reducers } from './reducers';
import { rootSaga } from './actions/sagas';

export type RootState = ReturnType<typeof reducers>;

export function makeStore(
  _context?: Context,
  preloadedState?: RootState,
): Store {
  const sagaMiddleware = createSagaMiddleware();

  const enhancers = composeWithDevToolsLogOnlyInProduction(
    applyMiddleware(sagaMiddleware as Middleware),
  );

  const store = preloadedState
    ? legacy_createStore(reducers as Reducer, preloadedState, enhancers)
    : legacy_createStore(reducers as Reducer, enhancers);

  sagaMiddleware.run(rootSaga);

  return store;
}

export function getInitialState(): RootState {
  // @ts-expect-error an action which isn't defined in any reducer was chosen on
  // purpose here, to make sure no reducers get triggered
  return reducers(undefined, { type: 'NOOP' });
}

export const wrapper = createWrapper<Store<RootState>>(makeStore);
