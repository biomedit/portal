import type { ProjectUsersInner, User, Userinfo } from './api/generated';
import { uniq } from 'lodash';

type UserProfileType = User | Userinfo | null;
type UserType = ProjectUsersInner | UserProfileType;
type EmailChoice = { label: string; value: string };

export const getFullName = (user: UserType) => {
  const firstName = user?.firstName;
  const lastName = user?.lastName;
  return (
    (firstName || lastName) &&
    (firstName ? firstName + ' ' : '') + (lastName ? lastName : '')
  );
};

export function getAffiliation(user: UserProfileType): string | undefined {
  return (user?.profile?.affiliation || '-').replace(',', ', ');
}

export function isUserInfo(
  userOrInfo: User | Userinfo,
): userOrInfo is Userinfo {
  return (
    !!userOrInfo &&
    ('manages' in userOrInfo ||
      'permissions' in userOrInfo ||
      'ipAddress' in userOrInfo)
  );
}

export function emailChoices(userInfo: UserProfileType): EmailChoice[] {
  if (!userInfo) {
    return [];
  }
  const emails =
    userInfo?.profile?.emails?.split(',') ||
    (userInfo.email && [userInfo.email]) ||
    [];
  return uniq(emails).map((email) => {
    return {
      label: email,
      value: email,
    };
  });
}

export const downloadFile = <T extends Blob | BlobPart>(
  filename: string,
  blob: Promise<T>,
  filetype?: string,
  callback?: () => void,
) => {
  const link = document.createElement('a');
  link.target = '_blank';
  link.download = filename;
  blob
    .then((text) => {
      link.href = window.URL.createObjectURL(
        new Blob([text], {
          type: filetype,
        }),
      );
      link.click();
      URL.revokeObjectURL(link.href);
    })
    .finally(() => (callback ? callback() : null));
};
