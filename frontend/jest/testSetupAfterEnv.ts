import '@testing-library/jest-dom';
import 'next';
import { LogLevel, mockI18n, onLog } from '@biomedit/next-widgets';
import failOnConsole from 'jest-fail-on-console';
/**
 * Importing next during test applies the built-in fetch polyfill by Next.js
 *
 * @see https://github.com/vercel/next.js/discussions/13678#discussioncomment-22383 How to use built-in fetch in tests?
 * @see https://nextjs.org/blog/next-9-4#improved-built-in-fetch-support Next.js Blog - Improved Built-in Fetch Support
 * @see https://jestjs.io/docs/en/configuration#setupfilesafterenv-array About setupFilesAfterEnv usage
 */

mockI18n();

failOnConsole();

jest.mock('@biomedit/next-widgets', () => ({
  ...jest.requireActual('@biomedit/next-widgets'),
  logger: jest.fn((namespace: string) => {
    const log = (data: unknown) =>
      console.error(
        'Log was made for namespace: ' +
          namespace +
          ' with data: ' +
          JSON.stringify(data),
      );
    return {
      [LogLevel.debug]: log,
      [LogLevel.error]: log,
      [LogLevel.http]: log,
      [LogLevel.info]: log,
      [LogLevel.silly]: log,
      [LogLevel.verbose]: log,
      [LogLevel.warn]: log,
    };
  }),
}));

// fail on logs, if logs are expected, `mockConsoleError` must be used
onLog((data) => console.error(data));
