declare module 'next-i18next.config.js' {
  interface I18nConfig {
    defaultLocale: string;
    locales: string[];
  }

  interface InterpolationConfig {
    format: (value: string, format?: string) => string;
  }

  interface NextI18NextConfig {
    i18n: I18nConfig;
    keySeparator?: string;
    interpolation?: InterpolationConfig;
    transpilePackages?: string[];
    serializeConfig?: boolean;
  }

  const nextI18NextConfig: NextI18NextConfig;
  export default nextI18NextConfig;
}
