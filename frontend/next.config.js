const { i18n } = require('./next-i18next.config');

module.exports = {
  poweredByHeader: false,
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback = {
        ...config.resolve.fallback,
        // Fixes npm packages that depend on `fs` module, like winston,
        // see https://github.com/vercel/next.js/issues/7755. Webpack5 no
        // longer polyfills Node.js core modules automatically.
        fs: false,
      };
      // Setting `resolve.alias` to `false` will tell webpack to ignore a module.
      // `msw/node` is a server-only module that exports methods not available in
      // the `browser`.
      config.resolve.alias = {
        ...config.resolve.alias,
        'msw/node': false,
      };
    }
    return config;
  },
  i18n,
  reactStrictMode: true,
  typescript: {
    ignoreBuildErrors: false,
  },
  eslint: {
    // Make sure that ESLint is run as part of the CI
    ignoreDuringBuilds: true,
  },
  images: {
    remotePatterns: process.env.NEXT_PUBLIC_IMAGES_DOMAINS.split(',').map(
      (domain) => {
        return {
          hostname: domain,
        };
      },
    ),
  },
};
