import tsEslint from 'typescript-eslint';
import prettier from 'eslint-config-prettier';
import jestPlugin from 'eslint-plugin-jest';
import prettierPlugin from 'eslint-plugin-prettier/recommended';
import reactHooksPlugin from 'eslint-plugin-react-hooks';
import reactPlugin from 'eslint-plugin-react';
import storybookPlugin from 'eslint-plugin-storybook';
import jsEslint from '@eslint/js';

const tsConfig = tsEslint.config({
  extends: [
    jsEslint.configs.recommended,
    ...tsEslint.configs.strict,
    prettier,
    jestPlugin.configs['flat/recommended'],
    prettierPlugin,
  ],
  files: ['**/*.js', '**/*.jsx', '**/*.ts', '**/*.tsx'],
  ignores: [
    '**/generated/**',
    '.next/**',
    '.storybook/*',
    'jest.config.js',
    'coverage/**',
    'next.config.js',
    'next-i18next.config.js',
    'storybook-static',
  ],
  languageOptions: {
    parser: tsEslint.parser,
    parserOptions: {
      project: './tsconfig.json',
    },
  },
  plugins: {
    react: reactPlugin,
    'react-hooks': reactHooksPlugin,
    storybook: storybookPlugin,
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    '@typescript-eslint/no-deprecated': 'error',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-unused-vars': [
      'error',
      { caughtErrorsIgnorePattern: '^_' },
    ],
    '@typescript-eslint/switch-exhaustiveness-check': 'error',
    'jest/no-identical-title': 'off',
    'jest/no-conditional-expect': 'off',
    'jest/expect-expect': 'off',
    'linebreak-style': ['error', 'unix'],
    'no-cond-assign': ['error', 'always'],
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'no-nested-ternary': 'error',
    // Use default rules of react and react-hooks. Adding rules in this way is
    // a hack needed because the plugin does not support ESlint 9.x.
    ...reactPlugin.configs.recommended.rules,
    ...reactHooksPlugin.configs.recommended.rules,
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'react/prop-types': 'off',
    'react/no-array-index-key': 'error',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'max-len': [
      'error',
      {
        comments: 100,
        code: 99999999, // handled by prettier
        ignorePattern: 'eslint-disable', // comments to disable eslint cannot be shortened
        ignoreUrls: true,
      },
    ],
    quotes: [
      'warn',
      'single',
      { avoidEscape: true, allowTemplateLiterals: true },
    ],
    semi: ['warn', 'always'],
    'sort-imports': ['error', { ignoreCase: true }],
  },
});

export default tsEslint.config(...tsConfig);
