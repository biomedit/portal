const nextJest = require('next/jest');

const createJestConfig = nextJest();

// Any custom config you want to pass to Jest
const customJestConfig = {
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**',
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/widgets/effects/.*',
    '<rootDir>/src/api/generated/.*',
    '<rootDir>/src/api/config.ts',
    '<rootDir>/src/testUtils.ts',
    '<rootDir>/src/config.ts',
    '<rootDir>/src/i18n.ts',
    '<rootDir>/.next/.*',
    '<rootDir>/next-i18next.config.js',
    '<rootDir>/next.config.js',
    '<rootDir>/ts-check.js',
    '<rootDir>/.eslintrc.js',
    '<rootDir>/coverage/.*',
    '<rootDir>/packages/next-widgets/lib/api/config.ts',
    '<rootDir>/packages/next-widgets/lib/testUtils.ts',
    '<rootDir>/packages/next-widgets/lib/testId.ts',
    '<rootDir>/packages/next-widgets/lib/config.ts',
    '<rootDir>/packages/next-widgets/lib/i18n.ts',
  ],
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],
  transformIgnorePatterns: [
    '/next[/\\\\]dist/',
    '/node_modules/',
    '^.+\\.module\\.(css|sass|scss)$',
  ],
  moduleDirectories: [
    'node_modules',
    'src',
    'pages',
    'pagesTests',
    'packages/*',
  ],
  setupFiles: ['<rootDir>/jest/testSetup.ts'],
  setupFilesAfterEnv: ['<rootDir>/jest/testSetupAfterEnv.ts'],
  testEnvironment: '<rootDir>/jest/FixJSDOMEnvironment.ts',
  testEnvironmentOptions: {
    customExportConditions: [''],
  },
  moduleNameMapper: {
    // Jest 28 specific fix/hack.
    // See https://github.com/microsoft/accessibility-insights-web/pull/5421#issuecomment-1109168149
    '^uuid$': '<rootDir>/node_modules/uuid/dist/index.js',
    '@biomedit/next-widgets': '<rootDir>/packages/next-widgets',
    'next-i18next.config.js': '<rootDir>/next-i18next.config.js',
  },
  snapshotSerializers: ['@emotion/jest/serializer'],
};

// createJestConfig is exported in this way to ensure that next/jest can load the Next.js config which is async
module.exports = createJestConfig(customJestConfig);
