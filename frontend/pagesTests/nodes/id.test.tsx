import {
  createAuthState,
  createNode,
  createNodeAdminAuthState,
  createStaffAuthState,
} from '../../factories';
import { getInitialState, makeStore } from '../../src/store';
import { render, screen } from '@testing-library/react';
import { mockConsoleWarn } from '@biomedit/next-widgets';
import { mockSuccessfulApiCall } from '../../src/testUtils';
import NodeDetailPage from '../../pages/nodes/[id]';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
    query: { id: '1' },
    push: jest.fn(),
  }),
}));

describe('NodeDetailPage', () => {
  const id = 1;
  const staffAuthState = createStaffAuthState();
  const node = createNode({ id });
  const nodeAdminAuthState = createNodeAdminAuthState();

  it.each`
    nodeExists | auth                  | expected
    ${true}    | ${staffAuthState}     | ${true}
    ${true}    | ${nodeAdminAuthState} | ${true}
    ${true}    | ${createAuthState()}  | ${false}
    ${false}   | ${staffAuthState}     | ${false}
  `(
    `Should show the edit button ($expected) if:
    1) the retrieved node exists ($nodeExists)
    2) user is $auth`,
    async ({ nodeExists, auth, expected }) => {
      mockSuccessfulApiCall('retrieveNode', nodeExists ? node : {});

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth,
          })}
        >
          <NodeDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      if (nodeExists) {
        await screen.findAllByText(node.name);
      } else {
        await screen.findByText('list:emptyMessage');
      }

      if (expected) {
        await screen.findByText('Edit common:models.node');
      } else {
        expect(
          screen.queryByText('Edit common:models.node'),
        ).not.toBeInTheDocument();
      }
    },
  );

  it('Should show the node form when clicking the edit button', async () => {
    mockConsoleWarn();

    const user = userEvent.setup();
    const node = createNode({ id });
    mockSuccessfulApiCall('retrieveNode', node);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: staffAuthState,
        })}
      >
        <NodeDetailPage />
      </Provider>,
    );

    // Wait for page to be rendered
    await screen.findAllByText(node.name);

    const editButton = await screen.findByLabelText('Edit common:models.node');
    await user.click(editButton);

    await screen.getByTitle(`Edit common:models.node ${node.name}`);
  });
});
