import {
  createAuthState,
  createBatch,
  createDataProvider,
  createNode,
  createStaffAuthState,
  createUserinfo,
} from '../../factories';
import { getInitialState, makeStore } from '../../src/store';
import { render, screen } from '@testing-library/react';
import DataProviderDetailPage from '../../pages/data-providers/[id]';
import { mockConsoleWarn } from '@biomedit/next-widgets';
import { mockSuccessfulApiCall } from '../../src/testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
    query: { id: '1' },
    push: jest.fn(),
  }),
}));

describe('DataProviderDetailPage', () => {
  const id = 1;

  const nodes = createBatch(createNode, 2);
  const staffAuthState = createStaffAuthState();
  const dataProvider = createDataProvider({
    id,
    nodes: nodes.map((node) => node.code),
  });
  const dataProviderAdminAuthState = createAuthState({
    user: createUserinfo({ manages: { dataProviderAdmin: [dataProvider] } }),
  });

  it.each`
    dataProviderExists | auth                          | expected
    ${true}            | ${staffAuthState}             | ${true}
    ${true}            | ${dataProviderAdminAuthState} | ${true}
    ${true}            | ${createAuthState()}          | ${false}
    ${false}           | ${staffAuthState}             | ${false}
  `(
    `Should show the edit button ($expected) if:
    1) the retrieved data provider exists ($dataProviderExists)
    2) user is $auth`,
    async ({ dataProviderExists, auth, expected }) => {
      mockSuccessfulApiCall(
        'retrieveDataProvider',
        dataProviderExists ? dataProvider : {},
      );
      mockSuccessfulApiCall('listNodes', nodes);

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth,
          })}
        >
          <DataProviderDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      if (dataProviderExists) {
        await screen.findAllByText(dataProvider.name);
      } else {
        await screen.findByText('list:emptyMessage');
      }

      if (expected) {
        await screen.findByLabelText('Edit common:models.dataProvider');
      } else {
        expect(
          screen.queryByLabelText('Edit common:models.dataProvider'),
        ).not.toBeInTheDocument();
      }
    },
  );

  it('Should show the data provider form when clicking the edit button', async () => {
    mockConsoleWarn();

    const user = userEvent.setup();
    const dataProvider = createDataProvider({
      id,
      nodes: [nodes[1].code],
    });
    mockSuccessfulApiCall('retrieveDataProvider', dataProvider);
    mockSuccessfulApiCall('listNodes', nodes);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: staffAuthState,
        })}
      >
        <DataProviderDetailPage />
      </Provider>,
    );

    // Wait for page to be rendered
    await screen.findAllByText(dataProvider.name);

    const editButton = await screen.findByLabelText(
      'Edit common:models.dataProvider',
    );
    await user.click(editButton);

    await screen.getByTitle(
      `Edit common:models.dataProvider ${dataProvider.name}`,
    );
  });
});
