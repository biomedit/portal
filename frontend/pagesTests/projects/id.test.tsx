import {
  createAuthState,
  createPagePermissions,
  createProject,
  createProjectPagePermissions,
  createProjectPermissions,
  createProjectPermissionsEdit,
  createStaffAuthState,
} from '../../factories';
import { getInitialState, makeStore } from '../../src/store';
import { render, screen } from '@testing-library/react';
import { mockConsoleWarn } from '@biomedit/next-widgets';
import { mockSuccessfulApiCall } from '../../src/testUtils';
import ProjectDetailPage from '../../pages/projects/[id]';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
    query: { id: '1' },
    push: jest.fn(),
  }),
}));

describe('ProjectDetailPage', () => {
  const id = 1;

  it.each`
    projectExists | editPermissions   | archived | expected
    ${true}       | ${{ name: true }} | ${false} | ${true}
    ${true}       | ${{}}             | ${false} | ${false}
    ${true}       | ${{ name: true }} | ${true}  | ${false}
    ${false}      | ${{ name: true }} | ${false} | ${false}
  `(
    `Should show the edit button ($expected) if:
    1) the retrieved project exists ($projectExists)
    2) edit permissions are $editPermissions
    3) project is archived ($archived)`,
    async ({ projectExists, editPermissions, archived, expected }) => {
      const project = createProject({
        id,
        archived,
        permissions: createProjectPermissions({
          edit: createProjectPermissionsEdit(editPermissions),
        }),
      });
      mockSuccessfulApiCall('retrieveProject', projectExists ? project : {});

      render(
        <Provider store={makeStore()}>
          <ProjectDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      if (projectExists) {
        await screen.findAllByText(project.name);
      } else {
        await screen.findByText('list:emptyMessage');
      }

      if (expected) {
        await screen.findByLabelText('Edit common:models.project');
      } else {
        expect(
          screen.queryByLabelText('Edit common:models.project'),
        ).not.toBeInTheDocument();
      }
    },
  );

  it.each`
    projectExists | deletePermission | expected
    ${true}       | ${true}          | ${true}
    ${true}       | ${false}         | ${false}
    ${false}      | ${true}          | ${false}
  `(
    `Should show the delete button ($expected) if:
    1) the retrieved project exists ($projectExists)
    2) delete permission is $deletePermission`,
    async ({ projectExists, deletePermission, expected }) => {
      const project = createProject({ id });
      mockSuccessfulApiCall('retrieveProject', projectExists ? project : {});

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth: createAuthState({
              pagePermissions: createPagePermissions({
                project: createProjectPagePermissions({
                  del: deletePermission,
                }),
              }),
            }),
          })}
        >
          <ProjectDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      if (projectExists) {
        await screen.findAllByText(project.name);
      } else {
        await screen.findByText('list:emptyMessage');
      }

      if (expected) {
        await screen.findByText('Delete');
      } else {
        expect(screen.queryByText('Delete')).not.toBeInTheDocument();
      }
    },
  );

  it('Should show the project form when clicking the edit button', async () => {
    mockConsoleWarn();

    const user = userEvent.setup();
    const project = createProject({
      id,
      archived: false,
      permissions: createProjectPermissions({
        edit: createProjectPermissionsEdit({ name: true }),
      }),
    });
    mockSuccessfulApiCall('retrieveProject', project);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createStaffAuthState(),
        })}
      >
        <ProjectDetailPage />
      </Provider>,
    );

    // Wait for page to be rendered
    await screen.findAllByText(project.name);

    const editButton = await screen.findByLabelText(
      'Edit common:models.project',
    );
    await user.click(editButton);

    await screen.getByTitle(`Edit common:models.project ${project.name}`);
  });

  it.each`
    projectExists | archivePermission | archived | expected | archiveExpected
    ${true}       | ${true}           | ${false} | ${true}  | ${true}
    ${true}       | ${true}           | ${true}  | ${true}  | ${false}
    ${true}       | ${false}          | ${true}  | ${false} | ${undefined}
    ${false}      | ${true}           | ${true}  | ${false} | ${undefined}
  `(
    `Should show the (un)archive button ($expected) when:
    1) the retrieved project exists ($projectExists)
    2) archive permission is $archivePermission
    3) project is archived ($archived)`,
    async ({
      projectExists,
      archivePermission,
      archived,
      expected,
      archiveExpected,
    }) => {
      const project = createProject({
        id,
        archived,
        permissions: createProjectPermissions({
          archive: archivePermission,
        }),
      });
      mockSuccessfulApiCall('retrieveProject', projectExists ? project : {});

      render(
        <Provider store={makeStore()}>
          <ProjectDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      if (projectExists) {
        await screen.findAllByText(project.name);
      } else {
        await screen.findByText('list:emptyMessage');
      }

      if (expected) {
        if (archiveExpected) {
          await screen.findByTitle('actionButtons.archive');
        } else {
          await screen.findByTitle('actionButtons.unarchive');
        }
      } else {
        expect(
          screen.queryByTitle('actionButtons.archive'),
        ).not.toBeInTheDocument();
        expect(
          screen.queryByTitle('actionButtons.unarchive'),
        ).not.toBeInTheDocument();
      }
    },
  );

  it('Should show the archive dialog when clicking the archive button', async () => {
    mockConsoleWarn();

    const user = userEvent.setup();
    const project = createProject({
      id,
      archived: false,
      permissions: createProjectPermissions({
        archive: true,
      }),
    });
    mockSuccessfulApiCall('retrieveProject', project);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createStaffAuthState(),
        })}
      >
        <ProjectDetailPage />
      </Provider>,
    );

    // Wait for page to be rendered
    await screen.findAllByText(project.name);

    const archiveButton = await screen.findByTitle('actionButtons.archive');
    await user.click(archiveButton);

    await screen.findByText(`archiveDialog.title`);
  });
});
