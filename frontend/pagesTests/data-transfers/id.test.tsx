import {
  createDataTransfer,
  createDataTransferPermissions,
  createDataTransferPermissionsEdit,
  createStaffAuthState,
} from '../../factories';
import { getInitialState, makeStore } from '../../src/store';
import { mockConsoleError, mockConsoleWarn } from '@biomedit/next-widgets';
import { render, screen } from '@testing-library/react';
import DataTransferDetailPage from '../../pages/data-transfers/[id]';
import { mockSuccessfulApiCall } from '../../src/testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import userEvent from '@testing-library/user-event';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
    query: { id: '1' },
    push: jest.fn(),
  }),
}));

describe('DataTransferDetailPage', () => {
  const id = 1;

  it.each`
    dataTransferExists | editPermissions                                | projectArchived | expected
    ${true}            | ${{ dataSpecification: true, status: false }}  | ${false}        | ${true}
    ${false}           | ${{ dataSpecification: true, status: false }}  | ${false}        | ${false}
    ${false}           | ${{ dataSpecification: false, status: false }} | ${true}         | ${false}
    ${false}           | ${{ dataSpecification: false, status: false }} | ${false}        | ${false}
  `(
    `Should show the edit button ($expected) if:
    1) the retrieved data transfer exists ($dataTransferExists)
    2) edit permissions are $editPermissions
    3) project is archived ($projectArchived)`,
    async ({
      dataTransferExists,
      editPermissions,
      projectArchived,
      expected,
    }) => {
      const dataTransfer = createDataTransfer({
        id,
        projectArchived,
        permissions: createDataTransferPermissions({
          edit: createDataTransferPermissionsEdit(editPermissions),
        }),
      });
      mockSuccessfulApiCall(
        'retrieveDataTransfer',
        dataTransferExists ? dataTransfer : {},
      );

      render(
        <Provider store={makeStore()}>
          <DataTransferDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      await screen.findByText(`common:models.dataTransfer ${id}`);

      if (expected) {
        await screen.findByLabelText('Edit common:models.dataTransfer');
      } else {
        expect(
          screen.queryByLabelText('Edit common:models.dataTransfer'),
        ).not.toBeInTheDocument();
      }
    },
  );

  it.each`
    dataTransferExists | deletePermission | expected
    ${true}            | ${true}          | ${true}
    ${false}           | ${true}          | ${false}
    ${false}           | ${false}         | ${false}
  `(
    `Should show the delete button ($expected) if:
    1) the retrieved data transfer exists ($dataTransferExists)
    2) delete permission is $deletePermission`,
    async ({ dataTransferExists, deletePermission, expected }) => {
      const dataTransfer = createDataTransfer({
        id,
        permissions: createDataTransferPermissions({
          _delete: deletePermission,
        }),
      });
      mockSuccessfulApiCall(
        'retrieveDataTransfer',
        dataTransferExists ? dataTransfer : {},
      );

      render(
        <Provider store={makeStore()}>
          <DataTransferDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      await screen.findByText(`common:models.dataTransfer ${id}`);

      if (expected) {
        await screen.findByText('Delete');
      } else {
        expect(screen.queryByText('Delete')).not.toBeInTheDocument();
      }
    },
  );

  it('Should show the data transfer form when clicking the edit button', async () => {
    mockConsoleWarn();

    // The helperText on the requestor field triggers a console error
    mockConsoleError();

    const user = userEvent.setup();
    const dataTransfer = createDataTransfer({
      id,
      projectArchived: false,
      permissions: createDataTransferPermissions({
        edit: createDataTransferPermissionsEdit({ dataSpecification: true }),
      }),
    });
    mockSuccessfulApiCall('retrieveDataTransfer', dataTransfer);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: createStaffAuthState(),
        })}
      >
        <DataTransferDetailPage />
      </Provider>,
    );

    // Wait for page to be rendered
    await screen.findByText(`common:models.dataTransfer ${id}`);

    const editButton = await screen.findByLabelText(
      'Edit common:models.dataTransfer',
    );
    await user.click(editButton);

    await screen.findByText(`Edit Data Transfer ${id}`);
  });

  it('Should call the DELETE endpoint for data transfers when confirming deletion', async () => {
    const user = userEvent.setup();
    const dataTransfer = createDataTransfer({
      id,
      permissions: createDataTransferPermissions({ _delete: true }),
    });
    const deleteConfirmationText = `Delete ${id}`;

    mockConsoleWarn();
    mockSuccessfulApiCall('retrieveDataTransfer', dataTransfer);
    const deleteApiCall = mockSuccessfulApiCall('destroyDataTransfer', {});

    render(
      <Provider store={makeStore()}>
        <DataTransferDetailPage />
      </Provider>,
    );

    const deleteButton = await screen.findByText('Delete');
    await user.click(deleteButton);

    await screen.findByText(deleteConfirmationText);

    const confirmationTextInputField = await screen.findByRole('textbox');
    await user.type(confirmationTextInputField, deleteConfirmationText);

    const confirmButton = await screen.findByText('Confirm');
    expect(confirmButton).not.toBeDisabled();

    await user.click(confirmButton);
    expect(deleteApiCall).toHaveBeenCalled();
  });
});
