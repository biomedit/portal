import {
  createAuthState,
  createStaffAuthState,
  createUser,
} from '../../factories';
import { getInitialState, makeStore } from '../../src/store';
import { render, screen } from '@testing-library/react';
import { mockConsoleWarn } from '@biomedit/next-widgets';
import { mockSuccessfulApiCall } from '../../src/testUtils';
import { Provider } from 'react-redux';
import React from 'react';
import UserDetailPage from '../../pages/users/[id]';
import userEvent from '@testing-library/user-event';

jest.mock('next/router', () => ({
  useRouter: jest.fn().mockReturnValue({
    asPath: '/',
    query: { id: '1' },
    push: jest.fn(),
  }),
}));

describe('UserDetailPage', () => {
  const id = 1;
  const staffAuthState = createStaffAuthState();
  const user = createUser({ id });

  it.each`
    userExists | auth                 | expected
    ${true}    | ${staffAuthState}    | ${true}
    ${true}    | ${createAuthState()} | ${false}
    ${false}   | ${staffAuthState}    | ${false}
  `(
    `Should show the edit button ($expected) if:
    1) the retrieved user exists ($userExists)
    2) user is $auth`,
    async ({ userExists, auth, expected }) => {
      mockSuccessfulApiCall('retrieveUser', userExists ? user : {});
      mockSuccessfulApiCall('listProjects', []);

      render(
        <Provider
          store={makeStore(undefined, {
            ...getInitialState(),
            auth,
          })}
        >
          <UserDetailPage />
        </Provider>,
      );

      // Wait for page to be rendered
      if (userExists) {
        await screen.findAllByText(`${user.firstName} ${user.lastName}`);
      } else {
        await screen.findByText('list:emptyMessage');
      }

      if (expected) {
        await screen.findByLabelText('Edit common:models.user');
      } else {
        expect(
          screen.queryByLabelText('Edit common:models.user'),
        ).not.toBeInTheDocument();
      }
    },
  );

  it('Should show the user form when clicking the edit button', async () => {
    mockConsoleWarn();

    const mockUser = userEvent.setup();
    const user = createUser({ id });
    mockSuccessfulApiCall('retrieveUser', user);
    mockSuccessfulApiCall('listProjects', user);

    render(
      <Provider
        store={makeStore(undefined, {
          ...getInitialState(),
          auth: staffAuthState,
        })}
      >
        <UserDetailPage />
      </Provider>,
    );

    // Wait for page to be rendered
    await screen.findAllByText(`${user.firstName} ${user.lastName}`);

    const editButton = await screen.findByLabelText('Edit common:models.user');
    await mockUser.click(editButton);

    await screen.findByText(`userManageForm:title`);
  });
});
