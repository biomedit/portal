import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { DataProviderList } from '../../src/components/admin/DataProviderManage';
import { I18nNamespace } from '../../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';

export const DataProviderPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.dataProvider_plural`),
        link: '/data-providers',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`models.dataProvider_plural`)}`}
      content={DataProviderList}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.DATA_PROVIDER,
      I18nNamespace.DATA_TRANSFER,
      I18nNamespace.LIST,
      I18nNamespace.USER_LIST,
    ])),
  },
});

export default DataProviderPage;
