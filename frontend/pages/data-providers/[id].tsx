import {
  PageBase,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../_app';
import type { DataProvider } from '../../src/api/generated';
import { DataProviderDetail } from '../../src/components/admin/DataProviderDetail';
import { DetailedViewToolbar } from '../../src/widgets/DetailedViewToolbar';
import { I18nNamespace } from '../../src/i18n';
import { isStaff } from '../../src/components/selectors';
import { RETRIEVE_DATA_PROVIDER } from '../../src/actions/actionTypes';
import type { RootState } from '../../src/store';
import { useDataProviderForm } from '../../src/components/admin/DataProviderManageForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from './index';

export const DataProviderDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList, isSubmitting } = useSelector(
    (state: RootState) => state.dataProvider,
  );
  const dataProvider: DataProvider | undefined = itemList.find(
    (dataProvider) => dataProvider.id === parseInt(id),
  );
  // Show empty string if data provider's name is not known yet
  const title = useMemo(() => dataProvider?.name ?? '', [dataProvider]);
  const { openFormDialog, dataProviderForm } = useDataProviderForm();
  const staff = useSelector(isStaff);
  const auth = useSelector((state: RootState) => state.auth);
  const dataProviderAdminFor = auth.user?.manages?.dataProviderAdmin?.map(
    (dp) => dp.id,
  );
  const canEdit = useMemo(
    () =>
      !!dataProvider &&
      (staff ||
        (dataProviderAdminFor
          ? dataProviderAdminFor.includes(dataProvider.id)
          : false)),
    [dataProvider, dataProviderAdminFor, staff],
  );

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_DATA_PROVIDER, { id }));
  }, [dispatch, id]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.dataProvider_plural`),
        link: '/data-providers',
      },
      {
        text: title,
        link: `/data-providers/${id}`,
      },
    ],
    [id, t, title],
  );

  return (
    <PageBase
      appName={appName}
      title={title}
      content={DataProviderDetail}
      toolbar={() => (
        <DetailedViewToolbar
          canEdit={canEdit}
          canDelete={false}
          itemId={parseInt(id)}
          isSubmitting={isSubmitting}
          onEditClick={() => openFormDialog(dataProvider)}
          entityType={t(`${I18nNamespace.COMMON}:models.dataProvider`)}
          entityName={dataProvider?.name}
        >
          {dataProviderForm}
        </DetailedViewToolbar>
      )}
      props={{ dataProvider }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default DataProviderDetailPage;
