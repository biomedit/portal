import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement } from 'react';
import { appName } from './_app';
import { I18nNamespace } from '../src/i18n';
import { ProfileTabs } from '../src/components/Profile';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

export const ProfilePage = (): ReactElement => {
  const title = 'Profile';
  const breadcrumbs = [
    { text: appName, link: '/' },
    {
      text: title,
      link: '/profile',
    },
  ];

  return (
    <PageBase
      appName={appName}
      title={title}
      content={ProfileTabs}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.LIST,
      I18nNamespace.USER_INFO,
      I18nNamespace.PGP_KEY_INFO_LIST,
      I18nNamespace.PGP_KEY_INFO_MANAGE_FORM,
      I18nNamespace.TERMS_OF_USE,
    ])),
  },
});

export default ProfilePage;
