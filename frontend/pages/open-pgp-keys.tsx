import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from './_app';
import { I18nNamespace } from '../src/i18n';
import { PgpKeyInfoList } from '../src/components/admin/PgpKeyInfoManage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';

export const OpenPGPKeyPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.openPgpKey_plural`),
        link: '/open-pgp-keys',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`models.openPgpKey_plural`)}`}
      content={PgpKeyInfoList}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.LIST,
      I18nNamespace.PGP_KEY_INFO_LIST,
    ])),
  },
});

export default OpenPGPKeyPage;
