import { appWithTranslation, Trans } from 'next-i18next';
import { Box, Link } from '@mui/material';
import {
  CollapsibleNavItem,
  Container,
  EmailLink,
  ErrorBoundary,
  Footer,
  isClient,
  isCollapsibleStructureItem,
  logger,
  type LogListener,
  Nav,
  NavItem,
  offLog,
  onLog,
  requestAction,
  SkipLink,
  type StructureItem,
  type SubItem,
  ThemeProvider,
  ToastBar,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import { contactEmail, privacyLink, version } from '../src/config';
import { Provider, useSelector } from 'react-redux';
import React, { type ReactElement, useEffect, useMemo, useState } from 'react';
import { type RootState, wrapper } from '../src/store';
import type { AppProps } from 'next/app';
import type { AuthState } from '../src/reducers/auth';
import { CHECK_AUTH } from '../src/actions/actionTypes';
import Head from 'next/head';
import { I18nNamespace } from '../src/i18n';
import Image from 'next/image';
import { isbot } from 'isbot';
import nextI18NextConfig from 'next-i18next.config.js';
import { supportedBrowsers } from '../src/supportedBrowsers';
import { useRouter } from 'next/router';
import { UserProfileDialog } from '../src/components/UserProfileDialog';
import { useStructureItems } from '../src/structure';
import { winstonLogger } from '../src/logger';

export const unsupportedBrowserLogMessage = 'Browser is not supported!';
export const appName = 'BioMedIT Portal';

export function isMenuItemSelected({ menu }: StructureItem, pathname: string) {
  if (!menu?.link) {
    return false;
  }
  const isHome = menu.link == '/';
  return isHome ? menu.link == pathname : pathname.startsWith(menu.link);
}

export function App({ children }: { children?: ReactElement }): ReactElement {
  // See https://github.com/vercel/next.js/discussions/17443
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  const isSupportedBrowser: boolean | undefined = useMemo(() => {
    if (isClient()) {
      const userAgent = (window.navigator || navigator).userAgent;
      if (isbot(userAgent)) {
        // prevent bots from crawling contact email address
        return true;
      }
      return supportedBrowsers.test(userAgent);
    } else {
      return undefined;
    }
  }, []);

  const dispatch = useReduxDispatch();

  const { isFetching, isAuthenticated, pagePermissions, user } = useSelector<
    RootState,
    AuthState
  >((state) => state.auth);

  const structureItems = useStructureItems(pagePermissions, user);

  const { pathname } = useRouter();
  useEffect(() => {
    if (isSupportedBrowser && !isAuthenticated) {
      dispatch(requestAction(CHECK_AUTH));
    }
  }, [dispatch, isAuthenticated, isSupportedBrowser]);

  useEffect(() => {
    const log = winstonLogger();
    const listener: LogListener = (data) => {
      log.log(data);
    };
    onLog(listener);
    return () => {
      offLog(listener);
    };
  }, []);

  const log = logger('App');

  if (mounted && !isSupportedBrowser) {
    log.warn({ message: unsupportedBrowserLogMessage });
    return (
      <Trans
        i18nKey={`${I18nNamespace.COMMON}:unsupportedBrowser`}
        values={{ contactEmail }}
        components={{
          email: <EmailLink email={contactEmail ?? ''} />,
          ulist: <ul />,
          litem: <li />,
          break: <br />,
        }}
      />
    );
  }

  const subjectPrefix = `${appName}: `;

  return (
    <ErrorBoundary
      subjectPrefix={subjectPrefix}
      contactEmail={contactEmail ?? ''}
    >
      <ThemeProvider>
        {mounted && <SkipLink />}
        <div>
          <ToastBar
            subjectPrefix={subjectPrefix}
            contactEmail={contactEmail ?? ''}
          />
          {isAuthenticated && !isFetching && (
            <Box display="flex">
              <UserProfileDialog />
              <Nav>
                <Link
                  href={'https://www.biomedit.ch/'}
                  target={'_blank'}
                  rel={'noopener noreferrer'}
                >
                  <Image
                    src={'/biomedit_logo.svg'}
                    width="230"
                    height="59"
                    alt="BioMedIT Logo"
                    priority={true}
                  />
                </Link>
                {structureItems.map((item) =>
                  isCollapsibleStructureItem(item) ? (
                    <CollapsibleNavItem
                      key={'MenuItem-' + item.title}
                      primary={item.title}
                      href={'#'}
                      icon={item.menu?.icon}
                      items={item.subItems}
                      getSelected={(item: SubItem) =>
                        !!item.menu && isMenuItemSelected(item, pathname)
                      }
                    />
                  ) : (
                    <NavItem
                      selected={
                        !!item.menu && isMenuItemSelected(item, pathname)
                      }
                      href={item.menu?.link ?? '#'}
                      key={'MenuItem-' + item.title}
                      primary={item.title}
                      icon={item.menu?.icon}
                    />
                  ),
                )}
                <Footer
                  logoSrc="/sib_logo.svg"
                  appName={appName}
                  logoText="SIB Logo"
                  logoWidth="52"
                  logoHeight="40"
                  logoHref="https://www.sib.swiss/"
                  privacyHref={privacyLink}
                  version={version}
                />
              </Nav>
              <Container maxWidth={false}>
                <main id="main-content">{children}</main>
              </Container>
            </Box>
          )}
        </div>
      </ThemeProvider>
    </ErrorBoundary>
  );
}

const AppWrapper = ({ Component, ...rest }: AppProps): ReactElement => {
  const { store, props } = wrapper.useWrappedStore(rest);
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/favicon.ico" />
        <title>{appName}</title>
      </Head>
      <Provider store={store}>
        <App>
          <Component {...props.pageProps} />
        </App>
      </Provider>
    </>
  );
};

export default appWithTranslation(AppWrapper, nextI18NextConfig);
