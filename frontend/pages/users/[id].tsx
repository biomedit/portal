import {
  type FormUser,
  UserForm,
} from '../../src/components/admin/UserManageForm';
import {
  PageBase,
  requestAction,
  useFormDialog,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../_app';
import { DetailedViewToolbar } from '../../src/widgets/DetailedViewToolbar';
import { getFullName } from '../../src/utils';
import { I18nNamespace } from '../../src/i18n';
import { isStaff } from '../../src/components/selectors';
import { RETRIEVE_USER } from '../../src/actions/actionTypes';
import type { RootState } from '../../src/store';
import type { User } from '../../src/api/generated';
import { UserDetail } from '../../src/components/admin/UserDetail';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from './index';

export const UserDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList, isSubmitting } = useSelector(
    (state: RootState) => state.users,
  );
  // We can assume this is a valid FormUser as the only requirement is to have
  // the id property, which is true for a user returned by this find predicate.
  const user: FormUser | undefined = itemList.find(
    (user: User) => user.id === parseInt(id),
  );
  // Show empty string if user's name is not known yet
  const title = useMemo(
    () =>
      user
        ? getFullName(user) ||
          user?.profile?.localUsername ||
          user.username ||
          ''
        : '',
    [user],
  );
  const { closeFormDialog, data, openFormDialog } =
    useFormDialog<FormUser>(undefined);
  const userForm = useMemo(() => {
    if (data) {
      return <UserForm user={data} onClose={closeFormDialog} />;
    }
    return;
  }, [data, closeFormDialog]);
  const staff = useSelector(isStaff);
  const canEdit = useMemo(() => !!user && staff, [user, staff]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.user_plural`),
        link: '/users',
      },
      {
        text: title,
        link: `/users/${id}`,
      },
    ],
    [id, t, title],
  );

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_USER, { id, includeInactive: true }));
  }, [dispatch, id]);

  return (
    <PageBase
      appName={appName}
      title={title}
      content={UserDetail}
      toolbar={() => (
        <DetailedViewToolbar
          canEdit={canEdit}
          canDelete={false}
          itemId={parseInt(id)}
          isSubmitting={isSubmitting}
          onEditClick={() => openFormDialog(user)}
          entityType={t(`${I18nNamespace.COMMON}:models.user`)}
          entityName={user?.username}
        >
          {userForm}
        </DetailedViewToolbar>
      )}
      props={{ user }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default UserDetailPage;
