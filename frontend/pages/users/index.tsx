import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { I18nNamespace } from '../../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { UserList } from '../../src/components/admin/UserManage';
import { useTranslation } from 'react-i18next';

export const UserPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.user_plural`),
        link: '/users',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`models.user_plural`)}`}
      content={UserList}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.BOOLEAN,
      I18nNamespace.COMMON,
      I18nNamespace.LIST,
      I18nNamespace.USER_LIST,
      I18nNamespace.USER_MANAGE_FORM,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default UserPage;
