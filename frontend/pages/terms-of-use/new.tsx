import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { I18nNamespace } from '../../src/i18n';
import { PageBase } from '@biomedit/next-widgets';
import { TermsOfUseForm } from '../../src/components/termsOfUse/TermsOfUseForm';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from '../index';

export const NewTermsOfUsePage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.termsOfUse`),
        link: `/terms-of-use`,
      },
      {
        text: 'New',
        link: `/terms-of-use/new`,
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={'New Terms of Use'}
      content={TermsOfUseForm}
      breadcrumbs={breadcrumbs}
    />
  );
};

export default NewTermsOfUsePage;
