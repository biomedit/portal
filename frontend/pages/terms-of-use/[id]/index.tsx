import {
  LOAD_NODES,
  RETRIEVE_TERMS_OF_USE,
} from '../../../src/actions/actionTypes';
import {
  PageBase,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../../_app';
import { I18nNamespace } from '../../../src/i18n';
import type { Node } from '../../../src/api/generated';
import type { RootState } from '../../../src/store';
import { TermsOfUseDetail } from '../../../src/components/termsOfUse/TermsOfUseDetail';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from '../index';

export const TermsOfUseDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList } = useSelector((state: RootState) => state.termsOfUse);
  const termsOfUse = itemList.find(
    (termsOfUse) => termsOfUse.id === parseInt(id),
  );
  const { itemList: nodes } = useSelector((state: RootState) => state.nodes);
  const node: Node | undefined = termsOfUse
    ? nodes.find((node) => node.id === parseInt(termsOfUse.node.toString()))
    : undefined;
  // Show empty string if the terms of use name is not ready yet
  const title = useMemo(
    () =>
      termsOfUse?.version && node?.name
        ? `${node.name} ${termsOfUse.version}`
        : '',
    [node?.name, termsOfUse?.version],
  );

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_TERMS_OF_USE, { id }));
    dispatch(requestAction(LOAD_NODES));
  }, [dispatch, id]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.termsOfUse`),
        link: `/terms-of-use`,
      },
      {
        text: title,
        link: `/terms-of-use/${id}`,
      },
    ],
    [id, t, title],
  );

  return (
    <PageBase
      appName={appName}
      title={title}
      content={TermsOfUseDetail}
      props={{ termsOfUse }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default TermsOfUseDetailPage;
