import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { I18nNamespace } from '../../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { TermsOfUseTable } from '../../src/components/termsOfUse/TermsOfUseTable';
import { useTranslation } from 'react-i18next';

export const TermsOfUsePage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.termsOfUse`),
        link: `/terms-of-use`,
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={'Terms of Use'}
      content={TermsOfUseTable}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.LIST,
      I18nNamespace.TERMS_OF_USE,
      I18nNamespace.USER_LIST,
    ])),
  },
});

export default TermsOfUsePage;
