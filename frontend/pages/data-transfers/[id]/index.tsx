import type {
  DataTransfer,
  DataTransferLite,
} from '../../../src/api/generated';
import {
  DELETE_DATA_TRANSFER,
  RETRIEVE_DATA_TRANSFER,
} from '../../../src/actions/actionTypes';
import {
  PageBase,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../../_app';
import { DataTransferDetail } from '../../../src/components/dataTransfer/DataTransferDetail';
import { DetailedViewToolbar } from '../../../src/widgets/DetailedViewToolbar';
import { I18nNamespace } from '../../../src/i18n';
import type { RootState } from '../../../src/store';
import { useDataTransferForm } from '../../../src/components/dataTransfer/DataTransferForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from '../index';

function isDataTransfer(
  dtr: DataTransferLite | DataTransfer | undefined,
): dtr is DataTransfer {
  if (!dtr) {
    return false;
  }
  const maybeDtr = dtr as DataTransfer;
  return (
    maybeDtr.projectArchived !== undefined && maybeDtr.permissions !== undefined
  );
}

export const DataTransferDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList, isSubmitting } = useSelector(
    (state: RootState) => state.dataTransfers,
  );
  const dataTransfer = itemList.find(
    (dataTransfer) => dataTransfer.id === parseInt(id),
  );
  const { openFormDialog, dtrForm } = useDataTransferForm();
  const canEdit = useMemo(
    () =>
      isDataTransfer(dataTransfer) &&
      !dataTransfer.projectArchived &&
      Object.values(dataTransfer.permissions?.edit || {}).some(
        (editPermission) => editPermission,
      ),
    [dataTransfer],
  );

  const canDelete = useMemo(
    () => isDataTransfer(dataTransfer) && !!dataTransfer.permissions?._delete,
    [dataTransfer],
  );

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_DATA_TRANSFER, { id }));
  }, [dispatch, id]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
        link: '/data-transfers',
      },
      {
        text: id,
        link: `/data-transfers/${id}`,
      },
    ],
    [id, t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`${I18nNamespace.COMMON}:models.dataTransfer`)} ${id}`}
      content={DataTransferDetail}
      toolbar={() => (
        <DetailedViewToolbar
          canEdit={canEdit}
          canDelete={canDelete}
          deleteConfirmationText={'Delete ' + String(dataTransfer?.id)}
          itemId={parseInt(id)}
          isSubmitting={isSubmitting}
          onEditClick={() => {
            if (isDataTransfer(dataTransfer)) {
              openFormDialog(dataTransfer);
            }
          }}
          onDeleteConfirmation={() => {
            dispatch(requestAction(DELETE_DATA_TRANSFER, { id }));
            router.push('/data-transfers');
          }}
          entityType={t(`${I18nNamespace.COMMON}:models.dataTransfer`)}
          entityName={dataTransfer?.id ? String(dataTransfer.id) : undefined}
        >
          {dtrForm}
        </DetailedViewToolbar>
      )}
      props={{
        dataTransfer: isDataTransfer(dataTransfer) ? dataTransfer : undefined,
      }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default DataTransferDetailPage;
