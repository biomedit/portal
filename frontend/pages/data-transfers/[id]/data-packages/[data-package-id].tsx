import type {
  DataTransferLitePackagesInner,
  DataTransferPackagesInner,
} from '../../../../src/api/generated';
import {
  PageBase,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../../../_app';
import { DataPackageDetail } from '../../../../src/components/dataTransfer/DataPackageDetail';
import { I18nNamespace } from '../../../../src/i18n';
import { RETRIEVE_DATA_TRANSFER } from '../../../../src/actions/actionTypes';
import type { RootState } from '../../../../src/store';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from '../index';

function isDataPackage(
  dataPackage:
    | DataTransferLitePackagesInner
    | DataTransferPackagesInner
    | undefined,
): dataPackage is DataTransferPackagesInner {
  return (
    !!dataPackage &&
    (dataPackage as DataTransferPackagesInner).metadata !== undefined
  );
}

export const DataPackageDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then id and data-package-id are present and
  // are single elements.
  const id = router.query.id as string;
  const dataPackageId = router.query['data-package-id'] as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList } = useSelector((state: RootState) => state.dataTransfers);
  const dataTransfer = itemList.find(
    (dataTransfer) => dataTransfer.id === parseInt(id),
  );
  const dataPackage = dataTransfer?.packages?.find(
    (pkg) => pkg.id === parseInt(dataPackageId),
  );
  useEffect(() => {
    dispatch(requestAction(RETRIEVE_DATA_TRANSFER, { id }));
  }, [dispatch, id]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
        link: '/data-transfers',
      },
      {
        text: id,
        link: `/data-transfers/${id}`,
      },
      {
        text: `${t(`${I18nNamespace.COMMON}:models.dataPackage`)} ${dataPackageId}`,
        link: `/data-transfers/${id}/data-packages/${dataPackageId}`,
      },
    ],
    [dataPackageId, id, t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`${I18nNamespace.COMMON}:models.dataPackage`)} ${dataPackageId}`}
      content={DataPackageDetail}
      props={{
        dataPackage: isDataPackage(dataPackage) ? dataPackage : undefined,
      }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default DataPackageDetailPage;
