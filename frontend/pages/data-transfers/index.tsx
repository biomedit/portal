import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { DataTransferOverviewTable } from '../../src/components/dataTransfer/DataTransferOverviewTable';
import { I18nNamespace } from '../../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';

export const DataTransferPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`),
        link: '/data-transfers',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`${I18nNamespace.COMMON}:models.dataTransfer_plural`)}`}
      content={DataTransferOverviewTable}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.DATA_TRANSFER,
      I18nNamespace.DATA_TRANSFER_APPROVALS,
      I18nNamespace.LIST,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default DataTransferPage;
