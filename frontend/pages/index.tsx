import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement } from 'react';
import { appName } from './_app';
import { Home } from '../src/components/Home';
import { I18nNamespace } from '../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

export const IndexPage = (): ReactElement => {
  return <PageBase appName={appName} title={'Home'} content={Home} />;
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.FEED_LIST,
      I18nNamespace.HOME,
      I18nNamespace.USER_INFO,
    ])),
  },
});

export default IndexPage;
