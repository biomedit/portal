import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from './_app';
import { FlagManageList } from '../src/components/admin/FlagManage';
import { I18nNamespace } from '../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';

export const FlagPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.flag_plural`),
        link: '/flags',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`models.flag_plural`)}`}
      content={FlagManageList}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.FLAG_LIST,
      I18nNamespace.LIST,
    ])),
  },
});

export default FlagPage;
