import {
  ProjectForm,
  ProjectFormMode,
} from '../../src/components/projectForm/ProjectForm';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { I18nNamespace } from '../../src/i18n';
import { PageBase } from '@biomedit/next-widgets';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from './index';

export const NewProjectPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.project_plural`),
        link: `/projects`,
      },
      {
        text: 'New',
        link: `/projects/new`,
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={'New Project'}
      content={ProjectForm}
      props={{ mode: ProjectFormMode.Create }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export default NewProjectPage;
