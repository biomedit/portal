import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { I18nNamespace } from '../../src/i18n';
import { ProjectTable } from '../../src/components/projectList/ProjectTable';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';

export const ProjectPage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.project_plural`),
        link: '/projects',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`models.project_plural`)}`}
      content={ProjectTable}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.DATA_TRANSFER,
      I18nNamespace.IP_RANGE_LIST,
      I18nNamespace.LIST,
      I18nNamespace.PROJECT_FORM,
      I18nNamespace.PROJECT_LIST,
      I18nNamespace.PROJECT_RESOURCE_LIST,
      I18nNamespace.PROJECT_USER_LIST,
      I18nNamespace.PROJECT_USER_ROLE_HISTORY,
      I18nNamespace.USER_INFO,
      I18nNamespace.USER_LIST,
      I18nNamespace.USER_TEXT_FIELD,
      I18nNamespace.SERVICE_LIST,
    ])),
  },
});

export default ProjectPage;
