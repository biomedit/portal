import {
  ARCHIVE_PROJECT,
  DELETE_PROJECT,
  RETRIEVE_PROJECT,
  UNARCHIVE_PROJECT,
} from '../../../src/actions/actionTypes';
import {
  Button,
  ConfirmDialog,
  PageBase,
  requestAction,
  useDialogState,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import type {
  Project,
  ProjectPermissionsEdit,
} from '../../../src/api/generated';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../../_app';
import { DetailedViewToolbar } from '../../../src/widgets/DetailedViewToolbar';
import { I18nNamespace } from '../../../src/i18n';
import { ProjectDetail } from '../../../src/components/projectList/ProjectDetail';
import type { RootState } from '../../../src/store';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from '../index';

export const ProjectDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { t: projectsTFunction } = useTranslation(I18nNamespace.PROJECT_LIST);
  const { itemList, isSubmitting } = useSelector(
    (state: RootState) => state.project,
  );
  const project = itemList.find((project) => project.id === parseInt(id));

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_PROJECT, { id }));
  }, [dispatch, id]);

  // Show empty string if project's name is not known yet (it exists for sure)
  const title = useMemo(() => project?.name ?? '', [project]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.project_plural`),
        link: '/projects',
      },
      {
        text: title,
        link: `/projects/${id}`,
      },
    ],
    [id, t, title],
  );

  const canEdit = useMemo(() => {
    if (!project || project.archived) {
      return false;
    }
    const edit = project.permissions?.edit;
    let key: keyof ProjectPermissionsEdit;
    for (key in edit) {
      if (edit && Object.prototype.hasOwnProperty.call(edit, key)) {
        const value = edit[key];
        if (Array.isArray(value)) {
          if (value.length) {
            return true;
          }
        } else {
          if (value) {
            return true;
          }
        }
      }
    }
    return false;
  }, [project]);

  const { pagePermissions } = useSelector((state: RootState) => state.auth);

  const canDelete = useMemo(() => {
    return !!project && pagePermissions.project.del;
  }, [project, pagePermissions]);

  const { item, setItem, onClose, open } = useDialogState<Project>();

  const additionalActionButtons = useMemo(() => {
    if (!project || !project.permissions?.archive) {
      return [];
    }

    if (project.archived) {
      return [
        <Button
          variant="outlined"
          key={'unarchive-button-' + project.code}
          aria-label={projectsTFunction('actionButtons.unarchive')}
          title={projectsTFunction('actionButtons.unarchive')}
          onClick={() =>
            dispatch(
              requestAction(UNARCHIVE_PROJECT, {
                id: String(project.id),
              }),
            )
          }
        >
          Unarchive
        </Button>,
      ];
    } else {
      return [
        <React.Fragment key={'archive-fragment-' + project.code}>
          {item ? (
            <ConfirmDialog
              key={'archive-dialog-' + project.code}
              title={projectsTFunction('archiveDialog.title')}
              text={projectsTFunction('archiveDialog.text')}
              open={open}
              confirmationText={project.name}
              isSubmitting={isSubmitting}
              onConfirm={(isConfirmed: boolean) => {
                if (isConfirmed) {
                  dispatch(requestAction(ARCHIVE_PROJECT, { id }));
                }
                onClose();
              }}
            />
          ) : null}
          <Button
            variant="outlined"
            key={'archive-button-' + project.code}
            aria-label={projectsTFunction('actionButtons.archive')}
            title={projectsTFunction('actionButtons.archive')}
            onClick={() => setItem(project)}
          >
            Archive
          </Button>
        </React.Fragment>,
      ];
    }
  }, [
    project,
    dispatch,
    projectsTFunction,
    item,
    open,
    isSubmitting,
    id,
    onClose,
    setItem,
  ]);

  return (
    <PageBase
      appName={appName}
      title={title}
      toolbar={() => (
        <DetailedViewToolbar
          canEdit={canEdit}
          canDelete={canDelete}
          deleteConfirmationText={'Delete ' + project?.name}
          itemId={parseInt(id)}
          isSubmitting={isSubmitting}
          onEditClick={() => router.push(`/projects/${id}/edit`)}
          onDeleteConfirmation={() => {
            dispatch(requestAction(DELETE_PROJECT, { id }));
            router.push('/projects');
          }}
          additionalButtons={additionalActionButtons}
          entityType={t(`${I18nNamespace.COMMON}:models.project`)}
          entityName={project?.name}
        />
      )}
      content={ProjectDetail}
      props={{ project }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default ProjectDetailPage;
