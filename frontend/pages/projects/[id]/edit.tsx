import {
  type BaseReducerState,
  PageBase,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import {
  ProjectForm,
  ProjectFormMode,
} from '../../../src/components/projectForm/ProjectForm';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../../_app';
import { I18nNamespace } from '../../../src/i18n';
import type { IdRequired } from '../../../src/global';
import type { Project } from '../../../src/api/generated';
import { RETRIEVE_PROJECT } from '../../../src/actions/actionTypes';
import type { RootState } from '../../../src/store';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from '../index';

export const ProjectDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList } = useSelector<
    RootState,
    BaseReducerState<IdRequired<Project>>
  >((state) => state.project);
  const project: Project | undefined = itemList.find(
    (project) => project.id === parseInt(id),
  );

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_PROJECT, { id }));
  }, [dispatch, id]);

  // Show empty string if project's name is not known yet (it exists for sure)
  const title = useMemo(() => project?.name ?? '', [project]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.project_plural`),
        link: '/projects',
      },
      {
        text: title,
        link: `/projects/${id}`,
      },
      {
        text: 'Edit',
        link: `/projects/${id}/edit`,
      },
    ],
    [id, t, title],
  );

  return (
    <PageBase
      appName={appName}
      title={`Edit ${title}`}
      content={ProjectForm}
      props={{ project, mode: ProjectFormMode.Edit }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default ProjectDetailPage;
