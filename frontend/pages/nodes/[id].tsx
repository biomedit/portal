import { isNodeAdmin, isStaff } from '../../src/components/selectors';
import {
  PageBase,
  requestAction,
  useReduxDispatch,
} from '@biomedit/next-widgets';
import React, { type ReactElement, useEffect, useMemo } from 'react';
import { appName } from '../_app';
import { DetailedViewToolbar } from '../../src/widgets/DetailedViewToolbar';
import { I18nNamespace } from '../../src/i18n';
import type { Node } from '../../src/api/generated';
import { NodeDetail } from '../../src/components/admin/NodeDetail';
import { RETRIEVE_NODE } from '../../src/actions/actionTypes';
import type { RootState } from '../../src/store';
import { useNodeForm } from '../../src/components/admin/NodeManageForm';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export { getStaticProps } from './index';

export const NodeDetailPage = (): ReactElement => {
  const router = useRouter();
  // If this page is being rendered, then the id is present and is a single
  // element.
  const id = router.query.id as string;
  const dispatch = useReduxDispatch();
  const { t } = useTranslation(I18nNamespace.COMMON);
  const { itemList, isSubmitting } = useSelector(
    (state: RootState) => state.nodes,
  );
  const node: Node | undefined = itemList.find(
    (node) => node.id === parseInt(id),
  );
  // Show empty string if node's name is not known yet
  const title = useMemo(() => node?.name ?? '', [node]);
  const { openFormDialog, nodeForm } = useNodeForm();
  const staff = useSelector(isStaff);
  const nodeAdmin = useSelector(isNodeAdmin);
  const canEdit = useMemo(
    () => !!node && (staff || nodeAdmin),
    [node, nodeAdmin, staff],
  );

  useEffect(() => {
    dispatch(requestAction(RETRIEVE_NODE, { id }));
  }, [dispatch, id]);

  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`${I18nNamespace.COMMON}:models.node_plural`),
        link: '/nodes',
      },
      {
        text: title,
        link: `/nodes/${id}`,
      },
    ],
    [id, t, title],
  );

  return (
    <PageBase
      appName={appName}
      title={title}
      content={NodeDetail}
      toolbar={() => (
        <DetailedViewToolbar
          canEdit={canEdit}
          canDelete={false}
          itemId={parseInt(id)}
          isSubmitting={isSubmitting}
          onEditClick={() => openFormDialog(node)}
          entityType={t(`${I18nNamespace.COMMON}:models.node`)}
          entityName={node?.name}
        >
          {nodeForm}
        </DetailedViewToolbar>
      )}
      props={{ node }}
      breadcrumbs={breadcrumbs}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default NodeDetailPage;
