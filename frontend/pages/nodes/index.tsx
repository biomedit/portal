import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement, useMemo } from 'react';
import { appName } from '../_app';
import { I18nNamespace } from '../../src/i18n';
import { NodeList } from '../../src/components/admin/NodeManage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';

export const NodePage = (): ReactElement => {
  const { t } = useTranslation(I18nNamespace.COMMON);
  const breadcrumbs = useMemo(
    () => [
      { text: appName, link: '/' },
      {
        text: t(`models.node_plural`),
        link: '/nodes',
      },
    ],
    [t],
  );

  return (
    <PageBase
      appName={appName}
      title={`${t(`models.node_plural`)}`}
      content={NodeList}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.IP_RANGE_LIST,
      I18nNamespace.LIST,
      I18nNamespace.NODE,
      I18nNamespace.USER_LIST,
    ])),
  },
});

export default NodePage;
