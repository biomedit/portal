import { type FrontendGetStaticProps, PageBase } from '@biomedit/next-widgets';
import React, { type ReactElement } from 'react';
import { appName } from './_app';
import { I18nNamespace } from '../src/i18n';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { TerminologyList } from '../src/components/terminology/TerminologyList';

export const TerminologyPage = (): ReactElement => {
  const title = 'Terminologies';
  const breadcrumbs = [
    { text: appName, link: '/' },
    {
      text: title,
      link: '/terminologies',
    },
  ];

  return (
    <PageBase
      appName={appName}
      title={title}
      content={TerminologyList}
      breadcrumbs={breadcrumbs}
    />
  );
};

export const getStaticProps: FrontendGetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      I18nNamespace.COMMON,
      I18nNamespace.LIST,
      I18nNamespace.TERMINOLOGY_LIST,
      I18nNamespace.TERMINOLOGY_VERSION_LIST,
    ])),
  },
});

export default TerminologyPage;
