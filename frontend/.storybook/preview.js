export const parameters = {
  controls: {
    matchers: {
      date: /Date$/,
    },
  },
};
export const tags = ['autodocs'];
