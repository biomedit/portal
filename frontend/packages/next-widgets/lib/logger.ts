import type { AnyObject } from '../types';
import EventEmitter from 'events';
import { generateShortUuid } from './utils';

const emitter = new EventEmitter();
const logEmitterName = 'log';

/**
 * npm Log Levels
 * See https://github.com/npm/npmlog#loglevelprefix-message-
 */
export enum LogLevel {
  debug = 'debug',
  error = 'error',
  http = 'http',
  info = 'info',
  silly = 'silly',
  verbose = 'verbose',
  warn = 'warn',
}

export interface LogEvent {
  correlationId: string;
  level: LogLevel;
  message: string;
  namespace: string;
  stack: string;
}

export type LogData = AnyObject & { message: string };

export type LogListener = (data: LogEvent) => void;

export type LogEmitter = Record<typeof logEmitterName, LogEvent>;

/**
 * Adds a {@code listener}, which gets called whenever a log is made.
 * @param listener to execute whenever a log is made.
 */
export const onLog = (listener: LogListener): void => {
  emitter.on(logEmitterName, listener);
};

/**
 * Removes a {@code listener}, so it no longer gets called when a log is made.
 * @param listener to prevent from getting called when a log is made.
 */
export const offLog = (listener: LogListener): void => {
  emitter.off(logEmitterName, listener);
};

/**
 * NOTE: When logging, specifying a `message` is mandatory!
 *       Otherwise, the log will not show up in Kibana.
 *
 * When using `log.error({...})`, the object may contain any key and value pairs,
 * however the values must be strings, except if the key is "json",
 * in this case the value can be any object.
 *
 * Example:
 * ```ts
 * const log = logger('Sagas');
 * const e = {some: "error"}
 * log.error({
 *   json: e,
 *   message: 'Something failed!',
 * });
 * ```
 *
 * @param namespace of the logger, usually the name of the file
 */
export const logger = (
  namespace: string,
): Record<LogLevel, (data: LogData) => void> => ({
  [LogLevel.debug]: (data) => log(namespace, LogLevel.debug, data),
  [LogLevel.error]: (data) => log(namespace, LogLevel.error, data),
  [LogLevel.http]: (data) => log(namespace, LogLevel.http, data),
  [LogLevel.info]: (data) => log(namespace, LogLevel.info, data),
  [LogLevel.silly]: (data) => log(namespace, LogLevel.silly, data),
  [LogLevel.verbose]: (data) => log(namespace, LogLevel.verbose, data),
  [LogLevel.warn]: (data) => log(namespace, LogLevel.warn, data),
});

const log = (namespace: string, level: LogLevel, data: LogData): void => {
  emitter.emit(logEmitterName, {
    namespace,
    level,
    stack: 'frontend',
    correlationId: generateShortUuid(),
    ...data,
  });
};
