import React, { type ReactElement } from 'react';
import { Typography } from '@mui/material';
import type { Variant } from '@mui/material/styles/createTypography';

export type VersionProps = {
  appName: string;
  variant?: Variant;
  version: string;
};

export const Version = ({
  appName,
  version,
  variant = 'body2',
}: VersionProps): ReactElement => (
  <Typography title={`${appName} Version`} variant={variant} color="inherit">
    {version}
  </Typography>
);
