import type { Meta, StoryFn } from '@storybook/react';
import Box from '@mui/material/Box';
import React from 'react';
import { Version } from './Version';

export default {
  title: 'Components/Version',
  component: Version,
} as Meta<typeof Version>;

const Template: StoryFn<typeof Version> = (args) => (
  <Box sx={{ backgroundColor: 'primary.main' }}>
    <Version {...args} appName="Portal" version="1.0.0" />
  </Box>
);

export const Default = Template.bind({});
