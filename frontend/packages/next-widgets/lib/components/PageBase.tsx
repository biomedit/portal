import { Breadcrumbs, Divider, Typography } from '@mui/material';
import React, { type FunctionComponent, type ReactElement } from 'react';
import Box from '@mui/material/Box';
import { getPageTitle } from '../utils';
import Head from 'next/head';
import Link from 'next/link';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

export type Breadcrumb = {
  link?: string;
  text: string;
};

type PageBaseProps<P extends Record<string, unknown>> = {
  appName?: string;
  breadcrumbs?: Breadcrumb[];
  content: FunctionComponent<P>;
  props?: P;
  title: string;
  toolbar?: FunctionComponent;
};

const StyledLink = styled(Link)(() => ({
  textDecoration: 'none',
  font: 'inherit',
  color: 'inherit',
  '&:hover': {
    textDecoration: 'underline',
  },
}));

export function PageBase<P extends Record<string, unknown>>({
  appName,
  title,
  content: Content,
  toolbar: Toolbar,
  props = {} as P,
  breadcrumbs = [],
}: PageBaseProps<P>): ReactElement {
  return (
    <div>
      <Head>
        <title>{appName ? getPageTitle(appName, title) : title}</title>
      </Head>
      <Paper elevation={0} sx={{ mx: 4, mt: 4, mb: 9 }}>
        <Breadcrumbs aria-label="breadcrumb">
          {breadcrumbs.map(({ text, link }) =>
            link ? (
              <StyledLink href={link} key={`breadcrumb-${text}`}>
                {text}
              </StyledLink>
            ) : (
              <Typography color="text.primary" key={`breadcrumb-${text}`}>
                {text}
              </Typography>
            ),
          )}
        </Breadcrumbs>
        {!!breadcrumbs.length && (
          <Divider sx={{ marginTop: 1, marginBottom: 2 }} />
        )}
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Typography sx={{ fontSize: 24, mb: 2 }}>{title}</Typography>
          {!!Toolbar && <Toolbar />}
        </Box>
        <Content id="content" {...props} />
      </Paper>
    </div>
  );
}
