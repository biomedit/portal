import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Environment } from '../api';
import { ErrorBoundary } from './ErrorBoundary';
import { isClient } from '../utils';
import { mockConsoleError } from '../testUtils';
import { onLog } from '../logger';

describe('ErrorBoundary', function () {
  describe('Component', function () {
    const originalEnv = process.env;

    beforeEach(() => {
      jest.resetModules();
      process.env = { ...originalEnv, NODE_ENV: Environment.PRODUCTION };
    });

    afterEach(() => {
      process.env = originalEnv;
    });

    it('should display the alert', async () => {
      const ThrowError = () => {
        throw new Error('An error has been thrown!');
      };
      const spy = mockConsoleError();
      let listenerHasBeenInvoked = false;
      const expectedMatchedObject = {
        namespace: 'Uncaught exception',
        level: 'error',
        json: {
          error: {
            name: 'Error',
            message: 'An error has been thrown!',
          },
        },
        message: 'Exception was thrown and not caught or handled.',
      };
      onLog((data) => {
        expect(data).toMatchObject(expectedMatchedObject);
        listenerHasBeenInvoked = true;
      });
      render(
        <ErrorBoundary
          contactEmail="jeyeg88454@chambile.com"
          subjectPrefix="portal: "
        >
          <ThrowError />
        </ErrorBoundary>,
      );

      expect(isClient()).toBe(true);
      expect(screen.getByRole('alert')).toBeVisible();
      expect(spy).toHaveBeenCalledTimes(2);
      expect(listenerHasBeenInvoked).toBe(true);
      expect(spy.mock.calls[1][0]).toMatchObject(expectedMatchedObject);
      spy.mockRestore();
    });
  });
});
