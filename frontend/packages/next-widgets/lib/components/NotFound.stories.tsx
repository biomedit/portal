import type { Meta, StoryFn } from '@storybook/react';
import { NotFound } from './NotFound';
import React from 'react';

export default {
  component: NotFound,
  title: 'Components/NotFound',
} as Meta<typeof NotFound>;

const Template: StoryFn<typeof NotFound> = (args) => <NotFound {...args} />;

export const Default = Template.bind({});
