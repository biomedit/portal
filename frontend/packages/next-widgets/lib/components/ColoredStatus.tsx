import Chip, { type ChipPropsColorOverrides } from '@mui/material/Chip';
import React, { type ReactElement } from 'react';
import type { OverridableStringUnion } from '@mui/types';

export interface Status<T> {
  color?: OverridableStringUnion<
    | 'default'
    | 'error'
    | 'info'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'warning',
    ChipPropsColorOverrides
  >;
  text?: string;
  value: T;
}

export type ColoredStatusProps<T> = {
  readonly statuses: Status<T>[];
  readonly value?: T;
};

export function ColoredStatus<T>({
  value,
  statuses,
}: ColoredStatusProps<T>): ReactElement | null {
  if (value === undefined) {
    return null;
  }
  const status = statuses.find((item) => item.value === value);
  if (!status) {
    return null;
  }

  return (
    <Chip
      label={String(status.text ?? status.value).toUpperCase()}
      color={status.color}
    />
  );
}
