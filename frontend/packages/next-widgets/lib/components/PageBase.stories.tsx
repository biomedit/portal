import { Button, Typography } from '@mui/material';
import type { Meta, StoryFn } from '@storybook/react';
import { PageBase } from './PageBase';
import React from 'react';

type CustomComponentProps = {
  text: string;
};

const CustomComponent = ({ text }: CustomComponentProps) => (
  <Typography>{text}</Typography>
);

export default {
  title: 'Components/PageBase',
  component: PageBase,
} as Meta<typeof PageBase>;

const Template: StoryFn<typeof PageBase> = (args) => (
  <PageBase
    {...args}
    title={'Foo'}
    content={() => <Typography>{'42'}</Typography>}
  />
);

const TemplateWithGeneric: StoryFn<typeof PageBase> = (args) => (
  <PageBase<CustomComponentProps>
    {...args}
    title={'Foo'}
    content={CustomComponent}
    props={{ text: '42' }}
  />
);

export const Default = Template.bind({});

export const WithToolbar = Template.bind({});
WithToolbar.args = { toolbar: () => <Button>{'toolbar'}</Button> };

export const WithAppName = Template.bind({});
WithAppName.args = { appName: 'Bar' };

export const WithBreadcrumbs = Template.bind({});
WithBreadcrumbs.args = {
  breadcrumbs: [{ text: 'Home', link: '/' }, { text: 'Baz' }],
};

export const WithContentProps = TemplateWithGeneric.bind({});
