import { Environment, redirectExternal } from '../api';
import { generateShortUuid, isClient } from '../utils';
import {
  InternalErrorContent,
  type InternalErrorContentProps,
} from '../widgets';
import React, { type ErrorInfo, type ReactNode } from 'react';
import type { AnyObject } from '../../types';
import { logger } from '../logger';

type Props = Omit<InternalErrorContentProps, 'correlationId'> & {
  children?: ReactNode;
};

type State = {
  correlationId?: string;
  hasError: boolean;
};

export type SerializedError = AnyObject & {
  message: string;
  name: string;
  stack?: string;
};

export class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(): State {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, correlationId: generateShortUuid() };
  }

  private static serializeError(error: Error): SerializedError {
    return {
      ...error,
      name: error.name,
      message: error.message,
      stack: error.stack,
    };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    const errorObject = {
      error: ErrorBoundary.serializeError(error),
      errorInfo,
    };

    const log = logger('Uncaught exception');
    log.error({
      json: errorObject,
      correlationId: this.state.correlationId,
      message: 'Exception was thrown and not caught or handled.',
    });
  }

  render(): ReactNode {
    if (isClient()) {
      const location = window.location;
      const homeUrl = location.protocol + '//' + location.host;
      if (
        this.state.hasError &&
        process.env.NODE_ENV === Environment.PRODUCTION // make sure we still get create-react-app's error boundary during development
      ) {
        return (
          <div role="alert">
            <p>
              <InternalErrorContent
                correlationId={this.state.correlationId as string}
                subjectPrefix={this.props.subjectPrefix}
                contactEmail={this.props.contactEmail}
              />
            </p>
            <button onClick={() => redirectExternal(homeUrl)}>
              Back to the home page
            </button>
          </div>
        );
      }
    }
    return this.props.children;
  }
}
