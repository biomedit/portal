import type { SortingFn } from '@tanstack/react-table';

export const useLocaleSortingFn = <T>() => {
  const localeSortingFn: SortingFn<T> = (rowA, rowB, columnId) => {
    const a: string = rowA.getValue(columnId);
    const b: string = rowB.getValue(columnId);
    return a.localeCompare(b);
  };
  return localeSortingFn;
};
