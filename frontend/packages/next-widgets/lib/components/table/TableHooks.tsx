import type { AdditionalActionButtonFactory, IdType } from '../../../types';
import {
  ButtonBox,
  DeleteIconButton,
  deleteIconButtonLabelPrefix,
  EditIconButton,
  editIconButtonLabelPrefix,
} from '../../widgets';
import React, { type ReactElement, type ReactNode, useCallback } from 'react';
import type { CellContext } from '@tanstack/react-table';
import { isFunction } from 'lodash';

export type TableActionButtonsProps = {
  addButtonLabel?: string;
  additionalActionButtons?: ReactNode | ReactNode[];
  canDelete?: boolean;
  canEdit?: boolean;
  onDelete?: () => void;
  onEdit?: () => void;
};

export type TableActionsCellProps<T, D> = CellContext<T, D> & {
  additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
  canDelete?: boolean;
  canEdit?: boolean;
};

function TableActionButtons({
  addButtonLabel,
  canEdit,
  canDelete,
  onEdit,
  onDelete,
  additionalActionButtons,
}: TableActionButtonsProps): ReactElement {
  const tableRowButtonLabelGeneric = 'row';

  return (
    <ButtonBox sx={{ display: 'flex', flexWrap: 'nowrap' }}>
      <>
        {canEdit && (
          <EditIconButton
            aria-label={
              editIconButtonLabelPrefix +
              (addButtonLabel ?? tableRowButtonLabelGeneric)
            }
            onClick={onEdit}
          />
        )}
        {canDelete && (
          <DeleteIconButton
            aria-label={
              deleteIconButtonLabelPrefix +
              (addButtonLabel ?? tableRowButtonLabelGeneric)
            }
            onClick={onDelete}
          />
        )}
        {additionalActionButtons}
      </>
    </ButtonBox>
  );
}

type useTableActionsCellProps<T extends IdType> = {
  addButtonLabel?: string;
  additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
  getDeleteConfirmationText?: (item: T) => string;
  handleDeleteOpen: (
    id?: number | undefined,
    name?: string | undefined,
  ) => void;
  itemList: T[];
  onEdit?: (item: T) => void;
};

export function useTableActionsCell<T extends IdType>({
  addButtonLabel,
  onEdit,
  handleDeleteOpen,
  getDeleteConfirmationText,
  itemList,
  additionalActionButtons,
}: useTableActionsCellProps<T>): {
  // Matches generic signature of `CellProps`
  TableActionsCell: <D extends T, V>(
    cellProps: TableActionsCellProps<D, V>,
  ) => ReactElement;
  hasAdditionalActionButtons: boolean;
} {
  const hasAdditionalActionButtons = itemList.some((item) =>
    additionalActionButtons?.some((actionButtonFn) => actionButtonFn(item)),
  );

  const TableActionsCell = useCallback(
    // Matches generic signature of `CellProps`
    <D extends T, V>({
      row,
      canEdit,
      canDelete,
      additionalActionButtons,
    }: TableActionsCellProps<D, V>) => {
      return (
        <TableActionButtons
          addButtonLabel={addButtonLabel}
          canEdit={isFunction(canEdit) ? canEdit(row.original) : canEdit}
          canDelete={
            isFunction(canDelete) ? canDelete(row.original) : canDelete
          }
          onEdit={() => onEdit && onEdit(row.original)}
          onDelete={() =>
            handleDeleteOpen(
              row.original.id,
              getDeleteConfirmationText &&
                getDeleteConfirmationText(row.original),
            )
          }
          additionalActionButtons={additionalActionButtons?.map(
            (actionButtonFn: AdditionalActionButtonFactory<D>) =>
              actionButtonFn(row.original),
          )}
        />
      );
    },
    [addButtonLabel, getDeleteConfirmationText, handleDeleteOpen, onEdit],
  );

  return { hasAdditionalActionButtons, TableActionsCell };
}
