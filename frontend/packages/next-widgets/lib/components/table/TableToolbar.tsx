import { addIconButtonLabelPrefix, Button } from '../../widgets/Buttons';
import { Box, Grid2, Toolbar, Typography } from '@mui/material';
import { GlobalFilter, type GlobalFilterProps } from './GlobalFilter';
import React, { type ReactElement, useCallback } from 'react';
import AddIcon from '@mui/icons-material/Add';
import { FabButton } from '../../widgets';

export type TableToolbarProps = {
  addButtonLabel?: string;
  canAdd?: boolean;
  canExport?: boolean;
  globalFilterProps?: GlobalFilterProps;
  inline?: boolean;
  isExporting?: boolean;
  onAdd?: () => void;
  onExport?: () => void;
  title?: string;
};

export const tableAddButtonAriaLabelPrefix = addIconButtonLabelPrefix;

export function TableToolbar({
  globalFilterProps,
  title,
  canAdd,
  onAdd,
  canExport,
  onExport,
  isExporting = false,
  addButtonLabel,
  inline,
}: TableToolbarProps): ReactElement {
  const showAddButton = inline && canAdd;
  const addOnClick = useCallback((): void => onAdd && onAdd(), [onAdd]);
  const showExportButton = inline && canExport;

  return (
    <Toolbar
      sx={{
        '&.MuiToolbar-root': {
          paddingLeft: 2,
          paddingRight: 1,
          minHeight: 'inherit',
        },
      }}
    >
      <Box>
        {title && (
          <Box mb={2}>
            <Typography
              variant="subtitle1"
              sx={{
                display: 'inline',
                paddingRight: 2,
              }}
            >
              {title}
            </Typography>
          </Box>
        )}

        <Grid2 container spacing={2} alignItems="center">
          {globalFilterProps && (
            <Grid2>
              <GlobalFilter {...globalFilterProps} />
            </Grid2>
          )}
          {showAddButton && (
            <Grid2>
              <Button
                variant="outlined"
                color="primary"
                startIcon={<AddIcon />}
                onClick={addOnClick}
                aria-label={tableAddButtonAriaLabelPrefix + addButtonLabel}
              >
                {addButtonLabel}
              </Button>
            </Grid2>
          )}
          {showExportButton && (
            <Grid2>
              <FabButton
                icon="download"
                size="small"
                disabled={isExporting}
                tooltip="Download as .csv"
                onClick={onExport}
              />
            </Grid2>
          )}
        </Grid2>
      </Box>
    </Toolbar>
  );
}
