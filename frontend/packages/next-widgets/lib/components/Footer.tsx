import { Box, Typography } from '@mui/material';
import Image from 'next/image';
import type { LogoProps } from './Header';
import React from 'react';
import { SafeLink } from '../widgets';
import { styled } from '@mui/material/styles';
import type { Variant } from '@mui/material/styles/createTypography';
import { Version } from './Version';

const StyledFooter = styled('footer')(({ theme }) => ({
  marginTop: 'auto!important',
  marginBottom: theme.spacing(2),
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: theme.spacing(1),
}));

export type FooterProps = LogoProps & {
  appName: string;
  color?: string;
  privacyHref?: string;
  variant?: Variant;
  version: string;
};

export const Footer = ({
  logoSrc,
  logoText,
  logoWidth,
  logoHeight,
  logoHref,
  priority = false,
  privacyHref,
  version,
  appName,
  color = 'primary.contrastText',
  variant = 'body2',
}: FooterProps) => {
  return (
    <StyledFooter>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Typography variant={variant} sx={{ marginRight: 1 }}>
          A project of
        </Typography>
        {logoHref ? (
          <SafeLink href={logoHref}>
            <Image
              src={logoSrc}
              width={logoWidth}
              height={logoHeight}
              alt={logoText}
              priority={priority}
            />
          </SafeLink>
        ) : (
          <Image
            src={logoSrc}
            width={logoWidth}
            height={logoHeight}
            alt={logoText}
          />
        )}
      </Box>
      {privacyHref && (
        <SafeLink
          href={privacyHref}
          color="inherit"
          sx={{
            textDecoration: 'underline',
          }}
          variant={variant}
        >
          Privacy Policy
        </SafeLink>
      )}
      <Version {...{ appName, version, color, variant }} />
    </StyledFooter>
  );
};
