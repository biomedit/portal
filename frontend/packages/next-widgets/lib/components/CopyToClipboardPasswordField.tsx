import {
  ContentCopyButton,
  CopyToClipboardSnackbar,
  useCopyToClipboardState,
} from './CopyToClipboardTextField';
import React, { useState } from 'react';
import TextField, { type TextFieldProps } from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

type CopyToClipboardPasswordFieldProps = Omit<
  TextFieldProps,
  'defaultValue'
> & {
  iconButtonTitle?: string;
  value: string | undefined;
};
export const CopyToClipboardPasswordField = ({
  value,
  ...props
}: CopyToClipboardPasswordFieldProps) => {
  const { open, onClose, onClick } = useCopyToClipboardState();
  const [showPassword, setShowPassword] = useState(false);
  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };
  return (
    <>
      <TextField
        type={showPassword ? 'text' : 'password'}
        defaultValue={value}
        margin="normal"
        size="small"
        {...props}
        slotProps={{
          input: {
            readOnly: true,
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  color="primary"
                  aria-label="Toggle password visibility"
                  title="Toggle password visibility"
                  onClick={() => setShowPassword((show) => !show)}
                  onMouseDown={handleMouseDownPassword}
                  size="medium"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
                <ContentCopyButton onClick={() => onClick(value)} />
              </InputAdornment>
            ),
          },
        }}
      />
      <CopyToClipboardSnackbar open={open} onClose={onClose} />
    </>
  );
};
