import type { Meta, StoryFn } from '@storybook/react';
import { CopyToClipboardPasswordField } from './CopyToClipboardPasswordField';
import React from 'react';

export default {
  title: 'Components/CopyToClipboardPasswordField',
  component: CopyToClipboardPasswordField,
} as Meta<typeof CopyToClipboardPasswordField>;

const Template: StoryFn<typeof CopyToClipboardPasswordField> = (args) => (
  <CopyToClipboardPasswordField {...args} value="Kerckhoffs's principle" />
);

export const Default = Template.bind({});
