import { Dialog, type DialogProps } from '../widgets';
import { Form, type FormProps } from './forms';
import React, { type ReactElement } from 'react';
import type { FieldValues } from 'react-hook-form';

export type FormDialogProps<T extends FieldValues> = Omit<
  FormProps<T>,
  'title'
> &
  Omit<DialogProps, 'onSubmit'>;

export function FormDialog<T extends FieldValues>({
  onSubmit,
  children,
  noValidate,
  ...other
}: FormDialogProps<T>): ReactElement {
  return (
    <Dialog
      confirmButtonProps={{
        // make sure submit button is always present (as `onSubmit` is required)
        autoFocus: false,
      }}
      {...other}
    >
      <Form onSubmit={onSubmit} id="dialog-form" noValidate={noValidate}>
        {children}
      </Form>
    </Dialog>
  );
}
