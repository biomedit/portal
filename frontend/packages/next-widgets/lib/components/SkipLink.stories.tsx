import type { Meta, StoryFn } from '@storybook/react';
import React from 'react';
import { SkipLink } from './SkipLink';

export default {
  title: 'Components/SkipLink',
  component: SkipLink,
} as Meta<typeof SkipLink>;

const Template: StoryFn<typeof SkipLink> = (args) => {
  return (
    <div>
      <SkipLink {...args} />
      <main id="main-content">
        <div>Press the tab key to see it in action!</div>
        <div>
          <a id="link1" href="#">
            Test Link 1
          </a>
        </div>
        <div>
          <a href="#">Test Link 2</a>
        </div>
        <div>
          <a href="#">Test Link 3</a>
        </div>
      </main>
    </div>
  );
};

export const Default = Template.bind({});

export const Text = Template.bind({});
Text.args = { text: 'Skip to content with enter!' };

export const Href = Template.bind({});
Href.args = { href: '#link1' };

export const TextAndHref = Template.bind({});
TextAndHref.args = {
  text: 'Skip to the first link with enter!',
  href: '#link1',
};
