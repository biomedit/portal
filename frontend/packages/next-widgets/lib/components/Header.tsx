import React, { type ReactElement } from 'react';
import Box from '@mui/material/Box';
import Image from 'next/image';
import { SafeLink } from '../widgets';

export type SafeNumber = number | `${number}`;

export type LogoProps = {
  logoHeight: SafeNumber;
  logoHref?: string;
  logoSrc: string;
  logoText: string;
  logoWidth: SafeNumber;
  priority?: boolean;
};

export const Header = ({
  logoSrc,
  logoText,
  logoWidth,
  logoHeight,
  logoHref,
  priority = false,
}: LogoProps): ReactElement => (
  <Box sx={{ textAlign: 'right' }}>
    {logoHref ? (
      <SafeLink href={logoHref}>
        <Image
          src={logoSrc}
          width={logoWidth}
          height={logoHeight}
          alt={logoText}
          priority={priority}
        />
      </SafeLink>
    ) : (
      <Image
        src={logoSrc}
        width={logoWidth}
        height={logoHeight}
        alt={logoText}
        priority={priority}
      />
    )}
  </Box>
);
