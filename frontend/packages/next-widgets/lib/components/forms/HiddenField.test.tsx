import * as React from 'react';
import { FormProvider, type UseFormReturn } from 'react-hook-form';
import { render, renderHook } from '@testing-library/react';
import type { AnyObject } from '../../../types';
import { HiddenField } from './HiddenField';
import type { Primitive } from 'utility-types';
import type { ReactElement } from 'react';
import { useEnhancedForm } from './hooks';

const name = 'id' as const;

type FieldValues = Record<typeof name, number>;
type FieldContext = AnyObject;

type TestHiddenFieldProps = {
  form: UseFormReturn<FieldValues, FieldContext>;
  value: Primitive;
};

const TestHiddenField = ({
  form,
  value,
}: TestHiddenFieldProps): ReactElement => (
  <FormProvider {...form}>
    <HiddenField name={name} initialValues={{ [name]: value }} />
  </FormProvider>
);

describe('HiddenField', function () {
  describe('Component', function () {
    let form: UseFormReturn<FieldValues, FieldContext>;

    beforeEach(() => {
      form = renderHook(() => useEnhancedForm<FieldValues, FieldContext>())
        .result.current;
    });

    // `NaN` resp. `undefined` as `changedValue` does NOT change form state (`changedFormState`)
    it.each`
      initialValue            | initialFormState        | changedValue | changedFormState
      ${0}                    | ${'0'}                  | ${0}         | ${'0'}
      ${0}                    | ${'0'}                  | ${-0}        | ${'0'}
      ${2}                    | ${'2'}                  | ${3}         | ${'3'}
      ${'2'}                  | ${'2'}                  | ${3}         | ${'3'}
      ${'2'}                  | ${'2'}                  | ${undefined} | ${undefined}
      ${2}                    | ${'2'}                  | ${'3'}       | ${'3'}
      ${''}                   | ${undefined}            | ${3}         | ${'3'}
      ${null}                 | ${undefined}            | ${undefined} | ${undefined}
      ${undefined}            | ${undefined}            | ${null}      | ${undefined}
      ${undefined}            | ${undefined}            | ${false}     | ${'false'}
      ${true}                 | ${'true'}               | ${1}         | ${'1'}
      ${0}                    | ${'0'}                  | ${false}     | ${'false'}
      ${false}                | ${'false'}              | ${NaN}       | ${undefined}
      ${'The cake is a lie!'} | ${'The cake is a lie!'} | ${'🎂'}      | ${'🎂'}
    `(
      "should update the form state's value from $initialFormState to $changedFormState if defaultValue changes from $initialValue to $changedValue",
      ({ initialValue, initialFormState, changedValue, changedFormState }) => {
        const { rerender } = render(
          <TestHiddenField form={form} value={initialValue} />,
        );

        expect(form.getValues()[name]).toBe(initialFormState);

        rerender(<TestHiddenField form={form} value={changedValue} />);

        expect(form.getValues()[name]).toBe(changedFormState);
      },
    );
  });
});
