import * as React from 'react';
import {
  AutocompleteField,
  type AutocompleteFieldProps,
} from './AutocompleteField';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormProvider } from 'react-hook-form';
import { useChoices } from '../choice';
import { useEnhancedForm } from './hooks';

const testSelectLabel = 'Favorite Color';

const firstChoice = { label: 'Red', value: 'red' };
const colors = [
  firstChoice,
  { label: 'Green', value: 'green' },
  { label: 'Blue', value: 'blue' },
];

function TestAutocompleteField(
  autocompleteFieldProps: Partial<AutocompleteFieldProps>,
) {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <AutocompleteField
        name="favoriteColor"
        label={testSelectLabel}
        choices={useChoices(colors, 'value', 'label')}
        {...autocompleteFieldProps}
      />
    </FormProvider>
  );
}

const selectOption = async (index: number) => {
  const textbox = await screen.findByRole('combobox');
  // textbox.click() does NOT make the options appear
  fireEvent.mouseDown(textbox);

  const options = await screen.findAllByRole('option');
  fireEvent.click(options[index]);
};

describe('AutocompleteField', function () {
  describe('without default value', function () {
    beforeEach(() => {
      render(<TestAutocompleteField />);
    });

    it('should render a AutocompleteField with no initial value', async () => {
      await screen.findAllByText(testSelectLabel);
    });

    it('should select a value', async () => {
      await selectOption(0);

      // Verify: first options maps to first color
      await screen.findByDisplayValue(colors[0].label);
      expect(
        screen.queryByDisplayValue(colors[1].label),
      ).not.toBeInTheDocument();
    });
  });

  describe('with default value', function () {
    beforeEach(() => {
      const green = colors[1];
      render(
        <TestAutocompleteField
          initialValues={{ favoriteColor: green.value }}
        />,
      );
    });

    it('should render a AutocompleteField with default value pre-selected', async () => {
      const green = colors[1];
      await screen.findByDisplayValue(green.label);
    });

    it('should clear the selected value', async () => {
      const green = colors[1];
      await screen.findByDisplayValue(green.label);

      const clearButton = await screen.findByTitle('Clear Favorite Color');
      fireEvent.click(clearButton);
      // Selection has been cleared
      expect(screen.queryByDisplayValue(green.label)).not.toBeInTheDocument();
    });

    it.each`
      populateIfSingleChoice
      ${true}
      ${false}
    `(
      'should select the only available option if populateIfSingleChoice is $populateIfSingleChoice',
      async ({ populateIfSingleChoice }) => {
        render(
          <TestAutocompleteField
            populateIfSingleChoice={populateIfSingleChoice}
            choices={[firstChoice]}
          />,
        );
        if (populateIfSingleChoice) {
          await screen.findByDisplayValue(firstChoice.label);
        } else {
          expect(
            screen.queryByDisplayValue(firstChoice.label),
          ).not.toBeInTheDocument();
        }
      },
    );
  });
});
