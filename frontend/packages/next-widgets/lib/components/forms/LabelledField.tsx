import 'setimmediate';
import * as merge from 'deepmerge';
import {
  DatePicker,
  type DatePickerProps,
} from '@mui/x-date-pickers/DatePicker';
import dayjs, { Dayjs } from 'dayjs';
import {
  type EnhancedArrayFieldProps,
  type EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
  type UseEnhancedFieldHookReturn,
} from './hooks';
import {
  type FieldError,
  type RegisterOptions,
  useFormContext,
} from 'react-hook-form';
import { HiddenArrayField, HiddenField } from './HiddenField';
import { isError, serializeResponse, toArray } from '../../utils';
import React, { type ReactElement, useRef } from 'react';
import { TextField, type TextFieldProps } from '@mui/material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import type { AnyObject } from '../../../types';
import debouncePromise from 'awesome-debounce-promise';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { omit } from 'lodash';
import { StatusCodes } from 'http-status-codes';
import { uniqueValidationDebounce } from '../../config';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);

export interface LabelledFieldBaseProps
  extends UseEnhancedFieldHookReturn,
    Omit<TextFieldProps, 'error'> {
  autofocus?: boolean;
  format?: string;
  name: string;
  /**
   * A function which is being called when checking for uniqueness.
   *
   * Should throw a `Response` object with `response.status === StatusCodes.CONFLICT`
   * in case of the value NOT being unique.
   */
  unique?: (value: AnyObject) => Promise<void>;
  validations?: RegisterOptions | RegisterOptions[];
}

const LabelledFieldBase = ({
  name,
  error,
  defaultValue,
  validations,
  unique,
  autofocus,
  disabled,
  type,
  format,
  ...textFieldProps
}: LabelledFieldBaseProps): ReactElement => {
  const { register, setValue, setError } = useFormContext();
  // prevent re-renders on value changes
  const lastUniqueValue = useRef<string | null>(null);
  const validationArray = toArray(validations);

  if (unique) {
    validationArray.push({
      validate: {
        unique: debouncePromise(
          async (value) => {
            /**
             * Initial value is always valid.
             *
             * Without this check, if an existing resource is edited,
             * where the value is changed to something else and
             * then back to the initial value, it would falsely show the value as NOT being unique,
             * as it would claim it to be already existing.
             */
            const isInitialValue = defaultValue === value;
            // prevent calling backend multiple times if the value is still the same
            const isAlreadyValidated = lastUniqueValue.current === value;
            if (isInitialValue || isAlreadyValidated) {
              return true;
            }
            try {
              await unique({ [name]: value });
              lastUniqueValue.current = value;
              return true;
            } catch (error) {
              if (isError(error) && 'response' in error) {
                const response = error.response as Response;
                if (response.status === StatusCodes.CONFLICT) {
                  return 'Must be unique.';
                } else {
                  // Unhandled response
                  throw new Error(
                    JSON.stringify(await serializeResponse(response)),
                  );
                }
              }
              throw error;
            }
          },
          uniqueValidationDebounce ? Number(uniqueValidationDebounce) : 750,
          // instantly validate after the first change, then debounce every call after that
          { leading: true },
        ),
      },
    });
  }

  if (disabled) {
    validationArray.push({ validate: { disabled: () => true } });
  }

  const { ref, ...rest } = register(name, merge.all(validationArray));
  if (type == 'date') {
    // We do not need `value` as this widget will NEVER be controlled
    const otherTextFieldProps = omit(textFieldProps as DatePickerProps<Dayjs>, [
      'value',
    ]);
    // In case of `undefined`, `useEnhancedField` returns '' as default value,
    // which is NOT a valid date.
    const defaultDate = defaultValue ? dayjs(defaultValue as Date) : undefined;
    return (
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DatePicker<Dayjs>
          {...otherTextFieldProps}
          {...rest}
          onError={(reason, value) => {
            setError(name, { message: `'${reason}' for '${value}'` });
          }}
          format={format}
          inputRef={disabled ? undefined : ref}
          onChange={(changed) => {
            // `utc(true)` changes the time zone without changing the current time.
            setValue(name, changed?.utc(true), {
              shouldValidate: true,
              shouldDirty: true,
            });
          }}
          defaultValue={defaultDate}
          autoFocus={autofocus}
          slotProps={{
            actionBar: {
              actions: ['clear'],
            },
            textField: {
              error: !!error,
              helperText: error?.message,
              size: 'small',
            },
          }}
        />
      </LocalizationProvider>
    );
  }

  return (
    <TextField
      {...textFieldProps}
      {...rest}
      type={type}
      inputRef={disabled ? undefined : ref}
      error={!!error}
      helperText={error?.message || textFieldProps.helperText}
      defaultValue={defaultValue}
      autoFocus={autofocus}
      size="small"
      disabled={disabled}
      slotProps={{
        htmlInput: {
          onBlur: (event: React.FocusEvent<HTMLInputElement>) => {
            if (type === 'url') {
              try {
                setValue(name, new URL(event.target.value).href);
              } catch (_e) {
                return;
              }
            } else {
              return;
            }
          },
        },
      }}
    />
  );
};

export type LabelledFieldProps<TInitialValues extends Record<string, unknown>> =
  EnhancedFieldProps<TInitialValues> &
    Omit<TextFieldProps, 'error'> &
    Pick<
      LabelledFieldBaseProps,
      'autofocus' | 'format' | 'unique' | 'validations'
    > & {
      fieldError?: FieldError;
    };

export function LabelledField<TInitialValues extends Record<string, unknown>>({
  name,
  initialValues,
  validations,
  unique,
  autofocus,
  fieldError,
  disabled,
  ...textFieldProps
}: LabelledFieldProps<TInitialValues>): ReactElement {
  const enhancedFieldProps = {
    name,
    initialValues,
  };
  const { error, defaultValue } =
    useEnhancedField<TInitialValues>(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise, its value becomes 'undefined'
        <HiddenField {...enhancedFieldProps} />
      )}
      <LabelledFieldBase
        {...textFieldProps}
        name={name}
        disabled={disabled}
        defaultValue={defaultValue}
        error={fieldError ?? error}
        validations={validations}
        unique={unique}
        autofocus={autofocus}
      />
    </>
  );
}

export type LabelledArrayFieldProps = EnhancedArrayFieldProps &
  Pick<LabelledFieldBaseProps, 'autofocus' | 'unique' | 'validations'> &
  TextFieldProps;
export const LabelledArrayField = ({
  arrayName,
  fieldName,
  field,
  index,
  validations,
  unique,
  autofocus,
  disabled,
  ...textFieldProps
}: LabelledArrayFieldProps): ReactElement => {
  const enhancedFieldProps = {
    arrayName,
    fieldName,
    field,
    index,
  };
  const { error, name, defaultValue } =
    useEnhancedArrayField(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise, its value becomes 'undefined'
        <HiddenArrayField {...enhancedFieldProps} />
      )}
      <LabelledFieldBase
        {...textFieldProps}
        disabled={disabled}
        name={name}
        defaultValue={defaultValue}
        error={error}
        unique={unique}
        validations={validations}
        autofocus={autofocus}
      />
    </>
  );
};
