import type { Meta, StoryFn } from '@storybook/react';
import type { AnyObject } from '../../../types';
import { AutocompleteArrayField } from './AutocompleteField';
import { FormProvider } from 'react-hook-form';
import { MultilineField } from './MultilineField';
import React from 'react';
import { useEnhancedForm } from './hooks';

export default {
  title: 'Components/Forms/MultilineField',
  component: MultilineField,
} as Meta<typeof MultilineField>;

const fieldName = 'field';
const arrayName = fieldName + 'Array';
const label = 'message';
const defaultProps = {
  arrayName: arrayName,
  renderChild: (field: Record<'key', string>, index: number) => (
    <AutocompleteArrayField
      field={field}
      index={index}
      fieldName={fieldName}
      arrayName={arrayName}
      label={label}
      choices={[
        { label: 'Hello', value: 'Hello' },
        { label: 'World', value: 'World' },
      ]}
    />
  ),
};

const EmptyTemplate: StoryFn<typeof MultilineField> = (args) => {
  const form = useEnhancedForm<AnyObject>({
    defaultValues: { fieldArray: [] },
  });
  return (
    <FormProvider {...form}>
      <MultilineField {...defaultProps} {...args} />
    </FormProvider>
  );
};

const Template: StoryFn<typeof MultilineField> = (args) => {
  const form = useEnhancedForm({
    defaultValues: { fieldArray: [{ field: 'Hello' }, { field: 'World' }] },
  });
  return (
    <FormProvider {...form}>
      <MultilineField {...defaultProps} {...args} />
    </FormProvider>
  );
};

export const Empty = EmptyTemplate.bind({});
Empty.args = {};

export const Default = Template.bind({});
Default.args = {};

export const WithDisabledAddButton = Template.bind({});
WithDisabledAddButton.args = { addButtonDisabled: true };

export const WithDisabledDeleteButton = Template.bind({});
WithDisabledDeleteButton.args = { deleteButtonDisabled: true };

export const WithDisabledButtons = Template.bind({});
WithDisabledButtons.args = {
  ...WithDisabledAddButton.args,
  ...WithDisabledDeleteButton.args,
};
