import { type Choice, useChoices } from '../choice';
import type { Meta, StoryFn } from '@storybook/react';
import React, { useEffect, useState } from 'react';
import { AutocompleteField } from './AutocompleteField';
import { Button } from '../../widgets';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';

export default {
  title: 'Components/Forms/AutocompleteField',
  component: AutocompleteField,
} as Meta<typeof AutocompleteField>;

const defaultColors = [
  { label: 'Red', value: 'red' },
  { label: 'Green', value: 'green' },
  { label: 'Blue', value: 'blue' },
];

const otherColors = [
  { label: 'Black', value: 'black' },
  { label: 'Yellow', value: 'yellow' },
  { label: 'Cyan', value: 'cyan' },
];

const Template: StoryFn<typeof AutocompleteField> = (args) => {
  const form = useEnhancedForm();

  const [colors, setColors] = useState<typeof defaultColors>(defaultColors);
  const [title, setTitle] = useState<string | undefined>();

  useEffect(() => {
    doSetTitle(form.getValues('favoriteColor'));
  }, [form]);

  const doSetTitle = (value: string) => {
    setTitle(value ? `You chose '${value}'!` : undefined);
  };

  return (
    <>
      <FormProvider {...form}>
        <AutocompleteField
          {...args}
          name="favoriteColor"
          label={'Favorite Color'}
          choices={useChoices(colors, 'value', 'label')}
          onChange={(choice) => {
            doSetTitle((choice as Choice)?.value as string);
          }}
          title={title}
        />
      </FormProvider>
      <Button onClick={() => setColors(defaultColors)}>Default Colors</Button>
      <Button onClick={() => setColors(otherColors)}>Other Colors</Button>
    </>
  );
};

const SingleChoiceTemplate: StoryFn<typeof AutocompleteField> = (args) => {
  const form = useEnhancedForm();

  const [colors, setColors] = useState<typeof defaultColors>(defaultColors);

  return (
    <>
      <FormProvider {...form}>
        <AutocompleteField
          {...args}
          name="favoriteColor"
          label={'Favorite Color'}
          choices={useChoices(colors.splice(0, 1), 'value', 'label')}
        />
      </FormProvider>
      <Button onClick={() => setColors(defaultColors)}>Default Colors</Button>
      <Button onClick={() => setColors(otherColors)}>Other Colors</Button>
    </>
  );
};

export const Default = Template.bind({});
Default.args = {
  initialValues: {
    favoriteColor: 'red',
  },
};

export const InputHeight = Template.bind({});
InputHeight.args = { inputHeight: 50 };

export const FullWidth = Template.bind({});
FullWidth.args = { width: '100%' };

export const Multiple = Template.bind({});
Multiple.args = { multiple: true };

export const MediumSized = Template.bind({});
MediumSized.args = { inputSize: 'medium' };

export const SingleChoiceDefault = SingleChoiceTemplate.bind({});
SingleChoiceDefault.args = {};

export const SingleChoicePopulated = SingleChoiceTemplate.bind({});
SingleChoicePopulated.args = { populateIfSingleChoice: true };
