import type { Meta, StoryFn } from '@storybook/react';
import { FormProvider } from 'react-hook-form';
import React from 'react';
import { SelectField } from './SelectField';
import { useChoices } from '../choice';
import { useEnhancedForm } from './hooks';

export default {
  title: 'Components/Forms/SelectField',
  component: SelectField,
} as Meta<typeof SelectField>;

const colors = [
  { name: 'Red', code: 'red' },
  { name: 'Green', code: 'green' },
  { name: 'Blue', code: 'blue' },
];

const Template: StoryFn<typeof SelectField> = (args) => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <SelectField
        {...args}
        name="favoriteColor"
        label={'Favorite Color'}
        choices={useChoices(colors, 'code', 'name')}
        helperText={'Choose your favorite color'}
      />
    </FormProvider>
  );
};

const SingleChoiceTemplate: StoryFn<typeof SelectField> = (args) => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <SelectField
        {...args}
        name="favoriteColor"
        label={'Favorite Color'}
        choices={useChoices(colors.slice(0, 1), 'code', 'name')}
      />
    </FormProvider>
  );
};

export const Empty = Template.bind({});
Empty.args = {};

export const InitialValue = Template.bind({});
InitialValue.args = { initialValues: { favoriteColor: 'blue' } };

export const Loading = Template.bind({});
Loading.args = { isLoading: true };

export const Required = Template.bind({});
Required.args = { required: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const FullWidth = Template.bind({});
FullWidth.args = { fullWidth: true };

export const SingleChoiceEmpty = SingleChoiceTemplate.bind({});
SingleChoiceEmpty.args = {};

export const SingleChoicePopulated = SingleChoiceTemplate.bind({});
SingleChoicePopulated.args = { populateIfSingleChoice: true };

export const Multiple = Template.bind({});
Multiple.args = { initialValues: { favoriteColor: [] }, multiple: true };
