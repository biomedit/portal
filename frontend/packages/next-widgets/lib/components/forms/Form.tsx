import {
  type FieldValues,
  type SubmitHandler,
  useFormContext,
} from 'react-hook-form';
import React, { type ReactElement, type ReactNode } from 'react';

export type FormProps<T extends FieldValues> = Omit<
  React.DetailedHTMLProps<
    React.FormHTMLAttributes<HTMLFormElement>,
    HTMLFormElement
  >,
  'onSubmit'
> & {
  children: ReactNode;
  onSubmit: SubmitHandler<T>;
};

export function Form<T extends FieldValues>({
  onSubmit,
  children,
  ...formProps
}: FormProps<T>): ReactElement {
  const { handleSubmit } = useFormContext<T>();
  return (
    <form onSubmit={handleSubmit(onSubmit)} {...formProps}>
      {children}
    </form>
  );
}
