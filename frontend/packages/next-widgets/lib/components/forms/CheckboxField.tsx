import {
  type EnhancedArrayFieldProps,
  type EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
} from './hooks';
import { FormControl, FormHelperText } from '@mui/material';
import React, { type ReactElement, type ReactNode, useState } from 'react';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import { HiddenArrayField } from './HiddenField';
import { useFormContext } from 'react-hook-form';

export type CheckboxArrayFieldProps = EnhancedArrayFieldProps & {
  disabled: boolean;
  disabledTitle?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
};

export const CheckboxArrayField = ({
  disabled,
  disabledTitle,
  onChange,
  required,
  ...props
}: CheckboxArrayFieldProps): ReactElement => {
  const { name, defaultValue, register } = useEnhancedArrayField(props);
  const { ref, ...restProps } = register(name, {
    disabled,
    required,
    onChange,
  });

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenArrayField {...props} />
      )}
      <Checkbox
        defaultChecked={defaultValue}
        {...restProps}
        ref={disabled ? undefined : ref}
        disabled={disabled}
        title={disabled ? disabledTitle : undefined}
      />
    </>
  );
};

export type CheckboxFieldProps<TInitialValues extends Record<string, unknown>> =
  EnhancedFieldProps<TInitialValues> & {
    autofocus?: boolean;
    className?: string;
    disabled?: boolean;
    label: ReactNode;
    name: string;
    required?: boolean;
  };

export function CheckboxField<TInitialValues extends Record<string, unknown>>({
  name,
  label,
  initialValues,
  autofocus,
  disabled,
  className,
  required,
  ...checkboxFieldProps
}: CheckboxFieldProps<TInitialValues>): ReactElement {
  const { register } = useFormContext();
  const { defaultValue, setValue, error } = useEnhancedField<TInitialValues>({
    name,
    initialValues,
  });

  const [selectedValue, setSelectedValue] = useState<boolean>(!!defaultValue);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(name, event.target.checked);
    setSelectedValue(event.target.checked);
  };
  const { ref, ...rest } = register(name, { required, onChange, disabled });

  const errorStyle = { color: 'red' };

  return (
    <FormControl error={!!error} variant="standard">
      <FormControlLabel
        sx={error ? errorStyle : undefined}
        control={
          <Checkbox
            {...checkboxFieldProps}
            {...rest}
            sx={error ? errorStyle : undefined}
            slotProps={{ input: { ref } }}
            checked={selectedValue}
            autoFocus={autofocus}
          />
        }
        label={label}
        className={className}
      />
      {!!error && <FormHelperText>Required</FormHelperText>}
    </FormControl>
  );
}
