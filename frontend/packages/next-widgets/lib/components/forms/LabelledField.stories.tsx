import type { Meta, StoryFn } from '@storybook/react';
import { FormProvider } from 'react-hook-form';
import { LabelledField } from './LabelledField';
import React from 'react';
import { Typography } from '@mui/material';
import { useEnhancedForm } from './hooks';

export default {
  title: 'Components/Forms/LabelledField',
  component: LabelledField,
  // Workaround for non-working `Date Field (Non) Empty` stories
  // See https://github.com/storybookjs/storybook/issues/12747 for details
  parameters: { docs: { source: { type: 'code' } } },
} as Meta<typeof LabelledField>;

const Template: StoryFn<typeof LabelledField> = (args) => {
  const form = useEnhancedForm();

  const text = form.watch('labelledField');
  return (
    <FormProvider {...form}>
      <LabelledField
        {...args}
        type={args.type}
        name="labelledField"
        label={'Your Labelled Field'}
      />
      {args.type == 'date' && text && <Typography>{text}</Typography>}
    </FormProvider>
  );
};

export const TextFieldEmpty = Template.bind({});
TextFieldEmpty.args = {};

export const TextFieldWithError = Template.bind({});
TextFieldWithError.args = {
  fieldError: {
    type: 'validate',
    message: 'Looks like you left this field empty',
  },
};

export const TextFieldNonEmpty = Template.bind({});
TextFieldNonEmpty.args = {
  initialValues: {
    labelledField:
      'your very long content for this field which uses the full width of your screen',
  },
  fullWidth: true,
};

export const DateFieldEmpty = Template.bind({});
DateFieldEmpty.args = {
  label: 'Your date field',
  type: 'date',
  format: 'DD.MM.YYYY',
};

export const DateFieldNonEmpty = Template.bind({});
DateFieldNonEmpty.args = {
  type: 'date',
  label: 'Your prefilled date field',
  initialValues: {
    labelledField: new Date('2022-03-17'),
  },
  format: 'DD.MM.YYYY',
};

export const DateFieldWithError = Template.bind({});
DateFieldWithError.args = {
  type: 'date',
  label: 'Wrong date input',
  initialValues: {
    labelledField: 'some text',
  },
  fieldError: {
    type: 'validate',
    message: 'Looks like you left this field empty',
  },
  format: 'DD.MM.YYYY',
};

export const UrlField = Template.bind({});
UrlField.args = {
  type: 'url',
};
