import { ColoredStatus, type Status } from './ColoredStatus';
import type { Meta, StoryFn } from '@storybook/react';
import React from 'react';

enum Outcome {
  DEFEAT = 'defeat',
  TIE = 'tie',
  VICTORY = 'victory',
}

const statuses: Status<Outcome>[] = [
  {
    color: 'success',
    value: Outcome.VICTORY,
    text: 'You won!',
  },
  {
    value: Outcome.TIE,
  },
  {
    color: 'error',
    value: Outcome.DEFEAT,
  },
];

export default {
  title: 'Components/ColoredStatus',
  component: ColoredStatus,
} as Meta<typeof ColoredStatus>;

const Template: StoryFn<typeof ColoredStatus> = (args) => {
  return <ColoredStatus {...args} statuses={statuses} />;
};

export const Default = Template.bind({});

export const WithoutColor = Template.bind({});
WithoutColor.args = { value: Outcome.TIE };

export const WithColor = Template.bind({});
WithColor.args = { value: Outcome.DEFEAT };

export const WithText = Template.bind({});
WithText.args = { value: Outcome.VICTORY };
