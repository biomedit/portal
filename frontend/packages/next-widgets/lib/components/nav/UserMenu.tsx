import * as React from 'react';
import { type MouseEventHandler, type ReactElement, useState } from 'react';
import AccountCircle from '@mui/icons-material/AccountCircle';
import IconButton from '@mui/material/IconButton';
import { Menu } from '@mui/material';
import { styled } from '@mui/material/styles';

const UserMenuBox = styled('div')(({ theme }) => ({
  paddingTop: theme.spacing(2),
  paddingBottom: theme.spacing(2),
  paddingLeft: theme.spacing(0.5),
  backgroundColor: theme.palette.grey[200],
}));

const UserIconContainer = styled('div')({
  position: 'relative',
});

type UserMenuProps = {
  children: React.ReactNode;
};

export const UserMenu = React.forwardRef<HTMLDivElement, UserMenuProps>(
  ({ children, ...userMenuProps }, ref): ReactElement => {
    const [anchorEl, setAnchorEl] = useState<Element | null>(null);
    const handleClick: MouseEventHandler<
      HTMLAnchorElement | HTMLButtonElement
    > = (event) => setAnchorEl(event.currentTarget as Element);
    const handleClose = () => setAnchorEl(null);

    return (
      <UserMenuBox>
        <UserIconContainer>
          <IconButton
            aria-owns={anchorEl ? 'user-menu' : undefined}
            aria-haspopup="true"
            onClick={handleClick}
            size="large"
            sx={{
              zIndex: 0,
              marginRight: 1,
              color: 'primary.dark',
            }}
          >
            <AccountCircle />
          </IconButton>
        </UserIconContainer>
        <Menu
          id="user-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          ref={ref}
          {...userMenuProps}
        >
          {children}
        </Menu>
      </UserMenuBox>
    );
  },
);
UserMenu.displayName = 'UserMenu';
