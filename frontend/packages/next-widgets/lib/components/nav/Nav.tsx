import { Drawer, List } from '@mui/material';
import React, { type PropsWithChildren, type ReactElement } from 'react';

export type NavProps = {
  backgroundColor?: string;
  color?: string;
  width?: number;
};

export const Nav = ({
  children,
  color = 'white',
  width = 230,
  backgroundColor = 'primary.main',
}: PropsWithChildren<NavProps>): ReactElement => {
  return (
    <Drawer
      variant="permanent"
      sx={{
        display: 'flex',
        width,
        '& .MuiPaper-root': {
          color,
          backgroundColor,
          width,
          borderRightWidth: 0,
        },
      }}
    >
      <nav aria-label="main navigation">
        <List
          sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '100vh',
            '& .MuiListItemButton-root': {
              flexGrow: 0,
            },
          }}
        >
          {children}
        </List>
      </nav>
    </Drawer>
  );
};
