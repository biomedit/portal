import type { Meta, StoryFn } from '@storybook/react';
import { legacy_createStore } from 'redux';
import { ListItemAction } from '../list';
import { Provider } from 'react-redux';
import React from 'react';
import { toast } from '../../reducers';
import { UserMenu } from './UserMenu';
import { WarningIcon } from '../../widgets';

export default {
  title: 'Components/Nav/UserMenu',
  component: UserMenu,
} as Meta<typeof UserMenu>;

const Template: StoryFn<typeof UserMenu> = () => {
  return (
    <Provider store={legacy_createStore(toast)}>
      <UserMenu>
        <ListItemAction
          action={{ type: 'NOOP' }}
          primary={'Test'}
          icon={<WarningIcon />}
        />
      </UserMenu>
    </Provider>
  );
};

export const Default = Template.bind({});
Default.args = {};
