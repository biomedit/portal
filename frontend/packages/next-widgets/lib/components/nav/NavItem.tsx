import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
} from '@mui/material';
import { ListItemLink, type ListItemLinkProps } from '../list';
import React, { type ReactElement, useMemo } from 'react';
import { styled, type Theme } from '@mui/material/styles';
import ExpandMoreIcon from '@mui/icons-material/ArrowForwardIosSharp';
import type { SubItem } from '../../structure';

const PREFIX = 'NavItem';

const classes = {
  menuItem: `${PREFIX}-menuItem`,
  menuIconStyles: `${PREFIX}-menuIconStyles`,
};

const StyledListItemLink = styled(ListItemLink, {
  shouldForwardProp: (prop) =>
    !['textColor', 'backgroundColor'].includes(String(prop)),
})<NavItemProps>(({ theme, textColor, backgroundColor }) => {
  const navItemTextColor = useMemo(
    () => (textColor ? textColor(theme) : theme.palette.primary.contrastText),
    [textColor, theme],
  );
  const navItemBackgroundColor = useMemo(
    () =>
      backgroundColor ? backgroundColor(theme) : theme.palette.primary.light,
    [backgroundColor, theme],
  );
  return {
    [`& .${classes.menuItem}`]: {
      color: navItemTextColor,
      fontSize: 18,
      textDecoration: 'none',
      textDecorationStyle: 'unset',
    },
    [`& .${classes.menuIconStyles}`]: {
      color: navItemTextColor,
    },
    '&.Mui-selected': {
      backgroundColor: navItemBackgroundColor,
    },
    '&.Mui-selected:hover': {
      backgroundColor: navItemBackgroundColor,
    },
  };
});

export type NavItemProps = ListItemLinkProps & {
  backgroundColor?: (theme: Theme) => string;
  textColor?: (theme: Theme) => string;
};

export const NavItem = (navItemProps: NavItemProps): ReactElement => {
  return (
    <StyledListItemLink
      {...navItemProps}
      textClasses={{ primary: classes.menuItem }}
      iconClasses={{ root: classes.menuIconStyles }}
    />
  );
};

export const CollapsibleNavItem = ({
  items,
  getSelected,
  ...navItemProps
}: NavItemProps & {
  getSelected: (item: SubItem) => boolean;
  items: SubItem[];
}) => {
  return (
    <Accordion sx={{ boxShadow: 'none' }} disableGutters>
      <AccordionSummary
        sx={{
          ...{
            backgroundColor: items.some(getSelected)
              ? 'primary.light'
              : 'primary.main',
            paddingLeft: 0,
          },
          ...{
            '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
              transform: 'rotate(90deg)',
            },
            '& .MuiAccordionSummary-content': { marginY: 0 },
          },
        }}
        expandIcon={<ExpandMoreIcon sx={{ color: 'white' }} />}
      >
        <StyledListItemLink
          textClasses={{ primary: classes.menuItem }}
          iconClasses={{ root: classes.menuIconStyles }}
          {...navItemProps}
        />
      </AccordionSummary>
      <AccordionDetails sx={{ padding: 0, backgroundColor: 'primary.main' }}>
        {items.map((item) => (
          <NavItem
            selected={getSelected(item)}
            href={item.menu?.link ?? '#'}
            key={'MenuItem-' + item.title}
            // Hack to indent the subitems as the NavItem component does not
            // accept the sx prop.
            icon={<Box sx={{ width: 24 }} />}
            primary={item.title}
          />
        ))}
      </AccordionDetails>
    </Accordion>
  );
};
