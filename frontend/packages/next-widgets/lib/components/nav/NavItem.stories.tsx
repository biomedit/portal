import { CollapsibleNavItem, NavItem } from './NavItem';
import type { CollapsibleStructureItem, StructureItem } from '../../structure';
import { HomeIcon, MessageIcon, UnarchiveIcon } from '../../widgets';
import type { Meta, StoryFn } from '@storybook/react';
import { Nav } from './Nav';
import React from 'react';
import type { Theme } from '@mui/material';

const homeLink = '/';

const structureItems: StructureItem[] = [
  {
    title: 'Home',
    menu: {
      link: homeLink,
      icon: <HomeIcon />,
    },
  },
  {
    title: 'Contact',
    menu: {
      link: '/contact',
      icon: <MessageIcon />,
    },
  },
];

const collapsibleStructureItem: CollapsibleStructureItem<StructureItem> = {
  title: 'Collapsible',
  menu: {
    icon: <UnarchiveIcon />,
  },
  subItems: structureItems,
};

export default {
  title: 'Components/Nav/NavItem',
  component: NavItem,
} as Meta<typeof NavItem>;

const Template: StoryFn = (args) => {
  return (
    <Nav>
      {structureItems.map((item) => (
        <NavItem
          {...args}
          selected={item.menu?.link == homeLink}
          href={item.menu?.link ?? '#'}
          key={'MenuItem-' + item.title}
          primary={item.title}
          icon={item.menu?.icon}
        />
      ))}
    </Nav>
  );
};

const CollapsibleTemplate: StoryFn = () => {
  return (
    <Nav>
      <NavItem href={'#'} primary={'Home'} icon={<HomeIcon />} />
      <CollapsibleNavItem
        primary={collapsibleStructureItem.title}
        href={'#'}
        items={collapsibleStructureItem.subItems}
        icon={collapsibleStructureItem.menu?.icon}
        getSelected={(item: StructureItem) => item.menu?.link == homeLink}
      />
    </Nav>
  );
};

export const Default = Template.bind({});
Default.args = {};

export const WithCustomColors = Template.bind({});
WithCustomColors.args = {
  textColor: (theme: Theme) => theme.palette.secondary.light,
  backgroundColor: (theme: Theme) => theme.palette.secondary.main,
};

export const Collapsible = CollapsibleTemplate.bind({});
