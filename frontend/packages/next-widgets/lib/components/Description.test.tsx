import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Description } from './Description';
import type { FormattedItemField } from './list';

const caption = 'First Name';
const component = 'Chuck';
const entries: FormattedItemField[] = [
  { caption: caption, component: component },
];
const title = 'Registration Form';
const labelWidth = '10%';
const styleRules = {
  caption: {
    padding: 0,
  },
};

describe('Description', function () {
  describe('Component', function () {
    it('should render the component without errors or warnings', async () => {
      render(
        <Description
          title={title}
          entries={entries}
          labelWidth={labelWidth}
          styleRules={styleRules}
        />,
      );

      await screen.findByText(title);
      await screen.findByText(caption);
      await screen.findByText(component);
    });
  });
});
