import React, { type CSSProperties, type ReactElement } from 'react';
// Circular dependency: do NOT simplify this import!
import type { FormattedItemField } from './list/ListHooks';
import { styled } from '@mui/material/styles';
import type { ThemeProps } from '../../types';
import { Typography } from '@mui/material';

/**
 * `ownerState` is a reserved object for props which should only be passed to
 * the `styled(...)` function and not to the underlying component.
 * Used to customize the styling of styled components through the usage of props.
 */
type StyleRulesProps = Record<
  'ownerState',
  Pick<DescriptionProps, 'styleRules'>
>;

const padding = 2;

const StyledTable = styled('table')(
  ({ ownerState: { styleRules } }: StyleRulesProps) => ({
    width: '100%',
    ...(styleRules?.table ?? {}),
  }),
);

const StyledRow = styled('tr')(
  ({ ownerState: { styleRules } }: StyleRulesProps) => ({
    ...(styleRules?.row ?? {}),
  }),
);

type StyledCaptionCellProps = Record<
  'ownerState',
  Pick<DescriptionProps, 'labelWidth'>
> &
  StyleRulesProps &
  ThemeProps;

const StyledCaptionCell = styled('td')(
  ({
    theme,
    ownerState: { styleRules, labelWidth },
  }: StyledCaptionCellProps) => ({
    width: labelWidth,
    verticalAlign: styleRules?.caption?.verticalAlign ?? 'baseline',
    padding: theme?.spacing(padding),
    ...(styleRules?.caption ?? {}),
  }),
);

type StyledValueCellProps = Record<
  'ownerState',
  Pick<FormattedItemField, 'caption'>
> &
  StyleRulesProps &
  ThemeProps;

const StyledValueCell = styled('td')(
  ({ theme, ownerState: { styleRules, caption } }: StyledValueCellProps) =>
    caption
      ? {
          padding: theme?.spacing(padding),
          ...(styleRules?.value ?? {}),
        }
      : { padding: 0, ...(styleRules?.valueOnly ?? {}) },
);

type DescriptionProps = {
  entries: FormattedItemField[] | null;
  labelWidth: number | string;
  styleRules?: Partial<
    Record<'caption' | 'row' | 'table' | 'value' | 'valueOnly', CSSProperties>
  >;
  title?: string;
};

export const Description = ({
  title,
  entries,
  labelWidth,
  styleRules,
}: DescriptionProps): ReactElement | null => {
  if (!entries) {
    return null;
  }

  return (
    <div>
      {title && <Typography variant="h6">{title}</Typography>}
      {entries && (
        <StyledTable ownerState={{ styleRules }}>
          <tbody>
            {entries.map(({ key, caption, component }: FormattedItemField) => (
              <StyledRow
                key={'description-tr-' + caption + '-' + key}
                ownerState={{ styleRules }}
              >
                {caption && (
                  <StyledCaptionCell
                    key={'description-td-caption-' + caption + '-' + key}
                    ownerState={{ styleRules, labelWidth }}
                  >
                    <Typography variant="subtitle1">{caption}</Typography>
                  </StyledCaptionCell>
                )}
                <StyledValueCell
                  key={'description-td-value-' + caption + '-' + key}
                  colSpan={caption ? 1 : 2}
                  ownerState={{ styleRules, caption }}
                >
                  <Typography component="div">{component}</Typography>
                </StyledValueCell>
              </StyledRow>
            ))}
          </tbody>
        </StyledTable>
      )}
    </div>
  );
};
