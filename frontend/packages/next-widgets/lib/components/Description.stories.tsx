import type { Meta, StoryFn } from '@storybook/react';
import { CopyToClipboardPasswordField } from './CopyToClipboardPasswordField';
import { CopyToClipboardTextField } from './CopyToClipboardTextField';
import { Description } from './Description';
import type { FormattedItemField } from './list';
import React from 'react';

export default {
  title: 'Components/Description',
  component: Description,
} as Meta<typeof Description>;

const Template: StoryFn<typeof Description> = (args) => (
  <Description {...args} entries={entries} labelWidth={labelWidth} />
);

const TemplateWithComplexComponents: StoryFn<typeof Description> = (args) => (
  <Description {...args} entries={entries2} labelWidth={labelWidth} />
);

const entries: FormattedItemField[] = [
  { component: 'Dr.' },
  { caption: 'First Name', component: 'Chuck' },
  { caption: 'Middle Name', component: '' },
  { caption: 'Last Name', component: 'Norris' },
];
const entries2: FormattedItemField[] = [
  { component: 'Dr.' },
  {
    caption: 'Copy To Clipboard',
    component: (
      <CopyToClipboardTextField value="Some text to copy" label="Text" />
    ),
  },
  { caption: 'First Name', component: 'Chuck' },
  {
    caption: 'Password',
    component: (
      <CopyToClipboardPasswordField
        value="Some text to copy"
        label="Password"
      />
    ),
  },
];
const title = 'Registration Form';
const labelWidth = '15%';
const styleRules = {
  caption: {
    padding: 0,
    verticalAlign: 'middle',
  },
  value: {
    padding: 0,
  },
  row: {
    color: 'red',
    height: '4rem',
  },
  table: {
    width: '100%',
    borderSpacing: 0,
  },
};

export const Default = Template.bind({});

export const Title = Template.bind({});
Title.args = { title };

export const StylesRules = TemplateWithComplexComponents.bind({});
StylesRules.args = { styleRules };
