import { Box, DialogContentText } from '@mui/material';
import { Dialog, type DialogProps } from '../../widgets';
import React, {
  type ReactElement,
  useCallback,
  useMemo,
  useState,
} from 'react';
import type { Optional } from 'utility-types';
import TextField from '@mui/material/TextField';

export type ConfirmDialogProps = Optional<DialogProps, 'title'> & {
  confirmationHelper?: React.ReactNode;
  confirmationText?: string;
  onConfirm: (isConfirmed: boolean) => void;
};

/**
 * Dialog to confirm, requiring the user to enter the {@code confirmationText}
 * into a text field in order to be able to confirm.
 * @param title of the dialog
 * @param text to appear after {@code title}
 * @param confirmationHelper text which appears above the text field where the
 *                         user needs to enter {@code confirmationText} to be
 *                         able to confirm
 * @param confirmationText text that the user needs to enter to be able to confirm
 * @param onConfirm method to be called if the user either dismisses the dialog
 *                 (in which case "isConfirmed" will be "false") or confirms
 *                 (in which case "isConfirmed" will be "true").
 * @param other all other props passed in
 * @constructor
 */
export const ConfirmDialog = ({
  title,
  text,
  confirmationText,
  confirmationHelper,
  onConfirm,
  ...other
}: ConfirmDialogProps): ReactElement => {
  const [confirmed, setConfirmed] = useState<boolean>(false);
  const onChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
    (event) => {
      setConfirmed(event.target.value === confirmationText);
    },
    [confirmationText],
  );

  const disabled = useMemo(() => {
    if (confirmationText) {
      return !confirmed;
    }
    return false;
  }, [confirmationText, confirmed]);

  return (
    <Dialog
      {...other}
      title={title}
      text={text}
      confirmButtonProps={{
        autoFocus: !confirmationText,
        onClick: () => onConfirm(true),
        disabled,
      }}
      onClose={() => onConfirm(false)}
    >
      {confirmationText && (
        <>
          <DialogContentText sx={{ marginBottom: 0.5, marginTop: 1.5 }}>
            {confirmationHelper ?? `Please type the following to confirm:`}
          </DialogContentText>
          <Box
            fontFamily="Monospace"
            fontSize="body1.fontSize"
            sx={{ marginBottom: 1.5 }}
          >
            {confirmationText}
          </Box>
          <TextField
            autoFocus={true}
            size="small"
            onChange={onChange}
            variant="outlined"
            fullWidth
          />
        </>
      )}
    </Dialog>
  );
};
