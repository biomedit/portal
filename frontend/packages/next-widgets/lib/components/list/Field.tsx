import React, {
  type PropsWithChildren,
  type ReactElement,
  type ReactNode,
} from 'react';
import { formatDate } from '../../utils';
import type { IdType } from '../../../types';
import type { ListModelField } from './ListHooks';

type ComponentFieldProps<ModelData, RenderProps> = {
  caption?: string;
  getProperty: (modelData: ModelData) => RenderProps;
  hideIf?: (modelData: ModelData) => boolean;
  key: string;
  render?: React.FunctionComponent<RenderProps>;
  tooltip?: string;
};

export function Field<ModelData, RenderProps>(
  props: ComponentFieldProps<ModelData, RenderProps>,
): ListModelField<ModelData> {
  const { render, getProperty, ...rest } = props;
  return {
    render: (modelData: ModelData) =>
      (render ?? renderLabel)(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore TODO
        getProperty(modelData) as PropsWithChildren<RenderProps>,
      ),
    ...rest,
  };
}

export function renderLabel(data: Date | ReactNode): ReactElement {
  return <>{data instanceof Date ? formatDate(data) : data}</>;
}

export function renderFieldArray<RenderProps extends IdType>(
  key: string,
  renderItem: (item: RenderProps) => React.ReactNode,
): (data: RenderProps[]) => ReactElement {
  function _FieldArray(data: RenderProps[]): ReactElement {
    return (
      <ul
        style={{
          listStyleType: 'none',
          padding: 0,
          marginBlockStart: 0,
          marginBlockEnd: 0,
        }}
      >
        {data.map((item) => (
          <li key={key + '-' + item.id}>{renderItem(item)}</li>
        ))}
      </ul>
    );
  }

  return _FieldArray;
}
