import { ConfirmDialog, type ConfirmDialogProps } from './ConfirmDialog';
import React, { type ReactElement } from 'react';
import { itemNameGeneric } from '../../widgets';

export type DeleteDialogProps = Omit<
  ConfirmDialogProps,
  'onConfirm' | 'text' | 'title'
> & {
  itemName?: string;
  onDelete: (isConfirmed: boolean) => void;
};

export const DeleteDialog = ({
  itemName,
  onDelete,
  ...other
}: DeleteDialogProps): ReactElement => {
  const title = `Delete ${itemName ? `${itemName} ` : ''}Confirmation`;
  const text = `You are about to permanently delete this
  ${itemName ?? itemNameGeneric}. Once it is permanently deleted, it cannot be
  recovered. This action cannot be undone.`;

  return (
    <ConfirmDialog {...other} title={title} text={text} onConfirm={onDelete} />
  );
};
