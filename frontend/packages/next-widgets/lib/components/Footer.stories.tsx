import type { Meta, StoryFn } from '@storybook/react';
import { Footer } from './Footer';
import React from 'react';

export default {
  component: Footer,
  title: 'Components/Footer',
} as Meta<typeof Footer>;

const Template: StoryFn<typeof Footer> = (args) => (
  <Footer
    {...args}
    color="black"
    appName="Portal"
    version="1.0.0"
    logoSrc="/sib_logo.svg"
    logoText="SIB Logo"
    logoWidth="52"
    logoHeight="40"
    logoHref="https://www.sib.swiss/"
    privacyHref="https://www.sib.swiss/privacy-policy/"
  />
);

export const Default = Template.bind({});
