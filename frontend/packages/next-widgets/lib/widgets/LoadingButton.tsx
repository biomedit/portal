import { Button, type ButtonProps, CircularProgress } from '@mui/material';
import React, { type ReactElement } from 'react';
import { styled } from '@mui/material/styles';

const Wrapper = styled('div')(() => ({
  position: 'relative',
  display: 'inline-block',
}));

export const LoadingButton = ({
  loading,
  children,
  disabled,
  ...buttonProps
}: ButtonProps): ReactElement => {
  return (
    <Wrapper>
      <Button {...buttonProps} disabled={loading || disabled}>
        {children}
      </Button>
      {loading && (
        <CircularProgress
          size={24}
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -1.5,
            marginLeft: -1.5,
          }}
        />
      )}
    </Wrapper>
  );
};
