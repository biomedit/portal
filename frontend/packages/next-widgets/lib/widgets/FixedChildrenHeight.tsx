import { Box, type BoxProps } from '@mui/material';
import React, { type ReactElement, type ReactNode } from 'react';

type FixedChildrenHeightProps = BoxProps & {
  childHeight?: number | string;
  children: ReactNode[];
};

/**
 * Wraps any non-falsey child into a div and sets the height.
 *
 * This is especially useful for components with helper text: it avoids the
 * jumping when the error text becomes visible.
 */
export const FixedChildrenHeight = React.memo(
  ({
    children,
    childHeight = '5rem',
    ...boxProps
  }: FixedChildrenHeightProps): ReactElement => (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        '&> *': {
          minHeight: childHeight,
          marginRight: 'auto',
        },
      }}
      {...boxProps}
    >
      {children}
    </Box>
  ),
);

FixedChildrenHeight.displayName = 'FixedChildrenHeight';
