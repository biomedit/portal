import React, { type ReactElement } from 'react';

export type ExternalLinkProps = React.ComponentPropsWithoutRef<'a'> & {
  sameTab?: boolean;
  to: string;
};

export const ExternalLink = ({
  to,
  sameTab,
  children,
  ...props
}: ExternalLinkProps): ReactElement => {
  return (
    <a
      href={to}
      target={sameTab ? undefined : '_blank'}
      rel="noopener noreferrer"
      {...props}
    >
      {children}
    </a>
  );
};
