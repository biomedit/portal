import { Alert, type AlertProps } from '@mui/material';
import { clearToast, ToastSeverity, type ToastState } from '../reducers';
import React, { type ReactElement, useCallback } from 'react';
import Snackbar, { type SnackbarProps } from '@mui/material/Snackbar';
import { useDispatch, useSelector } from 'react-redux';
import type { DefaultRootState } from '../../types';
import { mailto } from '../utils';
import type { SnackbarCloseReason } from '@mui/material/Snackbar/Snackbar';

export type InternalErrorContentProps = ToastBarProps & {
  correlationId: string;
};

export const InternalErrorContent = ({
  correlationId,
  subjectPrefix,
  contactEmail,
}: InternalErrorContentProps): ReactElement => {
  return (
    <span>
      An unexpected error occurred. Please{' '}
      <a
        href={mailto(
          contactEmail ?? '',
          `${subjectPrefix}Unexpected Error`,
          `Correlation ID: ${correlationId}

Reproduction steps:
(please explain step by step what you did before the error happened)

Expected behavior:
(please explain what you would expect to happen if everything is working correctly)

Actual behavior:
(please explain what happened instead of what you expected)`,
        )}
      >
        contact us
      </a>{' '}
      about this issue and refer to this ID: {correlationId}
    </span>
  );
};

interface ToastBarSnackBarProps
  extends Pick<AlertProps, 'children' | 'severity'>,
    Pick<SnackbarProps, 'open' | 'autoHideDuration'> {
  onClose?: (
    event?: Event | React.SyntheticEvent,
    reason?: SnackbarCloseReason,
  ) => void;
  withCloseButton?: boolean;
}

interface DismissibleToastBarSnackBarProps extends ToastBarSnackBarProps {
  withCloseButton: true;
}

interface NonDismissibleToastBarSnackBarProps extends ToastBarSnackBarProps {
  withCloseButton?: false;
  autoHideDuration: number;
}

type ToastBarSnackBarPropsType =
  | DismissibleToastBarSnackBarProps
  | NonDismissibleToastBarSnackBarProps;

export const ToastBarSnackBar = ({
  severity,
  open,
  onClose,
  withCloseButton,
  children,
  autoHideDuration,
}: ToastBarSnackBarPropsType) => {
  return (
    <Snackbar
      open={open}
      onClose={onClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      autoHideDuration={autoHideDuration}
    >
      <Alert
        onClose={
          withCloseButton || severity === ToastSeverity.ERROR
            ? onClose
            : undefined
        }
        elevation={6}
        variant="filled"
        severity={severity}
        sx={{
          '& a': {
            color: 'white',
          },
        }}
      >
        {children}
      </Alert>
    </Snackbar>
  );
};

type ToastBarProps = {
  contactEmail: string;
  subjectPrefix: string;
};

export function ToastBar({
  subjectPrefix,
  contactEmail,
}: ToastBarProps): ReactElement {
  const dispatch = useDispatch();

  const { message, severity, correlationId } = useSelector<
    DefaultRootState,
    ToastState
  >((state) => state.toast);

  const handleSnackBarClose = useCallback(
    (_event?: Event | React.SyntheticEvent, reason?: SnackbarCloseReason) => {
      if (reason === 'clickaway' && severity === ToastSeverity.ERROR) {
        return;
      }
      dispatch(clearToast());
    },
    [severity, dispatch],
  );

  return (
    <ToastBarSnackBar
      open={message !== undefined || correlationId !== undefined}
      onClose={handleSnackBarClose}
      severity={severity}
      autoHideDuration={6000}
    >
      {correlationId ? (
        <InternalErrorContent
          correlationId={correlationId}
          subjectPrefix={subjectPrefix}
          contactEmail={contactEmail}
        />
      ) : (
        <span>{message}</span>
      )}
    </ToastBarSnackBar>
  );
}
