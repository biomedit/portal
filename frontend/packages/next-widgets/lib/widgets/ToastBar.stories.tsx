import { combineReducers, legacy_createStore } from 'redux';
import type { Meta, StoryObj } from '@storybook/react';
import { Provider, useDispatch } from 'react-redux';
import React, { useState } from 'react';
import { setToast, type Toast, toast, ToastSeverity } from '../reducers';
import { Button } from './Buttons';
import { ToastBar } from './ToastBar';

const Template = ({ severity }: Pick<Toast, 'severity'>) => {
  const [counter, setCounter] = useState<number>(0);
  const dispatch = useDispatch();

  return (
    <div>
      <ToastBar subjectPrefix={'blurb'} contactEmail={'blerb'} />
      <Button
        onClick={() => {
          dispatch(
            setToast({
              message: `Crunchy toast number ${counter} incoming!`,
              severity: severity,
            }),
          );
          setCounter(counter + 1);
        }}
      >
        Make Toast!
      </Button>
    </div>
  );
};

export default {
  title: 'Widgets/ToastBar',
  component: Template,
  decorators: [
    (Story) => (
      <Provider store={legacy_createStore(combineReducers({ toast }))}>
        <Story />
      </Provider>
    ),
  ],
} as Meta<typeof Template>;

type Story = StoryObj<typeof Template>;

export const Error: Story = { args: { severity: ToastSeverity.ERROR } };
export const Warning: Story = { args: { severity: ToastSeverity.WARNING } };
export const Info: Story = { args: { severity: ToastSeverity.INFO } };
export const Success: Story = { args: { severity: ToastSeverity.SUCCESS } };
