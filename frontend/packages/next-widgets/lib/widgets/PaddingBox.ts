import { styled } from '@mui/material/styles';

export const PaddingBox = styled('div')(() => ({
  padding: 30,
}));
