import { marked, type MarkedOptions } from 'marked';
import React, { type ReactElement, useMemo } from 'react';

export type MarkdownProps = {
  openLinksInNewTab?: boolean;
  options?: MarkedOptions;
  text: string;
};

/**
 * NOTE: {@code text} MUST be sanitized and trusted, no input normalization will be performed!
 * Renders {@code text} as markdown.
 * @constructor
 */
export const Markdown = ({
  text,
  openLinksInNewTab,
}: MarkdownProps): ReactElement => {
  const renderedMarkdown = useMemo(() => {
    if (!text) {
      return '';
    }
    const html = marked.parse(text, { async: false }) as string;
    if (openLinksInNewTab) {
      return html.replace(
        new RegExp(/<a href="/g),
        '<a target="_blank" href="',
      );
    }
    return html;
  }, [text, openLinksInNewTab]);
  return <span dangerouslySetInnerHTML={{ __html: renderedMarkdown }} />;
};
