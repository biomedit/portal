import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { EmailLink } from './EmailLink';

describe('EmailLink', () => {
  describe('Component', () => {
    const email = 'a@a.com';
    const text = 'Contact us!';

    it('should show the email address as the text if no children are passed in', () => {
      // when
      render(<EmailLink email={email} />);

      // then
      screen.getByText(email);
    });

    it('should show the children if they are defined as the text', () => {
      // when
      render(<EmailLink email={email}>{text}</EmailLink>);

      // then
      screen.getByText(text);
      expect(screen.queryByText(email)).not.toBeInTheDocument();
    });
  });
});
