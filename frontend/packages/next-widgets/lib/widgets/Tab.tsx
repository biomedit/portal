import {
  Box,
  type BoxProps,
  Tab as MuiTab,
  Tabs as MuiTabs,
  Typography,
} from '@mui/material';
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import isString from 'lodash/isString';

export type TabItem = {
  content: React.ReactNode;
  id: string;
  label: string;
};

function getTabId(tabePaneId: string, tabId: string) {
  return `tab-${tabePaneId}-${tabId}`;
}

function getTabPanelId(tabPaneId: string, tabId: string) {
  return `tabpanel-${tabPaneId}-${tabId}`;
}

function a11yProps(tabPaneId: string, tabId: string) {
  const id = getTabId(tabPaneId, tabId);
  return {
    key: id,
    id,
    'aria-controls': getTabPanelId(tabPaneId, tabId),
  };
}

export type TabPaneProps = {
  id: string;
  label: string;
  model: TabItem[];
  panelBoxProps?: BoxProps;
  selected?: string;
};

export const TabPane = ({
  model,
  label,
  id,
  selected,
  panelBoxProps,
}: TabPaneProps): ReactElement => {
  const tabPaneId = id;

  const index = useMemo(() => {
    const tabIds = model.map((tab) => tab.id);
    return selected ? tabIds.indexOf(selected) : -1;
  }, [model, selected]);

  const [selectedTab, setSelectedTab] = useState<number>(
    index > -1 ? index : 0,
  );

  useEffect(() => {
    if (index > -1) {
      setSelectedTab(index);
    }
  }, [index]);

  const onTabChange = useCallback(
    (_event: React.SyntheticEvent, newTab: number) => {
      setSelectedTab(newTab);
    },
    [],
  );

  const { tabs, tabPanels } = useMemo(() => {
    const tabs: React.ReactNode[] = [];
    const tabPanels: React.ReactNode[] = [];

    for (let i = 0; i < model.length; i++) {
      const tab = model[i];
      const { key, ...rest } = a11yProps(tabPaneId, tab.id);
      tabs.push(
        <MuiTab
          label={tab.label}
          key={key}
          {...rest}
          sx={{ textTransform: 'capitalize' }}
        />,
      );
      tabPanels.push(
        <TabPanel
          key={getTabPanelId(tabPaneId, tab.id)}
          tabPaneId={tabPaneId}
          selectedTab={model[selectedTab].id}
          tabId={tab.id}
          boxProps={panelBoxProps}
        >
          {tab.content}
        </TabPanel>,
      );
    }

    return { tabs, tabPanels };
  }, [model, tabPaneId, selectedTab, panelBoxProps]);

  return (
    <>
      <MuiTabs
        key={`tab-${id}`}
        indicatorColor="primary"
        textColor="primary"
        value={selectedTab}
        onChange={onTabChange}
        aria-label={label}
      >
        {tabs}
      </MuiTabs>
      {tabPanels}
    </>
  );
};

interface TabPanelProps {
  boxProps?: BoxProps;
  children?: React.ReactNode;
  selectedTab: string;
  tabId: string;
  tabPaneId: string;
}

function TabPanel({
  tabPaneId,
  children,
  selectedTab,
  tabId,
  boxProps,
  ...other
}: TabPanelProps): ReactElement {
  const tabPanelId = getTabPanelId(tabPaneId, tabId);
  const content = useMemo(
    () =>
      isString(children) ? (
        <Typography variant="body2" key={'typography-' + tabPanelId}>
          {children}
        </Typography>
      ) : (
        children
      ),
    [tabPanelId, children],
  );
  return (
    <div
      role="tabpanel"
      hidden={selectedTab !== tabId}
      key={'div-' + tabPanelId}
      id={tabPanelId}
      aria-labelledby={getTabId(tabPaneId, tabId)}
      {...other}
    >
      {selectedTab === tabId && (
        <Box p={3} key={'box-' + tabPanelId} {...boxProps}>
          {content}
        </Box>
      )}
    </div>
  );
}
