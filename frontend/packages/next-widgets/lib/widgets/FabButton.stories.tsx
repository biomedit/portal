import type { Meta, StoryFn } from '@storybook/react';
import { FabButton } from './Buttons';
import React from 'react';

export default {
  title: 'Widgets/FabButton',
  component: FabButton,
} as Meta<typeof FabButton>;

const Template: StoryFn<typeof FabButton> = (args) => {
  return (
    <FabButton
      {...args}
      icon="download"
      tooltip="Download"
      // eslint-disable-next-line no-console
      onClick={(): void => console.log('Download')}
    />
  );
};

export const Default = Template.bind({});
Default.args = {};

export const Small = Template.bind({});
Small.args = { size: 'small' };
