import {
  AddIconButton,
  Button,
  DeleteIconButton,
  EditIconButton,
  FabButton,
} from './Buttons';
import type { Meta, StoryFn } from '@storybook/react';
import React from 'react';

export default {
  title: 'Widgets/Buttons',
  component: Button,
} as Meta<typeof Button>;

const Template: StoryFn<typeof FabButton> = (args) => {
  return <FabButton {...args} />;
};

export const Default = Template.bind({});
Default.args = { icon: 'search' };

export const WithTooltip = Template.bind({});
WithTooltip.args = { ...Default.args, title: 'Tooltip' };

export const MediumButton = Template.bind({});
MediumButton.args = { ...Default.args, size: 'medium' };

export const SmallButton = Template.bind({});
SmallButton.args = { ...Default.args, size: 'small' };

export const Add: StoryFn<typeof Button> = () => {
  return <AddIconButton />;
};

export const Edit: StoryFn<typeof Button> = () => {
  return <EditIconButton />;
};

export const Delete: StoryFn<typeof Button> = () => {
  return <DeleteIconButton />;
};
