import { AddIconButton, DeleteIconButton, EditIconButton } from './Buttons';
import type { Meta, StoryFn } from '@storybook/react';
import { ButtonBox } from './ButtonBox';
import React from 'react';

export default {
  title: 'Widgets/ButtonBox',
  component: ButtonBox,
} as Meta<typeof ButtonBox>;

const Template: StoryFn<typeof ButtonBox> = () => {
  return (
    <ButtonBox>
      <AddIconButton />
      <EditIconButton />
      <DeleteIconButton />
    </ButtonBox>
  );
};

export const Default = Template.bind({});
Default.args = {};
