import { type ButtonProps, Typography } from '@mui/material';
import {
  default as MaterialUiDialog,
  type DialogProps as MaterialUiDialogProps,
} from '@mui/material/Dialog';
import React, {
  type ReactElement,
  type ReactNode,
  useCallback,
  useState,
} from 'react';
import Button from '@mui/material/Button';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { LoadingButton } from './LoadingButton';

export function useDialogState<S>(): {
  item: S | null;
  onClose: () => void;
  open: boolean;
  setItem: (newItem: S) => void;
} {
  const [item, setItem] = useState<S | null>(null);
  const [open, setOpen] = useState<boolean>(false);
  const setItemCopy = useCallback((newItem: S) => {
    setItem(newItem);
    setOpen(true);
  }, []);

  const onClose = useCallback(() => {
    setOpen(false);
  }, []);

  return { item, setItem: setItemCopy, onClose, open };
}

export type DialogProps = Omit<MaterialUiDialogProps, 'title'> & {
  cancelLabel?: string;
  children?: React.ReactNode;
  confirmButtonProps?: ButtonProps;
  confirmLabel?: string;
  isSubmitting: boolean;
  onClose?: () => void;
  text?: React.ReactNode | string;
  title: React.ReactNode | string;
};

export enum ConfirmLabel {
  CONFIRM = 'Confirm',
  SAVE = 'Save',
}

export enum CancelLabel {
  CANCEL = 'Cancel',
  CLOSE = 'Close',
}

type DialogSubtitleProps = { children: ReactNode };

export const DialogSubtitle = ({
  children,
}: DialogSubtitleProps): ReactElement => {
  return (
    <DialogContentText
      sx={{
        color: 'common.black',
        paddingTop: 2,
      }}
    >
      {children}
    </DialogContentText>
  );
};

export const Dialog = ({
  title,
  text = undefined,
  confirmLabel = ConfirmLabel.CONFIRM,
  cancelLabel = CancelLabel.CANCEL,
  children,
  isSubmitting,
  confirmButtonProps,
  onClose,
  ...other
}: DialogProps): ReactElement => (
  <MaterialUiDialog
    aria-labelledby="dialog-title"
    aria-describedby="dialog-description"
    onClose={onClose}
    {...other}
  >
    <DialogTitle id="dialog-title">
      <Typography variant="h5" component="span">
        {title}
      </Typography>
    </DialogTitle>
    <DialogContent>
      {text && (
        <DialogContentText id="dialog-description">{text}</DialogContentText>
      )}
      {children}
    </DialogContent>
    <DialogActions sx={{ margin: 1 }}>
      {onClose && (
        <Button color="primary" onClick={onClose}>
          {cancelLabel}
        </Button>
      )}
      {confirmButtonProps && (
        <LoadingButton
          sx={{ marginLeft: 1 }}
          type="submit"
          form="dialog-form"
          color="primary"
          variant="outlined"
          loading={isSubmitting}
          {...confirmButtonProps}
        >
          {confirmLabel}
        </LoadingButton>
      )}
    </DialogActions>
  </MaterialUiDialog>
);
