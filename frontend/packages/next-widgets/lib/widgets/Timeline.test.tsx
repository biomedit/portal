import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Timeline, type TimelineElement } from './Timeline';
import { formatDate } from '../utils';
import { TestId } from '../testId';

describe('Timeline', () => {
  function getElements(length: number): TimelineElement[] {
    const elements: TimelineElement[] = [];
    for (let i = 0; i < length; i++) {
      const date = new Date();
      // make sure element dates are unique
      date.setMilliseconds(i);

      elements.push({
        icon: 'INFO',
        date,
        title: `Element ${i} Title`,
        message: `Element ${i} Message`,
      });
    }
    return elements;
  }

  describe('Component', () => {
    it.each`
      amountOfElements | amountOfConnectors
      ${1}             | ${0}
      ${2}             | ${1}
      ${3}             | ${2}
    `(
      'should only have $amountOfConnectors connectors when there are $amountOfElements elements',
      ({ amountOfElements, amountOfConnectors }) => {
        render(<Timeline elements={getElements(amountOfElements)} />);

        expect(screen.queryAllByTestId(TestId.TIMELINE_CONNECTOR)).toHaveLength(
          amountOfConnectors,
        );
      },
    );

    it('should render all of the information present in the elements', () => {
      // given
      const infoIcon = 'INFO';
      const warnIcon = 'WARN';

      const infoIconElement: TimelineElement = {
        icon: infoIcon,
        message: 'Message with info icon',
      };
      const warnIconElement: TimelineElement = {
        icon: warnIcon,
        message: 'Message with warning icon',
      };
      const titleOnlyElement: TimelineElement = {
        icon: infoIcon,
        title: 'Title only element',
      };
      const titleAndMessageElement: TimelineElement = {
        icon: infoIcon,
        title: 'Title of element',
        message: 'Message of element',
      };
      const currentDateElement: TimelineElement = {
        icon: infoIcon,
        date: new Date(),
        message: 'Element with current date',
      };
      const oldDateElement: TimelineElement = {
        icon: infoIcon,
        date: new Date('1995-12-17T12:34:56.789'),
        title: 'Element with old date title',
        message: 'Element with old date message',
      };

      const elements: TimelineElement[] = [
        infoIconElement,
        warnIconElement,
        titleOnlyElement,
        titleAndMessageElement,
        currentDateElement,
        oldDateElement,
      ];

      // when
      render(<Timeline elements={elements} />);

      // then

      // check for correct icons
      const infoIcons = screen.getAllByTestId(
        TestId.TIMELINE_DOT_PREFIX + infoIcon,
      );
      expect(infoIcons).toHaveLength(5);

      const warnIcons = screen.getAllByTestId(
        TestId.TIMELINE_DOT_PREFIX + warnIcon,
      );
      expect(warnIcons).toHaveLength(1);

      // check for element attributes to be rendered
      elements.forEach((element) => {
        if (element.date) {
          const date = screen.getAllByText(formatDate(element.date) as string);
          expect(date).toHaveLength(1);
        }
        if (element.message) {
          const date = screen.getAllByText(element.message as string);
          expect(date).toHaveLength(1);
        }
        if (element.title) {
          const date = screen.getAllByText(element.title as string);
          expect(date).toHaveLength(1);
        }
      });
    });
  });
});
