import { LabelledField, required, useEnhancedForm } from '../components';
import type { Meta, StoryFn } from '@storybook/react';
import { FixedChildrenHeight } from './FixedChildrenHeight';
import { FormProvider } from 'react-hook-form';
import React from 'react';

export default {
  title: 'Widgets/FixedChildrenHeight',
  component: FixedChildrenHeight,
} as Meta<typeof FixedChildrenHeight>;

const Template: StoryFn<typeof FixedChildrenHeight> = () => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <FixedChildrenHeight>
        <LabelledField
          name="favoriteColor"
          label={'Favorite Color'}
          validations={[required]}
        />
        <LabelledField
          name="secondFavoriteColor"
          label={'Second Favorite Color'}
          validations={[required]}
        />
        <LabelledField
          name="thirdFavoriteColor"
          label={'Third Favorite Color'}
          validations={[required]}
        />
      </FixedChildrenHeight>
    </FormProvider>
  );
};

export const Default = Template.bind({});
Default.args = {};
