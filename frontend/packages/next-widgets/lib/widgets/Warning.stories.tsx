import type { Meta, StoryFn } from '@storybook/react';
import React from 'react';
import { Warning } from './Warning';

export default {
  title: 'Widgets/Warning',
  component: Warning,
} as Meta<typeof Warning>;

const Template: StoryFn<typeof Warning> = (args) => {
  return <Warning {...args} />;
};

export const Text = Template.bind({});
Text.args = {
  tooltip: 'This might explode!',
};
