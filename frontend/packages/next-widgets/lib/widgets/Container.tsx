import { type ContainerProps, Container as MuiContainer } from '@mui/material';
import React, { type ReactElement } from 'react';
import { styled } from '@mui/material/styles';

const StyledContainer = styled(MuiContainer)(() => ({
  fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
  flexGrow: 1,
}));

export const Container = (props: ContainerProps): ReactElement => {
  return <StyledContainer {...props} />;
};
