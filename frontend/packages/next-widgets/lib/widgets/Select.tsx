import {
  default as MuiSelect,
  type SelectProps as MuiSelectProps,
} from '@mui/material/Select';
import React, { type ReactElement, useMemo } from 'react';
import MenuItem from '@mui/material/MenuItem';

export type SelectChoice<V> = {
  label: string;
  value: V;
};
export type SelectProps<V> = MuiSelectProps & {
  choices: SelectChoice<V>[];
  setValue: (value: V) => void;
};

export function Select<Value>({
  choices,
  setValue,
  value,
}: SelectProps<Value>): ReactElement {
  const valueByLabel = useMemo(
    () =>
      choices.reduce((acc: Record<string, Value>, currentValue) => {
        acc[currentValue.label] = currentValue.value;
        return acc;
      }, {}),
    [choices],
  );
  return (
    <MuiSelect
      value={value}
      onChange={(ev) => setValue(valueByLabel[ev.target.value as string])}
    >
      {choices.map(({ label }) => (
        <MenuItem value={label} key={label}>
          {label}
        </MenuItem>
      ))}
    </MuiSelect>
  );
}
