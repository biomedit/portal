import type { Meta, StoryFn } from '@storybook/react';
import { type TabItem, TabPane } from './Tab';
import React from 'react';

export default {
  title: 'Widgets/TabPane',
  component: TabPane,
} as Meta<typeof TabPane>;

const id = 'id';
const label = 'label';
const model: TabItem[] = [
  { id: 'hello', label: 'Hello', content: 'Hello' },
  {
    id: 'world',
    label: '<b>No HTML support here!</b>',
    content: <u>World</u>,
  },
  {
    id: 'linea',
    label: 'Linea',
    content: <hr />,
  },
  {
    id: 'lowercase',
    label: 'lowercase',
    content: 'Tab label should be capitalized',
  },
];

const Template: StoryFn<typeof TabPane> = (args) => (
  <TabPane {...args} id={id} label={label} model={model} />
);

export const Default = Template.bind({});
Default.args = {};

export const WithPanelBoxProps = Template.bind({});
WithPanelBoxProps.args = { panelBoxProps: { sx: { backgroundColor: 'red' } } };

export const WithSelection = Template.bind({});
WithSelection.args = { selected: 'linea' };
