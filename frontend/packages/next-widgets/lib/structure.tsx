import type { Action } from 'redux';
import React from 'react';

export type Menu = {
  icon?: React.ReactNode;
  link?: string;
};

export type SubItemMenu = Omit<Menu, 'icon'>;

export type StructureItem = {
  menu?: Menu;
  title: string;
};

export type SubItem = Omit<StructureItem, 'menu'> & { menu?: SubItemMenu };

export type CollapsibleStructureItem<T extends SubItem> = StructureItem & {
  subItems: T[];
};

export function isCollapsibleStructureItem<T extends StructureItem>(
  structureItem: CollapsibleStructureItem<T> | StructureItem,
): structureItem is CollapsibleStructureItem<T> {
  return 'subItems' in structureItem;
}
export type MenuItem = Partial<Menu> & Pick<StructureItem, 'title'>;

export function createMenuItems(structure: StructureItem[]): MenuItem[] {
  return structure
    .map(({ title, menu }) => ({ title, ...menu }))
    .filter((entry) => (entry as Menu).link);
}

export type UserMenuItemModel = {
  action: Action;
  icon: React.ReactNode;
  title: string;
};
