import * as timeZoneMock from 'timezone-mock';
import type { AnyObject, DefaultRootState } from '../types';
import { fireEvent, screen } from '@testing-library/react';
import type { Reducer, ReducersMapObject } from 'redux';
import type { $Keys } from 'utility-types';
import equal from 'fast-deep-equal/es6';
import type { Saga } from 'redux-saga';
import SagaTester from 'redux-saga-tester';

export async function matchRequestBody(
  req: Request,
  expectedBody: AnyObject,
): Promise<boolean> {
  return equal(await req.json(), expectedBody);
}

export const mockI18n = (): void => {
  jest.mock('react-i18next', () => ({
    useTranslation: () => ({ t: (key: string) => key }),
    Trans: ({ i18nKey }: Record<'i18nKey', string>) => i18nKey,
  }));
};

export async function expectRequestBody(
  req: Request,
  ...expectedBody: AnyObject[]
) {
  if (!expectedBody.some((body) => matchRequestBody(req, body))) {
    console.error(
      'MockAPI: Expected request body(s):',
      expectedBody,
      'but instead got:',
      await req.text(),
      'for request:',
      req.method,
      req.url,
    );
    throw req;
  }
}

const jsonHeaderKey = 'content-type';
const jsonHeaderValue = 'application/json';

export function expectJsonHeader(req: Request): void {
  if (req.headers.get(jsonHeaderKey) !== jsonHeaderValue) {
    console.error(
      'MockAPI: Expected JSON request header, but instead got:',
      req.headers,
      'for request:',
      req.method,
      req.url,
    );
    throw req;
  }
}

export function expectQueryParameter(
  req: Request,
  key: string,
  expectedValue: string,
): void {
  const url = new URL(req.url);
  const queryParameterValue = url.searchParams.get(key);
  if (queryParameterValue !== expectedValue) {
    console.error(
      'MockAPI: Expected query parameter:',
      key,
      'to be:',
      expectedValue,
      'but instead got:',
      queryParameterValue,
      'for request:',
      req.method,
      req.url,
    );
    throw req;
  }
}

/**
 * Initializes a mocked redux store with redux-saga for async testing.
 *
 * @example Usage:
 * ```typescript
 * let store;
 *
 * beforeEach(() => {
 *     store = makeMockStore({project: initialProjectState}, {project: projectReducer});
 * });
 *
 * it('...', async () => {
 *     store.dispatch({type: LOAD_PROJECTS.request, id: userId});
 *     await store.waitFor(LOAD_PROJECTS.success);
 *
 *     const projectState = store.getState().project;
 *     expect(projectState.itemList).toHaveLength(1);
 * ```
 *
 * @param initialState
 * @param reducers
 * @param rootSaga
 */
export const makeMockStore = <State = DefaultRootState>(
  initialState: Partial<State>,
  reducers: Reducer<Partial<State>> | ReducersMapObject,
  rootSaga: Saga,
): SagaTester<Partial<State>> => {
  const sagaTester = new SagaTester({
    initialState,
    reducers,
  });
  sagaTester.start(rootSaga);

  return sagaTester;
};

/**
 * Simulates clicking on the down arrow inside of a {@link AutocompleteField},
 * which opens the dropdown with the choices.
 *
 * @param label same as the one passed to {@link AutocompleteField}
 */
export const openAutocompleteDropdown = (label: string): void => {
  fireEvent.click(screen.getByLabelText('Open ' + label));
};

/**
 * Mocks the timezone to be a certain {@code timeZone} or UTC by default.
 */
export function mockTimeZone(timeZone?: timeZoneMock.TimeZone): void {
  timeZoneMock.register(timeZone ?? 'Europe/London');
}

export function mockConsoleWarn(): jest.SpyInstance<
  void,
  Parameters<typeof console.warn>
> {
  return mockConsole('warn');
}

export function mockConsoleError(): jest.SpyInstance<
  void,
  Parameters<typeof console.error>
> {
  return mockConsole('error');
}

function mockConsole(
  method: jest.FunctionPropertyNames<Required<Console>>,
): jest.SpyInstance<
  ReturnType<Required<Console>[jest.FunctionPropertyNames<Required<Console>>]>,
  jest.ArgsType<
    Required<Console>[jest.FunctionPropertyNames<Required<Console>>]
  >
> {
  return jest.spyOn(console, method).mockImplementation(() => undefined);
}

/**
 * Sets the text of {@code input} to to {@code text}.
 * @param input element of the input field to set the text on
 * @param text to be set in the input field
 */
export function setInputValue(
  input: HTMLElement,
  text: string | null | undefined,
): void {
  if (!text) {
    throw new Error(`Unexpected text ${text} for input ${input}`);
  }
  fireEvent.change(input, { target: { value: text } });
}

// object is necessary since the generic type of `$Keys` is `<T extends object>`
export type TextboxField<T extends object> = {
  name: $Keys<T>;
  value: string;
};

// object is necessary since the generic type of `$Keys` is `<T extends object>`
export function fillTextboxes<T extends object>(
  textboxes: HTMLElement[],
  fields: TextboxField<T>[],
): void {
  if (textboxes.length !== fields.length) {
    throw new Error('Textboxes and fields length must be the same!');
  }
  for (let i = 0; i < fields.length; i++) {
    expect(textboxes[i]).toHaveAttribute('name', fields[i]['name']);
    setInputValue(textboxes[i], fields[i]['value']);
  }
}

/**
 * If {@code isPresent} is {@code true}, will throw an error if {@code element}
 * is NOT present in the document.
 * If {@code isPresent} is {@code false}, will throw an error if {@code element}
 * IS present in the document.
 * @param element to check whether it is present in the document
 * @param isPresent if the {@code element} is expected to be present or not
 */
export function expectToBeInTheDocument(
  element: HTMLElement | null,
  isPresent: boolean,
): void {
  if (isPresent) {
    expect(element).toBeInTheDocument();
  } else {
    expect(element).not.toBeInTheDocument();
  }
}

/**
 * If {@code isPresent} is {@code true}, will throw an error if any of
 * {@code elements} are NOT present in the document.
 * If {@code isPresent} is {@code false}, will throw an error if any of
 * {@code elements} ARE present in the document.
 * @param elements to check whether they are present in the document
 * @param isPresent if the {@code elements} are expected to be present or not
 */
export function expectAllToBeInTheDocument(
  elements: HTMLElement[] | null,
  isPresent: boolean,
): void {
  elements?.forEach((element) => {
    expectToBeInTheDocument(element, isPresent);
  });
}
