# Maintenance tools

## Version bump

`bumpversion.sh` bumps Portal versions for both dev and production releases.

Run it from the project's root

```bash
./tools/bumpversion.sh <[dev|prod]>
```

## Database

This folder contains a set of shell scripts to backup / restore db /
volumes or migrate between major db releases.

```bash
./db-dump.sh [<container>] > <your-file-name>.sql.gz
```

Makes a backup from db inside a running container `<container>`. Outputs to `<sqldump>`.

```bash
./db-backup.sh
```

Makes a backup of the db in `portal-db-1`, stores to `/tmp/backup_${TIMESTAMP}_db.sql.gz`.

```bash
./db-restore.sh [<container>] < <backup-file>.sql.gz
```

Restores a backup to the db inside a running postgres container specified by
name or hash `<container>` (defaults to `portal-db-1`).
`<backup-file>` has to be passed via stdin redirection.

```bash
./volume-dump.sh <volume> > <tar>
```

Backs up a docker volume `<volume>` to a gzipped tar bundle `<tar.gz>`.

```bash
./volume-restore.sh <volume> < <tar.gz>
```

Restores a gzipped tar file `<tar.gz>` to a docker volume `<volume>` (stdin redirection).

```bash
./init-db-volume.sh <volume> [<sqldump>]
```

Creates a new volume `<volume>` for a **PostgreSQL** **Docker** container (version can be
choosen by setting the `PG_VERSION` env variable). If given, the db
will be populated with a db backup `<sqldump>`.

⚠ This script must be run in a folder containing a `.env` file. This
`.env` file must contain a postgresql root password
`POSTGRES_PASSWORD` (which is not required at runtime).

```bash
./db-migrate.sh
```

Migrates between postgres major releases. The destination release
number can be set via the `PG_VERSION` env variable.
Steps:

- sql dump
- Stop db container
- Backup db volume
- Remove db volume / container
- Initialize new volume
- Restore sql dump

This script is intended to be run against a running deployment.
After the script ran through, update the postgres version number in
the `docker-compose.yml` followed by `docker compose up`.
