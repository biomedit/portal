#!/usr/bin/env python3
"""Prints a schema of a json file.

Example input:

```json
{"a": 1, "b": {"c": 2}}
```

Output:
```
a
b.c
```
"""

from __future__ import annotations
import sys
from typing import Any, Iterator
import json


def dict_schema(data: dict[str, Any]) -> Iterator[tuple[str, ...]]:
    """Takes a dict and returns a generator whose elements are the dict paths of the
    leaves of the (possibly nested) dict"""
    node_stack: list[tuple[tuple[str, ...], dict[str, Any]]] = [((), data)]
    while node_stack:
        current_path, current_dict = node_stack.pop()
        for key, val in current_dict.items():
            if isinstance(val, dict):
                node_stack.append((current_path + (key,), val))
            else:
                yield current_path + (key,)


if __name__ == "__main__":
    _path = sys.argv[1]
    with open(_path, encoding="utf-8") as _f:
        _doc = json.load(_f)
    for _leaf in dict_schema(_doc):
        print(".".join(_leaf))
