# Changelog

All notable changes to this project will be documented in this file.

## [9.1.0](https://gitlab.com/biomedit/portal/-/releases/9.1.0) - 2025-02-11

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/9.0.0...9.1.0)


### ✨ Features

- **frontend/ProjectTable:** Display empty archived cell for non-archived projects ([35f34d5](https://gitlab.com/biomedit/portal/commit/35f34d53cc1a0226ddeae0b9473948aa6694df25)), Close #1208
- **frontend/DataTransferDetail:** Add sender column to data transfer log ([c543887](https://gitlab.com/biomedit/portal/commit/c543887a5f0ae0c2b396ffe98d0360de286782f9))
- **frontend/DataTransferDetail:** Open data package detail on row click ([5eb1d16](https://gitlab.com/biomedit/portal/commit/5eb1d1699d7e1b0ee38ba56175633039331bf0b2)), Close #1203
- **frontend/DataPackageDetail:** Add data package detail page ([589f498](https://gitlab.com/biomedit/portal/commit/589f49871b5bc2e5168901cd9de8c0169758def6))
- **backend/DataTransfersExport:** Add data package recipients to the export ([93d113f](https://gitlab.com/biomedit/portal/commit/93d113f533d3daa60ab893e57cdba2a45c7db4df))
- **backend/DataPackageSerializer:** Add recipients ([257fb34](https://gitlab.com/biomedit/portal/commit/257fb34067af6f4c4d135684ca731c93168721fe))
- **backend/ExportTOU:** List users who have not yet accepted ([824ee23](https://gitlab.com/biomedit/portal/commit/824ee23263c059b21a6e18ce20444bc3f93e6e40)), Closes #1176
- **frontend/DataTransfers:** Improve data package tracing ([1c58b3f](https://gitlab.com/biomedit/portal/commit/1c58b3f7b3b06ef1204075078f9e6d40454e33a1)), Closes #1072
- **backend/oidc:** Authentication class should fail silently ([b780071](https://gitlab.com/biomedit/portal/commit/b780071c1f860072d7b86550a5ab7579b5c63b95))
- **frontend/terminology:** Use original filename for downloaded object when available ([21d02aa](https://gitlab.com/biomedit/portal/commit/21d02aa7325ed03b7678e1fb299aa25b03c4b923)), Close #1204
- **backend/terminology:** Include original filename in version files objects ([c1d8e69](https://gitlab.com/biomedit/portal/commit/c1d8e69a0773ff80dfd0779c4634229237f6913c))
- **frontend/ToastBar:** Change default behavior to not automatically hide ([83195ea](https://gitlab.com/biomedit/portal/commit/83195eacc2ee0eaff605b3e80acfa46273ae5cdd)), Close #1200
- **frontend/ProjectDetail:** Add user role history tab ([6f79eba](https://gitlab.com/biomedit/portal/commit/6f79eba3041fce6b3bac1222b004c920335237fe)), Close #1174
- **backend/projects:** Show user role history to project leader and permission managers ([5786403](https://gitlab.com/biomedit/portal/commit/5786403631fb840d9b8403c9926902f8b3481d66))

### 🐞 Bug Fixes

- **frontend/DataTransferDetail:** Fix sorting for data package timestamps ([3819831](https://gitlab.com/biomedit/portal/commit/3819831983d6196e170d49146fc94520344b4913))
- **frontend/UserRoleHistory:** Fix translations ([f2cda85](https://gitlab.com/biomedit/portal/commit/f2cda85eab7a3596903130726bb8cd3afa1d97d4)), Close #1209
- **frontend/next-widgets:** Fix import name for short-uuid ([75fd757](https://gitlab.com/biomedit/portal/commit/75fd757df8d82bea79981601c48ff1862306b685))
- **tasks::remove_old_jobs:** Use the correct argument name ([876583f](https://gitlab.com/biomedit/portal/commit/876583f7559bcd84553d60a264e02c1be1a818ab)), Close #1205
- **backend/notifications:** Remove legal basis from test dtr approval email ([0fb5ead](https://gitlab.com/biomedit/portal/commit/0fb5ead033aeadbf53988eff6cc57581779ac0f2)), Close #1195
- **backend/notifications:** Remove misleading text from project expiration notification ([b5d5219](https://gitlab.com/biomedit/portal/commit/b5d5219d357e0c5cbef4729a936b92026500aae9)), Close #1191

### 👷 CI

- **prepare-prerelease:** Inline template ([a79c5d4](https://gitlab.com/biomedit/portal/commit/a79c5d40525a9662114e7abec908577028cabbb7))

### 🧹 Refactoring

- **backend/migrations:** Remove obsolete migration files ([b32c5d4](https://gitlab.com/biomedit/portal/commit/b32c5d43b36db8bb635666d66f87f97423d95c37)), Close #1192

### ✅ Test

- **backend:** Add tests for data exports ([77a2dfb](https://gitlab.com/biomedit/portal/commit/77a2dfbcaf6faa5263cc603992592cb1771a588b)), Close #1193
- **backend/terminology:** Add missing asserts ([c0b196b](https://gitlab.com/biomedit/portal/commit/c0b196b9e86680d33947c0dbcd28d5fe2508cecc))
- **frontend/next-widgets:** Add missing test files ([61fc3ba](https://gitlab.com/biomedit/portal/commit/61fc3ba4816b7b6839b645a9f944bcdb10114316)), Close #1172

## [9.0.0](https://gitlab.com/biomedit/portal/-/releases/9.0.0) - 2025-01-20

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/8.4.0...9.0.0)

### ⚠ BREAKING CHANGES

- remove can_see_credentials from /data-transfer endpoint
- remove node from /data-provider endpoint

### ℹ️ Actions Needed

- delete file services/traefik/dynamic/middleware.toml from target VM

### ✨ Features

- **frontend/TermsOfUse:** Show error message when tou does not exist ([2c9948f](https://gitlab.com/biomedit/portal/commit/2c9948f0ebe6a6db25c4fd7f1357e5299ed375f9)), Closes #1180
- **backend/data-transfer:** Remove can_see_credentials field ([2162822](https://gitlab.com/biomedit/portal/commit/216282266c977005cc8aa33c4fcdbca44a311cf1)), ⚠ BREAKING CHANGE: remove can_see_credentials from /data-transfer endpoint
- **frontend/DataTransferDetail:** Remove credentials tab ([ea79da7](https://gitlab.com/biomedit/portal/commit/ea79da764e652dc86522d2ee459d3c89b302747e)), Close #1101
- **frontend/Projects:** Load users on project edit page ([68ec17e](https://gitlab.com/biomedit/portal/commit/68ec17e561c1f1ea9b98fdaf11a67deb6adddc70)), Closes #1156
- **backend/data-transfer:** Do not create data transfer approval for data provider nodes ([1391ca1](https://gitlab.com/biomedit/portal/commit/1391ca17869b2141db77c2c6f832521a80ff8907))
- **backend/notifications:** Stop sending data transfer related notifications to data provider nodes ([0571930](https://gitlab.com/biomedit/portal/commit/057193028721c291ddf593a915ae91dc58f88068))
- **frontend:** Don't mention SFTP in the UI ([a42864b](https://gitlab.com/biomedit/portal/commit/a42864bd17f0058941904394da2010459a94f509))
- **data-provider:** Replace node with nodes ([cfac304](https://gitlab.com/biomedit/portal/commit/cfac3045221f46a9134bb851a9c6a6ac3eedeb5c)), ⚠ BREAKING CHANGE: remove node from /data-provider endpoint, Close #1120
- **frontend/TermsOfUse:** Prompt the user to accept new version of TOU ([3cf7fcf](https://gitlab.com/biomedit/portal/commit/3cf7fcf9598a42b4b06114620e4ab9f4d8c61dec)), Closes #1140
- **documentation:** Add instructions for local email testing ([8da4976](https://gitlab.com/biomedit/portal/commit/8da497677783e7c81e2ec270d3bcc4d9f025187d)), Closes #1140
- Produce a human readable openapi schema with redoc ([dac41d9](https://gitlab.com/biomedit/portal/commit/dac41d9cfa433ecce1fb51f4b7051792a427a035))
- **frontend:** Add terminology page ([3966c4a](https://gitlab.com/biomedit/portal/commit/3966c4aaad9d8b6b0a4109970b7b4173c45854c4))
- **frontend/Profile:** Add breadcrumbs ([dd5b322](https://gitlab.com/biomedit/portal/commit/dd5b322acdf67ff4993924d5d656d5f95039dee9))
- **backend:** Add terminology app ([b40598f](https://gitlab.com/biomedit/portal/commit/b40598f3b361010347fd2c7bfbb0d3955f547434))
- **backend/node:** Allow ip address ranges to be empty in the request ([5c6df65](https://gitlab.com/biomedit/portal/commit/5c6df65c1709e3d62653ab7c2746282f4c509c30))
- **frontend/NodeManageForm:** Add ip address range ([539c7aa](https://gitlab.com/biomedit/portal/commit/539c7aafe780cdae4c18513cb5149dc14579c311))
- **frontend/NodeDetail:** Add ip address range ([831d6a9](https://gitlab.com/biomedit/portal/commit/831d6a96e94356d9ad8268d2b0cf3afbb2b33aaa))
- **backend/nodes:** Add ip address range to the serializer ([7ea5d7a](https://gitlab.com/biomedit/portal/commit/7ea5d7a71cdd4c1aa995bfe6200ef4c748f0fb71))
- **backend/nodes:** Add ip address range model ([aff1432](https://gitlab.com/biomedit/portal/commit/aff1432bf360873a3238801d44b623a7db224c34))
- **backend/sts:** Dispatch read credentials given a package name ([76953e5](https://gitlab.com/biomedit/portal/commit/76953e5e4270eb07518f2181d5f68fe04ee5313d))
- **frontend/TermsOfUseTable:** Add read and accept button ([11637f9](https://gitlab.com/biomedit/portal/commit/11637f988b6b5e0d46083d4ec2a5b9b95af6b835))
- **backend/terms-of-use:** Only return terms of use that are relevant to the user ([98b5f0c](https://gitlab.com/biomedit/portal/commit/98b5f0c7908329786db3503ef8183a2478367a98))
- **backend/terms-of-use:** Allow GET only to authenticated users ([88f9df4](https://gitlab.com/biomedit/portal/commit/88f9df4dcf1a180e2dae0586522ddf0fa6848d45))
- **frontend/TermsOfUseDetails:** Merge details and text tabs ([eda07f6](https://gitlab.com/biomedit/portal/commit/eda07f68aff3d35aa0d62377cbedd8c9959bba63))
- **frontend/terms-of-use:** Redirect the user to the terms-of-use page after accepting ([08b7970](https://gitlab.com/biomedit/portal/commit/08b79705a4ae6b5f327248e58da188a9a9193013))
- **frontend/Profile:** Remove terms of use section ([5cc4c30](https://gitlab.com/biomedit/portal/commit/5cc4c3003a48efffd628ca918b9eeb2f4e3bbf80))
- **frontend/TermsOfUseTable:** Add date accepted column ([f4d41f3](https://gitlab.com/biomedit/portal/commit/f4d41f365b34f46f72d66a8744754f99c19c0f57))
- **frontend/TermsOfUseTable:** Show latest terms of use first ([671f73f](https://gitlab.com/biomedit/portal/commit/671f73ff55bd9666d7a0be5d3997875fb4d763b2))
- **frontend/terms-of-use:** Group terms of use by node ([b5416e9](https://gitlab.com/biomedit/portal/commit/b5416e9640dc60b49c6f5e4a481439a341c16047))
- **frontend/structure:** Move terms of use at top level ([0242664](https://gitlab.com/biomedit/portal/commit/0242664b6b46a83413f0b1a3325487507260ec25))
- **backend/TermsOfUse:** Add terms of use export ([50b7600](https://gitlab.com/biomedit/portal/commit/50b76001e5d6f6f20dd88e09529958876008e938)), Closes #1141

### 🐞 Bug Fixes

- **backend/user:** Allow node viewers to export users ([51949e3](https://gitlab.com/biomedit/portal/commit/51949e369bd0165076bd6006708d5235a650a46a))
- **backend/exports:** Replace data provider node with nodes ([a05c213](https://gitlab.com/biomedit/portal/commit/a05c213e00756496db30d54e11341908bf53581d))
- **migrations:** Make data migrations elidable ([78e912e](https://gitlab.com/biomedit/portal/commit/78e912e337753330f133428f1612cbeb5e6ba3eb))
- **frontend/UserProfileDialog:** Point the user to the latest version of the terms of use ([bbc547c](https://gitlab.com/biomedit/portal/commit/bbc547c7425e80278922bb29b4f8d598c6ecb748)), Close #1183
- **frontend/UserProfileDialog:** Hide toast bar behind feature toggle ([75199c1](https://gitlab.com/biomedit/portal/commit/75199c1930025cbc7ff6c04aec71a5a5ca5e5fbc))
- **backend:** Drop broken schema endpoint ([228c569](https://gitlab.com/biomedit/portal/commit/228c5697f88556ef79d2d1fd3ccba71d1467ea80))
- **frontend/DataTransferForm:** Always show data transfer's data provider ([a1ec034](https://gitlab.com/biomedit/portal/commit/a1ec03439f8cdcce3b099945eb1238ebd86f190b)), Close #1154

### 🧱 Build system and dependencies

- **frontend/react-hook-form:** Revert to 7.54.1 ([804db4f](https://gitlab.com/biomedit/portal/commit/804db4f3cee8813a3cb59c30b6da7fca67214efa)), Close #1194
- **docker:** Remove unnecessary uploads volume ([d6ff817](https://gitlab.com/biomedit/portal/commit/d6ff817c1ba551d25b48195ef08bfe54f40749ac))
- **traefik:** Stop serving the entire web/media folder ([837933c](https://gitlab.com/biomedit/portal/commit/837933ccc28d32dd8d6d795cb80df55175f240d6))
- **backend/settings:** Drop unsupported DEFAULT_FILE_STORAGE configuration option ([f519c82](https://gitlab.com/biomedit/portal/commit/f519c824dda5358800537e15b2a991a9da7b74bf))
- **git:** Ignore backend media files ([6d707c7](https://gitlab.com/biomedit/portal/commit/6d707c73fd472c58886b23015f8759c4b9e0fa23))
- **traefik:** Add tls default options ([6159df7](https://gitlab.com/biomedit/portal/commit/6159df7190f1b4ca9cf5d5fdd9a8baa1b822594f)), Closes #1173
- **frontend:** Use open-api-generator to generate API docs ([deb5cff](https://gitlab.com/biomedit/portal/commit/deb5cff1e1f02516f0479f3f94ec54e613557119))
- **frontend:** Switch from rimraf to shx ([9a5f765](https://gitlab.com/biomedit/portal/commit/9a5f7652add325be708829dae7b3e810b252b293))
- **frontend/next:** Use remotePatterns instead of domains for image config ([0de8de0](https://gitlab.com/biomedit/portal/commit/0de8de0faeea97510e1dc6f720123d37a009884d)), Close #1162
- **frontend:** Fix warnings ([9293c28](https://gitlab.com/biomedit/portal/commit/9293c28b830474d6bd3f073b88b2bd441ca72574))
- **frontend:** Drop typescript-coverage-report ([e4e59df](https://gitlab.com/biomedit/portal/commit/e4e59dfac9527aef151dbd2e59f3c86c70463488))
- **frontend:** Switch on noImplicitAny tsc rule ([50cae2a](https://gitlab.com/biomedit/portal/commit/50cae2add9200aa8842ea292420c4ad5c52a38bb))
- **frontend:** Switch to verbatim module syntax ([9375d1e](https://gitlab.com/biomedit/portal/commit/9375d1e1a8180ca6fda85b79bd7fb561633f784e))
- **frontend:** Import next-widgets tsc rules ([9e520ac](https://gitlab.com/biomedit/portal/commit/9e520ac9a349e1aba605badb2812a74b1633b00d))
- **frontend:** Import next-widgets eslint sorting rules ([b577a9a](https://gitlab.com/biomedit/portal/commit/b577a9ad674b7cf5b6c5769f2cea4a4a98f5ad4a))
- **frontend:** Import next-widgets eslint rules ([cef662b](https://gitlab.com/biomedit/portal/commit/cef662b50b2eaf282d4438989859f93cb205a65d))
- **backend:** Switch from tomli to tomllib ([c6b6feb](https://gitlab.com/biomedit/portal/commit/c6b6feb8ea8fd6d599015b88ae73d04a246e8115))
- Upgrade next.js to v15 ([f88bfb4](https://gitlab.com/biomedit/portal/commit/f88bfb4f637a85ef5430ba8646b93f0f62e5f771)), Close #1146
- **node:** Use Node LTS codenames ([f33db23](https://gitlab.com/biomedit/portal/commit/f33db233f1906cd8e76ee6a7eea7b863cffcabcf)), Close #1149
- **eslint:** Add typescript-eslint no-deprecated rule ([4b89e0a](https://gitlab.com/biomedit/portal/commit/4b89e0aa6373581248298da2a24151881d88ef73)), Close #1093
- **frontend:** Reduce image size ([724fc56](https://gitlab.com/biomedit/portal/commit/724fc56b6ea426fe047f04ce4bffc64f727a6459))
- **frontend:** Make devDependencies not required for production builds ([be9538a](https://gitlab.com/biomedit/portal/commit/be9538aa905d610fbefd053a1f2ad17e2f9f2215)), Close #1157

### 👷 CI

- **check-generated-api-frontend:** Check static redoc schema ([0d54d2d](https://gitlab.com/biomedit/portal/commit/0d54d2d8667d2d921c65713339740ccca548cf94)), Close #493
- **check-generated-api-frontend:** Stop using template ([4528160](https://gitlab.com/biomedit/portal/commit/45281609a3a8430703d377f0e2e45f9953170bc4))
- **frontend:** Run review-visuals only for relevant changes ([c18aec5](https://gitlab.com/biomedit/portal/commit/c18aec5b87accdbd859ba5bef4d285a3cf887a31))
- **frontend:** Run dpdm on next-widgets as well ([447b72d](https://gitlab.com/biomedit/portal/commit/447b72debe601d92c4849cd7cb5929714e826bcf))
- **frontend:** Run depcheck on next-widgets as well ([47ae963](https://gitlab.com/biomedit/portal/commit/47ae963457b02fc8a1f52f6c3d3a362e6409eeee))
- Add storybook related jobs ([de08cab](https://gitlab.com/biomedit/portal/commit/de08cab4a71b3eaf5794b1f88c24efcb53f154d2))
- **frontend:** Adapt dangerfile to new monorepo setup ([d466ce9](https://gitlab.com/biomedit/portal/commit/d466ce9e80db2c5cafb5731c499ec16944325b54))

### 📝 Documentation

- **web/config:** Fix sample json config ([8d3360f](https://gitlab.com/biomedit/portal/commit/8d3360f41b173aab4ae38f17a3d3f79a33fa9e27))

### 🧹 Refactoring

- **projects:** Squash migrations ([768282b](https://gitlab.com/biomedit/portal/commit/768282b6a533163b68261dd5810dcfdab665f54a)), Close #1133
- **identities:** Squash migrations ([672cec6](https://gitlab.com/biomedit/portal/commit/672cec6bd14f1094d4375a7e20bc78930fe11d67))
- **backend/approval:** Simplify logic on account of no transfer node ([cbcc26e](https://gitlab.com/biomedit/portal/commit/cbcc26e80d4b3dc8e1d73c5fc804a8fed6e843b1))
- **frontend/DataTransferApproval:** Simplify logic on account of no transfer node ([7cbb619](https://gitlab.com/biomedit/portal/commit/7cbb61998f419064d3290815de895c37c3531f3b))
- **frontend/UserProfileDialog:** Extract terms of use warning logic ([230c0af](https://gitlab.com/biomedit/portal/commit/230c0af88bc24be3109c0aefd6c61cf53550bb40))
- **frontend/eslint:** Do not reiterate defaults ([6025655](https://gitlab.com/biomedit/portal/commit/6025655ab73e78c67519848e2d66d8ed78e716a0))
- **frontend/terminologies:** Use generated api code instead of bare url ([95f3cd4](https://gitlab.com/biomedit/portal/commit/95f3cd43021d3064fc4c9c911191ded8e381440f)), Close #1177
- **backend/terminology:** Improve schema for file download endpoint ([7cf6414](https://gitlab.com/biomedit/portal/commit/7cf641400e369c6c6d6c4296a81ffd292e05da45))
- **frontend:** Extract common file download logic ([f037e54](https://gitlab.com/biomedit/portal/commit/f037e54c53a771a9f50cae65c20ebb7dbfe1dac5))
- **frontend:** Import next-widgets source code as a workspace ([b7d0f11](https://gitlab.com/biomedit/portal/commit/b7d0f117a2dab915792017f26a81d0b8dc406ae6)), Close #1164
- **frontend/widgets:** Extract generic IP address range logic ([4031d4b](https://gitlab.com/biomedit/portal/commit/4031d4b807810e72709cd2d8cde865002315f8a8))
- **backend/models:** Extract generic IP address range logic ([35b18b3](https://gitlab.com/biomedit/portal/commit/35b18b34aef6194eb842d0bc520bbad6eab12c21))
- **frontend/mui:** Migrate to Grid2 ([26befb3](https://gitlab.com/biomedit/portal/commit/26befb3e2cbbe0700d1c956691dae3d5f466f879))
- **backend:** Remove logic related to federated query system ([a943ca9](https://gitlab.com/biomedit/portal/commit/a943ca931390fc2524959f912debda02471e71cb))
- **frontend/selectors:** Add authenticated user selector ([44ef76d](https://gitlab.com/biomedit/portal/commit/44ef76d91ce0dc68c587bf3bf3d552d3d122c762))

### 🎨 Style

- **python:** Apply Ruff 2025 style rules ([cc19720](https://gitlab.com/biomedit/portal/commit/cc197200028b691a4412607a3f0d44bc91820aa7))
- **frontend/eslint:** Do not allow separate import groups ([b14462f](https://gitlab.com/biomedit/portal/commit/b14462f18afd1c4dbb2676f6c632692305942317))
- **frontend/UserProfileDialog:** Sort imports ([d3a273e](https://gitlab.com/biomedit/portal/commit/d3a273e2f53bd1dff80376de12d30cd057dfda5f))

### ✅ Test

- **backend/pgp-key-info:** Make sure the pk doesn't exist ([adc9395](https://gitlab.com/biomedit/portal/commit/adc93959d9fc91ef5d98da5097e40c049603d3f5)), Close #1196
- **backend/project_notification:** Ignore subject prefix ([b178bfa](https://gitlab.com/biomedit/portal/commit/b178bfa32c0f300f85a95d5b4e8fd0dca53c6a4d))
- **DataProviderFactory:** Use deterministic values for unique fields ([be649a1](https://gitlab.com/biomedit/portal/commit/be649a1a33ad1320d8d88ee28a1077f59fc319f6)), Close #1190
- **frontend/UserProfileDialog:** Write test for terms of use warning function ([ef6510b](https://gitlab.com/biomedit/portal/commit/ef6510b9a5265b975f2dfbe198e0977ff0934a58))
- **backend/UserNamespaceFactory:** Use deterministic names ([db50385](https://gitlab.com/biomedit/portal/commit/db50385021404eca8a99fb23f1c38cba4bd20021)), Closes #1184
- **backend/notification:** Ignore email recipients order ([9f28a86](https://gitlab.com/biomedit/portal/commit/9f28a86d7cd8ec4a70855b7e42fb742627b8880a)), Close #1170
- **backend/notification:** Ignore subject prefix ([a8a893d](https://gitlab.com/biomedit/portal/commit/a8a893d6493ccee029bf3eb95bc8660ec12d52f3))
- **backend/node:** Ignore ordering when comparing elements ([fe90230](https://gitlab.com/biomedit/portal/commit/fe90230b79209f0b87f28175bdc6e2874f09245c))
- **backend/data-transfer:** Fix flaky test ([6138e25](https://gitlab.com/biomedit/portal/commit/6138e25d0d5cc971df98742013ec31140c08788a)), Close #1181
- **backend/data-provider:** Fix flaky test ([938847a](https://gitlab.com/biomedit/portal/commit/938847a2537b48665a087f0eeeb27744f616e052)), Close #1179
- **frontend/next-widgets:** Import stories ([f7597d9](https://gitlab.com/biomedit/portal/commit/f7597d96309ccb28fc436797e02fd1aaa62c4a19))
- **frontend/next-widgets:** Import tests ([ef6c35c](https://gitlab.com/biomedit/portal/commit/ef6c35c8ec2047cc86786948c2545265ec4df065))
- **backend/pgpkey:** Fix flaky test ([16da95d](https://gitlab.com/biomedit/portal/commit/16da95d1f87fbf981aa15e7e7bd3a07f7190104e))

## [8.4.0](https://gitlab.com/biomedit/portal/-/releases/8.4.0) - 2024-11-13

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/8.3.1...8.4.0)


### ✨ Features

- **backend/TermsOfUse:** Confirmation email to accept terms of use ([eb13ee7](https://gitlab.com/biomedit/portal/commit/eb13ee78af804bce1c0bd3081b77cad3c8622115)), Closes #1114

### 🧱 Build system and dependencies

- **backend:** Use python-slim image ([795c69c](https://gitlab.com/biomedit/portal/commit/795c69ccc8cecdb078beef233515b63c3742fc1c))
- **backend:** Simplify Dockerfile ([0bbb710](https://gitlab.com/biomedit/portal/commit/0bbb710f6fc43e01f352d648857fec0f48757a5f))
- **frontend:** Disable Next.js telemetry ([362ae5d](https://gitlab.com/biomedit/portal/commit/362ae5d9979b74f4a117d8bc625b6b24f32ec4e7))
- **frontend:** Improve dockerignore file rules ([c36ba3a](https://gitlab.com/biomedit/portal/commit/c36ba3ae8f65339d47177057a5b873653d32e5c0))
- **frontend:** Allow running container as any uid ([bae6fb8](https://gitlab.com/biomedit/portal/commit/bae6fb86bd242926182f2bc9f721563338991ce2)), Close #1153

### 👷 CI

- **publish:** Use public runners for building images ([b2aeb54](https://gitlab.com/biomedit/portal/commit/b2aeb544fab1df477f2b39bde695a0657ee7f83b))

## [8.3.1](https://gitlab.com/biomedit/portal/-/releases/8.3.1) - 2024-11-08

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/8.3.0...8.3.1)


### 🐞 Bug Fixes

- **frontend/ProjectForm:** Validate mutual exclusivity correctly ([30063b2](https://gitlab.com/biomedit/portal/commit/30063b2f1f7000b53580d1a4ad917ec8904287b2)), Close #1155
- **frontend/users:** Load incative users as well ([1684f3c](https://gitlab.com/biomedit/portal/commit/1684f3c622cff02351e7de087e7052faf9c1bc29))
- **frontend/UserDetail:** Fix empty message ([4f11b70](https://gitlab.com/biomedit/portal/commit/4f11b7031445ef18926afc2c61011f99c46904b0))
- **frontend/app:** Expand navigation menu to fill the page ([fa2b047](https://gitlab.com/biomedit/portal/commit/fa2b04709227f4059f540e526340edf574f7fb81)), Close #1142
- Allow using `+` in names ([23b2c11](https://gitlab.com/biomedit/portal/commit/23b2c11caaf8dd7afe46d535f8db5248ee9fef50)), Close #1144
- **backend/data-provider:** Use node to populate nodes in patch/put requests ([5bea19a](https://gitlab.com/biomedit/portal/commit/5bea19aefb0a731c3a4c3a60eae979307323c38b)), Close #1145

### 🧱 Build system and dependencies

- **nginx:** Expose port 8080 ([19ae3e2](https://gitlab.com/biomedit/portal/commit/19ae3e23113f4dfae0a08b93ad383bedefb0616a))
- **frontend:** Drop pm2 ([d1094c7](https://gitlab.com/biomedit/portal/commit/d1094c7d0e4e4d16e283b7bd180bc6676b6ca4f3))
- **traefik:** Run traefik as root ([0795da0](https://gitlab.com/biomedit/portal/commit/0795da016d9d537a419f3364d83d1ba3fdd11627))
- **nginx:** Use default 8080 port ([0e1fbca](https://gitlab.com/biomedit/portal/commit/0e1fbca190c1fc20e3d781c47e61624c66b2d539))
- **docker:** Change ownership of non-named volumes as well ([08899de](https://gitlab.com/biomedit/portal/commit/08899deef945e6d44cd378238294ceafec29aa64))
- **web-tasks:** Change log level to INFO ([a0402ac](https://gitlab.com/biomedit/portal/commit/a0402aca2c0fb56bfd15c2239703b966bdc90fe4)), Close #1152

### 👷 CI

- Revert to LTS node 22 ([becb06b](https://gitlab.com/biomedit/portal/commit/becb06befe38aad575dbb431a4c66fb1bcda12e8)), Closes #1143

### 🧹 Refactoring

- **backend/projects:** Validate users with the validate method ([6f1bfb2](https://gitlab.com/biomedit/portal/commit/6f1bfb2d2f7e4621912842a03c835047d3eddf23))

### ✅ Test

- Fix faker deprecation warnings ([4f53d32](https://gitlab.com/biomedit/portal/commit/4f53d320c0e3c62b7204f937bdb3d152300ab8ea))

## [8.3.0](https://gitlab.com/biomedit/portal/-/releases/8.3.0) - 2024-10-28

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/8.2.0...8.3.0)


### ✨ Features

- **frontend/terms-of-use:** Prevent users from accepting the same version twice ([fda3cb0](https://gitlab.com/biomedit/portal/commit/fda3cb0847cb7d64a4c3d9e6aac4e603964af9e8)), Close #1134
- **backend/users:** Add terms of use's id to the detailed endpoint ([b244eb6](https://gitlab.com/biomedit/portal/commit/b244eb636f0a99ff7293e6618f75050bd256a929))
- **frontend:** Make NO ROLE mutually exclusive with other roles ([6469469](https://gitlab.com/biomedit/portal/commit/646946979795db5085e43b3bab67e5afb3b036cf))
- **backend:** Make NO ROLE mutually exclusive with other roles ([3ac9996](https://gitlab.com/biomedit/portal/commit/3ac99965d8c47810ec52a4b3cd3db87ba6482599))
- **DataPackage:** Handle data package deletion events ([b09343f](https://gitlab.com/biomedit/portal/commit/b09343f629c1ebd2b2c0bc06e1672ff5ddfcc133)), Close #1135
- **frontend/Admin:** Add ui to accept terms of use ([0144499](https://gitlab.com/biomedit/portal/commit/014449907811074d295b034186a485cb6634f440)), Closes #1097
- **backend/projects:** Prioritize project code in notifications ([2f40ad2](https://gitlab.com/biomedit/portal/commit/2f40ad2f1d507cb6cd07d32e408d00f80010851b)), Close #1128
- **backend/data-packages:** Notify data managers and sender when data package lands ([4bad0b3](https://gitlab.com/biomedit/portal/commit/4bad0b3a2b25ddd11eddbdf8319dcd8c3e678999)), Close #1130
- **backend/admin:** Add ProjectUserRole management ([a7cd444](https://gitlab.com/biomedit/portal/commit/a7cd444f0e1c76cc64d7fa5de86bc89c5593df21))
- Generate STS read-only credentials for data managers ([5e33126](https://gitlab.com/biomedit/portal/commit/5e3312607fb6c9d92017bd1da9edcbaf1450949c))
- **frontend/ProjectDetail:** Display resources and services support flags and add tabs accordingly ([08d4a34](https://gitlab.com/biomedit/portal/commit/08d4a34f3ef5f9cab128e1fe4f8caf4e4e2ec779)), Close #1118
- **frontend/ProjectForm:** Allow specifying resources and services support flags ([04e531c](https://gitlab.com/biomedit/portal/commit/04e531ced374b6c6d01a0532f9c8768fc449009c))
- **backend/projects:** Add resources and services support flags ([03be680](https://gitlab.com/biomedit/portal/commit/03be68074448594c7eb82b8730fa577bdb1a9013))
- **frontend/projects:** Add /new and /edit pages for projects ([1ab906a](https://gitlab.com/biomedit/portal/commit/1ab906a480e47e88253453ee25e3fe4c46558a3e))
- **backend/terms-of-use:** Notify users when a new version is available ([ba642eb](https://gitlab.com/biomedit/portal/commit/ba642ebb71d29466848c43c71b36f59b75c97d27)), Close #1115
- **frontend:** Show users tab to node viewers ([e593fcd](https://gitlab.com/biomedit/portal/commit/e593fcdbcf08c7264e322748d637d3bd46489a8e))
- **dataprovider:** Add support for multiple nodes ([6537aef](https://gitlab.com/biomedit/portal/commit/6537aef2561c0bcdf3eecb4d494c18b2bf919147)), Closes #1081
- **frontend/pgp:** Allow idempotent status update ([8166ce3](https://gitlab.com/biomedit/portal/commit/8166ce352844b73f81520f8c0cd5dd9b4d0c2e8e)), Close #1110
- **backend/pgp:** Allow idempotent status updates ([51d7134](https://gitlab.com/biomedit/portal/commit/51d7134f9d79cefe0b8050ff1812b32e7ddd0cf6))
- **frontend:** Update to next-widgets v19.0.0 ([c4127af](https://gitlab.com/biomedit/portal/commit/c4127af67d8080fc9cd417612af302b245e78085))
- **TOU:** Make terms of use public ([1ad9029](https://gitlab.com/biomedit/portal/commit/1ad902939c522c48adbfe1ebdd6b5cea871bde2f)), Closes #1107
- Prepare Docker deployment for rootless ([58f8571](https://gitlab.com/biomedit/portal/commit/58f85713647cf42f28f59c5ab6794033db631c85))

### 🐞 Bug Fixes

- **nginx:** `nginx` has to be started with `user: ` ([c9d0c16](https://gitlab.com/biomedit/portal/commit/c9d0c16fa72d01143b66ed7970f21dd9377f285a))
- **Docker:** Remove `user: ` for self-managing images ([7b74406](https://gitlab.com/biomedit/portal/commit/7b744062d4e90cc015d6e4992bd087d83333a9a9)), Closes #1099
- **backend/terms-of-use:** Avoid comparing int and string when checking permissions ([b906fb1](https://gitlab.com/biomedit/portal/commit/b906fb1ac7f589c702a703d39d7dcf82256e46dd))
- **backend/admin:** Remove misleading delete action from terms of use page ([5814ce3](https://gitlab.com/biomedit/portal/commit/5814ce356fb9f0f49aff6ba5afad89d442cb8a48))
- **frontend:** Ignore data package trace deletion events ([62663a6](https://gitlab.com/biomedit/portal/commit/62663a697f6c7166ee04f85853437b2b6d0e52fe))
- `docker-compose.prod.yml` not ready for rootless ([2c7d9e9](https://gitlab.com/biomedit/portal/commit/2c7d9e946cb9363044cd9c5bc87a2f5b10583a57))
- Ensure case-insensitivity of OpenPGP email check ([869afb5](https://gitlab.com/biomedit/portal/commit/869afb5015c5e79694f0625437e9344dba91863e)), Closes #1121

### 🧱 Build system and dependencies

- **eslint:** Update `eslint` to v9 ([1ced499](https://gitlab.com/biomedit/portal/commit/1ced49909c5e650f678226275fea86d7ec4e5f4c)), Closes #1013
- **python:** Bump Python to 3.13 ([ecf695b](https://gitlab.com/biomedit/portal/commit/ecf695b554771a86e6754320c89be511ae2a9720)), Close #1131
- **tasks:** Use procrastinate for running periodic tasks ([0555fd2](https://gitlab.com/biomedit/portal/commit/0555fd229dae217b6c355ebdb9177bd57c093f91)), Close #1113
- Drop support for SQLite ([b6e197e](https://gitlab.com/biomedit/portal/commit/b6e197ecc6810162d6cd476f05b4ef24cbd54f4d)), Close #1125
- Remove local-dev deployment ([79b30a1](https://gitlab.com/biomedit/portal/commit/79b30a14ff2584934ac330b9417ea381ed4cec97))
- **tests:** Pin test dependency versions ([339c3de](https://gitlab.com/biomedit/portal/commit/339c3de11e6c4488fd829f0b354b240c49f511e8))

### 📝 Documentation

- **dev:** Fix folder where to store populate_database.py ([13c68a0](https://gitlab.com/biomedit/portal/commit/13c68a0d4d6d10ae4cedf6889b42ea43bf864244))

### 🧹 Refactoring

- **frontend/ProjectTable:** Reduce duplication of emptyMessage ([afc679b](https://gitlab.com/biomedit/portal/commit/afc679be68fe5fbbe4dc34a2920d240e3599ed39))
- **backend/frontend:** Rename project role MINIMAL to NO_ROLE ([47b55d6](https://gitlab.com/biomedit/portal/commit/47b55d6fff81928db8f157f04751eafe14d2ad35))
- **backend:** Raise explicit error when missing email settings ([a72c4e5](https://gitlab.com/biomedit/portal/commit/a72c4e5a747673f1a9b77d5d725d664e0968301b))
- **backend/nodes:** Move away from signals ([3e2ca4f](https://gitlab.com/biomedit/portal/commit/3e2ca4f740c3e1efc28477f2a2b037df97f7b1ad)), Close #753
- **backend/data-providers:** Move away from signals ([fb1d580](https://gitlab.com/biomedit/portal/commit/fb1d580300ab6280f550fe0c19471eefaf9d8d84))
- **frontend/translations:** Remove unused namespace ([0b9f6d6](https://gitlab.com/biomedit/portal/commit/0b9f6d6d0272f0f64f9aac10e9365bf1afd93feb))

### ✅ Test

- **backend/factories:** Do not suppress signals ([099c604](https://gitlab.com/biomedit/portal/commit/099c60437b9aa5873bda7ada29bb0cd4fc47b794))
- Remove factory_boy warning ([ab81cf1](https://gitlab.com/biomedit/portal/commit/ab81cf14dab2bc4969894f37b84acde45a8413ef))
- Fix flaky tests ([21a408a](https://gitlab.com/biomedit/portal/commit/21a408a6df7253d05582c671643ba0a5458e1f1e))

## [8.2.0](https://gitlab.com/biomedit/portal/-/releases/8.2.0) - 2024-09-27

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/8.1.0...8.2.0)


### ℹ️ Actions Needed

- add NEXT_PUBLIC_TERMS_OF_USE_ENABLED to frontend env file

### ✨ Features

- **frontend/Toolbar:** Change toolbar action buttons ([d34f66d](https://gitlab.com/biomedit/portal/commit/d34f66d212925e7f30f1d9d563404bf2fa940382)), Closes #1083
- **backend/notifications:** Add filename to data package landed email ([5e35ea2](https://gitlab.com/biomedit/portal/commit/5e35ea215fd32e7675d234ef81ae2490f8f4d4f2)), Close #1104
- **frontend/user:** For each user show pgp key infos ([a75d255](https://gitlab.com/biomedit/portal/commit/a75d2557c7051e3e93d16a34087203ee4cfcc670)), Closes #1080
- **backend/user:** For each user show pgp key infos ([bb07652](https://gitlab.com/biomedit/portal/commit/bb07652095ea4a0f7f39a02c2ef91751dd5dea5f))
- Show accepted terms of use in user profile ([e3c56ce](https://gitlab.com/biomedit/portal/commit/e3c56ceeee9adc3612e75d80219bc3510fb25cc9)), Closes #1095
- **frontend/projects:** For each user show the latest accepted verison of the terms of use ([0775b67](https://gitlab.com/biomedit/portal/commit/0775b6799c9860c91778ae9b31464c7f41f67fe1)), Close #1096
- **backend/projects:** For each user return the latest accepted version of the terms of use ([c9b14d4](https://gitlab.com/biomedit/portal/commit/c9b14d46c88fb4707e4a14d32f077fde86bc77ed))
- **backend/projects:** Do not send permission email for minimal role ([9700f67](https://gitlab.com/biomedit/portal/commit/9700f67863030f883477fee75d695478f04e06d8)), Close #1088
- **backend/projects:** Do not send email for user addition or deletion ([ec1347e](https://gitlab.com/biomedit/portal/commit/ec1347e8f49e82e365241c2b049b1011bbc1e8da))
- **frontend/app:** Group administration menu items together ([ec44e4c](https://gitlab.com/biomedit/portal/commit/ec44e4cfd0397222ae236421199216ff7ab17d8e)), Close #1091
- **frontend/terms-of-use:** Add page for new terms of use ([dc9bc00](https://gitlab.com/biomedit/portal/commit/dc9bc001de3c909f80955b86c2207c5e649c933a)), Close #1074
- **frontend/terms-of-use:** Add terms of use detail page ([dcbacb5](https://gitlab.com/biomedit/portal/commit/dcbacb5be7aab95aa6503fce55d9a76118cdc888))
- **frontend:** Add terms of use page ([0c90195](https://gitlab.com/biomedit/portal/commit/0c901955492ce102e27046f0d31f2e75223039df))
- **frontend/config:** Add terms of use feature toggle ([d9524d4](https://gitlab.com/biomedit/portal/commit/d9524d4ac9db65ce3904c75d34e5d1f4ccd23cbd))
- **frontend/redux:** Add terms of use related actions ([1bb8159](https://gitlab.com/biomedit/portal/commit/1bb8159e6565d43c86b057e3763673d4c3922ea5))
- **frontend/Projects:** Always show all tabs ([ea7a51e](https://gitlab.com/biomedit/portal/commit/ea7a51e23be48480b0fd745e62f3450e515a0df5)), Closes #1089
- **backend/Users:** Improve page when user is inactive ([6015bf2](https://gitlab.com/biomedit/portal/commit/6015bf265580d1a20633b239e50e7f81a76a9fea)), Closes #1084
- **frontend/node:** Move users to a separate tab ([4beb252](https://gitlab.com/biomedit/portal/commit/4beb25242285aebf6a94a1a2bbf107997f8d36df)), Close #1086
- **frontend/data-provider:** Move users to a separate tab ([ab995fe](https://gitlab.com/biomedit/portal/commit/ab995fe8ad8ebfd147582f6e41319dac040fa0d7)), Close #1087
- **frontend/project:** Use title case for column headers ([28f47ec](https://gitlab.com/biomedit/portal/commit/28f47ec064d44ac9d6094a3678307b4751cdc068))
- **frontend:** Add RolesTable widget ([42680c6](https://gitlab.com/biomedit/portal/commit/42680c6004782ef67e9fc253bf31981933c6f723))
- **backend/users:** Add terms of use to the detail endpoint ([ffd89bc](https://gitlab.com/biomedit/portal/commit/ffd89bc77833ed50699b19821514e451f9c2de20))
- **backend/terms-of-use:** Add users to the detail endpoint ([51a7b38](https://gitlab.com/biomedit/portal/commit/51a7b38dae4d24934c91bd8693ff409efe166cdb))
- **backend/views:** Add POST endpoint for accepting terms of use ([06e6937](https://gitlab.com/biomedit/portal/commit/06e6937a6f5298809963cbff362087bc01fa2312))
- **backend/admin:** Add support for acceptance in terms of use page ([87f0de7](https://gitlab.com/biomedit/portal/commit/87f0de713070623c3cd7f2d378b67cec92175dab))
- **backend/models:** Add terms of use acceptance entity ([7420e6b](https://gitlab.com/biomedit/portal/commit/7420e6b0752970042d0b24eecc39c8dc4203f5cb))
- **backend/services:** Use nested router for project (member) services ([32804e3](https://gitlab.com/biomedit/portal/commit/32804e3057e3b5cf8a6fe8718eb01676c9ac2e79))
- **frontend/projects:** Add member service list to project tab ([fdb35f2](https://gitlab.com/biomedit/portal/commit/fdb35f26d9626efcdc64be0f43b13ddf39a5e8cf))
- Implement project member services with nested routers, using rest_framework_nested ([f65addb](https://gitlab.com/biomedit/portal/commit/f65addbc074f654022c6aac3d01e2af3abee797d))
- **frontend/Projects:** Display additional information in its own tab ([5aad610](https://gitlab.com/biomedit/portal/commit/5aad61027e0cc77613c4c2c349cf7da76345095a)), Closes #1078
- **frontend/DataProviders:** Show data transfers in data provider details ([be2d33a](https://gitlab.com/biomedit/portal/commit/be2d33aeb90ebb3cdd40416826fc9f86adbf9cef)), Closes #1070
- **frontend/structure:** Only show project menu item to users with acces to projects ([9e6256f](https://gitlab.com/biomedit/portal/commit/9e6256f03c3915f5ef0fc7aaf057f59e7fd7c2ca)), Close #1085

### 🐞 Bug Fixes

- **frontend/PgpKeyInfoManageForm:** Display right message for expired keys ([9a09d4b](https://gitlab.com/biomedit/portal/commit/9a09d4bcb930b856966f6dbfa1b5e3b57c725039)), Closes #1105
- **PgpKeyInfoManageForm:** Catch early empty key user ID ([bc49603](https://gitlab.com/biomedit/portal/commit/bc496036527b7639ab86b26aae2da1d008d2838d)), Closes #1102
- **frontend/structure:** Do not show collapsible items when no subItem is available ([987c9d9](https://gitlab.com/biomedit/portal/commit/987c9d9349cef13a67cf54454cddcf08e9c00c63))
- **backend/terms-of-use:** Use correct serializer for users ([abc4d70](https://gitlab.com/biomedit/portal/commit/abc4d70fa01b7d6e776ef1f90f7f3ff8d6cc6c95))

### 🚀 Performance

- **frontend/ProjectDetail:** Memoize tab model ([54678d0](https://gitlab.com/biomedit/portal/commit/54678d03cadfe63b517dce6701a3dca426c1c27e))

### 🧱 Build system and dependencies

- Refreshing `package-lock.json` ([c97bcf9](https://gitlab.com/biomedit/portal/commit/c97bcf91005eca72fe4137ce134fa036ee121bba))
- Automerge `lockFileMaintenace` MRs ([8404729](https://gitlab.com/biomedit/portal/commit/84047299e2849fd3d34c0338546124b0319015b0)), Closes #1090
- Regenerate frontend API ([21e9844](https://gitlab.com/biomedit/portal/commit/21e98445009351725f4865231fb6aadde9d309e0))

### 📝 Documentation

- Add info about URL of local-prod portal ([b15be00](https://gitlab.com/biomedit/portal/commit/b15be0097a9f0b29776f6a03c04ced1dc37955f5))

### 🧹 Refactoring

- Provide a better title for selected TOU ([1fea52e](https://gitlab.com/biomedit/portal/commit/1fea52ef78db4af2e2656633f094a541b2f2d68d))
- **frontend/structure:** Improve typing ([9f28b44](https://gitlab.com/biomedit/portal/commit/9f28b448c5eb1d327f002ba4f955e1957fa4eda0))
- **frontend/profile:** Fix tab ids ([273b600](https://gitlab.com/biomedit/portal/commit/273b600c8b0127bbccb7b5b7056d11df2d7ab1b5))
- **frontend/project:** Use RolesTable component for displaying users ([15143a8](https://gitlab.com/biomedit/portal/commit/15143a87638bc33010b949eca47bbb3df2b1419b))
- **backend:** Rename terms of usage to terms of use ([6521f4b](https://gitlab.com/biomedit/portal/commit/6521f4bc40e7cc73078adc05d1a3433edcf44bec))
- Remove sett-rs dependency ([92eace2](https://gitlab.com/biomedit/portal/commit/92eace264cce60a0cc4d894cf9c80ec770cc3615)), Close #1069

### ✅ Test

- **backend/services:** Adopt tests to new urls ([732b7c8](https://gitlab.com/biomedit/portal/commit/732b7c80b780ac1da7cc613aacd6227df4c11d46))

## [8.1.0](https://gitlab.com/biomedit/portal/-/releases/8.1.0) - 2024-08-08

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/8.0.0...8.1.0)


### ✨ Features

- **frontend/ProjectUserTable:** Use verified icon to display authorized users ([4336832](https://gitlab.com/biomedit/portal/commit/43368320a1064b0a78f9b29b47b689e532ea0b47))
- **backend/DataTransferSerializer:** Drop requirement for approved key when showing credentials ([53a68c1](https://gitlab.com/biomedit/portal/commit/53a68c1833e3dd6d7696429beab8b27dcf538e94)), Close #1079
- **backend/sts:** Drop requirement for approved key when fetching credentials ([c54c0c2](https://gitlab.com/biomedit/portal/commit/c54c0c29c4ea9d59c561cfc40bb5d597d483a074))
- **frontend/ProjectUserTable:** Add a column for the new project role MINIMAL ([da0b9c0](https://gitlab.com/biomedit/portal/commit/da0b9c0f032911553246f2138141ef9ba364bf54))
- **backend/project:** Send changed permission email to node ticketing system ([f400aba](https://gitlab.com/biomedit/portal/commit/f400aba9e317ddf8003d826522ee4d7392a521fb)), Close #870
- Add new project user role `MINIMAL` ([adc1cfe](https://gitlab.com/biomedit/portal/commit/adc1cfe82bf21d6e36a14b29824f7f7dd800b050)), Closes #1032
- **frontend/DataTransferForm:** Replace max packages radio button with dropdown menu ([e53d896](https://gitlab.com/biomedit/portal/commit/e53d896f4f1e9b59ff245ffb6cc0e506ad69fa95)), Close #1068
- **backend/admin:** Add terms of usage ([f20834f](https://gitlab.com/biomedit/portal/commit/f20834fe71dc3e0dfcd1cfe12de7b29897fd859b)), Close #1073
- **backend/terms-of-usage:** Add serializer and view ([07e2a0e](https://gitlab.com/biomedit/portal/commit/07e2a0e99aff9e73cc2d79bced34c36397b2fdbd))
- **backend/models:** Add terms of usage entity ([a5c7c74](https://gitlab.com/biomedit/portal/commit/a5c7c748645a2ec533d46a4c3a1eafbd43e4ec70))
- **frontend:** Use type url for input fields when relevant ([ea935ba](https://gitlab.com/biomedit/portal/commit/ea935bae066d6d8f6f585be525fd94f730e68065))

### 🐞 Bug Fixes

- **oidc:** Return a `HttpResponseForbidden` when user is not active ([b3dff74](https://gitlab.com/biomedit/portal/commit/b3dff74b0e70f8f8cde874baf694108db86bfe55)), Closes #1076
- **backend/filters:** Make email lookup case insensitive ([88a1f20](https://gitlab.com/biomedit/portal/commit/88a1f2042be0b55aea89357f66e34e89a8f1e595)), Close #1064
- **frontend/DataTransferDetail:** Display max packages consistently ([9c5e664](https://gitlab.com/biomedit/portal/commit/9c5e6642a6236fe9c2b24d6d2bead0973892654e))

### 🧱 Build system and dependencies

- **git-cliff:** Add actions needed title only once ([437d27f](https://gitlab.com/biomedit/portal/commit/437d27f777664f179c1e0e582ea8423deed6e8be)), Close #977

### 📝 Documentation

- **backend/config:** Make authorized_user example more descriptive ([04ad294](https://gitlab.com/biomedit/portal/commit/04ad294c7df9d83d096c814fdb13ae0c0484a0a2))

### 🧹 Refactoring

- **backend/check-dpkg:** Rename function according to new requirements ([b0a405c](https://gitlab.com/biomedit/portal/commit/b0a405c82f9b979895bb8752f312d44d7829ef8c))
- **frontend/DataTransferHooks:** Remove unreachable case ([e076275](https://gitlab.com/biomedit/portal/commit/e07627517d893a244aa734d94f734bd9e88cf63c))
- **backend/permissions:** Improve permission classes ([342936a](https://gitlab.com/biomedit/portal/commit/342936aeb1865e4e7515b5f46e19b8d6c392fb7a))

## [8.0.0](https://gitlab.com/biomedit/portal/-/releases/8.0.0) - 2024-07-30

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/7.0.0...8.0.0)

### ⚠ BREAKING CHANGES

- - Remove profile from /users list endpoint
- Remove flags from /users list endpoint
- Remove display_name from /users list endpoint

### ℹ️ Actions Needed

- Use the new .yml configuration file for FluentBit.

### ✨ Features

- **frontend/Projects:** Move project users to a separate tab ([b6f3c3f](https://gitlab.com/biomedit/portal/commit/b6f3c3f11aa198eefb09673a62941873ecc881d9)), Closes #1044
- **users:** Use lite serializer for list endpoint ([2bffd84](https://gitlab.com/biomedit/portal/commit/2bffd8406ec92ca3ed692982e016a3fb5fe8fc0f)), ⚠ BREAKING CHANGE: - Remove profile from /users list endpoint - Remove flags from /users list endpoint - Remove display_name from /users list endpoint
- **backend/data-package:** Send email when a data package log is created ([0932b1b](https://gitlab.com/biomedit/portal/commit/0932b1bdfca4c25e5e9fe8af1c8c388db38a794d)), Close #1039
- **backend/data-transfer:** Add sender and recipient properties do data package ([d25da64](https://gitlab.com/biomedit/portal/commit/d25da6451748cee1a8e9d8107bdf1cfb349b74a9))
- **frontend/groups:** Add group role to table ([7a566ef](https://gitlab.com/biomedit/portal/commit/7a566ef93f66cdd0668e5a82859e65a7441a95d5)), Close #1050
- **frontend/Projects:** Display more project related information ([909b333](https://gitlab.com/biomedit/portal/commit/909b3331499569949cbc9d4080da6f73087f4244)), Closes #1030
- Switch from `gunicorn` to `uvicorn` ([30e9954](https://gitlab.com/biomedit/portal/commit/30e99548a458df0ff124c3ab6aa8d17344b79d56)), Closes #1043
- **identity_provider:** Turn project identity provider into a db table ([ded613e](https://gitlab.com/biomedit/portal/commit/ded613e2c59950170f535dd826a7f12f047e10c4)), Closes #1054
- **frontend/users:** Turn details popup into separate routable page ([0381a37](https://gitlab.com/biomedit/portal/commit/0381a378beec0d60fd112634b75e32b3c78dbd74))
- **frontend/nodes:** Turn details popup into separate routable page ([9b566ae](https://gitlab.com/biomedit/portal/commit/9b566ae8d2b828d232f8d762069473f3aab4f234))
- **frontend/data-providers:** Turn details popup into separate routable page ([a398c80](https://gitlab.com/biomedit/portal/commit/a398c802928df1a238a4f904774312842136f9c3))
- **frontend:** Split administration page into separate routable pages ([0665efe](https://gitlab.com/biomedit/portal/commit/0665efed0a55ed993b63d63d58e0faeea86d261c)), Close #989
- **frontend/structure:** Use avatar as profile icon ([eec1d84](https://gitlab.com/biomedit/portal/commit/eec1d846b16493f3d950a10a68799a6158303060))
- **backend/notifications:** Include data specification in approval request email body ([55c838d](https://gitlab.com/biomedit/portal/commit/55c838dc86f4834c2cc8721d959af66ca02f7ccb)), Close #1051
- **frontend/DataTransfer:** Display all data transfer fields ([1cb94d1](https://gitlab.com/biomedit/portal/commit/1cb94d1446a5f3dacbaa73c54ae2a3ca2faa8eef)), Closes #1027
- Add identity_provider for each project ([f2ea000](https://gitlab.com/biomedit/portal/commit/f2ea000ac43c66a1dcfc2ad6bcc8cd7d9fbf4e39))

### 🐞 Bug Fixes

- **backend/users:** Fix export for non staff users ([6f3a4e3](https://gitlab.com/biomedit/portal/commit/6f3a4e3c7d8a5ad0f113f535d2c599a91fafbb5b))
- **DataTransfer:** Specify throttle for unsafe methods ([5c02c3f](https://gitlab.com/biomedit/portal/commit/5c02c3f653ef46beba8d55f5d00aa18c27826faf)), Closes #1066
- **DataTransfer:** `isSubmitting` is for form submission ([a9b4762](https://gitlab.com/biomedit/portal/commit/a9b4762a94c827b4c17c804c37f951b8cd9c3921)), Closes #1066
- **user:** Re-introduce display name for `UserLiteSerializer` ([0c20181](https://gitlab.com/biomedit/portal/commit/0c20181585b6bb8ab45d003e3a03d2555bbb4947)), Closes #1071, Relates #1065
- `addUserConfirmationText` is not translated ([2f12801](https://gitlab.com/biomedit/portal/commit/2f12801ff3ec5e16133441825b23d4f9fb04566b)), Closes #1071
- **frontend/users:** Fix captions ([b6b8aba](https://gitlab.com/biomedit/portal/commit/b6b8aba9eb5541b7091eeb1781c2388682afec21))
- **backend/notifications:** Do not show data provider warning to data specification approver ([a18ba25](https://gitlab.com/biomedit/portal/commit/a18ba2524e4975ad433b31450b93b70bc9138cb0))
- **ProjectSerializerLite:** Project serializer should contain every first-level simple attribute ([8626c63](https://gitlab.com/biomedit/portal/commit/8626c63cbbc5f3856cb3fbdb02f770c66cced8aa)), Closes #1047
- **otel-collector.yml:** Specify `command` which extends original `cmd` ([97d5e45](https://gitlab.com/biomedit/portal/commit/97d5e453e8cc0e647a762bdd0bc0ec1d6de0e9dc)), Closes #1036

### 🚀 Performance

- **backend/data-transfers:** Improve endpoint performance by prefetching information ([171c721](https://gitlab.com/biomedit/portal/commit/171c721d4d3afc91f757563c277f2f4f6deb2003)), Close #1065
- **backend/groups:** Improve endpoint performance by prefetching information ([aaa10ee](https://gitlab.com/biomedit/portal/commit/aaa10eebbb7bbdd0c56c209598a68c8382690055))
- **frontend/data-transfer:** Memoize breadcrumbs ([449e30a](https://gitlab.com/biomedit/portal/commit/449e30a1268ea42ae17bef0fc3c5505c9ca5615a))

### 🧱 Build system and dependencies

- **generator-cli:** Move to the currently latest version `7.7.0` ([08880ef](https://gitlab.com/biomedit/portal/commit/08880ef3b7de7f451b5ddb93e4f97a78f7a15894))
- **bumpversion:** Improve git cliff configuration ([510bc80](https://gitlab.com/biomedit/portal/commit/510bc80ad80055834b5516ab149e08f9c2523e18))
- **fluent-bit:** Replace filters with processors ([02ac557](https://gitlab.com/biomedit/portal/commit/02ac557f2bc18ba48005659d6370d1f5a046f8ec))
- **fluent-bit:** Use YAML configuration format ([369539d](https://gitlab.com/biomedit/portal/commit/369539d4af764e3c1240461a48d5f55e49a00c7a)), Close #1018
- **frontend:** Update next-widgets to 18.2.0 ([0ea0a70](https://gitlab.com/biomedit/portal/commit/0ea0a70a143212478ceaebb3587523f7ffe5ed08))

### 🧹 Refactoring

- **backend/users:** Improve readability of list_users ([8805cc8](https://gitlab.com/biomedit/portal/commit/8805cc871e11c945c214994fc27e3e711db3ccd1))
- **frontend/AdministrationHooks:** Move hooks closer to the actual component ([70d9135](https://gitlab.com/biomedit/portal/commit/70d9135ba487eaf391ac5500f115686c312d7c00))
- **frontend/permissions:** Improve typing for page permissions ([5599bce](https://gitlab.com/biomedit/portal/commit/5599bcef9ec45153b60fee9fb754fa5e5352dba4))
- **backend/approval:** Move is_data_specification_approval to BaseApproval ([1461a84](https://gitlab.com/biomedit/portal/commit/1461a8492a8a614270174759a67db367d9d7276b))

## [7.0.1](https://gitlab.com/biomedit/portal/-/releases/7.0.1) - 2024-07-19

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/7.0.0...7.0.1)


### 🧱 Build system and dependencies

- Bump Django ([0d765d2](https://gitlab.com/biomedit/portal/commit/0d765d22c17e9895a8fd7c909e824f54ead2b79b)), Close #1062

## [7.0.0](https://gitlab.com/biomedit/portal/-/releases/7.0.0) - 2024-06-10

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.6.0...7.0.0)

### ⚠ BREAKING CHANGES

- /messages endpoint has been removed
- `/projects2` has been removed and merged
  with `/projects`. For the list-projects endpoint it concretely
  means that it does no longer display as much information as it
  used to do. Following fields are now part of the list-projects
  endpoint: `id`, `code`, `name`, `archived`, `expiration_date`,
  `destination`, `created`, `data_transfer_count`, `user_has_role`.
- Rename sender_pgp_key_info to sender_open_pgp_key_info in /data-package and
/data-transfer
- Rename data_transfers_count to data_transfer_count in /projects2
- - remove the following fields from /data-transfer list endpoint:
  - node_approvals
  - data_provider_approvals
  - group_approvals
  - requestor_name
  - requestor_first_name
  - requestor_last_name
  - requestor_display_id
  - requestor_email
  - project_archived
  - can_see_credentials
  - permissions
  - data_provider
  - requestor
  - legal_basis
  - creation_date
  - data_specification
  - data_recipients
- Rename /userscsv to /exports/users
- remove /data-transfer/<id>/s3opts endpoint
- - Remove dtr attribute from /sts response;
- Change storageEndpoint to endpoint in /sts response.

### ℹ️ Actions Needed

- Run the add_default_legal_basis script

### ℹ️ Actions Needed

- Run the add_default_data_specification script

### ℹ️ Actions Needed

- remove NEXT_PUBLIC_SHOW_PROJECTS_V2 from frontend/.env

### ✨ Features

- **frontend/DataTransferForm:** Validate legal basis length ([88ea86d](https://gitlab.com/biomedit/portal/commit/88ea86de3c5cf67a81e1c2c6822c1d5bb09bd1a7)), Close #1040
- **backend/Messages:** Remove messages menu item ([bca4731](https://gitlab.com/biomedit/portal/commit/bca47317b5f6f94363df4aacbda8ad9f3587e725)), ⚠ BREAKING CHANGE: /messages endpoint has been removed, Closes #1017
- **frontend/Messages:** Remove messages menu item ([6e683d5](https://gitlab.com/biomedit/portal/commit/6e683d5496165457f921a4e0eb8bb563e1a17d51)), Closes #1017
- Make network notification more visible for users ([772fd53](https://gitlab.com/biomedit/portal/commit/772fd536304813c9c728564080e153c4e7cd3436))
- **project:** Remove project endpoint duplication ([3bc7d58](https://gitlab.com/biomedit/portal/commit/3bc7d58691c4123918e7b41b155eccf1ffb0f8b3)), ⚠ BREAKING CHANGE: `/projects2` has been removed and merged   with `/projects`. For the list-projects endpoint it concretely   means that it does no longer display as much information as it   used to do. Following fields are now part of the list-projects   endpoint: `id`, `code`, `name`, `archived`, `expiration_date`,   `destination`, `created`, `data_transfer_count`, `user_has_role`., Closes #1022
- **backend:** Improve consistency when dealing with OpenPGP keys ([3cef71f](https://gitlab.com/biomedit/portal/commit/3cef71f9f0761fb3c3aa85a3bacd62b049bed223)), ⚠ BREAKING CHANGE: Rename sender_pgp_key_info to sender_open_pgp_key_info in /data-package and /data-transfer
- **backend/urls:** Rename /pgpkey to /open-pgp-keys ([eec410e](https://gitlab.com/biomedit/portal/commit/eec410eb62d1ff306e64ce2518e1a0b2c2905463))
- **frontend:** Improve consistency when dealing with OpenPGP keys ([8068a4c](https://gitlab.com/biomedit/portal/commit/8068a4ca8fb5771e907415c342be9bc4061a1430))
- **backend/scripts:** Add default legal basis script ([98dc002](https://gitlab.com/biomedit/portal/commit/98dc0025dc1aa310a2ebcb16023aa3ecd0c8c9d1))
- **backend/scripts:** Add default data specification script ([7bedf5a](https://gitlab.com/biomedit/portal/commit/7bedf5afbf7918de402254eadf72518ec3cae2de)), Close #1025
- **frontend/ProjectDetail:** Remove redundant table title ([475008b](https://gitlab.com/biomedit/portal/commit/475008ba1b943c219c808dec196bdd87efae416b))
- **frontend/Projects:** Display all project fields ([189ee0e](https://gitlab.com/biomedit/portal/commit/189ee0e7706c314986498742e2c63b2f66a90eeb)), Closes #1019
- **project:** Improve consistency of project serializers ([7f5b503](https://gitlab.com/biomedit/portal/commit/7f5b5036a1ce7172f48d918216fa57993c22d48c)), ⚠ BREAKING CHANGE: Rename data_transfers_count to data_transfer_count in /projects2
- **frontend/data-transfer:** Use the new backend endpoint for exporting ([8ae19bc](https://gitlab.com/biomedit/portal/commit/8ae19bcbfb9c2efddc420f4aa76d3e92997fce06))
- **backend/data-transfer:** Add export ([7d49322](https://gitlab.com/biomedit/portal/commit/7d49322a0c6978273966feda566811bf5b2d6bd0))
- **frontend/projects:** Switch to new projects page ([c81c337](https://gitlab.com/biomedit/portal/commit/c81c337650e2f23721f2cf3d7c842ec3804372d1)), Close #971
- **frontend/projects2:** Add my projects and archived tab ([574d59a](https://gitlab.com/biomedit/portal/commit/574d59aea69b6997513210a39b56e855cf026625)), Close #992
- **backend/projects2:** Add user_has_role field ([d56846b](https://gitlab.com/biomedit/portal/commit/d56846be34e4acf278f6216e4c5604ad22de4cf9))
- **backend/sts:** Remove dtr from response and change storageEndpoint to endpoint ([4647769](https://gitlab.com/biomedit/portal/commit/464776918caca27a04c097908f72f6897510dfbd)), ⚠ BREAKING CHANGE: - Remove dtr attribute from /sts response; - Change storageEndpoint to endpoint in /sts response.
- **frontend/projects:** Show project name instead of id in the breadcrumbs ([9099a0a](https://gitlab.com/biomedit/portal/commit/9099a0a1e738f997824a4be3207e806440ad90a4)), Close #1015
- **data-transfer:** Align behavior for legal basis and data specification ([1c008bc](https://gitlab.com/biomedit/portal/commit/1c008bc04aaec15652574d64d393e3a32426f184))
- **backend/notifications:** Adjust email body for data transfer notifications ([cb3a21e](https://gitlab.com/biomedit/portal/commit/cb3a21e53616413c7e49643d742faa5da1d2d0c3))
- **backend/data-transfer:** Adjust data transfer creation notification ([bda2ac1](https://gitlab.com/biomedit/portal/commit/bda2ac18149349c00d4437e11676ab1c1041e0b5))
- **backend/DtrAuthorizedNotification:** Only mention requestor and data recipients in the email ([9e5811b](https://gitlab.com/biomedit/portal/commit/9e5811b4fe0333dc850146448b6da9b83e0bc643))
- **backend/DtrDataProviderApprovalNotification:** Move data provider managers to cc ([becd75d](https://gitlab.com/biomedit/portal/commit/becd75deec1ab708948fc6e0516c5ca0bf52542d))
- **data-transfer:** Add new data provider declaration ([14532fc](https://gitlab.com/biomedit/portal/commit/14532fc2eb7f8b4c8266cde2823b411a47416f56))
- **data-transfer:** Remove obsolete checkbox ([a4a1eb1](https://gitlab.com/biomedit/portal/commit/a4a1eb160ff1b66cedf69970d255af4f6d95c03f))
- **frontend/DataTransferForm:** Improve data recipients caption ([4ec7c11](https://gitlab.com/biomedit/portal/commit/4ec7c11698f2c637ba40e3d7a8701e0c4e187725))
- **frontend/projects2:** Add routing and improve consistency ([b57f249](https://gitlab.com/biomedit/portal/commit/b57f249fdf6418b24e95a6810a817a06ad2d1443)), Close #990
- **frontend/projects:** Add archive and unarchive buttons ([8a42cbc](https://gitlab.com/biomedit/portal/commit/8a42cbc032632531432ae14e2ae49cbcb2cc4bf0))
- **frontend/projects:** Add edit and delete buttons ([3e8b918](https://gitlab.com/biomedit/portal/commit/3e8b9186ac7af9d5b602e4447be7f82118590309))
- **frontend:** Add a routable project details page ([1a2ed0c](https://gitlab.com/biomedit/portal/commit/1a2ed0c92cae0b942b6fbdd4dbb8988075b20983))
- **frontend:** Add RETRIEVE_PROJECT action ([591532b](https://gitlab.com/biomedit/portal/commit/591532b76d69ea5fe966fb45562b7242010de20a))
- **frontend/DetailedViewToolbar:** Add support for additional buttons ([65ec341](https://gitlab.com/biomedit/portal/commit/65ec341da6cefcb5c5a1e7d374a1153636ef841a))
- **backend:** Add data transfer notification for approval requested ([feeb06b](https://gitlab.com/biomedit/portal/commit/feeb06b825bd7de960a19e4f0f6b100962cf5bfe)), Close #985
- **backend:** Change data transfer notification subjects and bodies ([f05e661](https://gitlab.com/biomedit/portal/commit/f05e6616c1dade4f53426777abebb6f55ed2b809))
- **backend:** Change data transfer notification recipients ([8913761](https://gitlab.com/biomedit/portal/commit/89137614fb29a0f5bd72819fd4520019e847709c))
- **frontend/Approvals:** Make sure the data provider approval comes last ([57b2ee4](https://gitlab.com/biomedit/portal/commit/57b2ee4f7fd65f57a98ceeb4eb78fa8cce31fc0c)), Closes #984

### 🐞 Bug Fixes

- **SSHPublicKeyManageForm:** `users` are no longer fetched along with projects ([16fed22](https://gitlab.com/biomedit/portal/commit/16fed22f3bbeab3ead04dfae271599d6c4e20e4e)), Closes #1045
- **SSHPublicKeyManageForm:** `userProfileDialog.sshPublicKey` removed ([68859be](https://gitlab.com/biomedit/portal/commit/68859be5d3f8de452c23bb354f912b43a2efda8a)), Closes #1045
- **backend/project-expiration-task:** Change email body to be a string ([79a17ec](https://gitlab.com/biomedit/portal/commit/79a17ec436299e0d715a5e58225c0341b4dd2092))
- Restore deprecated `pgpkey/status` endpoint ([de6eac6](https://gitlab.com/biomedit/portal/commit/de6eac6990fc7eb4b5dcd99395b1c0139aa3be13)), Close #1037
- Display email address as well ([6595735](https://gitlab.com/biomedit/portal/commit/65957355dc023e5e46bb020e90d2c5421cc2c976))
- **frontend/DataTransferDetail:** Retrieve project when editing a data transfer ([0f4447b](https://gitlab.com/biomedit/portal/commit/0f4447b7d18126728a7b534e30831c288c0c3128))
- **frontend/data-transfer:** Update misleading global search helper text ([b16add5](https://gitlab.com/biomedit/portal/commit/b16add518f4731cd716ef8aaac45de1a9ed84788))
- Check for non-None `role_entity_type` before getting `model` attribute ([9381104](https://gitlab.com/biomedit/portal/commit/9381104c60a91ce43e7184a6752e8562a38850a2)), Closes #1014
- **frontend/DataTransferApprovalStack:** Only show tooltip when relevant ([3a44524](https://gitlab.com/biomedit/portal/commit/3a44524ffdf2f7ece35e1d680c8e3e2014d4e5ba))

### 🚀 Performance

- Breadcrumb should not refresh the page ([94c7f84](https://gitlab.com/biomedit/portal/commit/94c7f8442e2b7241c1be4ba474ab6a29415e59ff))

### 🧱 Build system and dependencies

- Using `v7.5.0` from `generator-cli` ([70f524f](https://gitlab.com/biomedit/portal/commit/70f524febb13aa6ca61236c86b94e945b259334c))
- **frontend:** Remove dependencies related to exporting ([1c2d710](https://gitlab.com/biomedit/portal/commit/1c2d710d139afc96814afcf750f1d6db3f5d181c))

### 👷 CI

- **danger:** Fix check action needed task ([7df510e](https://gitlab.com/biomedit/portal/commit/7df510ebe04f98cbdc8cafaaa029d68ab7e8e478))
- Switch to merge request pipelines ([650379f](https://gitlab.com/biomedit/portal/commit/650379f8301eb03745382adc1a08d31bd3c55a6b)), Close #1006

### 📝 Documentation

- Improve and extend development setup ([92ec0c3](https://gitlab.com/biomedit/portal/commit/92ec0c3e4a993ff8a8a7f5aca39a58ea30bf8b19))
- Correct Danger setup ([aeaca75](https://gitlab.com/biomedit/portal/commit/aeaca75aa2ed50e4cea9c0bf518526606ee1ae91))

### 🧹 Refactoring

- **backend/urls:** Make ssh endpoint basename consistent with the rest ([7ad1902](https://gitlab.com/biomedit/portal/commit/7ad1902601b3b947cd1b11741e43315e6c6ac4c4))
- **frontend/pages:** Remove unused keys page ([608a00a](https://gitlab.com/biomedit/portal/commit/608a00a7cebfabfcaa600824dfddec29954a22c7))
- **data-transfer:** Switch to lite serialzier for list view ([9d0cb2d](https://gitlab.com/biomedit/portal/commit/9d0cb2d18c1616b6d45bd485b2b3aca6220d99e7)), ⚠ BREAKING CHANGE: - remove the following fields from /data-transfer list endpoint:   - node_approvals   - data_provider_approvals   - group_approvals   - requestor_name   - requestor_first_name   - requestor_last_name   - requestor_display_id   - requestor_email   - project_archived   - can_see_credentials   - permissions   - data_provider   - requestor   - legal_basis   - creation_date   - data_specification   - data_recipients, Close #991
- **backend/users:** Refactor export ([277104a](https://gitlab.com/biomedit/portal/commit/277104a00b61812b5fc0af7fde7f36e22c534d09))
- **backend/urls:** Add exports subendpoint ([e4e0fd5](https://gitlab.com/biomedit/portal/commit/e4e0fd5302a7a2144bca427c9b6a5ff4a918e1e9)), ⚠ BREAKING CHANGE: Rename /userscsv to /exports/users
- Display empty string instead of `Project` if project's name is not known yet ([6caf9fe](https://gitlab.com/biomedit/portal/commit/6caf9fec485271a4bc24373607e72c22ed2e96f0))
- **backend/data-transfer:** Remove unused endpoint ([3584f2c](https://gitlab.com/biomedit/portal/commit/3584f2c33ce7e61b255cb69976135ee31dc7b577)), ⚠ BREAKING CHANGE: remove /data-transfer/<id>/s3opts endpoint, Close #1005
- **frontend/StsCredentials:** Use sts endpoint only ([be4cabb](https://gitlab.com/biomedit/portal/commit/be4cabbb1dc444f213961720d976393a8948e1aa))
- `profile` should always be non-None ([d994b86](https://gitlab.com/biomedit/portal/commit/d994b86b836e4ad44b9278ba296a9adfe36c76ff))
- **frontend/DataTransferApproval:** Make type guards globally available ([262de37](https://gitlab.com/biomedit/portal/commit/262de37959de0d700ea8533767f5ec80cf99bf5e))
- **backend/project:** Remove deprecation notice ([6c42baa](https://gitlab.com/biomedit/portal/commit/6c42baa2a07412470f3d10981ab18b8da8b6b55a))
- **frontend/projects:** Resolve todo ([5aa8738](https://gitlab.com/biomedit/portal/commit/5aa873897ff016b46a87008364492044d575cb49))
- **frontend/DataTransferTable:** Refactor code to be more resilient to unknown values ([fe5f47f](https://gitlab.com/biomedit/portal/commit/fe5f47f3d50c4a4ba7dac3b9184adb77fb92a0d3))
- **frontend/react-redux:** Stop using deprecated batch function ([4a589d6](https://gitlab.com/biomedit/portal/commit/4a589d67be1685c910e44b13087e56a07e354e79))
- **backend:** Do not send messages for data transfer notifications ([a127f76](https://gitlab.com/biomedit/portal/commit/a127f76ed1815014cb153ae68b0f4450ab0054d0))

### 🎨 Style

- **backend/pgp-key-info:** Use snake case instead of camel case ([842a6f9](https://gitlab.com/biomedit/portal/commit/842a6f9e8019ecbf2dc2f852f77d1366f06bfd53))

### ✅ Test

- **backend/dtr-notification:** Fix race condition ([b104198](https://gitlab.com/biomedit/portal/commit/b10419826dfd6b5c6af7cdb56fc71e423f4dad87))
- **backend/projects2:** Test get_queryset ([b0a6ab3](https://gitlab.com/biomedit/portal/commit/b0a6ab3a572673c4dd4f63ceb1d64f0b48575d5b))
- **backend:** Improve project factory ([e2f00da](https://gitlab.com/biomedit/portal/commit/e2f00da0647aa5a9b0f372afc4023e5ecb865b78))

## [6.6.0](https://gitlab.com/biomedit/portal/-/releases/6.6.0) - 2024-04-17

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.5.0...6.6.0)


### ℹ️ Actions Needed

- Run the `add_data_specification_approval_group` script

### ℹ️ Actions Needed

- add boolean variable NEXT_PUBLIC_SHOW_PROJECTS_V2 as in sample file

### ✨ Features

- **data-transfer:** Align behavior for legal basis and data specification ([1c008bc](https://gitlab.com/biomedit/portal/commit/1c008bc04aaec15652574d64d393e3a32426f184))
- **backend/notifications:** Adjust email body for data transfer notifications ([cb3a21e](https://gitlab.com/biomedit/portal/commit/cb3a21e53616413c7e49643d742faa5da1d2d0c3))
- **backend/data-transfer:** Adjust data transfer creation notification ([bda2ac1](https://gitlab.com/biomedit/portal/commit/bda2ac18149349c00d4437e11676ab1c1041e0b5))
- **backend/DtrAuthorizedNotification:** Only mention requestor and data recipients in the email ([9e5811b](https://gitlab.com/biomedit/portal/commit/9e5811b4fe0333dc850146448b6da9b83e0bc643))
- **backend/DtrDataProviderApprovalNotification:** Move data provider managers to cc ([becd75d](https://gitlab.com/biomedit/portal/commit/becd75deec1ab708948fc6e0516c5ca0bf52542d))
- **data-transfer:** Add new data provider declaration ([14532fc](https://gitlab.com/biomedit/portal/commit/14532fc2eb7f8b4c8266cde2823b411a47416f56))
- **data-transfer:** Remove obsolete checkbox ([a4a1eb1](https://gitlab.com/biomedit/portal/commit/a4a1eb160ff1b66cedf69970d255af4f6d95c03f))
- **frontend/DataTransferForm:** Improve data recipients caption ([4ec7c11](https://gitlab.com/biomedit/portal/commit/4ec7c11698f2c637ba40e3d7a8701e0c4e187725))
- **frontend/projects2:** Add routing and improve consistency ([b57f249](https://gitlab.com/biomedit/portal/commit/b57f249fdf6418b24e95a6810a817a06ad2d1443)), Close #990
- **frontend/projects:** Add archive and unarchive buttons ([8a42cbc](https://gitlab.com/biomedit/portal/commit/8a42cbc032632531432ae14e2ae49cbcb2cc4bf0))
- **frontend/projects:** Add edit and delete buttons ([3e8b918](https://gitlab.com/biomedit/portal/commit/3e8b9186ac7af9d5b602e4447be7f82118590309))
- **frontend:** Add a routable project details page ([1a2ed0c](https://gitlab.com/biomedit/portal/commit/1a2ed0c92cae0b942b6fbdd4dbb8988075b20983))
- **frontend:** Add RETRIEVE_PROJECT action ([591532b](https://gitlab.com/biomedit/portal/commit/591532b76d69ea5fe966fb45562b7242010de20a))
- **frontend/DetailedViewToolbar:** Add support for additional buttons ([65ec341](https://gitlab.com/biomedit/portal/commit/65ec341da6cefcb5c5a1e7d374a1153636ef841a))
- **backend:** Add data transfer notification for approval requested ([feeb06b](https://gitlab.com/biomedit/portal/commit/feeb06b825bd7de960a19e4f0f6b100962cf5bfe)), Close #985
- **backend:** Change data transfer notification subjects and bodies ([f05e661](https://gitlab.com/biomedit/portal/commit/f05e6616c1dade4f53426777abebb6f55ed2b809))
- **backend:** Change data transfer notification recipients ([8913761](https://gitlab.com/biomedit/portal/commit/89137614fb29a0f5bd72819fd4520019e847709c))
- **frontend/Approvals:** Make sure the data provider approval comes last ([57b2ee4](https://gitlab.com/biomedit/portal/commit/57b2ee4f7fd65f57a98ceeb4eb78fa8cce31fc0c)), Closes #984
- **frontend/data-transfer:** Add data recipients to data transfer form ([ea508d5](https://gitlab.com/biomedit/portal/commit/ea508d540fee81d618975565d022bd962b775257)), Close #987
- **backend/users:** Allow data managers to see project users ([17190fc](https://gitlab.com/biomedit/portal/commit/17190fc640411bc289bb9b2711f0b53b250f6e1c))
- **frontend/data-transfer:** Show data recipients in data transfer details ([513c822](https://gitlab.com/biomedit/portal/commit/513c8227a86b36f959b2d6278bbb90b5734e4a65))
- **backend/data-transfer:** Add data recipients ([04c253b](https://gitlab.com/biomedit/portal/commit/04c253b0ac2fa0080468f79cd4adc1b31b0e0096))
- **frontend/UserTextField:** Optionally validate the user after fetching it ([2184096](https://gitlab.com/biomedit/portal/commit/21840960cd323a9d08972f3da8bce96f49b2190b))
- Sts endpoint returns storage endpoint and bucket info as well ([2497009](https://gitlab.com/biomedit/portal/commit/2497009800bd39a6ff9c2683d82de6c2144c904a))
- **frontend/ProjectList:** Increase label width to fit data specification ([5aae5fe](https://gitlab.com/biomedit/portal/commit/5aae5fe9c1ebcf53d8c0531f65c51f1808da420a))
- **frontend/data-transfers:** Allow fair approvers to update data specification ([bc5fb58](https://gitlab.com/biomedit/portal/commit/bc5fb5849e77aea453ad81e7a2d587f55d4c009f)), Close #986
- **frontend/data-transfers:** Show and allow specifying data specification ([b4e0a82](https://gitlab.com/biomedit/portal/commit/b4e0a827a1c6d779ac2876209d502b571906b2a4))
- **backend/data-transfers:** Allow fair approvers to update data specification ([df6155b](https://gitlab.com/biomedit/portal/commit/df6155bfa8ff03390b084cbc1f18c4959a824a13))
- **backend/data-transfers:** Add data specification ([9b6dbd9](https://gitlab.com/biomedit/portal/commit/9b6dbd966b955eb13d1746f88c69041a05426272))
- **backend/DataTransfers:** Ignore purpose if missing when checking data package ([ccbc835](https://gitlab.com/biomedit/portal/commit/ccbc8353b7a1d9c725d477ddd7de00fa7a9223d0)), Closes #996
- Add data specification approval ([0aacd6a](https://gitlab.com/biomedit/portal/commit/0aacd6a042e092c649f2f27e23a7e5db29a03b23)), Close #983
- Support for OAUTH2 authentication with access token ([7201859](https://gitlab.com/biomedit/portal/commit/72018596fae76819815f6cf7d7b3a42d00f86242)), close #994
- **frontend/Projects:** Add data specification approval group ([1371282](https://gitlab.com/biomedit/portal/commit/1371282f2702b029bf93893a7df1938f20b04109)), Close #982
- **backend/projects:** Add data specification approval group ([2cac91a](https://gitlab.com/biomedit/portal/commit/2cac91a0a45351732b230f373747df3fef09a479))
- **backend/scripts:** Add script to create internal data specification approval group ([ecdaffb](https://gitlab.com/biomedit/portal/commit/ecdaffbb7d953b19970f5a79c1298f51eadad040))
- **backend/GroupProfile:** Add data specification approval role ([d8ca010](https://gitlab.com/biomedit/portal/commit/d8ca0105c4aa0fb814b69aa982ef6a1eb5f5cadb)), Close #981
- **backend/notifications:** Send project SSH key notification to node ticketing system ([e1571b7](https://gitlab.com/biomedit/portal/commit/e1571b731c87cd034170fec94197d71019ee11ec)), Closes #978
- **frontend/Projects:** Implement projects page as a table overview ([634efea](https://gitlab.com/biomedit/portal/commit/634efea1b558fe86d6391f6b5dcebeec6ed7882b)), Closes #959
- **frontend/DataTransfers:** Add breadcrumbs ([a1f29f4](https://gitlab.com/biomedit/portal/commit/a1f29f4a1250f9f074b7bcf3f5699ac8fe1d7c2a))
- **frontend/app:** Move biomedit logo on top left of the screen ([a2ccf72](https://gitlab.com/biomedit/portal/commit/a2ccf72f899672ee0f7d6be70d93c0aa74a69429))
- **frontend/DataTransferDetail:** Turn popup into a separate page ([0cbb3fa](https://gitlab.com/biomedit/portal/commit/0cbb3facf623af476e4be5db1e285e503029392d)), Close #970
- **frontend:** Show data transfers and data transfer menu item to all project users ([2a8d947](https://gitlab.com/biomedit/portal/commit/2a8d9478150a1de4390f08ac43c6dada4e18d30c))

### 🐞 Bug Fixes

- **frontend/DataTransferApprovalStack:** Only show tooltip when relevant ([3a44524](https://gitlab.com/biomedit/portal/commit/3a44524ffdf2f7ece35e1d680c8e3e2014d4e5ba))
- Logout on  `/backend` no longer possible in local development ([a05bc99](https://gitlab.com/biomedit/portal/commit/a05bc99f696be29a2e7a92853857ca98fed8ccd4))
- Make export button visible if there is something to export only ([7feff73](https://gitlab.com/biomedit/portal/commit/7feff7374ebdd359837c7946433a60faed0df24a))
- **data-package/check:** Exclude metadata checksum when computing hash ([24e3e1f](https://gitlab.com/biomedit/portal/commit/24e3e1fef3da34c9d52663b1fa493f6c9fb10f69)), Close #1003
- **frontend/DataTransferDetail:** Do not render not found page when data transfer is missing ([3f35607](https://gitlab.com/biomedit/portal/commit/3f35607d51dadde90ccb6a95e337b142ebecf4f7)), Close #993

### 🧱 Build system and dependencies

- **bumpversion:** Use git-cliff for calculating next version ([2d483e0](https://gitlab.com/biomedit/portal/commit/2d483e054fb07077d125f3d2ed7599c4b781a00b))
- Regenerate API ([c88d472](https://gitlab.com/biomedit/portal/commit/c88d47240b5c10b47f341843a7ca11b2554dce70))
- Move from py311 to py312 ([c1d1dcf](https://gitlab.com/biomedit/portal/commit/c1d1dcfb84d97f449715ddb9d7804a573e0d9376))
- Update dependency django to v5 ([4b02b9c](https://gitlab.com/biomedit/portal/commit/4b02b9c992ecb4199f2bce9d94c6729cc717e7a1)), Closes #954
- **otel:** Include requirements-otel in the list of files to renovate ([0c002da](https://gitlab.com/biomedit/portal/commit/0c002dad757249bb7fdcf721183cac2f382de8b6))
- **otel:** Fix dependency conflict ([304e1f7](https://gitlab.com/biomedit/portal/commit/304e1f7520ea399886106d430eccfea68a162fce))

### 👷 CI

- Switch to merge request pipelines ([650379f](https://gitlab.com/biomedit/portal/commit/650379f8301eb03745382adc1a08d31bd3c55a6b)), Close #1006

### 🧹 Refactoring

- **frontend/DataTransferApproval:** Make type guards globally available ([262de37](https://gitlab.com/biomedit/portal/commit/262de37959de0d700ea8533767f5ec80cf99bf5e))
- **backend/project:** Remove deprecation notice ([6c42baa](https://gitlab.com/biomedit/portal/commit/6c42baa2a07412470f3d10981ab18b8da8b6b55a))
- **frontend/projects:** Resolve todo ([5aa8738](https://gitlab.com/biomedit/portal/commit/5aa873897ff016b46a87008364492044d575cb49))
- **frontend/DataTransferTable:** Refactor code to be more resilient to unknown values ([fe5f47f](https://gitlab.com/biomedit/portal/commit/fe5f47f3d50c4a4ba7dac3b9184adb77fb92a0d3))
- **frontend/react-redux:** Stop using deprecated batch function ([4a589d6](https://gitlab.com/biomedit/portal/commit/4a589d67be1685c910e44b13087e56a07e354e79))
- **backend:** Do not send messages for data transfer notifications ([a127f76](https://gitlab.com/biomedit/portal/commit/a127f76ed1815014cb153ae68b0f4450ab0054d0))
- **frontend:** Extract common logic for using UsetTextField with multiple users ([5e181fd](https://gitlab.com/biomedit/portal/commit/5e181fdce2974e14afae01677c00c635e7507ef4))
- **backend:** Make group user serializer more generic ([b164149](https://gitlab.com/biomedit/portal/commit/b164149e0ebb5fb79d21972f1cb8f11902dea779))
- Remove transfer path from description tab ([05ae5e3](https://gitlab.com/biomedit/portal/commit/05ae5e320a91dd46ee330df7b1e91863f7eb71b4))
- Ensure non-mandatory requirement of data provider `node` attribute ([55c84ef](https://gitlab.com/biomedit/portal/commit/55c84ef98e4d87668ab550052cef0febc599a29c)), Closes #988

### 🎨 Style

- **backend/notifications:** Use single quote instead of double quote ([bc121a0](https://gitlab.com/biomedit/portal/commit/bc121a0fff25a32c8442f510ac6c9230ebd1161b))

### ✅ Test

- **backend:** Improve project factory ([e2f00da](https://gitlab.com/biomedit/portal/commit/e2f00da0647aa5a9b0f372afc4023e5ecb865b78))

## [6.5.0](https://gitlab.com/biomedit/portal/-/releases/6.5.0) - 2024-02-08

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.4.1...6.5.0)


### ℹ️ Actions Needed

- - remove pgp.end_relax from web/.config.*
- remove NEXT_PUBLIC_SETT_DOCUMENTATION from frontend/.env

### ℹ️ Actions Needed

- remove NEXT_PUBLIC_HTTPS_DTR_ENABLED from `frontend/.env`

### ✨ Features

- New `lookup` implementation for searching comma separated db fields ([f26e73d](https://gitlab.com/biomedit/portal/commit/f26e73dfa815c95f02a767cec096a8f03642dfe8)), Closes #973
- **openpgp:** Improve error handling for OpenPGP certificates ([f198db5](https://gitlab.com/biomedit/portal/commit/f198db5d915e845ce7aa0b8766d1dea4d1143d94)), Close #957
- Switch to new sett docs ([e8382da](https://gitlab.com/biomedit/portal/commit/e8382da29924b7774bf3fc84323b20926beb17eb))
- **frontend/UserDetails:** Replace user id with full name or username ([e14edeb](https://gitlab.com/biomedit/portal/commit/e14edeb6f6d1e58ff66ab47d3b4ffc6a234f933c)), Close #911
- **backend/Projects:** Notify user and PM about ssh uploads ([196cd65](https://gitlab.com/biomedit/portal/commit/196cd65268a9985529e9a4568df51fed40d06864)), Closes #900
- End SHA1 support grace period ([4e088b9](https://gitlab.com/biomedit/portal/commit/4e088b92db81f68fe1fcc0bb68f1044f87650e6b)), Close #945
- **backend/admin:** Add administration view for SSH public keys ([50afea6](https://gitlab.com/biomedit/portal/commit/50afea6874bff4749d9d54aeb8828871a3b939c9)), Closes #947
- **backend/Projects:** Add flag ssh support to project form ([436b8b3](https://gitlab.com/biomedit/portal/commit/436b8b33d75aae6683a83e75dda045cc5cdba2a0)), Closes #900

### 🐞 Bug Fixes

- **SSHPublicKey:** Long SSH keys stretch the main window ([84580ba](https://gitlab.com/biomedit/portal/commit/84580ba376f4f2195e4f013d0c979a1abe3ed268)), Closes #974
- **backend/DtrNotification:** Fix email bodies ([de852d7](https://gitlab.com/biomedit/portal/commit/de852d7b6f4aedffefba86f95c0bd851cce15c2f)), Close #964
- **oauth:** Use kebab-case for constructing name claim from project code ([9b9b48f](https://gitlab.com/biomedit/portal/commit/9b9b48f6a3dea306e0319872cb4eb03b86488a65))
- **compose/logging:** Write celery logs into a file ([caaf104](https://gitlab.com/biomedit/portal/commit/caaf1041fd0f776a188a23d6eb20d03c62a63558)), Close #939

### 🧱 Build system and dependencies

- Remove husky pre-commit hooks ([821b5f7](https://gitlab.com/biomedit/portal/commit/821b5f702bedae3cc25e9699736074ca3016f427)), Close #961
- Remove 'overrides' restrictions from package.json ([2a09bc9](https://gitlab.com/biomedit/portal/commit/2a09bc9bd9f1e2b2e1e54e752d38077ea94b5855))
- Apply changes suggested by `npm run start` ([ace1dab](https://gitlab.com/biomedit/portal/commit/ace1dab6002194c1e3f19cbb7b99aeabd20b9290)), Closes #940
- Update `opentelemetry-instrumentation-*` to their latest version ([b021678](https://gitlab.com/biomedit/portal/commit/b0216781deccfe22db768a2c5996c192ba4603e0))
- Using latest version of `msw` ([6415764](https://gitlab.com/biomedit/portal/commit/6415764bc07ebeed0f9b76e32ec7ed5ee73ac71c))
- Using latest version of `generator-cli` ([a834122](https://gitlab.com/biomedit/portal/commit/a834122db0f5e8830613fb2567889d955765c421))
- Update `react-redux` and `redux` to its latest version ([d983b62](https://gitlab.com/biomedit/portal/commit/d983b62963ea538be07d73104284f8a31d255b24))

### 👷 CI

- Make CI/CD interruptible ([980d57e](https://gitlab.com/biomedit/portal/commit/980d57ec8618732659d9a05add6905edd69555df)), Closes #969
- **commitlint:** Enforce leading blank for footer and body ([aa28f24](https://gitlab.com/biomedit/portal/commit/aa28f2409712a691363442985ec6f84d94077b62))

### 🧹 Refactoring

- **frontend:** Remove https data transfer feature toggle ([c90dbdf](https://gitlab.com/biomedit/portal/commit/c90dbdf7b4162b7a47155baef10f5fcf35a67236)), Close #944

### ✅ Test

- **frontend:** Mock API functions using jest ([b7e27f0](https://gitlab.com/biomedit/portal/commit/b7e27f075dd35be704f2c47b4783142ac098150e)), Close #958
- `make_*` methods return tuple instead of simple user ([bf3594e](https://gitlab.com/biomedit/portal/commit/bf3594e5df98e4012d565bbc78dd4175d8546430))
- Correct `GroupFactory` ([361511b](https://gitlab.com/biomedit/portal/commit/361511b486bc6c891a1b3db04a04d7619f5355ec)), Closes #919
- Remove `undici` and fix `jsdom` environment ([22df7b7](https://gitlab.com/biomedit/portal/commit/22df7b7039f89afe43f786c31863de4f50b1598f))
- Catch `reselect` false positives ([e377258](https://gitlab.com/biomedit/portal/commit/e3772584ddac3492d60422ebcf3a77e50c3aad04))
- **users:** Make sure that users endpoint only return active users ([de65d1f](https://gitlab.com/biomedit/portal/commit/de65d1f2ea1f679daeb9c9824f7b4b5e3ee124e2)), Closes #948

## [6.4.1](https://gitlab.com/biomedit/portal/-/releases/6.4.1) - 2023-11-30

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.4.1...6.4.1)


### 🐞 Bug Fixes

- **notifications:** Improve project archival email format ([7d14f13](https://gitlab.com/biomedit/portal/commit/7d14f130455a5b35e7f93555e8e7d64fd9289a16)), Close #938
- **dtr:** Improve/finalize the test making the `Credentials` tab visible ([6b445bb](https://gitlab.com/biomedit/portal/commit/6b445bbc13b02277fc50ed2637034e0900d9f8dc)), Closes #914

### 🧹 Refactoring

- **openid:** Review OpenID implementation for minio ([82dfa9f](https://gitlab.com/biomedit/portal/commit/82dfa9fc1e6e9b17bf9578dad3e79109f97f026d))

### ✅ Test

- **db_group_notification:** Add unit test for each notification method ([0fb780e](https://gitlab.com/biomedit/portal/commit/0fb780e8c5349dcb10ec3acb54c2787c2562a0e6)), Closes #916

## [6.4.0](https://gitlab.com/biomedit/portal/-/releases/6.4.0) - 2023-11-16

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.4.0...6.4.0)


### ℹ️ Actions Needed

- add variable onboarding_new_dp_data_engineer to .config.json and .config.toml as in sample files
- add NEXT_PUBLIC_PRIVACY_LINK to .env

### ✨ Features

- **frontend/footer:** Add privacy link to footer ([99e5743](https://gitlab.com/biomedit/portal/commit/99e57433b7fcc5527b91519b4da533d14f304aa6)), Closes #890
- **node/data_provider:** Add `has_[node|data_provider]_manager_permission` method ([097d5f1](https://gitlab.com/biomedit/portal/commit/097d5f10b3162e347ea4029608bd52c0fcf3b4ec))
- Display error message coming from backend ([4f28088](https://gitlab.com/biomedit/portal/commit/4f28088f7ff74f14b36bbafe274adbc1bb4af42d)), closes #905

### 🐞 Bug Fixes

- **export:** Node admins shouldn't be able to export ALL users ([2ca8c0c](https://gitlab.com/biomedit/portal/commit/2ca8c0cbec0ded411c17a6f354405505c1818624)), Closes #937
- **users:** All users can't be accessed by NAs and PLs when adding new users to a project ([ab4f0da](https://gitlab.com/biomedit/portal/commit/ab4f0da419e48348937cf80102779bf6d2011b2b)), Closes #936
- **user:** Restrict visibility of the `users` endpoint ([c061846](https://gitlab.com/biomedit/portal/commit/c06184639b125d0ba441f655d8194979254e1000)), Closes #901
- **project:** Remove `projects/../users/?unassigned=true` endpoint ([65e99ea](https://gitlab.com/biomedit/portal/commit/65e99ea3a9c30fc6be782f435317f2440f510d84))
- **UserTextField:** Remove `profile.emails` and `username` from the confirmation dialog ([b431e46](https://gitlab.com/biomedit/portal/commit/b431e465538c796dbe1c663969491150e95c481b)), Closes #932
- **notification:** Apply several modifications to dp notifications handling ([cd8427d](https://gitlab.com/biomedit/portal/commit/cd8427dac861b86889d4cbc7f75fdf11d9106919))
- **DataTransferDetail:** Try to catch any case where the rendered DTR could be `null` ([0d4d32f](https://gitlab.com/biomedit/portal/commit/0d4d32f2c559ec5c3b1c72fec4f125cd49aff6f9)), Closes #913, #922
- **links:** Allow `None` for `links` ([20c1f43](https://gitlab.com/biomedit/portal/commit/20c1f43f921e5ab21b436857a76ddaedcf138e5d))
- **dtr:** Send separate emails instead of a single email with bcc ([3c7a8ce](https://gitlab.com/biomedit/portal/commit/3c7a8ce44f1d20b9222abab3a4155402911cc36e)), Close #918

### 🧱 Build system and dependencies

- **web/start:** Remove unnecessary commands ([9e70e76](https://gitlab.com/biomedit/portal/commit/9e70e761dec5bb6dd9d1688b01260c4103c634c9))
- **web/start:** Fix linter errors ([17386c9](https://gitlab.com/biomedit/portal/commit/17386c9e0efd14994ff43db5bf6584f8896221c4))
- **web:** Move Dockerfile to the corresponding source code directory ([1a2a897](https://gitlab.com/biomedit/portal/commit/1a2a897bd5157c9bb98707cab0d0bca2300b650d))
- **postgres:** Fix database initialization ([2d33d5c](https://gitlab.com/biomedit/portal/commit/2d33d5c7cefa59696cae9f82c9c8efda565b1c74))

### 👷 CI

- **format:** Replace black with ruff for code formatting check ([1389701](https://gitlab.com/biomedit/portal/commit/13897014b3032b4741c4932dab129a8ee1a6ef00))

### 🧹 Refactoring

- **frontend:** Nicely display the affiliations if any ([6b9a01f](https://gitlab.com/biomedit/portal/commit/6b9a01fc83aaecc3daea9dcdb90cbe7de6f9cb3d))
- **next.config.js:** `msw/node` should be ignored in client mode only ([1e270ea](https://gitlab.com/biomedit/portal/commit/1e270eaab05f87e32f25b836ce8dc13224aa4026)), Closes #926
- **links:** Do not use internal resources in configuration samples ([786f63f](https://gitlab.com/biomedit/portal/commit/786f63f9a392fb2eb63861c829551d0e1dc10999))
- **user:** Restrict access to users endpoint ([ad7190a](https://gitlab.com/biomedit/portal/commit/ad7190acc080fa91a9321b99dfd4e6e7f3755b5e)), Closes #899

### 🎨 Style

- Reformat code using ruff format ([8f3c96e](https://gitlab.com/biomedit/portal/commit/8f3c96ed6421a6c2b1cbb31084e5f59df71b9007)), Close #925

## [6.3.0](https://gitlab.com/biomedit/portal/-/releases/6.3.0) - 2023-10-24

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.3.0...6.3.0)


### ℹ️ Actions Needed

- For each Node, configure its current OAuth2 client.
Until now, the client was discovered using `object_storage_url`.

### ✨ Features

- **backend/user:** Add possibility to filter portal users by `username` ([41c328e](https://gitlab.com/biomedit/portal/commit/41c328e730de110d8a4b4e7d9fcaaec21877cb5e))
- **backend/data-transfer:** Create default legal approval when no legal approval group is defined ([d4013a1](https://gitlab.com/biomedit/portal/commit/d4013a1802d5ce9e5153a0d99a174b7aa3950f5e)), Close #908
- **user:** Check additional email addresses when adding a new user ([f0eed0f](https://gitlab.com/biomedit/portal/commit/f0eed0fe9459a0f5a2e24778da94e278f8148684)), Closes #895
- **gui:** Add public ssh key to profile ([43e07b5](https://gitlab.com/biomedit/portal/commit/43e07b564f77b988329baf3923041646b5dc39cc))
- Add username and project_name to SSH public key backend ([c859ac6](https://gitlab.com/biomedit/portal/commit/c859ac683057b07c4807a02979cb0e28320fabba))
- Remove `LOAD_USERS` call from data provider/node details view ([c945ee5](https://gitlab.com/biomedit/portal/commit/c945ee551d6208404ce617c3af029a5071b092b1)), Closes #898
- **group managers:** No autocomplete field for group managers anymore ([9e530ba](https://gitlab.com/biomedit/portal/commit/9e530ba61f2e296dd020e914a8b3bc5e8dd567d5)), Closes #887
- **frontend:** Update SIB logo ([9d39f59](https://gitlab.com/biomedit/portal/commit/9d39f59ff06832bf45b20a706a576fa3773ae11a))
- New endpoint for managing ssh keys ([9ca48c4](https://gitlab.com/biomedit/portal/commit/9ca48c4df5368d5888964784b7c385bc1c19084a)), closes #889
- **frontend/DataTransfers:** Get rid of fingerprint field on DTR creation form ([89ea097](https://gitlab.com/biomedit/portal/commit/89ea0971ee0441da9f17cee529c4281cb501e3ac)), Closes #876
- **UsersList:** Replace `AutoSelectBase` by a `TextField` ([dec4ae7](https://gitlab.com/biomedit/portal/commit/dec4ae7bcc12dc5313a28cd7c58cf758035a7360)), Closes #883
- **backend/user:** Remove access to all users for `PM` and `PL` ([3e7f6bc](https://gitlab.com/biomedit/portal/commit/3e7f6bc3c5ada405ccb44abf91b87891523edf42)), Closes #883
- **dataTransfer:** Prevent changing data transfer purpose ([b0ee0a6](https://gitlab.com/biomedit/portal/commit/b0ee0a6ca078cb9a0e15d42b60f91abda9586162)), Close #878
- **Node:** Make OAuth2 client configurable on Node object ([f7d08be](https://gitlab.com/biomedit/portal/commit/f7d08bedc2d8a04f4973ae3bf842c3e5997a66cb)), Closes #881

### 🐞 Bug Fixes

- **sts:** Data engineers only should be able to see the credentials tab ([47348cc](https://gitlab.com/biomedit/portal/commit/47348cc2b733ecc8446c92dd78c931ad8d1626d5))
- **oauth:** `dtr` could be `None` ([2f1b946](https://gitlab.com/biomedit/portal/commit/2f1b946527d89ab728ca05b33c2abd2df10f60b4))
- **deployment:** Use Node.js `20.8.1` instead of `21.0.0` ([20b377d](https://gitlab.com/biomedit/portal/commit/20b377d99c306bba2396a8ba4182376d734e50b0))
- **DataTransferForm:** As DM I am not able to create a new DTR ([9440bc1](https://gitlab.com/biomedit/portal/commit/9440bc12d76fed5a8ffd479c1429de843f6e5de4)), Closes #915
- **group:** User might not have any `profile` yet ([5449e81](https://gitlab.com/biomedit/portal/commit/5449e81bb229ef3ef5113fb0b0e471bef6f7ea2c))
- **GroupManageForm:** Longer list of users should be wrapped ([63d4fa5](https://gitlab.com/biomedit/portal/commit/63d4fa503b12a1432700315719f66a188e15b97a))
- **oauth2clients:** Allow node viewers to read the endpoint ([c7352b0](https://gitlab.com/biomedit/portal/commit/c7352b0437c6fa690a22d44d058072acbbc550ca)), Close #886
- **DataTransferDetail:** Do not display `requestorDisplayId` if empty ([1401901](https://gitlab.com/biomedit/portal/commit/1401901f974f44219fee045245464b6e43808c06))
- **Node:** `columns.client` was renamed to `columns.oauth2Client` ([b908f78](https://gitlab.com/biomedit/portal/commit/b908f78ddd8bdbf7ff12f91dc3b88a6c12eddd72))
- **sts:** Correctly access and display the error message ([5918ad2](https://gitlab.com/biomedit/portal/commit/5918ad235d31acae0ebcf98bcc617b9c890decf7)), Closes #881
- **frontend/Projects:** Removing a user without a role causes error ([4518528](https://gitlab.com/biomedit/portal/commit/451852870ec26cae35f31f9c8435e292f884b1a5))

### 🚀 Performance

- **project:** Projects do no longer need to load all the users ([09aae6a](https://gitlab.com/biomedit/portal/commit/09aae6a9ec50015b59b4a1a2d8f2fd49dc32d87d))

### 🧱 Build system and dependencies

- **fluent-bit:** Specify up to patch version ([1fb1271](https://gitlab.com/biomedit/portal/commit/1fb12717e86b306f90e815560439fa2d6bd0b178))
- **next:** Fix lockfile missing swc dependencies ([2029bee](https://gitlab.com/biomedit/portal/commit/2029beedf7dec0905bf0d203672f65908b8f001b))
- **tools:** Add dry-run mode to bumpversion script ([d774816](https://gitlab.com/biomedit/portal/commit/d774816896326565e095ef3a7eab3d3a8159ae68)), Closes #882
- Change backend to web ([172788b](https://gitlab.com/biomedit/portal/commit/172788bfd398688a022174242ada431cd2682750))
- **tools:** Update git cliff config file ([228d559](https://gitlab.com/biomedit/portal/commit/228d5599653bbd31f6abeebc015fe5e5d48bf84d)), Closes #892

### 👷 CI

- Rename jobs to reflect what their purpose is ([b169422](https://gitlab.com/biomedit/portal/commit/b16942247b6a307bddb6e9c181f779d12a2c8f14)), Close #904
- **danger:** Avoid introducing forbidden words ([0cbe3b2](https://gitlab.com/biomedit/portal/commit/0cbe3b2fd9f58bc091f0d969a9eb6a06cc7b8abe)), Close #875
- **renovate:** Include traefik, fluent-bit, otel-collector ([aa238ca](https://gitlab.com/biomedit/portal/commit/aa238cac88d2c5b08e40bf1c04ae50771add1fbc)), Close #885

### 📝 Documentation

- **contributing:** Add revert to the list of accepted commit types ([aa40702](https://gitlab.com/biomedit/portal/commit/aa4070277fa05af66c6ab44fec0f92b1aaa605f3))
- **frontend:** Update contributors list ([85b137c](https://gitlab.com/biomedit/portal/commit/85b137ccbd9d2ba3fe1cc90eddb4766c55fcc0b3))
- **changelog:** Include all non-chore commits ([b47026e](https://gitlab.com/biomedit/portal/commit/b47026ec402d97028891bd3d52cf14d53766458b)), Close #884

### 🧹 Refactoring

- **SecureLink:** Add new widget `SecureLink` for `noopener noreferrer` links ([86e631a](https://gitlab.com/biomedit/portal/commit/86e631a27703641d5c11506b26383b73daf89632))
- **redux:** `CLEAR_PGP_KEY_INFOS` no longer needed ([63134ec](https://gitlab.com/biomedit/portal/commit/63134eca2cee8c7d952ddcb3db85b03c6065c6bd))
- Replace pylint with ruff ([02fe3e3](https://gitlab.com/biomedit/portal/commit/02fe3e3350736650c72ec38ba5b8e6581433da8c)), Close #907
- **common:** Specify a default for query param type (`string`) ([74308b7](https://gitlab.com/biomedit/portal/commit/74308b76c269d1f3911ec3814056fe9db9937b41))
- **UserTextField:** Make it usable in other contexts ([2622850](https://gitlab.com/biomedit/portal/commit/2622850f0bff881d60ba6547f543c7f7bfab7d90))
- Remove unused images ([c419b63](https://gitlab.com/biomedit/portal/commit/c419b63ae2cb29499740fad28f03491f0cd44583))
- Use svg instead of png ([1e92b2c](https://gitlab.com/biomedit/portal/commit/1e92b2cf3b020efbf7521a8d96aaaa9bed4260b3))
- **backend/user:** Remove `(ID: 63457666153)` from displayed name ([b8f41ed](https://gitlab.com/biomedit/portal/commit/b8f41ed103a0b368b4aba5536b2f0e074fa67ace))

### ✅ Test

- Remove institution names from the codebase ([91a8144](https://gitlab.com/biomedit/portal/commit/91a8144b414a25e676d687f9664faaa2bfa62314))
- **frontend:** Replace test-data with factories ([6e61f71](https://gitlab.com/biomedit/portal/commit/6e61f716d2f49cb2aab474e27a6aa8adb540eca1))

## [6.2.0](https://gitlab.com/biomedit/portal/-/releases/6.2.0) - 2023-08-02

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.1.0...6.2.0)


### ℹ️ Actions Needed

- `oidc` part of config file has been enriched with a `mapper` section.

### Bug Fixes

- **tools:** Adapt the scripts to make them working out-of-the-box ([4f2e271](https://gitlab.com/biomedit/portal/commit/4f2e27175a0161a466db6311b7c24ff3281c66c6))
- `fluent-bit` and `otel-collector` do not restart on new deployment ([b152cb2](https://gitlab.com/biomedit/portal/commit/b152cb24a04f11562129228802c81a0d09aaae3c))
- Rename data provider role ([cf5ffff](https://gitlab.com/biomedit/portal/commit/cf5ffffb3f0cf81adf49ab05c4a870610c0e1d72))
- **oauth:** `groups` claim produces an exception during STS generation ([88ac095](https://gitlab.com/biomedit/portal/commit/88ac095f3667ae4f0da5c902a6d9cb200537f0a8)), Closes #856
- **UserDetail:** Directly fetch projects of selected user from backend ([53dd5ed](https://gitlab.com/biomedit/portal/commit/53dd5ed6b3b3187acec60d7edf4372e08240ece6)), Closes #863
- **frontend/Admin:** Local accounts not correctly rendered ([98e6711](https://gitlab.com/biomedit/portal/commit/98e6711367a6e84fc2a12e4c271b6384617e04b5)), Closes #652

### Features

- **backend/pgp_key_info:** Check that key email is one of users emails ([18c4dce](https://gitlab.com/biomedit/portal/commit/18c4dcef45ec7388b77d49ea46fafcae51ed9be4))
- **frontend/PgpKeyInfoManageForm:** Allow uploading pgp keys with any of users emails ([7c9862f](https://gitlab.com/biomedit/portal/commit/7c9862fe5aa5aa97d0bedc3be40106b571955cf8)), Close #869
- **frontend/DataTransfers:** Show legal basis in production DTR details ([24bad8e](https://gitlab.com/biomedit/portal/commit/24bad8e5a1c2ebdcb50056653c840ff1c725afd9)), Closes #868
- **oidc:** Do not overspecialise claims names requested by portal ([f31690f](https://gitlab.com/biomedit/portal/commit/f31690f6a21749c9c3e52ce0785610043c00db94)), Closes #861
- **frontend/Profile:** Frontend should refer to usernames consistently ([cec2824](https://gitlab.com/biomedit/portal/commit/cec2824234c10241529780d7f8edb794f16e6851)), Closes #855

## [6.1.0](https://gitlab.com/biomedit/portal/-/releases/6.1.0) - 2023-07-05

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/6.0.0...6.1.0)


### Bug Fixes

- Error handling in callback function ([8692432](https://gitlab.com/biomedit/portal/commit/869243249ec8dd79dc85a8942123f415edae9115))
- **Userinfo:** Only display unique email addresses ([b1b3b34](https://gitlab.com/biomedit/portal/commit/b1b3b342395347a04c7e63c96e24479819027f52)), Closes #860

### Features

- **frontend/DataTransfers:** Remove disabled DPs from select box ([09d21e0](https://gitlab.com/biomedit/portal/commit/09d21e0799bd61626c60008e0a1a336a0b06c9cf)), Closes #848

## [6.0.0](https://gitlab.com/biomedit/portal/-/releases/6.0.0) - 2023-07-03

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/5.3.0...6.0.0)

### ⚠ BREAKING CHANGES

- The `projects-info` endpoint has been removed. `data-provider` is now only accessible to privileged users.

### ℹ️ Actions Needed

- Run `store_empty_emails_as_null` script.
- We now expect user emails to be found in `swissEduIDAssociatedMail` resp. `swissEduIDLinkedAffiliationMail` claim. You may need to update the **OIDC** provider's configuration.

### Bug Fixes

- Group managers should NOT be able to see ELSI groups in `Administration/Groups` ([456f8ec](https://gitlab.com/biomedit/portal/commit/456f8ec15eb3893d55052ccf33a253475b186181)), Closes #858
- **sts:** Latest `Authlib` version (1.2.1) breaks STS credentials generation ([98d3849](https://gitlab.com/biomedit/portal/commit/98d3849061e2875bfdc66531431c2b9400b1c92e)), Closes #854
- **frontend/DataTransferHooks:** Do not render DataProviderField for unauthorizes users ([62d81ab](https://gitlab.com/biomedit/portal/commit/62d81abf08adf7b274d4646963458c5432823d37))
- **backend/user:** Prevent user creation using Django admin ([2448310](https://gitlab.com/biomedit/portal/commit/2448310b4ef1c3e126e0957bd40abf243548762b)), Close #842
- **data_transfer:** Convert `_` to `-` in bucket names ([622f85c](https://gitlab.com/biomedit/portal/commit/622f85c6c03dddeaafd8a023b29581fea92652b1)), Close #835
- **user:** Output users not associated with any flag, group, data provider or node ([af855b7](https://gitlab.com/biomedit/portal/commit/af855b7b3971f8c2f83929b66ddc508a6215c6b2)), Closes #853
- **choice:** Add display name fallback for users NOT having a `profile` ([ffff47a](https://gitlab.com/biomedit/portal/commit/ffff47acbfd03e57dbc723867c0ad71786dac84b)), Closes #838
- **security:** Restrict API endpoints ([77ff308](https://gitlab.com/biomedit/portal/commit/77ff30894b1ab24a94858b9c01f0188d2a26f095)), Closes #830
- Only use JSON in current deployment ([b54a840](https://gitlab.com/biomedit/portal/commit/b54a840f07c324dc8229883c63cfa62d8a1254e0)), Closes #844
- **backend/sts:** Remove leftover condition for data manager access to minio ([5058fb0](https://gitlab.com/biomedit/portal/commit/5058fb0c3173953ba3932760acd9e95088c6237f))
- Support new psycopg 3.x library in entrypoint ([89fc5b8](https://gitlab.com/biomedit/portal/commit/89fc5b89725ca4918efdfad35aef9abc88cae493))
- Make oidc callback and user_from_claims more robust ([168d21b](https://gitlab.com/biomedit/portal/commit/168d21b0ee4184e95686c7cdfa8988255888b97e)), closes #836
- **elsi:** Authenticated can only-read legal approval groups ([efef9c7](https://gitlab.com/biomedit/portal/commit/efef9c76f8fe42bc7548cf8639b77d5236fc38f8)), Closes #837
- **oauth:** `name` claim could NOT be found by MinIO ([5d2f176](https://gitlab.com/biomedit/portal/commit/5d2f176ac83c7267191e6ee425df46e634f92874))
- **group_approvals:** Legal approvers should be able to access their group approvals ([0edd926](https://gitlab.com/biomedit/portal/commit/0edd9268519746b4dfa7e9ebe820d3fdb2f0ef7e)), Closes #832

### Features

- **frontend/Projects:** Portal should warn PMs when they add users without roles ([6df4f33](https://gitlab.com/biomedit/portal/commit/6df4f33f5249ef5f782474aabe3d516cc39422d8)), Closes #852
- **backend/identites:** Store empty emails as null instead of empty string ([754337d](https://gitlab.com/biomedit/portal/commit/754337dce7ffa7b21d508ff890b39a4bd531e709)), Close #834
- **backend/DataPackageCheck:** Check using user foreign key instead of email ([bd1d9b7](https://gitlab.com/biomedit/portal/commit/bd1d9b7357cfa49963d1e7e06bea2eb8a3bbf9f1))
- **frontend/UserManageForm:** Remove information which is now in UserDetail ([23255b2](https://gitlab.com/biomedit/portal/commit/23255b2c72e54778cf03da2fb47b82afefc6adf2))
- **UserManage:** Disable the `export` button while export is processing ([db59d58](https://gitlab.com/biomedit/portal/commit/db59d58b9be56c6017d0af07960116495da9db4a)), Closes #803
- Remove filebeat from portal deployment ([391653c](https://gitlab.com/biomedit/portal/commit/391653c94befd350e9e39449b1345a17b30dae77)), closes #849
- **backend/DtrNotification:** Use bcc instead of to ([b5915d4](https://gitlab.com/biomedit/portal/commit/b5915d43d0d3bbf85eea953f9933812741553610)), Close #822
- **frontend/Admin:** Add user details window that appears on table row click ([206b38a](https://gitlab.com/biomedit/portal/commit/206b38aec548f638b9c4e7a44032b256a7a956b0)), Closes #831
- **frontend/Profile:** Allow users to change their email ([695393a](https://gitlab.com/biomedit/portal/commit/695393a7123eb6ae72b4ed06ba33e3eb0ddeaf61)), Close #810
- **frontend/Data Transfers:** Move tooltip requestor info to details view ([5831019](https://gitlab.com/biomedit/portal/commit/58310190245b05fb628876eaf2d3a27186064e01)), Closes #820
- Drop support for Kubernetes ([aa48617](https://gitlab.com/biomedit/portal/commit/aa48617895fe75e4def6f9c5e1dffa620f472b78))
- Allow web/config.toml format as an alternative to web/config.json ([bb38b29](https://gitlab.com/biomedit/portal/commit/bb38b29c734936c45583091439eb14f48bd793bb)), closes #839
- **sts:** Enable DP data engineers to push packages on MinIO ([8313e93](https://gitlab.com/biomedit/portal/commit/8313e9366be7cf6d973a1e73927c0b21ec171391)), Closes #785
- **project:** Display legal approval group on details panel if any specified ([3490ca8](https://gitlab.com/biomedit/portal/commit/3490ca8b0524f8375728357fee53d700faee57d0))
- **group:** Add possibility to set the role for new groups ([8875206](https://gitlab.com/biomedit/portal/commit/887520611ece3e78a024bb5211684195b1780d88))
- Ask users to select their email upon first login ([f4e02b6](https://gitlab.com/biomedit/portal/commit/f4e02b6622625ac36a799d288de1602a71550ede)), Close #794
- **frontend/Profile:** Custom affiliation not displayed in Userinfo ([033ac6c](https://gitlab.com/biomedit/portal/commit/033ac6c73b8ed7ff5578afe8fffbe62a47784a4a)), Closes #813
- **frontend/Administration:** Allow node admins to see users tab ([2706dac](https://gitlab.com/biomedit/portal/commit/2706dac1364115bc667730086651338616657779)), Close #766
- **backend/Projects:** Notification when project is unarchived ([59ebaf8](https://gitlab.com/biomedit/portal/commit/59ebaf88223212eb1e0ab2088e4f0633b9b0a874)), Closes #809

### Performance

- **ExportUsersCSV:** Improve users export ([886647c](https://gitlab.com/biomedit/portal/commit/886647cedd21c7da68be0bea7e49ad1a863bad2c)), Closes #828

## [5.3.0](https://gitlab.com/biomedit/portal/-/releases/5.3.0) - 2023-04-25

[See all changes since the last release](https://gitlab.com/biomedit/portal/compare/5.2.0...5.3.0)


### ℹ️ Actions Needed

- Add `pgp.end_relax` to `.config.json`
- Add `NEXT_PUBLIC_SETT_DOCUMENTATION` to `frontend/.env`
- Reactivate the job for synchronising keys with keyserver at the next release

### Bug Fixes

- Add (temporary) support for SHA-1 keys ([ebf1aae](https://gitlab.com/biomedit/portal/commit/ebf1aae5b6668190fa302cce55403274acf39933)), Closes #821, #812
- **DataTransferForm:** `populateIfSingleChoice` NOT working for requestor ([7b04a75](https://gitlab.com/biomedit/portal/commit/7b04a75ab083f7aaf4ec734f83d59491377da71d))
- Some fields in `FormDialog` no longer fullWidth ([bb355fe](https://gitlab.com/biomedit/portal/commit/bb355fe854f57bb7c2b41ac4acee4dc4ffa9def6)), Closes #814
- **ProjectForm:** Empty ('') string NOT acceptable for expiration date ([fb3cb6d](https://gitlab.com/biomedit/portal/commit/fb3cb6d33a111dd8bea555adb828299203388803)), Closes #808
- **DataTransferForm:** Coordinators not listed when one single data provider ([2da5d8e](https://gitlab.com/biomedit/portal/commit/2da5d8e24da8373144948bc0df3443110e941a2c)), Closes #796
- On `TEST` DTR user gets asked for legal basis ([7aa661c](https://gitlab.com/biomedit/portal/commit/7aa661cb7be498086ed782b399893f364260cf8a))
- **frontend/User export:** List of users should be exportable ([5d95890](https://gitlab.com/biomedit/portal/commit/5d95890c9aa1b50c7085b6500f2c90b50f8e22ca)), Closes #789
- **backend/User export:** Export users does not work on staging ([ef0052d](https://gitlab.com/biomedit/portal/commit/ef0052da38b440e1549ec07235c01217588020fc)), Closes #793
- **sts:** Improve STS endpoint OpenAPI generation ([c09e260](https://gitlab.com/biomedit/portal/commit/c09e26058e92ba9bc6febe14ac2fef6a0cbedc69))

### Features

- **frontend/Users:** Need to see projects in user profile ([5347aec](https://gitlab.com/biomedit/portal/commit/5347aece1e6694817a9433d2383a14f39ce6f897)), Closes #788
- **frontend/app:** Turn UserMenu into an actual NavItem ([0c64b9e](https://gitlab.com/biomedit/portal/commit/0c64b9e36c4e03f3df06b8481f43b2977d48f739)), Close #817
- **update_pgp_keys:** Inform DCC and concerned users about key status change ([f35c8a6](https://gitlab.com/biomedit/portal/commit/f35c8a64da2b5a481713e95fa3f82439bd9875fd)), Closes #818
- Add custom affiliation ([3f51ee3](https://gitlab.com/biomedit/portal/commit/3f51ee3a27f5594e762fb63b5a98780c55d73f13)), Closes #787
- **backend/audit:** Allow filtering for project code ([91a63fc](https://gitlab.com/biomedit/portal/commit/91a63fcff9c0b5723b8013d9b85ba9d85dc71995)), Close #803
- **backend/BaseLookupFilter:** Allow empty value for query parameters ([fcee6da](https://gitlab.com/biomedit/portal/commit/fcee6da177fca7f2fee908be3d25cb75e861aa5a)), Close #790
- **StsCredentials:** Add the possibility to copy all the credentials to clipboard ([b4695ba](https://gitlab.com/biomedit/portal/commit/b4695ba84ba433db438c7d4f703bdb5d1e9d842a))
- **StsCredentials:** Display error message in case of problem with server ([d08d603](https://gitlab.com/biomedit/portal/commit/d08d603c5ebf5779916548aad2fcdcd929cf9180)), Closes #795
- Allow project expiration date reset to empty ([5941a9f](https://gitlab.com/biomedit/portal/commit/5941a9f3cac8fe96855f4566ef1a4b90ff41dd02)), Closes #756
- **sts:** Add a `Credentials` pane to data transfer detail view ([69b713a](https://gitlab.com/biomedit/portal/commit/69b713af7bbdd815db42d5e6af9ea0512ea01139)), Closes #786
- **frontend/Admin:** List of users should be exportable ([bd0ab31](https://gitlab.com/biomedit/portal/commit/bd0ab3144e0965902234adcf22d2e2b38a17f69b)), Closes #765
- **backend:** Instrument backend with OpenTelemetry traces ([5876243](https://gitlab.com/biomedit/portal/commit/58762437a48e9d6edbe68d1114c158c622ac5790)), Closes #770
- Add STS endpoint to generate security credentials to access MinIO ([1157747](https://gitlab.com/biomedit/portal/commit/11577477ef57948b42704b2fda8d63f0df23e7fc)), Closes #777
- Do not store the token in the database ([c215930](https://gitlab.com/biomedit/portal/commit/c21593011beaaf3d56b06115c68f2f1e5953a8d1)), Closes #778
- Make portal IdP (Identity Provider) ([4890489](https://gitlab.com/biomedit/portal/commit/48904896989c1f68f26937b808574f2c999b96eb)), Closes #759
- Api extensions for project metadata ([5226a22](https://gitlab.com/biomedit/portal/commit/5226a220bd5468aa2d48a73133dc397391bcc452)), Closes #687

## [5.2.0](https://gitlab.com/biomedit/portal/compare/5.1.0...5.2.0) (2023-02-07)


### Features

* **/backend/admin:** display `Flag`'s history ([bdbff1a](https://gitlab.com/biomedit/portal/commit/bdbff1a2d709239261abc7b4cb8bc20825874f05)), closes [#745](https://gitlab.com/biomedit/portal/issues/745)
* **/backend/admin:** display `User`'s history ([f37b947](https://gitlab.com/biomedit/portal/commit/f37b947860c9d1ac393ffd58ea188b17a1cef666))
* add checkmark when package reaches its project ([1a6dd9f](https://gitlab.com/biomedit/portal/commit/1a6dd9f0e400de67977862a802fb6b39f6087baa)), closes [#754](https://gitlab.com/biomedit/portal/issues/754)
* **backend/data_migrations:** add migration to create legal approval groups ([5d15955](https://gitlab.com/biomedit/portal/commit/5d159550ae974128f0d4d965370a044e3dbad22a)), closes [#737](https://gitlab.com/biomedit/portal/issues/737)
* **backend/dataTransfers:** create group approval using project legal approval group ([40e0620](https://gitlab.com/biomedit/portal/commit/40e0620021f28bab72f57d6f1f3a633e9fc5786d))
* **backend/models:** add legal approval group to project model ([24a0f94](https://gitlab.com/biomedit/portal/commit/24a0f9412de3da99fd62e2f3315590667e3288bf))
* **backend/node:** add object storage url field ([10b9a84](https://gitlab.com/biomedit/portal/commit/10b9a847f476f0a4ee3f6d5343ef72ed8cef3bd4))
* **backend/permissions:** allow project leaders and data managers to read legal approval groups ([6a1aedc](https://gitlab.com/biomedit/portal/commit/6a1aedcc7ca5d03b3dc76d8e477b654bb6b05e78)), closes [#761](https://gitlab.com/biomedit/portal/issues/761)
* **backend/Profile:** add authorized property to user's Profile ([e45a5c8](https://gitlab.com/biomedit/portal/commit/e45a5c8279aa82bf11610bdb2969af2312029e1a))
* **backend/views:** add subendpoint for dtr object storage metadata ([bb474fc](https://gitlab.com/biomedit/portal/commit/bb474fcf61a3f51c4165aeef264ef5cd3707e813)), closes [#746](https://gitlab.com/biomedit/portal/issues/746)
* **frontend/DataTransferHooks:** include data package filename in global filter ([68a1b84](https://gitlab.com/biomedit/portal/commit/68a1b843a29479e8361e50ff5e57730e1088ea21)), closes [#663](https://gitlab.com/biomedit/portal/issues/663)
* **frontend/NodeManageForm:** add object storage url input field ([2b0b45e](https://gitlab.com/biomedit/portal/commit/2b0b45ef4680c7de1b9b4b92c50e5ee16afccdb1)), closes [#757](https://gitlab.com/biomedit/portal/issues/757)
* **frontend/ProjectForm:** add legal approval group field on project creation ([dedf8af](https://gitlab.com/biomedit/portal/commit/dedf8af808f49d404aac061754c8cefafae7f006))
* **frontend/Projects:** need an additional field in Project settings ([da04754](https://gitlab.com/biomedit/portal/commit/da04754336cfe6cdb9b322d58d333936b1c31e74)), closes [#725](https://gitlab.com/biomedit/portal/issues/725)
* **frontend/Projects:** set an expiration date on project level ([00945c2](https://gitlab.com/biomedit/portal/commit/00945c2334f7eb34fb29c4af0cdcbb8ebaa20b99)), closes [#725](https://gitlab.com/biomedit/portal/issues/725)
* **frontend/SeasonalMessage:** add new seasonal messages ([11746da](https://gitlab.com/biomedit/portal/commit/11746dac0dbdf352b31912a52783cbf2295a602d)), closes [#709](https://gitlab.com/biomedit/portal/issues/709)
* **frontend/UserRoleEntries:** add verified icon if user is authorized ([d6ff69c](https://gitlab.com/biomedit/portal/commit/d6ff69c76b9614e3b7372889589610f91907fd3a)), closes [#743](https://gitlab.com/biomedit/portal/issues/743)


### Bug Fixes

* **backend/permissions:** allow project managers to read groups ([90ac758](https://gitlab.com/biomedit/portal/commit/90ac758321f15b5bff6947c32d8cc2bbd4060efc)), closes [#773](https://gitlab.com/biomedit/portal/issues/773)
* **backend:** modify pgpkey endpoint to allow empty requests ([a0071ae](https://gitlab.com/biomedit/portal/commit/a0071ae7a22004d657a938e3d166b63d2303adf3)), closes [#742](https://gitlab.com/biomedit/portal/issues/742)
* **data_package:** file size NOT displayed when listing the data packages ([453ea9f](https://gitlab.com/biomedit/portal/commit/453ea9fcf2d3d5f60d0defa6dc2d3d2fe6a85e2f))
* **frontend/Home:** show feed posts in chronological order ([f1f7fd0](https://gitlab.com/biomedit/portal/commit/f1f7fd07bb2bba0a0b75e32b931e9135be41f079)), closes [#727](https://gitlab.com/biomedit/portal/issues/727)
* **frontend/Home:** show feed posts in reverse chronological order ([088818b](https://gitlab.com/biomedit/portal/commit/088818b4c8cbbaaf7e4aef8e9f8a48e0877ee14f)), closes [#727](https://gitlab.com/biomedit/portal/issues/727)
* **frontend/Projects:** wrong sorting of project roles ([50435e3](https://gitlab.com/biomedit/portal/commit/50435e35644726555588935fef8c7f6ba5ba667b)), closes [#752](https://gitlab.com/biomedit/portal/issues/752)
* **project:** add `legalSupportContact` to project permissions and use it in `ProjectForm.tsx` ([9e7a2a5](https://gitlab.com/biomedit/portal/commit/9e7a2a530b8bbfaac7bbe789b40b51011bef9e53))
* **Tasks:** fix jobs for project expiration ([1085f6a](https://gitlab.com/biomedit/portal/commit/1085f6a3076d2d878bbcc6ebcd27eb6e8874253b)), closes [#762](https://gitlab.com/biomedit/portal/issues/762)
* **Timeline:** fix jobs for project expiration ([6496898](https://gitlab.com/biomedit/portal/commit/6496898684b77b4bdb03242e90baf46cd73b8d20)), closes [#762](https://gitlab.com/biomedit/portal/issues/762)

## [5.1.0](https://gitlab.com/biomedit/portal/compare/5.0.0...5.1.0) (2022-12-07)


### Features

* allow patch to update first_name, last_name and email ([0d4272f](https://gitlab.com/biomedit/portal/commit/0d4272fd418c03c412bbb025808c24ea988aabe3))
* **backend/admin:** add role attribute to group admin section ([07475ab](https://gitlab.com/biomedit/portal/commit/07475ab279f3fa51c83944b3e1d8c1ace167ccdc))
* **backend/projects:** add project code in user add/remove email ([792a5c1](https://gitlab.com/biomedit/portal/commit/792a5c1bbede2691be2c75478c0343363e478cdd)), closes [#724](https://gitlab.com/biomedit/portal/issues/724)
* **backend:** replace GnuPG with SequoiaPGP ([9440170](https://gitlab.com/biomedit/portal/commit/9440170cb155998b65b5b524a16ed3a0b75f1b23)), closes [#578](https://gitlab.com/biomedit/portal/issues/578)
* create user with provided username. Use email as fallback ([f716e59](https://gitlab.com/biomedit/portal/commit/f716e59decb52ea6c3dce5832cb24a1f0f2f4fe2))
* **dataProvider:** associate data provider groups with data providers more robustly ([607ae33](https://gitlab.com/biomedit/portal/commit/607ae3336fd3b165099d53b9efbe679360066a9b))
* **elsi:** distinguish elsi group more robustly ([3526188](https://gitlab.com/biomedit/portal/commit/3526188212b6e6f97226c5d00a763ce39f5d88ba)), closes [#650](https://gitlab.com/biomedit/portal/issues/650)
* **frontend/Form:** populate select fields when only one option is available ([b60ade5](https://gitlab.com/biomedit/portal/commit/b60ade511fbbbb8febfb9b45318d2d7ddc3c8c9d)), closes [#374](https://gitlab.com/biomedit/portal/issues/374)
* **node:** associate node groups with nodes more robustly ([711caa6](https://gitlab.com/biomedit/portal/commit/711caa6468e0019701eb1d88864948dd0371c30a))
* support `sftp` scheme in resources specification ([0f254c7](https://gitlab.com/biomedit/portal/commit/0f254c792cb7da4caebbb0d7617495e025ac586a)), closes [#723](https://gitlab.com/biomedit/portal/issues/723)
* upate auto-generated user model ([ee08c6b](https://gitlab.com/biomedit/portal/commit/ee08c6b183b3ec629c6e29f39496d054b802f140))


### Bug Fixes

* **_app.tsx:** `ToastBar` does NO longer pop up and `SkipLink` appears too early ([df7eaff](https://gitlab.com/biomedit/portal/commit/df7eaffbc8146261ceafd2557663a66294c8a450)), closes [#731](https://gitlab.com/biomedit/portal/issues/731) [#729](https://gitlab.com/biomedit/portal/issues/729)
* **backend/permission:** give node/dp personnel read permissions on groups ([3a617e9](https://gitlab.com/biomedit/portal/commit/3a617e9e9360f46b50189b11008d557f2bd3b3cc)), closes [#738](https://gitlab.com/biomedit/portal/issues/738)
* **backend/permissions:** show all users to node and dp personnel ([926133a](https://gitlab.com/biomedit/portal/commit/926133aa3d1ef5d051b13b368883ba834431e229))
* **backend/permissions:** show dp users to node personnel ([e87a845](https://gitlab.com/biomedit/portal/commit/e87a845246356e13809a2a79baa0359f9eeb4f38)), closes [#739](https://gitlab.com/biomedit/portal/issues/739)
* create a user profile in those rare cases it does not exist yet ([4794c67](https://gitlab.com/biomedit/portal/commit/4794c672594ef3558a812a585f7a053ecff17c60))
* **DataTransferTable.test.tsx:** add missing mocked REST call ([1a7b2cc](https://gitlab.com/biomedit/portal/commit/1a7b2cc3720315227e7aaf86a024053f9a0d8d3b)), closes [#719](https://gitlab.com/biomedit/portal/issues/719)
* **frontend/DataTransferApprovals:** keep height constant for reject and approve buttons ([5ad043d](https://gitlab.com/biomedit/portal/commit/5ad043d5cb5af93f4ddab55a73a30a244749b24b)), closes [#708](https://gitlab.com/biomedit/portal/issues/708)
* **frontend/DataTransferForm:** autoselect field prevents DTR creation in some cases ([d16cbac](https://gitlab.com/biomedit/portal/commit/d16cbac93f980b42319abaa5b99679cb9e8ef4f5)), closes [#714](https://gitlab.com/biomedit/portal/issues/714)
* increment gid separately from uid ([4809686](https://gitlab.com/biomedit/portal/commit/4809686387ff58004235dbf49fbf310612649c7a)), closes [#734](https://gitlab.com/biomedit/portal/issues/734)
* messages NOT displayed when invoking '/messages' ([da34b41](https://gitlab.com/biomedit/portal/commit/da34b41deeb67eb480ed80d6e4cb2ede41a5660e)), closes [#730](https://gitlab.com/biomedit/portal/issues/730)
* **SkipLink:** add `SkipLink` when component is mounted ([aa1c0a7](https://gitlab.com/biomedit/portal/commit/aa1c0a7f786481b3a4ce5261a60ad2fc8d685263)), closes [#731](https://gitlab.com/biomedit/portal/issues/731)
* ugly underlined menu items ([f5356e3](https://gitlab.com/biomedit/portal/commit/f5356e31741a50b9e25f6cbc88097ec7456f0f8d)), closes [#720](https://gitlab.com/biomedit/portal/issues/720)
* **UserMenu:** user menu items NOT translated ([8f995dd](https://gitlab.com/biomedit/portal/commit/8f995dd699c26b750dc3cc3f79e4e9d92f213ef3))

## [5.0.0](https://gitlab.com/biomedit/portal/compare/4.7.0...5.0.0) (2022-11-03)


### ⚠ BREAKING CHANGES

* **config:** `user_last_logged_in_days` setting is no longer allowed.

The value previously defined in `user_last_logged_in_days` can now be
changed (from the default 60 days) using Django Admin's Periodic Tasks,
e.g. set `{"days": 180}` in the "Keyword Arguments" field.
* **pgp:** `pgpkey/info` endpoint is now available at `pgpkey`
* **pgp:** Deprecated PGP management has been removed.

`pgpkey` and `pgpkey/sign-request` endpoints and the corresponding
frontend page have been removed. The new PGP approval system should be
used instead.

### Features

* add approval of ELSI group to DTR approvals list ([0b9e662](https://gitlab.com/biomedit/portal/commit/0b9e662e55dffc86b78b4416bb3e9b93d1a62170)), closes [#642](https://gitlab.com/biomedit/portal/issues/642)
* add skip link to improve accessibility ([f7f2623](https://gitlab.com/biomedit/portal/commit/f7f2623d71ee0837aff7bc71d655415f51025ed1)), closes [#695](https://gitlab.com/biomedit/portal/issues/695)
* **data_package:** for each data package record its size ([23f45b8](https://gitlab.com/biomedit/portal/commit/23f45b866a6e01926d680d3e16f415d8fe0244cd)), closes [#668](https://gitlab.com/biomedit/portal/issues/668)
* **frontend:** define and use BioMedIT theme ([b395318](https://gitlab.com/biomedit/portal/commit/b3953185101e3beae505fd166d2ce1186f1bad6a))
* **frontend:** update logos and add favicon ([4bbca3d](https://gitlab.com/biomedit/portal/commit/4bbca3dec7a07eb9e7eec5f157f7220e5613f4eb)), closes [#611](https://gitlab.com/biomedit/portal/issues/611) [#653](https://gitlab.com/biomedit/portal/issues/653)
* **frontend:** update next-widgets dependency and handle breaking change ([03ca140](https://gitlab.com/biomedit/portal/commit/03ca140973e6ac0a1c31fbe23054f3d580fe96c0))
* make tab focus visible on all elements ([f9e4171](https://gitlab.com/biomedit/portal/commit/f9e41711c679897bce21e21c3977cafa8847aac5)), closes [#696](https://gitlab.com/biomedit/portal/issues/696)
* **pgp:** remove deprecated PGP endpoints and frontend key management ([817f0cf](https://gitlab.com/biomedit/portal/commit/817f0cf53a9c5b8b31745f42072f41af46895c32)), closes [#683](https://gitlab.com/biomedit/portal/issues/683)
* use semantic tags on pages ([41352ef](https://gitlab.com/biomedit/portal/commit/41352ef77137dd891a601c8636414f3a90346e6c)), closes [#702](https://gitlab.com/biomedit/portal/issues/702)


### Bug Fixes

* **config:** remove `user_last_logged_in_days` ([7eebd94](https://gitlab.com/biomedit/portal/commit/7eebd94148cb76b782913ac8d6353cc47ac01d3b))
* **dtr_notification:** reduce the number of notifications for ELSI group ([fec82e4](https://gitlab.com/biomedit/portal/commit/fec82e45c6b517db5c990298a9ad2e95807c9442)), closes [#711](https://gitlab.com/biomedit/portal/issues/711)
* **dtr_notification:** users of group ELSI should get notified ([307a597](https://gitlab.com/biomedit/portal/commit/307a5977a79f9034aea68b9224b1325f8f34fd52)), closes [#711](https://gitlab.com/biomedit/portal/issues/711)
* **frontend/Projects:** push newly added project name to the beginning of the list ([947158c](https://gitlab.com/biomedit/portal/commit/947158c44136fcb8be4bac5c9e72c64436676ca2)), closes [#680](https://gitlab.com/biomedit/portal/issues/680)
* **pgp:** rename `pgpkey/info` endpoint to `pgpkey` ([84456ee](https://gitlab.com/biomedit/portal/commit/84456eead0d026847a9591ed487db43e344b2133))

## [4.7.0](https://gitlab.com/biomedit/portal/compare/4.6.0...4.7.0) (2022-10-12)


### Features

* **backend/DtrNotification:** add ticket mail to dtr creation email body ([3fd7c10](https://gitlab.com/biomedit/portal/commit/3fd7c1019efcea89ac44aa315e7d2de8f0eb0611))
* **backend/DtrNotification:** exclude ticket mail from single approvals emails ([dba648e](https://gitlab.com/biomedit/portal/commit/dba648e742d0fa994de2fe386e3177342e8f5a1e))
* **backend/logging:** log client-side exceptions at the WARNING level ([bf2a1a2](https://gitlab.com/biomedit/portal/commit/bf2a1a2da604835e0878288c036a19cb384012af)), closes [#678](https://gitlab.com/biomedit/portal/issues/678)
* **backend/tasks:** periodically check for revoked keys on the keyserver ([864818d](https://gitlab.com/biomedit/portal/commit/864818d6144c75f09b06d321dcaae672447cabfa)), closes [#672](https://gitlab.com/biomedit/portal/issues/672) [#685](https://gitlab.com/biomedit/portal/issues/685)
* **DataProvider:** add deeplink for DP ([c07aad1](https://gitlab.com/biomedit/portal/commit/c07aad12b6096288c7dcc9fb5f16c3312edf894f)), closes [#689](https://gitlab.com/biomedit/portal/issues/689)
* **ProjectList:** support access for specific projects ([7653af7](https://gitlab.com/biomedit/portal/commit/7653af732849ae3473616ffa67e0063f841bfd35)), closes [#678](https://gitlab.com/biomedit/portal/issues/678)
* **Tab:** add support for pre-selected tab ([4e10e61](https://gitlab.com/biomedit/portal/commit/4e10e619a3906cc890d2cfb36deaba7aaa67d926)), closes [#689](https://gitlab.com/biomedit/portal/issues/689)


### Bug Fixes

* approvals layout when none of them is approved/rejected ([0866188](https://gitlab.com/biomedit/portal/commit/086618897a095df65ed66586e6819db5d0c28502)), closes [#681](https://gitlab.com/biomedit/portal/issues/681)
* **backend/logout:** remove OIDC RP-Initiated Logout incompatible implementation ([a732efa](https://gitlab.com/biomedit/portal/commit/a732efa10a5950a898c170a3a112a2c5b5791fdc)), closes [#688](https://gitlab.com/biomedit/portal/issues/688)
* **DataTransferDetails:** deletion of DTR throws an exception in frontend ([6c4be80](https://gitlab.com/biomedit/portal/commit/6c4be80e24c525234266ce41af1c8ea32592435e)), closes [#693](https://gitlab.com/biomedit/portal/issues/693)
* **DataTransferPathDetail:** deletion of DTR throws an exception ([2bcdd2b](https://gitlab.com/biomedit/portal/commit/2bcdd2bf70d690048778ba0417fc23b8d8ccd176)), closes [#682](https://gitlab.com/biomedit/portal/issues/682)
* **next-redux-wrapper:** warning on legacy implementation ([531f31e](https://gitlab.com/biomedit/portal/commit/531f31e1aae15c48fd8b6a5466576796907a22da))

## [4.6.0](https://gitlab.com/biomedit/portal/compare/4.5.0...4.6.0) (2022-09-20)


### Features

* add public endpoint returning the status of PGP keys ([10b8383](https://gitlab.com/biomedit/portal/commit/10b8383e99e95a4d7cbcf7b81f7db2137c131245)), closes [#645](https://gitlab.com/biomedit/portal/issues/645)
* add support for multiple keys to pgpkey status endpoint ([3d3c267](https://gitlab.com/biomedit/portal/commit/3d3c26735b3cc094aa5f1f4f571575d50c249c0b)), closes [#646](https://gitlab.com/biomedit/portal/issues/646)
* **approval:** add approval log in final approval notification email ([5bd8051](https://gitlab.com/biomedit/portal/commit/5bd8051db5e11ef9f0924690d45955a8ee34c9dc)), closes [#648](https://gitlab.com/biomedit/portal/issues/648)
* **backend/dataPackage:** use pgp_key_info as source of truth instead of signature and keyserver ([193fc71](https://gitlab.com/biomedit/portal/commit/193fc71d7e7af4ad19eefa1983f3d3978aeab2c5)), closes [#662](https://gitlab.com/biomedit/portal/issues/662)
* **backend/PgpKeyInfo:** add lookup filter to endpoint ([256ff24](https://gitlab.com/biomedit/portal/commit/256ff246affc78e63bde4a9581579715f5b9c414)), closes [#643](https://gitlab.com/biomedit/portal/issues/643)
* **backend/pgpKeyInfo:** send automated email when an approval is approved or rejected ([c9b07e6](https://gitlab.com/biomedit/portal/commit/c9b07e625b562cf6b98e8d8189f9a63f19ab4fae)), closes [#637](https://gitlab.com/biomedit/portal/issues/637)
* **backend:** better error handling for short or non-rsa gpg key signing requests ([31ab59b](https://gitlab.com/biomedit/portal/commit/31ab59b44c81ed02e704811aa1c0f0e574c01fe0)), closes [#603](https://gitlab.com/biomedit/portal/issues/603)
* **frontend/Admin:** add keys tab ([db4a4a1](https://gitlab.com/biomedit/portal/commit/db4a4a1bc94604b4dc1a7f188f861631c60476c2)), closes [#605](https://gitlab.com/biomedit/portal/issues/605)
* **frontend/admin:** make codes readonly for nodes, projects, and data providers ([e13ce23](https://gitlab.com/biomedit/portal/commit/e13ce235bf083054d643d052229fb571cfcc92cb)), closes [#632](https://gitlab.com/biomedit/portal/issues/632)
* **frontend/Approvals:** when rejecting an approval, a reason should be given ([35b9bd1](https://gitlab.com/biomedit/portal/commit/35b9bd117b8435680761289db92fed8bc98335c0)), closes [#550](https://gitlab.com/biomedit/portal/issues/550)
* **frontend/DataTransferForm:** use db as source instead of keyserver ([e7ddce9](https://gitlab.com/biomedit/portal/commit/e7ddce9d8529ca4a90f176f7f56973f6744cdb54)), closes [#671](https://gitlab.com/biomedit/portal/issues/671)
* **notification:** show user affiliations in project notifications ([44a0b75](https://gitlab.com/biomedit/portal/commit/44a0b75579bac314821620d693d3b13f2a0b2bdf)), closes [#666](https://gitlab.com/biomedit/portal/issues/666)
* **tasks:** make `user_last_logged_in_notification` configurable via DB ([fac6423](https://gitlab.com/biomedit/portal/commit/fac64233138295b8219d04ce395af54c01ad456d)), closes [#655](https://gitlab.com/biomedit/portal/issues/655)
* turn errors related to non-`AUTHORIZED` DTR into INFO ([6df67c3](https://gitlab.com/biomedit/portal/commit/6df67c3d9b3b1e96283279ada1498636fac413fd)), closes [#661](https://gitlab.com/biomedit/portal/issues/661)
* **UserInfo:** sort groups alphabetically in user form dropdown menu ([016a8f6](https://gitlab.com/biomedit/portal/commit/016a8f6c10e8d915f6ec7038c7acf56fe755388b)), closes [#667](https://gitlab.com/biomedit/portal/issues/667)


### Bug Fixes

* **backend/DataPackageSerializer:** cover missing pgp_key_info scenario in get_sender_pgp_key_info ([4dc8b42](https://gitlab.com/biomedit/portal/commit/4dc8b424fbaa8c062bfba89839cbb612b8044631)), closes [#665](https://gitlab.com/biomedit/portal/issues/665)
* **backend/notifications:** fix typo in dtr confirmation email body ([e958cde](https://gitlab.com/biomedit/portal/commit/e958cde64cf269f6e595851b795bd55ce621a9ed)), closes [#630](https://gitlab.com/biomedit/portal/issues/630)
* **backend:** correct error handling in key metadata download ([734d6b5](https://gitlab.com/biomedit/portal/commit/734d6b55502046aedf6fde365bf8296f8bd6c63e)), closes [#631](https://gitlab.com/biomedit/portal/issues/631)
* **backend:** do not throw error 500 when sending garbage to projects-users backend ([0960401](https://gitlab.com/biomedit/portal/commit/0960401d5199947885d018d31d342fb7f92bc641)), closes [#640](https://gitlab.com/biomedit/portal/issues/640)
* **cimmitlint:** `fixup` should be accepted in local environment ([03af561](https://gitlab.com/biomedit/portal/commit/03af5611586ad992e07baed99232676851b8a218)), closes [#608](https://gitlab.com/biomedit/portal/issues/608)
* **frontend/administration:** show only relevant DPs to DP users ([b75eede](https://gitlab.com/biomedit/portal/commit/b75eede7109e606bf9ee7e54f66cddb8bc244747)), closes [#634](https://gitlab.com/biomedit/portal/issues/634)
* **frontend/app:** restore old color scheme ([e43f445](https://gitlab.com/biomedit/portal/commit/e43f445aaf41a9339338082b01037857330f9a13)), closes [#664](https://gitlab.com/biomedit/portal/issues/664)
* **frontend/DataTransferApprovalForm:** change existingLegalBasis checkbox text ([c4cc163](https://gitlab.com/biomedit/portal/commit/c4cc16365bd00e037aa599b61c7f5b2fc3fd05ba)), closes [#629](https://gitlab.com/biomedit/portal/issues/629)
* revert Next.js to v12.2.5 ([cec5a7e](https://gitlab.com/biomedit/portal/commit/cec5a7e3921f13661269a4d07d0fdb17630da6f4)), closes [#677](https://gitlab.com/biomedit/portal/issues/677)

## [4.5.0](https://gitlab.com/biomedit/portal/compare/4.4.0...4.5.0) (2022-06-30)

### Features

* **app:** add biomedit logo to header ([8f90540](https://gitlab.com/biomedit/portal/commit/8f905409bb63b7e5dd364bf47bba5a92af5d897c)), closes [#610](https://gitlab.com/biomedit/portal/issues/610)
* **backend/dataProvider:** give permissions over new dp groups to dp managers ([52d2a6d](https://gitlab.com/biomedit/portal/commit/52d2a6d0d85a2585cfe12c12e3bfaa34c5d66c4f)), closes [#595](https://gitlab.com/biomedit/portal/issues/595)
* **CI/danger:** notify technical coordinator about UI/UX changes ([98cecb5](https://gitlab.com/biomedit/portal/commit/98cecb536b237acf46d876925ca647f65901ac9a)), closes [#619](https://gitlab.com/biomedit/portal/issues/619)
* **ci:** fixup! should trigger a commit-lint error ([c0eb1bf](https://gitlab.com/biomedit/portal/commit/c0eb1bf4d5b423a50fb2272d921bf35c91eb81d4)), closes [#608](https://gitlab.com/biomedit/portal/issues/608)
* **DataProvider:** show users and their roles on Data Providers ([5cc6624](https://gitlab.com/biomedit/portal/commit/5cc66245225d8aafb5bbd525f0f70828b5198c5d)), closes [#583](https://gitlab.com/biomedit/portal/issues/583)
* **feed:** add a possibility to set a time limit on Feeds ([d0a1010](https://gitlab.com/biomedit/portal/commit/d0a10101c002275a11151e7f5dc50dacd8388155)), closes [#320](https://gitlab.com/biomedit/portal/issues/320)
* **frontend/admin:** show data providers tab to node viewers and admins ([c6b66af](https://gitlab.com/biomedit/portal/commit/c6b66afc33e8b85ce0d099c3b4376b0806211e13)), closes [#580](https://gitlab.com/biomedit/portal/issues/580)
* **frontend/DataTransferDetail:** avoid dialog changing its height between different tabs ([493dce9](https://gitlab.com/biomedit/portal/commit/493dce95ec2ba4b6c46600bfc203dfa3461fface)), closes [#618](https://gitlab.com/biomedit/portal/issues/618)
* **frontend/Keys:** add '+ KEY' button ([4aff66f](https://gitlab.com/biomedit/portal/commit/4aff66f2299defbe76e43a3322b15ae967fa2747))
* **frontend/Keys:** switch to new Keys page depending on env variable ([7134684](https://gitlab.com/biomedit/portal/commit/7134684a81d294feb363400fcfd6b13e0688c006))
* **frontend/PgpKeyInfoList:** add new PgpKeyInfoList component ([2c55e2f](https://gitlab.com/biomedit/portal/commit/2c55e2fc0dc22ffa09257c9b80685bec3be879f2))
* **frontend/PgpKeyInfoList:** add retireKeyButton to Keys page ([6d9b3fc](https://gitlab.com/biomedit/portal/commit/6d9b3fc02234bd1e487eaea49709f325458ae7a8)), closes [#464](https://gitlab.com/biomedit/portal/issues/464)
* **frontend/reducers:** add reducer for PgpKeyInfo model ([f6c8d8c](https://gitlab.com/biomedit/portal/commit/f6c8d8c33421356617822d89227e2dfc0c124e88))
* **Node:** show users and their roles on Nodes ([4df4641](https://gitlab.com/biomedit/portal/commit/4df464186f007fd89d2d4bd502a20742989f4e72)), closes [#432](https://gitlab.com/biomedit/portal/issues/432)
* **pgpKeyInfo/notifications:** send automatic email upon new approval request ([cc820ae](https://gitlab.com/biomedit/portal/commit/cc820aec0dfecc5487687847b3955097f8620e2d))

### Bug Fixes

* **backend/notification:** do not send DTR final confirmation email to node managers ([2db63b9](https://gitlab.com/biomedit/portal/commit/2db63b97267d81627f6746d6b1343c1002aabac4)), closes [#598](https://gitlab.com/biomedit/portal/issues/598)
* **backend:** superusers are automatically DP coordinators ([a98c66c](https://gitlab.com/biomedit/portal/commit/a98c66c9797900ee204a67ff8991e03d4b6a14f4)), closes [#621](https://gitlab.com/biomedit/portal/issues/621)
* correct error handling in key metadata download ([f93016a](https://gitlab.com/biomedit/portal/commit/f93016aa278ab144054e56d401f1728190ac62b8)), closes [#623](https://gitlab.com/biomedit/portal/issues/623)
* **data_package:** improve error message in case of non-existing node ([e223741](https://gitlab.com/biomedit/portal/commit/e223741bbd8c41c7ab19a3a6c02405882e95a728)), closes [#628](https://gitlab.com/biomedit/portal/issues/628)
* **frontend/DataTransferForm:** show form only after keys have been loaded ([40ea9f9](https://gitlab.com/biomedit/portal/commit/40ea9f90780ac8345cd10cda7016ea47438bbdf7)), closes [#589](https://gitlab.com/biomedit/portal/issues/589)
* **frontend/IpRangeList:** fix delete button color ([3f66602](https://gitlab.com/biomedit/portal/commit/3f666029faf478aed9a5d7cb770795fa02a203ab)), closes [#591](https://gitlab.com/biomedit/portal/issues/591) [#557](https://gitlab.com/biomedit/portal/issues/557)
* **frontend/Userinfo:** duplicate entries for groups in userinfo after specifying the username ([69d3214](https://gitlab.com/biomedit/portal/commit/69d32142051d0a76eeacee707574fd00f3574e38)), closes [#563](https://gitlab.com/biomedit/portal/issues/563)
* persistent error field on submit ([c4b1c2b](https://gitlab.com/biomedit/portal/commit/c4b1c2ba6da3448da6d830569e09f7e568031216)), closes [#602](https://gitlab.com/biomedit/portal/issues/602)

## [4.4.0](https://gitlab.com/biomedit/portal/compare/4.4.0-dev.0...4.4.0) (2022-05-03)

### Features

* **frontend/Approval:** setting a DTR to `UNAUTHORIZED` blocks all waiting approvals ([5eaca63](https://gitlab.com/biomedit/portal/commit/5eaca639cd05b602e9cf1c725b5d9146481ae78b))

### Bug Fixes

* **DTR/transferPath:** compute transfer path on backend side ([e73f515](https://gitlab.com/biomedit/portal/commit/e73f5154d5ea0c3e332fcbca1ad3aaf8c527bac4)), closes [#597](https://gitlab.com/biomedit/portal/issues/597)
* **frontend/reducers:** update groups permissionsObject when a group is removed ([5da5441](https://gitlab.com/biomedit/portal/commit/5da544177a55b13a91d8a7e5786000528b61eab9)), closes [#590](https://gitlab.com/biomedit/portal/issues/590)
* **identities/groups:** remove group permissions when an object is removed ([81dc3b4](https://gitlab.com/biomedit/portal/commit/81dc3b4b5b14b4023ffacd11a5b59a4a22157c04))
* **backend/signals/logout:** handle anonymous users at the logout endpoint ([f9cb939](https://gitlab.com/biomedit/portal/commit/f9cb939a55ed4722a4597e888567abb42b7e491b)), closes [#594](https://gitlab.com/biomedit/portal/issues/594)

## [4.3.0](https://gitlab.com/biomedit/portal/compare/4.3.0-dev.22...4.3.0) (2022-04-28)

### Features

* **ProjectList:** add label to archived projects ([4047d74](https://gitlab.com/biomedit/portal/commit/4047d74903ae79a784fd9b3b524af30d81ddf4ff)), closes [#562](https://gitlab.com/biomedit/portal/issues/562)
* **ProjectList:** ask for a safer confirmation text when deleting ([6f04593](https://gitlab.com/biomedit/portal/commit/6f045937046299e47bdb628e430614b43280eea8)), closes [#568](https://gitlab.com/biomedit/portal/issues/568)
* **Config:** add possibility to specify max length for local_username ([4fa0e2e](https://gitlab.com/biomedit/portal/commit/4fa0e2eaea2f0bc884529c1f6c5a3699e99675c6)), closes [#565](https://gitlab.com/biomedit/portal/issues/565)
* **backend/dtr-notifications:** add final approval email notification ([f107a99](https://gitlab.com/biomedit/portal/commit/f107a994c5b88b8ecd4671ce5103a194086694e6)), closes [#569](https://gitlab.com/biomedit/portal/issues/569)
* **backend/Group:** add description field to group ([34c0d82](https://gitlab.com/biomedit/portal/commit/34c0d82e1c9de3fbb762df78a4813432856914c2)), closes [#559](https://gitlab.com/biomedit/portal/issues/559)
* **frontend/Group:** group description editable on frontend side ([11766e6](https://gitlab.com/biomedit/portal/commit/11766e64a5a080e15929512962981b5f19adc851)), closes [#559](https://gitlab.com/biomedit/portal/issues/559)
* **Group:** extend groups generation automation ([9461a14](https://gitlab.com/biomedit/portal/commit/9461a147019f5354b7894d3a542629a68f11bbd8)), closes [#554](https://gitlab.com/biomedit/portal/issues/554)
* **DataTransferApprovals:** add confirmation dialog when someone wants to reject an approval ([ba3b220](https://gitlab.com/biomedit/portal/commit/ba3b220e1f608db1e2aba2a4f9a463d1a9d7dde8)), closes [#555](https://gitlab.com/biomedit/portal/issues/555)
* **backend/dtr:** enable DTR creation for project DM ([d642201](https://gitlab.com/biomedit/portal/commit/d642201bd7d51055126ddacfc33f0ca8a09eb0f9))
* **frontend/dtr:** enable DTR creation for project DM ([c517c1c](https://gitlab.com/biomedit/portal/commit/c517c1c261f2ce645ff306fb45d67ca082d0568d)), closes [#551](https://gitlab.com/biomedit/portal/issues/551)
* add pgp metadata field to model ([e3d3c95](https://gitlab.com/biomedit/portal/commit/e3d3c95cc915d2d0fa000ff0c07cdbaca2a94ffd)), closes [#463](https://gitlab.com/biomedit/portal/issues/463)
* **backend/identities:** log login/logout events ([2fe67b3](https://gitlab.com/biomedit/portal/commit/2fe67b3107e99eb534310ff7148fc989711ecd38)), closes [#552](https://gitlab.com/biomedit/portal/issues/552)
* creation and update fields for approvals and DTR ([0e02a7f](https://gitlab.com/biomedit/portal/commit/0e02a7f4f8dac64f86bc79a52eaca608b7b606e3)), closes [#553](https://gitlab.com/biomedit/portal/issues/553)
* **backend/periodicTask:** add notification for periodic review of user roles ([1751371](https://gitlab.com/biomedit/portal/commit/17513718293d7b13941c6819a7a435410d8acc39)), closes [#549](https://gitlab.com/biomedit/portal/issues/549)
* **Config:** add possibility to specify min length for `local_username` ([a60e755](https://gitlab.com/biomedit/portal/commit/a60e75538bb9900370f11d543cc6bf94f0e59ee9)), closes [#545](https://gitlab.com/biomedit/portal/issues/545)
* **UserProfileDialog:** handle invalid username in frontend ([d0c14fe](https://gitlab.com/biomedit/portal/commit/d0c14fe072d46e859ef0fa0021c8773e1fc2de1a)), closes [#545](https://gitlab.com/biomedit/portal/issues/545)
* **Project:** display destination node ([a44dea6](https://gitlab.com/biomedit/portal/commit/a44dea6127a078a8be4cf1597e158cb9f920c4d4)), closes [#322](https://gitlab.com/biomedit/portal/issues/322)
* **ProjectUsers:** list project users ([b947568](https://gitlab.com/biomedit/portal/commit/b947568888febe81df483dcf0e579eb32bd0ebec)), closes [#322](https://gitlab.com/biomedit/portal/issues/322)
* **frontend/DataTransfer:** add data transfer path to data transfer details ([6eb1246](https://gitlab.com/biomedit/portal/commit/6eb1246d0268cf6684991cc8186dd1213cd9d890)), closes [#547](https://gitlab.com/biomedit/portal/issues/547)
* **frontend/redux:** add retrieve project action ([64c1b55](https://gitlab.com/biomedit/portal/commit/64c1b554161cc789bd1f0094b5dacd7114d513f0))

### Bug Fixes

* **config:** change session timeout to 30 min in .config.json template ([4b14773](https://gitlab.com/biomedit/portal/commit/4b147737a5b33e32e5f338b9926d5b39ca14e0fe)), closes [#544](https://gitlab.com/biomedit/portal/issues/544)
* **next-i18next:** latest version breaks `_app.test.tsx` ([e90d2d0](https://gitlab.com/biomedit/portal/commit/e90d2d0f76d15fa08ce7434e348e07b159d560a6)), closes [#576](https://gitlab.com/biomedit/portal/issues/576)
* **next:** last version v12.1.1 breaks STAGING ([97ccaec](https://gitlab.com/biomedit/portal/commit/97ccaeca6d708c9fbc56b7832e4bfdd9f7e41755))
* **frontend/DataTransfer:** update portal to reflect the fix in next-widgets issue 32 ([7b7f5a6](https://gitlab.com/biomedit/portal/commit/7b7f5a67dbf705f0b0f6120eae4643bdd3188dfa)), closes [#556](https://gitlab.com/biomedit/portal/issues/556)

## [4.2.0](https://gitlab.com/biomedit/portal/compare/4.2.0-dev.34...4.2.0) (2022-03-18)

### Features

* generate emails on approvals as well ([98fd3d7](https://gitlab.com/biomedit/portal/commit/98fd3d720c2c88d984a294b4c596e88dbfd6287d)), closes [#543](https://gitlab.com/biomedit/portal/issues/543)
* **approvals:** do not close dialog on approval/rejection ([9dc8bdc](https://gitlab.com/biomedit/portal/commit/9dc8bdcb9e55c15f7a618e9d49c5c8e582f6a83c)), closes [#546](https://gitlab.com/biomedit/portal/issues/546)
* **DataTransferApprovals:** only refresh/retrieve associated project/DTR on approval ([205f50b](https://gitlab.com/biomedit/portal/commit/205f50b52a9144ec8783a63683152a5a7906509e)), closes [#546](https://gitlab.com/biomedit/portal/issues/546)
* **frontend:** add `DataTransferApprovalForm.tsx` to approve/reject a data transfer request ([1c610d3](https://gitlab.com/biomedit/portal/commit/1c610d371c0bccc7f13feda80c57197f52d94a98)), closes [#507](https://gitlab.com/biomedit/portal/issues/507)
* **NodeApproval:** add `type` ([cf08c7c](https://gitlab.com/biomedit/portal/commit/cf08c7c99301d13dea4a2cb4f38c0724a27e0711)), closes [#507](https://gitlab.com/biomedit/portal/issues/507)
* **serializer/approval:** generate the notifications for approve and reject ([43ab657](https://gitlab.com/biomedit/portal/commit/43ab657618ae9707a59420d3b31bc77b4e83f4be)), closes [#507](https://gitlab.com/biomedit/portal/issues/507)
* **backend/mail:** add project roles to permission update email notification ([6097de4](https://gitlab.com/biomedit/portal/commit/6097de4c6bf173e842026e50762c4e33fffaf8a1)), closes [#376](https://gitlab.com/biomedit/portal/issues/376)
* **backend:** add project roles to users when fetching /projects/id/users ([f022b7f](https://gitlab.com/biomedit/portal/commit/f022b7f199f872e1abd321e74b0b8e1eae9cfaf1)), closes [#530](https://gitlab.com/biomedit/portal/issues/530)
* **backend/DataTransfer:** send notifications on data transfer creation ([dfb134f](https://gitlab.com/biomedit/portal/commit/dfb134f0bc2d8538c73b16fa0d84372089077843)), closes [#498](https://gitlab.com/biomedit/portal/issues/498)
* **frontend/DataTransfer:** add routing to data transfer details ([4c0ee88](https://gitlab.com/biomedit/portal/commit/4c0ee88476c61f3091cf22ca079b9dc428335df4))
* **commitlint:** add commitlint pre-commit hook ([8e15976](https://gitlab.com/biomedit/portal/commit/8e159762aa2ce680a0e885f7bf3973cacb255702))
* **frontend:** move position of "+" button ([6b5ff49](https://gitlab.com/biomedit/portal/commit/6b5ff49e131240ea21f95f364a5163cfb9854c98)), closes [#169](https://gitlab.com/biomedit/portal/issues/169)
* **backend/identities:** make User string representation more informative ([2ef3244](https://gitlab.com/biomedit/portal/commit/2ef32444a0d965ea10cc6f13b1a6e3d21a6ba007))
* **backend/message:** add history to the Message model ([bb3a366](https://gitlab.com/biomedit/portal/commit/bb3a366067df43fd621977fed14e5a2e995b5d76))
* **frontend/message:** change message status to `read` when messages is opened ([ca77147](https://gitlab.com/biomedit/portal/commit/ca77147d551546547fc07d3939624d4649119980)), closes [#521](https://gitlab.com/biomedit/portal/issues/521)
* **frontend/message:** display the beginning of each message in the table view ([14b4753](https://gitlab.com/biomedit/portal/commit/14b47530eeffddf469b903a955642edd2a5e38c2))
* **frontend/message:** show unread messages in bold in the table view ([d644f23](https://gitlab.com/biomedit/portal/commit/d644f231bf11786a7e4a7aa4a1b1c18988c4ff61))
* add possibility to Docker-based development ([133470f](https://gitlab.com/biomedit/portal/commit/133470f0a23a4dfe1b5268f50a073369527853be))

### Bug Fixes

* **CSV:** approvals should be correctly exported on frontend ([b5932c4](https://gitlab.com/biomedit/portal/commit/b5932c41a116a9d639a10249bd0353859e82e01b))
* legal basis not part of TEST DTRs ([60ad1d6](https://gitlab.com/biomedit/portal/commit/60ad1d632054362eb33fcc410dd9f4b021c27815))
* **next-widgets:** newest 'next-widgets' release removed ([4d91404](https://gitlab.com/biomedit/portal/commit/4d914041d575ca9ebcf3647e811166c67f17b6aa))
* **frontend/validations:** fix validations for required fields ([b8fe2da](https://gitlab.com/biomedit/portal/commit/b8fe2da79362a039bba7ebb0a3d3cb12be0d8708)), closes [#541](https://gitlab.com/biomedit/portal/issues/541)
* unnecessary error alert window while setting username ([964386c](https://gitlab.com/biomedit/portal/commit/964386cb057787c04b246d93ad05e7e390a1f53f)), closes [#518](https://gitlab.com/biomedit/portal/issues/518)
* **frontend/project:** fix validation in new project and new data provider form ([87c7fbd](https://gitlab.com/biomedit/portal/commit/87c7fbd43d87a97928b2486fe0078164b23ece58)), closes [#529](https://gitlab.com/biomedit/portal/issues/529)
* **Next.js:** randomly 504 errors on portal STAGING ([8d3c39f](https://gitlab.com/biomedit/portal/commit/8d3c39f6f0d307ea7d2b161eac9a28df51fbe258)), closes [#537](https://gitlab.com/biomedit/portal/issues/537)
* **staging:** a try to fix STAGING deployment ([731f2dc](https://gitlab.com/biomedit/portal/commit/731f2dc8e801a9faaac4dcb1a6041686d5145faf)), closes [#536](https://gitlab.com/biomedit/portal/issues/536)
* **backend/DataProvider:** show data provider coordinators on the API ([8be9cf4](https://gitlab.com/biomedit/portal/commit/8be9cf4045577d747b812b580b4981a906e3bcc9))
* **DataTransfer:** extend data transfer form with new fields ([65878a2](https://gitlab.com/biomedit/portal/commit/65878a2a733124523a135adf718723f26dc1bd91)), closes [#497](https://gitlab.com/biomedit/portal/issues/497)
* **husky:** fix husky to restore pre commit hooks ([14cfdd5](https://gitlab.com/biomedit/portal/commit/14cfdd571337a36ddcc82c88c6e7270cf58eba33))
* **UsersList:** fix TS ignored compiler error ([407592b](https://gitlab.com/biomedit/portal/commit/407592b585616b67b24049369df99797fcb1d325)), closes [#531](https://gitlab.com/biomedit/portal/issues/531)
* **frontend/DataTransfer:** Hide Data Transfers table column 'Actions' if no action possible ([ce320af](https://gitlab.com/biomedit/portal/commit/ce320afd28c2df5f171bea6da567722b8f1709f8)), closes [#517](https://gitlab.com/biomedit/portal/issues/517)
* **backend/models/DataProviderApproval:** use correct permission name ([8685462](https://gitlab.com/biomedit/portal/commit/86854625423253ea05965bdc59b0c636a78b2f24))
* **PermissionsObjectList:** do not fetch permissions for empty choice ([6480a76](https://gitlab.com/biomedit/portal/commit/6480a7637b0d31af28832895156fb8fdb24f281b)), closes [#510](https://gitlab.com/biomedit/portal/issues/510)
* **Project:** 'Invalid user field' when creating/updating a project ([57ef6cf](https://gitlab.com/biomedit/portal/commit/57ef6cfc0fb1a79a736d05c68a6820d063d0ba3a)), closes [#525](https://gitlab.com/biomedit/portal/issues/525)
* **CI:** MRs do no longer run the tests ([6f33962](https://gitlab.com/biomedit/portal/commit/6f339620ea6c10cc1c0de5c415d1bcb6d6f55524))

## [4.1.0](https://gitlab.com/biomedit/portal/compare/4.1.0-dev.0...4.1.0) (2022-01-13)

### Features

* **SWITCH:** switch off no-affiliation-consent emails by default ([abd11cc](https://gitlab.com/biomedit/portal/commit/abd11ccccf9c447fe5de1ea650c127776ecc7add)), closes [#522](https://gitlab.com/biomedit/portal/issues/522)

### Bug Fixes

* **OpenAPI:** reduce the number of warnings on OpenAPI schema generation ([efad404](https://gitlab.com/biomedit/portal/commit/efad40433fa98f30c8e5efe4ee504c63b768da25)), closes [#495](https://gitlab.com/biomedit/portal/issues/495)
* **frontend/Admin:** fix admin page without content after defining a username or giving affiliation consent ([de78897](https://gitlab.com/biomedit/portal/commit/de7889777fdbc760beb501ecbab2dd3cbe005917)), closes [#309](https://gitlab.com/biomedit/portal/issues/309)

## [4.0.0](https://gitlab.com/biomedit/portal/compare/4.0.0-dev.10...4.0.0) (2021-12-21)

### ⚠ BREAKING CHANGES

* **message:** `project-messages` endpoint has been changed to a general-purpose `messages` endpoint.
* **backend/notify:** `notify` endpoint has been removed
* **DataProvider:** Users will be removed from the DP objects.

After the introduction of DP groups, having users directly associated to DPs did not make sense anymore. DP <-> User associations should be covered by Groups, which allows a finer granular authorization pattern and control.

The migration has to be manually performed as it is difficult to foresee which role a previous DP user/manager would have in the new constellation. Following suggestions could be done though:

* If the current user is allowed to generally edit the DP entry (DP groups, DP attributes), then use the DP Manager group
* If the current user should just have read-only permissions, then use the DP Viewer group
* If the current user would be the one who is providing/sending the data to the DMs (Data Managers), the use the DP Technical Admin group
* If the current user is responsible for confirming the DTRs in portal, then use the DP Coordinator group.

### Features

* **backend/switch-notification:** do not make requests to SWITCH SCIM API if user consent is missing ([fbbf20c](https://gitlab.com/biomedit/portal/commit/fbbf20c74091fff137eaea41e55dfa4e1ba98499)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **backend/user:** allow the user to give affiliation consent ([1bd0b47](https://gitlab.com/biomedit/portal/commit/1bd0b4756ada74487b727cebacdc1428c52d5814)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **backend:** add fields `affiliation_consent` and `affiliation_consent_required` to `Profile` ([08edc6c](https://gitlab.com/biomedit/portal/commit/08edc6cf7599d700385ac6b3d72fca23e8439c54)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **frontend:** ask user for affiliation consent if not already given or not required ([61a25c2](https://gitlab.com/biomedit/portal/commit/61a25c2d63468e3325affbed103478d9d975e99f)), closes [#503](https://gitlab.com/biomedit/portal/issues/503)
* **DataProvider:** dissolve DP <-> User one-to-many connection ([51a6f5f](https://gitlab.com/biomedit/portal/commit/51a6f5f81fa195c3e7ffae44bdeca9b7a3424817)), closes [#351](https://gitlab.com/biomedit/portal/issues/351)
* **DataProvider:** use permissions to connect a DP with users ([aa5123e](https://gitlab.com/biomedit/portal/commit/aa5123ea9edbe6f512f3a61a22073483566ba050)), closes [#351](https://gitlab.com/biomedit/portal/issues/351)
* **message:** change messages to a general-purpose messaging system for users ([445d3af](https://gitlab.com/biomedit/portal/commit/445d3af6c8cbf49f2cab1be3aff65d735bce5e5b))
* **backend/notify:** remove unused endpoint ([b57ec7b](https://gitlab.com/biomedit/portal/commit/b57ec7b4c1101acc24ff5a27fa620db835440cf8))
* **backend/affiliation_change:** include flags in email notification about changed affiliation ([bb26430](https://gitlab.com/biomedit/portal/commit/bb26430bd595a54a51e276b298c8a3b04188a47b)), closes [#502](https://gitlab.com/biomedit/portal/issues/502)
* **backend/user_utils:** use a different subject for emails on affiliation changes on login compared to through the switch notification API ([ada35b8](https://gitlab.com/biomedit/portal/commit/ada35b8780a246065b21693f1612f6a2a207a35a)), closes [#502](https://gitlab.com/biomedit/portal/issues/502)
* **backend:** add switch notification API endpoint ([dd42016](https://gitlab.com/biomedit/portal/commit/dd4201655c52dbd0ddcce20005ce96f09311596b)), closes [#502](https://gitlab.com/biomedit/portal/issues/502)

### Bug Fixes

* **frontend/SetUserNameDialog): fix error `validateDOMNesting(...:** <div> cannot appear as a descendant of <p>` ([58a538a](https://gitlab.com/biomedit/portal/commit/58a538aaaf7bd56c20d61b7cd17803fe0b4d39a0)), closes [#512](https://gitlab.com/biomedit/portal/issues/512)
* **k8s:** Add missing quote char ([46b0b78](https://gitlab.com/biomedit/portal/commit/46b0b78da619112e3e4f0ac1fa3ac8d9c973e7b3))

## [3.13.0](https://gitlab.com/biomedit/portal/compare/3.13.0-dev.2...3.13.0) (2021-12-02)

### Features

* **Home/seasonal:** add new seasonal messages ([6f6d182](https://gitlab.com/biomedit/portal/commit/6f6d18203b0b090f9665d3aec468737d2e225064)), closes [#425](https://gitlab.com/biomedit/portal/issues/425)

### Bug Fixes

* **frontend/Projects:** show project filter dropdown when a user previously didn't have any projects but does after updating or adding a project ([2a7e129](https://gitlab.com/biomedit/portal/commit/2a7e12956be0c2474fe5f3f026175b73152caa53)), closes [#488](https://gitlab.com/biomedit/portal/issues/488)

## [3.12.0](https://gitlab.com/biomedit/portal/compare/3.12.0-dev.28...3.12.0) (2021-11-17)

### Features

* add `correlation_id` to requests to trace requests to the backend ([e6f2b6b](https://gitlab.com/biomedit/portal/commit/e6f2b6b109095cfa8cc5b891bd75df287b1a7445)), closes [#472](https://gitlab.com/biomedit/portal/issues/472)
* **backend/API:** add query parameters to /projetcs and /users API ([7663dbf](https://gitlab.com/biomedit/portal/commit/7663dbf5ca8574d3b757a7416c75d34949d872b1)), closes [#392](https://gitlab.com/biomedit/portal/issues/392)
* **models/PeriodicTaskRun:** implement `__str__` method ([f283cba](https://gitlab.com/biomedit/portal/commit/f283cba4ab287411636829f4f71c6c03e8d89a09))
* add service hostname to all logs ([b0bad35](https://gitlab.com/biomedit/portal/commit/b0bad357b541c61137c516372bb3e53242037c99)), closes [#461](https://gitlab.com/biomedit/portal/issues/461)
* **backend/node:** create data provider groups (technical admin, coordinator, manager, viewer) when creating a data provider ([f10cc7d](https://gitlab.com/biomedit/portal/commit/f10cc7ded9edd4d1e394e19517475068d9cc026c)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend/node:** create node admin, node viewer and node manager groups when creating a node ([892abd6](https://gitlab.com/biomedit/portal/commit/892abd63d1b591ef626cd6b084625d45c76d10e5)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend:** Use python:slim to reduce web container size ([f8cf139](https://gitlab.com/biomedit/portal/commit/f8cf139376524bdadb985bb08d8ceb2192ea6128))
* **backend:** Improve error message triggered by data_package.check_user_permissions ([4e40125](https://gitlab.com/biomedit/portal/commit/4e40125e4752565a7a3a2823928e8c9d702f191f))
* **pl:** data managers AND project leaders should be able to list their data transfers ([46e4876](https://gitlab.com/biomedit/portal/commit/46e4876f955da5dbabb333193603d93f32bc1e02)), closes [#440](https://gitlab.com/biomedit/portal/issues/440)
* **pl:** enable access for project leaders to data transfers menu item ([046d72c](https://gitlab.com/biomedit/portal/commit/046d72c31d8b276d768e5077c7eaee2b873c2acb)), closes [#440](https://gitlab.com/biomedit/portal/issues/440)

### Bug Fixes

* **frontend/Administration/Groups:** fix error when clearing object permission field ([f005e37](https://gitlab.com/biomedit/portal/commit/f005e3752e6c24815bedf8c82a4c43933feeeb6b)), closes [#490](https://gitlab.com/biomedit/portal/issues/490)
* **frontend/Administration/Groups:** fix incompatible objects not being removed when changing permission of a group ([4dd31a9](https://gitlab.com/biomedit/portal/commit/4dd31a9d961d67063f19b07176fab66bd10e0839)), closes [#490](https://gitlab.com/biomedit/portal/issues/490)
* **frontend/DataTransferDetail:** fix data transfer status coloring ([0059e9e](https://gitlab.com/biomedit/portal/commit/0059e9e1b72a0ff7c5b3a5e0da6efcfb356bca7c)), closes [#496](https://gitlab.com/biomedit/portal/issues/496)
* **frontend/ProjectList:** fix unique "key" prop React error in the console ([84e63a4](https://gitlab.com/biomedit/portal/commit/84e63a49ade5daaf58b921a2ba98fbbef838bf8f)), closes [#487](https://gitlab.com/biomedit/portal/issues/487)
* **frontend:** fix unhandled error appearing when editing a group or flag ([ef6573e](https://gitlab.com/biomedit/portal/commit/ef6573ed019a2bc73e8904cc637dd01ed5714bbb)), closes [#489](https://gitlab.com/biomedit/portal/issues/489)
* **frontend/GroupManageForm:** fix objects list being empty when adding a new group object permission ([28367b8](https://gitlab.com/biomedit/portal/commit/28367b888914c5cec90d1c279dc2ac2ee75b8d4e)), closes [#478](https://gitlab.com/biomedit/portal/issues/478)
* **frontend/GroupManageForm:** show different title when creating a group ([d5f01c5](https://gitlab.com/biomedit/portal/commit/d5f01c541efbb5f10ba5cec2f522f519a5090613))
* **frontend/DataTransferDetail:** add a missing field (Data Provider) ([6dc1bcf](https://gitlab.com/biomedit/portal/commit/6dc1bcfc6f9b228f638497b57518b66cd947eba6)), closes [#272](https://gitlab.com/biomedit/portal/issues/272)
* **ProjectList:** remove border on top of `ListPage` component ([7956655](https://gitlab.com/biomedit/portal/commit/79566559b68746c0c1fd2e21a45343a41b9d5dd5)), closes [#480](https://gitlab.com/biomedit/portal/issues/480)
* **backend:** fix broken log file rotation due to concurrency issues when using multiple gunicorn workers ([0525d73](https://gitlab.com/biomedit/portal/commit/0525d7332438caf8e77cfcb27dfc3207becfd942)), closes [#479](https://gitlab.com/biomedit/portal/issues/479)
* **frontend/UsersList:** ensure newly added users don't have any roles ([1fecf82](https://gitlab.com/biomedit/portal/commit/1fecf820c42d212ba157d8fa8949cf485bac6b13)), closes [#458](https://gitlab.com/biomedit/portal/issues/458)
* **celery:** only write logs with at least INFO level ([18df68b](https://gitlab.com/biomedit/portal/commit/18df68b6f7cec91235f5362146dbf6b74d084ea8)), closes [#469](https://gitlab.com/biomedit/portal/issues/469)
* **frontend/GroupManage:** make new groups appear when changing to the group tab after creating a dp or node ([8fbffbe](https://gitlab.com/biomedit/portal/commit/8fbffbe48a0d14576cf62ad4a39ef86238e9f761)), closes [#453](https://gitlab.com/biomedit/portal/issues/453)
* **backend:** remove deprecated fields ([7e36cf4](https://gitlab.com/biomedit/portal/commit/7e36cf44255ecf2ca84e0321b96850de502cf8e3)), closes [#373](https://gitlab.com/biomedit/portal/issues/373)

## [3.11.0](https://gitlab.com/biomedit/portal/compare/3.11.0-dev.6...3.11.0) (2021-10-05)

### Features

* **frontend/Projects:** add tooltip and separator to action buttons in project list items ([e359175](https://gitlab.com/biomedit/portal/commit/e3591752013585e81e15b568632622110668dd25)), closes [#434](https://gitlab.com/biomedit/portal/issues/434)
* k8s deployment ([8db0a7b](https://gitlab.com/biomedit/portal/commit/8db0a7bca931c9b7c2f63c99eb03a292bd6ba5d4))
* show data package sender PGP key info in the transfer log ([7198d66](https://gitlab.com/biomedit/portal/commit/7198d66931d8fa9071bd090dca58a97b4def6c21)), closes [#417](https://gitlab.com/biomedit/portal/issues/417)

### Bug Fixes

* **FeedList:** decrease font size of title and message feed items ([b550cfb](https://gitlab.com/biomedit/portal/commit/b550cfba0fd73f45f2bbdcdf21ab4289567b6110)), closes [#445](https://gitlab.com/biomedit/portal/issues/445)
* remove pylint ignores for consider-using-f-string ([fe3ab83](https://gitlab.com/biomedit/portal/commit/fe3ab83ea8f82d2bde7ae14f53e6c8e22f3c7f0e))
* Remove urls from sample portal config ([c782e7a](https://gitlab.com/biomedit/portal/commit/c782e7af186287ce7554ae4636f5365a4963e96e))
* **backend/user:** return all users when making a `GET` request to `/users` as node admin ([c84759c](https://gitlab.com/biomedit/portal/commit/c84759c0f22a93be1b5e8ef0cb2e5c50f355acf0)), closes [#454](https://gitlab.com/biomedit/portal/issues/454)

## [3.10.0](https://gitlab.com/biomedit/portal/compare/3.10.0-dev.0...3.10.0) (2021-09-15)

### Features

* **backend/data_transfer:** prevent creating or editing data transfers associated to archived projects ([87cae2e](https://gitlab.com/biomedit/portal/commit/87cae2ef6a83daf8edd4f8f35f8f8f9614ce7a5d)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** prevent editing of archived projects ([453beae](https://gitlab.com/biomedit/portal/commit/453beae8866517727182b9eabfc422125b0f9932)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** send notification to node and project users when a project is archived ([b2104dc](https://gitlab.com/biomedit/portal/commit/b2104dc7270fad84d8c80f8611337fd2588d0452)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **backend/project:** set all data transfers related to a project to status `EXPIRED` when the project is archived ([4376d85](https://gitlab.com/biomedit/portal/commit/4376d85e2739208760edbc6402c430b43ec09ea9)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/DataTransferOverviewTable:** prevent editing DTRs of archived projects ([d260ba4](https://gitlab.com/biomedit/portal/commit/d260ba417168eac61ca1c8dbf1b1825e948e2d16)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** add filter for archived projects and hide archived projects from "All Projects" and "My Projects" ([3b65842](https://gitlab.com/biomedit/portal/commit/3b658429417ad92707f8c41be72f61da2a13a5b2)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** make archived projects read only except for deletion of data transfers and projects for staff ([910fac0](https://gitlab.com/biomedit/portal/commit/910fac04fd12cb834e49a4f9444bf3f7f0561615)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend/ProjectList:** show confirmation dialog upon archival of a project ([438c8cb](https://gitlab.com/biomedit/portal/commit/438c8cbd5323e9cd51b9cb30a8d543b966981524)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **frontend:** require typing the item name in the deletion dialog ([9a0f9e0](https://gitlab.com/biomedit/portal/commit/9a0f9e0db023e977f9d9cb27ce3a4f3ff8bfaa7e)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)
* **projects:** allow PA's and NA's to archive and unarchive projects ([59ad86a](https://gitlab.com/biomedit/portal/commit/59ad86a9c6e227236515c82ef14575a74ac1def4)), closes [#426](https://gitlab.com/biomedit/portal/issues/426)

### Bug Fixes

* **frontend/Home:** replace deprecated justify prop with its newer equivalent ([a3d6315](https://gitlab.com/biomedit/portal/commit/a3d6315fea3fffa9ed482d2d08898bc6e5b81289))
* **frontend/Home:** use GridItem special key prop properly. ([3d6fc59](https://gitlab.com/biomedit/portal/commit/3d6fc592c321e7e93f9d053f29d35b4a64537c6c))
* **frontend/Administration:** sort columns of admin table correctly ([2bac0cb](https://gitlab.com/biomedit/portal/commit/2bac0cb4078e0aad931bfadf88a2334b3024b488))
* **frontend/DataTransfer:** sort requestor column of data-transfers correctly ([a9f0946](https://gitlab.com/biomedit/portal/commit/a9f094685a65dd0c4a0598da9210d5c3a47c196a))

## [3.9.0](https://gitlab.com/biomedit/portal/compare/3.9.0-dev.1...3.9.0) (2021-08-20)

### Features

* add new Data Provider (object-based) permissions ([6550b1b](https://gitlab.com/biomedit/portal/commit/6550b1b68e52466eaec56b28341367f350087246)), closes [#368](https://gitlab.com/biomedit/portal/issues/368)

### Bug Fixes

* **filebeat:** decode the message object in logs ([6f84fff](https://gitlab.com/biomedit/portal/commit/6f84fff38e00198a59489007bc560edd64196dfc)), closes [#442](https://gitlab.com/biomedit/portal/issues/442)

## [3.8.0](https://gitlab.com/biomedit/portal/compare/3.8.0-dev.16...3.8.0) (2021-08-20)

### Features

* Work around docker.sock in filebeat service ([073f5ea](https://gitlab.com/biomedit/portal/commit/073f5ea8ac7889760cf4992b659f1220d5f28f4c))
* **frontend:** show message instead of website if visitor is using an unsupported browser ([4d2ca25](https://gitlab.com/biomedit/portal/commit/4d2ca251328d1d0e64eb4df5a15edce03227d695)), closes [#189](https://gitlab.com/biomedit/portal/issues/189)
* podman deployment ([52d759b](https://gitlab.com/biomedit/portal/commit/52d759b8d106289eda0bd8be0697c2c7b75fc1d1))
* **security:** Avoid docker.sock in traefik service ([a204728](https://gitlab.com/biomedit/portal/commit/a20472869b3b8ea8771e17ccb45811c157f3ca2b))
* **backend:** add history for changes on `User` ([18d5a05](https://gitlab.com/biomedit/portal/commit/18d5a059b92431227b172f01786c83f6c57a4377)), closes [#422](https://gitlab.com/biomedit/portal/issues/422)
* **backend/projects:** allow node admins to edit and create projects associated with their node ([9579f31](https://gitlab.com/biomedit/portal/commit/9579f3133e8252f4eba7625cf6f4f6408f5c097a)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/ProjectForm:** allow node admins to only select nodes they are associated with when creating a new project ([d053ed0](https://gitlab.com/biomedit/portal/commit/d053ed0f30de082970073aa8e8042629355cf749)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/ProjectForm:** prevent node admins from changing the node of an existing project ([dcfd973](https://gitlab.com/biomedit/portal/commit/dcfd973636bdf1bf9dbd45d68179c7d33ded5b20)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend/Projects:** allow node admins to edit and create projects associated with their node ([2591ae4](https://gitlab.com/biomedit/portal/commit/2591ae4b45ff10aefea6925429b6664248de0175)), closes [#406](https://gitlab.com/biomedit/portal/issues/406)
* **frontend:** display groups that a user belongs to in userinfo box. ([c1a11ce](https://gitlab.com/biomedit/portal/commit/c1a11ced6eaaa47b0e60fd4c27ee1166a1b592ae)), closes [#370](https://gitlab.com/biomedit/portal/issues/370)
* **frontend/admin/user:** display user flags in username tooltip ([d886aa3](https://gitlab.com/biomedit/portal/commit/d886aa3aab342ee95830f513abcd998918c65dab)), closes [#414](https://gitlab.com/biomedit/portal/issues/414)

### Bug Fixes

* **docker-compose:** run filebeat with `root` user ([e19eaff](https://gitlab.com/biomedit/portal/commit/e19eafff6b0b655f538013708f410cbfa6586ee4))
* **docker-compose:** prevent filebeat from running in a restart loop ([ffba1c9](https://gitlab.com/biomedit/portal/commit/ffba1c9518449bb47923ae1d30ca98b85a04b18b))
* **backend/unique_check:** log `is not unique` responses from a unique check as `warn` instead of `error` ([4f615d4](https://gitlab.com/biomedit/portal/commit/4f615d4dc9f8eb9df8d3dc40f3fff80d357e8b83)), closes [#395](https://gitlab.com/biomedit/portal/issues/395)

## [3.7.0](https://gitlab.com/biomedit/portal/compare/3.7.0-dev.17...3.7.0) (2021-07-13)

### Features

* **backend/User:** add possibility to change/update 'uid', 'flags' and 'local_username' through the REST API ([37b8f33](https://gitlab.com/biomedit/portal/commit/37b8f33bb3557088fcc67acf6f0a026e4ebba9f9)), closes [#13](https://gitlab.com/biomedit/portal/issues/13)
* **frontend/Home:** make longer titles possible in quick access tiles ([26a7df6](https://gitlab.com/biomedit/portal/commit/26a7df62d35438c2c224a093170bc1ff0207b6d8)), closes [#412](https://gitlab.com/biomedit/portal/issues/412)
* **frontend/Home:** display quick access tiles specified in the database ([9d987e7](https://gitlab.com/biomedit/portal/commit/9d987e7524b95e4fc5cfad16f3d49906b240dd27)), closes [#393](https://gitlab.com/biomedit/portal/issues/393)
* **quick accesses:** move quick access tiles administration to database ([52e0560](https://gitlab.com/biomedit/portal/commit/52e05604a332599b49592265d59f627de2848936)), closes [#393](https://gitlab.com/biomedit/portal/issues/393)
* **backend/DataPackageLogViewSet:** accept model permissions on the view ([16fa701](https://gitlab.com/biomedit/portal/commit/16fa701695ef34a1d7a2d45f4012763001d90f68)), closes [#405](https://gitlab.com/biomedit/portal/issues/405)
* **backend/logging:** add app name to request logging ([e91449f](https://gitlab.com/biomedit/portal/commit/e91449f931a1d8c36f94fad574a641d4b0fa365b)), closes [#397](https://gitlab.com/biomedit/portal/issues/397)
* **frontend/project:** add 'contact' to project resource ([e34cbce](https://gitlab.com/biomedit/portal/commit/e34cbce3545b6504a1a9277a9acfe9c8f026aa5b)), closes [#321](https://gitlab.com/biomedit/portal/issues/321)
* **frontend/project:** change resources management in project space ([105dd9a](https://gitlab.com/biomedit/portal/commit/105dd9a5b84f3a5237aafe2328f4da24b1ae44d0)), closes [#321](https://gitlab.com/biomedit/portal/issues/321)

### Bug Fixes

* **frontend/Administration/Users:** fix issue where sorting by username would result in an exception ([0cc2030](https://gitlab.com/biomedit/portal/commit/0cc20302259c1849ef0712d946b79a6e504bf65d)), closes [#418](https://gitlab.com/biomedit/portal/issues/418)
* **frontend:** add `UserMenu` back ([9fb7fee](https://gitlab.com/biomedit/portal/commit/9fb7fee6fade425b91c0f746ee2d4980da90cb41)), closes [#411](https://gitlab.com/biomedit/portal/issues/411)
* **frontend:** fix weird styling in frontend production builds ([09ef4e6](https://gitlab.com/biomedit/portal/commit/09ef4e65c2e9afc64215b98808775df1b5553381)), closes [#411](https://gitlab.com/biomedit/portal/issues/411)

## [3.6.0](https://gitlab.com/biomedit/portal/compare/3.6.0-dev.6...3.6.0) (2021-06-15)

### Features

* **backend/permission:** allow node viewer/manager more read permissions ([2d63281](https://gitlab.com/biomedit/portal/commit/2d63281f6be66d3a07f9f1f079099dbbec81d8c9))
* **frontend/UserManage:** show affiliations tooltip on username ([ba68082](https://gitlab.com/biomedit/portal/commit/ba6808207adbbe2a618e2d66f374b0951d661c92)), closes [#361](https://gitlab.com/biomedit/portal/issues/361)

### Bug Fixes

* **backend/logging:** log requests in `identities` app ([4f9f337](https://gitlab.com/biomedit/portal/commit/4f9f3378907f9daba1c0f9ed48735e735681c3d2)), closes [#394](https://gitlab.com/biomedit/portal/issues/394)
* **frontend/admin/users:** label IS the code for flags on users panel ([bdc97db](https://gitlab.com/biomedit/portal/commit/bdc97dbbb4d372103f761f5b75f8333fa9845a4a)), closes [#399](https://gitlab.com/biomedit/portal/issues/399)
* **backend/tests:** fix tests not being executed which are in a class whose name is not prefixed by `Test` ([49d4de6](https://gitlab.com/biomedit/portal/commit/49d4de63dd26b8266ac7547150f157112244928e))

## [3.5.0](https://gitlab.com/biomedit/portal/compare/3.5.0-dev.12...3.5.0) (2021-05-25)

### Features

* **backend/user:** projects API view should display 'affiliation_id' of users as well ([2382c94](https://gitlab.com/biomedit/portal/commit/2382c94d1fedfbdd6e6e5a68331bc02ce195b629))
* **backend/user:** implement 'affiliation_id' based on 'linkedAffiliationUniqueID' ([3a7956b](https://gitlab.com/biomedit/portal/commit/3a7956b5425ea99f3507e63db7214fcc9b6e3b6b)), closes [#337](https://gitlab.com/biomedit/portal/issues/337)
* **backend:** remove LDAP support ([a893875](https://gitlab.com/biomedit/portal/commit/a893875dd1da6c3b8ba1d6c2df10e3725617608f)), closes [#388](https://gitlab.com/biomedit/portal/issues/388)
* **frontend:** show redux dev tools outside of local development if the environment variable `NEXT_USE_REDUX_DEV_TOOLS` is set to `true` ([a9fa207](https://gitlab.com/biomedit/portal/commit/a9fa207080314c508bde7723b6e9ed6721309179))
* **contact:** add contact form ([0682a64](https://gitlab.com/biomedit/portal/commit/0682a644cd99031931a5e37be635e24401c96702)), closes [#247](https://gitlab.com/biomedit/portal/issues/247)
* **backend/permissions:** add custom permission `admin_node` ([23b03bc](https://gitlab.com/biomedit/portal/commit/23b03bc96cfa51f094771cbe3583c9f495541d34)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **backend/permissions:** allow creating groups with permissions to access or modify permissions or groups ([28f7dbd](https://gitlab.com/biomedit/portal/commit/28f7dbdccfb62d2721b6f5529d4a9bae6214fad0)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **frontend/manage nodes:** make nodes editable by node admins ([0e41945](https://gitlab.com/biomedit/portal/commit/0e41945578a642eefbefb09464a9e1306eda0d53)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **frontend/NodeManagerForm:** make node id only editable by staff ([1a26f0a](https://gitlab.com/biomedit/portal/commit/1a26f0a3bd7d507b75b060f7a2cb34fd33d0ae65)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **group management:** allow Node Managers to edit `users` on groups ([8192569](https://gitlab.com/biomedit/portal/commit/81925696afd17df7d819def1eb41d2d4937ceee2)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **node:** remove `users` from `Node` ([aab1628](https://gitlab.com/biomedit/portal/commit/aab1628dabbff941d1c8e149ad7453b7667ee769)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)
* **node permissions:** show `Administration` menu option and `Node` tabs for Node Admins and Node Viewers ([d16b155](https://gitlab.com/biomedit/portal/commit/d16b1550aceef0b2537a2eed796c61a04a7f90d5)), closes [#352](https://gitlab.com/biomedit/portal/issues/352)

### Bug Fixes

* **frontend/GroupManageForm:** fix a bug where in some cases the objects field in the object permissions was empty on edit ([c89080a](https://gitlab.com/biomedit/portal/commit/c89080a4aa36896b6b5b6a6238902ad2d09f8005)), closes [#381](https://gitlab.com/biomedit/portal/issues/381), relates to [/github.com/react-hook-form/react-hook-form/issues/1558#issuecomment-623196472](https://github.com/react-hook-form/react-hook-form/issues/1558/issues/issuecomment-623196472)
* **frontend/NodeManage:** fix bug where navigating from `Projects` to the node administration will always show all nodes ([3145f1a](https://gitlab.com/biomedit/portal/commit/3145f1ac7880894ef78016d03ad86684de1c0d8b)), closes [#381](https://gitlab.com/biomedit/portal/issues/381)
* **frontend/Admin:** make 'isActive' property of user filterable and sortable ([e4f0caa](https://gitlab.com/biomedit/portal/commit/e4f0caa0bcdab5bb0c2639e8efa4ceed0b62cf68)), closes [#385](https://gitlab.com/biomedit/portal/issues/385)
* **frontend/LoadingButton:** make sure spinner in form submit buttons always stays within the boundaries of the button ([85ce44b](https://gitlab.com/biomedit/portal/commit/85ce44b2960ce9c1dc20a2427706fb9f61e930bd))

### [3.4.1](https://gitlab.com/biomedit/portal/compare/3.4.1-dev.0...3.4.1) (2021-05-11)

### Bug Fixes

* **backend/notification:** empty affiliation triggers an error ([05c0492](https://gitlab.com/biomedit/portal/commit/05c049290b91592357d8f66dac4da5153fd4aab0))
* **frontend/TextField:** labelled fields do not get properly disabled ([fdbc0cf](https://gitlab.com/biomedit/portal/commit/fdbc0cfcd38cd8dd749bf42cd6746cda7ddb2e6d))

## [3.4.0](https://gitlab.com/biomedit/portal/compare/3.4.0-dev.13...3.4.0) (2021-05-11)

### Features

* **frontend/user:** make sorting case- and language-insensitive ([25c4d4f](https://gitlab.com/biomedit/portal/commit/25c4d4f9499935d365aff662d31067500dc102fd)), closes [#357](https://gitlab.com/biomedit/portal/issues/357)
* **backend/data_transfer:** prevent data transfers from being created with a requestor that is not a data manager of the project ([1bfbb8d](https://gitlab.com/biomedit/portal/commit/1bfbb8d2e0b8ae5457f5b5d16c4b7be0d61952ca)), closes [#346](https://gitlab.com/biomedit/portal/issues/346)
* **frontend/DataTransferForm:** only show requestors in the dropdown which are data managers of the project ([7b442a9](https://gitlab.com/biomedit/portal/commit/7b442a9e93eeb8dd383b532852e50459e766c898)), closes [#346](https://gitlab.com/biomedit/portal/issues/346)
* **backend/scheduled job:** send a notification email if a user hasn't logged in for 60 days ([307866e](https://gitlab.com/biomedit/portal/commit/307866e946240853b33d65ba25ddc065f86f72bf)), closes [#333](https://gitlab.com/biomedit/portal/issues/333)
* **backend/data_package:** prevent a package from getting transferred using sett if it has been transferred before ([8b2a4c9](https://gitlab.com/biomedit/portal/commit/8b2a4c9b7e840e4b47cc96e1d7ad6a3f23e5a240)), closes [#350](https://gitlab.com/biomedit/portal/issues/350)
* **backend:** add object-level permission backend ([84f1c89](https://gitlab.com/biomedit/portal/commit/84f1c899dffda66848c145f28d2c7d1c5e070d36))
* **backend:** add object-level permission filter and permission verification class ([ecea135](https://gitlab.com/biomedit/portal/commit/ecea1354c9e4181a66fbe5ec302ea5c744e12170))
* **backend:** add object-level permission management endpoints ([b30b868](https://gitlab.com/biomedit/portal/commit/b30b868b83f0a7ea5c6dc9ad79c42a64012c1d91))
* **backend:** allow group management in user endpoint ([f6a39cf](https://gitlab.com/biomedit/portal/commit/f6a39cf56b890fc5571f5177b241a76abbcaeec4))
* **backend/DataProvider:** check object-level permissions ([ca74ab3](https://gitlab.com/biomedit/portal/commit/ca74ab381bda730c1067f589ccf275fc4f1fa927))
* **backend/Node:** check object-level permissions ([8a6bd11](https://gitlab.com/biomedit/portal/commit/8a6bd1144842fa2ce57b0bbada0609eb0c4e51aa))
* **frontend/admin:** add group management tab ([3281914](https://gitlab.com/biomedit/portal/commit/3281914cefa64b2efbc4a4f6ac9678f561710f62))
* **frontend/admin/user:** add group assignment field ([b71b621](https://gitlab.com/biomedit/portal/commit/b71b621b7a23a79ce6bbb870bccc86955318a204))
* **backend:** make the lowest and highest `uid` and `gid` of users and `gid` of projects configurable in `config.json` ([37f6fc1](https://gitlab.com/biomedit/portal/commit/37f6fc18d640373382447f0245cbb7c3bdf58adf)), closes [#11](https://gitlab.com/biomedit/portal/issues/11)

### Bug Fixes

* **backend/notification:** be smarter while checking user affiliations ([6d74528](https://gitlab.com/biomedit/portal/commit/6d74528a1ee536ebb1f01d3b48cfc598670f49c2))
* **frontend/AutocompleteField:** attach a hidden field in case a 'AutocompleteField' is disabled ([8b6d840](https://gitlab.com/biomedit/portal/commit/8b6d8408cfb106478f4febcdc795ed8cd160d0e4)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/LabelledField:** attach a hidden field in case a 'LabelledField' is disabled ([cd4420e](https://gitlab.com/biomedit/portal/commit/cd4420e415c9d1bde07c04570c7eb5808a5fa47f)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/project:** pass project role via hidden field and remove 'ref' when field is disabled ([a8f4850](https://gitlab.com/biomedit/portal/commit/a8f485006eef0e2f9a8e3ea463e6944ca1610112)), closes [#369](https://gitlab.com/biomedit/portal/issues/369)
* **frontend/logger:** import 'setimmediate' polyfill for winston logger ([5e1ead9](https://gitlab.com/biomedit/portal/commit/5e1ead940d4c1f0607e98c8756c083c4763691af)), closes [#377](https://gitlab.com/biomedit/portal/issues/377)
* **frontend/validation:** fix 'max' vs. 'maxLength' ([8270b91](https://gitlab.com/biomedit/portal/commit/8270b915f7fccdc176730746dace8c86997338f0)), closes [#360](https://gitlab.com/biomedit/portal/issues/360)
* **frontend/validation:** specify a key to each 'validate' definition and use 'deepmerge' for merging the validators ([7c91c2d](https://gitlab.com/biomedit/portal/commit/7c91c2d6d5738587fb2eb8395315f896e01fbaf7)), closes [#360](https://gitlab.com/biomedit/portal/issues/360)
* **frontend/user:** overwrite 'datetime' as it can't cope with 'null' value ([3b61117](https://gitlab.com/biomedit/portal/commit/3b611177c90ae0073d611b8c131ccf93ffb6eefb))
* **config:** revert configuration ([c71b369](https://gitlab.com/biomedit/portal/commit/c71b3698c6243d1c7182dab9419eae1502162d17))
* **frontend/user:** use 'datetime' as sort type for 'lastlogin' column ([c93ee2c](https://gitlab.com/biomedit/portal/commit/c93ee2cb9de92ebe63f8ecab4578926f761b59c2)), closes [#357](https://gitlab.com/biomedit/portal/issues/357)
* **backend/project:** make gid field read-only ([add778f](https://gitlab.com/biomedit/portal/commit/add778f099cdfd3ddaa53898b49c2cc760f9daed))
* **backend/model:** make model str representations more verbose ([3813c3b](https://gitlab.com/biomedit/portal/commit/3813c3b8575bb54c4390596f66d603c1ee197be2))
* **frontend/UserManageForm:** make flags field optional ([d98a768](https://gitlab.com/biomedit/portal/commit/d98a768425f97df662bb28335de6ba5ddb7d5ca5))

## [3.3.0](https://gitlab.com/biomedit/portal/compare/3.3.0-dev.12...3.3.0) (2021-04-20)

### Features

* **frontend/home:** add link to registry and specify 'target="_blank"' for each grid tile ([4370d4f](https://gitlab.com/biomedit/portal/commit/4370d4fd611038e6d79153eb3abcffa60d4bed2f)), closes [#336](https://gitlab.com/biomedit/portal/issues/336) [#343](https://gitlab.com/biomedit/portal/issues/343)
* **backend/user:** send notification ticket on user affiliation change ([3af7c70](https://gitlab.com/biomedit/portal/commit/3af7c70b14dc476fa204db8cc5e34cbd57ef244e)), closes [#332](https://gitlab.com/biomedit/portal/issues/332)
* **forms:** add `SelectArrayField` ([db01ca7](https://gitlab.com/biomedit/portal/commit/db01ca71d7b492f0a20e28d941d4f0ff273d04f3)), closes [#338](https://gitlab.com/biomedit/portal/issues/338)
* **frontend/forms:** add `AutocompleteArrayField` ([5d142f4](https://gitlab.com/biomedit/portal/commit/5d142f4aec13ee321d33c06bdcc8f63c8d0fd45e)), closes [#338](https://gitlab.com/biomedit/portal/issues/338)
* **frontend/DataTransferTables:** only search for numbers in `id` and `maxPackages` ([4f134db](https://gitlab.com/biomedit/portal/commit/4f134dbf1b1a980b48008495660d1629943535a9)), closes [#329](https://gitlab.com/biomedit/portal/issues/329)
* **user:** display 'local_username' for project users ([ac84f11](https://gitlab.com/biomedit/portal/commit/ac84f1196d321788692358a28b537ee082fbbda7))
* **frontend/administration:** prevent the currently logged in user from inactivating themself ([bbee346](https://gitlab.com/biomedit/portal/commit/bbee346cd036447809fd07c49163d679f7c43564)), closes [#328](https://gitlab.com/biomedit/portal/issues/328)
* **frontend/tests:** log all requests on fails associated with `RequestVerifier` ([09a0751](https://gitlab.com/biomedit/portal/commit/09a07514a2822092c167ea2cfd39a3a37f558da3))

### Bug Fixes

* **backend/data_package:** Propagate metadata to the DB ([871254e](https://gitlab.com/biomedit/portal/commit/871254e9d1f235e699a74cd0756c2af3bc3d7ab3))
* **backend/login:** redirect users trying to log in using a link with an expired session to the login page ([7437526](https://gitlab.com/biomedit/portal/commit/74375262fcfbba08824d7828f0a59f82f1397d38)), closes [#341](https://gitlab.com/biomedit/portal/issues/341)
* **frontend/DataTransfers:** fix a bug where newly added data transfers in `Projects` would not appear in `Data Transfers` ([21ae295](https://gitlab.com/biomedit/portal/commit/21ae2954d2a859c7d82b8ab0572b4c35602755e0))
* **frontend/renderers:** fix error when rendering a `LabelledControl` with `multiple` and an empty value ([de4d50b](https://gitlab.com/biomedit/portal/commit/de4d50b46e8caf412f629b8236978b23c760bfb1))

## [3.2.0](https://gitlab.com/biomedit/portal/compare/3.2.0-dev.11...3.2.0) (2021-03-22)

### Features

* **frontend/navigation:** highlight current route ([df1b46e](https://gitlab.com/biomedit/portal/commit/df1b46e944293b058553eb64883552e7461ef3f8))
* **frontend/Home:** add easter egg during easter ([1b5ffd2](https://gitlab.com/biomedit/portal/commit/1b5ffd2c8e5d66cb0777d2b3f07358087dd21765)), closes [#304](https://gitlab.com/biomedit/portal/issues/304)
* **frontend/Home:** show easter background during easter ([5c8d48a](https://gitlab.com/biomedit/portal/commit/5c8d48af3fc431ec96caf67f8acb82a3866f8dde)), closes [#304](https://gitlab.com/biomedit/portal/issues/304)
* **Administration/Users Table:** add column showing if user is active or not ([f162c09](https://gitlab.com/biomedit/portal/commit/f162c09386b8129c791eaa128ae90c8b80edb8d3)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **backend/user:** remove objects related to a user which was made inactive ([8ce40a6](https://gitlab.com/biomedit/portal/commit/8ce40a69b79dd223bec3edf565320e4fc1699fbf)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **user management:** include inactive users in the list of users ([fbf10bd](https://gitlab.com/biomedit/portal/commit/fbf10bd9d3115d8eb9e2700f07f2e8532f07781c)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **User Management:** allow changing the active status of a user from the frontend ([ee87471](https://gitlab.com/biomedit/portal/commit/ee874713d65d6a57e71e7f9de573bdbe81f6920e)), closes [#268](https://gitlab.com/biomedit/portal/issues/268)
* **Feed:** add basic markdown parsing for `title` and `message` ([9ba40ce](https://gitlab.com/biomedit/portal/commit/9ba40ce8ee50e8604a16f8c32616d849b4f1010a)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **Feed:** allow specifying a `title` separately from a `message` for `Feed` ([a6d742d](https://gitlab.com/biomedit/portal/commit/a6d742d6914eb8ed05f2d24fc21a2adf1bfcf469)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Feed:** change icon for `Feed` label `WARN` to a warning symbol ([02f8eb0](https://gitlab.com/biomedit/portal/commit/02f8eb0ebfdb0b27ff2f8f8ccfb8b45d2b890463)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/FeedList:** show empty message when no feeds are present ([ce8c8f9](https://gitlab.com/biomedit/portal/commit/ce8c8f9c9ed810cae3457dc0d6d7d59f9815e4d1)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)

### Bug Fixes

* **frontend:** catch 'undefined' on value and default value ([a7c293a](https://gitlab.com/biomedit/portal/commit/a7c293a803c063930ccbb6cd54b6630e053811a9)), closes [#324](https://gitlab.com/biomedit/portal/issues/324)
* **frontend:** fix console errors when specifying users in a new project ([de109be](https://gitlab.com/biomedit/portal/commit/de109be9b7b10a1d60c1662a8c2b216716c8ef89)), closes [#324](https://gitlab.com/biomedit/portal/issues/324)
* **frontend/Home:** fix layout issue where quick access links would separate too far apart ([0eb6b62](https://gitlab.com/biomedit/portal/commit/0eb6b62ab1956254e45443adc9d22036e4a8f4a5))
* **backend/signals:** remove deprecation warning ([586a458](https://gitlab.com/biomedit/portal/commit/586a4582b2ac20ff25b2397cf598b192b33bb0cd))
* **frontend/DataTransferTable:** fix console error related to updating a component while rendering a different component ([d21374a](https://gitlab.com/biomedit/portal/commit/d21374a8839ecacb2b7d4d3e1a706ada412a8afb)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Home:** fix console error about missing `ref` ([740a572](https://gitlab.com/biomedit/portal/commit/740a57223f2331b64e71f39073b0a4a18ae126f8)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Home:** fix console error related to invalid DOM nesting ([8ffc005](https://gitlab.com/biomedit/portal/commit/8ffc005900837efb25b7febc7ff56de3fa63d1cc)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Projects:** fix console error about `Description` not having unique keys ([c810e66](https://gitlab.com/biomedit/portal/commit/c810e661fcd8d1bd10db7c089de1c0902385da5f)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Projects:** fix console error related to duplicate keys ([013b2ca](https://gitlab.com/biomedit/portal/commit/013b2ca351b2b30420cd0e32f54cf3910a2d9ae1)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Tab:** fix console error related to invalid DOM nesting ([de77b2f](https://gitlab.com/biomedit/portal/commit/de77b2f0ea7972af6225cace7d5eb906cfc9daa4)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Tables:** fix console error related to invalid DOM nesting ([c6738fd](https://gitlab.com/biomedit/portal/commit/c6738fdc2dd434fb8a4c15dc853702d5c1f0291f)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/UserInfoBox:** fix console error when opening the `UserInfoBox` due to a missing forwarded ref ([a5b5b42](https://gitlab.com/biomedit/portal/commit/a5b5b421a1c1b8a1ede57cfd5eeabd85047057e2)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/UserMenu:** fix console error when opening the Messages box ([3e401c4](https://gitlab.com/biomedit/portal/commit/3e401c48b5f9405d7f881efdaed82a458c263434)), closes [#312](https://gitlab.com/biomedit/portal/issues/312)
* **frontend/Feed:** fix a bug where a feed with label `Warning` would not show an icon ([af417f7](https://gitlab.com/biomedit/portal/commit/af417f74e8b9981e309257e7edb03cb18391efad)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Feed:** fix a bug where line breaks in Feed messages were ignored ([5e5bf7f](https://gitlab.com/biomedit/portal/commit/5e5bf7fd838b34ee5b70962a72d1cbc0f9432b24)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)
* **frontend/Home:** fix strange layout with longer text in `Feed` ([6e49be1](https://gitlab.com/biomedit/portal/commit/6e49be168b2f2cce3234b81bcbb93a45ba5973e9)), closes [#61](https://gitlab.com/biomedit/portal/issues/61)

## [3.1.0](https://gitlab.com/biomedit/portal/compare/3.1.0-dev.9...3.1.0) (2021-03-09)

### Features

* **frontedn/UserInfoBox:** show user flags ([d0a12f9](https://gitlab.com/biomedit/portal/commit/d0a12f93bdae2cb08e3df21d2cb291c30c2bd887)), closes [#250](https://gitlab.com/biomedit/portal/issues/250)
* **backend/user:** add optional flags specification on user ([21d9e98](https://gitlab.com/biomedit/portal/commit/21d9e980fad2a2c388b447d18214a789f20e6425)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/flags:** add a new tab for flags administration ([c050f87](https://gitlab.com/biomedit/portal/commit/c050f8706aa91fa11517068b193d4327352561a3)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/reducers:** introduce reducer for flags ([1f142c4](https://gitlab.com/biomedit/portal/commit/1f142c4f953682860cfe0ac8e1e49e171c739fbd)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend/user form:** display the flags in the user form ([9d677b6](https://gitlab.com/biomedit/portal/commit/9d677b6a72398d9cda0a40bc2b430fddf4b0f300)), closes [#288](https://gitlab.com/biomedit/portal/issues/288)
* **frontend:** make docs url configurable in env vars ([4ed892c](https://gitlab.com/biomedit/portal/commit/4ed892c48c5273bb95a8b306b07effcd86166de2)), closes [#299](https://gitlab.com/biomedit/portal/issues/299)
* **maxPackages:** enforce 'maxPackages' specification (no longer optional) ([95d0bb7](https://gitlab.com/biomedit/portal/commit/95d0bb7783f0a5aa01d684fa340ef0d0f108627a))
* **backend/migrations:** generate migration script ([8d6c9bc](https://gitlab.com/biomedit/portal/commit/8d6c9bcf14dbfdb3eadaf9b623b7489819fb6a30)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)
* **backend/models:** replace user 'status' with flags ([76bb9a1](https://gitlab.com/biomedit/portal/commit/76bb9a1a5d0d5ea4ba65c4fb584e87ca45ad813a)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)
* **frontend/api:** re-generate the API ([6a7350c](https://gitlab.com/biomedit/portal/commit/6a7350c91f632be693c5564aa4d75cb022a29415)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)

### Bug Fixes

* **frontend/FlagManageForm:** use right backend API for flag uniqueness ([d42c71d](https://gitlab.com/biomedit/portal/commit/d42c71d1f32c5bf180225b14681c88b32772c5d6))
* **frontend/forms:** fix delay when clicking on the `submit` button in forms ([f94df38](https://gitlab.com/biomedit/portal/commit/f94df38d1b2f09f87bac4a8e6483328017a99bb4)), closes [#280](https://gitlab.com/biomedit/portal/issues/280)
* **frontend/affiliations:** nicely display the affiliations ([dedc85d](https://gitlab.com/biomedit/portal/commit/dedc85dc033fa14a20034625f826f3caa203a56e))
* **backend/exception_handler:** log exceptions where a user is not logged in as `INFO` instead of `ERROR` ([6fc62b5](https://gitlab.com/biomedit/portal/commit/6fc62b5f3fe70a419ddd9ee3e67424e492628f30))
* **frontend/maxPackages:** show unlimited DTR's when filtering data transfer tables for 'unlimited' ([eee6856](https://gitlab.com/biomedit/portal/commit/eee685643cd1eef277086e7c825b68d75edc0e3b)), closes [#249](https://gitlab.com/biomedit/portal/issues/249)
* **frontend/user administration:** remove displaying/management of user status ([aacf895](https://gitlab.com/biomedit/portal/commit/aacf895b33b13a4360e726190c1189e0f2429bd0)), closes [#283](https://gitlab.com/biomedit/portal/issues/283)

## [3.0.0](https://gitlab.com/biomedit/portal/compare/3.0.0-dev.6...3.0.0) (2021-02-22)

### ⚠ BREAKING CHANGES

* **frontend:** The restriction of only having environment variables exposed to the frontend
with keys prefixed by `REACT_APP_` came from `create-react-app`.
With the migration to Next.js, we have to use the Next.js specific prefix `NEXT_PUBLIC_`.

Migration Steps: Rename all environment variable key names starting with `REACT_APP_` to start with `NEXT_PUBLIC_` instead.

### Features

* **logging:** unify different logging streams into the console output ([3be84e1](https://gitlab.com/biomedit/portal/commit/3be84e1bb77776278a9d40a653966d5ec6ec8aca))
* **frontend:** conditional adding of empty entries for IP ranges and resources ([e15f7f7](https://gitlab.com/biomedit/portal/commit/e15f7f78d559a1de20d7318c951dabb9fba86244))
* **backend/test_node:** ensure that we have the right number of registered node users returned ([b473ee5](https://gitlab.com/biomedit/portal/commit/b473ee518928ff196e01027da8c95a29d149c662))

### Bug Fixes

* **frontend/resources:** improve layout of resources block ([e3c303f](https://gitlab.com/biomedit/portal/commit/e3c303fc9481fbf2005b0e28d3bd2894747b0de1)), closes [#290](https://gitlab.com/biomedit/portal/issues/290)
* **backend/user status:** remove all consequences linked to user status changes ([42acd87](https://gitlab.com/biomedit/portal/commit/42acd87b9d0ed0725fea7fc3ba2b210e81e1758c)), closes [#219](https://gitlab.com/biomedit/portal/issues/219) [#217](https://gitlab.com/biomedit/portal/issues/217) [#284](https://gitlab.com/biomedit/portal/issues/284)
* **frontend/UsersList:** remove warning icon when user is 'INITIAL' and get rid of 'excludeUnauthorized' ([6692199](https://gitlab.com/biomedit/portal/commit/6692199d3794750330de3b9d63d547ec27fd39cf)), closes [#284](https://gitlab.com/biomedit/portal/issues/284)
* **frontend/UsersList:** make sure that we are comparing numbers ([7cd17e6](https://gitlab.com/biomedit/portal/commit/7cd17e6d2205a6c125866795b4120258c8c3348e)), closes [#295](https://gitlab.com/biomedit/portal/issues/295) [#279](https://gitlab.com/biomedit/portal/issues/279)
* **frontend:** prevent errors when accessing `window` / `document` during SSR ([ea821d1](https://gitlab.com/biomedit/portal/commit/ea821d1cffe502bb77c324b5bbdcf950c4d3a724))
* **NotFound:** fix error `h6 cannot appear as a descendant of p` ([f95774a](https://gitlab.com/biomedit/portal/commit/f95774a76f4741146de69db2e97feff660be15cf))
* **frontend:** replace `react-env` mechanism for loading environment variables with Next.js built-in features ([46dcf8d](https://gitlab.com/biomedit/portal/commit/46dcf8d5c7fe3f5892abbd6522dd9f3b83cf25b1))
* **backend/user:** return all active users from '/backend/users' to node managers ([b4a722f](https://gitlab.com/biomedit/portal/commit/b4a722feb2986366b23ed545ab544c7216069439)), closes [#291](https://gitlab.com/biomedit/portal/issues/291)

## [2.2.0](https://gitlab.com/biomedit/portal/compare/2.2.0-dev.12...2.2.0) (2021-02-08)

### Features

* **backend/data_transfer:** completely remove RT ticket generation/email notification on DTR creation ([b8af12e](https://gitlab.com/biomedit/portal/commit/b8af12e6e0ad65e304b276b1ec12fdc3a42fa600)), closes [#276](https://gitlab.com/biomedit/portal/issues/276)
* **frontend:** DM are no longer able to create new DTRs ([260c860](https://gitlab.com/biomedit/portal/commit/260c8606e1b934a360b17690ec5b56c144c291e0)), closes [#276](https://gitlab.com/biomedit/portal/issues/276)
* **frontend/forms:** make all dialogs that can be cancelled closeable by pressing the escape key on the keyboard or clicking outside of the dialog ([0d19c8b](https://gitlab.com/biomedit/portal/commit/0d19c8bdb76d68a6b6eec1dd77d91e9a6a9cb2dd))
* **backend:** a node manager can edit his own nodes ([801e859](https://gitlab.com/biomedit/portal/commit/801e859b5029045afc88d64fd885b8f674875740)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **backend/ReadOnly:** 'ReadOnly' should apply to authenticated users only ([10462a5](https://gitlab.com/biomedit/portal/commit/10462a55cc4bd7240c6af2b8a8c8b4b9a743af77))
* **frontend:** polish node management by NM ([ce12192](https://gitlab.com/biomedit/portal/commit/ce1219219607f2b76f2d9063c4f3b27ca1757ed8)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **frontend/navigation:** rename 'Users & Groups' to 'Administration' ([a6a7651](https://gitlab.com/biomedit/portal/commit/a6a76511b3cc58d818291388130f4e13c519245f))
* **frontend/permmissions:** make 'Nodes' tab item accessible to node managers ([d813e85](https://gitlab.com/biomedit/portal/commit/d813e85b66a340136c1c660b4a8a97ea3e3c97c4)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **DataTransfer:** prevent changing purpose on DTR's once a data package was created ([6a1edbf](https://gitlab.com/biomedit/portal/commit/6a1edbf07fb1b8ce8c3cd4be433cf95406a4a23a))
* **users:** validate `email` on `User` to be unique ([2968a4d](https://gitlab.com/biomedit/portal/commit/2968a4d2d0477687189c9adfa16fecd7544baf35)), closes [#273](https://gitlab.com/biomedit/portal/issues/273)

### Bug Fixes

* **frontend/ListHooks:** fix repetitive effect calls ([87496b3](https://gitlab.com/biomedit/portal/commit/87496b396589d8bfd017ffbee39a831a8a00f17d))
* **frontend/UsersList:** fix add user not working ([87aa1bb](https://gitlab.com/biomedit/portal/commit/87aa1bbb842a2245b43f2e8328ba00ad20cb19d6))
* **frontend:** revert cleanup "missing dependencies" linter warnings ([cf42297](https://gitlab.com/biomedit/portal/commit/cf422971267853ab4a50dceb8e94b72e9e727361))
* **frontend/ProjectList:** only show edit button on projects the user has the permissions to edit ([2141216](https://gitlab.com/biomedit/portal/commit/21412161ba66bef32488ba8e10d161f9f6295111)), closes [#281](https://gitlab.com/biomedit/portal/issues/281)
* **backend/Userinfo:** make Userinfo ID required as we're only using GET ([dda607d](https://gitlab.com/biomedit/portal/commit/dda607d4e0032b0c4020cb0c556e3db0a9103333))
* **frontend:** make sure that NMs are not editing node IDs ([fcc6a63](https://gitlab.com/biomedit/portal/commit/fcc6a632501b950625c305c57c103e6e2a7927c4)), closes [#269](https://gitlab.com/biomedit/portal/issues/269)
* **frontend/empty tables:** fix add button not being visible when table is empty ([5f8ac65](https://gitlab.com/biomedit/portal/commit/5f8ac654960fc986efa1a9898efbf8f324befed5))

## [2.1.0](https://gitlab.com/biomedit/portal/compare/2.1.0-dev.0...2.1.0) (2021-01-26)

### Features

* **user:** enforce `email` of users to be unique ([2a51934](https://gitlab.com/biomedit/portal/commit/2a519344f05f998630baf0ea2632a3bad896441b))

## [2.0.0](https://gitlab.com/biomedit/portal/compare/2.0.0-dev.1...2.0.0) (2021-01-26)

### ⚠ BREAKING CHANGES

* **backend:** If you are upgrading an existing `portal` installation, you MUST first deploy this version
and follow the migration steps BEFORE deploying any later version!

Django requires the first migration to be the one that initializes a custom user model
(see <https://docs.djangoproject.com/en/3.1/topics/auth/customizing/#changing-to-a-custom-user-model-mid-project>)
which is why all existing migrations had to be removed, which requires manual intervention during deployment.

Migration Steps:

* Stop the running docker containers, except for the database container
* Make a backup of the database
* Run the following query on the database: `TRUNCATE django_migrations`
* Deploy the image of EXACTLY this version (don't run the backend server yet)
* Run `./manage.py migrate --fake` on the backend container
* Run the backend server as usual
* From this point on, you can deploy any version later than this one,
using `./manage.py migrate` (without `--fake`) and running the server like usual

* **backend:** use custom user model for authentication ([875c98f](https://gitlab.com/biomedit/portal/commit/875c98f27b7bfd27cfd76a06eb76cbfc01b6b398))

### Features

* **frontend/UserManageForm:** add cancel button to user edit form ([f118e18](https://gitlab.com/biomedit/portal/commit/f118e18d6cea66e4761fd56856d95b228a259614))
* **user management:** enable creating local users in the user management in the frontend ([0c7cef7](https://gitlab.com/biomedit/portal/commit/0c7cef78cf4f87f5aee0b5664435196f44786f4a))

### Bug Fixes

* **user_init:** use `get_user_model` to retrieve the `User` model as defined in `settings.py` ([cd7a6dc](https://gitlab.com/biomedit/portal/commit/cd7a6dc513536887d6e9e5c626bf8aa68f54d9ac))

## [1.4.0](https://gitlab.com/biomedit/portal/compare/1.4.0-dev.8...1.4.0) (2021-01-25)

### Features

* **backend/unauthorized users:** prevent the backend from accepting users in `UNAUTHORIZED` status in modifying operations on projects ([0b973cd](https://gitlab.com/biomedit/portal/commit/0b973cd6ec3cad498ffeeb863bfbc90fbe694bb5)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **frontend/ProjectForm:** show warning icon next to users with `INITIAL` state ([8662bfc](https://gitlab.com/biomedit/portal/commit/8662bfcf8e1fb36d57aa1fee091fe35d341477c5)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **frontend/user selection:** hide users in `UNAUTHORIZED` status from user selection in the project form ([7adc655](https://gitlab.com/biomedit/portal/commit/7adc655d4dacc80bba518a484d54828a5a7ff98e)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **Project Resources:** disable resources for users with `INITIAL` status ([c33a55b](https://gitlab.com/biomedit/portal/commit/c33a55b317fc9d11f316185b7434a1cf33e29cea)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **users:** hide users which are not active ([3b494a9](https://gitlab.com/biomedit/portal/commit/3b494a99fe5b56818f9314122d8f968210414ff3)), closes [#217](https://gitlab.com/biomedit/portal/issues/217)
* **backend/project_user_notification:** for each user added/removed, specify the status ([064eb1c](https://gitlab.com/biomedit/portal/commit/064eb1cc4eff99b4cd611d0807e36daffa0e9c35))
* **data package:** Add 'ordering' to '/backend/data-transfer/' ([a569dbc](https://gitlab.com/biomedit/portal/commit/a569dbcd663b65930ccff4b1e7ae38f6189910e8))
* **fontend/DataTransferDetail:** Use file name instead of 'Package X' ([fc78b35](https://gitlab.com/biomedit/portal/commit/fc78b358242404e4c7498eeaecad4f9c2943baf2))
* **serializers/data_transfer:** Sort data packages inside a data transfer by file name ([ac964c1](https://gitlab.com/biomedit/portal/commit/ac964c1482ada3feb7967c8e51871ccf3a2e0618))
* **backend:** add name to projects app ([157aa50](https://gitlab.com/biomedit/portal/commit/157aa50dcdd21eacfffecd3733637e189ce8f4f1))
* **logging:** add request logging ([e8302ae](https://gitlab.com/biomedit/portal/commit/e8302ae0af4bfce4a9a5b16b9d4e031e71af9785)), closes [#242](https://gitlab.com/biomedit/portal/issues/242)
* **test:** Improve error message on failing ([499a25c](https://gitlab.com/biomedit/portal/commit/499a25ce3615101d7a3084caa04943637f6ff3b4))
* **frontend/ProjectForm:** Add more space between project metadata fields ([5f214ab](https://gitlab.com/biomedit/portal/commit/5f214aba6bb2f75fa2b4465f26ed77de7e961d83))
* **frontend/SetUsernameDialog:** Use 'fullWidth' for input field ([46d73cb](https://gitlab.com/biomedit/portal/commit/46d73cb4142944cfe56d7d0128d365026282c2bb))

### Bug Fixes

* **backend/update_permissions:** add user status to PL mail ([30518bd](https://gitlab.com/biomedit/portal/commit/30518bd86a0795b5d5bdfaae03d06fccd1a7392a))
* **frontend/forms:** support nested array fields in forms ([12f41ec](https://gitlab.com/biomedit/portal/commit/12f41eced27af14221945ffb24237d445eecc293))
* **UserDataPermission:** handle requests without profile field ([ffdf7df](https://gitlab.com/biomedit/portal/commit/ffdf7df3ce6e306290ac1c8ce91277bdb21f4b67))

## [1.3.0](https://gitlab.com/biomedit/portal/compare/1.3.0-dev.12...1.3.0) (2021-01-11)

### Features

* **projects:** only send email notification to ticketing system when AUTHORIZED users are added/removed to/from a project ([df56f4d](https://gitlab.com/biomedit/portal/commit/df56f4d1e28f25e4d77bf7d8180b90c989468e23)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects:** remove all project permission from users when their status is changed from AUTHORIZED to INITIAL or UNAUTHORIZED and send email notification ([519d3ba](https://gitlab.com/biomedit/portal/commit/519d3babdf7534c09f208983367d1c684a00d2ea)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects:** send email notification to ticketing system when status of user is set to AUTHORIZED ([39a4a9f](https://gitlab.com/biomedit/portal/commit/39a4a9fe3885ceb33f9c246d49d3bb160010e571)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **projects/users:** only return users with AUTHORIZED status when users of a project are retrieved by permission ([c48b8c3](https://gitlab.com/biomedit/portal/commit/c48b8c3eccdd5dd98d4fc6c560aede21c333b18f)), closes [#219](https://gitlab.com/biomedit/portal/issues/219)
* **backend:** Add key_length to PgpKey model ([b1dd5d6](https://gitlab.com/biomedit/portal/commit/b1dd5d6157f10c0b37866398bf9ba2b2627087ef))
* **backend:** key sign request: check for minimal key length ([7da8401](https://gitlab.com/biomedit/portal/commit/7da8401c801972489b43b81075dc4c60ed81bb43))
* **backend/OIDCAuth:** Adapt 'OIDCAuth' to support local -> federated user migration ([5e5d727](https://gitlab.com/biomedit/portal/commit/5e5d7270e0137247b4fb6eec4911719ea10fbc80))
* **frontend/ProjectForm:** Check whether at least one PL has been assigned to the project ([2f31f01](https://gitlab.com/biomedit/portal/commit/2f31f01600243b530e81fc1c79bea472c09f8258))
* **data providers:** show unique validation errors for name and id ([cdd9831](https://gitlab.com/biomedit/portal/commit/cdd983188431ede5dbde6f2edb86aec4ae24ee5b))
* **frontend:** prevent repeated calls to the backend during uniqueness check if keystrokes are less than 750ms apart ([b626fcc](https://gitlab.com/biomedit/portal/commit/b626fcc6d129dbebaccfaeec46c3c0551a511567))
* **nodes:** show unique validation errors for name and id ([20b6bc1](https://gitlab.com/biomedit/portal/commit/20b6bc1d9ea882217c4775e6e1798a5f97fb5cc7))
* **projects:** show unique validation errors for name and id ([a6f06f6](https://gitlab.com/biomedit/portal/commit/a6f06f63cbfd9c33de768c1e0687a4fa8a0117a6))
* **backend:** send all logs to ELK ([2f59b3a](https://gitlab.com/biomedit/portal/commit/2f59b3a4efa2976739fbc4d0340ee4e8d27f4867))

### Bug Fixes

* **tables:** navigate to first page when search query is changed ([c49d1e3](https://gitlab.com/biomedit/portal/commit/c49d1e366a8d17b5578c6fa18fcb8a955296327c)), closes [#256](https://gitlab.com/biomedit/portal/issues/256)
* **username form:** correct validation error to mention that the username must start with a letter ([93e06fc](https://gitlab.com/biomedit/portal/commit/93e06fc155966f3238fc28fa4298fb3871229c78)), closes [#259](https://gitlab.com/biomedit/portal/issues/259)
* **backend/project:** `send_user_emails` should be invoked on project creation/deletion as well ([4748a9f](https://gitlab.com/biomedit/portal/commit/4748a9fe0bd51f66532d4c3cab905c108387c0bb))
* **node:** fix unique check for nodes ([ed07c0a](https://gitlab.com/biomedit/portal/commit/ed07c0adab6eb4078e58b07231784fa21ae37605))
* **username validation:** fix incorrect validation error message for usernames ([32c2210](https://gitlab.com/biomedit/portal/commit/32c2210f7341aaf5841a4598d5e842ba8a35939e))
* **frontend:** fix unique check not working in production builds ([891c6bc](https://gitlab.com/biomedit/portal/commit/891c6bcece7733c5ea2a9c3cd6110f74ef759ef8)), relates to [/github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js#L272](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js/issues/L272)
* **frontend:** when editing an object, changing a text field's value back to its initial value will not show a unique validation error ([d961e50](https://gitlab.com/biomedit/portal/commit/d961e50ccb781f384ffcff761ca882986b36ebbf))
* **frontend/projects:** Revert Display dedicated "empty project" message for initial users ([d71a91a](https://gitlab.com/biomedit/portal/commit/d71a91a1bfcbfdc9affe12c0477a24e33b5723b2))
* **frontend:** remove leading dollar sign in correlationId's ([eac12c6](https://gitlab.com/biomedit/portal/commit/eac12c64b02f89a74a16ce3a2d0fb003df129f43))
* **frontend:** fix toasts not appearing on errors ([bc9bd71](https://gitlab.com/biomedit/portal/commit/bc9bd7163629a8ad2ec80fb99bcff7e70b726357))

## [1.2.0](https://gitlab.com/biomedit/portal/compare/1.2.0-dev.4...1.2.0) (2020-12-14)

### Features

* **frontend/projects:** Display dedicated "empty project" message for initial users ([7410716](https://gitlab.com/biomedit/portal/commit/74107162e891cad4249937df6fa8c6d730a93e5c))
* **frontend/users:** show user status in table under `Users & Groups` ([ca93594](https://gitlab.com/biomedit/portal/commit/ca935941722721e48560fcf5f02c862fd9086f28)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **user:** add new property `status` to user profile ([c4fa04a](https://gitlab.com/biomedit/portal/commit/c4fa04a1b675112e1da6e87c4e3ba37f8d56cb67)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **users:** allow changing user status on the frontend ([0d22e13](https://gitlab.com/biomedit/portal/commit/0d22e13fc79278b7f96d6176b541bdda901c8796)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **frontend/users:** show user status in table under `Users & Groups` ([ca93594](https://gitlab.com/biomedit/portal/commit/ca935941722721e48560fcf5f02c862fd9086f28)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **user:** add new property `status` to user profile ([c4fa04a](https://gitlab.com/biomedit/portal/commit/c4fa04a1b675112e1da6e87c4e3ba37f8d56cb67)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)
* **users:** allow changing user status on the frontend ([b16d34c](https://gitlab.com/biomedit/portal/commit/b16d34cfcc05c627c68ce856224a1384b178cdd8)), closes [#215](https://gitlab.com/biomedit/portal/issues/215)

### Bug Fixes

* **frontend/fields:** support retrieving initial values of fields which are nested ([2dbaaa8](https://gitlab.com/biomedit/portal/commit/2dbaaa82e72e27acf180e26f879b073dd7c4344c))
* **frontend/fields:** support retrieving initial values of fields which are nested ([2dbaaa8](https://gitlab.com/biomedit/portal/commit/2dbaaa82e72e27acf180e26f879b073dd7c4344c))

## [1.1.0](https://gitlab.com/biomedit/portal/compare/1.1.0-dev.10...1.1.0) (2020-12-08)

### Features

* allow "'" in names ([69704d7](https://gitlab.com/biomedit/portal/commit/69704d79486ab9c5949685552e6dc28f0c6f2b04))
* **frontend/Home:** show christmas wishes during christmas ([f34a530](https://gitlab.com/biomedit/portal/commit/f34a5300c70710fee93d3d803bdfe4c16aec11cd))
* **frontend/UserMenu:** show christmas hat on user icon during christmas ([659bf33](https://gitlab.com/biomedit/portal/commit/659bf33db993fe927812e558b8521cbb3e3778ce))
* **frontend/UserMenu:** show snow background during christmas ([dc9d081](https://gitlab.com/biomedit/portal/commit/dc9d08162e36ad27aab2bf62513917dfae57bbf0))
* **frontend/nav:** show current portal version ([ecda227](https://gitlab.com/biomedit/portal/commit/ecda227091a603b657cafd3875d58abd3d03c6de))

### Bug Fixes

* **backend/dtr_creation:** only create child tickets if we have a 'complete' ticket body ([f21a021](https://gitlab.com/biomedit/portal/commit/f21a0216609d1ead2bfac786607bee8b09c9f1ea))
* **frontend/ProjectForm:** make project name wider ([de50406](https://gitlab.com/biomedit/portal/commit/de50406a5b998a5845bb2ad1de236d48bb6b91ff))
* **backend/dtr_creation:** remove unused method ([aed9901](https://gitlab.com/biomedit/portal/commit/aed9901ef200b0f1e17d4e87179de1fefc8110c7))
* **backend/dtr_creation:** switch log level from info to debug ([d8d54ad](https://gitlab.com/biomedit/portal/commit/d8d54ada88a59fe1bc8e153c4477b44e3b6dfba8))
* **backend/dtr_creation:** wrong salutation for node managers ([0a1837a](https://gitlab.com/biomedit/portal/commit/0a1837acf6aec233bad8f17e5cdb5cec210821ac))

## 1.0.0 (2020-12-02)

### Bug Fixes

* make ip_address_ranges consistent in /backend/projects/ ([f87bd93](https://gitlab.com/biomedit/portal/commit/f87bd93d99bd6eece96968f0209c1dedadcb8393))
