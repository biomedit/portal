# Development

Requirements:

- Python == 3.13
- NodeJS == 22
- Rust >= 1.80
- Docker Engine >= 17.04.0
- PostgreSQL >= 13

## Setup

### Configuration

The service configuration is stored in configuration files and environment variables.

- `web/.config.json` (or `web/.config.toml`) and `web/.env_local` for the backend
  configuration.
- `frontend/.env` for the frontend configuration.

Copy `web/.config.json.sample` (or `web/.config.toml`) to `web/.config.json`
(or `web/.config.toml`) and edit the settings.
The other configuration files should work without modification.

### Install dependencies

- Create and activate a Python virtual environment:

  `python3 -m venv .venv && source .venv/bin/activate`

- Install requirements:

  `pip install -r web/requirements-dev.txt ./portal-ext`

- Install testing dependencies to get code completion in
  your IDE install when working with the testing modules
  (this is not necessary for running tests):

  `pip install -r web/requirements-test.txt`

- Install [nodejs](https://nodejs.org) or install
  [nvm (Node Version Manager)](https://github.com/nvm-sh/nvm),
  to manage node environments without interfering with the system's node
  installation.

  - `nvm install --lts` will _install_ the latest LTS version
  - `nvm use --lts` will _use_ the latest LTS version

- Confirm that a compatible `node` version is installed: `node --version`
- Install/update the Node packages:

  ```bash
  cd frontend
  npm install
  ```

### Database and migrations

- Portal requires a PostgreSQL database to store data.
- Use a remote database or run a local PostgreSQL instance.
  For example, you can use Docker or Podman:

  ```bash
  # Docker
  docker run --name portal-postgres -e POSTGRES_PASSWORD=postgres \
  -p 5432:5432 -d docker.io/library/postgres:17
  # Podman
  podman run --name portal-postgres -e POSTGRES_PASSWORD=postgres \
  -p 5432:5432 -d docker.io/library/postgres:17
  ```

  Note, the above command also creates a database named `postgres` and user
  `postgres` with password `postgres`, which correspond to values stored
  in `web/.env_local`.

- To reset/recreate the database associated with Portal, remove the current container:

  ```bash
  # Docker
  docker stop portal-postgres && docker rm -v portal-postgres
  # Podman
  podman stop portal-postgres && podman rm -v portal-postgres
  ```

  Now, crate a new container as described above.

- Run the database migrations:

  ```bash
  ./manage.py migrate
  ```

- If you run Portal for the first time, you might want to populate the database
  with useful entities, such as nodes, data providers and users for all
  different roles. Simply copy this
  [snippet](https://gitlab.com/biomedit/portal/-/snippets/3678281),
  save it in `/web/scripts/populate_database.py` and run it like this:

  ```bash
  ./manage.py runscript populate_database
  ```

- Create an admin user for the Portal backend:

  ```bash
  ./manage.py createsuperuser
  ```

### Run the service

- Backend:

  ```bash
  cd web
  ./manage.py runserver localhost:8000
  ```

- Frontend:

  ```bash
  cd frontend
  npm start
  ```

Note:

- File changes in the backend are automatically detected and the corresponding
  services are restated.
- The Django admin can be accessed at <http://localhost:8000/backend/admin>
- The REST API can be accessed at <http://localhost:8000/backend>
- The frontend is available at <http://localhost:3000>
- Additional information about frontend can be found in the
  [frontend documentation](frontend/README.md).

## Testing

For detailed instructions on testing Portal see [testing](test.md).

## Automated code generation

When changing the API or database models it's necessary to generate code that
reflects those changes.

### Generating the backend API in the frontend based on the backend

Every time you change something in the backend that involves changes in the
API, you also need to re-generate the backend API in the frontend.

- Re-generate the backend API in the frontend:

  ```bash
  cd frontend
  npm run generate-api
  ```

- Commit the changes inside `frontend/src/api/generated` in the same commit as
  your change in the backend.

## Using Danger

The project uses [Danger JS](https://danger.systems/js/) to automate some code
review chores.

The following setup is specific to **GitLab** and will change if your
repository is hosted by another service.

1. From the group or project's home page, navigate to **Settings** -> **Access Tokens**.
2. Create an access token named `danger_bot` with **Reporter** role and **api** scope.
3. Copy the newly created access token - it will not be possible to access it
   again after leaving the page.
4. Navigate to group or project's **Settings** -> **CI/CD**.
5. Expand the **Variables** section.
6. Create a group or project variable named `DANGER_GITLAB_API_TOKEN` and set the access token as
   its value.
7. Create a variable named `TECHNICAL_COORDINATOR` and set the username of your
   team's technical coordinator as its value. A comma separated list of
   usernames will also work, in case you want more than one person to be
   notified. Make sure the variable is not protected, so that it is visible on
   feature branches.
8. Optionally, create a variable named
   `TECHNICAL_COORDINATOR_NOTIFICATION_LABEL`. Its value should be the name of
   the label that if present should trigger the notification to the technical
   coordinator. If you don't do this, the default `UX/UI` label will be used.

## CI/CD

This project uses GitLab CI/CD for testing, building, and releasing new
versions of the software. The CI jobs runs on both public (provided by
GitLab) and private (self-hosted) runners. We currently use `docker` and
`shell` types of runners (see
[executors](https://docs.gitlab.com/runner/executors/)).

In order to register a new private runner follow the
[official instructions](https://docs.gitlab.com/runner/register/).
Note:

- If you want to share the same runner for multiple projects register it as a
  [group runner](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#group-runners).
- Make sure that the host machine has sufficient CPU and memory resources to
  efficiently run the desired jobs.
- Add the required tag (e.g. `shell`) during the runner registration.
- Runners do **not** limit the number of concurrent jobs that they handle.
  In order to not exhaust the available resources on the host machine you
  should set a limit on concurrent jobs
  ([runners section of config.toml](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section)).
  You can also set the maximum number of concurrent jobs across all registered
  runners on a given
  host ([global section of config.toml](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section)).

## OAUTH

This project may act as [OIDC](https://en.wikipedia.org/wiki/OpenID) provider.
Associated configuration section from `web/.config.toml` looks as following:

```toml
[oauth]
active = true
issuer = "http://localhost:8000/oauth"
expiration_time_seconds = 3_600
alg = "RS256"

  [oauth.jwt_key]
  public_path = "/tmp/public.pem"
  private_path = "/tmp/private.pem"

```

In order to use **OIDC**, you will need a
[TSL](https://en.wikipedia.org/wiki/Transport_Layer_Security) key pair:

```bash
openssl genrsa -out private.pem 4096                 # To generate a new private key.
openssl rsa -in private.pem -pubout -out public.pem  # To generate a public key out of the private one.
```

After generation, just specify the full path to the keys in the above
configuration.

## Testing email sending locally

1. In settings.py make these temporary changes (locally only)

```python
# Email
#if DEBUG:
#    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
#elif CONFIG.email is None:
if CONFIG.email is None:
    raise RuntimeError("Email configuration is required in production")
else:
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    EMAIL_HOST = CONFIG.email.host
    EMAIL_PORT = CONFIG.email.port
    EMAIL_HOST_USER = CONFIG.email.user
    EMAIL_HOST_PASSWORD = CONFIG.email.password
    EMAIL_USE_TLS = CONFIG.email.use_tls
    EMAIL_TIMEOUT = 60
    DEFAULT_FROM_EMAIL = CONFIG.email.from_address
```

2. Set up mail crab locally. More here:
   https://github.com/tweedegolf/mailcrab

```
docker run --rm -p 1080:1080 -p 1025:1025 marlonb/mailcrab:latest
```

3. Change your local config.json

```json
"email": {
      "host": "127.0.0.1",
      "port": 1025,
      "use_tls": false,
      "user": "",
      "password": "",
      "from_address": "noreply@portal.localhost",
      "subject_prefix": ""
    },
```
