# Testing

## Running backend tests

- Install `tox`.
- Run `tox` to execute tests with the default configuration.
- Customize test runtime. For example:

  - Run tests from a specific file/directory in the Python 3.13 environment
    using 8 parallel processes.

    ```bash
    tox -e py313 -- -n 8 projects/tests/views
    ```

  - Run only a single test

    ```bash
    tox -e py313 -- projects/tests/views/test_project.py::test_project_get_queryset
    ```

  - Filter by name to only run specific tests

    ```bash
    tox -e py313 -- -k test_get_unauthenticated
    # Name filter can also be combined with path
    tox -e py313 -- projects/tests/views -k test_get_unauthenticated
    ```

  - Run tests in a watch mode. Failing tests are re-run when a file in the
    project changes. A full test run is performed when all previously failing
    tests are fixed.

    ```bash
    tox -e py313 -- --looponfail
    ```

  Note: arguments passed after `--` are passed directly to `pytest` not `tox`.

## Periodic tasks

Executing periodic tasks requires running an additional service.
When developing locally run the following command in addition to `runserver`

```bash
./manage.py procrastinate worker
```

You can also use the `local-prod` deployment.

The scheduling and execution of tasks can be monitored in the worker logs.
Additionally, the Django admin interface provides a task overview
(finished and failed).

## Performance testing

### Database setup

The overall performance of the application depends on the performance of the
database engine. For this reason is's important to run tests with Postgres,
the database engine used in a production deployment.

Follow the instructions below for a quick local Postgres deployment.
Note, you can initialize the database with an existing database dump
(e.g. from a staging deployment).

```bash
docker run --name postgres-perf-test -e POSTGRES_PASSWORD=test -p 5432:5432 -d postgres:13
gzip -dc <dbdump>.sql.gz | docker exec -i postgres-perf-test psql -U postgres
```

Update configuration to use the Postgres database. Append `web/.env.local` with:

```bash
DB_NAME=postgres
DB_USER=postgres
DB_PASS=test
DB_HOST=localhost
DB_PORT=5432
```

### Profiling

From [python.org](https://docs.python.org/3.12/library/profile.html):

> A profile is a set of statistics that describes how often and for how long
> various parts of the program executed. These statistics can be formatted
> into reports via the pstats module.

You can obtain profiles for individual requests using
[`runprofileserver`](https://github.com/django-extensions/django-extensions/blob/main/docs/runprofileserver.rst),
see [installation instructions](https://github.com/django-extensions/django-extensions).
After installation and initial setup you can start the web server
in the profiler mode.

```bash
mkdir /tmp/portal-profile-data
./manage.py runprofileserver --use-cprofile --prof-path=/tmp/portal-profile-data
```

Each request profile is stored in the `/tmp/portal-profile-data` folder.
Profiles can be analyzed in many ways (e.g. with built-in `pstats` module).
However, a quick overview can be obtained by visualizing the results, e.g.
with [`snakeviz`](https://github.com/jiffyclub/snakeviz).

```bash
snakeviz <profile_name.prof
```

Note: to avoid executing unnecessary code needed for rendering HTML, request
`json` output, e.g. <http://localhost:8000/backend/data-transfer/?format=json>.

### Analyzing database queries

To get more insight into how many/what queries are made for a given API request
use [Django Debug Toolbar](https://github.com/jazzband/django-debug-toolbar),
[installation instructions](https://django-debug-toolbar.readthedocs.io/en/latest/installation.html).
