import pytest

from projects.models.data_transfer import DataTransfer
from scripts.add_default_legal_basis import run, DEFAULT_LEGAL_BASIS


@pytest.mark.django_db
@pytest.mark.parametrize(
    "purpose, legal_basis, has_approval_group, should_be_updated",
    (
        (DataTransfer.PRODUCTION, "", True, True),
        (DataTransfer.TEST, "", True, False),
        (DataTransfer.PRODUCTION, "do-not-overwrite-me.pdf", True, False),
        (DataTransfer.PRODUCTION, "", False, False),
    ),
)
def test_add_default_legal_basis(
    purpose,
    legal_basis,
    has_approval_group,
    should_be_updated,
    data_transfer_factory,
    project_factory,
    group_factory,
):
    data_transfer = data_transfer_factory(
        purpose=purpose,
        legal_basis=legal_basis,
        project=project_factory(
            legal_approval_group=(group_factory() if has_approval_group else None)
        ),
    )

    run()

    data_transfer.refresh_from_db()
    assert (data_transfer.legal_basis == DEFAULT_LEGAL_BASIS) is should_be_updated
