import pytest

from django.contrib.auth.models import Group

from identities.models import GroupProfile
from projects.tests.factories import make_data_specification_approval_group

from scripts.add_data_specification_approval_group import run


@pytest.mark.django_db
@pytest.mark.parametrize("group_exist", (False, True))
def test_add_data_specification_approval_group(group_exist):
    if group_exist:
        make_data_specification_approval_group()

    run()

    groups = Group.objects.all()
    assert len(groups) == 1
    assert all(
        group.profile.role == GroupProfile.Role.DATA_SPECIFICATION_APPROVAL
        for group in groups
    )
