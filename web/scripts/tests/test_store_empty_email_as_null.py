import pytest

from django.contrib.auth import get_user_model

from scripts.store_empty_emails_as_null import run

User = get_user_model()


@pytest.mark.django_db
def test_store_empty_emails_as_null(user_factory):
    user_factory(email=None)
    user_factory(email="email@example.com")

    # Hack to create a user with email="" without triggering pre_save signal.
    user_factory(username="chuck_norris")
    User.objects.filter(username="chuck_norris").update(email="")

    assert User.objects.filter(email="").count() == 1
    assert User.objects.filter(email=None).count() == 1

    run()

    assert User.objects.filter(email="").count() == 0
    assert User.objects.filter(email=None).count() == 2
