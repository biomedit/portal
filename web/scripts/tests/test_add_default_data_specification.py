import pytest

from projects.models.data_transfer import DataTransfer
from scripts.add_default_data_specification import run, DEFAULT_DATA_SPECIFICATION


@pytest.mark.django_db
@pytest.mark.parametrize(
    "purpose, data_specification, has_approval_group, should_be_updated",
    (
        (DataTransfer.PRODUCTION, None, True, True),
        (DataTransfer.TEST, None, True, False),
        (DataTransfer.PRODUCTION, "https://do-not-overwrite-me.com", True, False),
        (DataTransfer.PRODUCTION, None, False, False),
    ),
)
def test_add_default_data_specification(
    purpose,
    data_specification,
    has_approval_group,
    should_be_updated,
    data_transfer_factory,
    project_factory,
    group_factory,
):
    data_transfer = data_transfer_factory(
        purpose=purpose,
        data_specification=data_specification,
        project=project_factory(
            data_specification_approval_group=(
                group_factory() if has_approval_group else None
            )
        ),
    )

    run()

    data_transfer.refresh_from_db()
    assert (
        data_transfer.data_specification == DEFAULT_DATA_SPECIFICATION
    ) is should_be_updated
