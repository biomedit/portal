import logging

from django.contrib.auth.models import Group
from django.db.transaction import atomic
from guardian.shortcuts import assign_perm

from identities.models import (
    GroupProfile,
    INTERNAL_DATA_SPECIFICATION_APPROVAL_GROUP_NAME,
)
from projects.models.const import APP_PERM_GROUP_APPROVAL_STATUS


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    try:
        data_specification_approval_group = Group.objects.get(
            name=INTERNAL_DATA_SPECIFICATION_APPROVAL_GROUP_NAME
        )
        logger.info("Skipping %s", data_specification_approval_group.name)
    except Group.DoesNotExist:
        data_specification_approval_group = Group.objects.create(
            name=INTERNAL_DATA_SPECIFICATION_APPROVAL_GROUP_NAME
        )
        GroupProfile.objects.create(
            group=data_specification_approval_group,
            role=GroupProfile.Role.DATA_SPECIFICATION_APPROVAL,
        )
        assign_perm(APP_PERM_GROUP_APPROVAL_STATUS, data_specification_approval_group)
        logger.info("Created %s", data_specification_approval_group.name)

    return 0
