import logging

from django.contrib.auth.models import Group
from django.db.transaction import atomic

from projects.models.node import Node
from identities.models import GroupProfile


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    for node in Node.objects.all():
        for name_pattern, role in [
            ("Node Viewer", GroupProfile.Role.NODE_VIEWER),
            ("Node Admin", GroupProfile.Role.NODE_ADMIN),
            ("Node Manager", GroupProfile.Role.NODE_MANAGER),
        ]:
            try:
                group = Group.objects.get(
                    name=f"{node.name} {name_pattern}", profile__role=None
                )
                group_profile, _ = GroupProfile.objects.get_or_create(group=group)
                group_profile.role = role
                group_profile.role_entity = node
                group_profile.save()

                logger.info("Updated group %s", group.name)

            except Group.DoesNotExist:
                logger.info("Skipping %s for %s", name_pattern, node.name)

    return 0
