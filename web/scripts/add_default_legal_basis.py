import logging

from django.db.transaction import atomic
from projects.models.data_transfer import DataTransfer


DEFAULT_LEGAL_BASIS = "example.pdf"


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    for data_transfer in DataTransfer.objects.filter(
        purpose=DataTransfer.PRODUCTION,
        legal_basis="",
        project__legal_approval_group__isnull=False,
    ):
        data_transfer.legal_basis = DEFAULT_LEGAL_BASIS
        data_transfer.save()
        logger.info("Updated data transfer %s", data_transfer.id)

    return 0
