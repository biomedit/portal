import logging

from django.contrib.auth import get_user_model
from django.db.transaction import atomic

User = get_user_model()


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    for user in User.objects.filter(email=""):
        logger.log(logging.INFO, "Setting email to None for %s...", user.username)
        user.email = None
        user.save()
        logger.log(logging.INFO, "Email set to None for %s", user.username)

    return 0
