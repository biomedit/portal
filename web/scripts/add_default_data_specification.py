import logging

from django.db.transaction import atomic
from projects.models.data_transfer import DataTransfer


DEFAULT_DATA_SPECIFICATION = "https://example.com"


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    for data_transfer in DataTransfer.objects.filter(
        purpose=DataTransfer.PRODUCTION,
        data_specification__isnull=True,
        project__data_specification_approval_group__isnull=False,
    ):
        data_transfer.data_specification = DEFAULT_DATA_SPECIFICATION
        data_transfer.save()
        logger.info("Updated data transfer %s", data_transfer.id)

    return 0
