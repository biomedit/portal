import logging

from django.contrib.auth.models import Group
from django.db.transaction import atomic


from projects.models.data_provider import DataProvider
from identities.models import GroupProfile


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    for data_provider in DataProvider.objects.all():
        for name_pattern, role in [
            ("DP Coordinator", GroupProfile.Role.DATA_PROVIDER_COORDINATOR),
            ("DP Data Engineer", GroupProfile.Role.DATA_PROVIDER_DATA_ENGINEER),
            ("DP Security Officer", GroupProfile.Role.DATA_PROVIDER_SECURITY_OFFICER),
            ("DP Manager", GroupProfile.Role.DATA_PROVIDER_MANAGER),
            ("DP Technical Admin", GroupProfile.Role.DATA_PROVIDER_TECHNICAL_ADMIN),
            ("DP Viewer", GroupProfile.Role.DATA_PROVIDER_VIEWER),
        ]:
            try:
                group = Group.objects.get(
                    name=f"{data_provider.code} {name_pattern}", profile__role=None
                )
                group_profile, _ = GroupProfile.objects.get_or_create(group=group)
                group_profile.role = role
                group_profile.role_entity = data_provider
                group_profile.save()

                logger.info("Updated group %s", group.name)

            except Group.DoesNotExist:
                logger.info("Skipping %s for %s", name_pattern, data_provider.name)

    return 0
