import logging

from django.contrib.auth.models import Group
from django.db.transaction import atomic
from guardian.shortcuts import assign_perm

from identities.models import GroupProfile, INTERNAL_LEGAL_APPROVAL_GROUP_NAME
from projects.models.node import Node, get_node_role_group
from projects.models.const import APP_PERM_GROUP_APPROVAL_STATUS

LEGAL_APPROVAL_GROUP_SUFFIX = "Legal Approval Group"


def get_node_legal_approval_group_name(node):
    return f"{node.name} {LEGAL_APPROVAL_GROUP_SUFFIX}"


@atomic
def run() -> int:
    logger = logging.getLogger(__name__)

    try:
        internal_legal_approval_group = Group.objects.get(
            name=INTERNAL_LEGAL_APPROVAL_GROUP_NAME
        )
        logger.info("Skipping %s", internal_legal_approval_group.name)
    except Group.DoesNotExist:
        internal_legal_approval_group = Group.objects.create(
            name=INTERNAL_LEGAL_APPROVAL_GROUP_NAME
        )
        GroupProfile.objects.create(
            group=internal_legal_approval_group,
            description="Ethical, Legal and Social Implications",
            role=GroupProfile.Role.ELSI,
        )
        assign_perm(APP_PERM_GROUP_APPROVAL_STATUS, internal_legal_approval_group)
        logger.info("Created %s", internal_legal_approval_group.name)

    for node in Node.objects.all():
        group_name = get_node_legal_approval_group_name(node)
        try:
            group = get_node_role_group(node, GroupProfile.Role.ELSI)
            logger.info("Skipping %s", group.name)
        except Group.DoesNotExist:
            group = Group.objects.create(name=group_name)
            GroupProfile.objects.create(
                group=group,
                description=f"Legal approval group for {node.name} node",
                role=GroupProfile.Role.ELSI,
                role_entity=node,
            )
            assign_perm(APP_PERM_GROUP_APPROVAL_STATUS, group)
            logger.info("Created %s", group.name)

    return 0
