import os

from django.core.files.storage import FileSystemStorage


class OverwriteStorage(FileSystemStorage):
    """`FileSystemStorage` extension which favors overwriting over preserving,
    avoiding renaming of the file."""

    def get_available_name(
        self,
        name,
        max_length=None,  # noqa: ARG002 unused-method-argument
    ):
        if self.exists(name):
            os.remove(os.path.join(self.location, name))
        return name
