import logging
import json

from authlib.integrations.base_client import (
    MismatchingStateError,
    MissingRequestTokenError,
    MissingTokenError,
)

from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import AbstractBaseUser
from django.http import HttpResponseForbidden
from django.shortcuts import redirect, render
from django.urls import reverse

from authlib.integrations.requests_client import OAuth2Session
from authlib.integrations.django_client import OAuth

from rest_framework.authentication import BaseAuthentication, get_authorization_header

from .user_utils import user_from_claims

User = auth.get_user_model()

logger = logging.getLogger(__name__)

oauth = OAuth()
oauth.register(name="identity_provider")


def authenticate(request):
    redirect_uri = request.build_absolute_uri(reverse("oidc-callback"))
    return oauth.identity_provider.authorize_redirect(request, redirect_uri)


def callback(request):
    try:
        token = oauth.identity_provider.authorize_access_token(request)
    except (MismatchingStateError, MissingRequestTokenError, MissingTokenError) as e:
        logger.warning(
            "Token is missing or invalid. User likely tried to login using a URL"
            " including an expired session (in a bookmark). Details: %s",
            e,
            exc_info=True,
        )
        return redirect(settings.LOGIN_REDIRECT_URL)

    try:
        userinfo = oauth.identity_provider.userinfo(token=token)
        user = user_from_claims(username_claim="preferred_username", claims=userinfo)
        if user is not None:
            if not user.is_active:
                logger.warning(
                    f"User '{user.username}' is not active. Could not authenticate."
                )
                context = {
                    "contact_email": settings.CONFIG.notification.contact_form_recipient
                }
                return HttpResponseForbidden(
                    render(
                        request,
                        "inactive_user.html",
                        context,
                    )
                )
            auth.login(
                request, user, backend="django.contrib.auth.backends.ModelBackend"
            )
    except Exception as e:
        logger.error(
            f"Could not fetch userinfo from identity provider with token '{token}' or"
            " create/update user from claims. Details: %s",
            e,
            exc_info=True,
        )
    return redirect(settings.LOGIN_REDIRECT_URL)


class TokenAuthentication(BaseAuthentication):
    def authenticate(self, request) -> tuple[AbstractBaseUser | None, None] | None:
        """
        Check JWT token validity, then extract the preferred_username
        from the payload and return a tuple of (user, None).
        """

        jwt_token = self._get_jwt_token_from_authorization_header(request)
        if not jwt_token:
            # this is important: if there is no authorization header,
            # we need to return None (and not raise an exception)
            return None
        user = self._validate_token_and_get_user(jwt_token)
        if not user:
            return None
        return (user, None)

    def _get_jwt_token_from_authorization_header(self, request) -> str | None:
        auth_header_items = get_authorization_header(request).split()
        prefix = "Bearer".encode()
        if (
            not auth_header_items
            # Prefix should be Bearer
            or auth_header_items[0].lower() != prefix.lower()
            # Prefix should be followed by a JWT token string without any spaces
            or len(auth_header_items) != 2
        ):
            logger.warning("The token is missing or malformed.")
            return None
        return auth_header_items[1]

    def _validate_token_and_get_user(self, token) -> AbstractBaseUser | None:
        session = OAuth2Session(
            client_id=oauth.identity_provider.client_id,
            client_secret=oauth.identity_provider.client_secret,
        )
        try:
            userinfo = session.introspect_token(
                token=token,
                token_type_hint="access_token",  # nosec
                url=oauth.identity_provider.load_server_metadata().get(
                    "introspection_endpoint"
                ),
            )
        except ValueError as e:
            logger.warning(f"Token introspection failed: {e}")
            return None

        decoded_userinfo = json.loads(userinfo.content.decode("utf-8"))
        if not decoded_userinfo.get("active") or not (
            username := decoded_userinfo.get("preferred_username")
        ):
            logger.warning(
                "The token is inactive or is missing the preferred_username attribute."
            )
            return None
        return User.objects.filter(username=username).first()

    def authenticate_header(self, _):
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        return None


def logout(request):
    auth.logout(request)
    provider_logout_endpoint = oauth.identity_provider.server_metadata.get(
        "end_session_endpoint"
    )
    return redirect(provider_logout_endpoint or settings.LOGOUT_REDIRECT_URL)
