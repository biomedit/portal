import factory
from factory.fuzzy import FuzzyChoice

from terminology.models import (
    Terminology,
    Version,
    VersionFile,
)


class VersionFileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VersionFile
        skip_postgeneration_save = True

    file = factory.django.FileField(filename="atc.zip")
    version = factory.SubFactory("terminology.tests.factories.VersionFactory", files=[])

    class Params:
        owl = factory.Trait(file=factory.django.FileField(filename="atc.owl"))
        txt = factory.Trait(file=factory.django.FileField(filename="atc.txt"))


class VersionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Version
        skip_postgeneration_save = True

    name = factory.Sequence(lambda n: f"2021-{n + 1}")
    released = factory.Faker("date")
    status = FuzzyChoice(Version.Status)
    files = factory.RelatedFactoryList(
        VersionFileFactory, factory_related_name="version", size=1
    )
    terminology = factory.SubFactory(
        "terminology.tests.factories.TerminologyFactory", versions=[]
    )


class TerminologyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Terminology
        skip_postgeneration_save = True

    code = factory.Faker("word")
    name = factory.Faker("sentence")
    contact = factory.Faker("sentence")
    description = factory.Faker("text")
    copyright = factory.Faker("text")
    documentation = factory.Faker("text")
    versions = factory.RelatedFactoryList(
        "terminology.tests.factories.VersionFactory",
        factory_related_name="terminology",
        size=10,
    )
