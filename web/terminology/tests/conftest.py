import pytest

from projects.tests.factories import UserFactory, USER_PASSWORD

from .factories import (
    TerminologyFactory,
    VersionFactory,
    VersionFileFactory,
)


@pytest.fixture
def terminology_factory(db):  # noqa: ARG001 unused-function-argument
    return TerminologyFactory


@pytest.fixture
def version_factory(db):  # noqa: ARG001 unused-function-argument
    return VersionFactory


@pytest.fixture
def version_file_factory(db):  # noqa: ARG001 unused-function-argument
    return VersionFileFactory


@pytest.fixture
def basic_user_auth(db, client):  # noqa: ARG001 unused-function-argument
    """Return authenticated basic user"""
    user = UserFactory(basic=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def staff_user_auth(db, client):  # noqa: ARG001 unused-function-argument
    """Return authenticated staff user"""
    user = UserFactory(staff=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()
