import pytest
from django.test import override_settings

from terminology.models import upload_to
from terminology.tests import get_upload_root


@override_settings(UPLOAD_ROOT=get_upload_root())
def test_create_terminology(terminology_factory):
    terminology = terminology_factory()
    assert terminology.code
    assert terminology.versions.count() == 10


@override_settings(UPLOAD_ROOT=get_upload_root())
def test_create_version(version_factory):
    version = version_factory()
    assert version.terminology.code


@override_settings(UPLOAD_ROOT=get_upload_root())
def test_create_version_file(version_file_factory):
    version_file = version_file_factory()
    assert version_file.version


class DictToObject:
    def __init__(self, dictionary):
        def _traverse(key, element):
            if isinstance(element, dict):
                return key, DictToObject(element)
            return key, element

        self.__dict__.update(dict(_traverse(k, v) for k, v in dictionary.items()))


@pytest.mark.parametrize(
    "instance,filename,expectation",
    (
        (
            {"version": {"name": "2021-3", "terminology": {"code": "ATC"}}},
            "tEsT.txt",
            "terminology/atc/2021-3/test.txt",
        ),
        (
            {"version": {"name": "Ökonomie", "terminology": {"code": "ATC"}}},
            "test.OWL",
            "terminology/atc/okonomie/test.owl",
        ),
    ),
)
def test_upload_to(instance, filename, expectation):
    version_file = DictToObject(instance)
    assert upload_to(version_file, filename) == expectation
