import tempfile

APPLICATION_JSON = "application/json"


def get_upload_root():
    # Should be preferably used over 'tempfile.mkdtemp()'
    # as it will be automatically deleted by the OS. We do NOT know though.
    with tempfile.TemporaryDirectory(prefix="uploadtest") as tmpdir:
        return tmpdir
