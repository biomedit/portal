from itertools import groupby
import pytest
from django.test import override_settings
from django.urls import reverse
from rest_framework import status


from terminology.models import Version
from terminology.tests import get_upload_root

VIEW_NAME = "terminology:terminology-list"


def test_list_anonymous(client):
    response = client.get(reverse(VIEW_NAME))
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("basic_user_auth")
def test_list_authenticated(client):
    response = client.get(reverse(VIEW_NAME))
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.usefixtures("basic_user_auth")
@override_settings(UPLOAD_ROOT=get_upload_root())
def test_terminology_json(client, version_factory, version_file_factory):
    version = version_factory()
    owl_file_version = version_file_factory(owl=True, version=version)
    txt_file_version = version_file_factory(txt=True, version=version)
    version.files.add(owl_file_version)
    version.files.add(txt_file_version)
    response = client.get(reverse(VIEW_NAME))
    assert response.status_code == status.HTTP_200_OK
    versions = response.json()[0]["versions"]
    assert len(versions) == 1
    files = versions[0]["files"]
    assert len(files) == 3
    formats = {file["format"] for file in files}
    assert formats == {"ZIP", "RDF/OWL", "TXT"}


@pytest.mark.usefixtures("basic_user_auth")
@override_settings(UPLOAD_ROOT=get_upload_root())
def test_terminologies_sorted(client, terminology_factory):
    # Create three terminologies
    for code in ("ZTC", "ATC", "MZK"):
        terminology_factory(code=code)
    response = client.get(reverse(VIEW_NAME))
    assert response.status_code == status.HTTP_200_OK
    output = [item["code"] for item in response.json()]
    assert output == ["ATC", "MZK", "ZTC"]


@pytest.mark.usefixtures("basic_user_auth")
@override_settings(UPLOAD_ROOT=get_upload_root())
def test_versions_sorted(client, terminology_factory):
    def by_status(versions, active_or_archived):
        return [
            version for version in versions if version["status"] == active_or_archived
        ]

    def assert_sorted_by_name(versions):
        all_version_names = [version["name"] for version in versions]
        assert all_version_names == sorted(all_version_names, reverse=True)

    # Create one terminology and ten versions
    terminology_factory()
    response = client.get(reverse(VIEW_NAME))
    assert response.status_code == status.HTTP_200_OK
    versions = response.json()[0]["versions"]
    actives = by_status(versions, Version.Status.ACTIVE)
    if actives:
        # `ACTIVE` are at the top
        assert versions[0]["status"] == Version.Status.ACTIVE
        assert_sorted_by_name(actives)
    archived = by_status(versions, Version.Status.ARCHIVED)
    if archived:
        # `ARCHIVED` are at the bottom
        assert versions[-1]["status"] == Version.Status.ARCHIVED
        assert_sorted_by_name(archived)
    # Check that ACTIVE at top and ARCHIVED at bottom
    if actives and archived:
        assert len(list(groupby(versions, key=lambda version: version["status"]))) == 2
