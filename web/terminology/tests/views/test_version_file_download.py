from pathlib import Path
from unittest.mock import patch, mock_open

import pytest
from django.urls import reverse
from django.test import override_settings
from rest_framework import status

from projects.tests.factories import UserFactory, USER_PASSWORD

from terminology.models import VersionFile, DownloadedFile
from terminology.tests import APPLICATION_JSON, get_upload_root
from terminology.tests.factories import VersionFileFactory

VIEW_NAME = "terminology:versionfiledownload-list"
VIEW_URL = reverse(VIEW_NAME)
AUTHENTICATED_FORBIDDEN_METHODS = ("get", "put", "delete", "patch")


@pytest.mark.django_db
class TestVersionFileDownloadView:
    @pytest.fixture(autouse=True)
    def before_each(self):
        self.staff = UserFactory(staff=True)
        self.basic_user = UserFactory(
            basic=True, username="user-basic-2", email="foo2@bar.org"
        )
        self.version_file = VersionFileFactory()
        self.data = {"id": self.version_file.id}

    def test_forbidden_anonymous(self, client):
        for method in (*AUTHENTICATED_FORBIDDEN_METHODS, "post"):
            response = getattr(client, method)(
                VIEW_URL, self.data, content_type=APPLICATION_JSON
            )
            assert response.status_code == status.HTTP_403_FORBIDDEN
        assert DownloadedFile.objects.count() == 0

    def test_forbidden_authenticated(self, client):
        # no one can use any method except for POST
        for user in (self.basic_user, self.staff):
            client.login(username=user.username, password=USER_PASSWORD)
            for method in AUTHENTICATED_FORBIDDEN_METHODS:
                response = getattr(client, method)(
                    VIEW_URL, self.data, content_type=APPLICATION_JSON
                )
                assert response.status_code == status.HTTP_403_FORBIDDEN
            client.logout()
        assert DownloadedFile.objects.count() == 0

    @pytest.mark.usefixtures("basic_user_auth")
    @override_settings(UPLOAD_ROOT=get_upload_root())
    def test_post(self, client):
        m = mock_open()
        original_file = self.version_file.file
        original_version = self.version_file.version
        count = 0
        with patch("__main__.open", m):
            for data in (
                self.data,
                {
                    "id": self.version_file.id,
                    "file": "different_file",
                    "version": "different_version",
                },
            ):
                assert DownloadedFile.objects.count() == count
                count += 1
                response = client.post(VIEW_URL, data, content_type=APPLICATION_JSON)
                assert response.status_code == status.HTTP_200_OK
                assert response.headers.get("Content-Type") == "application/zip"
                assert (
                    response.headers.get("Content-Disposition")
                    == f'attachment; filename="{Path(original_file.name).name}"'
                )
                assert DownloadedFile.objects.count() == count
                # POST is read only
                version_file = VersionFile.objects.get(id=self.version_file.id)
                assert version_file.file == original_file
                assert version_file.version == original_version
