import pytest
from django.urls import reverse
from rest_framework import status

from projects.tests.factories import UserFactory, USER_PASSWORD

from terminology.models import DownloadedFile
from terminology.tests.factories import VersionFileFactory

VIEW_NAME = "terminology:downloadedfile-list"
VIEW_URL = reverse(VIEW_NAME)
AUTHENTICATED_FORBIDDEN_METHODS = ("post", "put", "delete", "patch")


@pytest.mark.django_db
class TestDownloadedFileView:
    @pytest.fixture(autouse=True)
    def before_each(self):
        self.staff = UserFactory(staff=True, username="user-staff-2")
        self.basic_user = UserFactory(basic=True)
        self.version_file = VersionFileFactory()

    def test_forbidden_anonymous(self, client):
        for method in (*AUTHENTICATED_FORBIDDEN_METHODS, "get"):
            response = getattr(client, method)(VIEW_URL)
            assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_forbidden_authenticated(self, client):
        # no one can use any method except for POST
        for user in (self.basic_user, self.staff):
            client.login(username=user.username, password=USER_PASSWORD)
            for method in AUTHENTICATED_FORBIDDEN_METHODS:
                response = getattr(client, method)(VIEW_URL)
                assert response.status_code == status.HTTP_403_FORBIDDEN
            client.logout()

    @pytest.mark.usefixtures("staff_user_auth")
    def test_staff_get(self, client):
        file = DownloadedFile.objects.create(
            user=self.basic_user, version_file=self.version_file
        )
        response = client.get(VIEW_URL)
        assert response.status_code == status.HTTP_200_OK
        body = response.json()
        assert len(body) == 1
        response_file = body[0]
        assert response_file["id"] == file.id
        assert response_file["user"] == self.basic_user.email
        assert response_file["created"]
        assert response_file["version_file"]
        assert response_file["version_file"]["id"] == self.version_file.id
