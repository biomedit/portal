from projects.permissions import IsStaff
from rest_framework import permissions


class VersionFileDownloadPermission(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.method == "POST"

    def has_object_permission(
        self,
        request,  # noqa: ARG002 unused-method-argument
        view,  # noqa: ARG002 unused-method-argument
        obj,  # noqa: ARG002 unused-method-argument
    ):
        # objects cannot be accessed or modified
        return False


class DownloadedFilePermission(IsStaff):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.method == "GET"

    def has_object_permission(self, request, view, _):
        return self.has_permission(request, view)
