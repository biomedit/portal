from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.utils.html import format_html

from terminology.models import Terminology, Version, VersionFile

GREY = "aaaaaa"
BLACK = "000000"


@admin.register(Terminology)
class TerminologyAdmin(ModelAdmin):
    list_display = ("code", "name")


class VersionFileInline(admin.TabularInline):
    model = VersionFile


@admin.register(Version)
class VersionAdmin(ModelAdmin):
    list_display = ("__str__", "released", "colored_status")
    list_filter = ("terminology__code",)
    inlines = (VersionFileInline,)

    @staticmethod
    @admin.display(description="Status")
    def colored_status(obj):
        return format_html(
            '<span style="color: #{};">{}</span>',
            BLACK if obj.status == Version.Status.ACTIVE else GREY,
            obj.status,
        )
