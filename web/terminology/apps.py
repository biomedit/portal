from django.apps import AppConfig

APP_NAME = "terminology"


class TerminologyConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = APP_NAME
