from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter

from terminology import views
from terminology.apps import APP_NAME

router = DefaultRouter()
router.register(r"terminology", views.TerminologyViewSet, "terminology")
router.register(
    r"versionfile/download", views.VersionFileDownloadViewSet, "versionfiledownload"
)
router.register(r"downloaded_file", views.DownloadedFileViewSet, "downloadedfile")

# The API URLs are now determined automatically by the router.
app_name = APP_NAME
urlpatterns = [
    re_path(r"^", include(router.urls)),
]
