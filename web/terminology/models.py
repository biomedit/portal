import os
from datetime import date

from django.contrib.auth import get_user_model
from django.db import models
from django.dispatch import receiver
from django.utils.text import get_valid_filename, slugify
from django.utils.translation import gettext_lazy as _
from django_drf_utils.models import CodeField, NameField, CreatedMixin
from simple_history.models import HistoricalRecords

User = get_user_model()

VERSION_FILES_ROOT = "terminology"


class Terminology(models.Model):
    code = CodeField(
        lower=False,
        help_text="Terminology code and unique identifier. Must be defined.",
    )
    name = NameField(help_text="Terminology name. Must be defined.", unique=False)
    contact = models.CharField(
        max_length=256,
        blank=False,
        help_text="Contact person and/or organization. Must be defined.",
    )
    description = models.TextField(
        blank=False, help_text="Longer description of a terminology. Must be defined."
    )
    copyright = models.TextField(
        blank=True, help_text="Copyright notice when using a specific terminology."
    )
    documentation = models.TextField(
        blank=True,
        help_text="Further information/documentation on a given terminology.",
    )
    confirmation_checkbox_text = models.CharField(
        blank=True,
        max_length=512,
        help_text=(
            "Text to show next to the checkbox the user needs to tick before being able"
            " to download the file. If left empty, it will NOT show a dialog when the"
            " user attempts to download a file."
        ),
    )
    confirmation_description = models.TextField(
        blank=True,
        help_text=(
            "Additional text to show below the checkbox the user needs to tick before"
            " being able to download the file. If you specify text here, you MUST also"
            " specify text for the checkbox."
        ),
    )
    history = HistoricalRecords()

    class Meta:
        verbose_name_plural = "Terminologies"

    def __str__(self):
        return str(self.code)


class Version(models.Model):
    name = NameField(help_text="Version name. Must be defined.", unique=False)
    released = models.DateField(
        help_text=(
            "Date at which a given terminology file has been released. Must be defined."
        ),
        default=date.today,
        verbose_name="Release Date",
    )
    terminology = models.ForeignKey(
        Terminology, on_delete=models.CASCADE, related_name="versions"
    )

    class Status(models.TextChoices):
        ACTIVE = "ACTIVE", _("Active")
        ARCHIVED = (
            "ARCHIVED",
            _("Archived"),
        )

    status = models.CharField(
        max_length=8, choices=Status.choices, default=Status.ACTIVE
    )

    class Meta:
        constraints = (
            models.UniqueConstraint(
                fields=("name", "terminology"), name="unique_version_terminology"
            ),
        )

    def __str__(self):
        return f"{self.terminology.code} / {self.name}"


def upload_to(instance, filename):
    version = instance.version
    return os.path.join(
        VERSION_FILES_ROOT,
        version.terminology.code.lower(),
        slugify(version.name),
        get_valid_filename(filename).lower(),
    )


class VersionFile(models.Model):
    file = models.FileField(
        upload_to=upload_to,
        help_text=(
            "Versioned and terminology specific file to download. "
            "Must be defined. If the file already exists, then it will get overwritten."
        ),
        verbose_name="Download File",
    )
    version = models.ForeignKey(Version, on_delete=models.CASCADE, related_name="files")

    class Meta:
        constraints = (
            models.UniqueConstraint(
                fields=("file", "version"),
                name="unique_file_version",
            ),
        )

    def __str__(self):
        return f"{str(self.version)} / {os.path.basename(self.file.name)}"


class DownloadedFile(CreatedMixin):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    version_file = models.ForeignKey(VersionFile, on_delete=models.CASCADE)

    class Meta:
        ordering = ("-created",)


def _delete_file(file):
    file.storage.delete(file.name)


@receiver(models.signals.post_delete, sender=VersionFile)
def delete_file(
    sender,  # noqa: ARG001 unused-function-argument
    instance: VersionFile,
    *args,  # noqa: ARG001 unused-function-argument
    **kwargs,  # noqa: ARG001 unused-function-argument
):
    _delete_file(instance.file)
