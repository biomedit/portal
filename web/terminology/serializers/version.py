from rest_framework import serializers

from terminology.models import Version
from terminology.serializers.version_file import VersionFileSerializer


class VersionSerializer(serializers.ModelSerializer):
    files = VersionFileSerializer(many=True)

    class Meta:
        model = Version
        exclude = ("terminology",)
