import os

from rest_framework import serializers

from terminology.models import VersionFile


class VersionFileSerializer(serializers.ModelSerializer):
    format = serializers.SerializerMethodField()
    filename = serializers.SerializerMethodField()
    confirmation_checkbox_text = serializers.SerializerMethodField()
    confirmation_description = serializers.SerializerMethodField()

    class Meta:
        model = VersionFile
        exclude = ("version", "file")

    @staticmethod
    def get_format(version_file: VersionFile):
        ext = os.path.splitext(version_file.file.path)[1][1:].upper()
        if ext in ("TTL", "OWL"):
            return f"RDF/{ext}"
        return ext

    @staticmethod
    def get_filename(version_file: VersionFile):
        return os.path.basename(version_file.file.name)

    @staticmethod
    def get_confirmation_checkbox_text(version_file: VersionFile):
        return version_file.version.terminology.confirmation_checkbox_text

    @staticmethod
    def get_confirmation_description(version_file: VersionFile):
        return version_file.version.terminology.confirmation_description


class VersionFileDownloadSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = VersionFile
        fields = ("id",)
