from django.contrib.auth import get_user_model
from rest_framework import serializers

from terminology.models import DownloadedFile

User = get_user_model()


class DownloadedFileSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(
        slug_field="email",
        queryset=User.objects.all(),
    )

    class Meta:
        model = DownloadedFile
        read_only_fields = ("id", "created", "user", "version_file")
        fields = read_only_fields
        depth = 3
