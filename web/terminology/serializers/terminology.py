from rest_framework import serializers

from terminology.models import Terminology
from terminology.serializers.version import VersionSerializer


def sort_versions(versions):
    return sorted(
        sorted(versions, key=lambda version: version["name"].lower(), reverse=True),
        key=lambda version: version["status"],
    )


class TerminologySerializer(serializers.ModelSerializer):
    versions = VersionSerializer(many=True)

    class Meta:
        model = Terminology
        fields = "__all__"

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["versions"] = sort_versions(response["versions"])
        return response
