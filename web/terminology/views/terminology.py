from projects.permissions import ReadOnly
from rest_framework import viewsets
from rest_framework.schemas.openapi import AutoSchema

from terminology.models import Terminology
from terminology.serializers import TerminologySerializer


class TerminologySchema(AutoSchema):
    """Because `AutoSchema` returns `Terminologys` as `Terminology` plural"""

    def get_operation_id_base(
        self,
        path,  # noqa: ARG002 unused-method-argument
        method,
        action,
    ):
        return (
            Terminology._meta.verbose_name_plural
            if action == "list" and method == "GET"
            else Terminology.__name__
        )


class TerminologyViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = TerminologySerializer
    permission_classes = (ReadOnly,)
    queryset = Terminology.objects.all().order_by("code")
    schema = TerminologySchema()
