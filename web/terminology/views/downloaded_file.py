from rest_framework import viewsets

from terminology.models import DownloadedFile
from terminology.permissions import DownloadedFilePermission
from terminology.serializers import DownloadedFileSerializer


class DownloadedFileViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DownloadedFileSerializer
    permission_classes = (DownloadedFilePermission,)
    queryset = DownloadedFile.objects.all()
