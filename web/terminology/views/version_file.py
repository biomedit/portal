from django.http import FileResponse
from rest_framework import viewsets, mixins
from rest_framework.schemas.openapi import AutoSchema

from terminology.models import VersionFile, DownloadedFile
from terminology.permissions import VersionFileDownloadPermission
from terminology.serializers.version_file import (
    VersionFileDownloadSerializer,
)


class VersionFileDownloadSchema(AutoSchema):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def get_responses(self, path, method):  # noqa: ARG002 unused-method-argument
        responses = super().get_responses(path, method)
        if method.lower() == "post":
            responses["200"] = {
                "description": "File download",
                "content": {
                    "application/zip": {
                        "schema": {"type": "string", "format": "binary"}
                    },
                    "text/turtle": {"schema": {"type": "string", "format": "text"}},
                    "application/rdf+xml": {
                        "schema": {"type": "string", "format": "text"}
                    },
                },
            }
        return responses


class VersionFileDownloadViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = VersionFileDownloadSerializer
    permission_classes = (VersionFileDownloadPermission,)
    queryset = VersionFile.objects.all()
    schema = VersionFileDownloadSchema()

    def create(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        """Download file of a version (read-only POST)"""
        version_file = VersionFile.objects.get(id=request.data["id"])
        DownloadedFile.objects.create(user=request.user, version_file=version_file)
        response = FileResponse(open(version_file.file.path, "rb"), as_attachment=True)
        return response
