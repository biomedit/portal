from django.conf.urls import include
from django.urls import re_path
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedSimpleRouter


from . import views
from .apps import APP_NAME

router = DefaultRouter()
router.register(r"nodes", views.NodeViewSet, "node")
router.register(r"projects", views.ProjectViewSet, "project")

projects_router = NestedSimpleRouter(router, r"projects", lookup="project")
projects_router.register(r"members", views.MemberViewSet, basename="project-members")
projects_router.register(
    r"services", views.ProjectServiceViewSet, basename="project-services"
)

members_router = NestedSimpleRouter(projects_router, r"members", lookup="user")
members_router.register(
    r"services", views.MemberServiceViewSet, basename="member-services"
)

router.register(r"users", views.UserViewSet, "user")
router.register(r"services", views.ServiceViewSet, "service")
router.register(r"user-namespaces", views.UserNamespaceViewSet, "user-namespace")
router.register(r"open-pgp-keys", views.PgpKeyInfoViewSet, "open-pgp-keys")
router.register(r"ssh-public-keys", views.SSHPublicKeyViewSet, "ssh-public-keys")
router.register(
    r"audit/project-permission",
    views.ProjectUserRoleHistoryViewSet,
    "audit-project-permission",
)
router.register(
    r"identity-provider", views.IdentityProviderViewSet, "identity-provider"
)
router.register(r"feed", views.FeedViewSet, "feed")
router.register(r"data-provider", views.DataProviderViewSet, "dataprovider")
router.register(r"data-package/log", views.DataPackageLogViewSet, "datapackagelog")
router.register(
    r"data-package/check", views.DataPackageCheckViewSet, "datapackagecheck"
)
router.register(r"data-package", views.DataPackageViewSet, "datapackage")
router.register(r"data-transfer", views.DataTransferViewSet, "datatransfer")
router.register(r"approval", views.ApprovalViewSet, "approval")
router.register(r"log", views.LogView, "log")
router.register(r"contact", views.ContactView, "contact")
router.register(r"flags", views.FlagViewSet, "flag")
router.register(
    r"custom-affiliations", views.CustomAffiliationViewSet, "customaffiliation"
)
router.register(r"quick-accesses", views.QuickAccessTileViewSet, "quickaccess")
router.register(r"oauth2-clients", views.OAuth2ClientViewSet, "oauth2client")
router.register(r"terms-of-use", views.TermsOfUseViewSet, "termsofuse")
router.register(
    r"terms-of-use-acceptance",
    views.TermsOfUseAcceptanceViewSet,
    "termsofuseacceptance",
)

# The API URLs are now determined automatically by the router.
app_name = APP_NAME
urlpatterns = [
    re_path(r"^", include(router.urls)),
    re_path(r"^userinfo/$", views.UserinfoView.as_view(), name="userinfo"),
    re_path(
        r"^switch-notification/",
        views.SwitchNotificationView.as_view(),
        name="switchnotification",
    ),
    re_path(r"^sts/", views.SecurityTokenService.as_view(), name="sts"),
    re_path(r"^exports/users", views.UsersExport.as_view(), name="usersexport"),
    re_path(
        r"^exports/terms-of-use/(?P<node_id>\d+)$",
        views.UsersTermsOfUseExport.as_view(),
        name="termsofuseexport",
    ),
    re_path(
        r"^exports/data-transfers",
        views.DataTransfersExport.as_view(),
        name="dtrsexport",
    ),
    re_path(r"", include(projects_router.urls)),
    re_path(r"", include(members_router.urls)),
]
