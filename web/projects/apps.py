from django.apps import AppConfig

APP_NAME = "projects"


class ProjectsConfig(AppConfig):
    name = APP_NAME
