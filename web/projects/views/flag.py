from django_drf_utils.views.utils import unique_check
from rest_framework import viewsets

from ..permissions import IsStaff, ReadOnly, IsAuthenticatedAndUniqueCheck
from ..models.flag import Flag
from ..serializers.user import FlagSerializer


@unique_check((IsAuthenticatedAndUniqueCheck,))
class FlagViewSet(viewsets.ModelViewSet):
    serializer_class = FlagSerializer
    permission_classes = ((IsStaff | ReadOnly),)
    queryset = Flag.objects.prefetch_related("users")
