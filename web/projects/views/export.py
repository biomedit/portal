import csv
from abc import ABC, abstractmethod
from datetime import datetime

from django.http import HttpResponse
from rest_framework.schemas.openapi import AutoSchema
from rest_framework.views import APIView


class RowObject(ABC):
    @abstractmethod
    def get_row_data(self) -> tuple[str, ...]:
        pass


class ExportSchema(AutoSchema):
    def __init__(self, description: str, **kwargs) -> None:
        self.description = description
        super().__init__(**kwargs)

    def get_responses(self, path, method):  # noqa: ARG002 unused-method-argument
        return {
            "200": {
                "content": {"text/csv": {"schema": {"type": "string"}}},
                "description": self.description,
            }
        }


class Export(APIView, ABC):
    def __init__(
        self, header: list[str], description: str, filename_prefix: str, *args, **kwargs
    ) -> None:
        def make_response() -> HttpResponse:
            timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            filename = f"{filename_prefix}_export_{timestamp}.csv"
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = f'attachment; filename="{filename}"'
            response["Content-Type"] = "text/csv"
            return response

        self.header = header
        self.schema = ExportSchema(description=description)
        self.response = make_response()
        super().__init__(*args, **kwargs)

    def get_writer(self, delimiter: str = "comma"):
        map_delimiter_format = {"tab": "\t", "comma": ",", "semicolon": ";"}
        delimiter = map_delimiter_format.get(delimiter, ",")
        return csv.writer(self.response, delimiter=delimiter)
