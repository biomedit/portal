from authlib.jose import JsonWebKey, KeySet
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from identities.models import OAuth2Client
from portal.settings import CONFIG
from projects.oauth import server


class Authorize(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        return server.create_authorization_response(request, grant_user=request.user)


class Token(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        return server.create_token_response(request)


class OpenIDConfig(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(cache_page(60 * 60 * 24))  # Cache returned response for 24h
    def get(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        auth_methods = OAuth2Client.TokenEndpointAuthMethod.values
        return Response(
            {
                "issuer": CONFIG.oauth.issuer,
                "authorization_endpoint": f"{CONFIG.oauth.issuer}/authorize",
                "token_endpoint": f"{CONFIG.oauth.issuer}/token",
                "jwks_uri": f"{CONFIG.oauth.issuer}/.well-known/jwks",
                "response_types_supported": ["code"],
                "id_token_signing_alg_values_supported": [CONFIG.oauth.alg],
                "grant_types_supported": OAuth2Client.GrantType.values,
                "token_endpoint_auth_methods_supported": auth_methods,
            }
        )


class JWKS(APIView):
    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def _load_public_keys():
        with open(CONFIG.oauth.jwt_key.public_path) as fp:
            data = fp.read()
            return KeySet(
                [(JsonWebKey.import_key(data, {"use": "sig", "alg": CONFIG.oauth.alg}))]
            )

    @method_decorator(cache_page(60 * 60 * 24))  # Cache returned response for 24h
    def get(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        return Response(JWKS._load_public_keys().as_dict(is_private=False))
