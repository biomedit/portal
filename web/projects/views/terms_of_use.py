from django.contrib.auth import get_user_model
from django.db.models import Q, QuerySet
from rest_framework import viewsets
from rest_framework.mixins import CreateModelMixin

from django_drf_utils.permissions import IsRead, IsPost
from ..permissions.node import IsNodeViewer, IsNodeAdmin as GenericIsNodeAdmin

from ..models.node import get_node_permission_nodes, Node
from ..models.project import ProjectUserRole, Project
from ..models.terms_of_use import TermsOfUse, TermsOfUseAcceptance
from ..permissions import IsAuthenticated, IsStaff
from ..permissions.terms_of_use import IsNodeAdmin, OwnsTermsOfUseAcceptance
from ..serializers.terms_of_use import (
    TermsOfUseAcceptanceSerializer,
    TermsOfUseLiteSerializer,
    TermsOfUseSerializer,
)

from .export import Export
from datetime import datetime

User = get_user_model()


def list_terms_of_use(user: User) -> QuerySet[TermsOfUse]:
    if user.is_staff:
        return TermsOfUse.objects.all()
    else:
        user_nodes = get_node_permission_nodes(user)
        user_project_nodes = (
            ProjectUserRole.objects.filter(user=user)
            .values_list("project__destination", flat=True)
            .distinct()
        )
        return TermsOfUse.objects.filter(
            Q(node__in=user_nodes) | Q(node__in=user_project_nodes)
        ).distinct()


class TermsOfUseViewSet(viewsets.ModelViewSet):
    permission_classes = (
        (IsRead & IsAuthenticated) | (IsPost & (IsNodeAdmin | IsStaff)),
    )
    serializer_class = TermsOfUseSerializer

    def get_serializer_class(self):
        if self.action == "list":
            return TermsOfUseLiteSerializer
        return TermsOfUseSerializer

    def get_queryset(self):
        requestor = self.request.user
        queryset = list_terms_of_use(requestor)

        if self.action == "list":
            return queryset

        return self.get_serializer_class().prefetch(queryset)


class TermsOfUseAcceptanceViewSet(viewsets.GenericViewSet, CreateModelMixin):
    permission_classes = (IsPost & (IsStaff | OwnsTermsOfUseAcceptance),)
    serializer_class = TermsOfUseAcceptanceSerializer


class UsersTermsOfUseExport(Export):
    permission_classes = (IsStaff | GenericIsNodeAdmin | IsNodeViewer,)

    def list_terms_of_use_by_node(self, node_id: int) -> QuerySet[TermsOfUse]:
        return TermsOfUse.objects.filter(node_id=node_id)

    def get_users_by_node(self, node_id: int):
        projects = Project.objects.filter(destination_id=node_id)
        project_user_ids = ProjectUserRole.objects.filter(
            project__in=projects
        ).values_list("user", flat=True)

        acceptance_user_ids = TermsOfUseAcceptance.objects.filter(
            terms_of_use__node_id=node_id
        ).values_list("user", flat=True)

        return User.objects.filter(
            Q(id__in=project_user_ids) | Q(id__in=acceptance_user_ids)
        ).distinct()

    def get_acceptances_by_node_and_user(self, node_id: int, user: User):
        return TermsOfUseAcceptance.objects.select_related(
            "user", "terms_of_use__node"
        ).filter(terms_of_use__node_id=node_id, user=user)

    def __init__(self, *args, **kwargs):
        super().__init__(
            header=[],
            description="Export Terms of Use",
            filename_prefix="",
            *args,
            **kwargs,
        )

    def get(self, request, node_id):
        self.node_id = node_id
        tou_by_node = self.list_terms_of_use_by_node(node_id)
        dynamic_tou_headers = [
            f"{tou.version} {tou.version_type}" for tou in tou_by_node
        ]
        self.header = [
            "First Name",
            "Last Name",
            "Email",
            *dynamic_tou_headers,
        ]
        writer = self.get_writer(request.query_params.get("delimiter", "comma"))
        writer.writerow(self.header)

        users_by_node = self.get_users_by_node(node_id)
        for user in users_by_node:
            row_data = [user.first_name, user.last_name, user.email]
            acceptances = self.get_acceptances_by_node_and_user(node_id, user)
            acceptance_ids = [a.terms_of_use.id for a in acceptances]
            acceptance_dates = []
            for tou in tou_by_node:
                if tou.id in acceptance_ids:
                    toa = TermsOfUseAcceptance.objects.get(
                        terms_of_use__id=tou.id, user=user
                    )
                    date_accepted = toa.created
                    formatted_date_accepted = (
                        date_accepted.strftime("%d/%m/%Y")
                        if date_accepted
                        else "Unknown"
                    )
                    acceptance_dates.append(formatted_date_accepted)
                else:
                    acceptance_dates.append("")
            row_data.extend(acceptance_dates)
            writer.writerow(row_data)

        response = self.response
        node = Node.objects.get(id=node_id)
        node_code = node.code
        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        new_filename = f"terms_of_use_node_{node_code}_{timestamp}.csv"
        response["Content-Disposition"] = f'attachment; filename="{new_filename}"'
        return response
