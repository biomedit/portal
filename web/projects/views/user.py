from dataclasses import asdict, astuple, dataclass
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django_drf_utils.exceptions import ConflictError
from django_drf_utils.permissions import IsRead, IsWrite, IsPatch, IsPost
from django_drf_utils.views.utils import unique_check
from django.http import Http404
from rest_framework import (
    mixins,
    permissions,
    response,
    viewsets,
    generics,
    serializers,
)
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError, ValidationError as RestValidationError
from rest_framework.schemas.openapi import AutoSchema

from identities.models import User, Group, GroupProfile
from identities.permissions import has_change_user_permission
from ..filters.user import UserActiveFilter, UserProjectRoleFilter, UserLookupFilter
from ..models.data_provider import (
    DataProvider,
    get_data_provider_role_group,
    get_data_provider_permission_data_providers,
    has_data_provider_manager_permission,
)
from ..models.node import (
    Node,
    get_node_role_group,
    get_node_permission_nodes,
    has_node_manager_permission,
    has_node_admin_nodes,
)
from ..models.project import (
    Project,
    ProjectRole,
    ProjectUserRole,
    Service,
    is_project_manager,
)
from ..models.user import UserNamespace
from ..permissions import IsStaff, IsAuthenticated, UPDATE_METHODS
from ..permissions.node import IsNodeAdmin, IsNodeViewer
from ..permissions.user import (
    UserModelPermission,
    IsAllowedPatch,
    MemberPermission,
)
from ..permissions.project import (
    ProjectServiceReadPermission,
    ProjectServiceWritePermission,
)
from ..serializers import (
    UserNamespaceSerializer,
    UserSerializer,
    UserLiteSerializer,
    UserinfoSerializer,
    ServiceSerializer,
)
from .export import Export, RowObject
from .project import ProjectMemberSchema, ProjectMemberServiceSchema


class UserinfoSchema(AutoSchema):
    def map_field(self, field):
        mapped_field = super().map_field(field)
        if isinstance(field, serializers.SerializerMethodField):
            field_name_to_component = {
                "permissions": {
                    "type": "object",
                    "readOnly": True,
                    "properties": {
                        "manager": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": (
                                "Whether current user is either a "
                                "project manager (PM) or a project leader (PL)"
                            ),
                        },
                        "staff": {"readOnly": True, "type": "boolean"},
                        "data_manager": {"readOnly": True, "type": "boolean"},
                        "project_leader": {"readOnly": True, "type": "boolean"},
                        "data_provider_admin": {"readOnly": True, "type": "boolean"},
                        "data_provider_viewer": {"readOnly": True, "type": "boolean"},
                        "node_admin": {"readOnly": True, "type": "boolean"},
                        "node_viewer": {"readOnly": True, "type": "boolean"},
                        "group_manager": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": (
                                "Whether current user is allowed to manage groups "
                                "(a node/data provider manager for instance)"
                            ),
                        },
                        "has_projects": {"readOnly": True, "type": "boolean"},
                        "legal_approver": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": (
                                "Whether current user is allowed to approve DTR"
                                " legal approvals"
                            ),
                        },
                        "data_specification_approver": {
                            "readOnly": True,
                            "type": "boolean",
                            "description": (
                                "Whether current user is allowed to approve DTR group"
                                " data specification approvals"
                            ),
                        },
                    },
                },
                "manages": {
                    "readOnly": True,
                    "type": "object",
                    "properties": {
                        "data_provider_admin": {
                            "readOnly": True,
                            "type": "array",
                            "items": {"$ref": "#/components/schemas/DataProvider"},
                        },
                        "node_admin": {
                            "readOnly": True,
                            "type": "array",
                            "items": {"$ref": "#/components/schemas/Node"},
                        },
                    },
                },
            }
            return field_name_to_component.get(field.field_name, mapped_field)
        return mapped_field


def list_users(request):
    def is_email_valid(email):
        try:
            validate_email(email)
            return True
        except ValidationError as exc:
            raise ParseError("Invalid email address") from exc

    def is_safe_with_param():
        return (
            request.method in permissions.SAFE_METHODS
            and "email" in request.query_params
            and is_email_valid(request.query_params["email"])
        )

    def can_see_all_users() -> bool:
        user = request.user
        return (
            user.is_staff
            or (
                # Users with `identities.change_user` permission
                has_change_user_permission(user)
                and (is_safe_with_param() or request.method in UPDATE_METHODS)
            )
            or (
                # Group/Project (DM, PL) managers if they are filtering (for adding
                # new users). NAs as well as they are able to add new users to
                # projects.
                is_safe_with_param()
                and (
                    is_project_manager(user)
                    or has_node_admin_nodes(user)
                    or has_data_provider_manager_permission(user)
                    or has_node_manager_permission(user)
                )
            )
        )

    def users_because_of_node_permissions():
        nodes = get_node_permission_nodes(user).distinct()
        q = Q()
        if nodes.exists():
            q |= Q(
                groups__profile__role_entity_type__model=GroupProfile.RoleEntityType.NODE.value,
                groups__profile__role_entity_id__in=nodes,
            )
            q |= Q(
                id__in=ProjectUserRole.objects.filter(
                    project__destination__in=nodes
                ).values_list("user", flat=True)
            )
        return q

    def users_because_of_data_provider_permissions():
        nodes = get_node_permission_nodes(user)
        data_providers = get_data_provider_permission_data_providers(
            user
        ) | DataProvider.objects.filter(nodes__in=nodes)
        q = Q()
        if data_providers.exists():
            q |= Q(
                groups__profile__role_entity_type__model=GroupProfile.RoleEntityType.DATA_PROVIDER.value,
                groups__profile__role_entity_id__in=data_providers,
            )
        return q

    def users_because_of_data_manager_roles():
        data_manager_roles = ProjectUserRole.objects.filter(
            user=user, role=ProjectRole.DM.value
        )
        q = Q()
        if data_manager_roles.exists():
            projects = Project.objects.filter(
                id__in=data_manager_roles.values_list("project")
            )
            q |= Q(
                id__in=ProjectUserRole.objects.filter(project__in=projects).values_list(
                    "user", flat=True
                )
            )
        return q

    if can_see_all_users():
        return User.objects.all()

    user = request.user
    q = Q(username=user.username)
    q |= users_because_of_node_permissions()
    q |= users_because_of_data_provider_permissions()
    q |= users_because_of_data_manager_roles()

    return User.objects.filter(q).distinct()


class MemberViewSet(viewsets.ModelViewSet):
    serializer_class = UserLiteSerializer
    schema = ProjectMemberSchema()
    permission_classes = (IsRead & MemberPermission,)

    def get_project(self):
        return get_object_or_404(Project.objects.all(), pk=self.kwargs["project_pk"])

    def get_queryset(self):
        project = self.get_project()
        users = {x.user for x in project.users.all()}
        return users

    def get_object(self):
        try:
            users = self.get_queryset()
        except Project.DoesNotExist as e:
            raise Project.DoesNotExist(e) from e

        for user in users:
            if user.pk == int(self.kwargs["pk"]):
                return user
        raise Http404("Project member does not exist")


class MemberServiceViewSet(viewsets.ModelViewSet):
    serializer_class = ServiceSerializer
    schema = ProjectMemberServiceSchema()
    permission_classes = (
        IsStaff
        | (IsWrite & IsNodeAdmin & ProjectServiceWritePermission)
        | (IsRead & ProjectServiceReadPermission),
    )

    def get_project(self):
        return get_object_or_404(Project.objects.all(), pk=self.kwargs["project_pk"])

    def get_user(self):
        project = self.get_project()
        users = {
            x.user
            for x in project.users.all()
            if x.user.pk == int(self.kwargs["user_pk"])
        }
        if not users:
            return None

        user = next(iter(users))
        return user

    def get_queryset(self):
        project = self.get_project()
        user = self.get_user()
        if not user:
            return []
        return user.services.filter(project=project)

    def get_object(self):
        project = self.get_project()
        user = self.get_user()
        services = Service.objects.filter(
            project=project, user=user, pk=self.kwargs["pk"]
        )
        if not services.count() == 1:
            raise Http404("Service for this project member not found")
        return services[0]


@unique_check((IsStaff & IsPost,))
class UserViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    """Show details of registered users."""

    def get_serializer_class(self):
        if self.action == "list":
            return UserLiteSerializer
        return UserSerializer

    permission_classes = (
        UserModelPermission
        | (
            IsAuthenticated & (IsRead | (IsPost & IsStaff) | (IsPatch & IsAllowedPatch))
        ),
    )
    filter_backends = (
        UserActiveFilter,
        UserProjectRoleFilter,
        UserLookupFilter,
    )
    schema = UserinfoSchema()

    def get_queryset(self):
        return self.get_serializer_class().prefetch(list_users(self.request))

    def create(self, request, *args, **kwargs):
        # False positive, exists on mixins.CreateModelMixin
        try:
            resp = super().create(request, *args, **kwargs)
        except RestValidationError as exc:
            raise ConflictError(exc) from exc

        created_user = User.objects.get(id=resp.data["id"])
        created_user.set_unusable_password()
        created_user.save()
        return resp

    @action(
        detail=True,
        permission_classes=(IsStaff,),
        methods=["GET"],
    )
    def services(self, request, pk):  # noqa: ARG002 unused-method-argument
        # Although user's `pk` is provided, we use `get_object` to fetch the user
        user = self.get_object()
        services = user.services.all()
        serialized = ServiceSerializer(services, many=True)
        return response.Response(serialized.data)


class UserinfoView(generics.RetrieveAPIView):
    serializer_class = UserinfoSerializer
    permission_classes = (permissions.IsAuthenticated,)
    schema = UserinfoSchema()

    def get_queryset(self):
        return User.objects.filter(username=self.request.user.username)

    def get_object(self):
        return self.get_queryset()[0]


class UserNamespaceViewSet(viewsets.ReadOnlyModelViewSet):
    """View available namespaces."""

    queryset = UserNamespace.objects.all()
    serializer_class = UserNamespaceSerializer
    permission_classes = (permissions.IsAuthenticated,)


@dataclass()
class UserRowObject(RowObject):
    user_id: str = ""
    first_name: str = ""
    last_name: str = ""
    email: str = ""
    username: str = ""
    last_login: str = ""
    is_active: str = ""
    affiliation: str = ""
    affiliation_id: str = ""
    custom_affiliation: str = ""
    uid: str = ""
    gid: str = ""
    local_username: str = ""
    display_name: str = ""
    display_id: str = ""
    namespace: str = ""
    display_local_username: str = ""
    affiliation_consent: str = ""
    user_flag: str = ""
    user_project: str = ""
    user_project_node: str = ""
    user_data_provider: str = ""
    user_data_provider_role: str = ""
    user_data_provider_nodes: str = ""
    user_node: str = ""
    user_node_role: str = ""

    def get_row_data(self) -> tuple[str, ...]:
        return astuple(self)


class UsersExport(Export):
    def __init__(self, *args, **kwargs):
        super().__init__(
            header=[
                "ID",
                "First Name",
                "Last Name",
                "Email",
                "Username",
                "Last Login",
                "Is Active",
                "Affiliation",
                "Affiliation ID",
                "Custom Affiliation",
                "Uid",
                "Gid",
                "Local username",
                "Display name",
                "Display ID",
                "Namespace",
                "Display local username",
                "Affiliation consent",
                "Flags",
                "Projects",
                "Project Node",
                "Data Provider",
                "Data Provider Role",
                "Data Provider Node",
                "Node",
                "Node Role",
            ],
            description="Export Users as CSV",
            filename_prefix="users",
            *args,
            **kwargs,
        )

    permission_classes = (IsStaff | IsNodeAdmin | IsNodeViewer,)

    def get(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        def write_user_flags(user) -> bool:
            flags = user.flags.all()
            for flag in flags:
                flag_row_object = UserRowObject(
                    **(
                        asdict(rowobj)
                        | {"user_flag": f"{flag.code} ({flag.description})"}
                    )
                )
                writer.writerow(flag_row_object.get_row_data())
            return bool(flags)

        def write_user_projects(user) -> bool:
            projects = Project.objects.filter(users__user=user).distinct()
            for project in projects:
                project_row_object = UserRowObject(
                    **(
                        asdict(rowobj)
                        | {
                            "user_project": f"{project.code} ({project.name})",
                            "user_project_node": project.destination,
                        }
                    )
                )
                writer.writerow(project_row_object.get_row_data())
            return bool(projects)

        def get_groups_of_user(user):
            node_groups_of_user = []
            data_provider_groups_of_user = []
            groups = Group.objects.filter(user=user).select_related(
                "profile__role_entity_type"
            )
            for group in groups:
                if role := group.profile.role_entity_type:
                    user_group_type = role.model
                    if user_group_type == "node":
                        node_groups_of_user.append(group)
                    elif user_group_type == "dataprovider":
                        data_provider_groups_of_user.append(group)
            return node_groups_of_user, data_provider_groups_of_user

        def write_user_data_providers(data_providers, data_provider_groups_of_user):
            for data_provider in data_providers:
                for dp_group_of_user in data_provider_groups_of_user:
                    try:
                        group = get_data_provider_role_group(
                            data_provider, dp_group_of_user.profile.role
                        )
                    except (
                        Group.DoesNotExist,
                        Group.profile.RelatedObjectDoesNotExist,
                    ):
                        continue
                    if group.id == dp_group_of_user.id:
                        group_row_object = UserRowObject(
                            **(
                                asdict(rowobj)
                                | {
                                    "user_data_provider": data_provider.name,
                                    "user_data_provider_role": group.name,
                                    "user_data_provider_nodes": ", ".join(
                                        node.name for node in data_provider.nodes.all()
                                    ),
                                }
                            )
                        )
                        writer.writerow(group_row_object.get_row_data())

        def write_user_nodes(nodes, node_groups_of_user):
            for node in nodes:
                for node_group_of_user in node_groups_of_user:
                    try:
                        group = get_node_role_group(
                            node, node_group_of_user.profile.role
                        )
                    except (
                        Group.DoesNotExist,
                        Group.profile.RelatedObjectDoesNotExist,
                    ):
                        continue
                    if group.id == node_group_of_user.id:
                        rowobj.user_node = node.name
                        rowobj.user_node_role = group.name
                        writer.writerow(rowobj.get_row_data())

        writer = self.get_writer(request.query_params.get("delimiter", "comma"))

        # Write the column headers to csv file
        writer.writerow(self.header)

        user = request.user
        users = User.objects.all() if user.is_staff else list_users(request)
        nodes = Node.objects.all()
        dataproviders = DataProvider.objects.all()

        # For each user, write rows for flags, projects, nodes, data providers
        for user in users:
            user_row_info = {
                "user_id": user.id,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "email": user.email,
                "username": user.username,
                "last_login": user.last_login,
                "is_active": user.is_active,
            }

            if hasattr(user, "profile"):
                profile = user.profile
                user_row_info = user_row_info | {
                    "affiliation": profile.affiliation,
                    "affiliation_id": profile.affiliation_id,
                    "custom_affiliation": profile.custom_affiliation,
                    "uid": profile.uid,
                    "gid": profile.gid,
                    "local_username": profile.local_username,
                    "display_name": profile.display_name,
                    "display_id": profile.display_id,
                    "namespace": profile.namespace,
                    "display_local_username": profile.display_local_username,
                    "affiliation_consent": profile.affiliation_consent,
                }

            rowobj = UserRowObject(**user_row_info)

            # Outputs the user flags
            has_flags = write_user_flags(user)

            # Write the projects the user is associated with
            has_projects = write_user_projects(user)

            # Helper method to provide arrays for different user groups
            node_groups_of_user, data_provider_groups_of_user = get_groups_of_user(user)

            # Write the DPs and Nodes the user is associated with
            if len(data_provider_groups_of_user) > 0:
                write_user_data_providers(dataproviders, data_provider_groups_of_user)

            if len(node_groups_of_user) > 0:
                write_user_nodes(nodes, node_groups_of_user)

            # Output users even though they have no flags, projects, nodes, or
            # data providers associated with them
            if (
                not has_flags
                and not has_projects
                and not node_groups_of_user
                and not data_provider_groups_of_user
            ):
                writer.writerow(rowobj.get_row_data())

        return self.response
