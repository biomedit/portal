import logging
from dataclasses import dataclass
from itertools import chain

import requests
import xmltodict
from django_drf_utils.views.utils import DetailedResponse
from rest_framework import permissions, serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import BasicAuthentication, SessionAuthentication

from identities.models import OAuth2Client
from projects.models.data_transfer import DataPackage, DataTransfer
from projects.oauth import server, IClaimProvider
from portal.oidc import TokenAuthentication
from portal.settings import CONFIG
from projects.models.project import ProjectRole

logger = logging.getLogger(__name__)


class CredentialsSerializer(serializers.Serializer):
    endpoint = serializers.CharField(
        label="Storage Endpoint",
        help_text="Storage Endpoint",
        read_only=True,
    )
    bucket = serializers.CharField(
        label="Bucket",
        help_text="Bucket",
        read_only=True,
    )
    access_key_id = serializers.CharField(
        label="Access key ID",
        help_text="Access key ID",
        read_only=True,
    )
    secret_access_key = serializers.CharField(
        label="Secret access key",
        help_text="Secret access key",
        read_only=True,
    )
    session_token = serializers.CharField(
        label="Session token",
        help_text="Session token",
        read_only=True,
    )
    expiration = serializers.DateTimeField(
        label="Expiration date time",
        help_text="Expiration date time",
        read_only=True,
    )
    dtr = serializers.IntegerField(
        label="DTR ID", help_text="Data Transfer ID", write_only=True, required=False
    )
    file_name = serializers.CharField(
        label="Data package name",
        help_text="Name of the data package",
        write_only=True,
        required=False,
    )
    action = serializers.ChoiceField(
        label="Action",
        write_only=True,
        choices=tuple([item.value for item in IClaimProvider.Action]),
    )


@dataclass
class StsOutput:
    endpoint: str
    bucket: str
    access_key_id: str
    secret_access_key: str
    session_token: str
    expiration: str


class LoggedErrorResponse(DetailedResponse):
    def __init__(self, detail, status_code):
        super().__init__(detail, status_code)
        logger.error(detail)


class SecurityTokenService(APIView):
    authentication_classes = (
        BasicAuthentication,
        SessionAuthentication,
        TokenAuthentication,
    )
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CredentialsSerializer

    def get_serializer_context(self):
        return {"request": self.request, "format": self.format_kwarg, "view": self}

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    def post(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        dtr_key = "dtr"
        file_name_key = "file_name"
        action = IClaimProvider.Action(serializer.validated_data["action"])
        if action == IClaimProvider.Action.READ:
            try:
                data_package = DataPackage.objects.get(file_name=data[file_name_key])
            except DataPackage.DoesNotExist:
                data_package = None
                return LoggedErrorResponse(
                    f"""
                    Given '{file_name_key}' ('{data[file_name_key]}') not found
                    """,
                    status.HTTP_422_UNPROCESSABLE_ENTITY,
                )
            except KeyError:
                data_package = None
        else:
            data_package = None

        if data_package:
            dtr = data_package.data_transfer
        else:
            try:
                dtr = DataTransfer.objects.get(id=data[dtr_key])
            except DataTransfer.DoesNotExist:
                return LoggedErrorResponse(
                    f"Given '{dtr_key}' ('{data[dtr_key]}') not found",
                    status.HTTP_422_UNPROCESSABLE_ENTITY,
                )
            except KeyError:
                return LoggedErrorResponse(
                    f"Missing '{dtr_key}' parameter",
                    status.HTTP_400_BAD_REQUEST,
                )
        if dtr.status != DataTransfer.AUTHORIZED:
            return LoggedErrorResponse(
                f"Data transfer '{dtr}' is not authorized",
                status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        is_data_recipient = request.user in set(
            chain(dtr.data_recipients.all(), dtr.project.users_by_role(ProjectRole.DM))
        )
        is_data_engineer = request.user in dtr.data_provider.data_engineers
        if not is_data_recipient and not is_data_engineer:
            return LoggedErrorResponse(
                f"User '{request.user}' is neither a data engineer"
                f" of '{dtr.data_provider}', nor a data recipient of '{dtr}',"
                f" nor a data manager of '{dtr.project}'",
                status.HTTP_403_FORBIDDEN,
            )
        if action == IClaimProvider.Action.READ and not is_data_recipient:
            return LoggedErrorResponse(
                f"User '{request.user}' is not authorized to read data from '{dtr}'",
                status.HTTP_403_FORBIDDEN,
            )
        if action == IClaimProvider.Action.WRITE and not is_data_engineer:
            return LoggedErrorResponse(
                f"User '{request.user}' is not authorized to write data to '{dtr}'",
                status.HTTP_403_FORBIDDEN,
            )
        node = dtr.project.destination
        if node.oauth2_client is None:
            return LoggedErrorResponse(
                f"No OAuth2 client found for node '{node}'",
                status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        client = node.oauth2_client
        scope = client.scope
        # All following attributes MUST be specified in `request` object to
        # make the token generation working
        request.client = client
        request.scope = scope
        request.grant_type = OAuth2Client.GrantType.AUTHORIZATION_CODE
        # `OpenIDToken.process_token` expects `request` to have
        # `authorization_code` attribute
        request.authorization_code = None
        if len(node.object_storage_url) == 0:
            return LoggedErrorResponse(
                f"No object storage URL for node '{node}' has been specified",
                status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        object_storage_url = node.object_storage_url

        bucket = dtr.project.code.replace("_", "-")
        grant = server.get_token_grant(request)
        token = grant.generate_token(user=request.user, scope=scope)
        grant.execute_hook("process_token", token=token, dtr=dtr, action=action)
        payload = {
            "Action": "AssumeRoleWithWebIdentity",
            "WebIdentityToken": token["id_token"],
            "Version": "2011-06-15",
            "DurationSeconds": CONFIG.oauth.sts.duration_seconds,
        }
        try:
            answer = requests.post(object_storage_url, data=payload, timeout=3)
        except requests.exceptions.RequestException as e:
            return LoggedErrorResponse(
                f"A problem occurred while reaching '{object_storage_url}': {e}",
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        # Because the answer is always XML, independent of the `Accept` header
        data = xmltodict.parse(answer.text)
        if answer.status_code != 200:
            return LoggedErrorResponse(
                f"STS '{object_storage_url}' returned HTTP {answer.status_code}:"
                f" {data['ErrorResponse']['Error']['Message']}",
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        credentials = data["AssumeRoleWithWebIdentityResponse"][
            "AssumeRoleWithWebIdentityResult"
        ]["Credentials"]
        logger.info(
            "Credentials for user '%s' successfully obtained from STS '%s'",
            request.user.username,
            object_storage_url,
        )
        serializer = CredentialsSerializer(
            StsOutput(
                endpoint=object_storage_url,
                bucket=bucket,
                access_key_id=credentials["AccessKeyId"],
                secret_access_key=credentials["SecretAccessKey"],
                session_token=credentials["SessionToken"],
                expiration=credentials["Expiration"],
            )
        )
        return Response(
            serializer.data,
            status=status.HTTP_200_OK,
        )
