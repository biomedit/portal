from django_drf_utils.permissions import IsRead, IsUpdate
from django_drf_utils.views.utils import unique_check
from rest_framework.viewsets import ModelViewSet

from ..filters.data_provider import DataProviderPermissionsFilter
from ..models.data_provider import DataProvider, has_any_data_provider_permissions
from ..permissions import (
    IsAuthenticatedAndUniqueCheck,
    IsStaff,
)
from ..permissions.data_provider import IsDataProviderAdmin, ViewDataProvider
from ..serializers.data_provider import DataProviderSerializer


@unique_check((IsAuthenticatedAndUniqueCheck,))
class DataProviderViewSet(ModelViewSet):
    """Authenticated users can list, data provider admins can update their data
    providers and (portal) staff can create and update any data provider."""

    filter_backends = (DataProviderPermissionsFilter,)
    search_fields = ("username",)

    serializer_class = DataProviderSerializer
    permission_classes = (
        IsStaff | (IsUpdate & IsDataProviderAdmin) | (IsRead & ViewDataProvider),
    )

    def get_queryset(self):
        queryset = DataProvider.objects.prefetch_related("nodes")
        if self.request and (
            self.request.user.is_staff
            or has_any_data_provider_permissions(self.request.user)
        ):
            return queryset.all()
        return queryset.filter(enabled=True)
