import logging
from collections import defaultdict

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django_drf_utils.email import sendmail
from django_drf_utils.views.utils import DetailedResponse, unique_check
from django_drf_utils.permissions import IsRead, IsUpdate, IsWrite
from django.http import Http404
from rest_framework import status, response, viewsets, serializers
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.permissions import SAFE_METHODS
from rest_framework.schemas.openapi import AutoSchema

from ..filters.common import CaseInsensitiveOrderingFilter
from ..filters.project import (
    ProjectsPermissionFilter,
    ProjectsLookupFilter,
    ProjectUserRoleHistoryLookupFilter,
)
from ..models.data_transfer import DataTransfer
from ..models.node import (
    Node,
    get_node_permission_nodes,
    has_any_node_permissions,
    has_node_admin_permission,
)
from ..models.project import (
    Project,
    ProjectRole,
    ProjectUserRole,
    ProjectUserRoleHistory,
    Service,
)
from ..notifications.project_archival import (
    project_archive_users_notification,
    project_unarchive_users_notification,
    project_archive_unarchive_node_notification,
)
from ..permissions import IsStaff

from ..permissions.node import IsNodeAdmin, IsNodeViewer
from ..permissions.project import (
    IsProjectLeader,
    IsPermissionManager,
    ProjectPermission,
    ProjectServiceReadPermission,
    ProjectServiceWritePermission,
    ProjectUniquePermission,
    IsNodeAdmin as IsProjectNodeAdmin,
    IsNodeViewer as IsProjectNodeViewer,
)
from ..serializers import (
    ProjectRestrictedSerializer,
    ProjectSerializer,
    ProjectSerializerLite,
    ProjectUserRoleHistorySerializer,
    UserSerializer,
    UserShortSerializer,
    ServiceSerializer,
)
from ..signals.project import update_permissions_signal

User = get_user_model()
archive_permission = [IsStaff | (IsUpdate & IsProjectNodeAdmin)]

logger = logging.getLogger(__name__)


class ProjectSchema(AutoSchema):
    def map_field(self, field):
        mapped_field = super().map_field(field)
        if isinstance(field, serializers.SerializerMethodField):
            field_name_to_component = {
                "permissions": {
                    "readOnly": True,
                    "type": "object",
                    "properties": {
                        "edit": {
                            "type": "object",
                            "readOnly": True,
                            "properties": {
                                "name": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "code": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "destination": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "users": {
                                    "type": "array",
                                    "readOnly": True,
                                    "items": {"enum": [p.name for p in ProjectRole]},
                                },
                                "ip_address_ranges": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "resources": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "legal_support_contact": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "legal_approval_group": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "expiration_date": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "ssh_support": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "resources_support": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "services_support": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "identity_provider": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "data_specification_approval_group": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                                "additional_info": {
                                    "type": "boolean",
                                    "readOnly": True,
                                },
                            },
                        },
                        "archive": {
                            "type": "boolean",
                            "readOnly": True,
                        },
                    },
                },
                "user_has_role": {
                    "type": "boolean",
                    "readOnly": True,
                },
            }
            return field_name_to_component.get(field.field_name, mapped_field)
        return mapped_field


class ProjectServiceSchema(AutoSchema):
    def get_operation_id(self, path, method):  # noqa: ARG002 unused-method-argument
        method_name = getattr(self.view, "action", method.lower())
        if method.lower() in self.method_mapping:
            act = self.method_mapping[method.lower()]
        else:
            act = self._to_camel_case(method_name)

        operation_id = act + "ProjectService"
        if path.endswith("services/"):
            operation_id += "List"
        return operation_id


class ProjectServiceViewSet(viewsets.ModelViewSet):
    serializer_class = ServiceSerializer
    schema = ProjectServiceSchema()
    permission_classes = (
        IsStaff
        | (IsWrite & IsNodeAdmin & ProjectServiceWritePermission)
        | (IsRead & ProjectServiceReadPermission),
    )

    def get_project(self):
        return get_object_or_404(Project.objects.all(), pk=self.kwargs["project_pk"])

    def get_queryset(self):
        project = self.get_project()
        return project.services.filter(user=None)

    def get_object(self):
        project = self.get_project()
        services = Service.objects.filter(
            project=project, user=None, pk=self.kwargs["pk"]
        )
        if not services.count() == 1:
            raise Http404("Service for this project not found")
        return services[0]


class ProjectMemberSchema(AutoSchema):
    def get_operation_id(self, path, method):  # noqa: ARG002 unused-method-argument
        method_name = getattr(self.view, "action", method.lower())
        if method.lower() in self.method_mapping:
            act = self.method_mapping[method.lower()]
        else:
            act = self._to_camel_case(method_name)

        operation_id = act + "ProjectMember"
        if path.endswith("members/"):
            operation_id += "List"
        return operation_id


class ProjectMemberServiceSchema(AutoSchema):
    def get_operation_id(self, path, method):  # noqa: ARG002 unused-method-argument
        method_name = getattr(self.view, "action", method.lower())
        if method.lower() in self.method_mapping:
            act = self.method_mapping[method.lower()]
        else:
            act = self._to_camel_case(method_name)

        operation_id = act + "ProjectMemberService"
        if path.endswith("services/"):
            operation_id += "List"
        return operation_id


@unique_check((ProjectUniquePermission,))
class ProjectViewSet(viewsets.ModelViewSet):
    """Show, add, and modify projects."""

    permission_classes = (ProjectPermission,)
    filter_backends = (
        SearchFilter,
        CaseInsensitiveOrderingFilter,
        ProjectsPermissionFilter,
        ProjectsLookupFilter,
    )
    search_fields = ("code", "name")
    ordering_fields = ("code", "name")
    schema = ProjectSchema(component_name="Project", operation_id_base="Project")

    def has_node_admin_permission(self):
        if self.request.method in SAFE_METHODS:
            return has_node_admin_permission(user=self.request.user)
        try:
            return has_node_admin_permission(
                user=self.request.user,
                node=Node.objects.get(code=self.request.data["destination"]),
            )
        except (ObjectDoesNotExist, KeyError) as exc:
            logger.warning(str(exc))
            return False

    def get_serializer_class(self):
        if hasattr(self, "action") and self.action == "list":
            return ProjectSerializerLite
        is_generate_schema = not self.request and settings.DEBUG
        if is_generate_schema or (
            self.request
            and (self.request.user.is_staff or self.has_node_admin_permission())
        ):
            return ProjectSerializer
        return ProjectRestrictedSerializer

    def perform_destroy(self, instance):
        # All the permissions must be removed on project deletion
        update_permissions_signal.send(
            sender=instance, user=self.request.user, user_roles=[]
        )
        super().perform_destroy(instance)

    def get_queryset(self):
        if self.request and self.request.user.is_staff:
            query = Project.objects
        else:
            users_projects = Q(users__user__username=self.request.user.username)
            if self.request and has_any_node_permissions(self.request.user):
                # User can see all projects where
                # a) they are a member of
                # b) the destination points to a node where they have any permission
                query = Project.objects.filter(
                    users_projects
                    | Q(
                        destination__in=get_node_permission_nodes(
                            self.request.user
                        ).all()
                    )
                ).distinct()
            else:
                query = Project.objects.filter(users_projects).distinct()
        return self.get_serializer_class().prefetch(query)

    @action(
        detail=True,
        permission_classes=(
            IsRead & (IsStaff | IsProjectNodeAdmin | IsProjectNodeViewer),
        ),
    )
    def users(self, request, pk=None):  # noqa: ARG002 unused-method-argument
        """List users of a given project by permission.

        GET params:
        - role: one of projects.models.ProjectRole
                (default: USER, role=* shows all users of that project)
        - short: show shortend version of User records
                (default: true)
        """
        project = self.get_object()
        request_role = request.query_params.get("role", ProjectRole.USER.name)
        request_role_id = 0
        for role in ProjectRole:
            if request_role == "*":
                request_role_id = 0
                break
            if request_role.upper() == role.name:
                request_role_id = role.value
                break
        else:
            return DetailedResponse(
                "Provided role does not match any of the predefined roles.",
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        short = request.query_params.get("short", "true").lower() == "true"

        if request_role_id:
            users = {x.user for x in project.users.filter(role=request_role_id)}
        else:
            users = {x.user for x in project.users.all()}

        user_roles = defaultdict(list)
        for user in project.users.all():
            # go through all user_roles combinations of this project
            # create a dict like { "username": [role1, role2]}
            for role in ProjectRole:
                if role.value == user.role:
                    user_roles[user.user.username].append(role.name)

        serializer = UserShortSerializer if short else UserSerializer
        serialized = serializer(
            users,
            many=True,
            context={"user_roles": user_roles, "project": project},
        )
        return response.Response(serialized.data)

    @action(detail=True, permission_classes=archive_permission, methods=["put"])
    def archive(self, request, pk=None):  # noqa: ARG002 unused-method-argument
        project = self.get_object()

        archive_project(project, False)

        return response.Response(
            ProjectSerializer(project, context={"request": request}).data
        )

    @action(detail=True, permission_classes=archive_permission, methods=["put"])
    def unarchive(self, request, pk=None):  # noqa: ARG002 unused-method-argument
        project = self.get_object()
        project.archived = False
        project.save()

        subject = f'Project "{project.name}" was unarchived'
        body_users = project_unarchive_users_notification(project.name)
        send_project_archival_notification(project, subject, body_users)

        return response.Response(
            ProjectSerializer(project, context={"request": request}).data
        )


def archive_project(project, project_expired):
    project.archived = True
    project.save()

    for dtr in project.data_transfers.all():
        dtr.status = DataTransfer.EXPIRED
        dtr.save()

    subject = (
        f'Project "{project.name}" was'
        f" archived{' due to expiration' if project_expired else ''}"
    )
    body_users = project_archive_users_notification(project.name)

    send_project_archival_notification(project, subject, body_users)
    logger.info(subject)


def send_project_archival_notification(project, subject, body_users):
    # notify users
    sendmail(
        subject=subject,
        body=body_users,
        recipients=tuple({pur.user.email for pur in project.users.all()}),
        email_cfg=settings.CONFIG.email,
        reply_to=(settings.CONFIG.notification.ticket_mail,),
    )

    # notify node
    sendmail(
        subject=subject,
        body=project_archive_unarchive_node_notification(project),
        recipients=(project.destination.ticketing_system_email,),
        email_cfg=settings.CONFIG.email,
    )


class ProjectUserRoleHistoryViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (
        IsRead
        & (
            IsStaff | IsNodeAdmin | IsNodeViewer | IsProjectLeader | IsPermissionManager
        ),
    )
    serializer_class = ProjectUserRoleHistorySerializer
    filter_backends = (ProjectUserRoleHistoryLookupFilter,)

    def get_queryset(self):
        if not self.request:
            return ()
        if self.request.user.is_staff:
            return ProjectUserRoleHistory.objects.all()
        return ProjectUserRoleHistory.objects.filter(
            Q(
                project__in=Project.objects.filter(
                    destination__in=get_node_permission_nodes(self.request.user)
                )
            )
            | Q(
                project__in={
                    project_role.project
                    for project_role in ProjectUserRole.objects.filter(
                        user=self.request.user
                    )
                }
            )
        )
