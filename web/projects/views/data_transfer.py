from dataclasses import asdict, astuple, dataclass
from django.db.models import Q
from rest_framework import viewsets, throttling
from rest_framework.permissions import SAFE_METHODS
from rest_framework.authentication import BasicAuthentication, SessionAuthentication

from django_drf_utils.permissions import IsPost, IsRead, IsPatch

from portal.oidc import TokenAuthentication
from ..models.approval import BaseApproval
from ..models.data_provider import (
    get_data_provider_permission_data_providers,
)
from ..models.data_transfer import DataTransfer
from ..models.node import get_node_permission_nodes
from ..models.user import (
    get_legal_approval_role_groups,
    get_data_specification_approval_role_groups,
)
from ..permissions import IsAuthenticated, IsStaff
from ..permissions.data_transfer import (
    IsDataSpecificationApprover,
    IsPatchingDataSpecification,
    IsProjectDataManager,
)
from ..serializers import (
    DataTransferSerializer,
    DataTransferLite,
    DataPackageSerializer,
)
from .export import Export, RowObject


def list_data_transfers(user):
    user_nodes = get_node_permission_nodes(user)
    data_providers = get_data_provider_permission_data_providers(user)
    user_approval_groups = get_legal_approval_role_groups(
        user
    ) | get_data_specification_approval_role_groups(user)
    if user.is_staff:
        query = DataTransfer.objects
    else:
        query = DataTransfer.objects.filter(
            Q(project__users__user=user)
            | Q(data_provider__in=data_providers)
            | (
                Q(project__destination__in=user_nodes)
                | Q(data_provider__nodes__in=user_nodes)
            )
            | (
                Q(group_approvals__group__in=user_approval_groups)
                & Q(purpose=DataTransfer.PRODUCTION)
            )
        ).distinct()
    return query


class UnsafeUserRateThrottle(throttling.UserRateThrottle):
    scope = "unsafe_user"

    def allow_request(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return super().allow_request(request, view)


class DataTransferViewSet(viewsets.ModelViewSet):
    authentication_classes = (
        SessionAuthentication,
        BasicAuthentication,
        TokenAuthentication,
    )
    permission_classes = (
        IsAuthenticated
        & (
            IsStaff
            | IsRead
            | IsPost & IsProjectDataManager
            | IsPatch & IsPatchingDataSpecification & IsDataSpecificationApprover
        ),
    )
    throttle_classes = (UnsafeUserRateThrottle,)

    def get_serializer_class(self):
        if self.action == "list":
            return DataTransferLite
        return DataTransferSerializer

    def get_queryset(self):
        requestor = self.request.user
        query = list_data_transfers(requestor)
        return self.get_serializer_class().prefetch(query)


@dataclass()
class DataTransferRowObject(RowObject):
    id: str = ""
    data_provider_name: str = ""
    project_name: str = ""
    purpose: str = ""
    data_provider_nodes: str = ""
    destination_node: str = ""
    requestor: str = ""
    status: str = ""
    last_changed: str = ""
    data_recipients: str = ""
    data_specification: str = ""
    legal_basis: str = ""
    created: str = ""
    max_packages: str = ""
    approval_id: str = ""
    approval_institution: str = ""
    approval_status: str = ""
    approval_rejection_reason: str = ""
    approval_created: str = ""
    approval_last_changed: str = ""
    package_id: str = ""
    package_file_name: str = ""
    package_file_size: str = ""
    package_metadata_hash: str = ""
    package_metadata: str = ""
    package_sender_open_pgp_key_info: str = ""
    package_recipients_open_pgp_key_info: str = ""
    package_destination_node: str = ""
    package_data_provider: str = ""
    package_trace_node: str = ""
    package_trace_status: str = ""
    package_trace_timestamp: str = ""

    def get_row_data(self) -> tuple[str, ...]:
        return astuple(self)


class DataTransfersExport(Export):
    def __init__(self, *args, **kwargs):
        super().__init__(
            header=[
                "Transfer ID",
                "Data Provider",
                "Project",
                "Purpose",
                "Data Provider Nodes",
                "Destination Node",
                "Requestor",
                "Status",
                "Last Changed",
                "Data Recipients",
                "Data Specification",
                "Legal Basis",
                "Created",
                "Max Number of Packages",
                "Approval ID",
                "Approval Institution",
                "Approval Status",
                "Approval Rejection Reason",
                "Approval Created",
                "Approval Last Changed",
                "Package ID",
                "Package File Name",
                "Package File Size",
                "Package Metadata Hash",
                "Package Metadata",
                "Package Sender OpenPGP Key Info",
                "Package Destination Node",
                "Package Data Provider",
                "Package Trace Node",
                "Package Trace Status",
                "Package Trace Timestamp",
            ],
            description="Export Data Transfer Requests as CSV",
            filename_prefix="data_transfers",
            *args,
            **kwargs,
        )

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        def write_approvals(data_transfer: DataTransfer) -> None:
            for approval in data_transfer.approvals:
                approval_rowobj = DataTransferRowObject(
                    **(
                        asdict(rowobj)
                        | {
                            "approval_id": approval.id,
                            "approval_institution": approval.institution.name,
                            "approval_status": BaseApproval.ApprovalStatus(
                                approval.status
                            ).label,
                            "approval_rejection_reason": approval.rejection_reason,
                            "approval_created": approval.created,
                            "approval_last_changed": approval.change_date,
                        }
                    )
                )
                writer.writerow(approval_rowobj.get_row_data())

        def write_data_packages(data_transfer: DataTransfer) -> None:
            for package in data_transfer.packages.all():
                package_sender_open_pgp_key_info = (
                    DataPackageSerializer().get_sender_open_pgp_key_info(package)
                )
                package_recipients_open_pgp_key_info = (
                    DataPackageSerializer().get_recipients_open_pgp_key_info(package)
                )
                package_destination_node = data_transfer.project.destination.name
                package_rowobj = DataTransferRowObject(
                    **(
                        asdict(rowobj)
                        | {
                            "package_id": package.id,
                            "package_file_name": package.file_name,
                            "package_file_size": package.file_size,
                            "package_metadata_hash": package.metadata_hash,
                            "package_metadata": package.metadata,
                            "package_sender_open_pgp_key_info": (
                                package_sender_open_pgp_key_info
                            ),
                            "package_recipients_open_pgp_key_info": (
                                package_recipients_open_pgp_key_info
                            ),
                            "package_destination_node": package_destination_node,
                            "package_data_provider": data_transfer.data_provider.name,
                        }
                    )
                )

                if not package.trace.exists():
                    writer.writerow(package_rowobj.get_row_data())
                else:
                    for trace in package.trace.all():
                        trace_rowobj = DataTransferRowObject(
                            **(
                                asdict(package_rowobj)
                                | {
                                    "package_trace_node": trace.node.name,
                                    "package_trace_status": trace.status,
                                    "package_trace_timestamp": trace.timestamp,
                                }
                            )
                        )

                        writer.writerow(trace_rowobj.get_row_data())

        user = request.user
        writer = self.get_writer(request.query_params.get("delimiter", "comma"))
        data_transfers = list_data_transfers(user).all()

        writer.writerow(self.header)
        for data_transfer in data_transfers:
            data_transfer_row_info = {
                "id": data_transfer.id,
                "data_provider_name": data_transfer.data_provider.name,
                "project_name": data_transfer.project.name,
                "purpose": data_transfer.purpose,
                "destination_node": data_transfer.project.destination.name,
                "requestor": data_transfer.requestor,
                "status": data_transfer.status,
                "last_changed": data_transfer.change_date,
                "data_recipients": ", ".join(
                    [
                        user.profile.display_name
                        for user in data_transfer.data_recipients.all()
                    ]
                ),
                "data_specification": data_transfer.data_specification,
                "legal_basis": data_transfer.legal_basis,
                "created": data_transfer.creation_date,
                "max_packages": (
                    data_transfer.max_packages
                    if data_transfer.max_packages > 0
                    else "UNLIMITED"
                ),
            }

            if data_transfer.data_provider.nodes:
                data_transfer_row_info["data_provider_nodes"] = ", ".join(
                    node.name for node in data_transfer.data_provider.nodes.all()
                )

            rowobj = DataTransferRowObject(**data_transfer_row_info)

            # A data transfer will always have at least two approvals.
            write_approvals(data_transfer)
            write_data_packages(data_transfer)

        return self.response
