from rest_framework import viewsets, permissions, mixins
from rest_framework.throttling import AnonRateThrottle

from ..serializers import ContactSerializer


class ContactView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = ContactSerializer
    permission_classes = (permissions.AllowAny,)
    throttle_classes = [AnonRateThrottle]
