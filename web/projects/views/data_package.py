from django.db.models import Q
from rest_framework import status, viewsets, mixins, permissions
from rest_framework.response import Response
from rest_framework.schemas.openapi import AutoSchema

from ..filters.common import CaseInsensitiveOrderingFilter
from ..models.data_provider import (
    get_data_provider_permission_data_providers,
)
from ..models.data_transfer import DataPackage, DataPackageTrace
from ..models.node import get_node_permission_nodes
from ..models.project import ProjectRole
from ..permissions import IsStaff
from ..serializers import (
    DataPackageSerializer,
    DataPackageCheckSerializer,
    DataPackageLogSerializer,
)


class DataPackageCheckSchema(AutoSchema):
    def get_operation_id(self, path, method):
        return f"check{self.get_operation_id_base(path, method, 'check')}"


class DataPackageCheckViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = DataPackage.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = DataPackageCheckSerializer
    schema = DataPackageCheckSchema()

    def create(self, request, *args, **kwargs):
        r = super().create(request, *args, **kwargs)
        r.status_code = status.HTTP_200_OK
        return r


class DataPackageLogViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (IsStaff | permissions.DjangoModelPermissions,)
    serializer_class = DataPackageLogSerializer
    queryset = DataPackageTrace.objects.all()

    # FIXME: The return type is wrong!
    # The OpenAPI schema promises the result of `DataPackageLogSerializer`,
    # not `DataPackage`.
    # However, we depend on `DataPackage` in `stransfer-sync`, so we can't
    # change yet.
    # https://gitlab.com/biomedit/libbiomedit/-/blob/
    # d5bc8aedff1957d6811c912712e883a7a4de1b3c/libbiomedit/portal.py#L35
    def create(self, request, *args, **kwargs):  # noqa: ARG002 unused-method-argument
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        trace = serializer.save()
        data_package = trace.data_package
        data_package.refresh_from_db()
        return Response(
            DataPackageSerializer(data_package).data,
            status=status.HTTP_201_CREATED,
        )


class DataPackageViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DataPackageSerializer
    filter_backends = (CaseInsensitiveOrderingFilter,)
    ordering_fields = ("file_name",)

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.serializer_class.prefetch(DataPackage.objects)
        requestor = self.request.user
        users_pkgs = Q(
            data_transfer__project__users__user__username=requestor.username,
            data_transfer__project__users__role=ProjectRole.DM.value,
        )
        node_permission_pkgs = Q(
            data_transfer__project__destination__in=get_node_permission_nodes(
                requestor
            ).all()
        )
        data_providers = get_data_provider_permission_data_providers(requestor)
        data_provider_pkgs = Q(data_transfer__data_provider__in=data_providers)
        return DataPackage.objects.filter(
            users_pkgs | node_permission_pkgs | data_provider_pkgs
        ).distinct()
