from django.db.models import Q
from django.utils import timezone
from rest_framework import viewsets

from ..models.feed import Feed
from ..permissions import IsStaff, ReadOnly
from ..serializers import FeedSerializer


class FeedViewSet(viewsets.ModelViewSet):
    permission_classes = ((IsStaff | ReadOnly),)
    serializer_class = FeedSerializer

    def get_queryset(self):
        now = timezone.now()
        return Feed.objects.filter(
            Q(from_date__lt=now),
            Q(until_date=None) | Q(until_date__gt=now),
        ).order_by("-label", "-from_date")
