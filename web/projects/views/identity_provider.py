from rest_framework import viewsets

from ..models.identity_provider import IdentityProvider
from ..permissions import ReadOnly
from ..serializers.identity_provider import IdentityProviderSerializer
from ..filters.common import CaseInsensitiveOrderingFilter


class IdentityProviderViewSet(viewsets.ModelViewSet):
    permission_classes = (ReadOnly,)
    serializer_class = IdentityProviderSerializer
    queryset = IdentityProvider.objects.all()
    filter_backends = (CaseInsensitiveOrderingFilter,)
    ordering_fields = ("name",)
