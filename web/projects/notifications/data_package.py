from . import EmailScope
from .data_transfer import create_subject
from ..models.data_transfer import DataPackage


def create_data_package_landed_notification(dpkg: DataPackage) -> tuple[str, str]:
    data_transfer = dpkg.data_transfer
    subject = create_subject(data_transfer, EmailScope.INFO)
    body = f"""
Dear All,

The following data package was successfully delivered. The data package will
soon be available in the project space for decryption.

- DTR ID: {data_transfer.id}
- Project: {data_transfer.project.name}
- source: {data_transfer.data_provider.name}
- destination: {data_transfer.project.destination.name}
- sender: {str(dpkg.sender)}
- recipients: {", ".join(str(recipient) for recipient in dpkg.recipients)}
- filename: {dpkg.file_name} 

Kind Regards,
"""
    return subject, body
