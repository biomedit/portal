from django.conf import settings
from django.contrib.auth import get_user_model
from django_drf_utils.email import sendmail
from ..models.project import Project, ProjectRole

User = get_user_model()


def send_email_notification_ssh_key(project: Project, user: User, operation: str):
    node_ticketing_email = project.destination.ticketing_system_email
    permissions_leaders_email = [
        user.email for user in project.users_by_role(ProjectRole.PM)
    ]
    all_recipients = list(
        set(permissions_leaders_email + [user.email] + [node_ticketing_email])
    )
    subject = f"[BioMedIT portal] - user has {operation}"
    body = (
        f"Please be informed that {user.first_name} {user.last_name} ({user.email}) "
        f"has updated their SSH key for the project '{project.name}'.\n\n"
        "Kind Regards,\n"
        "The BioMedIT Team"
    )
    sendmail(
        subject=subject,
        body=body,
        recipients=all_recipients,
        email_cfg=settings.CONFIG.email,
    )
