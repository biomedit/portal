from itertools import groupby

from django.conf import settings
from django.contrib.auth import get_user_model

from ..models.project import Project, ProjectRole

User = get_user_model()
TupleUserRole = tuple[User, ProjectRole]
PermissionChanges = set[TupleUserRole]


def pretty_print_permission_changes(permission_changes: PermissionChanges):
    return "\n".join(
        [
            f"User: {user.profile.display_name} "
            f"({user.profile.affiliation or 'no affiliation'})\t"
            f"Permissions: {', '.join([perm.label for _, perm in permissions])}"
            for (user, permissions) in groupby(permission_changes, lambda x: x[0])
        ]
    )


def update_permissions_notification(
    project: Project,
    requestor_name: str,
    created_permissions: PermissionChanges,
    deleted_permissions: PermissionChanges,
) -> str:
    def pretty_print_permission_changes_multiline(
        permission_changes: PermissionChanges,
    ):
        return "\n".join(
            [
                f"User: {user.profile.display_name} "
                f"({user.profile.affiliation or 'no affiliation'})\t"
                f"Permission: {permission.label}"
                for (user, permission) in permission_changes
            ]
        )

    message = [
        f"Project code: {project.code}",
        f"Project name: {project.name}",
        f"Changed by: {requestor_name}\n",
    ]

    if created_permissions:
        message.append(
            "Added permissions:\n"
            f"{pretty_print_permission_changes_multiline(created_permissions)}"
        )

    if created_permissions and deleted_permissions:
        message.append("\n")

    if deleted_permissions:
        message.append(
            "Deleted permissions:\n"
            f"{pretty_print_permission_changes_multiline(deleted_permissions)}"
        )
    return "\n".join(message)


def review_user_roles_notification(project_name, project_user_roles: PermissionChanges):
    subject = f"Periodic review of user roles for the project {project_name}"
    message = [
        f"""Dear project lead and permission manager of project {project_name},

This is a bi-annual email to remind you about user roles in you project.
Please review the list of users and their permissions.
In case of any discrepancy, reach out to BioMedIT helpdesk \
({settings.CONFIG.notification.ticket_mail})."""
    ]

    def by_project_role(user_role: TupleUserRole):
        return user_role[1].value

    def by_display_name(user: User):
        return user.profile.display_name

    permissions_list = [
        "\n".join(
            [f"\n{ProjectRole(role).label}"]
            + [
                user.profile.display_name
                for user in sorted(
                    (user for user, _ in user_roles),
                    key=by_display_name,
                )
            ]
        )
        for role, user_roles in groupby(
            sorted(project_user_roles, key=by_project_role, reverse=True),
            key=by_project_role,
        )
    ]
    body = "\n".join(message + permissions_list)

    return subject, body


def project_expiration_notification(project_name, expiration_date):
    subject = f"Project {project_name} is about to expire"
    message = f"""
Dear project leader and permission manager of project {project_name},

This is a notification to remind you that your project is about to expire on
{expiration_date}. If you need to extend this date or have any questions, please
contact BioMedIT helpdesk ({settings.CONFIG.notification.ticket_mail}).
"""
    return subject, message
