from itertools import chain

from django.conf import settings
from django.contrib.auth import get_user_model

from . import EmailScope
from .data_transfer import create_subject, project_metadata
from ..models.approval import (
    NodeApproval,
    DataProviderApproval,
    BaseApproval,
    GroupApproval,
)
from ..models.data_transfer import DataTransfer
from ..models.user import to_display_name

User = get_user_model()


def create_reject_notification(approval: BaseApproval, user: User) -> tuple[str, str]:
    dtr = approval.data_transfer
    subject = f"{create_subject(dtr, EmailScope.INFO)} is rejected"
    body = f"""Dear all,

Please be informed that the DTR {dtr.id} has been rejected by {user}. \
Reason: {approval.rejection_reason}

Project:

{project_metadata(dtr)}"""
    return subject, body


def create_approve_notification(
    approval: BaseApproval, user: User, base_url: str
) -> tuple[str, str]:
    dtr = approval.data_transfer
    subject = create_subject(dtr, EmailScope.INFO)
    project_space_ready = (
        "  - A B-space has been set up in the BioMedIT node: Yes"
        if (isinstance(approval, NodeApproval))
        else ""
    )
    legal_basis = (
        "  - There is a legal basis for the data transfer: Yes"
        if dtr.purpose == DataTransfer.PRODUCTION
        and not approval.is_data_specification_approval()
        else ""
    )
    technical_measures = (
        "  - All technical measures are in place to send the data: Yes"
        if not isinstance(approval, GroupApproval)
        else ""
    )
    data_specification = (
        " - The data specification of the Data Transfer has been approved: Yes"
        if (
            dtr.purpose == DataTransfer.PRODUCTION
            and approval.is_data_specification_approval()
        )
        else ""
    )
    data_delivery_approved = (
        " - Internal governance processes have been executed to authorize the "
        "delivery of the requested data in accordance with the project's "
        "specification: Yes"
        if (
            dtr.purpose == DataTransfer.PRODUCTION
            and isinstance(approval, DataProviderApproval)
        )
        else ""
    )
    body = f"""Dear all,
    
On behalf of '{approval.institution}', I, {to_display_name(user)}, hereby confirm:

{legal_basis}
{technical_measures}
{project_space_ready}
{data_specification}
{data_delivery_approved}

DTR details:

{project_metadata(dtr)}

{base_url}data-transfers/{dtr.id}

Kind Regards,"""
    return subject, body


def create_final_approval_notification(
    dtr: DataTransfer,
) -> tuple[str, str]:
    prj = dtr.project
    data_managers = ", ".join(
        [to_display_name(user) for user in dtr.data_recipients.all()]
        + [to_display_name(dtr.requestor)]
    )
    dp = dtr.data_provider
    subject = f"{create_subject(dtr, EmailScope.INFO)} is approved"
    approvals = "\n".join(
        f"- {institution} by {user.profile.display_name} at {ts:%Y-%m-%d %H:%M:%S}"
        for (institution, user, ts) in chain(
            (
                (
                    approval.node.name,
                    approval.history.latest().history_user,
                    approval.change_date,
                )
                for approval in NodeApproval.objects.filter(data_transfer=dtr)
            ),
            (
                (
                    approval.data_provider.name,
                    approval.history.latest().history_user,
                    approval.change_date,
                )
                for approval in DataProviderApproval.objects.filter(data_transfer=dtr)
            ),
            (
                (
                    approval.group.name,
                    approval.history.latest().history_user,
                    approval.change_date,
                )
                for approval in GroupApproval.objects.filter(data_transfer=dtr)
            ),
        )
    )
    data_specification_text = (
        f"""The transferred data will be in accordance with the specification
outlined here:
{dtr.data_specification}. 
"""
        if (dtr.purpose == DataTransfer.PRODUCTION and dtr.data_specification)
        else ""
    )
    body = f"""Dear All,

Clearance has been granted to transfer data in accordance with DTR \
{dtr.id} for project {prj.name} ({prj.code}).

The DP Data Engineer from {dp.name} is now authorized to encrypt, sign, and
transfer the data package(s) using the sett tool according with the agreed
procedures to the {prj.destination.name} node. Detailed instructions on how to
use sett can be found at https://biomedit.gitlab.io/docs/sett/.

{data_specification_text}

Upon completion of the data package transfer, please inform the Project's Data
Manager(s), {data_managers}, so that they can confirm the reception, integrity,
and successful decryption of the data package in the B-space.

For any further questions, please don't hesitate to contact
{settings.CONFIG.notification.ticket_mail}.

Kind Regards

BioMedIT Team

Approval log:

{approvals}
"""

    return subject, body
