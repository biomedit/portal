from django.conf import settings

from . import EmailScope
from ..models.approval import BaseApproval
from ..models.data_transfer import DataTransfer
from ..models.project import ProjectRole
from ..models.data_provider import DataProvider


def create_data_transfer_creation_notification(dtr: DataTransfer) -> tuple[str, str]:
    subject = create_subject(dtr, EmailScope.INFO)

    from_text = "the Nodes"
    if g := dtr.project.data_specification_approval_group:
        from_text += f", {g.name}"
    if g := dtr.project.legal_approval_group:
        from_text += f", {g.name}"

    body = f"""Dear {dtr.data_provider.name},

We have received the following Data Transfer Request (DTR-{dtr.id}) from the \
{dtr.project.code} Project: 

{project_metadata(dtr)}

Please note that Data Provider approval will only be available upon completion
of approvals from {from_text}. 

If you have any questions or need support, please contact \
{settings.CONFIG.notification.ticket_mail}

Kind Regards,

BioMedIT Team

"""
    return subject, body


def create_data_transfer_confirm_creation_notification(
    dtr: DataTransfer,
) -> tuple[str, str]:
    subject = create_subject(dtr, EmailScope.INFO)
    body = f"""Dear {dtr.requestor},

Your Data Transfer Request (DTR-{dtr.id}) has been successfully submitted. 

DTR details:
{project_metadata(dtr)}

You will be informed about the status of the approval process.

If you have any questions or need support, please contact \
{settings.CONFIG.notification.ticket_mail}. 

Kind Regards, 

BioMedIT Team 

"""

    return subject, body


SEP = ", "


def dp_coordinators(dtr):
    return SEP.join(
        f"{coordinator.first_name} {coordinator.last_name} ({coordinator.email})"
        for coordinator in dtr.data_provider.coordinators
    )


def project_leads(dtr: DataTransfer) -> str:
    return SEP.join(
        f"{user.first_name} {user.last_name} ({user.email})"
        for user in dtr.project.users_by_role(ProjectRole.PL)
    )


def create_data_transfer_approval_request_notification(
    dtr: DataTransfer,
    base_url: str,
    approval: BaseApproval,
) -> tuple[str, str]:
    approver = approval.institution
    subject = create_subject(dtr, EmailScope.ACTION_NEEDED)
    data_specification_link = ""
    data_provider_warning = ""
    if (
        isinstance(approver, DataProvider) or approval.is_data_specification_approval()
    ) and dtr.purpose == DataTransfer.PRODUCTION:
        if dtr.data_specification:
            data_specification_link = f"""
Data specification of this DTR are available here:
{dtr.data_specification}
        """
        if isinstance(approver, DataProvider):
            data_provider_warning = """
Important: This technical DTRs should be approved only after the UH internal
(governance) processes have been initiated and completed.
"""

    body = f"""
Dear {approver.name},

You are kindly requested to review and approve or reject the Data Transfer
Request {dtr.id} associated with the {dtr.project.code} project by using the
following Data Transfer Request Approval form: 

{base_url}data-transfers/{dtr.id}

Below are the details of the Data Transfer Request (DTR-ID): 

{project_metadata(dtr)}

{data_specification_link}

{data_provider_warning}

If you have any questions or need support, please contact \
{settings.CONFIG.notification.ticket_mail}

Kind Regards, 

BioMedIT Team 

"""
    return subject, body


def create_subject(dtr: DataTransfer, scope: EmailScope) -> str:
    match scope:
        case EmailScope.INFO:
            header = "[For information]"
        case EmailScope.ACTION_NEEDED:
            header = "[Action needed]"
    return (
        f"{header} DTR-{dtr.id} - Data Transfer Request from "
        f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
    )


def project_metadata(dtr: DataTransfer):
    return f"""  - Project Name: {dtr.project.name}
  - Project Lead: {project_leads(dtr)}
  - Data Provider and coordinator: {dtr.data_provider.name}, {dp_coordinators(dtr)}
  - Requestor: {dtr.requestor.first_name} {dtr.requestor.last_name} \
({dtr.requestor.email})
  - Frequency of transfer: {dtr.max_packages_repr}
  - Data to be transferred is real patient data: \
{"Yes" if dtr.purpose == DataTransfer.PRODUCTION else "No"}
"""
