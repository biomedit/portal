def contact_form(first_name: str, last_name: str, email: str, message: str) -> str:
    return "\n".join(
        (
            f"First Name: {first_name}",
            f"Last Name: {last_name}",
            f"Email Address: {email}",
            "",
            "Message:",
            message,
        )
    )
