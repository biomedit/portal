from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django_drf_utils.email import sendmail

from identities.models import GroupProfile
from ..models.data_provider import DataProvider, get_data_provider_coordinators
from ..models.user import to_display_name

User = get_user_model()


def send_email_notification_create_data_provider(dataprovider: DataProvider):
    subject = (
        f"[BioMedIT portal] - Data provider '{dataprovider.code}' "
        f"has been added to the BioMedIT portal"
    )
    body = (
        f"Please be informed of the following update:\n\n"
        f"Data Provider: {dataprovider})\n"
        f"Operation: Data Provider has been added\n"
        f"Nodes: {', '.join((str(node) for node in dataprovider.nodes.all())) or '-'}\n"
    )
    for recipient in recipients(dataprovider):
        sendmail(
            subject=subject,
            body=body,
            recipients=(recipient,),
            email_cfg=settings.CONFIG.email,
        )


def recipients(dataprovider: DataProvider):
    yield from (
        [settings.CONFIG.notification.ticket_mail]
        + [node.ticketing_system_email for node in dataprovider.nodes.all()]
    )


def send_notification_add_or_remove_user(
    user: User,
    dataprovider: DataProvider,
    group: Group,
    operation: str,
):
    subject = (
        f"[BioMedIT portal] - User '{to_display_name(user)}' has "
        f"been {operation} the DP Group '{group.name}'"
    )
    body = (
        f"Please be informed of the following update:\n\n"
        f"Data Provider: {dataprovider.name} ({dataprovider.code})\n"
        f"Operation: User {operation} Data Provider group\n"
        f"Data Provider Group: {group.name}\n"
    )
    for recipient in recipients(dataprovider):
        sendmail(
            subject=subject,
            body=body,
            recipients=(recipient,),
            email_cfg=settings.CONFIG.email,
        )


def send_notification_to_dp_manager(
    user: User,
    dataprovider: DataProvider,
    group: Group,
    operation: str,
):
    recipient_emails = []
    dp_manager_groups = Group.objects.filter(
        profile__role=GroupProfile.Role.DATA_PROVIDER_MANAGER
    )
    users_in_dp_manager_groups = []
    for g in dp_manager_groups:
        users_in_dp_manager_groups.extend(g.user_set.all())
    for u in group.user_set.all():
        if u in users_in_dp_manager_groups:
            recipient_emails.append(u.email)
    cc_emails = [
        dp_coordinator.email
        for dp_coordinator in get_data_provider_coordinators(dataprovider)
    ]
    display_name = to_display_name(user)
    g_name = group.name
    sendmail(
        subject=f"[BioMedIT portal] - User '{display_name}' has been {operation} the "
        f"DP Group '{g_name}'",
        body=f"Dear {dataprovider.name},\n\n"
        f"This is an automated message from the BioMedIT portal confirming that "
        f"the user '{display_name}' has been {operation} the Data "
        f"Provider Group '{g_name}'.\n\n"
        "Kind Regards,\n"
        "The BioMedIT Team",
        recipients=recipient_emails,
        cc=cc_emails,
        email_cfg=settings.CONFIG.email,
    )


def send_notification_to_dp_data_engineer(
    user: User,
    dataprovider: DataProvider,
    group: Group,
):
    notification = settings.CONFIG.notification
    biomedit_ticketing_email = notification.ticket_mail
    onboarding_new_dp_data_engineer = (
        notification.links.onboarding_new_dp_data_engineer or "(unspecified)"
    )
    cc_emails = [
        dp_manager.email
        for dp_manager in (
            group.user_set.all()
            if group.profile.role == GroupProfile.Role.DATA_PROVIDER_MANAGER
            else ()
        )
    ]
    cc_emails.extend(
        [
            dp_coordinator.email
            for dp_coordinator in get_data_provider_coordinators(dataprovider)
        ]
    )
    sendmail(
        subject="[BioMedIT portal] - Your Access to the DP Data Engineer Group",
        body=f"Dear {to_display_name(user)},\n\n"
        f"This is an automated message from the BioMedIT portal, confirming "
        f"your successful addition to the Group '{group.name}'. "
        "Notice that to encrypt, sign, and transfer of data, there are "
        "specific technical requirements you need to meet. Please review the "
        "following instructions for complete details:\n"
        f"{onboarding_new_dp_data_engineer}"
        "\n\n"
        f"If you have any questions or encounter any issues, please do not "
        f"hesitate to contact us at {biomedit_ticketing_email}.\n\n"
        "Kind Regards,\n"
        "The BioMedIT Team",
        recipients=(user.email,),
        cc=cc_emails,
        email_cfg=settings.CONFIG.email,
    )
