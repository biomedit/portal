import itertools
from abc import ABC, abstractmethod

from django.conf import settings
from django.contrib.auth import get_user_model
from django_drf_utils.email import sendmail

from ..models.approval import BaseApproval
from ..models.data_transfer import DataTransfer
from ..models.node import get_node_admins
from ..notifications.approval import (
    create_approve_notification,
    create_final_approval_notification,
    create_reject_notification,
)
from ..notifications.data_transfer import (
    create_data_transfer_approval_request_notification,
    create_data_transfer_confirm_creation_notification,
    create_data_transfer_creation_notification,
)


User = get_user_model()


class DtrNotification(ABC):
    """
    Abstract class for data transfer notifications. When inheriting, make sure
    to implement the `create_notification` and `recipients` methods.
    """

    def __init__(self, dtr: DataTransfer, base_url: str) -> None:
        self.dtr = dtr
        self.base_url = base_url
        self.dtr_path = f"data-transfers/{dtr.id}"
        # List of unique nodes
        self.nodes = frozenset(approval.node for approval in dtr.node_approvals.all())
        # List of unique node admins
        self.node_admins = frozenset(
            itertools.chain.from_iterable(get_node_admins(node) for node in self.nodes)
        )
        self.dp_coordinators = list(dtr.data_provider.coordinators)
        self.dp_data_engineers = list(dtr.data_provider.data_engineers)
        self.dp_managers = list(dtr.data_provider.managers)
        self.legal_approval_group_users = (
            self.dtr.project.legal_approval_group.user_set.all()
            if self.dtr.project.legal_approval_group
            else ()
        )
        self.data_specification_approval_group_users = (
            self.dtr.project.data_specification_approval_group.user_set.all()
            if self.dtr.project.data_specification_approval_group
            else ()
        )

    @abstractmethod
    def create_notification(self) -> tuple[str, str]:
        """Generate subject and body for the notification"""

    @abstractmethod
    def recipients(self) -> frozenset[str]:
        """Generate the list of recipients for the notification"""

    def cc_recipients(self) -> frozenset[str]:
        return frozenset()

    def all_recipients(self) -> frozenset[str]:
        """Returns the full list of interested parties for a given data transfer"""

        recipients = frozenset(
            (
                self.dtr.project.destination.ticketing_system_email,
                settings.CONFIG.notification.ticket_mail,
                self.dtr.requestor.email,
            )
        ).union(
            (user.email for user in self.node_admins),
            (user.email for user in self.dp_coordinators),
            (user.email for user in self.dp_data_engineers),
            (user.email for user in self.dp_managers),
        )

        if self.dtr.purpose == DataTransfer.PRODUCTION:
            # Legal and data specification approval group only gets notified in
            # case of a `PRODUCTION` DTR
            return recipients.union(
                (user.email for user in self.legal_approval_group_users),
                (user.email for user in self.data_specification_approval_group_users),
            )
        return recipients

    def sendmail(self) -> None:
        subject, body = self.create_notification()
        for recipient in self.recipients():
            sendmail(
                subject=subject,
                body=body,
                recipients=(recipient,),
                email_cfg=settings.CONFIG.email,
                reply_to=(settings.CONFIG.notification.ticket_mail,),
            )
        for cc_recipient in self.cc_recipients():
            sendmail(
                subject=subject,
                body=body,
                cc=(cc_recipient,),
                email_cfg=settings.CONFIG.email,
                reply_to=(settings.CONFIG.notification.ticket_mail,),
            )


class DtrCreatedNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_data_transfer_creation_notification(self.dtr)

    def recipients(self) -> frozenset[str]:
        user_recipients = frozenset().union(
            self.dp_coordinators, self.dp_data_engineers, self.dp_managers
        )
        return frozenset(user.email for user in user_recipients)


class DtrConfirmCreationNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_data_transfer_confirm_creation_notification(self.dtr)

    def recipients(self) -> frozenset[str]:
        return frozenset(
            (settings.CONFIG.notification.ticket_mail, self.dtr.requestor.email)
        )


class DtrApprovalNotification(DtrNotification):
    """
    Abstract class for data transfer approval notifications. When inheriting,
    make sure to implement the `recipients` methods.
    """

    def __init__(
        self,
        base_url: str,
        approval: BaseApproval,
    ) -> None:
        super().__init__(approval.data_transfer, base_url)
        self.approval = approval

    def create_notification(self) -> tuple[str, str]:
        return create_data_transfer_approval_request_notification(
            self.dtr, self.base_url, self.approval
        )


class DtrNodeApprovalNotification(DtrApprovalNotification):
    def recipients(self) -> frozenset[str]:
        node = self.approval.institution
        return frozenset(
            [node.ticketing_system_email]
            + [user.email for user in get_node_admins(node)]
        )


class DtrGroupApprovalNotification(DtrApprovalNotification):
    def recipients(self) -> frozenset[str]:
        return frozenset(
            user.email for user in self.approval.institution.user_set.all()
        )


class DtrDataProviderApprovalNotification(DtrApprovalNotification):
    def recipients(self) -> frozenset[str]:
        return frozenset(user.email for user in self.dp_coordinators)

    def cc_recipients(self) -> frozenset[str]:
        return super().cc_recipients().union(user.email for user in self.dp_managers)


class DtrApprovalSubmittedNotification(DtrNotification):
    """
    Abstract class for data transfer approval submission notifications. When
    inheriting, make sure to implement the `create_notification` and
    `recipients` methods.
    """

    def __init__(
        self,
        base_url: str,
        approval: BaseApproval,
        user: User,
    ) -> None:
        super().__init__(approval.data_transfer, base_url)
        self.approval = approval
        self.user = user


class DtrApprovedNotification(DtrApprovalSubmittedNotification):
    """Approval has been approved."""

    def create_notification(self) -> tuple[str, str]:
        return create_approve_notification(self.approval, self.user, self.base_url)

    def recipients(self) -> frozenset[str]:
        return frozenset((self.dtr.requestor.email,))


class DtrRejectedNotification(DtrApprovalSubmittedNotification):
    """Approval has been rejected."""

    def create_notification(self) -> tuple[str, str]:
        return create_reject_notification(self.approval, self.user)

    def recipients(self) -> frozenset[str]:
        return self.all_recipients()


class DtrAuthorizedNotification(DtrNotification):
    def create_notification(self) -> tuple[str, str]:
        return create_final_approval_notification(self.dtr)

    def recipients(self) -> frozenset[str]:
        return self.all_recipients()
