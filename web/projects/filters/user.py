from rest_framework.filters import BaseFilterBackend

from .common import BaseLookupFilter, QueryParam
from ..models.project import ProjectRole, ProjectUserRole


class UserActiveFilter(BaseFilterBackend):
    """Filter projects depending on the user being active (`is_active` is `True`)."""

    query_param = "include_inactive"

    def filter_queryset(
        self,
        request,
        queryset,
        view,  # noqa: ARG002 unused-method-argument
    ):
        if request.method != "GET" or request.query_params.get(self.query_param):
            return queryset
        # exclude inactive users by default
        return queryset.exclude(is_active=False)

    def get_schema_operation_parameters(
        self,
        view,  # noqa: ARG002 unused-method-argument
    ):
        return (
            {
                "name": self.query_param,
                "required": False,
                "in": "query",
                "description": (
                    "Includes users which are not active (`is_active` is `False`)"
                ),
                "schema": {"type": "boolean"},
            },
        )


class UserProjectRoleFilter(BaseFilterBackend):
    """
    Return users which have the specified role in the specified project.
    If no role is specified, all users with any role in the specified project
    are returned.
    """

    project_id_query_param = "project_id"
    role_query_param = "role"
    query_params = (project_id_query_param, role_query_param)

    def filter_queryset(
        self,
        request,
        queryset,
        view,  # noqa: ARG002 unused-method-argument
    ):
        project_id: str | None = request.query_params.get(self.project_id_query_param)
        role: str | None = request.query_params.get(self.role_query_param)
        kwargs = {}
        if role:
            kwargs["role"] = ProjectRole[role].value
        if project_id:
            kwargs["project__id"] = project_id
        if kwargs:
            return queryset.filter(
                id__in=(pur.user.id for pur in ProjectUserRole.objects.filter(**kwargs))
            )
        return queryset.all()

    def get_schema_operation_parameters(
        self,
        view,  # noqa: ARG002 unused-method-argument
    ):
        return (
            {
                "name": self.project_id_query_param,
                "required": False,
                "in": "query",
                "description": (
                    "Only includes users which have at least one role in this project."
                ),
                "schema": {"type": "integer"},
            },
            {
                "name": self.role_query_param,
                "required": False,
                "in": "query",
                "description": (
                    "Only includes users which have at least this role in any project."
                ),
                "schema": {"type": "string", "enum": [p.name for p in ProjectRole]},
            },
        )


class UserLookupFilter(BaseLookupFilter):
    """
    Filters users by `first_name`, `last_name`, `profile.affiliation`, `email`
    and `group`.
    """

    query_params = [
        QueryParam(
            "first_name",
            "first_name__iexact",
            "Includes only users with specified first name",
        ),
        QueryParam(
            "last_name",
            "last_name__iexact",
            "Includes only users with specified last name",
        ),
        QueryParam(
            "affiliation",
            ("profile__affiliation__icontains", "profile__affiliation_id__icontains"),
            "Includes only users for whom at least one of the linked "
            "affiliations matches (or contains) the provided string",
        ),
        QueryParam(
            "username",
            "username__exact",
            "Includes only users with specified username",
        ),
        QueryParam(
            "email",
            ("email__iexact", "profile__emails__list_icontains"),
            "Returns the first user matching given email address. "
            "We first try to exactly match the official (or main) email address. "
            "As fallback we scan the provided of additional email addresses.",
        ),
    ]
