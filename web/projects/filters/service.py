from .common import BaseLookupFilter, QueryParam


class ServiceLookupFilter(BaseLookupFilter):
    """Filters service by `name`"""

    query_params = [
        QueryParam(
            "name",
            "name__iexact",
            "Includes only services with specified name",
        ),
    ]
