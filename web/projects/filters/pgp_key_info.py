from .common import BaseLookupFilter, QueryParam


class PgpKeyInfoLookupFilter(BaseLookupFilter):
    """Filters users by `first_name`, `last_name` and `profile.affiliation`"""

    query_params = [
        QueryParam(
            "fingerprint",
            "fingerprint__iexact",
            "Includes only info about OpenPGP keys with specified fingerprint",
        ),
        QueryParam(
            "key_email",
            "key_email__iexact",
            "Includes only info about OpenPGP keys verified with specified email",
        ),
        QueryParam(
            "key_user_id",
            "key_user_id__iexact",
            "Includes only info about OpenPGP keys with User ID",
        ),
        QueryParam(
            "status",
            "status__iexact",
            "Includes only info about OpenPGP keys in specified status",
        ),
        QueryParam(
            "username",
            "user__username__iexact",
            "Includes only info about OpenPGP keys associated to user with specified"
            " username",
        ),
    ]
