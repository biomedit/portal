from django.contrib.auth import get_user_model
from rest_framework.filters import BaseFilterBackend

from ..models.data_provider import (
    get_data_provider_permission_data_providers,
)

User = get_user_model()


class DataProviderPermissionsFilter(BaseFilterBackend):
    """Filters out all data providers the user has permission to"""

    param = "username"

    def filter_queryset(
        self,
        request,
        queryset,
        view,  # noqa: ARG002 unused-method-argument
    ):
        username = request.query_params.get(self.param)
        if username:
            return get_data_provider_permission_data_providers(
                User.objects.get(username=username)
            )
        return queryset

    def get_schema_operation_parameters(
        self,
        view,  # noqa: ARG002 unused-method-argument
    ):
        return (
            {
                "name": self.param,
                "required": False,
                "in": "query",
                "description": (
                    "Only returns data providers which the user has permission to view"
                    " or edit."
                ),
                "schema": {"type": "string"},
            },
        )
