import operator
from dataclasses import dataclass
from functools import reduce

from django.db.models import Q
from django.db.models.constants import LOOKUP_SEP
from django.db.models.functions import Lower
from rest_framework.filters import BaseFilterBackend, OrderingFilter
from rest_framework.request import Request


@dataclass
class QueryParam:
    name: str
    lookup: str | tuple[str, ...]
    description: str
    type: str = "string"


class BaseLookupFilter(BaseFilterBackend):
    """
    Filters entity using just lookups.
    Inheriting class should initialize `query_params`.
    """

    query_params: list[QueryParam]

    def _get_lookups(self, request: Request):
        def reduce_or(lookups, key, value):
            return reduce(
                operator.or_, [Q(**{key(lookup): value}) for lookup in lookups]
            )

        def get_lookup(param: QueryParam):
            name = param.name
            value: str = request.query_params.get(name)
            lookups = (
                param.lookup if isinstance(param.lookup, tuple) else (param.lookup,)
            )
            if param.type == "string" or value != "":
                return reduce_or(lookups, lambda x: x, value)
            return reduce_or(
                lookups,
                lambda x: x[: param.lookup.rfind(LOOKUP_SEP)] + LOOKUP_SEP + "isnull",
                True,
            )

        return [
            get_lookup(param)
            for param in self.query_params
            if request.query_params.get(param.name, None) is not None
        ]

    def filter_queryset(self, request, queryset, _):
        if lookups := self._get_lookups(request):
            return queryset.filter(reduce(operator.and_, lookups)).distinct()
        return queryset

    def get_schema_operation_parameters(self, _):
        return [
            {
                "name": param.name,
                "required": False,
                "in": "query",
                "description": param.description,
                "schema": {"type": param.type},
            }
            for param in self.query_params
        ]


class CaseInsensitiveOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)
        if ordering:
            return queryset.order_by(
                *[
                    Lower(f[1:]).desc() if f.startswith("-") else Lower(f)
                    for f in ordering
                ]
            )
        return queryset
