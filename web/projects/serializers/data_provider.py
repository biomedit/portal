from django.db import transaction
from rest_framework import serializers

from .user import UserShortSerializer
from ..models.data_provider import DataProvider
from ..models.node import Node

from projects.notifications.dp_group_notification import (
    send_email_notification_create_data_provider,
)


class DataProviderShortSerializer(serializers.ModelSerializer):
    nodes = serializers.SlugRelatedField(
        slug_field="code",
        many=True,
        queryset=Node.objects.all(),
    )

    class Meta:
        model = DataProvider
        read_only_fields = ("id",)
        fields = read_only_fields + (
            "code",
            "name",
            "enabled",
            "nodes",
        )


class DataProviderSerializer(DataProviderShortSerializer):
    coordinators = UserShortSerializer(
        many=True,
        read_only=True,
        help_text="Users who are allowed to change data transfer approval status.",
    )

    class Meta(DataProviderShortSerializer.Meta):
        coordinators = ("coordinators",)
        read_only_fields = (
            DataProviderShortSerializer.Meta.read_only_fields + coordinators
        )
        fields = DataProviderShortSerializer.Meta.fields + coordinators

    @transaction.atomic
    def create(self, validated_data):
        data_provider = super().create(validated_data)
        send_email_notification_create_data_provider(data_provider)
        return data_provider
