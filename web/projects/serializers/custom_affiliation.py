from rest_framework import serializers

from ..models.custom_affiliation import CustomAffiliation


class CustomAffiliationSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomAffiliation
        read_only_fields = ("id", "code", "name")
        fields = read_only_fields
