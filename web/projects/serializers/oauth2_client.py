from rest_framework import serializers

from identities.models import OAuth2Client


class OAuth2ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = OAuth2Client
        read_only_fields = ("id", "client_name", "redirect_uri")
        fields = read_only_fields
