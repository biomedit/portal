import logging

from django.db import transaction
from django.db.models import QuerySet
from django_drf_utils.serializers.utils import (
    DetailedValidationError,
    get_request_username,
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from identities.serializers.group import NestedUserSerializer
from .approval import (
    NodeApprovalSerializer,
    DataProviderApprovalSerializer,
    block_approvals,
    GroupApprovalSerializer,
)
from .data_package import DataPackageSerializer
from ..models.approval import DataProviderApproval, NodeApproval, GroupApproval
from ..models.data_provider import DataProvider
from ..models.data_transfer import DataPackage, DataTransfer
from ..models.project import ProjectRole, ProjectUserRole
from ..notifications.dtr_notification import (
    DtrConfirmCreationNotification,
    DtrCreatedNotification,
    DtrGroupApprovalNotification,
    DtrNodeApprovalNotification,
)

logger = logging.getLogger(__name__)

CREATE_MESSAGE_ARCHIVED_PROJECT = "Cannot create data transfer for an archived project."
UPDATE_MESSAGE_ARCHIVED_PROJECT = "Cannot update data transfer of an archived project."


class DataPackageLite(serializers.ModelSerializer):
    class Meta:
        model = DataPackage
        read_only_fields = ("file_name", "id")
        fields = read_only_fields


class DataTransferLite(serializers.ModelSerializer):
    class Meta:
        model = DataTransfer
        read_only_fields = (
            "id",
            "change_date",
            "data_provider_name",
            "max_packages",
            "packages",
            "project_name",
            "purpose",
            "requestor_full_name",
            "status",
            "data_provider_code",
        )
        fields = read_only_fields

    data_provider_name = serializers.CharField(
        source="data_provider.name", read_only=True
    )

    data_provider_code = serializers.CharField(
        source="data_provider.code", read_only=True
    )

    packages = DataPackageLite(
        many=True,
        read_only=True,
    )
    project_name = serializers.CharField(source="project.name", read_only=True)
    requestor_full_name = serializers.SerializerMethodField()

    def get_requestor_full_name(self, dtr: DataTransfer) -> str:
        return f"{dtr.requestor.first_name} {dtr.requestor.last_name}"

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related(
            "data_provider", "requestor", "project", "packages"
        )


class DataTransferSerializer(DataTransferLite):
    class Meta:
        model = DataTransfer
        read_only_fields = (
            "id",
            "packages",
            "node_approvals",
            "data_provider_approvals",
            "data_provider_name",
            "group_approvals",
            "requestor_name",
            "requestor_display_id",
            "requestor_first_name",
            "requestor_full_name",
            "requestor_last_name",
            "requestor_email",
            "project_name",
            "project_archived",
            "permissions",
        )
        fields = read_only_fields + (
            "project",
            "max_packages",
            "status",
            "data_provider",
            "requestor",
            "purpose",
            "legal_basis",
            "creation_date",
            "change_date",
            "data_specification",
            "data_recipients",
        )
        extra_kwargs = {"requestor": {"required": False}}

    # Sets `required: true` in the OpenAPI schema
    max_packages = serializers.IntegerField(required=True)
    data_provider = serializers.SlugRelatedField(
        queryset=DataProvider.objects.all(),
        read_only=False,
        slug_field="code",
        required=True,
    )

    packages = DataPackageSerializer(
        many=True,
        read_only=True,
    )

    data_recipients = NestedUserSerializer(
        many=True,
        label="Data Recipients",
        help_text="Data recipients associated with this data transfer",
        required=False,
    )

    node_approvals = NodeApprovalSerializer(
        many=True,
        read_only=True,
    )

    data_provider_approvals = DataProviderApprovalSerializer(
        many=True,
        read_only=True,
    )

    group_approvals = GroupApprovalSerializer(
        many=True,
        read_only=True,
    )

    requestor_display_id = serializers.CharField(
        source="requestor.profile.display_id", read_only=True
    )
    requestor_first_name = serializers.CharField(
        source="requestor.first_name", read_only=True
    )
    requestor_last_name = serializers.CharField(
        source="requestor.last_name", read_only=True
    )
    requestor_email = serializers.CharField(source="requestor.email", read_only=True)

    project_archived = serializers.BooleanField(
        source="project.archived", read_only=True
    )
    permissions = serializers.SerializerMethodField(
        help_text="Permissions the requestor has over this data transfer object."
    )

    def get_permissions(self, data_transfer: DataTransfer):
        user = self.context["request"].user
        staff = user.is_staff
        data_specification_approval_group = (
            data_transfer.project.data_specification_approval_group
        )

        return {
            "delete": staff,
            "edit": {
                "max_packages": staff,
                "data_provider": staff,
                "purpose": False,
                "status": staff,
                "requestor": staff,
                "legal_basis": staff,
                "data_specification": staff
                or (
                    data_specification_approval_group
                    and user in data_specification_approval_group.user_set.all()
                ),
                "data_recipients": staff,
            },
        }

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related(
            "node_approvals__node",
            "data_provider_approvals__data_provider",
            "packages__trace",
            "data_provider__nodes",
        ).select_related("project__destination", "requestor__profile")

    def validate(self, attrs):
        purpose = attrs.get("purpose")
        project = attrs.get("project")
        if (
            purpose is not None  # PATCH request may not contain `purpose`
            and purpose == DataTransfer.PRODUCTION
        ):
            if project.legal_approval_group and not attrs.get("legal_basis", ""):
                raise ValidationError("This data transfer requires a legal basis.")
            if project.data_specification_approval_group and not attrs.get(
                "data_specification", ""
            ):
                raise ValidationError(
                    "This data transfer requires a data specification."
                )
        if data_recipients := attrs.get("data_recipients"):
            data_managers = project.users_by_role(ProjectRole.DM)
            if invalid_recipients := set(data_recipients) - set(data_managers):
                raise ValidationError(
                    f"Data recipients should be data managers of the project. "
                    f"Invalid data recipients: {invalid_recipients}"
                )
        return super().validate(attrs)

    @transaction.atomic
    def create(self, validated_data):
        if validated_data["project"].archived:
            raise ValidationError(CREATE_MESSAGE_ARCHIVED_PROJECT)
        if validated_data.get("status", DataTransfer.INITIAL) != DataTransfer.INITIAL:
            raise DetailedValidationError(
                "Cannot create new transfer in state different than 'initial'",
                field="status",
            )
        request_user = get_request_username(self)
        requestor = validated_data.setdefault("requestor", request_user)
        if requestor != request_user and not request_user.is_staff:
            raise DetailedValidationError(
                "Not allowed to specify requestor", field="requestor"
            )
        if not ProjectUserRole.objects.filter(
            project=validated_data["project"], user=requestor, role=ProjectRole.DM.value
        ).exists():
            raise DetailedValidationError(
                "Requestor is not a data manager of the project", field="requestor"
            )
        # drf does not support writable nested fields by default
        data_recipients = validated_data.pop("data_recipients", {})
        instance = super().create(validated_data)
        if data_recipients:
            instance.data_recipients.set(data_recipients)
        self.create_approvals(instance)
        self.notify_on_creation(
            instance, self.context["request"].build_absolute_uri("/")
        )
        return instance

    @transaction.atomic
    def update(self, instance: DataTransfer, validated_data):
        if instance.project.archived:
            raise ValidationError(UPDATE_MESSAGE_ARCHIVED_PROJECT)
        if validated_data.get("purpose", instance.purpose) != instance.purpose:
            raise DetailedValidationError(
                "Cannot update data transfer's purpose",
                field="purpose",
            )
        new_status: str = validated_data.get("status")
        # If we try to change the status to `UNAUTHORIZED`
        if (
            instance.status is not new_status
            and new_status == DataTransfer.UNAUTHORIZED
        ):
            block_approvals(instance)
        # drf does not support writable nested fields by default
        data_recipients = validated_data.pop("data_recipients", None)
        if data_recipients is not None:
            instance.data_recipients.set(data_recipients)
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["packages"] = sorted(
            response["packages"], key=lambda x: x["file_name"]
        )
        return response

    @staticmethod
    def create_approvals(data_transfer: DataTransfer):
        approvals = [
            DataProviderApproval(
                data_transfer=data_transfer, data_provider=data_transfer.data_provider
            ),
            NodeApproval(
                node=data_transfer.project.destination, data_transfer=data_transfer
            ),
        ]
        # Legal and data specification approval group approvals are needed for
        # `PRODUCTION` DTR only!
        if data_transfer.purpose == DataTransfer.PRODUCTION:
            for group in (
                data_transfer.project.legal_approval_group,
                data_transfer.project.data_specification_approval_group,
            ):
                if group:
                    approvals.append(
                        GroupApproval(data_transfer=data_transfer, group=group)
                    )

        for approval in approvals:
            approval.save()

    @staticmethod
    def notify_on_creation(dtr: DataTransfer, base_url: str):
        DtrCreatedNotification(dtr, base_url).sendmail()
        DtrConfirmCreationNotification(dtr, base_url).sendmail()
        for approval in dtr.node_approvals.all():
            DtrNodeApprovalNotification(base_url, approval).sendmail()
        for approval in dtr.group_approvals.all():
            DtrGroupApprovalNotification(base_url, approval).sendmail()
