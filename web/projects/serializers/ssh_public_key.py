from django.db import transaction
from django_drf_utils.serializers.utils import get_request_username
from rest_framework import serializers

from ..models.ssh_public_key import SSHPublicKey

from projects.notifications.ssh_notification import (
    send_email_notification_ssh_key,
)


class SSHPublicKeySerializer(serializers.ModelSerializer):
    """Serializer for SSHPublicKey objects."""

    username = serializers.CharField(source="user.username", read_only=True)
    project_name = serializers.CharField(source="project.name", read_only=True)

    class Meta:
        model = SSHPublicKey
        read_only_fields = (
            "id",
            "username",
            "project_name",
            "user",
        )
        fields = (
            "project",
            "created",
            "changed",
            "key",
        ) + read_only_fields

    @transaction.atomic
    def create(self, validated_data):
        """Method called by POST requests to the endpoint.
        If an SSH key already exists for that user and project,
        it will be updated instead.
        """
        validated_data["user"] = get_request_username(self)
        existing_ssh = SSHPublicKey.objects.filter(
            user=validated_data["user"],
            project=validated_data["project"],
        ).first()

        if existing_ssh:
            send_email_notification_ssh_key(
                validated_data["project"],
                validated_data["user"],
                "updated their SSH key",
            )
            return super().update(instance=existing_ssh, validated_data=validated_data)

        send_email_notification_ssh_key(
            validated_data["project"], validated_data["user"], "uploaded a new SSH key"
        )
        return super().create(validated_data=validated_data)
