from typing import cast

from django.db import transaction
from django.forms.models import model_to_dict
from django_drf_utils.serializers.utils import (
    get_request_username,
    DetailedValidationError,
)
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from identities.serializers.group import GroupShortSerializer
from .data_provider import DataProviderShortSerializer
from .node import NodeSerializer
from ..models.approval import (
    BaseApproval,
    DataProviderApproval,
    NodeApproval,
    GroupApproval,
)
from ..models.data_provider import has_dp_coordinator_role
from ..models.data_transfer import DataTransfer
from ..notifications.dtr_notification import (
    DtrApprovedNotification,
    DtrAuthorizedNotification,
    DtrDataProviderApprovalNotification,
    DtrRejectedNotification,
)
from ..permissions import User
from ..permissions.node import has_node_admin_permission

APPROVAL_FIELDS = ("id", "status", "can_approve", "rejection_reason")
INSTITUTIONS = ("node", "data_provider", "group")


class ApprovalSerializer(serializers.Serializer):
    id = serializers.IntegerField(
        read_only=True,
        min_value=1,
        help_text="Approval ID in institution approval table",
    )
    institution = serializers.ChoiceField(
        choices=INSTITUTIONS,
        help_text="Institution (either data provider or node)",
    )
    status = serializers.ChoiceField(
        choices=BaseApproval.ApprovalStatus.choices,
        help_text="Approval status",
    )
    existing_legal_basis = serializers.BooleanField(
        write_only=True,
        # Does NOT have to be specified when rejecting
        required=False,
        help_text="Confirmation that a legal basis exists for the data transfer",
    )
    technical_measures_in_place = serializers.BooleanField(
        write_only=True,
        # Does NOT have to be specified when rejecting
        required=False,
        help_text="Confirmation that all technical measures are in place",
    )
    project_space_ready = serializers.BooleanField(
        write_only=True,
        required=False,
        help_text=(
            "Confirmation that project space is ready. Applies to hosting node only."
        ),
    )
    data_delivery_approved = serializers.BooleanField(
        write_only=True,
        required=False,
        help_text="Confirmation that the data delivery has been approved",
    )
    rejection_reason = serializers.CharField(
        # Does NOT have to be specified when approving
        required=False,
        help_text="Reason for rejecting the approval",
    )

    def create(self, validated_data):  # noqa: ARG002 unused-method-argument
        raise MethodNotAllowed(method="POST")

    @transaction.atomic
    def update(
        self,
        instance: BaseApproval,
        validated_data,
    ):
        def validate_approvals(approvals_queryset):
            for approval in approvals_queryset:
                if approval.status != BaseApproval.ApprovalStatus.APPROVED:
                    raise DetailedValidationError(
                        "Data Provider approval must come after all other approvals",
                        field="status",
                    )

        user = get_request_username(self)
        if not can_approve(user, instance):
            raise DetailedValidationError(
                "Only staff or an user with approval permissions can update the status",
                field="status",
            )

        instance.status = validated_data["status"]
        if instance.status == BaseApproval.ApprovalStatus.REJECTED:
            instance.rejection_reason = validated_data["rejection_reason"]
        data_transfer = instance.data_transfer

        if isinstance(instance, DataProviderApproval):
            validate_approvals(data_transfer.node_approvals.all())
            validate_approvals(data_transfer.group_approvals.all())

        declarations = instance.declarations(data_transfer.purpose)
        if instance.status == BaseApproval.ApprovalStatus.APPROVED:
            for attr in declarations:
                if not validated_data.pop(attr, False):
                    raise DetailedValidationError(
                        "NOT all the declarations are fulfilled for approval",
                        field=attr,
                    )

        instance.save()

        # Changes on related DTR
        if instance.status == BaseApproval.ApprovalStatus.REJECTED:
            data_transfer.status = DataTransfer.UNAUTHORIZED
            # All non-handled approvals should be set to `BLOCKED`
            block_approvals(data_transfer)
        elif all(
            approval.status == BaseApproval.ApprovalStatus.APPROVED
            for approval in data_transfer.approvals
        ):
            data_transfer.status = DataTransfer.AUTHORIZED
        data_transfer.save()
        self.notify_on_update(
            instance,
            self.context["request"].build_absolute_uri("/"),
            user,
            instance.status == BaseApproval.ApprovalStatus.APPROVED,
        )
        return instance

    @staticmethod
    def notify_on_update(
        approval: BaseApproval,
        base_url: str,
        user: User,
        approve: bool,
    ):
        dtr_notification = (
            DtrApprovedNotification(base_url, approval, user)
            if approve
            else DtrRejectedNotification(base_url, approval, user)
        )
        dtr_notification.sendmail()

        # Data provider approval notification should be sent only once all other
        # approvals have been submitted.
        if (
            not approval.data_transfer.node_approvals.exclude(
                status=BaseApproval.ApprovalStatus.APPROVED
            ).exists()
            and not approval.data_transfer.group_approvals.exclude(
                status=BaseApproval.ApprovalStatus.APPROVED
            ).exists()
        ):
            for approval in approval.data_transfer.data_provider_approvals.filter(
                status=BaseApproval.ApprovalStatus.WAITING
            ):
                DtrDataProviderApprovalNotification(base_url, approval).sendmail()

        if approval.data_transfer.status == DataTransfer.AUTHORIZED:
            DtrAuthorizedNotification(approval.data_transfer, base_url).sendmail()

    def to_representation(self, instance: NodeApproval | DataProviderApproval):
        return model_to_dict(instance)


class NodeApprovalSerializer(serializers.ModelSerializer):
    node = NodeSerializer(read_only=True)
    can_approve = serializers.SerializerMethodField(
        help_text="Whether current user is allowed to change approval's status"
    )

    def get_can_approve(self, node_approval: NodeApproval) -> bool:
        return can_approve(get_request_username(self), node_approval)

    class Meta:
        model = NodeApproval
        read_only_fields = APPROVAL_FIELDS + (
            "node",
            "type",
            "created",
            "change_date",
        )
        fields = read_only_fields


class DataProviderApprovalSerializer(serializers.ModelSerializer):
    data_provider = DataProviderShortSerializer(read_only=True)
    can_approve = serializers.SerializerMethodField(
        help_text="Whether current user is allowed to change approval's status"
    )

    def get_can_approve(self, data_provider_approval: DataProviderApproval) -> bool:
        return can_approve(get_request_username(self), data_provider_approval)

    class Meta:
        model = DataProviderApproval
        read_only_fields = APPROVAL_FIELDS + (
            "data_provider",
            "created",
            "change_date",
        )
        fields = read_only_fields


class GroupApprovalSerializer(serializers.ModelSerializer):
    group = GroupShortSerializer(read_only=True)
    can_approve = serializers.SerializerMethodField(
        help_text="Whether current user is allowed to change approval's status"
    )

    def get_can_approve(self, group_approval: GroupApproval) -> bool:
        user = get_request_username(self)
        return user.is_staff or can_group_approve(user, group_approval)

    class Meta:
        model = GroupApproval
        read_only_fields = APPROVAL_FIELDS + (
            "group",
            "created",
            "change_date",
        )
        fields = read_only_fields


def can_approve(user: User, approval: BaseApproval):
    return user.is_staff or (
        can_node_approve(user, approval)
        if isinstance(approval, NodeApproval)
        else (
            can_data_provider_approve(user, approval)
            if isinstance(approval, DataProviderApproval)
            else can_group_approve(user, cast(GroupApproval, approval))
        )
    )


def can_group_approve(user: User, approval: GroupApproval):
    return user.groups.filter(name=approval.group).exists()


def can_node_approve(user: User, approval: NodeApproval):
    return has_node_admin_permission(user, approval.node)


def can_data_provider_approve(user: User, approval: DataProviderApproval):
    return has_dp_coordinator_role(user, approval.data_provider)


# Have to keep it here due to circular dependency
def block_approvals(instance: DataTransfer):
    """Sets all approvals related to given data transfer to `BLOCKED`"""
    for approval in filter(
        lambda approval: approval.status == BaseApproval.ApprovalStatus.WAITING,
        instance.approvals,
    ):
        approval.status = BaseApproval.ApprovalStatus.BLOCKED
        approval.save()
