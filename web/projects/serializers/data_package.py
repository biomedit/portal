import hashlib
import json

from django.conf import settings
from django.db import transaction
from django.db.models import QuerySet
from django.utils import dateparse
from django_drf_utils.email import sendmail
from django_drf_utils.serializers.utils import DetailedValidationError
from rest_framework import serializers

from ..models.data_transfer import DataPackage, DataPackageTrace, DataTransfer
from ..models.node import Node
from ..models.pgp import PgpKeyInfo
from ..models.project import Project, ProjectRole
from ..notifications.data_package import create_data_package_landed_notification


class NodeField(serializers.Field):
    """Field for deserializing a node code into a Node object"""

    def to_representation(self, value):
        return value.code

    def to_internal_value(self, data):
        try:
            return Node.objects.get(code=data)
        except Node.DoesNotExist as e:
            raise DetailedValidationError(
                f"Invalid node: {data}", field=self.field_name
            ) from e


class DataPackageCheckSerializer(serializers.ModelSerializer):
    """Check PKG integrity (read-only POST)"""

    project_code = serializers.CharField(
        source="data_transfer.project.code", read_only=True
    )

    class Meta:
        model = DataPackage
        read_only_fields = (
            "metadata_hash",
            "data_transfer",
            "purpose",
            "project_code",
        )
        fields = read_only_fields + ("file_name", "metadata")

    def create(self, validated_data):
        _, data = check_dpkg(**validated_data)
        return DataPackage(**data)


def check_dpkg(
    metadata, file_name: str, file_size: int | None = None, read_only: bool = True
):
    """Check if DataPackage is valid.

    validated_data: request data
    read_only: If True, the check will always fail for status != AUTHORIZED.
        If False, the check also succeeds if status is EXPIRED and
        DataPackage with the given metadata_hash already exists.
        This flag is necessary to differentiate between the following two cases:
          1. `read_only` is `True`: this function is called from sett through the
             `check` endpoint when attempting to transfer a data package. sett only
              checks if the package is valid and doesn't perform any modifying
              operations (is `read_only`).
          2. `read_only` is `False`: this function is called implicitly in the
             `data-package/log` endpoint to check if the data package data is valid
             before creating a data package, which is a modifying operation
             (is NOT `read_only`).
    """
    metadata_str = metadata
    try:
        metadata = json.loads(metadata)
    except json.decoder.JSONDecodeError as e:
        raise DetailedValidationError(f"Invalid metadata: {e}") from e
    try:
        metadata_hash = hash_metadata(metadata)
        transfer_id = metadata["transfer_id"]
        purpose: str | None = metadata["purpose"]
    except KeyError as e:
        raise DetailedValidationError(f"metadata is missing field '{e}'") from e
    try:
        transfer = DataTransfer.objects.get(id=transfer_id)
    except (ValueError, DataTransfer.DoesNotExist):
        raise DetailedValidationError(f"Invalid transfer: {transfer_id}") from None
    hash_exists = DataPackage.objects.filter(metadata_hash=metadata_hash).exists()
    if transfer.status != DataTransfer.AUTHORIZED:
        if read_only or not (transfer.status == DataTransfer.EXPIRED and hash_exists):
            raise DetailedValidationError(
                "Cannot create data package for transfer with "
                f"ID '{transfer_id}' in state '{transfer.status}'"
            )
    if read_only and hash_exists:
        raise DetailedValidationError(
            "Data package already exists for transfer ID "
            f"'{transfer_id}' with the same timestamp, sender, and recipients."
        )
    name_exists = DataPackage.objects.filter(file_name=file_name).exists()
    if read_only and name_exists:
        raise DetailedValidationError(
            f"Data package name '{file_name}' already exists, "
            "package names must be unique."
        )
    if not transfer.data_provider.enabled:
        raise DetailedValidationError(
            f"Data Provider: {transfer.data_provider.code} is currently "
            f"not able to send data for transfer with ID '{transfer_id}'"
        )
    if transfer.project.destination.node_status != Node.STATUS_ONLINE:
        raise DetailedValidationError(
            f"Node: {transfer.project.destination.name} is not online"
        )
    check_fingerprints(transfer.project, metadata["recipients"], metadata["sender"])
    if purpose is None:
        purpose = transfer.purpose
    elif purpose != transfer.purpose:
        raise DetailedValidationError(
            f"Got a '{purpose}' package for a '{transfer.purpose}' "
            f"data transfer with ID '{transfer_id}'"
        )

    return (
        transfer,
        {
            "metadata_hash": metadata_hash,
            "purpose": purpose,
            "data_transfer": transfer,
            "file_name": file_name,
            "file_size": file_size,
            "metadata": metadata_str,
        },
    )


def check_fingerprints(project: Project, recipients, sender) -> None:
    """Check if all fingerprints involved in the dtr are allowed to be involved"""
    recipients_keys: QuerySet[PgpKeyInfo] = PgpKeyInfo.objects.filter(
        fingerprint__in=recipients
    )
    keys = recipients_keys | PgpKeyInfo.objects.filter(fingerprint=sender)
    unapproved_fingerprints = {
        (key.fingerprint, "unapproved")
        for key in keys
        if key.status != PgpKeyInfo.Status.APPROVED
    }
    unauthorized_fingerprints = {
        (key.fingerprint, f"not a data manager of project {project}")
        for key in recipients_keys
        if key.user not in project.users_by_role(ProjectRole.DM)
    }
    missing_fingerprints = {
        (fpr, "not found on Portal")
        for fpr in {*recipients, sender} - {key.fingerprint for key in keys}
    }
    if violations := (
        *unapproved_fingerprints,
        *unauthorized_fingerprints,
        *missing_fingerprints,
    ):
        raise DetailedValidationError(
            "The following keys are invalid: "
            + ", ".join(f"{fpr} ({reason})" for fpr, reason in violations)
        )


class DataPackageTraceSerializer(serializers.ModelSerializer):
    node = NodeField()

    class Meta:
        model = DataPackageTrace
        fields = ("node", "timestamp", "status")


class DataPackageSerializer(DataPackageCheckSerializer):
    """Serializer to list data packages"""

    destination_node = serializers.CharField(
        source="data_transfer.project.destination.code", read_only=True
    )
    trace = DataPackageTraceSerializer(read_only=True, many=True)
    data_provider = serializers.CharField(
        source="data_transfer.data_provider.code", read_only=True
    )
    sender_open_pgp_key_info = serializers.SerializerMethodField()
    recipients_open_pgp_key_info = serializers.SerializerMethodField()

    class Meta(DataPackageCheckSerializer.Meta):
        more_fields = (
            "id",
            "trace",
            "destination_node",
            "data_provider",
            "file_size",
            "sender_open_pgp_key_info",
            "recipients_open_pgp_key_info",
        )
        read_only_fields = (
            DataPackageCheckSerializer.Meta.read_only_fields + more_fields
        )
        fields = DataPackageCheckSerializer.Meta.fields + more_fields

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related(
            "data_transfer__project__destination",
            "data_transfer__data_provider",
            "trace",
        )

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response["trace"] = sorted(
            response["trace"], key=lambda x: dateparse.parse_datetime(x["timestamp"])
        )
        return response

    def validate(self, attrs):
        _, validated_data = check_dpkg(**attrs)
        return validated_data

    def extract_key(self, obj: DataPackage, default: str, key: str, lookup: str) -> str:
        info = default
        try:
            if value := json.loads(obj.metadata).get(key):
                if (keys := PgpKeyInfo.objects.filter(**{lookup: value})).count():
                    info = ", ".join(
                        (
                            f"{key.user.first_name} {key.user.last_name} "
                            + f"({key.fingerprint})"
                            for key in keys
                        )
                    )
        except json.decoder.JSONDecodeError:
            pass
        return info

    def get_sender_open_pgp_key_info(self, obj: DataPackage) -> str:
        """Return data package's sender's PGP key info"""

        return self.extract_key(obj, "Sender not found", "sender", "fingerprint")

    def get_recipients_open_pgp_key_info(self, obj: DataPackage) -> str:
        return self.extract_key(
            obj, "Recipient(s) not found", "recipients", "fingerprint__in"
        )


class DataPackageLogSerializer(serializers.ModelSerializer):
    """Serializer for logging data packages reported by a landing zone

    (involves metadata checks)
    """

    node = NodeField()
    file_name = serializers.CharField(
        write_only=True,
    )
    metadata = serializers.CharField(
        allow_null=True,
        write_only=True,
    )
    file_size = serializers.IntegerField(
        allow_null=True,
        write_only=True,
        min_value=1,
        help_text="File size in bytes (min. 1b)",
    )

    class Meta:
        model = DataPackageTrace
        read_only_fields = ("id", "data_package", "timestamp")
        fields = read_only_fields + (
            "node",
            "status",
            "file_name",
            "metadata",
            "file_size",
        )

    def validate(self, attrs):
        attrs = super().validate(attrs)
        node = attrs["node"]
        file_name = attrs["file_name"]
        if attrs["status"] == DataPackageTrace.DELETED:
            try:
                package = DataPackage.objects.get(file_name=file_name)
            except DataPackage.DoesNotExist:
                raise DetailedValidationError(
                    f"Data package with name '{file_name}' not found"
                )
            except DataPackage.MultipleObjectsReturned:
                raise DetailedValidationError(
                    f"Multiple data packages with name '{file_name}' found"
                )
            if not DataPackageTrace.objects.filter(
                node=node,
                data_package=package,
                status=DataPackageTrace.PROCESSED,
            ).exists():
                raise DetailedValidationError(
                    f"Data package with name '{file_name}' is not processed"
                )
        else:
            if attrs.get("metadata") is None:
                raise DetailedValidationError(
                    "metadata is required for status other than 'DELETED'"
                )
            if attrs.get("file_size") is None:
                raise DetailedValidationError(
                    "file_size is required for status other than 'DELETED'"
                )
            _, data = check_dpkg(
                attrs["metadata"],
                attrs["file_name"],
                attrs.get("file_size"),
                read_only=False,
            )
            package_query = DataPackage.objects.filter(
                metadata_hash=data["metadata_hash"]
            )
            if package_query.exists():
                package = package_query.first()
                # Block other nodes while package is being processed
                processing_node = get_processing_node_code(package)
                if processing_node is not None and processing_node != node.code:
                    raise DetailedValidationError(
                        f"Package is processed by {processing_node}, logging blocked "
                        "for other nodes"
                    )
                # After PROCESSED, forbid further processing on the same node:
                if DataPackageTrace.objects.filter(
                    node=node,
                    data_package=package,
                    status=DataPackageTrace.PROCESSED,
                ).exists():
                    raise DetailedValidationError(
                        f"Data package '{file_name}' already processed on "
                        f"node {node.code}"
                    )
        return attrs

    @transaction.atomic
    def create(self, validated_data):
        node = validated_data["node"]
        trace_status = validated_data["status"]
        file_name = validated_data["file_name"]

        if trace_status == DataPackageTrace.DELETED:
            package = DataPackage.objects.get(file_name=file_name)
            # `get_or_create` prevents trace duplication
            trace, _ = DataPackageTrace.objects.get_or_create(
                node=node, data_package=package, status=trace_status
            )
            return trace

        transfer, data = check_dpkg(
            validated_data["metadata"],
            file_name,
            validated_data.get("file_size"),
            read_only=False,
        )
        matadata_hash = data.pop("metadata_hash")
        package, created = DataPackage.objects.get_or_create(
            metadata_hash=matadata_hash, defaults=data
        )
        if created:
            subject, body = create_data_package_landed_notification(package)
            sendmail(
                subject=subject,
                body=body,
                recipients={
                    user.email
                    for user in [
                        pgp_key_info.user for pgp_key_info in package.recipients
                    ]
                    + list(package.data_transfer.project.users_by_role(ProjectRole.DM))
                    + [package.sender.user]
                },
                email_cfg=settings.CONFIG.email,
                reply_to=(settings.CONFIG.notification.ticket_mail,),
            )
            if 0 < transfer.max_packages <= transfer.packages.count():
                transfer.status = DataTransfer.EXPIRED
                transfer.save()

        if trace_status == DataPackageTrace.ERROR:
            return DataPackageTrace.objects.create(
                node=node, data_package=package, status=trace_status
            )
        else:
            # `get_or_create` prevents trace duplication
            trace, _ = DataPackageTrace.objects.get_or_create(
                node=node, data_package=package, status=trace_status
            )
            return trace


def get_processing_node_code(package):
    try:
        obj = DataPackageTrace.objects.filter(data_package=package).latest("timestamp")
    except DataPackageTrace.DoesNotExist:
        return None
    if obj.status != DataPackageTrace.PROCESSED:
        return obj.node.code
    return None


def hash_metadata(metadata: dict) -> str:
    return hashlib.sha3_256(
        repr(
            (
                metadata["sender"],
                sorted(metadata["recipients"]),
                metadata["transfer_id"],
                metadata["timestamp"],
            )
        ).encode()
    ).hexdigest()
