from rest_framework import serializers

from ..models.feed import Feed


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed
        read_only_fields = ("id",)
        fields = read_only_fields + (
            "created",
            "label",
            "title",
            "message",
            "from_date",
        )
