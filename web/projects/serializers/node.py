from django.db import transaction
from django.db.models import QuerySet
from django_drf_utils.serializers.utils import (
    get_request_username,
    DetailedValidationError,
    update_related_fields,
)
from rest_framework import serializers

from ..models.node import Node, NodeIPAddresses


class NodeIPAddressesSerializer(serializers.ModelSerializer):
    class Meta:
        model = NodeIPAddresses
        fields = ("ip_address", "mask")


class NodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        read_only_fields = ("id",)
        fields = read_only_fields + (
            "code",
            "name",
            "node_status",
            "ticketing_system_email",
            "object_storage_url",
            "oauth2_client",
            "ip_address_ranges",
        )

    ip_address_ranges = NodeIPAddressesSerializer(many=True, required=False)

    @transaction.atomic
    def create(self, validated_data):
        ip_address_ranges = validated_data.pop("ip_address_ranges", [])

        node = super().create(validated_data)

        update_related_fields(node, NodeIPAddressesSerializer, ip_address_ranges)
        return node

    @transaction.atomic
    def update(self, instance, validated_data):
        user = get_request_username(self)
        # Only staff is allowed to edit 'code'
        if (
            not user.is_staff
            and validated_data.get("code", instance.code) != instance.code
        ):
            raise DetailedValidationError(
                "Only staff are allowed to change node code", field="code"
            )

        ip_address_ranges = validated_data.pop("ip_address_ranges", [])
        update_related_fields(instance, NodeIPAddressesSerializer, ip_address_ranges)
        return super().update(instance, validated_data)

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related("ip_address_ranges")
