import re
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import validators
from django.db import models, transaction
from django.db.models import QuerySet
from django_drf_utils.serializers.fields import EnumChoiceField
from django_drf_utils.serializers.utils import (
    DetailedValidationError,
    get_request_username,
    update_related_fields,
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ..models.node import Node
from ..models.project import (
    RESOURCE_SCHEMES,
    Project,
    ProjectIPAddresses,
    ProjectRole,
    ProjectUserRole,
    ProjectUserRoleHistory,
    Resource,
    Service,
)
from ..models.terms_of_use import TermsOfUse, TermsOfUseAcceptance
from ..permissions.node import has_node_admin_permission
from ..signals.project import (
    DictUserRole,
    get_project_change_roles,
    update_permissions_signal,
)

User = get_user_model()

ARCHIVED_READ_ONLY_MESSAGE = "Archived Projects are read-only."


def check_archived_read_only(instance: Project):
    if instance.archived:
        raise ValidationError(ARCHIVED_READ_ONLY_MESSAGE)


class ResourceSerializer(serializers.ModelSerializer):
    location = serializers.CharField(
        validators=[validators.URLValidator(schemes=RESOURCE_SCHEMES)]
    )

    class Meta:
        model = Resource
        read_only_fields = ("id",)
        fields = read_only_fields + ("name", "location", "description", "contact")


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        read_only_fields = (
            "id",
            "user_id",
            "project_id",
        )
        fields = read_only_fields + (
            "name",
            "description",
            "data",
            "action",
            "action_timestamp",
            "state",
            "created",
            "changed",
            "locked",
            "archived",
        )

    def validate(self, attrs):
        """The validated data should contain project_id and (possibly) user_id.
        These values are not part of the payload, but come from the endpoint url.
        We distinguish between project member services and project services,
        where the user_id is null.
        """
        data = super().validate(attrs)
        request = self.context["request"]
        match = re.search(
            r"\/projects\/(?P<project_id>\d+)\/members\/(?P<user_id>\d+)\/services",
            request.build_absolute_uri(),
        )
        if match:
            self.is_project_service = False
            member_info = match.groupdict()
            return {**data, **member_info}
        match = re.search(
            r"\/projects\/(?P<project_id>\d+)\/services",
            request.build_absolute_uri(),
        )
        if match:
            self.is_project_service = True
            member_info = match.groupdict()
            return {**data, **member_info}

        raise ValidationError("Could not process the service input")

    def save(self, **kwargs):
        """We need to manually check for uniqueness here, since the
        unique_together constraint in the Service model class does not
        work correctly with NULL values in user_id."""
        if self.is_project_service:
            existing_service = Service.objects.filter(
                project=self.validated_data.get("project_id"),
                user=None,
                name__iexact=self.validated_data.get("name"),
            )
            if existing_service:
                raise ValidationError(
                    "Such a service for this project already exists", code=422
                )
        else:
            existing_service = Service.objects.filter(
                project=self.validated_data.get("project_id"),
                user=self.validated_data.get("user_id"),
                name__iexact=self.validated_data.get("name"),
            )
            if existing_service:
                raise ValidationError(
                    "Such a service for this project member already exists", code=422
                )

        return super().save(**kwargs)


class ProjectIPAddressesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIPAddresses
        fields = ("ip_address", "mask")


class ProjectServiceListSerializer(serializers.ListSerializer):
    def update(self, *args, **kwargs):
        raise NotImplementedError(
            "Update currently not supported (suppresses .update from ListSerializer"
        )

    def to_representation(self, data):
        services = []
        for service in data.filter(user=None):
            services.append(service.name)
        return services


class ProjectUserSerializer(serializers.ModelSerializer):
    """ProjectUser serializer"""

    # explicit `id` field to make `id` NOT `read_only` to allow passing in `id` when
    # updating roles see
    # https://www.django-rest-framework.org/api-guide/serializers/#customizing-multiple-update
    id = serializers.IntegerField(required=False)
    roles = serializers.ListField(child=EnumChoiceField(ProjectRole), allow_empty=True)
    affiliation = serializers.CharField(source="profile.affiliation", read_only=True)
    affiliation_id = serializers.CharField(
        source="profile.affiliation_id", read_only=True
    )
    custom_affiliation = serializers.SlugRelatedField(
        slug_field="name",
        read_only=True,
        many=False,
    )
    display_name = serializers.CharField(source="profile.display_name", read_only=True)
    uid = serializers.CharField(source="profile.uid", read_only=True)
    gid = serializers.CharField(source="profile.gid", read_only=True)
    local_username = serializers.CharField(
        source="profile.local_username", read_only=True
    )
    authorized = serializers.BooleanField(source="profile.authorized", read_only=True)
    terms_of_use_accepted = serializers.CharField(read_only=True)

    class Meta:
        model = User
        read_only_fields = (
            "username",
            "local_username",
            "email",
            "first_name",
            "last_name",
            "display_name",
            "uid",
            "gid",
            "affiliation",
            "affiliation_id",
            "custom_affiliation",
            "authorized",
            "terms_of_use_accepted",
        )
        fields = read_only_fields + (
            "id",
            "roles",
        )


class ProjectUserListSerializer(serializers.ListSerializer):
    """ProjectUserList serializer

    Groups / ungroups rows of ProjectUserRole by user into / from  ProjectUser rows
    """

    @classmethod
    def role_by_name(cls, name) -> ProjectRole:
        try:
            return ProjectRole[name]
        except KeyError as e:
            raise DetailedValidationError(f"Invalid permission type: {e}") from e

    def to_representation(self, data):
        iterable = data.all() if isinstance(data, models.Manager) else data
        roles_by_user: dict[User, list[ProjectRole]] = {}
        user_role: ProjectUserRole
        for user_role in iterable.prefetch_related("user__profile").all():
            roles_by_user.setdefault(user_role.user, []).append(
                ProjectRole(user_role.role).name
            )
        for user, roles in roles_by_user.items():
            # Extend User class with the roles field
            user.roles = roles
            # Extend User class with the terms_of_use_accepted field
            destination_terms_of_use = TermsOfUse.objects.filter(
                node=user_role.project.destination
            )
            terms_of_use_acceptance = TermsOfUseAcceptance.objects.filter(
                terms_of_use__in=destination_terms_of_use,
                user=user,
            )
            if terms_of_use_acceptance.exists():
                user.terms_of_use_accepted = terms_of_use_acceptance.latest(
                    "terms_of_use__created"
                ).terms_of_use.version
        return [ProjectUserSerializer(user).data for user in roles_by_user]

    def to_internal_value(self, data):
        try:
            user_roles: list[tuple[int, list[ProjectRole]]] = [
                (int(project_user["id"]), project_user.get("roles", ()))
                for project_user in data
            ]
        except ValueError as e:
            raise DetailedValidationError("Invalid user field") from e
        except KeyError as e:
            raise DetailedValidationError("Missing user field") from e
        users: dict[int, User] = {
            u.id: u
            for u in User.objects.filter(pk__in=(user for user, _ in user_roles))
        }
        try:
            return [
                {"user": users[user], "role": self.role_by_name(role)}
                for user, roles in user_roles
                for role in roles
            ]
        except KeyError as e:
            raise DetailedValidationError(f"User does not exist: {e}") from e

    def save(self, **kwargs):
        raise NotImplementedError(
            "Save currently not supported (suppresses .save from ListSerializer"
        )


class ProjectSerializerLite(serializers.ModelSerializer):
    class Meta:
        model = Project
        read_only_fields = (
            "id",
            "gid",
            "code",
            "name",
            "archived",
            "expiration_date",
            "destination",
            "created",
            "data_transfer_count",
            "user_has_role",
            "ssh_support",
            "resources_support",
            "services_support",
            "legal_support_contact",
            "additional_info",
        )
        fields = read_only_fields

    user_has_role = serializers.SerializerMethodField(
        label="User has role",
        help_text="Whether the current user has at least one role in the project",
    )
    data_transfer_count = serializers.IntegerField(
        source="data_transfers.count",
        read_only=True,
        label="Data transfers count",
        help_text="The number of data transfers associated with the project",
    )
    destination = serializers.SlugRelatedField(
        queryset=Node.objects.all(),
        read_only=False,
        slug_field="code",
        required=False,
    )

    def get_user_has_role(self, project):
        return ProjectUserRole.objects.filter(
            project=project, user=self.context["request"].user
        ).exists()

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.select_related("destination").prefetch_related(
            "ip_address_ranges",
            "resources",
        )


class ProjectRestrictedSerializer(ProjectSerializerLite):
    """Basic/default project serializer"""

    users = ProjectUserListSerializer(child=ProjectUserSerializer(), required=False)
    ip_address_ranges = ProjectIPAddressesSerializer(
        many=True,
        read_only=True,
    )
    resources = ResourceSerializer(
        many=True,
        read_only=True,
    )
    services = ProjectServiceListSerializer(child=ServiceSerializer(), read_only=True)
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Project
        read_only_fields = ProjectSerializerLite.Meta.read_only_fields + (
            "permissions",
            "identity_provider",
        )
        fields = read_only_fields + (
            "data_specification_approval_group",
            "legal_approval_group",
            "ip_address_ranges",
            "resources",
            "users",
            "services",
        )

    def get_permissions(self, obj):
        requestor = get_request_username(self)
        staff = requestor.is_staff
        node_admin = has_node_admin_permission(requestor, obj.destination)
        return {
            "edit": {
                "name": not obj.archived and (staff or node_admin),
                "code": not obj.archived and (staff or node_admin),
                "destination": not obj.archived and staff,
                "expiration_date": not obj.archived and (staff or node_admin),
                "users": (
                    []
                    if obj.archived
                    else [p.name for p in get_project_change_roles(requestor, obj)]
                ),
                "ip_address_ranges": not obj.archived and (staff or node_admin),
                "resources": not obj.archived and (staff or node_admin),
                "legal_support_contact": not obj.archived and (staff or node_admin),
                "data_specification_approval_group": not obj.archived and staff,
                "legal_approval_group": not obj.archived and (staff or node_admin),
                "ssh_support": not obj.archived and (staff or node_admin),
                "resources_support": not obj.archived and (staff or node_admin),
                "services_support": not obj.archived and (staff or node_admin),
                "identity_provider": not obj.archived and (staff or node_admin),
                "additional_info": not obj.archived and (staff or node_admin),
            },
            "archive": staff or node_admin,
        }

    @transaction.atomic
    def create(self, validated_data):
        """
        :raises: ServiceUnavailable
        """
        users: list[DictUserRole] = validated_data.pop("users", [])
        project: Project = super().create(validated_data)
        update_permissions_signal.send(
            sender=project, user=get_request_username(self), user_roles=users
        )
        return project

    @transaction.atomic
    def update(self, instance, validated_data):
        """
        :raises: ServiceUnavailable, ValidationError
        """
        check_archived_read_only(instance)
        update_permissions_signal.send(
            sender=instance,
            user=get_request_username(self),
            user_roles=validated_data.pop("users", []),
        )
        return super().update(instance, validated_data)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if "users" in attrs:
            inactive = [
                user_role["user"].username
                for user_role in attrs["users"]
                if not user_role["user"].is_active
            ]
            if inactive:
                _users = ",".join(f'"{u}"' for u in inactive)
                msg = f"Cannot add inactive users {_users} to project"
                raise DetailedValidationError(msg, field="users")
            no_role_users = {
                user_role["user"]
                for user_role in attrs["users"]
                if user_role["role"] == ProjectRole.NO_ROLE
            }
            other_roles_users = {
                user_role["user"]
                for user_role in attrs["users"]
                if user_role["role"] != ProjectRole.NO_ROLE
            }
            if not no_role_users.isdisjoint(other_roles_users):
                msg = "A user with NO ROLE cannot have another role assigned"
                raise DetailedValidationError(msg, field="users")
        return attrs


class ProjectSerializer(ProjectRestrictedSerializer):
    """Extension of restricted project serializer for staff (PA - Portal Admin)"""

    ip_address_ranges = ProjectIPAddressesSerializer(many=True, required=False)
    resources = ResourceSerializer(many=True, required=False)

    gid = serializers.IntegerField(
        validators=(
            validators.MinValueValidator(settings.CONFIG.project.gid.min),
            validators.MaxValueValidator(settings.CONFIG.project.gid.max),
        ),
        read_only=True,
    )

    class Meta:
        model = Project
        read_only_fields = (
            "id",
            "created",
            "gid",
            "permissions",
            "data_transfer_count",
            "archived",
            "user_has_role",
        )
        fields = read_only_fields + (
            "name",
            "code",
            "data_specification_approval_group",
            "legal_approval_group",
            "destination",
            "legal_support_contact",
            "expiration_date",
            "ip_address_ranges",
            "resources",
            "users",
            "ssh_support",
            "resources_support",
            "services_support",
            "identity_provider",
            "additional_info",
        )

    @transaction.atomic
    def create(self, validated_data):
        gid_max = Project.objects.aggregate(models.Max("gid"))["gid__max"]
        validated_data["gid"] = (
            gid_max + 1 if gid_max else settings.CONFIG.project.gid.min
        )

        ip_address_ranges = validated_data.pop("ip_address_ranges", [])
        resources = validated_data.pop("resources", [])

        project = super().create(validated_data)

        update_related_fields(project, ProjectIPAddressesSerializer, ip_address_ranges)
        update_related_fields(project, ResourceSerializer, resources)
        return project

    @transaction.atomic
    def update(self, instance, validated_data):
        """
        :raises: ValidationError
        """
        check_archived_read_only(instance)
        ip_address_ranges = validated_data.pop("ip_address_ranges", [])
        resources = validated_data.pop("resources", [])
        if "expiration_date" not in validated_data:
            instance.expiration_date = None
        update_related_fields(instance, ProjectIPAddressesSerializer, ip_address_ranges)
        update_related_fields(instance, ResourceSerializer, resources)
        return super().update(instance, validated_data)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs["name"] == attrs["code"]:
            raise DetailedValidationError(
                "Project code must not be the same as project name!", field="code"
            )
        return attrs


class ProjectUserRoleHistorySerializer(serializers.ModelSerializer):
    timestamp = serializers.DateTimeField(source="created")
    changed_by = serializers.CharField(source="display_changed_by")
    user = serializers.CharField(source="display_user")
    project = serializers.CharField(source="project_str")
    permission = serializers.CharField(source="display_permission")
    id = serializers.IntegerField()

    class Meta:
        model = ProjectUserRoleHistory
        fields = (
            "timestamp",
            "changed_by",
            "user",
            "project",
            "permission",
            "enabled",
            "id",
        )
