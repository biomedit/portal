from django.db.models import QuerySet
from django.db import transaction
from rest_framework import serializers

from django.contrib.auth import get_user_model
from django.conf import settings

import markdown2
from django.core.mail import EmailMessage

from ..models.terms_of_use import TermsOfUse, TermsOfUseAcceptance
from ..models.node import get_node_permission_nodes

User = get_user_model()


class TermsOfUseLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsOfUse
        read_only_fields = (
            "id",
            "created",
        )
        fields = read_only_fields + ("node", "version_type", "version", "text")


class TermsOfUseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsOfUseAcceptance
        read_only_fields = ("id", "first_name", "last_name", "email", "accepted_at")
        fields = read_only_fields

    id = serializers.IntegerField(source="user.id")
    first_name = serializers.CharField(source="user.first_name")
    last_name = serializers.CharField(source="user.last_name")
    email = serializers.EmailField(source="user.email")
    accepted_at = serializers.DateTimeField(source="created")


class TermsOfUseSerializer(TermsOfUseLiteSerializer):
    class Meta:
        model = TermsOfUse
        read_only_fields = TermsOfUseLiteSerializer.Meta.read_only_fields + ("users",)
        fields = TermsOfUseLiteSerializer.Meta.fields + ("users",)

    users = TermsOfUseUserSerializer(
        source="termsofuseacceptance_set", many=True, read_only=True
    )

    @staticmethod
    def prefetch(queryset: QuerySet):
        return queryset.prefetch_related("users")

    def to_representation(self, instance: TermsOfUse):
        ret = super().to_representation(instance)
        req_user = self.context["request"].user
        # `staff` has all permissions on all available nodes,
        # must not be handled separately
        nodes = set(get_node_permission_nodes(req_user))
        if instance.node not in nodes:
            del ret["users"]
        return ret


class TermsOfUseAcceptanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsOfUseAcceptance
        fields = ("user", "terms_of_use")

    @transaction.atomic
    def create(self, validated_data):
        terms_of_use_acceptance = super().create(validated_data)
        send_acceptance_confirmation(terms_of_use_acceptance)
        return terms_of_use_acceptance


def send_acceptance_confirmation(terms_of_use_acceptance):
    user_email = terms_of_use_acceptance.user.email
    terms_of_use_markdown = terms_of_use_acceptance.terms_of_use.text
    subject = "Terms of Use Acceptance Confirmation"
    node_name = terms_of_use_acceptance.terms_of_use.node.name
    plain_text_content_as_html = (
        f"<p>Dear {terms_of_use_acceptance.user.get_full_name()},</p>"
        f"Thank you for accepting the {node_name} Terms of Use. This email serves as a "
        f"confirmation of your acceptance and contains important details related to "
        f"the agreement. <p>Node: {node_name} Document: "
        f"Version {terms_of_use_acceptance.terms_of_use.version}</p>"
        f"<p>Acceptance Declaration: You have confirmed that you have read "
        f"and accepted the Terms of Use "
        f"as of {terms_of_use_acceptance.created.strftime('%d/%m/%Y %H:%M:%S')}.</p>"
        "<p>For your reference, the full version of the Terms of Use you "
        f"accepted is below.</p><p>If you have any questions or concerns, please "
        f"do not hesitate to contact us "
        f"at {settings.CONFIG.notification.ticket_mail}.</p>"
        "<p>Thank you and best regards,<br>BioMedIT Central Team</p>"
    )
    html_content = markdown2.markdown(terms_of_use_markdown)
    combined_content = f"{plain_text_content_as_html}<hr>{html_content}"
    reply_to_email = settings.CONFIG.notification.ticket_mail
    email = EmailMessage(
        subject=subject,
        body=combined_content,
        from_email=reply_to_email,
        to=[user_email],
        headers={"Reply-To": reply_to_email},
    )
    email.content_subtype = "html"
    email.send()
