from rest_framework import serializers

from ..models.identity_provider import IdentityProvider


class IdentityProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = IdentityProvider
        read_only_fields = ("id", "code", "name")
        fields = read_only_fields
