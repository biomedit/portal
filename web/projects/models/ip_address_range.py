from django.core import validators
from django.db import models


class IPAddressRange(models.Model):
    ip_address = models.GenericIPAddressField()
    mask = models.PositiveSmallIntegerField(
        validators=(validators.MaxValueValidator(32),)
    )

    def __str__(self):
        return f"{self.ip_address}/{self.mask}"

    class Meta:
        abstract = True
