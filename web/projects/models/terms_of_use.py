from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django_drf_utils.email import sendmail
from django_drf_utils.models import CreatedMixin
from simple_history.models import HistoricalRecords

from .node import Node


User = get_user_model()


class TermsOfUse(CreatedMixin):
    """A node's terms of use or acceptable use policy."""

    class VersionType(models.TextChoices):
        MINOR = (
            "MINOR",
            "A minor update which doesn't require the users to re-accept the terms.",
        )
        MAJOR = (
            "MAJOR",
            "A major update which requires the users to re-accept the terms.",
        )

    history = HistoricalRecords()
    node = models.ForeignKey(
        Node,
        on_delete=models.CASCADE,
        related_name="terms_of_use",
        help_text="The node to which the terms of use belong.",
    )
    text = models.TextField(help_text="The terms of use content.")
    version = models.CharField(
        max_length=10, help_text="The version of the terms of use."
    )
    version_type = models.CharField(
        choices=VersionType.choices,
        max_length=10,
        help_text="Which type of version this is. See VersionType for details.",
    )
    users = models.ManyToManyField(
        User, through="TermsOfUseAcceptance", related_name="terms_of_use"
    )

    def __str__(self):
        return f"{self.node} - v{self.version}"

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        node = self.node
        recipients = [
            user.email for user in User.objects.filter(terms_of_use__node=node)
        ]
        if len(recipients) and created:
            sendmail(
                email_cfg=settings.CONFIG.email,
                bcc=recipients,
                subject=f"New terms of use for {node.name}",
                body=f"""
    Dear user,

    A new version of the terms of use has been published for {node.name}. You may
    need to accept this new version in order to continue using services offered by
    the node.

    Please contact {settings.CONFIG.notification.ticket_mail} if you have any
    questions.

    Kind Regards,

    BioMedIT Team
    """,
            )


class TermsOfUseAcceptance(CreatedMixin):
    """A user's acceptance of the terms of use."""

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        help_text="Which user this acceptance belongs to.",
    )
    terms_of_use = models.ForeignKey(
        TermsOfUse,
        on_delete=models.CASCADE,
        help_text="Which terms of use were accepted.",
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user", "terms_of_use"],
                name="A user can only accept a terms of use once.",
            )
        ]
