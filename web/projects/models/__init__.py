# Otherwise Django creates a deletion migration for this model
from django.db.models import Field
from django.db.models import Lookup


class ListContains(Lookup):
    """Custom lookup for case-insensitively filtering comma-separated
    (list-like) content of a database field, following the example
    given at https://docs.djangoproject.com/en/5.0/howto/custom-lookups/.
    """

    lookup_name = "list_icontains"
    sep = ","

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        lhs = f"lower({lhs})"
        result = (
            f"upper({lhs}) = {rhs} or {lhs} like {rhs} "
            f"or {lhs} like {rhs} or {lhs} like {rhs}"
        )
        params = [
            item.format(rhs_params[0].lower(), self.sep)
            for item in ("{0}", "{0}{1}%%", "%%{1}{0}", "%%{1}{0}{1}%%")
        ]
        return result, params


Field.register_lookup(ListContains)
