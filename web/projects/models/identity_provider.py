from django_drf_utils.models import CreatedMixin, CodeField, NameField


class IdentityProvider(CreatedMixin):
    """Project's identity provider."""

    code = CodeField(help_text="Unique code for the identity provider")
    name = NameField(help_text="Descriptive name of the identity provider")

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"
