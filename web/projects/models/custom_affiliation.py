from django_drf_utils.models import CodeField, CreatedMixin, NameField


class CustomAffiliation(CreatedMixin):
    code = CodeField(
        help_text="Code for the non-Switch organisation a user is associated with.",
        verbose_name="custom affiliation code",
        lower=True,
    )
    name = NameField(
        help_text="Name of the non-Switch organisation a user is associated with.",
        verbose_name="custom affiliation name",
    )

    def __str__(self) -> str:
        return self.name
