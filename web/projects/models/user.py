from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core import validators
from django.db import models
from django.db.models import QuerySet
from django.utils.functional import cached_property
from simple_history.models import HistoricalRecords

from identities.models import GroupProfile
from .flag import Flag
from ..models.custom_affiliation import CustomAffiliation

User = get_user_model()


class UserNamespace(models.Model):
    name = models.CharField(
        max_length=4,
        unique=True,
        validators=(
            validators.MinLengthValidator(2),
            validators.RegexValidator(
                regex=r"^[a-z]{2,4}$",
                message='Enter a valid value. Allowed characters: "a-z"',
            ),
        ),
    )

    def __str__(self):
        return f"Namespace ({self.name})"


PROFILE_LOCAL_USERNAME_REGEX = r"^[a-z][a-z0-9_]*$"
PROFILE_LOCAL_USERNAME_REGEX_MESSAGE = (
    "Enter a valid value. "
    'Allowed characters: "a-z", "0-9", "_". '
    "Must start with a lowercase letter."
)


def to_display_name(user: User) -> str:
    if user.first_name or user.last_name:
        name = " ".join(filter(bool, (user.first_name, user.last_name)))
    else:
        try:
            name = user.profile.local_username
        except User.profile.RelatedObjectDoesNotExist:
            name = user.username
    display_email = user.email and f"({user.email})"
    return " ".join(filter(bool, (f"{name}", display_email)))


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    affiliation = models.CharField(max_length=512, blank=True)
    affiliation_id = models.CharField(max_length=512, blank=True)
    custom_affiliation = models.ForeignKey(
        CustomAffiliation, on_delete=models.SET_NULL, blank=True, null=True
    )
    local_username = models.CharField(
        blank=True,
        max_length=64,
        validators=(
            validators.RegexValidator(
                regex=PROFILE_LOCAL_USERNAME_REGEX,
                message=PROFILE_LOCAL_USERNAME_REGEX_MESSAGE,
            ),
        ),
    )
    namespace = models.ForeignKey(
        UserNamespace, on_delete=models.SET_NULL, blank=True, null=True
    )
    uid = models.PositiveIntegerField(blank=True, null=True, unique=True)
    gid = models.PositiveIntegerField(blank=True, null=True, unique=True)
    affiliation_consent = models.BooleanField(default=False)
    emails = models.TextField(default="")

    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["namespace", "local_username"], name="unique username"
            )
        ]

    def __str__(self):
        return f"Profile ({self.user.username})"

    @cached_property
    def display_id(self):
        return (
            f"ID: {self.user.username.split('@')[0]}"
            if "@" in self.user.username
            else None
        )

    @cached_property
    def display_name(self):
        return to_display_name(self.user)

    @cached_property
    def display_local_username(self):
        return "_".join(
            filter(None, (self.namespace and self.namespace.name, self.local_username))
        )

    @cached_property
    def authorized(self):
        try:
            return (
                self.user
                in Flag.objects.get(
                    code=settings.CONFIG.flags.authorized_user
                ).users.all()
            )
        except Flag.DoesNotExist:
            return False


# Legal Approval
def is_legal_approver(user: User) -> bool:
    """Whether current user is allowed to change the legal approval status.

    There might be multiple legal approval groups. On each project one can set the
    legal approval group. If none is specified, then the `ELSI Help Desk` will be
    in charge.
    """
    return get_legal_approval_role_groups(user).exists()


def get_legal_approval_role_groups(user: User) -> QuerySet[Group]:
    """For given user returns all the legal approval groups he is associated to."""
    return user.groups.filter(profile__role=GroupProfile.Role.ELSI)


# Data Specification Approval
def is_data_specification_approver(user: User) -> bool:
    """Whether current user is allowed to change the data specification approval
    status.

    There might be multiple data specification approval groups. On each project
    one can set the data specification approval group. If none is specified,
    then the `DCC (Data Specification)` will be in charge.
    """
    return get_data_specification_approval_role_groups(user).exists()


def get_data_specification_approval_role_groups(user: User) -> QuerySet[Group]:
    """For given user returns all the data specification approval groups he is
    associated to."""
    return user.groups.filter(
        profile__role=GroupProfile.Role.DATA_SPECIFICATION_APPROVAL
    )
