import pytest

from projects.permissions.terms_of_use import IsNodeAdmin, OwnsTermsOfUseAcceptance

from ..factories import UserFactory, make_node_admin


@pytest.mark.parametrize(
    "admin_of, target, has_access",
    (
        (0, 0, True),
        (0, 1, False),
        (None, 0, False),
    ),
)
def test_is_node_admin(rf, admin_of, target, has_access, node_factory):
    nodes = node_factory.create_batch(3)
    perm = IsNodeAdmin()
    request = rf.post("/")
    if admin_of is None:
        request.user = UserFactory()
    else:
        request.user = make_node_admin(node=nodes[admin_of])[0]
    request.data = {"node": nodes[target].id}
    assert perm.has_permission(request, None) is has_access


@pytest.mark.parametrize(
    "user_id, target, has_access",
    ((2, 2, True), (2, 1, False)),
)
def test_owns_terms_of_use_acceptance(rf, user_id, target, has_access, user_factory):
    perm = OwnsTermsOfUseAcceptance()
    request = rf.post("/")
    request.user = user_factory(id=user_id)
    request.data = {"user": target}

    assert perm.has_permission(request, None) is has_access
