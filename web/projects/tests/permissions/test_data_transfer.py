import pytest

from django.contrib.auth.models import AnonymousUser

from projects.models.project import ProjectRole
from projects.permissions.data_transfer import (
    IsPatchingDataSpecification,
    IsProjectDataManager,
    IsDataSpecificationApprover,
)

from ..factories import (
    ProjectFactory,
    UserFactory,
    make_data_specification_approver,
    make_project_user,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "user_maker, has_access",
    (
        (AnonymousUser, False),
        (UserFactory, False),
        (
            lambda: make_project_user(
                project=ProjectFactory(id=1), role=ProjectRole.USER
            ),
            False,
        ),
        (
            lambda: make_project_user(
                project=ProjectFactory(id=1), role=ProjectRole.DM
            ),
            True,
        ),
        (
            lambda: make_project_user(
                project=ProjectFactory(id=2), role=ProjectRole.DM
            ),
            False,
        ),
    ),
)
def test_is_project_data_manager(rf, user_maker, has_access):
    perm = IsProjectDataManager()
    request = rf.get("/")
    request.user = user_maker()
    request.data = {"project": 1}
    assert perm.has_permission(request, None) is has_access


@pytest.mark.parametrize(
    "data, has_access",
    (
        ({"data_specification": "https://example.com", "id": 1}, False),
        ({"data_specification": "https://example.com"}, True),
    ),
)
def test_is_patching_data_specification(rf, data, has_access):
    perm = IsPatchingDataSpecification()
    request = rf.get("/")
    request.data = data
    assert perm.has_permission(request, None) is has_access


def test_is_data_specification_approver(
    rf, group_factory, data_transfer_factory, project_factory
):
    data_specification_approval_group = group_factory()
    data_transfer = data_transfer_factory(
        project=project_factory(
            data_specification_approval_group=data_specification_approval_group
        )
    )
    perm = IsDataSpecificationApprover()
    request = rf.get("/")
    for u in (AnonymousUser(), UserFactory(), make_data_specification_approver()):
        request.user = u
        assert perm.has_object_permission(request, None, data_transfer) is False
    request.user = make_data_specification_approver(
        group=data_specification_approval_group
    )
    assert perm.has_object_permission(request, None, data_transfer) is True
