import pytest
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase

from projects.models.project import ProjectRole
from projects.permissions import UPDATE_METHODS
from projects.permissions.data_provider import (
    IsDataProviderAdmin,
    ViewDataProvider,
)
from ..factories import (
    UserFactory,
    DataProviderFactory,
    make_data_provider_admin,
    make_data_provider_viewer,
    make_data_specification_approver,
    make_node_viewer,
    make_project_user,
)


class TestIsDataProviderAdmin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user, self.admin1, self.admin2 = UserFactory.create_batch(3)
        self.staff = UserFactory(staff=True)
        self.dp1, self.dp2 = DataProviderFactory.create_batch(2)
        make_data_provider_admin(self.admin1, self.dp1)
        make_data_provider_admin(self.admin2, self.dp2)
        self.permission_obj = IsDataProviderAdmin()
        self.request = self.factory.request()
        self.request.method = "PUT"

    def test_no_permission(self):
        # Anonymous user and user with no data provider permissions
        for user in (AnonymousUser(), self.user):
            self.request.user = user
            assert not self.permission_obj.has_permission(self.request, None)

    def test_staff(self):
        self.request.user = self.staff
        for method in UPDATE_METHODS:
            self.request.method = method
            assert not self.permission_obj.has_permission(self.request, None)

    def test_dp_admin_own_dp(self):
        for method in ("PUT", "PATCH"):
            for user, dp in ((self.admin1, self.dp1), (self.admin2, self.dp2)):
                self.request.user = user
                self.request.method = method
                assert self.permission_obj.has_permission(self.request, None)
                assert self.permission_obj.has_object_permission(self.request, None, dp)

    def test_dp_admin_other_dp(self):
        for user, node in (
            (self.admin1, self.dp2),
            (self.admin2, self.dp1),
        ):
            self.request.user = user
            assert self.permission_obj.has_permission(self.request, None)
            assert not self.permission_obj.has_object_permission(
                self.request, None, node
            )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "user_maker,has_access",
    (
        (AnonymousUser, False),
        (UserFactory, False),
        (make_node_viewer, True),
        (make_data_provider_viewer, True),
        (lambda: make_project_user(role=ProjectRole.DM), True),
        (make_data_specification_approver, True),
    ),
)
def test_view_data_provider_roles(rf, user_maker, has_access):
    perm = ViewDataProvider()
    request = rf.get("/")
    request.user = isinstance(user := user_maker(), tuple) and user[0] or user
    assert perm.has_permission(request, None) is has_access
