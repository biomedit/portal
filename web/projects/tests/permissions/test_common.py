from unittest.mock import Mock

import pytest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from rest_framework import permissions
from rest_framework.exceptions import MethodNotAllowed

from projects.apps import APP_NAME
from projects.models.project import Project
from projects.permissions import (
    IsStaff,
    ObjectPermission,
    ReadOnly,
)

User = get_user_model()
NONSAFE_METHODS = ("POST", "PUT", "PATCH", "DELETE")


def test_staff_permission_anonymous(rf):
    request = rf.request()
    request.user = AnonymousUser()
    assert IsStaff().has_permission(request, None) is False


def test_staff_permission_staff(staff_user_auth, rf):
    request = rf.request()
    request.user = staff_user_auth
    for method in permissions.SAFE_METHODS + NONSAFE_METHODS:
        request.method = method
        assert IsStaff().has_permission(request, None) is True


def test_staff_permission_user(basic_user_auth, rf):
    request = rf.request()
    request.user = basic_user_auth
    assert IsStaff().has_permission(request, None) is False


@pytest.mark.parametrize(
    "test_input, expected",
    (
        (permissions.SAFE_METHODS, True),
        (NONSAFE_METHODS, False),
    ),
)
def test_readonly(rf, test_input, expected, basic_user_auth):
    permission_obj = ReadOnly()
    request = rf.request()
    request.user = basic_user_auth
    for method in test_input:
        request.method = method
        assert permission_obj.has_permission(request, None) is expected


class TestObjectPermission:
    @pytest.mark.parametrize(
        "method", ("GET", "OPTIONS", "HEAD", "PUT", "PATCH", "DELETE")
    )
    def test_get_required_object_permissions(self, method):
        ObjectPermission().get_required_object_permissions(method, Project)

    @pytest.mark.parametrize("method", ("POST", "FOO", "get"))
    def test_get_required_object_permissions_not_allowed_method(self, method):
        with pytest.raises(MethodNotAllowed):
            ObjectPermission().get_required_object_permissions(method, Project)

    def test_has_permission_anonymous(self, rf):
        request = rf.get("/")
        request.user = None
        assert ObjectPermission().has_permission(request, None) is False

    def test_has_permission_method_not_allowed(self, user_factory, rf):
        request = rf.post("/")
        request.user = user_factory()
        with pytest.raises(MethodNotAllowed):
            ObjectPermission().has_permission(request, None)

    @pytest.mark.parametrize(
        "method", ("get", "options", "head", "put", "patch", "delete")
    )
    def test_has_permission_method_ok(self, user_factory, method, rf):
        request = getattr(rf, method)("/")
        request.user = user_factory()
        assert ObjectPermission().has_permission(request, None) is True

    def test_has_object_permission(self, rf, project_factory):
        request = rf.get("/")
        mock_user = Mock()
        request.user = mock_user
        project = project_factory.build()
        ObjectPermission().has_object_permission(request, None, project)
        mock_user.has_perms.assert_called_once_with(
            (f"{APP_NAME}.view_{project.__class__._meta.model_name}",), project
        )
