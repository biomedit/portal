import pytest
from django.contrib.auth import get_user_model

from projects.filters.user import UserLookupFilter

User = get_user_model()

group_id = 1
another_group_id = 2


@pytest.fixture
def users(user_factory, group_factory):
    group = group_factory(id=group_id)
    another_group = group_factory(id=another_group_id)
    for index, (
        username,
        first_name,
        last_name,
        affiliation,
        affiliation_id,
        emails,
    ) in enumerate(
        (
            (
                "1@hogwarts.uk",
                "Harry",
                "Potter",
                "member@hogwarts.uk",
                "",
                "1@1.es,2@2.es",
            ),
            (
                "2@hogwarts.uk",
                "Albus",
                "Dumbledore",
                "member@hogwarts.uk,staff@hogwarts.uk",
                "",
                "",
            ),
            (
                "3@hogwarts.uk",
                "Ron",
                "Weasley",
                "member@hogwarts.uk",
                "",
                "a@a.com,b@b.com,c@c.com",
            ),
            ("4@hogwarts.uk", "Ginny", "Weasley", "member@hogwarts.uk", "", "d@d.ch"),
            ("5@hogwarts.uk", "Hermione", "Granger", "member@hogwarts.uk", "", ""),
            ("6@hogwarts.uk", "Albus", "Potter", "", "", "e@e.de,f@f.fr,g@g.gb"),
            (
                "7@hogwarts.uk",
                "Hermione",
                "Granger",
                "",
                "member@hogwarts.uk",
                "m@m.mx",
            ),
        )
    ):
        emails = emails.split(",") if emails else []
        user = user_factory.create(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=emails[0] if emails else None,
            profile__affiliation=affiliation,
            profile__affiliation_id=affiliation_id,
            profile__emails=",".join(emails),
        )
        group.user_set.add(user)
        if index % 2 == 0:
            another_group.user_set.add(user)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_length",
    [
        [{}, 8],
        # `first_name`
        [{"first_name": "Albus"}, 2],
        [{"first_name": "albus"}, 2],
        [{"first_name": "Hermione"}, 2],
        [{"first_name": "Viktor"}, 0],
        # `last_name`
        [{"last_name": "Weasley"}, 2],
        [{"last_name": "Weasley"}, 2],
        [{"last_name": "Granger"}, 2],
        [{"last_name": "Krum"}, 0],
        # full name
        [{"first_name": "Harry", "last_name": "Potter"}, 1],
        [{"first_name": "John", "last_name": "Potter"}, 0],
        [{"first_name": "Harry", "last_name": "Houdini"}, 0],
        # `affiliation`
        [{"affiliation": "staff@hogwarts.uk"}, 1],
        [{"affiliation": "hogwarts"}, 6],
        [{"affiliation": "Hogwarts"}, 6],
        [{"affiliation": "durmstrang"}, 0],
        # `username`
        [{"username": "1@hogwarts.uk"}, 1],
        [{"username": "1@hogwarts.UK"}, 0],
        # `email`
        [{"email": "2@2.es"}, 1],
        [{"email": "2@2.e"}, 0],
        [{"email": "2@2.com"}, 0],
        [{"email": "f@f.fr"}, 1],
        [{"email": "1@1.ES"}, 1],
    ],
)
def test_user_lookup_filter(
    users,  # noqa: ARG001 unused-function-argument
    query_params,
    expected_length,
    rf,
):
    request = rf.get("/")
    request.query_params = query_params
    queryset = User.objects.all()
    q = UserLookupFilter().filter_queryset(request, queryset, None)
    assert len(q) == expected_length
