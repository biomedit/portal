from unittest import mock

import factory
import pytest
from django.db.models import Q
from rest_framework import generics, permissions, serializers

from projects.filters.common import (
    BaseLookupFilter,
    CaseInsensitiveOrderingFilter,
    QueryParam,
)
from projects.models.project import Project


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = "__all__"


class OrderingListView(generics.ListAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (permissions.AllowAny,)
    filter_backends = (CaseInsensitiveOrderingFilter,)
    ordering_fields = ("name",)


@pytest.mark.django_db
@pytest.mark.parametrize("order_by", ("name", "-name"))
def test_case_insensitive_ordering(order_by, rf, project_factory):
    names = (
        "aBc",
        "Bcd",
        "cde",
        "AaC",
        "AbC",
        "AbD",
        "-AB",
        "-AC",
        "AA",
        "BB",
        "ÇA",
        "CB",
        "çc",
        "DD",
        "A̧A",
        "B̧B",
        "ḐD",
        "łó",
        "Ęt",
        "abc",
        "Abc",
        "abC",
        "ABC",
        "Test 1",
        "Test 2",
        "Test 3",
    )
    names_sorted = [
        "AA",
        "A̧A",
        "AaC",
        "-AB",
        "abc",
        "AbC",
        "aBc",
        "ABC",
        "abC",
        "Abc",
        "AbD",
        "-AC",
        "BB",
        "B̧B",
        "Bcd",
        "ÇA",
        "CB",
        "çc",
        "cde",
        "DD",
        "ḐD",
        "Ęt",
        "łó",
        "Test 1",
        "Test 2",
        "Test 3",
    ]
    project_factory.create_batch(len(names), name=factory.Iterator(names))
    view = OrderingListView.as_view()
    request = rf.get("/", {"ordering": order_by})
    response = view(request)
    assert [x["name"].lower() for x in response.data] == [
        x.lower()
        for x in (names_sorted[::-1] if order_by.startswith("-") else names_sorted)
    ]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "query_params, expected_lookups",
    [
        [{}, []],
        [{"first_name": "Harry"}, [Q(first_name__iexact="Harry")]],
        [
            {"first_name": "Harry", "last_name": "Potter"},
            [Q(first_name__iexact="Harry"), Q(last_name__iexact="Potter")],
        ],
        [
            {"last_name": "", "age": ""},
            [Q(last_name__iexact=""), Q(age__isnull=True)],
        ],
        [{"animagus": ""}, [Q(animagus__species__iexact="")]],
        [
            {"email": "w@b.com"},
            [Q(email__exact="w@b.com") | Q(profile__emails__icontains="w@b.com")],
        ],
    ],
)
def test_base_lookup_filter(query_params, expected_lookups, rf):
    request = rf.get("/")
    request.query_params = query_params
    mock_queryset = mock.Mock()
    base_lookup_filter = BaseLookupFilter()
    base_lookup_filter.query_params = [
        QueryParam("first_name", "first_name__iexact", "first_name"),
        QueryParam("last_name", "last_name__iexact", "last_name"),
        QueryParam("age", "age__iexact", "age", "integer"),
        QueryParam("animagus", "animagus__species__iexact", "animagus"),
        QueryParam("email", ("email__exact", "profile__emails__icontains"), "email"),
    ]
    base_lookup_filter.filter_queryset(request, mock_queryset, None)

    assert base_lookup_filter._get_lookups(request) == expected_lookups

    if query_params:
        mock_queryset.filter.assert_called()
    else:
        mock_queryset.filter.assert_not_called()
