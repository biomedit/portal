import pytest
from projects.serializers.user import UserSerializer, UserTermsOfUseSerializer
from projects.models.terms_of_use import TermsOfUseAcceptance

from ..factories import UserFactory


@pytest.mark.django_db
def test_user_local_username():
    user = UserFactory()
    profile = user.profile
    new_local_username = "some_new_local_username"
    UserSerializer._set_local_username(
        profile=profile, new_local_username=new_local_username
    )
    assert profile.local_username == new_local_username
    assert profile.namespace is not None


@pytest.mark.django_db
def test_user_uid():
    user = UserFactory()
    profile = user.profile
    prev_uid = profile.uid
    UserSerializer._next_uid(profile=profile)
    assert prev_uid is not profile.uid
    assert isinstance(profile.uid, int)

    # once set, uid should not increase
    prev_uid = profile.uid
    UserSerializer._next_uid(profile=profile)
    assert profile.uid == prev_uid


@pytest.mark.django_db
def test_user_gid():
    user = UserFactory()
    profile = user.profile
    prev_gid = profile.gid
    UserSerializer._next_gid(profile=profile)
    assert prev_gid is not profile.gid
    assert isinstance(profile.gid, int)

    # once set, gid should not increase
    prev_gid = profile.gid
    UserSerializer._next_gid(profile=profile)
    assert profile.gid == prev_gid


@pytest.mark.django_db
def test_update(user_factory, group_factory):
    group = group_factory()
    assert group.profile.role_entity_type is None
    user = user_factory()
    serializer = UserSerializer()
    serializer.update(user, {"groups": (group,)})


@pytest.mark.django_db
def test_user_terms_of_use_serializer(terms_of_use_factory, node_factory, user_factory):
    node = node_factory()
    terms_of_use = terms_of_use_factory(node=node)
    user = user_factory()
    terms_of_use_acceptance = TermsOfUseAcceptance.objects.create(
        user=user, terms_of_use=terms_of_use
    )

    serialized_data = UserTermsOfUseSerializer(terms_of_use_acceptance).data

    assert serialized_data["node"] == node.code
    assert (
        serialized_data["accepted_at"]
        == terms_of_use_acceptance.created.strftime("%Y-%m-%dT%H:%M:%S.%f") + "Z"
    )
    assert serialized_data["version"] == terms_of_use.version
