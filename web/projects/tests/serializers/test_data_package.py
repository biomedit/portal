import pytest

from django_drf_utils.serializers.utils import (
    DetailedValidationError,
)

from ..views.test_pgpkey import (
    CN_EMAIL,
    CN_FINGERPRINT,
    CN_FIRST_NAME,
    CN_LAST_NAME,
    CN_USER_ID,
    SGT_HARTMANN_EMAIL,
    SGT_HARTMANN_FINGERPRINT,
    SGT_HARTMANN_FIRST_NAME,
    SGT_HARTMANN_LAST_NAME,
    SGT_HARTMANN_USER_ID,
)
from ...models.pgp import PgpKeyInfo
from ...models.project import ProjectRole
from ...serializers.data_package import (
    DataPackageSerializer,
    check_fingerprints,
)

UNAUTHORIZED = "not a data manager of project"
UNAPPROVED = "unapproved"
MISSING = "not found on Portal"


@pytest.mark.parametrize(
    "recipients, sender, project, should_raise_exception, error_messages",
    (
        # No exception if keys are approved and recipients are DM of the project
        ((CN_FINGERPRINT,), CN_FINGERPRINT, 0, False, ()),
        # Raise exception if a recipient is not DM of the project
        (
            (CN_FINGERPRINT,),
            CN_FINGERPRINT,
            1,
            True,
            (UNAUTHORIZED,),
        ),
        # Raise exception if at least one key is not approved
        ((SGT_HARTMANN_FINGERPRINT,), CN_FINGERPRINT, 1, True, (UNAPPROVED,)),
        ((CN_FINGERPRINT,), SGT_HARTMANN_FINGERPRINT, 0, True, (UNAPPROVED,)),
        # Raise exception if at least one key in not in the db
        ((CN_FINGERPRINT,), "DEADBEEF", 0, True, (MISSING,)),
        # When multiple violations occur, report all of them
        (
            (SGT_HARTMANN_FINGERPRINT,),
            "DEADBEEF",
            0,
            True,
            (UNAUTHORIZED, UNAPPROVED, MISSING),
        ),
    ),
)
def test_check_user_permissions(
    recipients,
    sender,
    project,
    should_raise_exception,
    error_messages,
    pgp_key_info_factory,
    project_user_role_factory,
    user_factory,
    project_factory,
):
    """Test that the correct exception is raised, if needed"""

    projects = project_factory.create_batch(2)

    cn = user_factory(email=CN_EMAIL)
    sgt = user_factory(email=SGT_HARTMANN_EMAIL)

    for status, fingerprint, key_email, user in [
        (PgpKeyInfo.Status.APPROVED, CN_FINGERPRINT, CN_EMAIL, cn),
        (PgpKeyInfo.Status.REJECTED, SGT_HARTMANN_FINGERPRINT, SGT_HARTMANN_EMAIL, sgt),
    ]:
        pgp_key_info_factory(
            status=status, fingerprint=fingerprint, key_email=key_email, user=user
        )

    for project_id, user in [(0, cn), (1, sgt)]:
        project_user_role_factory(
            role=ProjectRole.DM.value, project=projects[project_id], user=user
        )

    try:
        check_fingerprints(projects[project], recipients, sender)
        if should_raise_exception:
            assert False
    except DetailedValidationError as error:
        if not should_raise_exception:
            assert False
        else:
            for message in error_messages:
                assert message in str(error.detail)


CN = f"{CN_USER_ID} ({CN_FINGERPRINT})"
SGT_HARTMANN = f"{SGT_HARTMANN_USER_ID} ({SGT_HARTMANN_FINGERPRINT})"


@pytest.mark.parametrize(
    "metadata, expected",
    (
        (f'{{"sender": "{CN_FINGERPRINT}"}}', f"{CN}"),
        ('{"sender": malformed}', "Sender not found"),
        ('{"sender": "DEADBEEF"}', "Sender not found"),
    ),
)
def test_get_sender_open_pgp_key_info(
    metadata, expected, pgp_key_info_factory, data_package_factory, user_factory
):
    """Test that sender's info or an error message are returned correctly"""

    pgp_key_info_factory(
        fingerprint=CN_FINGERPRINT,
        key_user_id=CN_USER_ID,
        user=user_factory(first_name=CN_FIRST_NAME, last_name=CN_LAST_NAME),
    )
    data_package = data_package_factory(metadata=metadata)

    assert (
        DataPackageSerializer(data_package).data["sender_open_pgp_key_info"] == expected
    )


@pytest.mark.parametrize(
    "metadata, expected",
    (
        (
            f'{{"recipients": ["{CN_FINGERPRINT}", "{SGT_HARTMANN_FINGERPRINT}"]}}',
            f"{CN}, {SGT_HARTMANN}",
        ),
        ('{"recipients": malformed}', "Recipient(s) not found"),
        ('{"recipients": "DEADBEEF"}', "Recipient(s) not found"),
    ),
)
def test_get_recipients_open_pgp_key_info(
    metadata, expected, pgp_key_info_factory, data_package_factory, user_factory
):
    """Test that recipients' info or an error message are returned correctly"""

    pgp_key_info_factory(
        fingerprint=CN_FINGERPRINT,
        key_user_id=CN_USER_ID,
        user=user_factory(first_name=CN_FIRST_NAME, last_name=CN_LAST_NAME),
    )
    pgp_key_info_factory(
        fingerprint=SGT_HARTMANN_FINGERPRINT,
        key_user_id=SGT_HARTMANN_USER_ID,
        user=user_factory(
            first_name=SGT_HARTMANN_FIRST_NAME, last_name=SGT_HARTMANN_LAST_NAME
        ),
    )
    data_package = data_package_factory(metadata=metadata)

    assert (
        DataPackageSerializer(data_package).data["recipients_open_pgp_key_info"]
        == expected
    )
