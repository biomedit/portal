from unittest import mock

import pytest
from django.contrib.auth.models import Group
from rest_framework.exceptions import ValidationError

from projects.models.approval import DataProviderApproval, GroupApproval, NodeApproval
from projects.models.data_transfer import DataTransfer
from projects.models.project import Project, ProjectRole
from projects.serializers.data_transfer import DataTransferSerializer
from projects.notifications.dtr_notification import (
    DtrConfirmCreationNotification,
    DtrCreatedNotification,
    DtrDataProviderApprovalNotification,
    DtrGroupApprovalNotification,
    DtrNodeApprovalNotification,
)
from ..factories import (
    UserFactory,
    make_data_specification_approval_group,
    make_data_specification_approver,
    make_legal_approval_group,
    make_project_user,
)


def test_notify_on_creation(data_transfer_factory, project_factory):
    data_transfer = data_transfer_factory(
        purpose=DataTransfer.PRODUCTION,
        project=project_factory(legal_approval_group=make_legal_approval_group()),
    )
    DataTransferSerializer().create_approvals(data_transfer)

    with (
        mock.patch.object(DtrCreatedNotification, "sendmail") as created,
        mock.patch.object(
            DtrConfirmCreationNotification, "sendmail"
        ) as confirm_creation,
        mock.patch.object(DtrNodeApprovalNotification, "sendmail") as node_approval,
        mock.patch.object(DtrGroupApprovalNotification, "sendmail") as group_approval,
        mock.patch.object(
            DtrDataProviderApprovalNotification, "sendmail"
        ) as dp_approval,
    ):
        DataTransferSerializer().notify_on_creation(data_transfer, "")

        assert created.called
        assert confirm_creation.called
        assert node_approval.called
        assert group_approval.called
        assert not dp_approval.called


@pytest.mark.parametrize(
    "purpose, has_legal_approval_group, has_data_specification_approval_group",
    (
        (DataTransfer.TEST, False, False),
        (DataTransfer.PRODUCTION, False, False),
        (DataTransfer.PRODUCTION, True, True),
    ),
)
def test_create_approvals(
    purpose,
    has_legal_approval_group,
    has_data_specification_approval_group,
    node_factory,
    project_factory,
    data_transfer_factory,
    data_provider_factory,
):
    local, remote = node_factory.create_batch(2)
    legal_approval_group = make_legal_approval_group()
    data_specification_approval_group = make_data_specification_approval_group()
    project = project_factory(
        legal_approval_group=legal_approval_group if has_legal_approval_group else None,
        data_specification_approval_group=(
            data_specification_approval_group
            if has_data_specification_approval_group
            else None
        ),
        destination=local,
    )
    data_provider = data_provider_factory(nodes=[remote])
    DataTransferSerializer.create_approvals(
        data_transfer_factory(
            project=project,
            purpose=purpose,
            data_provider=data_provider,
        )
    )

    data_provider_approvals = DataProviderApproval.objects.all()
    assert len(data_provider_approvals) == 1
    assert data_provider_approvals[0].data_provider == data_provider

    assert len(NodeApproval.objects.all()) == 1
    assert NodeApproval.objects.filter(node=local).exists()

    assert len(GroupApproval.objects.all()) == int(
        has_legal_approval_group and purpose == DataTransfer.PRODUCTION
    ) + int(
        has_data_specification_approval_group and purpose == DataTransfer.PRODUCTION
    )
    if has_legal_approval_group:
        assert GroupApproval.objects.filter(group=legal_approval_group).exists()
    if has_data_specification_approval_group:
        assert GroupApproval.objects.filter(
            group=data_specification_approval_group
        ).exists()


def test_create_approvals_with_orphan_data_provider(
    data_provider_factory, data_transfer_factory
):
    data_provider = data_provider_factory(nodes=[])
    DataTransferSerializer.create_approvals(
        data_transfer_factory(
            data_provider=data_provider,
        )
    )
    assert GroupApproval.objects.count() == 0
    assert DataProviderApproval.objects.count() == 1
    assert (
        NodeApproval.objects.count() == 1
    )  # We get `2` in previous test `test_create_approvals`


staff_permissions = {
    "delete": True,
    "edit": {
        "max_packages": True,
        "data_provider": True,
        "purpose": False,
        "status": True,
        "requestor": True,
        "legal_basis": True,
        "data_specification": True,
        "data_recipients": True,
    },
}

regular_user_permissions = {
    "delete": False,
    "edit": {
        "max_packages": False,
        "data_provider": False,
        "purpose": False,
        "status": False,
        "requestor": False,
        "legal_basis": False,
        "data_specification": False,
        "data_recipients": False,
    },
}

data_specification_approver_permissions = {
    "delete": False,
    "edit": {
        "max_packages": False,
        "data_provider": False,
        "purpose": False,
        "status": False,
        "requestor": False,
        "legal_basis": False,
        "data_specification": True,
        "data_recipients": False,
    },
}

DATA_SPECIFICATION_APPROVAL_GROUP_NAME = "Data Specification Approvers"
OTHER_DATA_SPECIFICATION_APPROVAL_GROUP_NAME = "Other Data Specification Approvers"


@pytest.mark.parametrize(
    "user_maker,expected_permissions",
    (
        (UserFactory, regular_user_permissions),
        (lambda: UserFactory(staff=True), staff_permissions),
        (
            lambda: make_data_specification_approver(
                group=Group.objects.get(name=DATA_SPECIFICATION_APPROVAL_GROUP_NAME)
            ),
            data_specification_approver_permissions,
        ),
        (
            lambda: make_data_specification_approver(
                group=Group.objects.get(
                    name=OTHER_DATA_SPECIFICATION_APPROVAL_GROUP_NAME
                )
            ),
            regular_user_permissions,
        ),
    ),
)
def test_permissions(
    rf,
    user_maker,
    expected_permissions,
    group_factory,
    data_transfer_factory,
    project_factory,
):
    data_specification_approval_group = group_factory(
        name=DATA_SPECIFICATION_APPROVAL_GROUP_NAME
    )
    group_factory(name=OTHER_DATA_SPECIFICATION_APPROVAL_GROUP_NAME)
    data_transfer = data_transfer_factory(
        project=project_factory(
            data_specification_approval_group=data_specification_approval_group
        )
    )
    serializer = DataTransferSerializer()
    request = rf.get("/")
    request.user = user_maker()
    serializer.context.update({"request": request})

    assert serializer.get_permissions(data_transfer) == expected_permissions


@pytest.mark.parametrize(
    "data_recipient_factory, valid",
    (
        (lambda: UserFactory(), False),
        (
            lambda: next(
                Project.objects.get(code="project").users_by_role(ProjectRole.DM)
            ),
            True,
        ),
    ),
)
def test_validate_data_recipients(
    data_recipient_factory,
    valid,
    project_factory,
):
    project = project_factory(code="project")
    make_project_user(project, ProjectRole.DM)
    data_recipient = data_recipient_factory()
    serializer = DataTransferSerializer()

    def validate():
        serializer.validate({"data_recipients": [data_recipient], "project": project})

    if valid:
        validate()
    else:
        with pytest.raises(ValidationError):
            validate()
