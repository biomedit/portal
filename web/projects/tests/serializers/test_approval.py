from unittest.mock import patch
from django_drf_utils.serializers.utils import DetailedValidationError

from projects.models.approval import BaseApproval
import pytest

from projects.models.data_transfer import DataTransfer
from projects.notifications import dtr_notification
from projects.serializers.approval import ApprovalSerializer
from ..factories import (
    DataProviderApprovalFactory,
    NodeApprovalFactory,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "approval_factory, status",
    [
        (NodeApprovalFactory, DataTransfer.AUTHORIZED),
        (NodeApprovalFactory, DataTransfer.INITIAL),
        (NodeApprovalFactory, DataTransfer.UNAUTHORIZED),
        (DataProviderApprovalFactory, DataTransfer.AUTHORIZED),
        (DataProviderApprovalFactory, DataTransfer.INITIAL),
        (DataProviderApprovalFactory, DataTransfer.UNAUTHORIZED),
    ],
)
def test_notification_sent_on_approval(
    approval_factory,
    status,
    user_factory,
    data_transfer_factory,
    node_factory,
    project_factory,
    data_provider_factory,
):
    """Test that email notification is correctly sent upon approval"""

    node = node_factory()
    data_provider = data_provider_factory(nodes=[node])
    project = project_factory(destination=node)
    dtr_approval = approval_factory(
        data_transfer=data_transfer_factory(
            status=status,
            project=project,
            data_provider=data_provider,
        ),
        **(
            {"node": node}
            if approval_factory == NodeApprovalFactory
            else {"data_provider": data_provider}
        ),
    )

    approved = status in [DataTransfer.AUTHORIZED, DataTransfer.INITIAL]
    authorized = status == DataTransfer.AUTHORIZED
    user = user_factory()

    with patch.object(
        dtr_notification,
        "create_final_approval_notification",
        return_value=("subject", "body"),
    ) as mock:
        ApprovalSerializer.notify_on_update(
            dtr_approval,
            "https://example.ch",
            user,
            approved,
        )

        # Test that final email notification is sent if and only if DTRs is authorized
        if authorized:
            mock.assert_called_once()
        else:
            mock.assert_not_called()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "dp_last, node_approval_status",
    [
        (True, BaseApproval.ApprovalStatus.APPROVED),
        (False, BaseApproval.ApprovalStatus.WAITING),
    ],
)
def test_dp_approval_comes_last(
    data_provider_approval_factory,
    node_approval_factory,
    data_transfer_factory,
    rf,
    user_factory,
    dp_last,
    node_approval_status,
):
    approval_serializer = ApprovalSerializer()

    request = rf.get("/")
    request.user = user_factory(staff=True)
    approval_serializer.context.update({"request": request})

    data_transfer = data_transfer_factory()

    dtr_approval = data_provider_approval_factory(
        status=BaseApproval.ApprovalStatus.WAITING, data_transfer=data_transfer
    )
    node_approval_factory(status=node_approval_status, data_transfer=data_transfer)

    update_data = {"status": BaseApproval.ApprovalStatus.APPROVED}
    update_data.update(
        {attr: True for attr in dtr_approval.declarations(data_transfer.purpose)}
    )

    if dp_last:
        with patch.object(
            dtr_notification,
            "create_final_approval_notification",
            return_value=("subject", "body"),
        ) as mock:
            approval_serializer.update(dtr_approval, update_data)
            mock.assert_called_once()
    else:
        with pytest.raises(DetailedValidationError):
            approval_serializer.update(dtr_approval, update_data)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "should_send_email, other_approval_status",
    [
        (True, BaseApproval.ApprovalStatus.APPROVED),
        (False, BaseApproval.ApprovalStatus.WAITING),
    ],
)
def test_dp_approval_notification_is_sent(
    rf,
    should_send_email,
    other_approval_status,
    user_factory,
    data_transfer_factory,
    data_provider_approval_factory,
    node_approval_factory,
):
    approval_serializer = ApprovalSerializer()

    request = rf.get("/")
    request.user = user_factory(staff=True)
    approval_serializer.context.update({"request": request})

    data_transfer = data_transfer_factory()

    data_provider_approval_factory(
        status=BaseApproval.ApprovalStatus.WAITING, data_transfer=data_transfer
    )
    updated_approval = node_approval_factory(
        status=BaseApproval.ApprovalStatus.WAITING, data_transfer=data_transfer
    )
    node_approval_factory(status=other_approval_status, data_transfer=data_transfer)

    update_data = {"status": BaseApproval.ApprovalStatus.APPROVED}
    update_data.update(
        {attr: True for attr in updated_approval.declarations(data_transfer.purpose)}
    )

    with patch.object(
        dtr_notification.DtrDataProviderApprovalNotification, "sendmail"
    ) as mock:
        approval_serializer.update(updated_approval, update_data)

        assert mock.called is should_send_email
