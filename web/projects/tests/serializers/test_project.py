import pytest

from projects.serializers.project import (
    ProjectSerializerLite,
    ProjectRestrictedSerializer,
)
from projects.models.terms_of_use import TermsOfUseAcceptance


@pytest.mark.django_db
def test_user_has_role(project_factory, project_user_role_factory, rf):
    project_user_role = project_user_role_factory.create()
    other_project = project_factory.create()
    request = rf.get("/")
    request.user = project_user_role.user

    assert ProjectSerializerLite(
        project_user_role.project, context={"request": request}
    ).data["user_has_role"]

    assert not ProjectSerializerLite(other_project, context={"request": request}).data[
        "user_has_role"
    ]


@pytest.mark.django_db
def test_terms_of_use_accepted(
    node_factory,
    project_factory,
    terms_of_use_factory,
    project_user_role_factory,
    user_factory,
    rf,
):
    def get_terms_of_use_accepted():
        return (
            ProjectRestrictedSerializer(project, context={"request": request})
            .data["users"][0]
            .get("terms_of_use_accepted")
        )

    first_version = "1.0.0"
    new_version = "2.0.0"
    user = user_factory()
    node = node_factory()
    project = project_factory(destination=node)
    project_user_role_factory(project=project, user=user)
    request = rf.get("/")
    request.user = user

    # No terms of use exist yet
    assert get_terms_of_use_accepted() is None

    # A first version is created
    first = terms_of_use_factory(node=node, version=first_version)
    assert get_terms_of_use_accepted() is None

    # User accepts the first version
    TermsOfUseAcceptance.objects.create(user=user, terms_of_use=first)
    assert get_terms_of_use_accepted() == first_version

    # A new version is created
    new = terms_of_use_factory(node=node, version=new_version)
    assert get_terms_of_use_accepted() == first_version

    # User accepts the new version
    TermsOfUseAcceptance.objects.create(user=user, terms_of_use=new)
    assert get_terms_of_use_accepted() == new_version
