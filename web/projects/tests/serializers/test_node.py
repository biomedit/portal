import pytest

from projects.serializers.node import NodeSerializer
from projects.models.node import NodeIPAddresses


@pytest.mark.django_db
def test_create_with_ip_address_ranges():
    node = NodeSerializer().create(
        {"ip_address_ranges": [{"ip_address": "127.0.0.1", "mask": 32}]}
    )
    assert NodeIPAddresses.objects.filter(node=node).exists()
