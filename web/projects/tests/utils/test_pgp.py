import contextlib
import re
from unittest import mock

import pytest
import requests
from rest_framework import status

from projects.utils.pgp import (
    PGP_BEGIN_BLOCK,
    PGP_END_BLOCK,
    KeyDownloadError,
    KeyInvalidError,
    KeyNotVerifiedError,
    KeyRevokedError,
    download_ascii_armored_public_pgp_key,
    download_key_metadata_with_validation,
)

KEYSERVER_URL = "hagrid.hogwarts.org"
CN_USER_ID = "CN Testkey 4096 (test key)"
CN_EMAIL = "chuck.norris@roundhouse.swiss"

CN_NONVERIFIED_FINGERPRINT_4096 = "33F8833EDDEE86E7B099E53C19E6B92330651E73"
CN_PUB_KEY_NONVERIFIED_4096 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: 33F8 833E DDEE 86E7 B099  E53C 19E6 B923 3065 1E73

xsFNBGW47RcBEADJkZfUCeX/gG30meYfbfOwQNa3mTANsddiWI6Gx1CtbqVqM+T7
ohQkG1nP1jM1uSLFEx1kCEPU8MMocp1hS6WRbik+TWbQiN1xq+oIB5QjAxSdS7LF
R+jtoTv235AuX7DFIWbzeOrKXXumhf7ZWtk9yu7mqUYGvxq/cTul13zpxwaBIV3x
ftrZX4Ei6BoC8oGOm1KTlnstBkPGaH4fNzND33QJu3WWQkq3z4/Gu/mDBlRLNHva
sd8FCGXwCH7eZOiG8ikYSCIPwAZs7eQb6kpoL7dBOb3s3rroltc/1eSzqo9V1wJq
F0rcmWeTGSm8s0KdHXPQcXk1yN3dVKFWxPGLcSmOx1cavsHlPkCO/OGdP5/RfKv/
JggentKSU4O52WvbuyrbN4npzXvc/epYqhm9Nx9kyzb7dhagkC2u5mJc7QZ70ktJ
O5sD/XmfwUdnBrCc4BlMl9ynBMG3EmMixi/CFZ6riKMUG+xpzlGivEKL43ejMSE8
wFC/qIBz6Se47LXPoVjTSjprUsXOwZEVsGqGyK1d7mfh3Vn1wPR+ut0G2+D+0vkj
dxxS5OUczjAr9Dvo4nl0Q30EC30NDC1j0dDgymdmJPQ/1/q0V3+6tNInq+AyshPZ
ZBEZvZ0GoO+Mi1xeszH6Z8tv01XNwtoqxutTuAufVRIGcI1ncjNres5YBwARAQAB
wsHPBB8BCgCDBYJluO0XBYmOzWnpAwsJBwkQGea5IzBlHnNHFAAAAAAAHgAgc2Fs
dEBub3RhdGlvbnMuc2VxdW9pYS1wZ3Aub3JnrFHPNyoEpK2HhZUBh/Wfwoa5Mp1N
PgKhP1HGeuV/eaADFQoIApsBAh4BFiEEM/iDPt3uhuewmeU8Gea5IzBlHnMAAEGX
EACZpqbnT7dC//x7BeYyyAWuh7O4mfv/DiM8qRZFAK5SiFU5i0+3YSfaXy9pHc5/
tnBPOpjQBVaDxyQSbgeYPcNt/dmSvCqJXXQ7/CHjFLoLvnyBfzatLQmsGhOTEDSF
LGuqefJtPxXNVy5CIfAq4h3lNkx/7h3PgXeVKB5KZ5fxW/E5Es9kHvfBeMSVvkCW
PvN+wb+Fs9Wxa0d/PVpxfEwRNKiguCgXZfUGgHgiHxSs7fLTDFihzv007sAFvQ/J
1cl6CQWhSu2Q9QJF4GkbFaTdGdcswo8xv0t6H6gpTo8vMSdIrXiP9S74F0dJVjAH
zV1aJbjEiHIj8tJbaxpYWwIveVmk7YtjIOjfAh2rG47jCHjQiJuMSrQ7dSEVGzJu
rnIjRBJjysdRDXYYHthNIQkpbuiGZWZQSxpXLqt10TT9hb/AWVdp4/EGclQZ7IrH
Ou6Fl+uCOu7tyc4QzU6VcL1z9oImRGk3yNf9DbsvXibTcTbcEylWXNjUaFDV4W9V
dljEC5VMwYAqH+/L1GP57Arzu7dq5sCUoCbAyEgC6Wn+2y5J8BMjKBT2HrbmdM6E
FfvRaBeG8MhFfQmUU4yivNbZtcBciiBLO1nQF3Qu5jMF4ei7HOHZ1pyHv0RXliNa
yvzJ9D9sJm7xGRP5xoMCTLt1F9eNv2vUHSkV/Sc6+zU7KQ==
=d5JF
-----END PGP PUBLIC KEY BLOCK-----
"""

CN_FINGERPRINT_3072 = "6E9CAB4D3275503DCF080F4FFBE8952A1597D014"
CN_PUB_KEY_VERIFIED_3072 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBGW45XQBDADq3wZG5yKIwnTdFKRJozjfH6ZhATI2oEoWgEX3nRfllDIe4eo4
ESn5VFuLXQ+w/21wF7sgUJt7SsO27sQyU1ZfpuK59CMAuhlIPqxCkNd6RXoOrQ9g
jP+0yjJXsndSyhvWG1xheNFXAkZWde3qG2HdDtxTjYDoiGbqwENQlHDrGFJ1PcOS
SvQv69VidqT3EO1xJWc5tjn79y7fAIu+0kM6BRL/fED+QS8Q+zWyV3vlV47e4XzF
1J7BYJ1Y+CbJA27zaYpIZ/QbNIubwR7Ry9T8s1KO3pysQ6Gg18uXbe0jFX5XMRnZ
RMm9lknIyCeHmpxJw8tFKNg5rFEAv2kHz+C3jfMFJH4H4xFkntEw4eixP9I7/N+E
emM4oH6BGsz9ersxoAzytmtKh3DayZ4i90/I/jku2B0WR4LaPIlD9P7W611hKXaO
TR+neL3+oDYxHPnEeYJOPia31WjjkMqVO2dYbdgFlB9d+dqsiPYtwBvqfE4ltADi
YWAwYfqW+W2wha8AEQEAAbQ+Q04gVGVzdGtleSBSU0EgMzA3MiAodGVzdCBrZXkp
IDxjaHVjay5ub3JyaXNAcm91bmRob3VzZS5zd2lzcz6JAc4EEwEKADgWIQRunKtN
MnVQPc8ID0/76JUqFZfQFAUCZbjldAIbAwULCQgHAgYVCgkICwIEFgIDAQIeAQIX
gAAKCRD76JUqFZfQFAKWDADdGoKAfLGKSTWCD4iCEBUIvL1LZCJuaKjaVyQSvqja
2bX5AhwkOuAg6+dt4CXZwYH4yX0vMvAi0gPbMTp+4a4SK9B5kRWueWCwDpTnhpvA
mzuCpbPjIJOTinJ+Jyi+8K09k42rR44WX8UNMDnpQ7QeHct+NOxkwEoKfpMScyVz
Yrm5as76wldd+1KBvglCwh4dUXyliFzZu5FjcGu88t3X6z+UC5HBav772JCsFizp
HHvJK6kLBIO/WyoxLMNOaEE3w9O7JfXAq3IUazbRIFwjHPyj8MzsebkS3MU6itH1
COvMr3urGOstjzYsLDSi9AN69ubqU9AknnzjgEAD2eMKuxHaaVu3Mvu+1Zph8BrG
HFsKsfJGKTV2Je5UJJZsjtlvETMHi2lSqI0up0A6RRwATL7n7ZyXmgomc1I/+6um
6tpNLrFKDrWC+LRzMIylLurHkRVPFW1OG/L21ANZGvb4lqh8r5K64+ZEQsr0/uSE
J7J+8+2/ZQZR+2SRybDqzpa5AY0EZbjldAEMAJbmaklmJu9NXPlGPNZvgqExZglJ
ma9DUZH9u5/2uIJm9R0u+2WhMuxH8qJypM2zcWX0NeOJqZDzTLZJEdSxRFg+kwCu
7AzLPhspfrhu5Zz5PYpiYg9ip8Ny8Ou9ZzXYD7AFyefeVe1Bn4IGV6bAC6H73z7I
eiHBdD7spDGIwZzIZEWe7vr/yzO3Qh2ZXLgOK1+jiAOTFPFNP8vikth1FCW83PBG
1DY1PRLOXeUL8p6aQTXfarLmOqoSzUIsgLmGEEHCx1QA1NMwL50UNQn5C9cG4qtE
cVrr/m/EZScZjv1h3pKShao7TOqAR39Wz7rjEGM3aP/biAzgj2GZ/iXay8dUo/dg
qu8GoivfDkXpe6bEuvauYW8t/lkJkYbtZfWhPVWMu2FXmgOW5Clr1NfwSgXxi+iE
B1m+QAEvuLe2ULcdm85shVOZOYANA4I6+ZL+XhzLFXo34DrgdfAesyptdRRduDty
6U5ZsucBsa7qVjCOCcCh0CIMIZo1p+LP0C1E0wARAQABiQG2BBgBCgAgFiEEbpyr
TTJ1UD3PCA9P++iVKhWX0BQFAmW45XQCGwwACgkQ++iVKhWX0BQ3lgv6AraWyTFi
19Uvt3zgBRqh0LJl0g1YPx4sDWaRw8dz27uqf8GU4qDdGSVyy0HVcItcYLoOsyBN
vkpwPAhWa2wCHS99IikEM8PhM6i5xYHePQuUKWsI0AIsVcLfH0IcbSO7xCSS6O59
CdGbbrQ8Unj+XT6JCLwveh40yDRNWeDdEuYK1T5WRAYWYYlWpZbqoN9+c3ykN4mB
PUIsLIOFYPIBE63miEYxBZX6SD6v/ZdwnJPtIzvtG2E5zE3JQ1hibfVxgO+TPXLn
8CCgNAm3wHu+EG1wQxRFpMCA0CRxUQG5n8L6pMkSVQIrnMaCmrfd04hs7vO4uJVl
92oE8LAh1mXgrcIjmwgqhI3k6M/Rw2cobR7SUoU4V2+uBFaWdntm/EG6Npvsgjv2
rA+qoqAfTZGK+RqhKrSXoC4TpNyl3A1pJe0YdpX2NDX3DkxhVCAFABsCNjlcmUmH
XBNXWodcpmyttu0A6mdAsX3bGjJC8+Kf9afJ7AIeqoMPrvNyj8HeO0yi
=X7wE
-----END PGP PUBLIC KEY BLOCK-----
"""

CN_FINGERPRINT_3072_DSA = "AE777DA3F961E3F687D4F8D1CAEC27BC08CA37D5"
CN_PUB_KEY_VERIFIED_3072_DSA = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----

mQSuBGW46HoRDADVu1jaLbDo8Hi6hqNS4+3HQ8IuD3SWxdnCH4f90YV0H4+iF4Fk
Y4bEds0+/tcnCzDWx+859/Dy73DsNF/7Fpu9COtb5I/YosBvnoTb782ffiIj3NSq
3E+s5t0DzFVuWsLkpxGCtZRyo29uWayZ5eDAbz7xnbZQKSyEuCk3RW6joIZahPJf
nsvzV1FzFqm99Svf6SYnqHwxKUJn0Un29ln2Irzxs9DpkU1U0yF04HV1pGG1SjfR
JuWwJCyBigGd+XV2z48qpbGn6kCuSqv0KBXfwaUzXLfhG8UTF5w2XBN6qRNrFasc
SVus4L6XdFEWLyYCMcN0FGm1Mm7X6P/g87fFRDxrsFee0WowEDT9DgxkezsBkiBT
UconShdA6NtxV7LT44tR2nkHSGUQEwBxkN4a/stphjcsvIcDsso2JVZ+qLOTih8Q
3cedpDuymCyLTVtBu0lUNcoPCh9l3VoDFWGsEpHfKvl4mgZUxZ+Mpud6iCkZPOcV
IFlZymuPpvYfoWsBAKtAxaFTnlz4GFZT/xvaeUV7ZUpKBAyWvwmfsRRxE9TLC/4g
BFWiMKJPWs4ut+eAoLqs8cumOtlbsj+Cunj95sstO+WnxxLTpXk01vPN7CRPQ1ez
A1L7MeIMW4PGHapkBUZjmExUZ4HlyYve9Feq5wXS154UdWb0CRoflD01YR9i3RA8
SroLX9Isn1+pKHn2mrzeBkyaM71bWLxcWnwl0AUr+unFswkxtRkfmpF6+tRjeLtR
rDDJd3TZYhzRl/VvFt3CSGWPf5dw5iqBXKVxnL+KOLxolGN4Ll/SgkCtE0XOlenJ
/Olzbye5tFqrRzdN8Ga+aMO6RUmnF1ihXuSJy5/WS+T7ubuceZDBQORANf4Cf5m6
MWw9AIj6t3s6NGrD6kax6Y2iA8e2W6gT2zHFw5hOg+rDLtWc4uBpi+9hcvcQNLCZ
lNIly+6ChVC6gVoqxGRm2d44CxJtrBdCuk+il+nDcY1W0Fciv8qcb1ZwaEGhIzjW
zJCPZpqByKvjIfw3NUZ0/D35gGhhkWrBP74qfGHswtvurcaN5C64WTJlTKiX7jgL
/RdktIyw4bl/lfg6QVdJcgb6MxdufaDIKpunQVnVvESXxGgXW2zHKqnOo/2demXo
uH7dmDwuDmuJWjcxn10esM4jYrIalEpoVpKGcWwYsf88q9X9WeweQP0U6ux7Fvbv
2k+REo3EA4Us21+A/yyeDdKmW3A3x6ERKUF87bTyc61ufqN1MDLA3iuw7Hq5qANh
2d7Iro3UzIZnGrPEOM5CyIqPwogoPl2OEM9w3i7cpaALGqNiqlJQbTfOa6rJZcmV
hmPdJ88fr2Fp8C4TqC5OyRVzJlRsvVBT3VxSlknK8VE9FMU/Uk+cvvRJLgBTaDDc
ZXnErfE4M2Bdf35bH25/mEsOtjcv2Ije2sO7fhm9Hrl3qf8+EzfL/H+TBxqr46Qm
AxtbEnVXs2/mXyU5GqkhRtksVhVBrm7lJOLQf1QuR12YNe+8qKLu80EJsRFpfoUR
Qy9R+Gc9Oic8QX7dldWLnmezRjtB9AyxrlSFHHWw2w1TG/QO5OklERBGLmYeVAZq
cbQ+Q04gVGVzdGtleSBEU0EgMzA3MiAodGVzdCBrZXkpIDxjaHVjay5ub3JyaXNA
cm91bmRob3VzZS5zd2lzcz6IkAQTEQgAOBYhBK53faP5YeP2h9T40crsJ7wIyjfV
BQJluOh6AhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEMrsJ7wIyjfVDHYA
/021PnzM/UFe7g2CXCX8qTZ59kcpGURnN1jjD61WyjXwAP41CpporVbCnAKa3MCj
CErEVL4xVxskSZDAaTaxSEG0Pg==
=dEaY
-----END PGP PUBLIC KEY BLOCK-----
"""

CN_FINGERPRINT_4096 = "B2E961753ECE0B345E718E74BA6F29C998DDD9BF"
CN_PUB_KEY_VERIFIED_4096 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: B2E9 6175 3ECE 0B34 5E71 8E74 BA6F 29C9 98DD D9BF
Comment: CN Testkey 4096 (test key) <chuck.norris@roundhouse.swiss>

mQINBGJNZD4BEADx72ORnn64kWKbhbEh4V/6YxBykBo7neh0S1Hyy+QLJEv9Qqsg
Zhl0WZIDbA19FXrXFlGLICGYo1AH49PgiqPyzSoPXO1umQkvcwQ+mdnQ+3DVWK7d
HVtrbfYUN+NGIJc/1a5w85oKth0CKeULVtau38QlQ8N+sVG+Cj1TKfXFFk/bc5iC
7LfndPoq+lhlOBqlhUBo6dFjhqkYagt+vwYW9pDbrQk++LtX7gF0707pSQ2/gjb8
rQbRsvxEIIcebtKeN6sh3L2O8jGuw1iSWsffEimqKknVx+vgSdlFx1y4VZ8+pqK9
1PDrFT0d2P4HD2oxaNYMKyLUf0UkUBEtiB9O9M+UtSoEHUsImIEPBK06a+WSIRCi
8DyH10oq8gArnFlsetmCVu9oGIW8wOoIgvT9+o+NdYQOwCmp/fuGSLx/r1TqtVM6
i8B39gqcgo/lZGpfDcY0+bm06bYf67mNHF6krpzYSS/o7irUuGsFjm2Rpvmn3qkq
9TspN5zTk1xA0lYAEYDQVpwGeOm0bC7jI24BVjRUE2OIjP/VEQOqzQxUOXn3ArM8
XA/nfLObQeWKaZmcGMQV7kY+Jco2PGb19tlcjeTlt04+mD2FLTTSoEMwN5aJ06OB
Yg88BkvcTTV87+HYn7yuYRw02p8zy3JWHnsUvQ2HVQyy2R6YiBOE9+PONQARAQAB
tDpDTiBUZXN0a2V5IDQwOTYgKHRlc3Qga2V5KSA8Y2h1Y2subm9ycmlzQHJvdW5k
aG91c2Uuc3dpc3M+iQJOBBMBCgA4FiEEsulhdT7OCzRecY50um8pyZjd2b8FAmJN
ZD4CGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQum8pyZjd2b+Vmw//dFBS
+th2ZCtWrDCjodXFoLyb4gG5mR6NGPo+esp17Obzs5POPrtLRLi41tNPBtNy//jF
Dwg9vFrwUzoC8zniV/dPQhxNjShB4Tjlg8kNssftXbbc/tgWLPedH1OluwuLtuEI
CTbPpVL1vAA/WytEhNw3CCC73Y1RpFNx7wi8wlZZUgjtgsDLQlzP5ogwuNdLlvhX
0QRi6hAkdPRScqi0n+pDZXamNwyeYEt2lRXYce153jqZJh3P8CYRVFmfZeWruSX5
yyZSabWy7aIFZ48RimOyTyHfmeOQM93GCDQa9g8sLtfSSXtKrZbCRpmd1s848TjI
1PFHgahy3Xoa97KhXG8EtmqzASO3fpwkcfM6nUelMtoIoG4IdbCZytFXPXvAwPMy
QY0dY0/uTrdrFWkXanQkwdqy++iOIGilI0MDi80UzqmXbY3a93oOJl4mqUlUMVwA
EpW0PXVo8QnDc0t5XLUZJq9UA0Nk2qAKm3OUTSqtuq6zdm7l5K1lbMFEBoBa6+4Z
EccK9Liz5lFtDmxqoi/rVGO6WEWvZ/TD3qbfCUWEIqsm0vKxDLaUrDvjQjcZoyHO
HGBTXhwmRceMcOM7OsqttSYDqAFRDqrsnfROJBqizUltER16qcWmSCJzJKRTxbQ3
Iql6s/Gchn2m0wkUxG/DN51UGBAb965Alaa+BsG5Ag0EYk1kPgEQAKAriehd0J+K
FDq5Kx4k+CpeednUPfsTDSmhIL6Z8qZRkD2kSQHA8EPCMliw8rWG/Drhjq5jxMBP
NFct3oJ0d7AXCAqg0u41kU3f95Z/ldePWZpmTiaQ74aIMMhwY5WK61QnXUUrkCQp
ZnVEohEbJ5uU/2VXRML8hZ5leP5j2Rx3LU7fSBuXefZDhlM1z6yEpS6p9P4uztG8
YSbPNFlgxu4E+feQFP3ntNlWPmody1FGF8L4PjxmAF2x2ChzxG5XDx4fGr17JWKX
urPT9GUuKkO4l/VNRH58u6OFgoruVkuUscgORfE3g2/0UeD+Ewk2nr7Jr23DiGJb
4nfp2Azew3Moj+hrknz+df/AO3x1sD8GIZZwRDttUa4XQ1Bo/RQHEEAn5V+Ts63P
dNAYtZid1X6KmBqVxVjuTVP9KrCBg5mxd7LND0Y7L1IleRiuQI5Be/5cNpptxMeP
A9y+nDIjr/fZYHNz+UBeiAe4c5ffeYIcVlDjDEc9ZksIHBhGiBarKII2LW7W67mh
KSSOA9Rcv4hBrP5bY5OfqCc72ybhZH5N9/1z94234TiYs5cKreRE+vN19vORAWNo
cOQWD6zsYoib82KqC1iNsq0nt+cCt+amBMvK6oAz08rOxXwGd2im9xo0W0YVGbFD
rr8DfgIvYjBl6huzr7LQPdu+7v8qkhhbABEBAAGJAjYEGAEKACAWIQSy6WF1Ps4L
NF5xjnS6bynJmN3ZvwUCYk1kPgIbDAAKCRC6bynJmN3Zv5nLEACzXNt7Ei5T4sTv
+NdMOCmTxXc0GmTtikUCUA4jNptePihIC7wrwIjVha96f6jx1UnfibV4PoW2uT4o
p6eAh4jXqjf8yud02ujwZUXaueX59IuU3JRTAQ8N0i7HCqxaZdiK9bhYc7iQopJ/
yhq+GYPKUswry1ZnoJuHTmfjst8P0ilnAJaCxvtcKTOt0JikdjoZloUdsfZ3fXMN
3kDtLCiQr6PrXuVEMGmZnIuzIupcdoqldUFNPXsW4k7yt1Y7z/b7rbOpLGByPnrx
50K0A5ViX+wCjwSv1gwTYEMUdpF0kSG7s7NTSRDAUIkuiL1K5KSXnmRqccbG323o
USUdBUqxGVzAZ2g8pytVW/ydU6RE2gcu7sohRkWuypxf0HHkVNCZ39ykjkX3vvBc
ZQlN1jFhmkDeIKl9BebO/44mMCGifHDGo0GF6gpcuYQmwnjQdcGj8pIk7phrWVqX
OclCdLvM03G/siTQyJcqiZKwWOrMOU/lw0j6rIx7l9ZIubyVLt/Ne2H/vA6A/iM8
nE5Voo45u9WIo1guPRMtKaq3VJ53vozGx30lu/gCJ/7VftDRpZLqTdJ88K5RcyzQ
+okwGVflw7N7wWG3N5PWj6hJMHJ3KwGOnue1r904UTfu96RXdtceHe4//fZWA57Q
l1i57wdI9lgg8lljC7ULMFIRColHnA==
=SAz/
-----END PGP PUBLIC KEY BLOCK-----
"""
CN_PUB_KEY = CN_PUB_KEY_VERIFIED_4096
CN_FINGERPRINT = CN_FINGERPRINT_4096

CN_FINGERPRINT_REVOKED = "E8EF2B8922EF6A0D2442D1B18CBF9F57A35735E9"
CN_PUB_KEY_REVOKED = b"""
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGNSkScBEACq9lZG151YD7YEf5tpTUZ666pPy4CaMqw73rTjphTlWsD5dnkR
EKNMW9AqxTCtldgWJMAV2OrlTUqYy88qkD5MX8YaqyB2k3bBjqc3wm2GAFRAkWra
S6D/9o2rf8PFSOMMJ6NF7Rv5FFZvoJXLfpSXMOZ7xii1ckvQb5oJpBJtPDOJBdct
mi1niwpn/1oKv6EC0O5w+GCgmc+1FnS3ikLmD/n75b7L842g/+oyDZv2/BFs6GmC
laZk+E5m9rshGfOEqoNUOYRNvM7ZEW0OEkTuXpcx1zFNvJ4twzofDfTl2eLyZLge
tbaD1V0Rxos1Oxu1P8evQPVZvzM59e2cOjCofnSYEequR+phlnk33bYqDHj2TS7U
98e0a7yoq1YSLypbAWTxa2YP6tXlQct+HMwBkMEjgvkNHU3mp/mtQehGn5MqQ8pP
Y+XDv5pwVWXh1l9YDFBHgeJgvqDAAHxVJXFvrD9cXQjl35z1lRxGQzhb+4v91BWi
+Ch1KP+FaSk8EE8JfwU9XXTlv4RWAuiq5hoCWzBRUrJS1CBI9zthQWPdSo1TkNyH
FFjy+XMjy1GHQ5nrajTj1LTsKAyL9OG6bSdGtWlWPEUHzyN8g8ha+adf0m7NQMId
G1O6qFWNdYnPPFty/2XG3b6T3ZNWTdxcJV48Gpz907GgsNI/BEbOTElcTQARAQAB
iQI2BCABCgAgFiEE6O8riSLvag0kQtGxjL+fV6NXNekFAmNSkS8CHQAACgkQjL+f
V6NXNemmvxAAnC4EcX0wD/5NRyE36XPawFppJVuWuXjbJkmcEHacLI7l+HA3Wa3b
m7UmFy/N8bHLlnDegW/9XK6GxnzpNh14qBUcaZpKZac9f6PykUyNYErJC+cAKcCI
E9Ik8ItSGfSQ7BjdxsjU8glC9LgseO2hlgIxsxp9q4LYJdANiZaZmixENJTeNaxS
tE8mYdqbtrmQIGbPjrPnynWPjezNspVhQDD/I+dDCtCMYg4fyLLBoFNaIRGEQaS3
a2Opunyh26Q9+hT3I/qNZUrcs813kiwA4IVXg6p6dxSRE5is6+PyHG2cmZplnuAi
4KkxZ4AMrdG+WT50GDvvYmygNhZ+6KIe0T6BJiim0mhe1LfOw2PfBgwmHuR4D5Ha
fGkRJ+iRRIlhc0/Ntk/FGxbofrkvu2Fw6ploXRlKlVfLiaE2U4ybrDK3lVZndfZc
8t/x9yA9Q878klBGHd6ZXE0z1cMrQScSL3hUh2jhuzWiYBaG1zDofnXQgSg9Ny13
C9o8hgprc13opSmU1vU2zttssRpVZbASnfp6cVCipeaZBn4wh99M7qi6uAr6R/TJ
NRZo0qwN8BJB3e7ev2/qrpFpXpGcE/eVpXZ/TsrPTsupfRF80NIpnx8+QaMS7DD6
RqUcfZxUW+g/K8geWk29il/yHyA/9pWzNj+izB8WxuyhSuKRhRr8lfC0G1Rlc3Qg
S2V5IDx0ZXN0QGV4YW1wbGUub3JnPokCTgQTAQoAOBYhBOjvK4ki72oNJELRsYy/
n1ejVzXpBQJjUpEnAhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEIy/n1ej
VzXpC9UQAJjT/56rIr6Y7/UeY55tqkKuIKwd6ZERNDSb4UuO6SPviVj0sIgprRCo
ogwo8yVILIaM+bbXLhUUJOq38XX5iXkUBTiExqUu+AziOoPT9yuOjoulSlg7MHCi
6zXBBeeSz60PYBsfRtOqXECtJymHSWQe9VSaTQOiK5wc7pnohIe72EyJ/F4YPiJD
xO7aRnl1NalfgVIxU2ro4XRbMHg74kWsPBk23POfgGHfxzFabfZWzk0JwmtipqRr
/2/nbTbY6jFbwTy3NteEwkSPkUQGKE0DwwFtTggxUdhSTvoCwzZ/j6Qfl+5Ow368
ldGypbv3Gcc5QUz6VpcFD8p4/AnQmSDwUsID9r1EqgFIiaSqlABa7SVXzsqzlQZ2
JeA0yNSS8AlILdy3TQ/1xudzPYeTOlYtZK1MRjlfFQs0/XENLEv44l4nfr/0czzW
Kk/4oBKTA7svdEt1kWsQlmWVAmDNW9EXfKAqI0Ei5w05fHJ8/4c9qqgRBVeHbVPX
gdNyFwAMR7mgoFkLlEbPfJDQfz5kdM80ALSGk7G9XCwlOW6tvZiLzXrHDlGTcDly
EbNnEV40Wr+8vw4b0UtcmYy/IOZZUSzl9bAZrp14B7Cp82d46RC1gcihBwM7E+n4
DQ0F9WrWP96V6IFRV270ENtgeRtTQKJZ4tRMpOCIodgCqe+FQGz7uQINBGNSkScB
EACnTRhUEPvwBw2MC3vQSOehdmqXzp13QPikRVIFsJ/jo1q0KYWfpb0XlAPy9Z0T
qrvX9Ivci2BZrXpAZUFeZyuVsV+rz162Y5W6XQ6jWyfvBf5IRuAwJVBAxmZVUxt7
24+5bdYIG2Zce9akONXsm2AM92Hkx67xqsPcyrGGtrZFi6WXRWQ5ebN/VmiFUUtA
lDPY0gyE2q8nuvu66KsyCXtRIX1zFdC8yF3ntcbMmxhnVAf4tRYyHhQQy1a6GxZk
kZ0/Y9cic34gprsDY9qpG/NN6r6oCtAmb6kg5tnyUwY44fzB2gi7vlaFUh2rnIVT
sWWmCdp3SanygC8iWf4PxZXrvIHkr6qijouky1nmnAfowdnrTrN506YRXMo6p6tA
BVVFCM+hfev+4YJgrghKILFhljfSlQvrHItkfsorxX2x5NpZV0RTXzxD0eGqvwrf
nqh6OcxpdD9QqRtLhJ/hBfZTf8Ey1e1AgVDTuErn8YlEfGPhux8jDVv4p6gsfOx+
YrQr1AILnJ3dhl9QYmxLsFlcOrEa/gQc+qEcyf4F3BHwCAmAiuCdwz/AtzZ1ps7f
iG+ZYTsdA/tsS9bA7t41AA+uOlySGPFGvopEt/kiTfeGERaMGdN05sNEFRL4jFyf
02epxEPb9YAXb4KSTFzyerv8+v0EwJStxfFZkHzE5XpXtwARAQABiQI2BBgBCgAg
FiEE6O8riSLvag0kQtGxjL+fV6NXNekFAmNSkScCGwwACgkQjL+fV6NXNem8QA//
V06wUKiM2TVYLCtosNXpBV1M39ZMb9THAfnIzIr4bY0N4GcAirSrULP31uuTGHrT
1t+T4GFc4VeCFZXOi1a078r09TFmOoX+iNQkLbFaL+qBhuZIcs3okX01RZan5Cgw
xFrkSoYUXwopFrzSxvQ5LZl+GC3AEALCgw07uBan7yLefG6YxrYE6ALUos92ZgmV
Mm+n3YtCGuGF29Yh1TGTiv7WzbGJq1nLf737R0ZMw1UNWDSz0wMNxoABMvqGVHRs
0Ji2lly5pEg2d6/rAhZXMf1mNjGEnEMaIm8eIS9HQxNfkrgsoBZxe2Z24UEEJJ/b
x1Uc/WovKA+Xhwlsa61HsOwwj7WJhhjhxB5u7dgaezv2jpJk5OuhSqRbG42w5cp0
C3ZeA/TSXf/jrfuy7b1dd2ZFluJcZjeNnPVxK3xYmiOLOI76Lx5R17bkUwW6cd7D
JylFHEkoHtFS0RGowAiCg/FWgCekCSgSxTm4kCdFIAZXqyMMdX8oS+nztyuoegm6
Dd2hRDFpDJLgqeq9RM+3jbPvIXbLW3KuieI2Y8GWJ+CBBCN2IYFnBb3z1hYZvTUb
fCEA8CvYnvZ6Hf5IAa0XPL0Dvz27oUEqYT7KMMGiAH997zSFHkY7OZDyb4e+F6Se
kfObBDR3ZLgile+Ge3JYUrLPpYWYh0Al3zptEvl2rVc=
=FehY
-----END PGP PUBLIC KEY BLOCK-----
"""

CN_FINGERPRINT_SHA1 = "BEC30D051069167063F1925433EBD42184A8A0EC"
CN_PUB_KEY_SHA1 = b"""-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: BEC3 0D05 1069 1670 63F1  9254 33EB D421 84A8 A0EC

xsFNBGQzsVwBEADIhATt02IHmL5coxrU5HMtBOKo0JZ570YQf2bq/FyTftLoD48u
GPZSaTZHEv9Ba+E9ca7hQlvl/g+8Elxqa3QuSaRUx3v1C5SZ6e0uxr8aDSOawntE
cFGLmU+4cNr9P7gwSwdrJOYzi8fbF5ee/AQYH3tE4FVeyVb1w7FiZwWMYM3fPt8Q
pFWVnJfzf7hGSGBOYD8nw+Ph0xEJbWKyLTx+q1IR6AkjPCelSgIiZHOC252LRqVj
2uPCgA3MuQzyNGD6jkYrucDcCAIuX8LJdkb/sqZ2KRzPHYq8jjpPUlWHRKZYvs8F
OoPNcX9yZi25c7LubBR1OjJAusHsNhCM+AmTv4BctTXfKbiX64G+AbC++RtOkeXu
EilNDHiYnwPXUcBNbcImttiMmib2JCDCIRAQnOanQtatwM3KxHgFQ2K/KnKGW4Z8
PfOBTIAVFsZobJtuQAciFWLriHqk3ZLy2l9ymoeTxvkorK4z9X9CzIcoLwy6hEXd
4rWzpr9I15/M1JK5Z216NbVj3T96/tC5Q686BTwI8vE4H18izUNgswoq97GFL5P5
4zRGey/enTvuiZSOIFDOc/eCaCLdE3iE+dImaHz0v167AUHeRHtGVuiRTkLCJWbr
9AILnz9NsUuWVVbcZ+FjrNT+z2Bwoi3ALYwH1BWQFnIw9ZwCqnllb/1iTQARAQAB
zTlHUEcgQ29uZiAoQ2VudCBPUyB3aXRoIGdvZy5jb25mKSA8amVub3llODU5MEBm
ZWN0b2RlLmNvbT7CwZAEEwECACMFAmQzsVwCGwMHCwkIBwMCAQYVCAIJCgsEFgID
AQIeAQIXgAAhCRAz69QhhKig7BYhBL7DDQUQaRZwY/GSVDPr1CGEqKDss+8QAJjb
l3BEzjK9s2fkbZ6bFPkB+zvxd430OGv/Xd3leDuJtn4DC59J3xuSYilWdko74fyk
cWmbmbeuxjYNG2kRkJ/CN8dg7/Wnh5Lf6NCsDdErvfF29qYazrRvd7ufjE/LlmNf
oMUKpp+DuXrH45DvwUZ+qVzI6/jXEHtMvNogS+aR/fy1NaiSn0wRyoguvYJnH9cI
367QLmsDCg002E4zbcgGJxZgfTrVTc2+WlFowHKbDnHu5UAq75l/gUJaquoOTYyR
hAl4uQMy6u/Y28WoDS3hBC8yPsOUOxUqn7jI1KUosTRWw29FANFa+O7ccflDp0+0
LzSfdRo54mFaW+HDFQNdRT9eZ3DOpCflv33bKMdzBJ6zm+cNynAnzErD0JWF2fsS
DamAyiGDbRdy1fFU5jMpcUBRTIx1CN1sKdV9hDWfBlz938v0pKqrXmkqd5FU+uYS
OmwCgJpycfrkpRg5vU2KNnpyswj0Q0wDMzHqGwcrQeD53SFsz7T4O3+S19KsqCdF
9JZcMJjYBzCuvBLZ4wQRWdEGPz4Fiyf6ZxYrt7mokA9DYN6gyNpI/O+suW4gDGXy
jDHcheQUHwvErdERN4VDeGPsYkiVlBHfkQPNdU1VWsxh3Gp3c6kON3l6uK6YrWPV
Erq0gjIEuSTyQL2L6H5p8BMYdmfDqncPTH4RdiShzsFNBGQzsVwBEADIvv1DeiiC
KCh+xMgbHF0uTPmTRg/5haBs9+RweFL+Azp3gwfujymdrV5x6h9FSexrIWk85fmK
KIAXz30p8cl7d/MsvJdk4Pam3SGXWazjtBMffYLKJltn+nXSdGRfLBDsfJVUBDuT
+6UKqMbnqRcFWWLQmg+145RYLnOhm3Lds4T5aJnaqy6/jtXWUnVPQEGMpiy5sSiY
RpS8/MvilqG1RjZPDw/uvQOzlpgqncrBJYr/FdNYX+alP1y5Y8fuM2xCn8lR4X13
GAMSr+/TLRASQzcDVERBXW0QrDx3cTMCgS1p9LSlvDjSBV6+PzDAUXAthJhaBV6B
JDZZ9i/ngczCGr2E2IINv+jl5Fl2pYAplBIgIUAb66pqD9SqT+HzC/Ab+tAsTt0M
xUf94Gz9SfHSmcpw5aQhTPxfaoG4WLH5Pjz8C06DsO5IqQfzGx4Pb6jIr8dF8KnR
A/V/EKm+sxwFqrgjbIZcQtswyK016G8P94/J7n/H6kqWQGX0CNy4y3D8Dwz7JGH4
4wbtB56vIX061wKlCs1fY5NM88xI68Y4xMnVImlrBUaurXKBFK4L6aEpfo/FpTZu
I53WPfk8clBiZMRkcTr74VYa5U3opA36NCdN9s9bnp+0TrBC4qaXnedMrRW5TTdM
HCBEiOHwdhXGtlGGAQjm2jXKUYHimAPSoQARAQABwsF2BBgBAgAJBQJkM7FcAhsM
ACEJEDPr1CGEqKDsFiEEvsMNBRBpFnBj8ZJUM+vUIYSooOxPAQ//ZQhLGg+/oLIK
dkjSlZOPrB9gcSGZLYzjn7pDcDhKfzK20gCLJnP4Xm4hZkHMHMtVURfSzaWp1S8t
72NZYnUKgAbOS+yF1xH6edXy/s4xZWV7j+EkmVvdviGt8FuLW4tML5cXYLybBraG
A/3Gd6I55KxqvBbUi2HswL7Lz9pMP/BOukHK06IG3hupriQCBGxaBlu5YtLD08jp
FqdPPo0F/jS8dQfv6IhC4cRSWhVJsl6Qna2+NBswsyCeSV6A02lK18fTmqct5Z0v
wjPoDcCNTdhyzkJLBorCbzbH64OSw9jWPBeWBriSMevspAdr8+Um04JjSTLalmJz
uDHLr0JgpjrqkwhAtxZ49x06nS2A0UJanKfZGOXO2nEC/GYwOB/QBpu7W99mbScL
TOvqqaPucBIjurrm72vlxzeUOYV8IPkNNcY5Zb0Ljj6rIg8v1LFHsZQ1NOQXy6Ts
vw5Qeu8CBwa9pl88OaqhSyFlwcbRQR3bO1GbX49NdB9NhvhcokG7YoRSSf/HQAMQ
c1Lf1r6OSzHS4ag9F9VbU5XmUziuVJcgixBgSARSnBkqgOI09PjSX+9SISDQ0wgV
pYy6fbnZ0wSB7ATA5OQRTLvSHrscvQhiHYtkFTj7WrBLDyGJpzEDVW2HNEYu4QNi
CT4O8XDoe6cz2q50gHNoEIUWviBIFks=
=RtCX
-----END PGP PUBLIC KEY BLOCK-----
"""


@pytest.mark.parametrize(
    "status_ok, status_code, key_bloc_content, should_pass",
    [
        # Invalid key format.
        (True, status.HTTP_200_OK, re.sub(PGP_BEGIN_BLOCK, b"", CN_PUB_KEY), False),
        (True, status.HTTP_200_OK, re.sub(PGP_END_BLOCK, b"", CN_PUB_KEY), False),
        # Missing key scenarios.
        (True, status.HTTP_404_NOT_FOUND, CN_PUB_KEY, False),
        (False, status.HTTP_200_OK, CN_PUB_KEY, False),
        # Valid key scenario.
        (True, status.HTTP_200_OK, CN_PUB_KEY, True),
    ],
)
def test_download_ascii_armored_pgp_key(
    patch_request,
    status_ok: bool,
    status_code: int,
    key_bloc_content: str,
    should_pass: bool,
) -> None:
    """Verify that:
    * A bad key formats raise an error:
        -> Missing PGP key BEGIN block.
        -> Missing PGP key END block.
    * A Valid key returns the downloaded public key bloc.
    * A missing key raises an error.
    """
    mock_request: mock.Mock = patch_request(
        mock.Mock(ok=status_ok, status_code=status_code, content=key_bloc_content)
    )
    with contextlib.nullcontext() if should_pass else pytest.raises(KeyDownloadError):
        downloaded_key = download_ascii_armored_public_pgp_key(
            fingerprint=CN_FINGERPRINT, keyserver_url=KEYSERVER_URL
        )
        assert CN_PUB_KEY == downloaded_key
        assert mock_request.assert_called_once
        mock_call_args_value = mock_request.call_args.kwargs["url"]
        assert KEYSERVER_URL in mock_call_args_value
        assert CN_FINGERPRINT in mock_call_args_value


def test_download_ascii_armored_pgp_key_no_connection(patch_request) -> None:
    """Verify a non-existing keyserver raises an error."""

    mock_request: mock.Mock = patch_request(
        mock.Mock(ok=False, status_code=status.HTTP_503_SERVICE_UNAVAILABLE)
    )
    mock_request.side_effect = requests.exceptions.ConnectionError
    with pytest.raises(KeyDownloadError):
        download_ascii_armored_public_pgp_key(
            fingerprint=CN_FINGERPRINT, keyserver_url="https://fake.server.org"
        )


@pytest.mark.parametrize(
    "key_bloc_content, fingerprint, expected_exception, user",
    [
        # Non-verified deleted key (i.e. missing user ID info).
        (
            CN_PUB_KEY_NONVERIFIED_4096,
            CN_NONVERIFIED_FINGERPRINT_4096,
            KeyNotVerifiedError,
            None,
        ),
        # Revoked key
        (
            CN_PUB_KEY_REVOKED,
            CN_FINGERPRINT_REVOKED,
            KeyRevokedError,
            None,
        ),
        # Fingerprint of returned key does not match fingerprint from request
        (
            CN_PUB_KEY_VERIFIED_4096,
            CN_NONVERIFIED_FINGERPRINT_4096,
            KeyDownloadError,
            None,
        ),
        # Bad key algorithm (DSA keys are not accepted).
        (
            CN_PUB_KEY_VERIFIED_3072_DSA,
            CN_FINGERPRINT_3072_DSA,
            KeyDownloadError,
            None,
        ),
        # Bad key length.
        (
            CN_PUB_KEY_VERIFIED_3072,
            CN_FINGERPRINT_3072,
            KeyDownloadError,
            None,
        ),
        # Valid key scenario.
        (CN_PUB_KEY_VERIFIED_4096, CN_FINGERPRINT_4096, None, None),
        # Bad key cipher (SHA1 keys are not accepted).
        (
            CN_PUB_KEY_SHA1,
            CN_FINGERPRINT_SHA1,
            KeyInvalidError,
            None,
        ),
    ],
)
def test_download_key_metadata_with_validation(
    key_bloc_content: str,
    fingerprint: str,
    expected_exception: Exception | None,
    user,
) -> None:
    """Verify that:
    * A non-verified/deleted/revoked key (key missing the user ID information)
      raises an error.
    * A mismatch between the specified fingerprint and the fingerprint
      extracted from the key bloc raises an error.
    * Non RSA keys and keys with length < 4096 raise an error.
    * A Valid key bloc returns a metadata object with the correct user ID and
      email.
    """
    if user is None:
        user = (CN_USER_ID, CN_EMAIL)
    mock_download_pgp_key = mock.Mock(return_value=key_bloc_content)
    with mock.patch(
        "projects.utils.pgp.download_ascii_armored_public_pgp_key",
        mock_download_pgp_key,
    ):
        with (
            pytest.raises(expected_exception)
            if expected_exception
            else contextlib.nullcontext()
        ):
            key_metadata = download_key_metadata_with_validation(
                fingerprint=fingerprint,
                keyserver_url=KEYSERVER_URL,
            )
            assert key_metadata.user_id == user[0]
            assert key_metadata.email == user[1]
