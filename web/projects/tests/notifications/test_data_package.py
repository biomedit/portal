import json
import pytest

from projects.notifications.data_package import (
    create_data_package_landed_notification,
)
from projects.models.pgp import PgpKeyInfo

ALICE = "411CE00000000000000000000000000000000000"
BOB = "8080000000000000000000000000000000000000"
METADATA = {
    "sender": ALICE,
    "recipients": [BOB],
}


@pytest.mark.django_db
def test_create_data_package_landed_notification(
    data_package_factory,
    data_transfer_factory,
    project_factory,
    data_provider_factory,
    node_factory,
    pgp_key_info_factory,
):
    data_package = data_package_factory(
        metadata=json.dumps(METADATA),
        data_transfer=data_transfer_factory(
            id=42,
            project=project_factory(
                name="My Project", destination=node_factory(name="My Node")
            ),
            data_provider=data_provider_factory(name="My Provider"),
        ),
        file_name="filename.zip",
    )
    pgp_key_info_factory(
        fingerprint=ALICE, status=PgpKeyInfo.Status.APPROVED, key_user_id="Alice"
    )
    pgp_key_info_factory(
        fingerprint=BOB, status=PgpKeyInfo.Status.APPROVED, key_user_id="Bob"
    )

    _, body = create_data_package_landed_notification(data_package)

    for s in (
        "42",
        "My Project",
        "My Provider",
        "My Node",
        "Alice",
        "Bob",
        "filename.zip",
    ):
        assert s in body
