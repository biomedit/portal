from unittest.mock import patch, ANY

from projects.notifications import ssh_notification
from projects.notifications.ssh_notification import (
    send_email_notification_ssh_key,
)


class AnyStringWith(str):
    def __eq__(self, other):
        return self in other


def test_send_email_notification_ssh_key(project_factory, user_factory):
    project = project_factory()
    user = user_factory()
    with patch.object(ssh_notification, "sendmail") as mock_sendmail:
        send_email_notification_ssh_key(project, user, "updated their SSH key")
        assert mock_sendmail.call_count == 1
        mock_sendmail.assert_called_with(
            subject=AnyStringWith("user has updated their SSH key"),
            body=AnyStringWith(
                f"updated their SSH key for the project '{project.name}'."
            ),
            recipients=list({user.email, project.destination.ticketing_system_email}),
            email_cfg=ANY,
        )
