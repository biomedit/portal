from enum import Enum
import pytest

from projects.models.data_transfer import DataTransfer
from projects.notifications.dtr_notification import (
    DtrApprovedNotification,
    DtrAuthorizedNotification,
    DtrConfirmCreationNotification,
    DtrCreatedNotification,
    DtrDataProviderApprovalNotification,
    DtrGroupApprovalNotification,
    DtrNodeApprovalNotification,
    DtrRejectedNotification,
)
from projects.serializers.data_transfer import DataTransferSerializer
from projects.tests.factories import (
    DataProviderFactory,
    DataTransferFactory,
    NodeFactory,
    ProjectFactory,
    UserFactory,
    make_data_provider_coordinator,
    make_data_provider_data_engineer,
    make_data_provider_manager,
    make_data_specification_approval_group,
    make_data_specification_approver,
    make_legal_approval_group,
    make_legal_approver,
    make_node_admin,
)

DESTINATION_NODE = "destination_node@email.com"
TRANSFER_NODE = "transfer_node@email.com"
TICKETING_SYSTEM = "to@example.org"
REQUESTOR = "requestor@email.com"
DESTINATION_NA = "na_destination@email.com"
TRANSFER_NA = "na_transfer@email.com"
DPC = "dpc@email.com"
DPDE = "dpde@email.com"
DPM = "dpm@email.com"
ELSI = "elsi@email.com"
FAIR = "fair@email.com"

all_recipients = frozenset(
    (
        DESTINATION_NODE,
        TICKETING_SYSTEM,
        REQUESTOR,
        DESTINATION_NA,
        DPC,
        DPDE,
        DPM,
        ELSI,
        FAIR,
    )
)


class TransferNode(Enum):
    SAME = "SAME"
    DIFFERENT = "DIFFERENT"
    NONE = "NONE"


def setUp(
    transfer_node: TransferNode = TransferNode.DIFFERENT,
    purpose: str = DataTransfer.PRODUCTION,
) -> DataTransfer:
    destination_node = NodeFactory(ticketing_system_email=DESTINATION_NODE)
    make_node_admin(user=UserFactory(email=DESTINATION_NA), node=destination_node)

    match transfer_node:
        case TransferNode.SAME:
            data_provider = DataProviderFactory(nodes=[destination_node])
        case TransferNode.DIFFERENT:
            transfer_node = NodeFactory(ticketing_system_email=TRANSFER_NODE)
            make_node_admin(user=UserFactory(email=TRANSFER_NA), node=transfer_node)
            data_provider = DataProviderFactory(nodes=[transfer_node])
        case TransferNode.NONE:
            data_provider = DataProviderFactory(nodes=[])

    make_data_provider_coordinator(
        user=UserFactory(email=DPC), data_provider=data_provider
    )
    make_data_provider_data_engineer(
        user=UserFactory(email=DPDE), data_provider=data_provider
    )
    make_data_provider_manager(user=UserFactory(email=DPM), data_provider=data_provider)

    if purpose == DataTransfer.PRODUCTION:
        legal_approval_group = make_legal_approval_group()
        make_legal_approver(user=UserFactory(email=ELSI), group=legal_approval_group)
        data_specification_approval_group = make_data_specification_approval_group()
        make_data_specification_approver(
            user=UserFactory(email=FAIR),
            group=data_specification_approval_group,
        )
        project = ProjectFactory(
            destination=destination_node,
            legal_approval_group=legal_approval_group,
            data_specification_approval_group=data_specification_approval_group,
        )
    else:
        project = ProjectFactory(destination=destination_node)

    requestor = UserFactory(email=REQUESTOR)
    data_transfer = DataTransferFactory(
        project=project,
        requestor=requestor,
        data_provider=data_provider,
        purpose=purpose,
    )

    DataTransferSerializer().create_approvals(data_transfer)
    return data_transfer


@pytest.mark.usefixtures("apply_notification_settings")
@pytest.mark.django_db
@pytest.mark.parametrize(
    "transfer_node", (TransferNode.SAME, TransferNode.DIFFERENT, TransferNode.NONE)
)
def test_all_recipients_by_transfer_node(transfer_node):
    data_transfer = setUp(transfer_node=transfer_node)
    expected_recipients = all_recipients
    match transfer_node:
        case TransferNode.NONE:
            expected_recipients = expected_recipients.difference(
                (TRANSFER_NODE, TRANSFER_NA)
            )
        case TransferNode.SAME:
            expected_recipients = expected_recipients.difference(
                (TRANSFER_NODE, TRANSFER_NA)
            )

    dtr_notification = DtrCreatedNotification(data_transfer, "")

    assert expected_recipients == dtr_notification.all_recipients()


@pytest.mark.usefixtures("apply_notification_settings")
@pytest.mark.django_db
@pytest.mark.parametrize("purpose", (DataTransfer.PRODUCTION, DataTransfer.TEST))
def test_all_recipients_by_purpose(purpose):
    data_transfer = setUp(purpose=purpose)
    expected_recipients = all_recipients
    if purpose == DataTransfer.TEST:
        expected_recipients = expected_recipients.difference((ELSI, FAIR))

    dtr_notification = DtrCreatedNotification(data_transfer, "")

    assert expected_recipients == dtr_notification.all_recipients()


@pytest.mark.usefixtures("apply_notification_settings")
@pytest.mark.django_db
@pytest.mark.parametrize(
    "notification_factory",
    (
        lambda data_transfer: DtrRejectedNotification(
            "", data_transfer.approvals[0], UserFactory()
        ),
        lambda data_transfer: DtrAuthorizedNotification(data_transfer, ""),
    ),
)
def test_all_recipients(notification_factory):
    assert all_recipients == notification_factory(setUp()).recipients()


@pytest.mark.django_db
def test_created_recipients():
    data_transfer = setUp()
    dtr_notification = DtrCreatedNotification(data_transfer, "")

    assert dtr_notification.recipients() == frozenset((DPC, DPDE, DPM))


@pytest.mark.usefixtures("apply_notification_settings")
@pytest.mark.django_db
def test_confirm_creation_recipients():
    data_transfer = setUp()
    dtr_notification = DtrConfirmCreationNotification(data_transfer, "")

    assert dtr_notification.recipients() == frozenset((TICKETING_SYSTEM, REQUESTOR))


@pytest.mark.django_db
def test_node_approval_recipients():
    data_transfer = setUp(transfer_node=TransferNode.SAME)
    dtr_notification = DtrNodeApprovalNotification(
        "", data_transfer.node_approvals.first()
    )

    assert dtr_notification.recipients() == frozenset(
        (DESTINATION_NODE, DESTINATION_NA)
    )


@pytest.mark.django_db
def test_group_approval_recipients():
    data_transfer = setUp()
    dtr_notification = DtrGroupApprovalNotification(
        "", data_transfer.group_approvals.first()
    )

    assert dtr_notification.recipients() == frozenset((ELSI,))


@pytest.mark.django_db
def test_data_provider_approval_recipients():
    data_transfer = setUp()
    dtr_notification = DtrDataProviderApprovalNotification(
        "", data_transfer.data_provider_approvals.first()
    )

    assert dtr_notification.recipients() == frozenset((DPC,))
    assert dtr_notification.cc_recipients() == frozenset((DPM,))


@pytest.mark.django_db
def test_approved_recipients():
    data_transfer = setUp()
    dtr_notification = DtrApprovedNotification(
        "", data_transfer.approvals[0], user=UserFactory()
    )

    assert dtr_notification.recipients() == frozenset((REQUESTOR,))
