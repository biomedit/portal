from unittest import mock

import pytest
from django.conf import settings

from projects.models.approval import DataProviderApproval, NodeApproval, GroupApproval
from projects.models.data_transfer import DataTransfer
from projects.notifications import approval
from projects.notifications.approval import (
    create_reject_notification,
    create_approve_notification,
    create_final_approval_notification,
)
from projects.models.user import to_display_name
from ..factories import (
    make_data_specification_approval_group,
    make_legal_approval_group,
    make_data_provider_coordinator,
)


@pytest.mark.parametrize(
    "purpose",
    (DataTransfer.PRODUCTION, DataTransfer.TEST),
)
def test_create_approve_notification(
    purpose,
    data_transfer_factory,
    data_provider_approval_factory,
    node_approval_factory,
    user_factory,
    project_factory,
    group_approval_factory,
):
    project = project_factory(
        legal_approval_group=make_legal_approval_group(),
        data_specification_approval_group=make_data_specification_approval_group(),
    )
    dtr = data_transfer_factory(purpose=purpose, project=project)
    data_provider_approval = data_provider_approval_factory(data_transfer=dtr)
    node_approval = node_approval_factory(data_transfer=dtr)
    legal_approval = group_approval_factory(
        data_transfer=dtr, group=dtr.project.legal_approval_group
    )
    data_specification_approval = group_approval_factory(
        data_transfer=dtr, group=dtr.project.data_specification_approval_group
    )
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, data_provider_approval.data_provider)
    legal_basis_text = "There is a legal basis for the data transfer: Yes"
    data_specification_text = (
        "The data specification of the Data Transfer has been approved:"
    )
    project_space_ready_text = "A B-space has been set up in the BioMedIT node: Yes"
    data_delivery_approved_text = (
        "Internal governance processes have been executed to authorize the "
        "delivery of the requested data in accordance with the project's "
        "specification: Yes"
    )

    with mock.patch.object(approval, "project_metadata") as mock_method:
        dp_subject, dp_body = create_approve_notification(
            data_provider_approval, coordinator, "http://example.com/"
        )
        node_subject, node_body = create_approve_notification(
            node_approval, coordinator, "http://example.com/"
        )
        legal_approval_subject, legal_approval_body = create_approve_notification(
            legal_approval, coordinator, "http://example.com/"
        )
        data_specification_subject, data_specification_body = (
            create_approve_notification(
                data_specification_approval, coordinator, "http://example.com/"
            )
        )

        # project_metadata is always called
        assert mock_method.call_count == 4

    for subject in (
        dp_subject,
        node_subject,
        legal_approval_subject,
        data_specification_subject,
    ):
        # Subject is always the same
        assert subject.startswith(
            f"[For information] DTR-{data_provider_approval.data_transfer.id} -"
            f" Data Transfer Request "
            f"from '{data_provider_approval.data_provider.name}'"
        )

    for body, institution in zip(
        (dp_body, node_body, legal_approval_body, data_specification_body),
        (
            data_provider_approval.data_provider,
            node_approval.node,
            legal_approval.group,
            data_specification_approval.group,
        ),
    ):
        # Body always contains the institution
        assert str(institution) in body
        # Body always contains the user
        assert to_display_name(coordinator) in body
        # Body always contains the dtr url
        assert f"http://example.com/data-transfers/{dtr.id}" in body

    for body in (dp_body, node_body, legal_approval_body):
        # Legal basis is present only if purpose is PRODUCTION
        assert (legal_basis_text in body) == (purpose == DataTransfer.PRODUCTION)
        # Data specification is not mentioned
        assert data_specification_text not in body

    # Legal basis is not present in data specification approval
    assert legal_basis_text not in data_specification_body
    # Data specification is present only if purpose is PRODUCTION
    assert (data_specification_text in data_specification_body) == (
        purpose == DataTransfer.PRODUCTION
    )

    # Project space ready is present only for the hosting node
    assert project_space_ready_text not in dp_body
    assert project_space_ready_text in node_body

    # Data provider checkbox is present only for production data provider
    # approvals
    assert (data_delivery_approved_text in dp_body) == (
        purpose == DataTransfer.PRODUCTION
    )

    for body in (node_body, legal_approval_body, data_specification_body):
        assert data_delivery_approved_text not in body


def test_create_reject_notification(node_approval_factory, user_factory):
    node_approval = node_approval_factory()
    user = user_factory()
    subject, body = create_reject_notification(node_approval, user)
    assert subject.startswith(
        f"[For information] DTR-{node_approval.data_transfer.id} -"
        f" Data Transfer Request "
        f"from '{node_approval.data_transfer.data_provider.name}'"
    )
    assert (
        f"Please be informed that the DTR {node_approval.data_transfer.id} "
        f"has been rejected by {user}."
    ) in body


@pytest.mark.parametrize(
    "purpose, data_specification, data_specification_text_expected",
    (
        (DataTransfer.PRODUCTION, "http://example.com/", True),
        (DataTransfer.PRODUCTION, "", False),
        (DataTransfer.TEST, "http://example.com/", False),
    ),
)
def test_create_final_approval_notification(
    purpose,
    data_specification,
    data_specification_text_expected,
    data_transfer_factory,
    data_provider_factory,
    project_factory,
    node_factory,
    user_factory,
):
    data_provider = data_provider_factory()
    project = project_factory()
    user1, user2, legal_approval_user, data_recipient = user_factory.create_batch(4)
    dtr = data_transfer_factory(
        data_provider=data_provider,
        project=project,
        purpose=purpose,
        data_specification=data_specification,
    )
    dtr.data_recipients.add(data_recipient)

    # NOTE: we can't use approval factories here, they don't produce
    # historical records
    node = node_factory()
    na = NodeApproval.objects.create(
        data_transfer=dtr,
        node=node,
        status=NodeApproval.ApprovalStatus.APPROVED,
    )
    na_history_latest = na.history.latest()
    na_history_latest.history_user = user1
    na_history_latest.save()
    dpa = DataProviderApproval.objects.create(
        data_transfer=dtr,
        data_provider=data_provider,
        status=DataProviderApproval.ApprovalStatus.APPROVED,
    )
    dpa_history_latest = dpa.history.latest()
    dpa_history_latest.history_user = user2
    dpa_history_latest.save()

    legal_approval_group = make_legal_approval_group()
    legal_approval_group.user_set.add(legal_approval_user)
    group_approval = GroupApproval.objects.create(
        data_transfer=dtr,
        group=legal_approval_group,
        status=DataProviderApproval.ApprovalStatus.APPROVED,
    )
    group_approval_history_latest = group_approval.history.latest()
    group_approval_history_latest.history_user = legal_approval_user
    group_approval_history_latest.save()

    subject, body = create_final_approval_notification(dtr)

    assert (
        subject == f"[For information] DTR-{dtr.id} - Data Transfer Request from"
        f" '{data_provider.name}' to '{project.name}' project is approved"
    )
    for s in (
        f"DTR {dtr.id}",
        f"project {project.name} ({project.code})",
        f"{data_provider.name}",
        f"{project.destination.name}",
        f"{to_display_name(dtr.requestor)}",
        f"{to_display_name(dtr.data_recipients.all()[0])}",
        f"{settings.CONFIG.notification.ticket_mail}",
        (
            f"{node.name} by {user1.profile.display_name} at"
            f" {na.change_date:%Y-%m-%d %H:%M:%S}"
        ),
        (
            f"{data_provider.name} by {user2.profile.display_name} at"
            f" {dpa.change_date:%Y-%m-%d %H:%M:%S}"
        ),
        (
            f"{legal_approval_group.name} by"
            f" {legal_approval_user.profile.display_name} at"
            f" {group_approval.change_date:%Y-%m-%d %H:%M:%S}"
        ),
    ):
        assert s in body, f"{s} not in {body}."
    assert (
        "The transferred data will be in accordance with the specification" in body
    ) is data_specification_text_expected
