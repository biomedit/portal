from unittest.mock import patch, ANY

from django.conf import settings

from identities.models import GroupProfile
from projects.models.user import to_display_name
from projects.notifications import dp_group_notification
from projects.notifications.dp_group_notification import (
    send_email_notification_create_data_provider,
    send_notification_add_or_remove_user,
    send_notification_to_dp_manager,
    send_notification_to_dp_data_engineer,
)
from projects.tests import AnyOrder, AnyStringWith
from projects.tests.factories import make_data_provider_coordinator


def test_send_email_notification_create_data_provider(data_provider_factory):
    data_provider = data_provider_factory()
    with patch.object(dp_group_notification, "sendmail") as mock_sendmail:
        send_email_notification_create_data_provider(data_provider)
        assert mock_sendmail.call_count == 2
        mock_sendmail.assert_called_with(
            subject=AnyStringWith(f"Data provider '{data_provider.code}'"),
            body=AnyStringWith("Data Provider has been added"),
            recipients=ANY,
            email_cfg=ANY,
        )


def test_send_notification_add_or_remove_user(
    data_provider_factory, group_factory, user_factory
):
    data_provider = data_provider_factory()
    user = user_factory()
    group = group_factory()
    operation = "added to"
    with patch.object(dp_group_notification, "sendmail") as mock_sendmail:
        send_notification_add_or_remove_user(user, data_provider, group, operation)
        assert mock_sendmail.call_count == 2
        mock_sendmail.assert_called_with(
            subject=AnyStringWith(f"has been {operation} the DP Group '{group.name}'"),
            body=AnyStringWith(f"Data Provider Group: {group.name}"),
            recipients=ANY,
            email_cfg=ANY,
        )


def test_send_notification_to_dp_manager(
    data_provider_factory, user_factory, group_factory
):
    data_provider = data_provider_factory()
    user, dp_manager = user_factory.create_batch(2)
    dp_manager_group = group_factory(
        profile__role=GroupProfile.Role.DATA_PROVIDER_MANAGER
    )
    dp_manager_group.user_set.add(dp_manager, user)
    dp_coordinator, _ = make_data_provider_coordinator(data_provider=data_provider)
    operation = "removed from"
    with patch.object(dp_group_notification, "sendmail") as mock_sendmail:
        send_notification_to_dp_manager(
            user, data_provider, dp_manager_group, operation
        )
        mock_sendmail.assert_called_once()
        mock_sendmail.assert_called_with(
            subject=AnyStringWith(
                f"User '{to_display_name(user)}' has been {operation} the DP Group"
            ),
            body=AnyStringWith(f"Dear {data_provider.name},"),
            recipients=AnyOrder([user.email, dp_manager.email]),
            cc=AnyOrder([dp_coordinator.email]),
            email_cfg=ANY,
        )


def test_send_notification_to_dp_data_engineer(
    data_provider_factory, user_factory, group_factory
):
    data_provider = data_provider_factory()
    user, dp_manager = user_factory.create_batch(2)
    dp_manager_group = group_factory(
        profile__role=GroupProfile.Role.DATA_PROVIDER_MANAGER
    )
    dp_manager_group.user_set.add(dp_manager)
    dp_coordinator, _ = make_data_provider_coordinator(data_provider=data_provider)
    with patch.object(dp_group_notification, "sendmail") as mock_sendmail:
        send_notification_to_dp_data_engineer(user, data_provider, dp_manager_group)
        mock_sendmail.assert_called_once()
        mock_sendmail.assert_called_with(
            subject="[BioMedIT portal] - Your Access to the DP Data Engineer Group",
            body=AnyStringWith(f"Dear {to_display_name(user)},"),
            recipients=AnyOrder([user.email]),
            cc=AnyOrder([dp_manager.email, dp_coordinator.email]),
            email_cfg=ANY,
        )


def test_recipients_for_dp_manager(data_provider_factory):
    data_provider = data_provider_factory()
    assert (nodes := data_provider.nodes).count()
    assert {node.ticketing_system_email for node in nodes.all()} | {
        settings.CONFIG.notification.ticket_mail
    } == {email for email in dp_group_notification.recipients(data_provider)}
    data_provider = data_provider_factory(nodes=[])
    assert len(list(dp_group_notification.recipients(data_provider))) == 1
