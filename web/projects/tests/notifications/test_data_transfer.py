from unittest import mock
import pytest
from django.conf import settings

from projects.models.data_transfer import DataTransfer
from projects.models.project import ProjectRole
from projects.notifications import data_transfer
from projects.notifications.data_transfer import (
    create_data_transfer_approval_request_notification,
    create_data_transfer_confirm_creation_notification,
    create_data_transfer_creation_notification,
)
from projects.serializers.data_transfer import DataTransferSerializer
from ..factories import (
    DataProviderApprovalFactory,
    GroupApprovalFactory,
    NodeApprovalFactory,
    make_data_provider_coordinator,
    make_data_specification_approval_group,
)


def test_data_transfer_creation_notification(
    data_transfer_factory, project_user_role_factory, user_factory
):
    dtr: DataTransfer = data_transfer_factory(purpose=DataTransfer.PRODUCTION)
    pl = project_user_role_factory(project=dtr.project, role=ProjectRole.PL.value).user
    coordinator = user_factory()
    make_data_provider_coordinator(coordinator, dtr.data_provider)
    DataTransferSerializer.create_approvals(dtr)
    subject, body = create_data_transfer_creation_notification(dtr)
    assert (
        subject == f"[For information] DTR-{dtr.id} - Data Transfer Request from "
        f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
    )
    assert "Recurring" not in body
    for s in (
        f"Project Lead: {pl.first_name} {pl.last_name} ({pl.email})",
        (
            f"Data Provider and coordinator: {dtr.data_provider.name}, "
            f"{coordinator.first_name} {coordinator.last_name} ({coordinator.email})"
        ),
        "One time",
        "real patient data: Yes",
        settings.CONFIG.notification.ticket_mail,
        "the Nodes",
        f"{dtr.project.data_specification_approval_group.name}",
        f"{dtr.project.legal_approval_group.name}",
    ):
        assert s in body, f"{s} not in {body}."

    dtr.purpose = DataTransfer.TEST
    dtr.max_packages = -1
    dtr.save()
    notification = create_data_transfer_creation_notification(dtr)[1]
    for s in (
        "Recurring",
        "real patient data: No",
    ):
        assert s in notification


def test_create_data_transfer_confirm_creation_notification(data_transfer_factory):
    dtr: DataTransfer = data_transfer_factory()
    with mock.patch.object(data_transfer, "project_metadata") as mock_method:
        subject, _ = create_data_transfer_confirm_creation_notification(dtr)

        assert (
            subject == f"[For information] DTR-{dtr.id} - Data Transfer Request from "
            f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
        )
        assert mock_method.called


@pytest.mark.django_db
@pytest.mark.usefixtures("apply_notification_settings")
@pytest.mark.parametrize(
    "approval_factory, purpose, data_specification, link_expected, warning_expected",
    (
        (
            NodeApprovalFactory,
            DataTransfer.PRODUCTION,
            "https://exaple.com",
            False,
            False,
        ),
        (
            DataProviderApprovalFactory,
            DataTransfer.PRODUCTION,
            "https://exaple.com",
            True,
            True,
        ),
        (DataProviderApprovalFactory, DataTransfer.PRODUCTION, None, False, True),
        (
            lambda: GroupApprovalFactory(
                group=make_data_specification_approval_group()
            ),
            DataTransfer.PRODUCTION,
            "https://exaple.com",
            True,
            False,
        ),
        (
            DataProviderApprovalFactory,
            DataTransfer.TEST,
            "https://exaple.com",
            False,
            False,
        ),
        (
            GroupApprovalFactory,
            DataTransfer.PRODUCTION,
            "https://exaple.com",
            False,
            False,
        ),
    ),
)
def test_create_data_transfer_approval_request_notification(
    approval_factory,
    purpose,
    data_specification,
    link_expected,
    warning_expected,
    data_transfer_factory,
):
    dtr = data_transfer_factory(purpose=purpose, data_specification=data_specification)
    approval = approval_factory()
    base_url = "http://example.ch/"
    with mock.patch.object(data_transfer, "project_metadata") as mock_method:
        subject, body = create_data_transfer_approval_request_notification(
            dtr, base_url, approval
        )
        assert (
            subject == f"[Action needed] DTR-{dtr.id} - Data Transfer Request from "
            f"'{dtr.data_provider.name}' to '{dtr.project.name}' project"
        )
        for s in (
            approval.institution.name,
            f"{dtr.id}",
            dtr.project.code,
            f"{base_url}data-transfers/{dtr.id}",
            "to@example.org",
        ):
            assert s in body

        assert (
            "Data specification of this DTR are available here" in body
        ) is link_expected

        assert (
            "Important: This technical DTRs should be approved only after" in body
        ) is warning_expected

        assert mock_method.called
