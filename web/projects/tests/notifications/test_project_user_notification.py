from random import sample

import pytest
from django.conf import settings
from django.contrib.auth import get_user_model

from projects.models.project import ProjectRole
from projects.notifications.project_user_notification import (
    pretty_print_permission_changes,
    review_user_roles_notification,
    project_expiration_notification,
)

User = get_user_model()
PROJECT_NAME = "Doghouse"
PROJECT_CODE = "DG1"
TEST_PROJECT_NAME = f"Project name: {PROJECT_NAME}"
TEST_PROJECT_CODE = f"Project code: {PROJECT_CODE}\n"


@pytest.fixture(name="permissions_list")
def permissions_list_fixture(user_factory):
    ww = user_factory(
        first_name="Walter",
        last_name="White",
        email="walter.white@doghouse.com",
        profile__affiliation="lab1",
    )
    jp = user_factory(
        first_name="Jesse",
        last_name="Pinkman",
        email="jesse.pinkman@doghouse.com",
        profile__affiliation="lab2",
    )
    gb = user_factory(
        first_name="Gale",
        last_name="Boetticher",
        email="gale.boetticher@doghouse.com",
        profile__affiliation="",
    )
    gf = user_factory(
        first_name="Gustavo",
        last_name="Fring",
        email="gustavo.fring@doghouse.com",
        profile__affiliation="",
    )
    return [
        (ww, ProjectRole.PL),
        (jp, ProjectRole.DM),
        (jp, ProjectRole.USER),
        (gb, ProjectRole.USER),
        (gf, ProjectRole.NO_ROLE),
    ]


pretty_printed_permission_changes = (
    (
        "User: Walter White (walter.white@doghouse.com) (lab1)\tPermissions: Project"
        " Leader"
    ),
    (
        "User: Jesse Pinkman (jesse.pinkman@doghouse.com) (lab2)\tPermissions: Data"
        " Manager, User"
    ),
    (
        "User: Gale Boetticher (gale.boetticher@doghouse.com) (no"
        " affiliation)\tPermissions: User"
    ),
    (
        "User: Gustavo Fring (gustavo.fring@doghouse.com) (no"
        " affiliation)\tPermissions: User without role"
    ),
)


def test_pretty_print_permission_changes(permissions_list):
    lines = pretty_printed_permission_changes
    assert pretty_print_permission_changes(permissions_list) == "\n".join(lines)


def test_review_user_roles_body(permissions_list):
    subject, body = review_user_roles_notification(
        PROJECT_NAME, sample(permissions_list, len(permissions_list))
    )

    assert subject == f"Periodic review of user roles for the project {PROJECT_NAME}"
    for s in (
        PROJECT_NAME,
        settings.CONFIG.notification.ticket_mail,
        "Project Leader\nWalter White (walter.white@doghouse.com)",
        "Data Manager\nJesse Pinkman (jesse.pinkman@doghouse.com)",
        (
            "User\n"
            "Gale Boetticher (gale.boetticher@doghouse.com)\n"
            "Jesse Pinkman (jesse.pinkman@doghouse.com)"
        ),
    ):
        assert s in body


def test_project_expiration_notification():
    subject, body = project_expiration_notification("My Project", "2024-12-30")
    assert subject == "Project My Project is about to expire"
    for s in ("My Project", "2024-12-30"):
        assert s in body
