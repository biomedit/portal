APPLICATION_JSON = "application/json"
TEST_SERVER_URL = "http://testserver"


class AnyStringWith(str):
    def __eq__(self, other):
        return self in other


class AnyOrder(list):
    def __eq__(self, other):
        return sorted(self) == sorted(other)
