import string
import itertools
from typing import Tuple, Callable

import factory
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from guardian.shortcuts import assign_perm

from identities.models import (
    GroupProfile,
    INTERNAL_DATA_SPECIFICATION_APPROVAL_GROUP_NAME,
    INTERNAL_LEGAL_APPROVAL_GROUP_NAME,
)
from identities.permissions import perm_group_manager
from projects.models.approval import DataProviderApproval, NodeApproval, GroupApproval
from projects.models.const import (
    APP_PERM_NODE_ADMIN,
    APP_PERM_NODE_VIEWER,
    APP_PERM_GROUP_APPROVAL_STATUS,
)
from projects.models.data_provider import (
    DataProvider,
    assign_dp_coordinator_role,
    assign_dp_viewer_role,
    assign_dp_admin_role,
)
from projects.models.data_transfer import DataPackage, DataPackageTrace, DataTransfer
from projects.models.feed import Feed
from projects.models.flag import Flag
from projects.models.node import Node
from projects.models.pgp import PgpKeyInfo
from projects.models.project import (
    Project,
    ProjectRole,
    ProjectUserRole,
    ProjectUserRoleHistory,
)
from projects.models.tile import QuickAccessTile
from projects.models.user import Profile, UserNamespace, CustomAffiliation
from projects.models.terms_of_use import TermsOfUse

User = get_user_model()
USER_PASSWORD = "pass"  # nosec


class FeedFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Feed

    label = factory.Faker("random_element", elements=Feed.FeedLabel)
    title = factory.Faker("sentence")
    message = factory.Faker("paragraph")


class FlagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Flag
        django_get_or_create = ("code",)

    code = "flag"
    description = "This is a flag description"


class QuickAccessTileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = QuickAccessTile

    title = "GitLab"
    url = "https://gitlab.com/"
    image = factory.django.ImageField()
    flag = factory.SubFactory(FlagFactory)


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile
        skip_postgeneration_save = True

    affiliation = factory.Faker("company")
    emails = factory.Faker("email")

    @factory.post_generation
    def post_emails(
        self,
        create,
        extracted,  # noqa: ARG002 unused-method-argument
        **kwargs,  # noqa: ARG002 unused-method-argument
    ):
        if not create:
            return

        if email := self.user.email:
            self.emails = ",".join({email} | set((self.emails or "").split(",")))
            self.save()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        skip_postgeneration_save = True

    username = factory.Sequence(lambda n: f"user_{n}")
    password = factory.django.Password(USER_PASSWORD)
    email = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    profile = factory.RelatedFactory(ProfileFactory, "user")

    class Params:
        basic = factory.Trait(
            username="user-basic",
            email="foo@bar.org",
        )
        staff = factory.Trait(
            username="user-staff",
            is_staff=True,
        )

    @factory.post_generation
    def post_flags(
        self,
        create,
        extracted,
        **kwargs,  # noqa: ARG002 unused-method-argument
    ):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            for flag in extracted:
                self.flags.add(flag)


class UserNamespaceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserNamespace

    name = factory.Iterator(
        itertools.product(string.ascii_lowercase, repeat=2), getter=lambda x: "".join(x)
    )


class GroupProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GroupProfile


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group
        django_get_or_create = ("name",)
        skip_postgeneration_save = True

    name = factory.Sequence(lambda n: f"Group {n}")
    profile = factory.RelatedFactory(GroupProfileFactory, "group")


class NodeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Node

    code = factory.Sequence(lambda n: f"node_{n}")
    name = factory.Sequence(lambda n: f"HPC center {n}")
    node_status = Node.STATUS_ONLINE
    ticketing_system_email = factory.Faker("email")


class CustomAffiliationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CustomAffiliation

    code = factory.Sequence(lambda n: f"custom_affiliation_{n}")
    name = factory.Sequence(lambda n: f"Custom Affiliation {n}")


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Project

    gid = factory.Sequence(lambda n: n)
    code = factory.Sequence(lambda n: f"project_{n}")
    name = factory.Sequence(lambda n: f"Project {n}")
    destination = factory.SubFactory(
        NodeFactory,
        code=factory.Sequence(lambda n: f"project_node_{n}"),
        name=factory.Sequence(lambda n: f"Project Node {n}"),
    )
    legal_approval_group = factory.SubFactory(GroupFactory)
    data_specification_approval_group = factory.SubFactory(GroupFactory)


class ProjectUserRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectUserRole

    project = factory.SubFactory(ProjectFactory)
    user = factory.SubFactory(UserFactory)
    role = ProjectRole.USER.value


class ProjectUserRoleHistoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProjectUserRoleHistory

    changed_by = factory.SubFactory(UserFactory)
    user = factory.SubFactory(UserFactory)
    project = factory.SubFactory(ProjectFactory)
    project_str = factory.LazyAttribute(lambda o: o.project.code)
    role = ProjectRole.USER.value
    enabled = True


class DataProviderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataProvider
        skip_postgeneration_save = True

    name = factory.Sequence(lambda n: f"Data Provider {n}")
    code = factory.Sequence(lambda n: f"data_provider_{n}")

    @factory.post_generation
    def nodes(self, create, extracted, **kwargs):  # noqa: ARG002 unused-method-argument
        if not create:
            # Simple build, do nothing.
            return

        self.nodes.set(extracted if extracted is not None else [NodeFactory()])


class DataTransferFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataTransfer

    project = factory.SubFactory(ProjectFactory)
    data_provider = factory.SubFactory(DataProviderFactory)
    requestor = factory.SubFactory(UserFactory)
    purpose = DataTransfer.TEST


class DataPackageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataPackage

    metadata = "{}"
    metadata_hash = factory.Sequence(lambda n: f"{n}")
    data_transfer = factory.SubFactory(DataTransferFactory)
    file_name = factory.Faker("file_name")


class DataPackageTraceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataPackageTrace

    data_package = factory.SubFactory(DataPackageFactory)
    node = factory.SubFactory(NodeFactory)


class PgpKeyInfoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PgpKeyInfo

    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def fingerprint(x):
        return f"{'A' * (40 - len(str(x)))}{x}"

    key_user_id = factory.Faker("name")
    key_email = factory.Faker("email")
    status = PgpKeyInfo.Status.PENDING


class NodeApprovalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = NodeApproval

    data_transfer = factory.SubFactory(DataTransferFactory)
    node = factory.SubFactory(NodeFactory)
    type = NodeApproval.Type.HOSTING


class DataProviderApprovalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataProviderApproval

    data_transfer = factory.SubFactory(DataTransferFactory)
    data_provider = factory.SelfAttribute("data_transfer.data_provider")


class GroupApprovalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GroupApproval
        skip_postgeneration_save = True

    data_transfer = factory.SubFactory(
        DataTransferFactory, purpose=DataTransfer.PRODUCTION
    )
    group = factory.SubFactory(GroupFactory)

    @factory.post_generation
    def users(self, create, extracted, **kwargs):  # noqa: ARG002 unused-method-argument
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            self.group.user_set.add(*extracted)


class TermsOfUseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TermsOfUse

    node = factory.SubFactory(NodeFactory)
    version_type = TermsOfUse.VersionType.MAJOR
    version = "1.0"
    text = factory.Faker("paragraph")


def make_node_admin(
    user: User | None = None,
    node: Node | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    if user is None:
        user = UserFactory()
    if node is None:
        node = NodeFactory()
    if group is None:
        group, _ = Group.objects.get_or_create(
            profile__role=GroupProfile.Role.NODE_ADMIN,
            profile__role_entity_type__model=GroupProfile.RoleEntityType.NODE.value,
            profile__role_entity_id=node.id,
        )
    group.user_set.add(user)
    assign_perm(APP_PERM_NODE_ADMIN, group, node)
    return user, group


def make_node_viewer(
    user: User | None = None,
    node: Node | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    if user is None:
        user = UserFactory()
    if node is None:
        node = NodeFactory()
    if group is None:
        group, _ = Group.objects.get_or_create(
            profile__role=GroupProfile.Role.NODE_VIEWER,
            profile__role_entity_type__model=GroupProfile.RoleEntityType.NODE.value,
            profile__role_entity_id=node.id,
        )
    group.user_set.add(user)
    assign_perm(APP_PERM_NODE_VIEWER, group, node)
    return user, group


def make_group_manager(
    managed_group: Group, user: User | None = None, manager_group: Group | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if manager_group is None:
        manager_group = Group.objects.create(name=f"Group Manager {managed_group.name}")
    manager_group.user_set.add(user)
    assign_perm(perm_group_manager, manager_group, managed_group)
    return user


def _make_data_provider_user(
    group_profile_role_or_group: GroupProfile.Role | Group,
    assign_fn: Callable[[Group, DataProvider], None],
    user: User | None = None,
    data_provider: DataProvider | None = None,
) -> Tuple[User, Group]:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    if not isinstance(group_profile_role_or_group, Group):
        group, _ = Group.objects.get_or_create(
            profile__role=group_profile_role_or_group,
            profile__role_entity_type__model=GroupProfile.RoleEntityType.DATA_PROVIDER.value,
            profile__role_entity_id=data_provider.id,
        )
    else:
        group = group_profile_role_or_group
    group.user_set.add(user)
    assign_fn(group, data_provider)
    return user, group


def make_data_provider_admin(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    return _make_data_provider_user(
        group or GroupProfile.Role.DATA_PROVIDER_TECHNICAL_ADMIN,
        assign_dp_admin_role,
        user,
        data_provider,
    )


def make_data_provider_manager(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    if user is None:
        user = UserFactory()
    if data_provider is None:
        data_provider = DataProviderFactory()
    if group is None:
        group, _ = Group.objects.get_or_create(
            profile__role_entity_type__model=GroupProfile.RoleEntityType.DATA_PROVIDER.value,
            profile__role_entity_id=data_provider.id,
            profile__role=GroupProfile.Role.DATA_PROVIDER_MANAGER,
        )
    group.user_set.add(user)
    return user, group


def make_data_provider_coordinator(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    return _make_data_provider_user(
        group or GroupProfile.Role.DATA_PROVIDER_COORDINATOR,
        assign_dp_coordinator_role,
        user,
        data_provider,
    )


def make_data_provider_viewer(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    return _make_data_provider_user(
        group or GroupProfile.Role.DATA_PROVIDER_VIEWER,
        assign_dp_viewer_role,
        user,
        data_provider,
    )


def make_data_provider_data_engineer(
    user: User | None = None,
    data_provider: DataProvider | None = None,
    group: Group | None = None,
) -> Tuple[User, Group]:
    return _make_data_provider_user(
        group or GroupProfile.Role.DATA_PROVIDER_DATA_ENGINEER,
        assign_dp_viewer_role,
        user,
        data_provider,
    )


def make_data_provider_users(dp: DataProvider) -> Tuple[User, User, User, User]:
    viewer, technical_admin, coordinator, data_engineer = UserFactory.create_batch(4)
    make_data_provider_viewer(viewer, dp)
    make_data_provider_coordinator(coordinator, dp)
    make_data_provider_admin(technical_admin, dp)
    make_data_provider_data_engineer(data_engineer, dp)
    return viewer, coordinator, technical_admin, data_engineer


def make_project_user(
    project: Project | None = None, role=ProjectRole.USER, user: User | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if project is None:
        project = ProjectFactory()
    ProjectUserRoleFactory(project=project, user=user, role=role.value)
    return user


def make_legal_approver(user: User | None = None, group: Group | None = None) -> User:
    if user is None:
        user = UserFactory()
    if group is None:
        group = make_legal_approval_group()
    group.user_set.add(user)
    # We need to refetch the user from the database as the permission caching
    # prevents us to get the updated version. See following link for more details:
    # https://docs.djangoproject.com/en/4.2/topics/auth/default/#permission-caching
    return User.objects.get(pk=user.pk)


def make_legal_approval_group(
    name: str = INTERNAL_LEGAL_APPROVAL_GROUP_NAME,
    entity=None,
    group: Group | None = None,
) -> Group:
    if group is None:
        group = GroupFactory(
            name=name, profile__role=GroupProfile.Role.ELSI, profile__role_entity=entity
        )
    assign_perm(APP_PERM_GROUP_APPROVAL_STATUS, group)
    return group


def make_data_specification_approver(
    user: User | None = None, group: Group | None = None
) -> User:
    if user is None:
        user = UserFactory()
    if group is None:
        group = make_data_specification_approval_group()
    group.user_set.add(user)
    # We need to refetch the user from the database as the permission caching
    # prevents us to get the updated version. See following link for more details:
    # https://docs.djangoproject.com/en/4.2/topics/auth/default/#permission-caching
    return User.objects.get(pk=user.pk)


def make_data_specification_approval_group(
    name: str = INTERNAL_DATA_SPECIFICATION_APPROVAL_GROUP_NAME,
    group: Group | None = None,
) -> Group:
    if group is None:
        group = GroupFactory(
            name=name, profile__role=GroupProfile.Role.DATA_SPECIFICATION_APPROVAL
        )
    assign_perm(APP_PERM_GROUP_APPROVAL_STATUS, group)
    return group
