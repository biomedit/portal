import pytest

from django_drf_utils.tests.utils import (  # noqa: F401 unused-import
    no_requests,
    patch_request,
    patch_request_side_effect,
)

from portal.config import (
    Email as EmailConfig,
    Notification as NotificationConfig,
    Flags as FlagsConfig,
    Links as LinksConfig,
)

from .factories import (
    DataPackageFactory,
    FeedFactory,
    GroupFactory,
    ProjectFactory,
    ProjectUserRoleHistoryFactory,
    TermsOfUseFactory,
    UserFactory,
    USER_PASSWORD,
    DataTransferFactory,
    DataProviderFactory,
    ProjectUserRoleFactory,
    FlagFactory,
    QuickAccessTileFactory,
    NodeFactory,
    DataProviderApprovalFactory,
    NodeApprovalFactory,
    UserNamespaceFactory,
    ProfileFactory,
    PgpKeyInfoFactory,
    GroupApprovalFactory,
)

SWITCH_USERNAME = "switch@user.com"


@pytest.fixture
def feed_factory(db):  # noqa: ARG001 unused-function-argument
    return FeedFactory


@pytest.fixture
def project_factory(db):  # noqa: ARG001 unused-function-argument
    return ProjectFactory


@pytest.fixture
def user_factory(db):  # noqa: ARG001 unused-function-argument
    return UserFactory


@pytest.fixture
def ppg_key_info_factory(db):  # noqa: ARG001 unused-function-argument
    return PgpKeyInfoFactory


@pytest.fixture
def profile_factory(db):  # noqa: ARG001 unused-function-argument
    return ProfileFactory


@pytest.fixture
def group_factory(db):  # noqa: ARG001 unused-function-argument
    return GroupFactory


@pytest.fixture
def data_transfer_factory(db):  # noqa: ARG001 unused-function-argument
    return DataTransferFactory


@pytest.fixture
def data_provider_factory(db):  # noqa: ARG001 unused-function-argument
    return DataProviderFactory


@pytest.fixture
def node_factory(db):  # noqa: ARG001 unused-function-argument
    return NodeFactory


@pytest.fixture
def project_user_role_factory(db):  # noqa: ARG001 unused-function-argument
    return ProjectUserRoleFactory


@pytest.fixture
def switch_user_auth(db, client):  # noqa: ARG001 unused-function-argument
    """Return authenticated switch user"""
    user = UserFactory(basic=True, username=SWITCH_USERNAME)
    client.login(username=SWITCH_USERNAME, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def basic_user_auth(db, client):  # noqa: ARG001 unused-function-argument
    """Return authenticated basic user"""
    user = UserFactory(basic=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def staff_user_auth(db, client):  # noqa: ARG001 unused-function-argument
    """Return authenticated staff user"""
    user = UserFactory(staff=True)
    client.login(username=user.username, password=USER_PASSWORD)
    yield user
    client.logout()


@pytest.fixture
def flag_factory(db):  # noqa: ARG001 unused-function-argument
    return FlagFactory


@pytest.fixture
def quick_access_tile_factory(db):  # noqa: ARG001 unused-function-argument
    return QuickAccessTileFactory


@pytest.fixture
def node_approval_factory(db):  # noqa: ARG001 unused-function-argument
    return NodeApprovalFactory


@pytest.fixture
def data_provider_approval_factory(db):  # noqa: ARG001 unused-function-argument
    return DataProviderApprovalFactory


@pytest.fixture
def group_approval_factory(db):  # noqa: ARG001 unused-function-argument
    return GroupApprovalFactory


@pytest.fixture
def user_namespace_factory(db):  # noqa: ARG001 unused-function-argument
    return UserNamespaceFactory


@pytest.fixture
def pgp_key_info_factory(db):  # noqa: ARG001 unused-function-argument
    return PgpKeyInfoFactory


@pytest.fixture
def data_package_factory(db):  # noqa: ARG001 unused-function-argument
    return DataPackageFactory


@pytest.fixture
def project_user_role_history_factory(db):  # noqa: ARG001 unused-function-argument
    return ProjectUserRoleHistoryFactory


@pytest.fixture
def terms_of_use_factory(db):  # noqa: ARG001 unused-function-argument
    return TermsOfUseFactory


@pytest.fixture
def apply_notification_settings(settings):
    settings.CONFIG.email = EmailConfig(  # nosec
        host="localhost",
        port=25,
        use_tls=False,
        user="",
        password="",
        from_address="from@example.org",
        subject_prefix="",
    )
    settings.CONFIG.notification = NotificationConfig(
        ticket_mail="to@example.org",
        contact_form_recipient="contact_form@example.org",
        links=LinksConfig(onboarding_new_dp_data_engineer=""),
    )
    return settings


@pytest.fixture
def apply_flags_settings(settings):
    settings.CONFIG.flags = FlagsConfig(authorized_user="auth_user")  # nosec
    return settings
