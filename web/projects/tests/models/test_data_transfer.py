import pytest

from projects.models.pgp import PgpKeyInfo


def test_approvals(node_approval_factory):
    node_approval = node_approval_factory()
    data_transfer = node_approval.data_transfer
    assert len(data_transfer.approvals) == 1
    assert data_transfer.approvals[0] == node_approval


@pytest.mark.django_db
@pytest.mark.parametrize(
    "metadata, expected",
    (
        ('{"sender": "8080000000000000000000000000000000000000"}', True),
        ("invalid JSON", False),
        ('{"sender": "411CE00000000000000000000000000000000000"}', False),
    ),
)
def test_sender(metadata, expected, pgp_key_info_factory, data_package_factory):
    pgp_key_info_factory(
        status=PgpKeyInfo.Status.APPROVED,
        fingerprint="8080000000000000000000000000000000000000",
    )

    data_package = data_package_factory(metadata=metadata)
    assert (data_package.sender is not None) is expected


@pytest.mark.django_db
@pytest.mark.parametrize(
    "metadata, expected",
    (
        ('{"recipients": ["8080000000000000000000000000000000000000"]}', 1),
        ("invalid JSON", 0),
        ('{"recipients": ["411CE00000000000000000000000000000000000"]}', 0),
    ),
)
def test_recipients(metadata, expected, pgp_key_info_factory, data_package_factory):
    pgp_key_info_factory(
        status=PgpKeyInfo.Status.APPROVED,
        fingerprint="8080000000000000000000000000000000000000",
    )

    data_package = data_package_factory(metadata=metadata)
    assert len(data_package.recipients) == expected
