import pytest

from django.db import IntegrityError
from unittest.mock import patch

from projects.models import terms_of_use
from projects.models.terms_of_use import TermsOfUseAcceptance


@pytest.mark.django_db
def test_uniqueness_constraint(user_factory, terms_of_use_factory):
    user = user_factory()
    terms_of_use = terms_of_use_factory()

    TermsOfUseAcceptance.objects.create(user=user, terms_of_use=terms_of_use)

    with pytest.raises(IntegrityError):
        TermsOfUseAcceptance.objects.create(user=user, terms_of_use=terms_of_use)


@pytest.mark.django_db
def test_notify_new_terms_of_use(terms_of_use_factory, user_factory):
    with patch.object(terms_of_use, "sendmail") as mock_sendmail:
        # No email is sent if there are no users to notify
        old = terms_of_use_factory()
        assert not mock_sendmail.called

        # An email is sent when there are users to notify
        mock_sendmail.reset_mock()
        TermsOfUseAcceptance.objects.create(user=user_factory(), terms_of_use=old)
        new = terms_of_use_factory(node=old.node)
        assert mock_sendmail.called

        # No email is sent on updating an existing instance
        mock_sendmail.reset_mock()
        new.save()
        assert not mock_sendmail.called
