from unittest.mock import patch, Mock, mock_open

import pytest
from django.urls import reverse

from identities.tests import APPLICATION_JSON
from identities.tests.conftest import (  # noqa: F401 unused-import
    oauth2_client_factory,
)
from portal.config import JWTKey, OAuth
from projects.apps import APP_NAME
from projects.models.data_transfer import DataTransfer
from projects.models.project import ProjectRole
from projects.tests.factories import (
    make_project_user,
    USER_PASSWORD,
    make_data_provider_data_engineer,
)
from projects.views import sts

ACCESS_KEY_ID = "2EEUTKX0FUW9J41NJLWF"
SECRET_ACCESS_KEY = "CquM10JsvIODA4czdgQXdycLUiOUpRzTEvc2EIdX"
SESSION_TOKEN = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9..."
EXPIRATION = "2023-02-28T09:00:51Z"

SUCCESSFUL = f"""<?xml version="1.0" encoding="UTF-8"?>
<AssumeRoleWithWebIdentityResponse xmlns="https://sts.amazonaws.com/doc/2011-06-15/">
<AssumeRoleWithWebIdentityResult><AssumedRoleUser><Arn></Arn><AssumeRoleId></AssumeRoleId></AssumedRoleUser>
<Credentials><AccessKeyId>{ACCESS_KEY_ID}</AccessKeyId>
<SecretAccessKey>{SECRET_ACCESS_KEY}</SecretAccessKey>
<SessionToken>{SESSION_TOKEN}</SessionToken>
<Expiration>{EXPIRATION}</Expiration></Credentials>
<SubjectFromWebIdentityToken>2</SubjectFromWebIdentityToken></AssumeRoleWithWebIdentityResult>
<ResponseMetadata><RequestId>1747EE2A410B0348</RequestId></ResponseMetadata></AssumeRoleWithWebIdentityResponse>"""

FAILED = """<?xml version="1.0" encoding="UTF-8"?>
<ErrorResponse xmlns="https://sts.amazonaws.com/doc/2011-06-15/">
<Error><Type></Type><Code>InvalidParameterValue</Code>
<Message>Role arn:minio:iam:::role/dummy-internal does not exist</Message></Error>
<RequestId>1747EE5D45F61338</RequestId></ErrorResponse>"""


@pytest.fixture(scope="session")
def jwt_key():
    public_key = """-----BEGIN PUBLIC KEY----------BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAul5HcIdRK1iEf2owC0DU
V33L3e6NQkICg4nuFMf9SbPnmp1UiKmWymj3+ocaNqYuNU+m1YlMnrMjy0toHltL
1hCdkAPC/xNIXYigdcZNLzWWQBO/Xo2fa2KOFCW8qMRwqRixDuWnDSf7P9WqJ1G1
lyR6b0/en+rkaeXDEfHhkJfG/q+8B8n3ni/rFcfoShq3dN6SuPnzNOePyFAUV+JC
7LgyAiBtV+aFEIShLmXkIpyRFl9lEUfPTMZWT20wrOctEFPtiSAmlRM2Jbvlxbvz
dNLPOfeEL3jc8I6PkO/K9klPBjaR9WHA2Rg489ECSSMW6ddDgHq3fs42h8GRcwOd
aWfF8LM5RykhM6r2OkwNQCzkTImev1T3luqEanvbYNWkWRhZIY5qBEFCiJ7hZFjl
0fXezTqMBBZlrJ2ZWqSr7jo2tV342+xJ6AXGtei8SJXKx/Qq6m8NKUNzfi5kAnAw
YrHItNlfcpY2/wHmDcJd7rZSvDyB4t9XR0VNUfZ+i/O2u3xyrlpc5McjAu7s7dnu
DLWhbZvzvu0Yw/bmJPMOJzrLce6+qAkzVfIgzuSO7Fb79a05tiEiHvumeEKSRADQ
4xkjcEW06letrFx0tRSV5ALeqvdhGjn9xgsnOKbvbXyreSZ9c5rR+E58SdU34iQ/
XZTshOrgPcChxwvfHBU0XpcCAwEAAQ==
-----END PUBLIC KEY-----"""
    private_key = """-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAul5HcIdRK1iEf2owC0DUV33L3e6NQkICg4nuFMf9SbPnmp1U
iKmWymj3+ocaNqYuNU+m1YlMnrMjy0toHltL1hCdkAPC/xNIXYigdcZNLzWWQBO/
Xo2fa2KOFCW8qMRwqRixDuWnDSf7P9WqJ1G1lyR6b0/en+rkaeXDEfHhkJfG/q+8
B8n3ni/rFcfoShq3dN6SuPnzNOePyFAUV+JC7LgyAiBtV+aFEIShLmXkIpyRFl9l
EUfPTMZWT20wrOctEFPtiSAmlRM2JbvlxbvzdNLPOfeEL3jc8I6PkO/K9klPBjaR
9WHA2Rg489ECSSMW6ddDgHq3fs42h8GRcwOdaWfF8LM5RykhM6r2OkwNQCzkTIme
v1T3luqEanvbYNWkWRhZIY5qBEFCiJ7hZFjl0fXezTqMBBZlrJ2ZWqSr7jo2tV34
2+xJ6AXGtei8SJXKx/Qq6m8NKUNzfi5kAnAwYrHItNlfcpY2/wHmDcJd7rZSvDyB
4t9XR0VNUfZ+i/O2u3xyrlpc5McjAu7s7dnuDLWhbZvzvu0Yw/bmJPMOJzrLce6+
qAkzVfIgzuSO7Fb79a05tiEiHvumeEKSRADQ4xkjcEW06letrFx0tRSV5ALeqvdh
Gjn9xgsnOKbvbXyreSZ9c5rR+E58SdU34iQ/XZTshOrgPcChxwvfHBU0XpcCAwEA
AQKCAgApx6D0VSGZOgraFZAXtbzksErXwfbg97NgkbdR6VObBj3Rp5uf14T6c3XE
MC1sOuz4/pZEILeQjFBl7s20oHrrzmXEN1Oro0MB1PND8+SxxK2eR88K/2zRwhJf
6eXVX9Rx1Bs9X7aHeIMW2GuAzQyMiZz4/0rFRv9VzjBhAXyz0ZLG3dalWF2ulMd6
5onosvyVsp/p3/7pyU+udGVbHloqf1CWsXZR0lfaUCFiKW1vZPIRtekOaq7KY1xO
Cghzeo/brTT/HLoJiXkGFI16OpxlgCz8rcRwcAP32b3NnDQ/6kdQ+ULXY8a75HZA
EocUpk0otx1m3BvuzYPkAzz8L79F049glydTSlcEyyEjW03R4da/GEzKU2CYvrTL
sJLb2Tt26bbr1cOvkz5oVDdsP8SGaNb6HPHYor3YzkRmSbu8WksFd6s7RGVaeZGO
DTZzZ/WAIwUKHHF6KH/9iBr7GlFAgvsZGQdLC/LZDkbgdA7/SBe5y2Z/CEctQwnU
8OiIjgS2LE/ueuvg27mExuO2QXwQxrPS/jIrQwF5IoAKeAyBNUGH1WDmSzRLKtLh
73dQIsMRaldWpcU1eTtSJEmlo1DhWW3tl5yE84Vcio1j9zpbbWXi9oC/OAfchjNt
lf7R9P3WSzlDNOq2jJdpDmoAPBh8bs+SPmBpH5dFvlf03bHlwQKCAQEA8e+MuE1c
psWTZSy0USAq13uZzy5jdPogq2CoFS60W9C8K3HSC1+psDATMHrfYNT0DM9Kxdaa
8WdVVX72VFc3/79VpWr3iaMnnrVtNvKc/B0TRkK5TQTG5SXmGido+75hx3W3/k12
r1ahKB5igsrlkPftHZWQ9RzQcQKhF/gLC428Sv0G1MAjvGC5UtkF0xIQpKUyh2sR
FFEY/gEGhiCveO0dkvXvr3UZZ3knxrSgsNZebvTeKkZkXhOn7wMnr4NIGLJaUGsk
qbmHI8RzYrGAxfRVN+G0kg0aYO/MklUU8Ia7QzTwtTS7hMtWx2fMoV3++fjFx4tN
Hcj2R+gLe/SQnwKCAQEAxTPIcLZYdpvDXOSphLK5wbVZdkbF87u8GJHo64s2IKAa
kep9XG+ryIRaNBFXMRCwg2rDIwYGBiTSb4fydcsFt90RMb8eCCw+FiXZ91ZhHv6f
jUSXbGhQEraX3kgdBYzJtesRIrZ4L614Px8pa35Y04v7mmwsTKxA++VZxNnWLCnC
m6RxUSiSWfbC4HFZ5GNeOOliAY5MmRA4h2gQaihXLYD8Nbde/wVtIuiq6LijbpyP
1KnjYBq/uwq0dEA/tREGyLIzWANeG54jIctBZ8NN/AD6/qYeeyP1XoaIbDS6MoQH
9UXj4Ndf7XtSt5zzC0QxGtj23MBaXh42E/LTuUMXCQKCAQA/uWcJTGcxDi1qosHD
9IMUStwSdBeCQjfErCrZmXmsDWji0JIQqNvbKm6DgHnB2ZWGojsBrSsWlq8lW9Gq
GWcIl+8JKQmWWUEgTo07wd+TkFz5Tq3cGXwB81+OV/8Q/+5lSR5zfxfZyymblZVA
+gxJiOQNvM13omVMvvKLi6vtGSIVZlwcQZeoEJn/tmYQWFvF4jVS8eKHxWydystb
HNTbNylpAhplzqQoWwMdFYqVyYEKHLk/zVa2aNr824nicuPUjXW4ZOA9TD7KLv5t
am+OtHKVy39yUL039zGdAYgIapR8eT9Fm2Qg8CYlU7PUdRup39rIWmqvg7VK71XH
Cn3lAoIBAQCVPdFN69IbQVqirLLWKGSIgvSHnJwnD0cU2SgLSrnWCxTQpUPmduTs
xa12hkEUcusDKgMwOFjKwYc4vQvztYhIU9e1bEwQJ1t9a3v/wbGSwWSChwyKEI+4
+dMJeE3ua1Qkhxfay3k9q+y66JxyK5aR0vjBL2h/RkWP3U0Iu0to9NnA7LyNjUTl
LOji7yX2wHUIYSe4wbaaiDofz9PMY2dGmr1E8dfE7JhhuO/PXBzjkz0O1turWdMM
fNeAuzb7kz0tGh/dVf8cq5su8iI3owkb1KofmhyrWNVTikEokubvYBiduRzyhkyk
75X0O6V8O0lmZhi9jB1X6UlH45f0Nx/xAoIBAQCxoWpbtqGvWZd7xp/1AnbRuPGH
7hekfzL09zSr1T0id/Z9obHRqKoNfbY3Dlx+A8bhBkuew6Ao3TC4Tw4YEm6HAkE2
vfRLHmJsbaZ4V7jiLTy2Sl17GsHApu7Dq2bG/FykqbBJXt/uwCWN9Puys+2+z4vX
/PV8trv+drJ2KA6dZ1qQ8OPsyYXE/hIcaUbhJN1tCKNBsF+JkYF9zyO8x9Y3ggiV
64inGPabQG4Fzn1QpwAmmr4X6crmRFDAFpxGImDof/SWULiOeppK+9dIxI8a+6hu
JXZtg05S3+egTqhOW1p67Tt7iLMTprB0geu5pJnjZLTrvL6zu7BrjNRD3te2
-----END RSA PRIVATE KEY-----"""
    with (
        patch(
            "projects.oauth.open", mock_open(read_data=private_key)
        ) as mocked_private_key,
        patch(
            "projects.views.oauth.open", mock_open(read_data=public_key)
        ) as mocked_public_key,
    ):
        yield JWTKey(private_path=mocked_private_key, public_path=mocked_public_key)


def _mock(status_code, text):
    mock_response = Mock()
    mock_response.status_code = status_code
    mock_response.text = text
    return mock_response


@pytest.fixture
def dtr_factory(
    project_factory,
    data_transfer_factory,
    node_factory,
    oauth2_client_factory,  # noqa: F811 redefined-while-unused
):
    def _dtr_factory(
        dtr_status=DataTransfer.AUTHORIZED, oauth2_client=oauth2_client_factory()
    ):
        node = node_factory(
            oauth2_client=oauth2_client, object_storage_url="http://minio"
        )
        project = project_factory(destination=node)
        project_dm = make_project_user(project, ProjectRole.DM)
        make_project_user(project, ProjectRole.USER)
        dtr = data_transfer_factory(
            id=1,
            project=project,
            requestor=project_dm,
            status=dtr_status,
            data_provider__nodes=[node],
        )
        # Generates a new user
        make_data_provider_data_engineer(data_provider=dtr.data_provider)
        return dtr

    return _dtr_factory


def test_post_sts(
    client,
    settings,
    jwt_key,
    dtr_factory,
    data_package_factory,
):
    dtr = dtr_factory()
    data_package = data_package_factory(data_transfer=dtr)
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    for u, data in (
        (dtr.data_provider.data_engineers.first(), {"dtr": dtr.id, "action": "write"}),
        (
            next(dtr.project.users_by_role(ProjectRole.DM)),
            {"dtr": dtr.id, "action": "read"},
        ),
        (
            next(dtr.project.users_by_role(ProjectRole.DM)),
            {"file_name": data_package.file_name, "action": "read"},
        ),
    ):
        client.login(username=u.username, password=USER_PASSWORD)
        with patch.object(sts, "requests") as mock_requests:
            mock_response = _mock(200, SUCCESSFUL)
            mock_requests.post.return_value = mock_response
            response = client.post(
                reverse(f"{APP_NAME}:sts"), data, content_type=APPLICATION_JSON
            )
            assert response.status_code == 200
            credentials = response.data
            assert credentials["access_key_id"] == ACCESS_KEY_ID
            assert credentials["secret_access_key"] == SECRET_ACCESS_KEY
            assert credentials["session_token"] == SESSION_TOKEN
            assert credentials["expiration"] == EXPIRATION
            assert credentials["bucket"] == dtr.project.code.replace("_", "-")
            assert credentials["endpoint"] == dtr.project.destination.object_storage_url
        client.logout()


def test_post_sts_unauthorized(
    client,
    settings,
    jwt_key,
    dtr_factory,
):
    dtr = dtr_factory()
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    for u, action in (
        (dtr.data_provider.data_engineers.first(), "read"),
        (next(dtr.project.users_by_role(ProjectRole.DM)), "write"),
    ):
        client.login(username=u.username, password=USER_PASSWORD)
        data = {"dtr": dtr.id, "action": action}
        response = client.post(
            reverse(f"{APP_NAME}:sts"), data, content_type=APPLICATION_JSON
        )
        assert response.status_code == 403
        client.logout()


def test_post_sts_failed_for_staff(admin_client, jwt_key, settings, dtr_factory):
    dtr = dtr_factory()
    data = {"dtr": dtr.id, "action": "write"}
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    response = admin_client.post(
        reverse(f"{APP_NAME}:sts"), data, content_type=APPLICATION_JSON
    )
    assert response.status_code == 403
    admin_client.logout()


def test_post_sts_bad_request_from_object_storage(
    client, jwt_key, settings, dtr_factory
):
    dtr = dtr_factory()
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    client.login(
        username=dtr.data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    data = {"dtr": dtr.id, "action": "write"}
    with patch.object(sts, "requests") as mock_requests:
        mock_response = _mock(400, FAILED)
        mock_requests.post.return_value = mock_response
        response = client.post(
            reverse(f"{APP_NAME}:sts"), data, content_type=APPLICATION_JSON
        )
        assert response.status_code == 500
    client.logout()


@pytest.mark.django_db
def test_post_sts_no_dtr_id(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    client.login(
        username=dtr_factory().data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"), {"action": "write"}, content_type=APPLICATION_JSON
    )
    assert response.status_code == 400


@pytest.mark.django_db
def test_post_sts_wrong_dtr_id(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    client.login(
        username=dtr_factory().data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"),
        {"dtr": 2, "action": "write"},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == 422


@pytest.mark.django_db
def test_post_sts_no_action(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    dtr = dtr_factory()
    client.login(
        username=dtr.data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"), {"dtr": dtr.id}, content_type=APPLICATION_JSON
    )
    assert response.status_code == 400


@pytest.mark.django_db
def test_post_sts_wrong_action(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    dtr = dtr_factory()
    client.login(
        username=dtr.data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"),
        {"dtr": dtr.id, "action": "foo"},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == 400


@pytest.mark.django_db
def test_post_sts_not_a_data_engineer(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    dtr = dtr_factory()
    client.login(
        username=next(dtr.project.users_by_role(ProjectRole.USER)).username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"),
        {"dtr": dtr.id, "action": "write"},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == 403


@pytest.mark.django_db
def test_post_sts_no_oauth_client(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    dtr = dtr_factory(oauth2_client=None)
    client.login(
        username=dtr.data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"),
        {"dtr": dtr.id, "action": "write"},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == 422


@pytest.mark.django_db
def test_post_sts_dtr_not_authorized(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    dtr = dtr_factory(dtr_status=DataTransfer.INITIAL)
    client.login(
        username=dtr.data_provider.data_engineers.first().username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"),
        {"dtr": dtr.id, "action": "write"},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == 422


@pytest.mark.django_db
def test_post_sts_package_not_found(client, jwt_key, settings, dtr_factory):
    settings.CONFIG.oauth = OAuth(jwt_key=jwt_key)
    dtr = dtr_factory()
    client.login(
        username=next(dtr.project.users_by_role(ProjectRole.DM)).username,
        password=USER_PASSWORD,
    )
    response = client.post(
        reverse(f"{APP_NAME}:sts"),
        {"file_name": "wrong", "action": "read"},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == 422
    assert "Given 'file_name' ('wrong') not found" in response.data["detail"]
