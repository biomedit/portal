import csv

import pytest
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from identities.models import INTERNAL_LEGAL_APPROVAL_GROUP_NAME
from identities.tests.conftest import (  # noqa: F401 unused-import
    oauth2_client_factory,
)
from projects.apps import APP_NAME
from projects.models.data_transfer import DataTransfer
from projects.models.project import Project, ProjectRole
from projects.serializers.data_transfer import (
    UPDATE_MESSAGE_ARCHIVED_PROJECT,
    CREATE_MESSAGE_ARCHIVED_PROJECT,
)
from .. import APPLICATION_JSON
from ..factories import (
    DataProviderFactory,
    DataTransferFactory,
    NodeFactory,
    ProjectFactory,
    ProjectUserRoleFactory,
    USER_PASSWORD,
    UserFactory,
    make_data_provider_users,
    make_node_admin,
    make_node_viewer,
    make_project_user,
    make_data_specification_approver,
    make_data_specification_approval_group,
    make_legal_approval_group,
    make_legal_approver,
    GroupApprovalFactory,
)
from ...models.pgp import PgpKeyInfo
from ...models.user import is_legal_approver

ENDPOINT = "https://example.com"
REVERSE_URL = f"{APP_NAME}:datatransfer"


def make_transfer_path(data_transfer: DataTransfer):
    dp_node = data_transfer.data_provider.nodes.first()
    path = [
        data_transfer.data_provider.name,
        dp_node.name,
        data_transfer.project.name,
    ]
    if dp_node != data_transfer.project.destination:
        path.insert(2, data_transfer.project.destination.name)
    return path


def response_json(data_transfer: DataTransfer):
    data_manager = make_project_user(data_transfer.project, ProjectRole.DM)
    return {
        "id": data_transfer.pk,
        "project": data_transfer.project.id,
        "project_archived": False,
        "project_name": data_transfer.project.name,
        "max_packages": data_transfer.max_packages,
        "status": data_transfer.status,
        "data_provider": data_transfer.data_provider.code,
        "requestor": data_manager.id,
        "requestor_name": data_manager.profile.display_name,
        "requestor_first_name": data_manager.first_name,
        "requestor_last_name": data_manager.last_name,
        "requestor_email": data_manager.email,
        "requestor_display_id": None,
        "purpose": data_transfer.purpose,
        "packages": [],
        "node_approvals": [],
        "group_approvals": [],
        "data_provider_approvals": [],
        "legal_basis": data_transfer.legal_basis,
        "creation_date": data_transfer.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "change_date": data_transfer.change_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "transfer_path": make_transfer_path(data_transfer),
    }


def test_view_list(client, data_transfer_factory, user_factory):
    url = reverse(REVERSE_URL + "-list")
    data_transfer = data_transfer_factory()
    (dp_viewer, dp_coordinator, dp_technical_admin, _) = make_data_provider_users(
        data_transfer.data_provider
    )
    node_viewer, _ = make_node_viewer(node=data_transfer.project.destination)
    assert client.get(url).status_code == status.HTTP_403_FORBIDDEN
    client.logout()
    r_json = response_json(data_transfer)
    for username, expected_json in (
        (user_factory(staff=True).username, [response_json(data_transfer)]),
        (
            make_project_user(data_transfer.project, ProjectRole.DM).username,
            [response_json(data_transfer)],
        ),
        (make_project_user(data_transfer.project, ProjectRole.PL).username, [r_json]),
        (node_viewer.username, [r_json]),
        (dp_viewer.username, [r_json]),
        (dp_technical_admin.username, [r_json]),
        (dp_coordinator.username, [r_json]),
    ):
        client.login(username=username, password=USER_PASSWORD)
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert response.json(), expected_json
        client.logout()


@pytest.fixture(autouse=True)
def configure_settings(settings):
    # Set the rate to `unlimited` for test cases
    settings.REST_FRAMEWORK["DEFAULT_THROTTLE_RATES"]["unsafe_user"] = None


@pytest.mark.django_db
@pytest.mark.parametrize(
    "purpose,expected_len",
    (
        (DataTransfer.PRODUCTION, 1),
        (DataTransfer.TEST, 0),
    ),
)
def test_view_list_as_group_approver(
    client,
    data_transfer_factory,
    user_factory,
    group_approval_factory,
    purpose,
    expected_len,
):
    legal_approver = user_factory()
    legal_approval_group = make_legal_approval_group()
    legal_approval_group.user_set.add(legal_approver)

    data_specification_approver = user_factory()
    data_specification_approval_group = make_data_specification_approval_group()
    data_specification_approval_group.user_set.add(data_specification_approver)

    data_transfer = data_transfer_factory(purpose=purpose)

    group_approval_factory(data_transfer=data_transfer, group=legal_approval_group)
    group_approval_factory(
        data_transfer=data_transfer, group=data_specification_approval_group
    )

    other_legal_group = make_legal_approval_group("Legal Approval Group")
    other_data_specification_group = make_legal_approval_group(
        "Data Specification Approval Group"
    )

    assert is_legal_approver(legal_approver)
    for username, length in (
        (legal_approver.username, expected_len),
        (data_specification_approver.username, expected_len),
        (
            make_legal_approver(group=other_legal_group).username,
            0,
        ),
        (
            make_data_specification_approver(
                group=other_data_specification_group
            ).username,
            0,
        ),
    ):
        client.login(username=username, password=USER_PASSWORD)
        response = client.get(reverse(REVERSE_URL + "-list"))
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == length
        client.logout()


def test_view_list_with_node_admin(
    user_factory, data_transfer_factory, client, node_factory
):
    data_transfer = data_transfer_factory()
    new_node = node_factory()
    na_user, new_node_user = user_factory.create_batch(2)
    make_node_admin(na_user, data_transfer.project.destination)
    make_node_admin(new_node_user, new_node)

    def assert_response_length(username, length):
        client.login(username=username, password=USER_PASSWORD)
        response = client.get(reverse(REVERSE_URL + "-list"))
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == length
        client.logout()

    # 'new_node' NOT associated to DP yet
    for username, length in (
        (na_user.username, 1),
        (new_node_user.username, 0),
    ):
        assert_response_length(username, length)
    # 'new_node' associated to DP NOW
    data_transfer.data_provider.nodes.set([new_node])
    data_transfer.data_provider.save()
    assert_response_length(new_node_user.username, 1)


def test_view_list_with_node_viewer(data_transfer_factory, client):
    data_transfer = data_transfer_factory()
    nv_user, _ = make_node_viewer(node=data_transfer.project.destination)
    client.login(username=nv_user.username, password=USER_PASSWORD)
    response = client.get(reverse(REVERSE_URL + "-list"))
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 1
    client.logout()


def assert_access_forbidden(
    client,
    url,
    users=(),
    test_unauthorized=True,
    methods=("get",),
    return_code=status.HTTP_403_FORBIDDEN,
):
    for method in methods:
        # Unauthenticated is not allowed
        if test_unauthorized:
            assert getattr(client, method)(url).status_code == return_code
        for username, user_return_code in users:
            client.login(username=username, password=USER_PASSWORD)
            assert getattr(client, method)(url).status_code == user_return_code
            client.logout()


def test_write_methods_not_allowed(client, user_factory):
    # PUT, DELETE are not allowed
    url = reverse(REVERSE_URL + "-list")
    assert client.post(url).status_code == status.HTTP_403_FORBIDDEN
    node_viewer, _ = make_node_viewer()
    data_manager = make_project_user(role=ProjectRole.DM)
    assert_access_forbidden(
        client,
        url,
        (
            (user_factory(basic=True).username, status.HTTP_403_FORBIDDEN),
            (user_factory(staff=True).username, status.HTTP_405_METHOD_NOT_ALLOWED),
            (node_viewer.username, status.HTTP_403_FORBIDDEN),
            (data_manager.username, status.HTTP_403_FORBIDDEN),
        ),
        methods=("put", "delete"),
        test_unauthorized=False,
    )


def test_update_after_pkg(
    data_transfer_factory, data_package_factory, client, user_factory
):
    transfer = data_transfer_factory(purpose=DataTransfer.PRODUCTION)
    data_package_factory(
        metadata_hash="hash",
        data_transfer=transfer,
        purpose=DataTransfer.PRODUCTION,
    )
    client.login(username=user_factory(staff=True).username, password=USER_PASSWORD)
    url = reverse(REVERSE_URL + "-detail", kwargs={"pk": transfer.id})
    for data, expected_status_code in (
        ({"purpose": DataTransfer.TEST}, status.HTTP_400_BAD_REQUEST),
        ({"status": DataTransfer.UNAUTHORIZED}, status.HTTP_200_OK),
        ({"max_packages": 1}, status.HTTP_200_OK),
    ):
        response = client.patch(url, data={**data}, content_type=APPLICATION_JSON)
        assert response.status_code == expected_status_code
    client.logout()


def test_update_for_archived_project(data_transfer_factory, client, user_factory):
    transfer = data_transfer_factory()
    transfer.project.archived = True
    transfer.project.save()
    client.login(username=user_factory(staff=True).username, password=USER_PASSWORD)
    url = reverse(REVERSE_URL + "-detail", kwargs={"pk": transfer.id})
    response = client.patch(
        url,
        data={"status": DataTransfer.UNAUTHORIZED},
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()[0] == UPDATE_MESSAGE_ARCHIVED_PROJECT
    client.logout()


def test_create_for_archived_project(data_transfer_factory, client, user_factory):
    data_transfer = data_transfer_factory()
    data = {
        "project": data_transfer.project.id,
        "data_provider": data_transfer.data_provider.code,
        "purpose": data_transfer.purpose,
        "max_packages": -1,
        "status": DataTransfer.INITIAL,
        "legal_basis": "some",
    }
    data_transfer.project.archived = True
    data_transfer.project.save()
    client.login(username=user_factory(staff=True).username, password=USER_PASSWORD)
    response = client.post(reverse(REVERSE_URL + "-list"), data=data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()[0] == CREATE_MESSAGE_ARCHIVED_PROJECT
    client.logout()


def test_patch(client, data_transfer_factory, user_factory):
    data_transfer = data_transfer_factory()
    data = {"status": DataTransfer.AUTHORIZED}
    basic_user = user_factory(basic=True)
    staff_user = user_factory(staff=True)
    client.login(username=staff_user.username, password=USER_PASSWORD)
    url = reverse(REVERSE_URL + "-detail", kwargs={"pk": data_transfer.id})
    assert (
        client.patch(
            url, data={**data, "max_packages": -1}, content_type=APPLICATION_JSON
        ).status_code
        == status.HTTP_200_OK
    )
    client.logout()
    node_viewer, _ = make_node_viewer(node=data_transfer.project.destination)
    check_method(
        client,
        "patch",
        REVERSE_URL,
        allowed=(staff_user.username,),
        forbidden=(basic_user.username, node_viewer.username),
        data=data,
        content_type=APPLICATION_JSON,
        ids=(data_transfer.id,),
    )


@pytest.mark.parametrize("purpose", (DataTransfer.PRODUCTION, DataTransfer.TEST))
def test_create(
    purpose,
    client,
    user_factory,
    data_provider_factory,
    node_factory,
    project_factory,
):
    legal_approval_group = make_legal_approval_group()
    data_specification_approval_group = make_data_specification_approval_group()
    node = node_factory()
    project = project_factory(
        destination=node,
        legal_approval_group=legal_approval_group,
        data_specification_approval_group=data_specification_approval_group,
    )
    staff_user = user_factory(staff=True)
    basic_user = user_factory(basic=True)
    node_viewer, _ = make_node_viewer(node=node)
    data_provider = data_provider_factory(nodes=[node])
    data_manager = make_project_user(project, ProjectRole.DM)
    # User `staff` is at the same time DM of the project
    make_project_user(project, ProjectRole.DM, staff_user)
    data = {
        "project": project.id,
        "data_provider": data_provider.code,
        "max_packages": -1,
        "purpose": purpose,
        "legal_basis": "some",
        "data_specification": "http://example.com",
    }
    client.login(username=staff_user.username, password=USER_PASSWORD)
    response = client.post(
        reverse(REVERSE_URL + "-list"),
        data=data,
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == status.HTTP_201_CREATED
    client.logout()
    data_transfer = DataTransfer.objects.get(id=response.json()["id"])
    check_method(
        client,
        "post",
        REVERSE_URL,
        allowed=(
            staff_user.username,
            data_manager.username,
        ),
        forbidden=(
            basic_user.username,
            node_viewer.username,
        ),
        data=data,
        content_type=APPLICATION_JSON,
    )
    check_approvals(data_transfer, client, staff_user)


def check_approvals(data_transfer, client, staff_user):
    (dp_viewer, dp_coordinator, *_) = make_data_provider_users(
        data_transfer.data_provider
    )
    for username, can_node_approve, can_dp_approve, can_group_approve in (
        (staff_user.username, True, True, True),
        (dp_viewer.username, False, False, False),
        (dp_coordinator.username, False, True, False),
    ):
        client.login(username=username, password=USER_PASSWORD)
        response = client.get(reverse(REVERSE_URL + "-list"))
        assert response.status_code == status.HTTP_200_OK
        data_transfers = response.json()
        assert len(data_transfers) == 3
        response = client.get(
            reverse(REVERSE_URL + "-detail", kwargs={"pk": data_transfer.id})
        )
        assert response.status_code == status.HTTP_200_OK
        resp_data_transfer = response.json()
        for approvals_type, expected_approvals in (
            ("node_approvals", 1),
            ("data_provider_approvals", 1),
            ("group_approvals", 2),
        ):
            approvals = resp_data_transfer[approvals_type]
            if (
                data_transfer.purpose == DataTransfer.TEST
                and approvals_type == "group_approvals"
            ):
                assert len(approvals) == 0
            else:
                assert len(approvals) == expected_approvals, (
                    f"{len(approvals)} (instead of {expected_approvals}) approval(s)"
                    f" of type '{approvals_type}' found"
                )
        destination_node = data_transfer.project.destination
        assert (
            resp_data_transfer["node_approvals"][0].items()
            >= {
                "node": {
                    "code": destination_node.code,
                    "id": destination_node.pk,
                    "name": destination_node.name,
                    "node_status": "ON",
                    "oauth2_client": None,
                    "object_storage_url": "",
                    "ticketing_system_email": destination_node.ticketing_system_email,
                    "ip_address_ranges": [],
                },
                "status": "W",
                "type": "H",
                "can_approve": can_node_approve,
            }.items()
        )
        data_provider = data_transfer.data_provider
        assert resp_data_transfer["data_provider_approvals"][0].items() >= (
            {
                "data_provider": {
                    "code": data_provider.code,
                    "enabled": data_provider.enabled,
                    "id": data_provider.pk,
                    "name": data_provider.name,
                    "nodes": [node.code for node in data_provider.nodes.all()],
                },
                "status": "W",
                "can_approve": can_dp_approve,
            }.items()
        )
        if data_transfer.purpose == DataTransfer.PRODUCTION:
            assert resp_data_transfer["group_approvals"][0].items() >= (
                {
                    "group": {
                        "name": INTERNAL_LEGAL_APPROVAL_GROUP_NAME,
                        "profile": {
                            "description": "",
                            "role": "ELSI",
                            "role_entity_id": None,
                            "role_entity_type": None,
                        },
                    },
                    "status": "W",
                    "can_approve": can_group_approve,
                }.items()
            )
        client.logout()


@pytest.mark.parametrize(
    "purpose, has_approval_groups, has_relevant_attribute, expected_status",
    (
        (DataTransfer.PRODUCTION, True, True, status.HTTP_201_CREATED),
        (DataTransfer.TEST, True, False, status.HTTP_201_CREATED),
        (DataTransfer.PRODUCTION, False, False, status.HTTP_201_CREATED),
        (DataTransfer.PRODUCTION, True, False, status.HTTP_400_BAD_REQUEST),
    ),
)
def test_create_without_legal_basis(
    purpose,
    has_approval_groups,
    has_relevant_attribute,
    expected_status,
    client,
    user_factory,
    project_factory,
    data_transfer_factory,
):
    # Using the same test to test both field validations because they are
    # supposed to behave the same.

    data_transfer = data_transfer_factory(
        purpose=purpose,
        project=project_factory(
            data_specification_approval_group=(
                make_data_specification_approval_group()
                if has_approval_groups
                else None
            ),
            legal_approval_group=(
                make_legal_approval_group() if has_approval_groups else None
            ),
        ),
    )
    staff_user = user_factory(staff=True)
    make_project_user(data_transfer.project, ProjectRole.DM, staff_user)
    data = {
        "project": data_transfer.project.id,
        "data_provider": data_transfer.data_provider.code,
        "max_packages": -1,
        "purpose": purpose,
        "legal_basis": "Legal basis" if has_relevant_attribute else "",
        "data_specification": "http://example.com" if has_relevant_attribute else "",
    }
    client.login(username=staff_user.username, password=USER_PASSWORD)
    response = client.post(
        reverse(REVERSE_URL + "-list"),
        data=data,
        content_type=APPLICATION_JSON,
    )
    assert response.status_code == expected_status
    client.logout()


def test_create_with_requestor_and_status(
    pgp_key_info_factory, client, user_factory, data_transfer_factory
):
    data_transfer = data_transfer_factory()
    data_manager = make_project_user(project=data_transfer.project, role=ProjectRole.DM)
    pgp_key_info_factory(
        status=PgpKeyInfo.Status.APPROVED,
        user=data_manager,
    )
    staff_user = user_factory(staff=True)
    data = {
        "project": data_transfer.project.id,
        "data_provider": data_transfer.data_provider.code,
        "purpose": data_transfer.purpose,
        "max_packages": -1,
        "legal_basis": "some",
    }
    for user, status_code, requestor in (
        (
            staff_user,
            status.HTTP_400_BAD_REQUEST,
            "Requestor is not a data manager of the project",
        ),
        (data_manager, status.HTTP_201_CREATED, data_manager.id),
    ):
        client.login(username=user.username, password=USER_PASSWORD)
        for sts in (
            DataTransfer.INITIAL,
            DataTransfer.AUTHORIZED,  # not allowed
        ):
            data["status"] = sts
            response = client.post(
                reverse(REVERSE_URL + "-list"), data=data, content_type=APPLICATION_JSON
            )
            if sts == DataTransfer.AUTHORIZED:
                assert response.status_code == status.HTTP_400_BAD_REQUEST
            else:
                assert response.status_code == status_code
                assert response.data["requestor"] == requestor
        client.logout()


def test_create_with_not_approved_key(client, data_transfer_factory, user_factory):
    data_transfer = data_transfer_factory()
    data_manager = make_project_user(project=data_transfer.project, role=ProjectRole.DM)
    staff = user_factory(staff=True)
    data = {
        "project": data_transfer.project.id,
        "data_provider": data_transfer.data_provider.code,
        "requestor": data_manager.id,
        "max_packages": -1,
        "status": DataTransfer.INITIAL,
        "legal_basis": "some",
    }
    client.login(username=staff.username, password=USER_PASSWORD)
    assert (
        client.post(
            reverse(REVERSE_URL + "-list"), data=data, content_type=APPLICATION_JSON
        ).status_code
        == status.HTTP_400_BAD_REQUEST
    )
    client.logout()


# NOTE: This function has side effects. It creates new data trasnfer objects.
def check_method(
    client,
    method,
    url,
    allowed,
    forbidden,
    data=None,
    ids=(),
    test_unauthorized=True,
    **kwargs,
):
    client_method = getattr(client, method)
    # Unauthenticated is not allowed
    id_kwargs = ({"pk": pk} for pk in ids) if ids else ({},)
    args = () if data is None else (data,)
    status_code = {
        "post": status.HTTP_201_CREATED,
        "delete": status.HTTP_204_NO_CONTENT,
        "patch": status.HTTP_200_OK,
    }[method]
    url_suffix = {"post": "-list", "delete": "-detail", "patch": "-detail"}[method]
    for i_kwargs in id_kwargs:
        url = reverse(url + url_suffix, kwargs=i_kwargs)
        if test_unauthorized:
            assert client_method(url).status_code == status.HTTP_403_FORBIDDEN
        for username in forbidden:
            client.login(username=username, password=USER_PASSWORD)
            assert (
                status.HTTP_403_FORBIDDEN
                == client_method(url, *args, **kwargs).status_code
            ), f"Wrong HTTP status for username '{username}'"
            client.logout()
        for username in allowed:
            client.login(username=username, password=USER_PASSWORD)
            r = client_method(url, *args, **kwargs)
            assert r.status_code == status_code
            client.logout()


class TransferViewTestBase(TestCase):
    def setUp(self):
        self.dest_node = NodeFactory()
        self.legal_approval_group = make_legal_approval_group()
        self.data_specification_approval_group = (
            make_data_specification_approval_group()
        )
        self.project = ProjectFactory(
            destination=self.dest_node,
            legal_approval_group=self.legal_approval_group,
            data_specification_approval_group=self.data_specification_approval_group,
        )
        (
            self.basic_user,
            self.project_leader,
            self.data_manager,
            self.node_viewer,
        ) = UserFactory.create_batch(4)
        self.staff = UserFactory(staff=True)
        for u, r in (
            (self.basic_user, ProjectRole.USER),
            (self.project_leader, ProjectRole.PL),
            (self.data_manager, ProjectRole.DM),
            (self.staff, ProjectRole.DM),
        ):
            ProjectUserRoleFactory(project=self.project, user=u, role=r.value)
        make_node_viewer(self.node_viewer, self.dest_node)
        self.data_provider = DataProviderFactory(nodes=[self.dest_node])
        self.purpose = DataTransfer.PRODUCTION
        self.transfer = self.create_transfer(self.purpose)
        GroupApprovalFactory(
            data_transfer=self.transfer, group=self.legal_approval_group
        )
        GroupApprovalFactory(
            data_transfer=self.transfer, group=self.data_specification_approval_group
        )
        self.node = NodeFactory()

    def create_transfer(self, purpose, project: Project | None = None):
        return DataTransferFactory(
            project=project or self.project,
            data_provider=self.data_provider,
            requestor=self.data_manager,
            status=DataTransfer.AUTHORIZED,
            purpose=purpose,
        )


@pytest.mark.usefixtures("staff_user_auth")
def test_export_data_transfers(client, data_package_factory):
    packages = data_package_factory.create_batch(3)
    url = reverse(f"{APP_NAME}:dtrsexport")
    r = client.get(
        url,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    assert r.headers["Content-Type"] == "text/csv"
    content = list(csv.reader(r.content.decode().strip().split("\r\n")))
    hash_index = content[0].index("Package Metadata Hash")
    assert {u.metadata_hash for u in packages}.issubset(
        {line[hash_index] for line in content}
    )
