from contextlib import contextmanager

import pytest
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.data_transfer import DataTransfer
from projects.models.project import (
    ProjectIPAddresses,
    Resource,
    Project,
    ProjectRole,
)
from projects.notifications.project_archival import project_archive_users_notification
from projects.serializers import UserShortSerializer
from projects.serializers.project import ARCHIVED_READ_ONLY_MESSAGE
from projects.views.project import ProjectViewSet, ProjectUserRoleHistoryViewSet
from .. import APPLICATION_JSON
from ..factories import (
    DataTransferFactory,
    NodeFactory,
    ProjectFactory,
    ProjectUserRoleFactory,
    ProjectUserRoleHistoryFactory,
    UserFactory,
    USER_PASSWORD,
    make_project_user,
    make_node_admin,
    make_node_viewer,
)

User = get_user_model()


def get_project_detail_url(project):
    return reverse(f"{APP_NAME}:project-detail", kwargs={"pk": project.pk})


def get_project_archive_url(project):
    return reverse(f"{APP_NAME}:project-archive", kwargs={"pk": project.pk})


def get_project_unarchive_url(project):
    return reverse(f"{APP_NAME}:project-unarchive", kwargs={"pk": project.pk})


def get_project_services_url(project_id):
    return reverse(
        f"{APP_NAME}:project-services-list", kwargs={"project_pk": project_id}
    )


def get_project_service_detail_url(project_id, service_id):
    return reverse(
        f"{APP_NAME}:project-services-detail",
        kwargs={"project_pk": project_id, "pk": service_id},
    )


def get_member_services_url(project_id, user_id):
    return reverse(
        f"{APP_NAME}:member-services-list",
        kwargs={"project_pk": project_id, "user_pk": user_id},
    )


def get_member_service_detail_url(project_id, user_id, service_id):
    return reverse(
        f"{APP_NAME}:member-services-detail",
        kwargs={"project_pk": project_id, "user_pk": user_id, "pk": service_id},
    )


class ProjectViewTestMixin(TestCase):
    def setUp(self):
        self.destination1 = NodeFactory()
        self.project1 = ProjectFactory(destination=self.destination1)
        self.basic_user = make_project_user(self.project1, ProjectRole.USER)
        self.permission_manager = make_project_user(self.project1, ProjectRole.PM)
        self.pl_user = make_project_user(self.project1, ProjectRole.PL)
        self.staff = UserFactory(staff=True)
        self.node_viewer, _ = make_node_viewer(node=self.destination1)
        self.project2 = ProjectFactory()

        # Setup for project services and project member services
        self.destination_s = NodeFactory()
        self.project_s = ProjectFactory(destination=self.destination_s)
        self.basic_user_s = make_project_user(self.project_s, ProjectRole.USER)
        self.node_admin_s = make_project_user(self.project_s, ProjectRole.USER)
        make_node_admin(self.node_admin_s, self.destination_s)

        self.destination3 = NodeFactory()
        self.project_node_admin = ProjectFactory(
            gid=1500003, destination=self.destination3
        )
        # Node admin of destination3, user of project1 (destination 1):
        self.node_admin = make_project_user(self.project1, ProjectRole.USER)
        make_node_admin(self.node_admin, self.destination3)
        self.ip_address_ranges = [
            {"ip_address": "127.0.0.1", "mask": 32},
            {"ip_address": "127.0.2.1", "mask": 4},
        ]
        self.resources = [
            {
                "name": "Test Resource",
                "description": "Test Resource Description",
                "location": "https://example.com",
                "contact": "ticketing_system_email@example.org",
            },
            {
                "name": "Test Resource 2",
                "description": "",
                "location": "https://example2.com",
                "contact": "",
            },
        ]
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        url = get_project_detail_url(self.project1)
        self.project_users = [
            {"id": u["id"], "roles": u["roles"]}
            for u in self.client.get(url).json()["users"]
        ]
        self.client.logout()
        self.new_user = UserFactory(email="new_user@example.org")

    def assert_user_has_roles(self, user, roles):
        self.assertEqual(
            {p.role for p in self.project1.users.all() if p.user == user},
            {p.value for p in roles},
        )

    def users_payload(self, roles, user=None, **kwargs):
        if not user:
            user = self.new_user
        return {
            "users": self.project_users
            + [{"id": user.pk, "roles": [p.name for p in roles]}],
            **kwargs,
        }

    def put_expected_roles_to_new_user(self, expected_roles, user=None):
        url = get_project_detail_url(self.project1)
        # PM removes DM permission from new_user
        response = self.client.put(
            url,
            self.users_payload(expected_roles, user=user),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_user_has_roles(user or self.new_user, expected_roles)

    @contextmanager
    def pm_add_permission_to_user(self):
        try:
            url = get_project_detail_url(self.project1)
            make_project_user(self.project1, ProjectRole.DM, self.new_user)
            self.client.login(
                username=self.permission_manager.username,
                password=USER_PASSWORD,
            )
            expected_roles = (ProjectRole.USER, ProjectRole.DM)
            # PM adds User permission to new_user
            self.put_expected_roles_to_new_user(expected_roles)
            yield url, expected_roles
        finally:
            self.client.logout()


class TestProjectView(ProjectViewTestMixin):
    def test_view_list(self):
        url = reverse(f"{APP_NAME}:project-list")
        # Unauthenticated is not allowed
        self.assertEqual(self.client.get(url).status_code, status.HTTP_403_FORBIDDEN)
        # Basic authorized user gets an empty list
        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.client.logout()
        # Permission-manager gets a list with one project
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_json = response.json()
        self.assertEqual(len(response_json), 1)
        project = response_json[0]
        self.assertListEqual(
            list(project.keys()),
            list(
                (
                    "id",
                    "gid",
                    "code",
                    "name",
                    "archived",
                    "expiration_date",
                    "destination",
                    "created",
                    "data_transfer_count",
                    "user_has_role",
                    "ssh_support",
                    "resources_support",
                    "services_support",
                    "legal_support_contact",
                    "additional_info",
                )
            ),
        )
        self.assertEqual(project["code"], self.project1.code)
        self.assertEqual(project["name"], self.project1.name)
        self.client.logout()
        # Staff gets a list with 4 projects
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 4)
        # Filter by project name
        response = self.client.get(url, {"search": self.project1.name})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["code"], self.project1.code)
        # Filter by project username
        response = self.client.get(url, {"username": self.basic_user.username})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.client.logout()
        # Node admin gets a list with their project1 and destination3 project:
        self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        projects = {p["name"] for p in response.json()}
        self.assertEqual(projects, {self.project1.name, self.project_node_admin.name})

    def test_view_details(self):
        url1 = get_project_detail_url(self.project1)
        url2 = get_project_detail_url(self.project2)
        url_na = get_project_detail_url(self.project_node_admin)
        test_resource_description = "Test description"
        test_resource_location = "https://resource.org"
        Resource.objects.create(
            project=self.project1,
            name="Test resource",
            location=test_resource_location,
            description=test_resource_description,
        )
        expected_permissions_edit = {
            "name": False,
            "code": False,
            "destination": False,
            "users": [],
            "ip_address_ranges": False,
            "resources": False,
            "legal_support_contact": False,
            "legal_approval_group": False,
            "data_specification_approval_group": False,
            "expiration_date": False,
            "ssh_support": False,
            "resources_support": False,
            "services_support": False,
            "identity_provider": False,
            "additional_info": False,
        }
        all_roles = [
            ProjectRole.PL.name,
            ProjectRole.PM.name,
            ProjectRole.DM.name,
            ProjectRole.USER.name,
            ProjectRole.NO_ROLE.name,
        ]
        with self.subTest("Unauthenticated"):
            # Unauthenticated is not allowed
            self.assertEqual(
                self.client.get(url1).status_code, status.HTTP_403_FORBIDDEN
            )
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_403_FORBIDDEN
            )
        with self.subTest("Basic user"):
            # Basic authorized user can get one project
            self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
            r = self.client.get(url1)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            response = r.json()
            resource = response["resources"][0]
            self.assertTrue("location" in resource)
            self.assertEqual(resource["description"], test_resource_description)
            r_edit = response["permissions"]["edit"]
            assert r_edit == expected_permissions_edit
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Permission Manager"):
            # Permission manager can get one project
            self.client.login(
                username=self.permission_manager.username,
                password=USER_PASSWORD,
            )
            r = self.client.get(url1)
            r_edit = r.json()["permissions"]["edit"]
            assert {
                key: value
                for key, value in expected_permissions_edit.items()
                if key != "users"
            } == {key: r_edit[key] for key in r_edit if key != "users"}
            assert sorted(r_edit["users"]) == sorted(
                [ProjectRole.DM.name, ProjectRole.NO_ROLE.name, ProjectRole.USER.name]
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Project Leader"):
            # Project leader can get one project
            self.client.login(username=self.pl_user.username, password=USER_PASSWORD)
            r = self.client.get(url1)
            r_permissions = r.json()["permissions"]
            r_edit = r_permissions["edit"]
            users = r_edit.pop("users")
            assert r_edit == {
                key: value
                for key, value in expected_permissions_edit.items()
                if key != "users"
            }
            assert len(users) == 4
            assert set(users) == {
                ProjectRole.PM.name,
                ProjectRole.DM.name,
                ProjectRole.USER.name,
                ProjectRole.NO_ROLE.name,
            }
            self.assertFalse(r_permissions["archive"])
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Node Admin"):
            # Node Admin see the projects assigned to them and the ones associated with
            # their node
            self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
            r = self.client.get(url_na)
            r_permissions = r.json()["permissions"]
            r_edit = r_permissions["edit"]
            assert {
                "name": True,
                "code": True,
                "ip_address_ranges": True,
                # Node admin cannot edit node of a project
                "destination": False,
                "resources": True,
                "legal_support_contact": True,
                "legal_approval_group": True,
                "data_specification_approval_group": False,
                "expiration_date": True,
                "ssh_support": True,
                "resources_support": True,
                "services_support": True,
                "identity_provider": True,
                "additional_info": True,
            }.items() <= r_edit.items()
            assert sorted(r_edit["users"]) == sorted(all_roles)
            # can archive projects associated to the node
            assert r_permissions["archive"]
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            r = self.client.get(url1)
            # can NOT archive projects associated to not administered node
            self.assertFalse(r.json()["permissions"]["archive"])
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                self.client.get(url2).status_code, status.HTTP_404_NOT_FOUND
            )
            self.client.logout()
        with self.subTest("Staff"):
            # Staff can get both projects
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            r = self.client.get(url1)
            response = r.json()
            r_edit = response["permissions"]["edit"]
            users = r_edit.pop("users")
            assert {
                "name": True,
                "code": True,
                "ip_address_ranges": True,
                "destination": True,
                "resources": True,
                "legal_support_contact": True,
                "legal_approval_group": True,
                "data_specification_approval_group": True,
                "expiration_date": True,
                "ssh_support": True,
                "resources_support": True,
                "services_support": True,
                "identity_provider": True,
                "additional_info": True,
            }.items() >= r_edit.items()
            assert set(users) == set(all_roles)
            resource = response["resources"][0]
            self.assertEqual(resource["location"], test_resource_location)
            self.assertEqual(resource["description"], test_resource_description)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(self.client.get(url2).status_code, status.HTTP_200_OK)

            # archived project returns read-only permissions
            self.project1.archived = True
            self.project1.save()
            r = self.client.get(url1)
            response = r.json()
            r_edit = response["permissions"]["edit"]
            assert r_edit == {
                "name": False,
                "code": False,
                "ip_address_ranges": False,
                "destination": False,
                "resources": False,
                "users": [],
                "legal_support_contact": False,
                "legal_approval_group": False,
                "data_specification_approval_group": False,
                "expiration_date": False,
                "ssh_support": False,
                "resources_support": False,
                "services_support": False,
                "identity_provider": False,
                "additional_info": False,
            }
            assert response["permissions"]["archive"]
            self.client.logout()

    def test_view_users(self):
        url = reverse(f"{APP_NAME}:project-users", kwargs={"pk": self.project1.id})
        for username in (self.basic_user.username, self.permission_manager.username):
            self.client.login(username=username, password=USER_PASSWORD)
            self.assertEqual(
                self.client.get(url).status_code, status.HTTP_403_FORBIDDEN
            )
            self.client.logout()
        self.client.login(username=self.node_viewer.username, password=USER_PASSWORD)

        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(r.json()),
            2,
        )
        r = self.client.get(url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        r_json = r.json()
        self.assertEqual(
            len(r_json),
            self.project1.users.filter(role=ProjectRole.USER.value).count(),
        )
        for user in r_json:
            self.assertListEqual(
                list(UserShortSerializer.Meta.read_only_fields),
                list(user.keys()),
            )

        user_returned = [
            u for u in r_json if u.get("username") == self.basic_user.username
        ][0]
        self.assertEqual(
            user_returned["local_username"], self.basic_user.profile.local_username
        )
        self.assertEqual(
            user_returned["affiliation"], self.basic_user.profile.affiliation
        )
        self.assertEqual(
            user_returned["affiliation_id"], self.basic_user.profile.affiliation_id
        )
        self.assertEqual(
            user_returned["local_username"], self.basic_user.profile.local_username
        )
        assert user_returned["roles"] == ["USER"]

        # check long representation
        r = self.client.get(url, {"short": "false"})
        user_long_returned = [
            u for u in r.json() if u.get("username") == self.basic_user.username
        ][0]
        self.assertIn("affiliation", user_long_returned["profile"])
        self.assertIn("affiliation_id", user_long_returned["profile"])

        # Select role
        r = self.client.get(url, {"role": ProjectRole.DM.name})
        self.assertEqual(
            len(r.json()),
            self.project1.users.filter(role=ProjectRole.DM.value).count(),
        )
        # Select invalid role
        self.assertEqual(
            self.client.get(url, {"role": "bad_role"}).status_code,
            status.HTTP_400_BAD_REQUEST,
        )

    def test_create_fails_without_staff_rights(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "test",
            "code": "bar",
            "destination": self.destination1.name,
            "ip_address_ranges": [],
            "resources": [],
            "users": [],
            "legal_support_contact": "legal@legal.ch",
            "expiration_date": "2023-01-01",
        }
        # Only staff can create new projects
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        for username in (
            self.basic_user.username,
            self.permission_manager.username,
            self.node_viewer.username,
        ):
            self.client.login(username=username, password=USER_PASSWORD)
            self.assertEqual(
                self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

    def test_create(self):
        url = reverse(f"{APP_NAME}:project-list")
        code = "bar"
        gid = 1000
        data = {
            "gid": gid,  # gid is a read-only field so this value should be ignored
            "name": "test",
            "code": code,
            "destination": self.destination1.code,
            "legal_support_contact": "legal@legal.ch",
            "expiration_date": "2023-01-01",
            "ip_address_ranges": self.ip_address_ranges,
            "resources": self.resources,
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.DM.name,
                    ],
                }
            ],
        }
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )

        model_ip_ranges = [
            {"ip_address": ip.ip_address, "mask": ip.mask}
            for ip in ProjectIPAddresses.objects.all()
        ]
        self.assertEqual(len(model_ip_ranges), len(self.ip_address_ranges))
        assert all(elem in model_ip_ranges for elem in self.ip_address_ranges)

        model_resources = [
            {
                "name": resource.name,
                "description": resource.description,
                "location": resource.location,
                "contact": resource.contact,
            }
            for resource in Resource.objects.all()
        ]
        self.assertEqual(len(model_resources), len(self.resources))
        assert all(res in model_resources for res in self.resources)
        project = Project.objects.get(code=code)
        assert project.gid != gid
        assert project.gid >= settings.CONFIG.project.gid.min
        assert project.gid <= settings.CONFIG.project.gid.max

    def test_norole_mutual_exclusivity(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "Foolish",
            "code": "foolish",
            "destination": self.destination1.code,
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.NO_ROLE.name,
                    ],
                }
            ],
        }
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_400_BAD_REQUEST,
        )

    def test_unique(self):
        url = reverse(f"{APP_NAME}:project-unique")
        for code, expected_status_code in (
            (self.project1.code, status.HTTP_409_CONFLICT),
            ("chuck_norris_research", status.HTTP_200_OK),
        ):
            for user in [self.staff, self.node_admin]:
                self.client.login(username=user.username, password=USER_PASSWORD)
                self.assertEqual(
                    self.client.post(
                        url, {"code": code}, content_type=APPLICATION_JSON
                    ).status_code,
                    expected_status_code,
                )
                self.client.logout()

    def test_create_same_name_as_code(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "test",
            "code": "test",
            "destination": self.destination1.name,
            "legal_support_contact": "legal@legal.ch",
            "expiration_date": "2023-01-01",
            "ip_address_ranges": self.ip_address_ranges,
            "resources": self.resources,
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.DM.name,
                    ],
                }
            ],
        }
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_400_BAD_REQUEST,
        )

    def test_create_without_ip_and_resources(self):
        url = reverse(f"{APP_NAME}:project-list")
        data = {
            "name": "test",
            "code": "bar",
            "destination": self.destination1.code,
            "legal_support_contact": "legal@legal.ch",
            "expiration_date": "2023-01-01",
            "users": [
                {
                    "id": self.basic_user.pk,
                    "roles": [
                        ProjectRole.USER.name,
                        ProjectRole.DM.name,
                    ],
                }
            ],
        }
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.post(url, data, content_type=APPLICATION_JSON).status_code,
            status.HTTP_201_CREATED,
        )

        ip_address_ranges = ProjectIPAddresses.objects.all()
        if ip_address_ranges:
            self.fail("IP Address Ranges should not be defined!")

        resources = Resource.objects.all()
        if resources:
            self.fail("Resources should not be defined!")

    def test_create_with_node_admin(self):
        url = reverse(f"{APP_NAME}:project-list")
        self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
        for destination, expected_status_code in (
            (self.destination3, status.HTTP_201_CREATED),
            (self.destination1, status.HTTP_403_FORBIDDEN),
        ):
            data = {
                "name": "test",
                "code": "bar",
                "destination": destination.code,
                "legal_support_contact": "legal@legal.ch",
                "expiration_date": "2023-01-01",
                "ip_address_ranges": self.ip_address_ranges,
                "resources": self.resources,
                "users": [
                    {
                        "id": self.new_user.pk,
                        "roles": [
                            ProjectRole.USER.name,
                        ],
                    }
                ],
            }
            response = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                response.status_code,
                expected_status_code,
            )
            if expected_status_code == status.HTTP_201_CREATED:
                assert response.json()["name"] == data["name"]
                assert response.json()["code"] == data["code"]

    def test_update_unauthorized(self):
        url = get_project_detail_url(self.project1)
        self.assertEqual(
            self.client.put(url, {}, content_type=APPLICATION_JSON).status_code,
            status.HTTP_403_FORBIDDEN,
        )

        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.put(
                url, {"name": "changed"}, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        self.client.logout()

        self.client.login(username=self.node_viewer.username, password=USER_PASSWORD)
        self.assertEqual(
            self.client.put(
                url, {"name": "changed"}, content_type=APPLICATION_JSON
            ).status_code,
            status.HTTP_403_FORBIDDEN,
        )
        self.client.logout()

    def test_update_permission_manager(self):
        with self.pm_add_permission_to_user() as (url, expected_roles):
            # PM can't assign the PM role
            response = self.client.put(
                url,
                self.users_payload([ProjectRole.PM]),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            # PM can't assign the PL role
            response = self.client.put(
                url,
                self.users_payload([ProjectRole.PL]),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            # PM can't update other fields than "users"
            response = self.client.put(
                url,
                self.users_payload(expected_roles, name="new name"),
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotEqual(response.json()["name"], "new name")
            # PM can't see other projects
            self.assertEqual(
                self.client.put(
                    get_project_detail_url(self.project2),
                    self.users_payload(expected_roles),
                    content_type=APPLICATION_JSON,
                ).status_code,
                status.HTTP_404_NOT_FOUND,
            )

    def test_update_project_leader(self):
        url = get_project_detail_url(self.project1)
        self.client.login(username=self.pl_user.username, password=USER_PASSWORD)
        expected_roles = (
            ProjectRole.USER,
            ProjectRole.DM,
            ProjectRole.PM,
        )
        response = self.client.put(
            url,
            self.users_payload(expected_roles),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # PL can't assign the PL role
        response = self.client.put(
            url,
            self.users_payload([ProjectRole.PL]),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # PL can't update other fields than "users"
        response = self.client.put(
            url,
            self.users_payload(expected_roles, name="new name"),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.json()["name"], "new name")
        # PL can't see other projects
        self.assertEqual(
            self.client.put(
                get_project_detail_url(self.project2),
                self.users_payload(expected_roles),
                content_type=APPLICATION_JSON,
            ).status_code,
            status.HTTP_404_NOT_FOUND,
        )
        self.client.logout()

    def test_update_staff(self):
        url = get_project_detail_url(self.project1)
        for ip_range in self.ip_address_ranges:
            ProjectIPAddresses.objects.create(project=self.project1, **ip_range)
        for resource in self.resources:
            Resource.objects.create(project=self.project1, **resource)
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        data = self.client.get(url).json()
        # Update project field
        updated_ip_ranges = [
            self.ip_address_ranges[0],
            {"ip_address": "159.124.12.2", "mask": 20},
        ]
        updated_resources = [
            self.resources[0],
            {"name": "New Resource", "location": "https://institute3.com"},
        ]
        data.update(
            {
                "name": "new project name",
                "ip_address_ranges": updated_ip_ranges,
                "resources": updated_resources,
                "legal_support_contact": "legal@legal.ch",
                "expiration_date": "2023-01-01",
            }
        )
        # Update by adding an user
        data["users"] = self.project_users + [
            {
                "id": self.new_user.pk,
                "roles": [ProjectRole.USER.name],
            }
        ]
        response = self.client.put(url, data, content_type=APPLICATION_JSON)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        model_ip_ranges = [
            {"ip_address": ip.ip_address, "mask": ip.mask}
            for ip in ProjectIPAddresses.objects.all()
        ]
        self.assertEqual(len(model_ip_ranges), len(updated_ip_ranges))
        assert all(elem in model_ip_ranges for elem in updated_ip_ranges)

        model_resources = [
            {
                "name": resource.name,
                "description": resource.description,
                "location": resource.location,
                "contact": resource.contact,
            }
            for resource in Resource.objects.all()
        ]
        self.assertEqual(len(model_resources), len(updated_resources))
        # Add the missing default field to resources to match model's default
        updated_resources[1]["description"] = ""
        updated_resources[1]["contact"] = ""
        assert all(elem in model_resources for elem in updated_resources)
        self.client.logout()

    def test_update_same_name_as_code(self):
        url = get_project_detail_url(self.project1)
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        data = self.client.get(url).json()
        # Update project field
        data.update({"name": self.project1.code})
        response = self.client.put(url, data, content_type=APPLICATION_JSON)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.logout()

    def test_update_with_node_admin(self):
        self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
        node1 = self.destination1.code
        node3 = self.destination3.code
        na_code = self.project_node_admin.code
        counter = 0
        for project, code, destination, expected_status_code in (
            # node admin can update projects they manage
            (self.project_node_admin, na_code, node3, status.HTTP_200_OK),
            # node admin cannot update destination of a project
            (self.project_node_admin, na_code, node1, status.HTTP_403_FORBIDDEN),
            # node admin can update the code of a project
            (self.project_node_admin, "other_code", node3, status.HTTP_200_OK),
            # node admin cannot update the node of another project to their managed node
            (self.project1, self.project1.code, node3, status.HTTP_403_FORBIDDEN),
            # node admin cannot update projects with a different node from their
            # managed node
            (self.project1, self.project1.code, node1, status.HTTP_403_FORBIDDEN),
        ):
            counter += 1
            url = get_project_detail_url(project)
            data = {
                "name": f"test-{counter}",
                "code": code,
                "destination": destination,
                "legal_support_contact": "legal@legal.ch",
                "expiration_date": "2023-01-01",
                "ip_address_ranges": self.ip_address_ranges,
                "resources": self.resources,
                "users": [
                    {
                        "id": self.new_user.pk,
                        "roles": [
                            ProjectRole.USER.name,
                        ],
                    }
                ],
            }
            response = self.client.put(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                response.status_code,
                expected_status_code,
            )
            if 200 <= expected_status_code < 300:
                response_json = response.json()
                assert response_json["name"] == data["name"]
                assert response_json["code"] == data["code"]

    def test_update_archived(self):
        for user, project in (
            (self.node_admin.username, self.project_node_admin),
            (self.staff.username, self.project1),
            (self.pl_user.username, self.project1),
        ):
            self.client.login(username=user, password=USER_PASSWORD)
            project.archived = True
            project.save()
            url = get_project_detail_url(project)
            payload = self.users_payload(
                (
                    ProjectRole.USER,
                    ProjectRole.DM,
                    ProjectRole.PM,
                )
            )
            payload["destination"] = project.destination.code
            payload["name"] = "New Name"
            payload["code"] = project.code
            self.assertContains(
                self.client.put(
                    url,
                    payload,
                    content_type=APPLICATION_JSON,
                ),
                ARCHIVED_READ_ONLY_MESSAGE,
                status_code=status.HTTP_400_BAD_REQUEST,
            )
            self.client.logout()

    def test_assign_inactive_user(self):
        make_project_user(self.project1, ProjectRole.DM, self.new_user)
        self.client.login(
            username=self.permission_manager.username,
            password=USER_PASSWORD,
        )
        self.new_user.is_active = False
        self.new_user.save()
        url = get_project_detail_url(self.project1)
        response = self.client.put(
            url,
            self.users_payload((ProjectRole.USER,)),
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.logout()

    def test_archive_unarchive(self):
        for user, role in (
            (self.new_user, ProjectRole.DM),
            (self.new_user, ProjectRole.USER),
            (self.pl_user, ProjectRole.PL),
            (self.pl_user, ProjectRole.USER),
            (self.basic_user, ProjectRole.USER),
        ):
            ProjectUserRoleFactory(
                project=self.project_node_admin, user=user, role=role.value
            )

        archive_url = get_project_archive_url(self.project_node_admin)
        unarchive_url = get_project_unarchive_url(self.project_node_admin)
        for user in (self.staff.username, self.node_admin.username):
            self.client.login(username=user, password=USER_PASSWORD)

            mail.outbox = []

            # here it archives
            response = self.client.put(archive_url)

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertTrue(response.json()["archived"])
            self.assertTrue(Project.objects.get(pk=self.project_node_admin.pk).archived)

            self.assertEqual(len(mail.outbox), 2)

            assert mail.outbox[0].body == project_archive_users_notification(
                self.project_node_admin.name
            )
            self.assertEqual(
                sorted(mail.outbox[0].to),
                sorted(
                    [self.new_user.email, self.pl_user.email, self.basic_user.email]
                ),
            )

            assert mail.outbox[1].body == "\n".join(
                (
                    f"Project Name: {self.project_node_admin.name}",
                    f"Project Code: {self.project_node_admin.code}",
                    "",
                    "Roles and Users:",
                    f"PL: {self.pl_user.profile.display_name}",
                    f"DM: {self.new_user.profile.display_name}",
                    (
                        f"USER: {self.new_user.profile.display_name}, "
                        f"{self.pl_user.profile.display_name}, "
                        f"{self.basic_user.profile.display_name}"
                    ),
                )
            )
            assert mail.outbox[1].to == [
                self.project_node_admin.destination.ticketing_system_email
            ]
            mail.outbox = []

            # here it unarchives
            response = self.client.put(unarchive_url)

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertFalse(response.json()["archived"])
            self.assertFalse(
                Project.objects.get(pk=self.project_node_admin.pk).archived
            )

            self.assertEqual(len(mail.outbox), 2)

            self.client.logout()

    def test_archive_unarchive_unauthorized(self):
        archive_url = get_project_archive_url(self.project1)
        unarchive_url = get_project_unarchive_url(self.project1)
        for user in (
            self.basic_user.username,
            self.node_admin.username,  # of a different node
            self.pl_user.username,
        ):
            self.client.login(username=user, password=USER_PASSWORD)

            response = self.client.put(archive_url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertFalse(Project.objects.get(pk=self.project1.pk).archived)

            response = self.client.put(unarchive_url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertFalse(Project.objects.get(pk=self.project1.pk).archived)

            self.client.logout()

    def test_archive_dtr_expired(self):
        data_transfer = DataTransferFactory.create()
        archive_url = get_project_archive_url(data_transfer.project)
        self.assertEqual(data_transfer.status, DataTransfer.INITIAL)
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        self.client.put(archive_url)
        self.assertEqual(
            DataTransfer.objects.get(pk=data_transfer.pk).status, DataTransfer.EXPIRED
        )
        self.client.logout()

    service_data = {
        "name": "testservice",
        "action": "CREATE",
        "data": '{"key":"value"}',
    }

    def test_project_service_crud(self):
        url = get_project_services_url(self.project_s.id)

        self.client.login(username=self.basic_user_s.username, password=USER_PASSWORD)
        # Ordinary project users should be able to see the project services
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ordinary users cannot create services for projects
        response = self.client.post(
            url, self.service_data, content_type=APPLICATION_JSON
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Only node_admin or portal user can create services
        self.client.login(username=self.node_admin_s.username, password=USER_PASSWORD)
        response = self.client.post(
            url,
            self.service_data,
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        service = response.json()
        assert {
            "project_id": str(self.project_s.id),
            "user_id": None,
            "name": self.service_data["name"],
            "description": "",
            "data": self.service_data["data"],
            "action": self.service_data["action"],
            "action_timestamp": None,
            "state": "INITIAL",
            "locked": None,
            "archived": None,
        }.items() <= service.items()

        assert service["created"] is not None
        assert service["changed"] is not None

        # No two project services with the same name, regardless of upper/lowercase
        self.assertContains(
            response=self.client.post(
                url,
                self.service_data | {"name": "TESTSERVICE"},
                content_type=APPLICATION_JSON,
            ),
            text="already exists",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

        response = self.client.post(
            url,
            self.service_data | {"name": "another service"},
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(url)
        services = response.json()
        self.assertEqual(len(services), 2)

        ### Update existing project service
        data_up = {"state": "CREATED", "action": "NONE"}
        project_service_url = get_project_service_detail_url(
            self.project_s.id, service["id"]
        )

        response = self.client.patch(
            project_service_url, data_up, content_type=APPLICATION_JSON
        )
        service_up = response.json()
        self.assertEqual(service_up, service_up | data_up)
        self.assertNotEqual(service["changed"], service_up["changed"])

        ### Delete project service
        self.service_delete(project_service_url)

        self.client.logout()

    def test_project_member_service_crud(self):
        url = get_member_services_url(self.project_s.id, self.basic_user_s.id)

        self.client.login(username=self.basic_user_s.username, password=USER_PASSWORD)
        # Ordinary project members should be able to see their services
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ordinary users cannot create services for project members
        response = self.client.post(
            url,
            self.service_data,
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Only node_admin or portal user can create services
        self.client.login(username=self.node_admin_s.username, password=USER_PASSWORD)
        response = self.client.post(
            url,
            self.service_data,
            content_type=APPLICATION_JSON,
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        service = response.json()
        assert {
            "project_id": str(self.project_s.id),
            "user_id": str(self.basic_user_s.id),
            "name": self.service_data["name"],
            "description": "",
            "data": self.service_data["data"],
            "action": self.service_data["action"],
            "action_timestamp": None,
            "state": "INITIAL",
            "locked": None,
            "archived": None,
        }.items() <= service.items()
        assert service["created"] is not None
        assert service["changed"] is not None

        # No two member services with the same name, regardless of case
        self.assertContains(
            response=self.client.post(
                url,
                self.service_data | {"name": "TESTSERVICE"},
                content_type=APPLICATION_JSON,
            ),
            text="already exists",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

        ### Update existing member service
        data_up = {"state": "CREATED", "action": "NONE"}
        member_service_url = get_member_service_detail_url(
            self.project_s.id, self.basic_user_s.id, service["id"]
        )
        response = self.client.patch(
            member_service_url, data_up, content_type=APPLICATION_JSON
        )
        service_up = response.json()
        self.assertEqual(service_up, service_up | data_up)
        self.assertNotEqual(service["changed"], service_up["changed"])

        ### Delete member service
        self.service_delete(member_service_url)

        self.client.logout()

    def service_delete(self, url):
        self.assertEqual(
            self.client.get(url).status_code,
            status.HTTP_200_OK,
        )
        self.assertEqual(
            self.client.delete(url).status_code,
            status.HTTP_204_NO_CONTENT,
        )
        self.assertEqual(
            self.client.get(url).status_code,
            status.HTTP_404_NOT_FOUND,
        )


@pytest.mark.django_db
def test_project_get_queryset(rf, user_factory, node_factory, project_factory):
    node = node_factory()

    node_project = project_factory(destination=node)
    node_viewer, _ = make_node_viewer(node=node)

    project_with_user = project_factory()
    project_user = make_project_user(project=project_with_user)

    project_factory()  # Extra project that only staff should see
    staff = user_factory(staff=True)
    regular_user = user_factory(staff=False)

    request = rf.get("/")

    # Staff users see all projects
    request.user = staff
    assert set(ProjectViewSet(request=request).get_queryset()) == set(
        Project.objects.all()
    )

    # Regular users see no projects
    request.user = regular_user
    assert ProjectViewSet(request=request).get_queryset().count() == 0

    # Node users see node projects
    request.user = node_viewer
    assert set(ProjectViewSet(request=request).get_queryset()) == {node_project}

    # Project users see their projects
    request.user = project_user
    assert set(ProjectViewSet(request=request).get_queryset()) == {project_with_user}


VISIBLE = "visible"
OTHER = "other"


@pytest.mark.django_db
@pytest.mark.parametrize(
    "user_maker,target_project,expected",
    # Staff users see history for all projects
    tuple((lambda: UserFactory(staff=True), code, True) for code in (VISIBLE, OTHER))
    # Privileged project users (PL/PM) see history for all/only their projects
    + tuple(
        (
            lambda: make_project_user(
                project=Project.objects.get(code=VISIBLE), role=ProjectRole.PL
            ),
            code,
            expected,
        )
        for code, expected in zip((VISIBLE, OTHER), (True, False))
    )
    # Node personnel see history for all/only projects hosted at their node
    + tuple(
        (
            lambda: make_node_viewer(
                node=Project.objects.get(code=VISIBLE).destination
            )[0],
            code,
            expected,
        )
        for code, expected in zip((VISIBLE, OTHER), (True, False))
    ),
)
def test_project_user_role_history_queryset(user_maker, target_project, expected, rf):
    ProjectUserRoleHistoryFactory(project=ProjectFactory(code=VISIBLE))
    ProjectUserRoleHistoryFactory(project=ProjectFactory(code=OTHER))
    user = user_maker()
    request = rf.get("/")
    request.user = user

    assert (
        ProjectUserRoleHistoryViewSet(request=request)
        .get_queryset()
        .filter(project__code=target_project)
        .exists()
        == expected
    )
