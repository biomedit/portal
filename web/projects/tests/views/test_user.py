import csv

import pytest

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import assign_perm
from rest_framework import status

from identities.models import GroupProfile
from identities.permissions import has_change_user_permission, perm_change_user
from projects.apps import APP_NAME
from projects.models.data_provider import (
    has_any_data_provider_permissions,
    get_data_provider_role_group,
)
from projects.models.flag import Flag
from projects.models.node import has_node_admin_nodes, get_node_role_group
from projects.models.project import ProjectRole, ProjectUserRole
from projects.views.user import list_users
from .. import APPLICATION_JSON
from ..factories import (
    USER_PASSWORD,
    CustomAffiliationFactory,
    DataProviderFactory,
    FlagFactory,
    GroupFactory,
    NodeFactory,
    ProjectFactory,
    ProjectUserRoleFactory,
    UserFactory,
    UserNamespaceFactory,
    make_data_provider_admin,
    make_data_provider_viewer,
    make_node_admin,
    make_node_viewer,
    make_project_user,
)

FLAG_NAME = "flag"
User = get_user_model()


class TestUserView(TestCase):
    def setUp(self):
        self.project1, self.project2 = ProjectFactory.create_batch(2)
        self.basic_user = make_project_user(self.project1, ProjectRole.USER)
        self.staff = UserFactory(staff=True)
        self.pm_user = make_project_user(self.project1, ProjectRole.PM)
        self.dm_user = make_project_user(self.project1, ProjectRole.DM)
        self.user_with_perm = UserFactory()
        self.inactive_user = UserFactory(is_active=False)
        assign_perm(perm_change_user, self.user_with_perm)
        self.flag = FlagFactory(code=FLAG_NAME)
        self.node = NodeFactory()
        self.dp = DataProviderFactory()
        self.node_admin, _ = make_node_admin(node=self.node)
        self.node_viewer, _ = make_node_viewer(node=self.node)
        self.dp_admin, _ = make_data_provider_admin(data_provider=self.dp)
        self.dp_viewer, _ = make_data_provider_viewer(data_provider=self.dp)
        self.norole_user = make_project_user(self.project1, ProjectRole.NO_ROLE)
        self.custom_affiliation = CustomAffiliationFactory()
        self.url = reverse(f"{APP_NAME}:user-list")

    def test_user_view_list(self):
        # Basic and no-role user
        for user in (self.basic_user, self.norole_user):
            self.client.login(username=user.username, password=USER_PASSWORD)
            # Returns request user
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), 1)
            self.assertEqual(r.json()[0]["username"], user.username)
            # Access user by path
            r = self.client.get(self.url + str(user.pk) + "/")
            self.assertEqual(r.json()["username"], user.username)
            # Filter by `doesnotexist`
            r = self.client.get(self.url, {"doesnotexist": "nonsense"})
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()[0]["username"], user.username)
            self.client.logout()

        # staff
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        # All
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), User.objects.count() - 1)
        for user in r.json():
            self.assertTrue(user["is_active"])
        r = self.client.get(self.url, {"include_inactive": True})
        self.assertEqual(len(r.json()), User.objects.count())
        # Filter by email
        r = self.client.get(self.url, {"email": self.basic_user.email})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 1)
        r = self.client.get(self.url, {"email": self.inactive_user.email})
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.json()), 0)
        # Access user by path
        r = self.client.get(self.url + str(self.basic_user.pk) + "/")
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.json()["username"], self.basic_user.username)
        r = self.client.get(self.url + str(self.inactive_user.pk) + "/")
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()

    def test_user_view_list_with_change_user_permission(self):
        self.client.login(username=self.user_with_perm.username, password=USER_PASSWORD)
        # User access by path is only granted for update operations
        r = self.client.get(self.url + str(self.basic_user.pk) + "/")
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()

    def _test_user_view_list_for_personnel(self, *users):
        for user in users:
            username = user.username
            self.client.login(username=username, password=USER_PASSWORD)
            # All users belonging to my entity
            r = self.client.get(self.url)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), len(users))
            # Filter by email: NAs can see all users when filtering by email
            r = self.client.get(self.url, {"email": self.basic_user.email})
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(len(r.json()), 1 if user == self.node_admin else 0)
            # Filter by wrong email
            r = self.client.get(self.url, {"email": "notanemail"})
            self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            # Access user by path
            r = self.client.get(self.url + str(self.basic_user.pk) + "/")
            self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)
            r = self.client.get(self.url + str(users[0].pk) + "/")
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.client.logout()

    def test_user_view_list_for_data_provider_personnel(self):
        self._test_user_view_list_for_personnel(self.dp_admin, self.dp_viewer)

    def test_user_view_list_for_node_personnel(self):
        self._test_user_view_list_for_personnel(self.node_admin, self.node_viewer)

    def test_view_list_user_project_role_filter(self):
        url = reverse(f"{APP_NAME}:user-list")
        self.client.login(username=self.staff.username, password=USER_PASSWORD)
        ProjectUserRoleFactory(
            project=self.project1, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectUserRoleFactory(
            project=self.project2, user=self.staff, role=ProjectRole.USER.value
        )
        ProjectFactory()

        user = ProjectRole.USER.name
        dm = ProjectRole.DM.name
        pl = ProjectRole.PL.name

        for parameters, expected_user_ids in (
            (
                {"project_id": self.project1.id},
                [
                    self.basic_user.id,
                    self.staff.id,
                    self.pm_user.id,
                    self.dm_user.id,
                    self.norole_user.id,
                ],
            ),
            ({"project_id": self.project2.id}, [self.staff.id]),
            ({"project_id": self.staff.id}, []),
            ({"role": user}, [self.basic_user.id, self.staff.id]),
            ({"role": dm}, [self.dm_user.id]),
            ({"role": pl}, []),
            (
                {"project_id": self.project1.id, "role": user},
                [self.basic_user.id, self.staff.id],
            ),
            ({"project_id": self.project2.id, "role": user}, [self.staff.id]),
            ({"project_id": self.project2.id, "role": dm}, []),
        ):
            response = self.client.get(url, parameters).json()
            assert sorted([usr["id"] for usr in response]) == expected_user_ids
        self.client.logout()

    def test_update_as_user_with_perm(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})

        self.client.login(username=self.user_with_perm.username, password=USER_PASSWORD)
        self.assertTrue(has_change_user_permission(self.user_with_perm))

        with self.subTest(
            "Changing 'local_username' and/or 'uid' possible if "
            "the user has the permission 'identities.change_user'"
        ):
            for expected_profile in (
                {"uid": 1000020},
                {"local_username": "new_user_local"},
                {"local_username": "cookie_monster", "uid": 1000050},
            ):
                user_change = {"profile": expected_profile}
                resp = self.client.patch(
                    url,
                    user_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                # Verify that user has been updated
                profile = User.objects.get(pk=self.basic_user.pk).profile
                assert expected_profile.items() <= profile.__dict__.items()

        with self.subTest(
            "Updating 'flags' possible if the user has the permission"
            " 'identities.change_user'"
        ):
            flag = Flag.objects.get(code=FLAG_NAME)
            assert flag.users.count() == 0
            user_change = {"flags": [FLAG_NAME]}
            resp = self.client.patch(
                url,
                user_change,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            flag = Flag.objects.get(code=FLAG_NAME)
            assert tuple(flag.users.all()) == (self.basic_user,)

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "update some specific user properties. Everything else will be ignored."
        ):
            for user_change in (
                {"first_name": "Kirkia"},
                {"last_name": "Douglasie"},
                {"email": "kirkia.douglasie@localhost"},
            ):
                resp = self.client.patch(
                    url,
                    user_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                for key, value in user_change.items():
                    self.assertEqual(resp.json()[key], value)

            for profile_change in (
                # Non-updatable fields
                {"profile": {"affiliation_id": "21501@institution.ch"}},
            ):
                resp = self.client.patch(
                    url,
                    profile_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                for key, value in profile_change["profile"].items():
                    self.assertIsNot(resp.json()["profile"][key], value)
                user = User.objects.get(pk=self.basic_user.pk)
                assert user == self.basic_user

            for no_change in (
                # Non-existing fields will be ignored
                {"not_a_user_field": 12345},
                {"this_is_really_not_there": "void"},
            ):
                resp = self.client.patch(
                    url,
                    no_change,
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(resp.status_code, status.HTTP_200_OK)
                for key in no_change:
                    self.assertTrue(key not in resp.json())

            new_local_username = "new_user_local"
            resp = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_id": "21501@institution.ch",
                        "local_username": new_local_username,
                    },
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            profile = User.objects.get(pk=self.basic_user.pk).profile
            assert profile.local_username == new_local_username
            assert profile.affiliation_id == self.basic_user.profile.affiliation_id

        with self.subTest(
            "Even with the permission 'identities.change_user' you can only "
            "use method PATCH"
        ):
            resp = self.client.put(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        self.client.logout()

    def test_update_as_user(self):
        default_namespace = "ch"
        user = User.objects.get(username=self.pm_user.username)
        user.profile.local_username = "user_local"
        user.profile.namespace = UserNamespaceFactory(name=default_namespace)
        user.profile.save()
        self.basic_user.profile.affiliation_consent = True
        self.basic_user.profile.save()
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.basic_user.pk})
        dm_url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.dm_user.pk})
        na_url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.node_admin.pk})

        self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
        with self.subTest("local_username is missing"):
            for value in ("", None):
                r = self.client.patch(
                    url,
                    {"profile": {"local_username": value}},
                    content_type=APPLICATION_JSON,
                )
                self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
            r = self.client.patch(url, {"profile": {}}, content_type=APPLICATION_JSON)
            self.assert_no_permission(r)

        with self.subTest(
            "should not be allowed to use modifying methods other than PATCH"
        ):
            r = self.client.put(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("`local_username` has a max length"):
            too_long_local_username = "a" * 100
            r = self.client.patch(
                url,
                {"profile": {"local_username": too_long_local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)

        with self.subTest("`local_username` has a min length"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "short"}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)
            response = r.json()
            self.assertEqual(
                response["detail"],
                "Ensure this value has at least 6 characters (it has 5).",
            )

        with self.subTest("local_username and namespace must be unique together"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": user.profile.local_username}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            response = r.json()
            assert "unique constraint" in response["detail"].lower()

        with self.subTest(
            "normal user should not be able to change first_name, last_name or email"
        ):
            r = self.client.patch(
                url,
                {
                    "first_name": "Chuck",
                    "last_name": "Norris",
                    "email": "chuck.norris@localhost",
                },
                content_type=APPLICATION_JSON,
            )
            response = r.json()
            self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)

        with self.subTest("Changing local_username is forbidden"):
            self.basic_user.profile.local_username = "user_local"
            self.basic_user.profile.save()
            r = self.client.patch(
                url,
                {"profile": {"local_username": "new_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("Revoking affiliation_consent is forbidden"):
            r = self.client.patch(
                url,
                {"profile": {"affiliation_consent": False}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "Changing username and revoking affiliation_consent is forbidden"
        ):
            r = self.client.patch(
                url,
                {
                    "profile": {
                        "affiliation_consent": False,
                        "local_username": "new_user_local",
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("Cannot change username when giving affiliation consent"):
            self.client.login(username=self.dm_user.username, password=USER_PASSWORD)
            self.dm_user.profile.local_username = "dm_username"
            self.dm_user.profile.save()
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "local_username": "local_us",
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "Can give affiliation consent independently of username already being set"
        ):
            r = self.client.patch(
                dm_url,
                {
                    "profile": {
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["local_username"], "dm_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], True)

        with self.subTest(
            "Can set both local_username and affiliation_consent at once"
        ):
            self.client.login(username=self.node_admin.username, password=USER_PASSWORD)
            r = self.client.patch(
                na_url,
                {
                    "profile": {
                        "local_username": "na_username",
                        "affiliation_consent": True,
                    }
                },
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(r.json()["profile"]["local_username"], "na_username")
            self.assertEqual(r.json()["profile"]["affiliation_consent"], True)

        self.client.logout()

    def test_update_as_staff(self):
        url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": self.user_with_perm.pk})
        self.client.login(username=self.staff.username, password=USER_PASSWORD)

        with self.subTest("should not be allowed to set username of someone else"):
            r = self.client.patch(
                url,
                {"profile": {"local_username": "my_user_local"}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest("should be allowed to set a custom affiliation for a user"):
            r = self.client.patch(
                url,
                {"profile": {"custom_affiliation": self.custom_affiliation.id}},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertEqual(
                r.json()["profile"]["custom_affiliation"], self.custom_affiliation.id
            )

        with self.subTest(
            "should not be allowed to give affiliation consent for someone else"
        ):
            r = self.client.patch(
                url,
                {"profile": {"affiliation_consent": True}},
                content_type=APPLICATION_JSON,
            )
            self.assert_no_permission(r)

        with self.subTest(
            "should remove objects associated to the user when setting `is_active` to"
            " `False`"
        ):
            p3_node = NodeFactory()
            active_user, node_admin_group = make_node_admin(node=p3_node)

            url = reverse(f"{APP_NAME}:user-detail", kwargs={"pk": active_user.pk})

            make_node_admin(self.staff, p3_node, node_admin_group)
            users = (active_user, self.staff)

            p3 = ProjectFactory(destination=p3_node)
            ProjectUserRoleFactory(
                project=p3,
                user=active_user,
                role=ProjectRole.USER.value,
            )
            make_project_user(p3, ProjectRole.DM, active_user)
            p3_staff_pl = ProjectUserRoleFactory(
                project=p3,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p4 = ProjectFactory(destination=NodeFactory())
            p4_staff_pl = ProjectUserRoleFactory(
                project=p4,
                user=self.staff,
                role=ProjectRole.PL.value,
            )

            p5 = ProjectFactory(destination=NodeFactory())
            ProjectUserRoleFactory(
                project=p5,
                user=active_user,
                role=ProjectRole.USER.value,
            )

            # project without ProjectUserRoles to check for raised exceptions
            ProjectFactory(destination=NodeFactory())

            data_provider = DataProviderFactory(nodes=[p3_node])
            coordinator_group = GroupFactory(
                name=f"Data Provider Coordinator {data_provider.code}",
                profile__role_entity=data_provider,
                profile__role=GroupProfile.Role.DATA_PROVIDER_COORDINATOR,
            )
            for u in users:
                make_data_provider_viewer(u, data_provider, coordinator_group)
            data_provider.save()

            flag = FlagFactory()
            flag.users.set(users)
            flag.save()

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertTrue(has_node_admin_nodes(active_user))

            r = self.client.patch(
                url,
                {"is_active": False},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

            self.assertEqual(
                ProjectUserRole.objects.filter(project=p3).get(), p3_staff_pl
            )
            self.assertEqual(
                ProjectUserRole.objects.filter(project=p4).get(), p4_staff_pl
            )
            self.assertEqual(ProjectUserRole.objects.filter(project=p5).count(), 0)

            self.assertTrue(has_node_admin_nodes(self.staff))
            self.assertFalse(has_node_admin_nodes(active_user))
            self.assertFalse(has_any_data_provider_permissions(active_user))
            self.assertEqual(flag.users.get(), self.staff)

        with self.subTest("should be able to change `is_active` to `True` again"):
            r = self.client.patch(
                url,
                {"is_active": True},
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_200_OK)

        self.client.logout()

    def test_create_local_user(self):
        url = reverse(f"{APP_NAME}:user-list")
        data = {
            "first_name": "Chuck",
            "last_name": "Norris",
            "username": "0815@portal.ch",
            "email": "chuck@norris.gov",
            "profile": {"local_username": "chuck_norris"},
            "flags": [],
        }

        with self.subTest("Creating local user as non-staff user is forbidden"):
            self.client.login(username=self.basic_user.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest(
            "Creating local user as user with 'identities.change_user' is forbidden"
        ):
            self.client.login(
                username=self.user_with_perm.username,
                password=USER_PASSWORD,
            )
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            self.assertEqual(
                r.status_code,
                status.HTTP_403_FORBIDDEN,
            )
            self.client.logout()

        with self.subTest("Create local user as staff"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            r = self.client.post(url, data, content_type=APPLICATION_JSON)
            response = r.json()
            self.assertEqual(r.status_code, status.HTTP_201_CREATED)
            self.assertEqual(response["first_name"], data["first_name"])
            self.assertEqual(response["last_name"], data["last_name"])
            self.assertEqual(response["email"], data["email"])
            self.assertEqual(response["username"], data["username"])
            self.assertEqual(
                response["profile"]["local_username"], data["profile"]["local_username"]
            )
            created_local_user = User.objects.get(id=response["id"])
            # make sure user cannot be used to log in
            self.assertFalse(created_local_user.has_usable_password())
            self.client.logout()

        with self.subTest("Cannot create user with non-unique email address"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            new_data = {
                **data,
                "username": "brandnew_username",
                "email": "chuck@norris.gov",
            }
            r = self.client.post(
                url,
                new_data,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            self.assertEqual(
                r.json(),
                {
                    "detail": (
                        "{'email': [ErrorDetail(string='user with this email address"
                        " already exists.', code='unique')]}"
                    )
                },
            )
            self.client.logout()

        with self.subTest("Cannot create user with non-unique username"):
            self.client.login(username=self.staff.username, password=USER_PASSWORD)
            new_data = {
                **data,
                "username": "0815@portal.ch",
                "email": "brandnew_email@norris.gov",
            }

            r = self.client.post(
                url,
                new_data,
                content_type=APPLICATION_JSON,
            )
            self.assertEqual(r.status_code, status.HTTP_409_CONFLICT)
            self.assertEqual(
                r.json(),
                {
                    "detail": (
                        "{'username': [ErrorDetail(string='This field must be unique.',"
                        " code='unique')]}"
                    )
                },
            )
            self.client.logout()

    def assert_no_permission(self, r):
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
        self.assertDictEqual(
            r.json(),
            {"detail": "You do not have permission to perform this action."},
        )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role, expected_length",
    (
        (None, 1),
        (
            GroupProfile.Role.NODE_VIEWER,
            3,
        ),
        (
            GroupProfile.Role.DATA_PROVIDER_VIEWER,
            2,
        ),
        (
            ProjectRole.NO_ROLE,
            1,
        ),
        (
            ProjectRole.USER,
            1,
        ),
        (
            ProjectRole.DM,
            2,
        ),
    ),
)
def test_list_user(
    role,
    expected_length,
    node_factory,
    data_provider_factory,
    project_factory,
    rf,
):
    node = node_factory()
    data_provider = data_provider_factory(nodes=[node])
    project = project_factory()

    # Users that should be listed
    make_node_viewer(node=node)
    make_data_provider_viewer(data_provider=data_provider)
    make_project_user(project=project, role=ProjectRole.USER)

    # Users that should not be listed
    other_node = node_factory()
    make_data_provider_viewer(data_provider=data_provider_factory(nodes=[other_node]))
    make_project_user()

    request = rf.request()
    match role:
        case None:
            request.user = UserFactory()
        case GroupProfile.Role.NODE_VIEWER:
            request.user = make_node_viewer(
                node=node,
                group=get_node_role_group(node, role=GroupProfile.Role.NODE_VIEWER),
            )[0]
        case GroupProfile.Role.DATA_PROVIDER_VIEWER:
            request.user = make_data_provider_viewer(
                data_provider=data_provider,
                group=get_data_provider_role_group(
                    data_provider,
                    role=GroupProfile.Role.DATA_PROVIDER_VIEWER,
                ),
            )[0]
        case ProjectRole.NO_ROLE | ProjectRole.USER | ProjectRole.DM:
            request.user = make_project_user(project=project, role=role)
        case _:
            raise ValueError(f"Unexpected role: {role}")
    request.query_params = {}
    assert len(list_users(request)) == expected_length


@pytest.mark.usefixtures("staff_user_auth")
def test_export_users(client, user_factory):
    users = user_factory.create_batch(3)
    url = reverse(f"{APP_NAME}:usersexport")
    r = client.get(
        url,
        content_type=APPLICATION_JSON,
    )
    assert r.status_code == status.HTTP_200_OK
    assert r.headers["Content-Type"] == "text/csv"
    content = list(csv.reader(r.content.decode().strip().split("\r\n")))
    username_index = content[0].index("Username")
    assert {u.username for u in users}.issubset(
        {line[username_index] for line in content}
    )
