import pytest
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from projects.apps import APP_NAME
from projects.serializers.feed import FeedSerializer


NOW = timezone.now()
ONE_DAY = timezone.timedelta(days=1)
YESTERDAY = timezone.now() - ONE_DAY
TOMORROW = timezone.now() + ONE_DAY
ONE_HOUR = timezone.timedelta(hours=1)


@pytest.mark.usefixtures("basic_user_auth")
@pytest.mark.parametrize(
    "from_date, until_date, expected",
    (
        # Active feeds
        (YESTERDAY, TOMORROW, True),
        (YESTERDAY, NOW + ONE_HOUR, True),
        (YESTERDAY, None, True),
        # Past feeds
        (YESTERDAY, NOW - ONE_HOUR, False),
        # Future feeds
        (NOW + ONE_HOUR, None, False),
        (NOW + ONE_HOUR, TOMORROW, False),
        (TOMORROW, None, False),
    ),
)
def test_only_active_feeds_are_returned(
    from_date,
    until_date,
    expected,
    feed_factory,
    client,
):
    feed = feed_factory(from_date=from_date, until_date=until_date)
    serialized_feed = FeedSerializer(feed).data
    response = client.get(reverse(f"{APP_NAME}:feed-list"))
    assert (serialized_feed in response.data) == expected


@pytest.mark.usefixtures("basic_user_auth")
def test_feed_order(client, feed_factory):
    from_yesterday = feed_factory(label="WARN", from_date=YESTERDAY)
    from_now = feed_factory(label="WARN", from_date=NOW)
    unbound = feed_factory(label="INFO")
    r = client.get(reverse(f"{APP_NAME}:feed-list"))
    assert r.status_code == status.HTTP_200_OK
    r_json = r.json()
    assert r_json == [
        FeedSerializer(f).data
        for f in (
            from_now,
            from_yesterday,
            unbound,
        )
    ]
