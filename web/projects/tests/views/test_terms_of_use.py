import io
import csv

import pytest
from django.urls import reverse
from rest_framework import status

from projects.apps import APP_NAME
from projects.models.terms_of_use import TermsOfUseAcceptance
from projects.views.terms_of_use import list_terms_of_use

from ..factories import (
    NodeFactory,
    ProjectFactory,
    make_node_viewer,
    USER_PASSWORD,
    UserFactory,
    make_project_user,
)


@pytest.fixture
def create_terms_of_use(terms_of_use_factory, node_factory):
    node = node_factory()
    node_viewer, _ = make_node_viewer(node=node)
    tou = terms_of_use_factory(node=node)
    tou.users.add(node_viewer)
    return node_viewer


def assert_terms_of_use(client):
    url = reverse(f"{APP_NAME}:termsofuse-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    terms_of_use_list = response.json()
    assert len(terms_of_use_list) == 1
    assert "users" not in terms_of_use_list[0]
    url = reverse(
        f"{APP_NAME}:termsofuse-detail", kwargs={"pk": terms_of_use_list[0]["id"]}
    )
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    return response.json()


def test_list_anonymous(client):
    url = reverse(f"{APP_NAME}:termsofuse-list")
    response = client.get(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_list_node_viewer(client, create_terms_of_use):
    nv = create_terms_of_use
    client.login(username=nv.username, password=USER_PASSWORD)
    terms_of_use = assert_terms_of_use(client)
    assert "users" in terms_of_use
    client.logout()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "user_maker, expected",
    [
        # Node viewer for the correct node
        (lambda node, _: make_node_viewer(node=node)[0], True),
        # Project user for the correct project
        (lambda _, project: make_project_user(project=project), True),
        # Node viewer for a different node
        (lambda node, _: make_node_viewer(node=NodeFactory(id=node.id + 1))[0], False),
        # Project user for a different project
        (
            lambda _, project: make_project_user(
                project=ProjectFactory(id=project.id + 1)
            ),
            False,
        ),
        # Staff user
        (lambda _, __: UserFactory(staff=True), True),
        # Unprivileged user
        (lambda _, __: UserFactory(basic=True), False),
    ],
)
def test_list_terms_of_use(
    project_factory, node_factory, terms_of_use_factory, user_maker, expected
):
    node = node_factory()
    project = project_factory(destination=node)
    terms_of_use_factory(node=node)

    user = user_maker(node, project)
    assert list_terms_of_use(user).exists() == expected


@pytest.mark.usefixtures("staff_user_auth")
def test_export_terms_of_use(
    client, terms_of_use_factory, user_factory, node_factory, project_factory
):
    # Create a node and then create 3 TOU instances on that node
    node = node_factory()
    tou1 = terms_of_use_factory(node=node, version="1.0", version_type="MAJOR")
    tou2 = terms_of_use_factory(node=node, version="1.1", version_type="MAJOR")
    tou3 = terms_of_use_factory(node=node, version="1.2", version_type="MINOR")
    # Make a list sorted by id (which should match the export ordering)
    tous = sorted([tou1, tou2, tou3], key=lambda t: t.id)
    node_id = node.id

    # Create a user with known details who accepted
    accepted_user = user_factory(
        first_name="John", last_name="Doe", email="john@example.com"
    )
    accepted_dates = []
    for tou in tous:
        acceptance = TermsOfUseAcceptance.objects.create(
            terms_of_use=tou, user=accepted_user
        )
        formatted_date = acceptance.created.strftime("%d/%m/%Y")
        accepted_dates.append(formatted_date)

    # Create a user who didn't accept any TOU
    # but is part of a project hosted at the node
    project = project_factory(destination=node)
    nonaccepted_user = make_project_user(project=project)

    # Create an unassociated user (a user that has not accepted any TOU,
    # and is not part of any project hosted at the node)
    unassociated_user = user_factory(
        first_name="Alice", last_name="Smith", email="alice@example.com"
    )

    # Make the export request
    url = reverse(f"{APP_NAME}:termsofuseexport", kwargs={"node_id": node_id})
    response = client.get(url, content_type="application/json")
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["Content-Type"] == "text/csv"

    # Decode and parse the CSV
    content = response.content.decode("utf-8")
    csv_file = io.StringIO(content)
    reader = csv.reader(csv_file)
    rows = list(reader)

    # There should be 3 rows: 1 header + 2 user rows (accepted and non-accepted)
    assert len(rows) == 3, f"Expected 3 rows in CSV, got {len(rows)} rows."

    # Verify header
    header = rows[0]
    expected_fixed = ["First Name", "Last Name", "Email"]
    expected_total_columns = len(expected_fixed) + len(tous)
    assert len(header) == expected_total_columns, (
        f"Expected {expected_total_columns} columns in header, got {len(header)}."
    )
    expected_dynamic_headers = [f"{tou.version} {tou.version_type}" for tou in tous]
    dynamic_headers = header[len(expected_fixed) :]
    assert dynamic_headers == expected_dynamic_headers, (
        f"Expected dynamic headers {expected_dynamic_headers}, got {dynamic_headers}."
    )

    # Identify rows for the accepted and non-accepted users
    accepted_row = None
    nonaccepted_row = None
    for row in rows[1:]:
        if row[2] == accepted_user.email:
            accepted_row = row
        elif row[2] == nonaccepted_user.email:
            nonaccepted_row = row

    assert accepted_row is not None, (
        f"No CSV row found for accepted user {accepted_user.email}"
    )
    assert nonaccepted_row is not None, (
        f"No CSV row found for non-accepted user {nonaccepted_user.email}"
    )

    # Verify accepted user row
    assert accepted_row[0] == accepted_user.first_name, (
        f"Expected first name '{accepted_user.first_name}', got '{accepted_row[0]}'"
    )
    assert accepted_row[1] == accepted_user.last_name, (
        f"Expected last name '{accepted_user.last_name}', got '{accepted_row[1]}'"
    )
    accepted_dynamic = accepted_row[len(expected_fixed) :]
    assert len(accepted_dynamic) == len(tous), (
        f"Expected {len(tous)} dynamic columns for accepted user, "
        f"got {len(accepted_dynamic)}"
    )
    for idx, cell in enumerate(accepted_dynamic):
        expected_date = accepted_dates[idx]
        assert cell == expected_date, (
            f"Expected dynamic column {idx} to be '{expected_date}', got '{cell}'"
        )

    # Verify non-accepted user row
    assert nonaccepted_row[0] == nonaccepted_user.first_name, (
        f"Expected first name '{nonaccepted_user.first_name}', "
        f"got '{nonaccepted_row[0]}'"
    )
    assert nonaccepted_row[1] == nonaccepted_user.last_name, (
        f"Expected last name '{nonaccepted_user.last_name}', got '{nonaccepted_row[1]}'"
    )
    nonaccepted_dynamic = nonaccepted_row[len(expected_fixed) :]
    assert len(nonaccepted_dynamic) == len(tous), (
        f"Expected {len(tous)} dynamic columns for non-accepted user, "
        f"got {len(nonaccepted_dynamic)}"
    )
    for idx, cell in enumerate(nonaccepted_dynamic):
        assert cell == "", (
            f"Expected dynamic column {idx} for non-accepted user to be empty, "
            f"got '{cell}'"
        )

    # Verify that the unassociated user is NOT present in any row of the export
    for row in rows[1:]:
        assert row[2] != unassociated_user.email, (
            f"Unassociated user {unassociated_user.email} should not be present "
            "in the export."
        )
