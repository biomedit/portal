from rest_framework.renderers import JSONRenderer

from . import SCIM_MEDIA_TYPE


# adds support for SCIM API's content-type and accept headers
class ScimRenderer(JSONRenderer):
    media_type = SCIM_MEDIA_TYPE
