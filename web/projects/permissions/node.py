from rest_framework.permissions import IsAuthenticated

from ..models.node import (
    has_node_admin_nodes,
    has_node_admin_permission,
    has_node_viewer_nodes,
    has_node_viewer_permission,
)


class IsNodeAdmin(IsAuthenticated):
    """User is a node admin"""

    def has_permission(self, request, view):  # noqa: ARG002 unused-method-argument
        return super().has_permission(request, view) and has_node_admin_nodes(
            request.user
        )

    def has_object_permission(
        self,
        request,
        view,  # noqa: ARG002 unused-method-argument
        obj,
    ):
        return has_node_admin_permission(request.user, obj)


class IsNodeViewer(IsAuthenticated):
    """User is a node viewer"""

    def has_permission(self, request, view):  # noqa: ARG002 unused-method-argument
        return super().has_permission(request, view) and has_node_viewer_nodes(
            request.user
        )

    def has_object_permission(
        self,
        request,
        view,  # noqa: ARG002 unused-method-argument
        obj,
    ):
        return has_node_viewer_permission(request.user, obj)
