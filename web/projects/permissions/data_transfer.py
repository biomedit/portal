from rest_framework.permissions import BasePermission

from ..models.data_transfer import DataTransfer
from ..models.project import ProjectRole, ProjectUserRole
from ..permissions import IsAuthenticated


class IsProjectDataManager(IsAuthenticated):
    def has_permission(self, request, view):
        user = request.user
        project_id = request.data.get("project")

        return super().has_permission(request, view) and (
            project_id is not None
            and ProjectUserRole.objects.filter(
                user__id=user.id,
                project_id=project_id,
                role=ProjectRole.DM.value,
            ).exists()
        )


class IsPatchingDataSpecification(BasePermission):
    def has_permission(self, request, _):
        """Returns true as long as the user is NOT trying to patch anything else
        than `data_specification`"""

        updatable_keys = {"data_specification"}
        request_keys = set(request.data.keys())

        return request_keys <= updatable_keys


class IsDataSpecificationApprover(IsAuthenticated):
    def has_object_permission(
        self,
        request,
        view,
        data_transfer: DataTransfer,
    ):
        user = request.user
        data_specification_approval_group = (
            data_transfer.project.data_specification_approval_group
        )
        return (
            super().has_permission(request, view)
            and data_specification_approval_group
            and user in data_specification_approval_group.user_set.all()
        )
