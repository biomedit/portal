from django.core.exceptions import ObjectDoesNotExist
from rest_framework.permissions import IsAuthenticated, SAFE_METHODS

from ..models.node import (
    Node,
    has_node_admin_permission,
    has_node_admin_nodes,
    has_node_viewer_permission,
)
from ..models.project import (
    ProjectRole,
    is_project_leader,
    is_project_manager,
    is_project_member,
)


class ProjectPermission(IsAuthenticated):
    modify_methods = ("PUT",)
    create_modify_methods = ("POST",) + modify_methods

    def has_permission(self, request, view):
        return super().has_permission(request, view) and self.project_has_permission(
            request
        )

    def has_object_permission(
        self,
        request,
        view,  # noqa: ARG002 unused-method-argument
        obj,
    ):
        return (
            request.method in SAFE_METHODS
            or request.user.is_staff
            or (
                request.method in self.modify_methods
                and obj.users.filter(
                    user__id=request.user.id,
                    role__in=(
                        ProjectRole.PM.value,
                        ProjectRole.PL.value,
                    ),
                ).exists()
            )
            or (
                request.method in self.create_modify_methods
                and has_node_admin_permission(request.user, obj.destination)
                # node admin cannot change node of a project
                and request.data["destination"] == obj.destination.code
            )
        )

    def project_has_permission(self, request):
        try:
            node = Node.objects.get(code=request.data["destination"])
        except (ObjectDoesNotExist, KeyError):
            node = None
        return (
            request.method in SAFE_METHODS
            or request.user.is_staff
            or (
                is_project_manager(request.user)
                and request.method in self.modify_methods
            )
            or (
                has_node_admin_permission(request.user, node)
                and request.method in self.create_modify_methods
            )
        )


class ProjectServiceReadPermission(IsAuthenticated):
    def has_permission(self, request, view) -> bool:
        """A project member should be able to see its services,
        as well as the services of other project members.
        Node viewers should be allowed to see all services of all
        project members."""
        user = request.user
        project = view.get_project()

        return super().has_permission(request, view) and (
            is_project_member(user, project)
            or has_node_admin_permission(user, project.destination)
            or has_node_viewer_permission(user, project.destination)
        )


class ProjectServiceWritePermission(IsAuthenticated):
    def has_permission(self, request, view) -> bool:
        """Only node admin should have permission to modify
        project member services.
        """
        user = request.user
        project = view.get_project()

        return super().has_permission(request, view) and has_node_admin_permission(
            user, project.destination
        )


class ProjectUniquePermission(ProjectPermission):
    modify_methods = ("POST",)

    def has_permission(self, request, view):
        return super().has_permission(request, view) or (
            request.method in self.modify_methods and has_node_admin_nodes(request.user)
        )


class IsNodeAdmin(IsAuthenticated):
    """User is a node admin for this project's node"""

    def has_object_permission(self, request, view, obj):
        return super().has_permission(request, view) and has_node_admin_permission(
            request.user, obj.destination
        )


class IsNodeViewer(IsAuthenticated):
    """User is a node viewer for this project's node"""

    def has_object_permission(self, request, view, obj):
        return super().has_permission(request, view) and has_node_viewer_permission(
            request.user, obj.destination
        )


class IsPermissionManager(IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and is_project_manager(
            request.user
        )


class IsProjectLeader(IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and is_project_leader(request.user)
