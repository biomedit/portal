import logging

from django.contrib.auth import get_user_model
from rest_framework.permissions import (
    BasePermission,
    DjangoModelPermissions,
    IsAuthenticated,
)
from identities.permissions import has_change_user_permission
from ..models.user import Profile
from ..models.node import has_node_admin_permission, has_node_viewer_permission
from ..models.project import is_project_member

logger = logging.getLogger(__name__)

User = get_user_model()


class UserModelPermission(DjangoModelPermissions):
    """Apply same permissions to user object than to user model.
    `has_object_permission` returns `True` by default in `super()`.
    Also ensures that caller is applying PATCH updates ONLY.
    """

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.method == "PATCH"

    def has_object_permission(
        self,
        request,
        view,
        obj,  # noqa: ARG002 unused-method-argument
    ):
        return self.has_permission(request, view)


class IsAllowedPatch(BasePermission):
    def has_object_permission(
        self,
        request,
        view,  # noqa: ARG002 unused-method-argument
        obj,
    ):
        updatable_keys = {"profile", "email"}
        profile_information_keys = {"local_username", "affiliation_consent"}
        request_keys = set(request.data.keys())
        request_profile_keys = set(request.data.get("profile", {}).keys())

        def request_contains_profile_information():
            return (
                request_profile_keys & profile_information_keys
            ) or "email" in request_keys

        def request_contains_only_profile_information():
            if not request_keys <= updatable_keys:
                return False

            if not request_profile_keys <= profile_information_keys:
                return False

            return True

        def request_does_not_overwrite_profile_information():
            profile, _ = Profile.objects.get_or_create(user=obj)

            return not any(getattr(profile, key) for key in request_profile_keys)

        def user_updates_itself():
            return request.user.id == obj.id

        if request_contains_profile_information():
            return (
                user_updates_itself()
                and request_does_not_overwrite_profile_information()
                and (
                    request_contains_only_profile_information()
                    or request.user.is_staff
                    or has_change_user_permission(request.user)
                )
            )

        return request.user.is_staff or has_change_user_permission(request.user)


class MemberPermission(IsAuthenticated):
    def has_permission(self, request, view):
        """Only node admin should have permission to see project members.
        A project member should be able to see its other members.
        """
        user = request.user
        project = view.get_project()

        return super().has_permission(request, view) and (
            is_project_member(user, project)
            or has_node_admin_permission(user, project.destination)
            or has_node_viewer_permission(user, project.destination)
        )
