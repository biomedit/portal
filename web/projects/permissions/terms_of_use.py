from ..models.node import Node, has_node_admin_permission
from . import IsAuthenticated


class IsNodeAdmin(IsAuthenticated):
    """User is a node admin for the terms of use's node"""

    def has_permission(self, request, view):  # noqa: ARG002 unused-method-argument
        node_id = request.data.get("node")

        return (
            super().has_permission(request, view)
            and Node.objects.filter(id=node_id).exists()
            and has_node_admin_permission(request.user, Node.objects.get(id=node_id))
        )


class OwnsTermsOfUseAcceptance(IsAuthenticated):
    """User is the owner of the terms of use acceptance"""

    def has_permission(self, request, view):
        return super().has_permission(request, view) and int(
            request.data["user"]
        ) == int(request.user.id)
