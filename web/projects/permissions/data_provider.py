from rest_framework.permissions import IsAuthenticated

from ..models.data_provider import (
    has_any_data_provider_permissions,
    has_data_provider_admin_data_providers,
    has_data_provider_admin_permission,
)
from ..models.node import has_any_node_permissions
from ..models.project import is_data_manager
from ..models.user import is_data_specification_approver


class IsDataProviderAdmin(IsAuthenticated):
    """User is a data provider admin"""

    def has_permission(self, request, view):  # noqa: ARG002 unused-method-argument
        return super().has_permission(
            request, view
        ) and has_data_provider_admin_data_providers(request.user)

    def has_object_permission(
        self,
        request,
        view,  # noqa: ARG002 unused-method-argument
        obj,
    ):
        return has_data_provider_admin_permission(request.user, obj)


class ViewDataProvider(IsAuthenticated):
    def has_permission(self, request, view):
        return super().has_permission(request, view) and (
            has_any_node_permissions(request.user)
            or has_any_data_provider_permissions(request.user)
            or is_data_manager(request.user)
            or is_data_specification_approver(request.user)
        )
