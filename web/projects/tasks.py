import logging
import time
from datetime import date, timedelta

from django.conf import settings
from django_drf_utils.email import sendmail
from procrastinate import builtin_tasks
from procrastinate.contrib.django import app

from .models.pgp import PgpKeyInfo
from .models.project import Project, ProjectRole, ProjectUserRole
from .notifications.pgp_key_info import (
    pgp_key_info_status_updated,
    pgp_key_info_status_all_updated,
)
from .notifications.project_user_notification import (
    review_user_roles_notification,
    project_expiration_notification,
)
from .utils.pgp import KeyDownloadError, KeyStatus, download_key_metadata
from .views.project import archive_project

logger = logging.getLogger(__name__)


# Projects that expire "today" are still valid until midnight.
# Check if any non-archived project expired yesterday.
@app.periodic(cron="0 3 * * *")
@app.task
def archive_expired_project(timestamp: int):
    yesterday = date.fromtimestamp(timestamp) - timedelta(days=1)
    for project in Project.objects.filter(archived=False):
        if project.expiration_date is not None and yesterday >= project.expiration_date:
            archive_project(project, True)


def user_roles_recipients_exp_warning(project_user_roles):
    return [
        role.user.email
        for role in project_user_roles.filter(
            role__in=[ProjectRole.PL.value, ProjectRole.PM.value, ProjectRole.DM.value]
        )
    ]


# This job must only run once per day, otherwise it will send too many emails.
# Send an email 3 months in advance but could possibly send emails closer to
# expiration date.
@app.periodic(cron="0 3 * * *")
@app.task
def send_notification_email_expiring_projects(timestamp: int):
    date_in_3_months = date.fromtimestamp(timestamp) + timedelta(days=90)
    for project in Project.objects.filter(archived=False):
        expiration_date = project.expiration_date
        recipients = ProjectUserRole.objects.filter(project=project)
        if date_in_3_months == project.expiration_date:
            subject, body = project_expiration_notification(
                project.name, expiration_date
            )
            sendmail(
                subject=subject,
                body=body,
                recipients=user_roles_recipients_exp_warning(recipients),
                email_cfg=settings.CONFIG.email,
            )


def review_user_roles_recipients(project_user_roles):
    return [
        role.user.email
        for role in project_user_roles.filter(
            role__in=[ProjectRole.PL.value, ProjectRole.PM.value]
        )
    ]


def send_review_user_roles_notification():
    for project in Project.objects.filter(archived=False):
        project_user_roles = ProjectUserRole.objects.filter(project=project)
        subject, body = review_user_roles_notification(
            project.name,
            {(role.user, ProjectRole(role.role)) for role in project_user_roles},
        )
        sendmail(
            subject=subject,
            body=body,
            recipients=review_user_roles_recipients(project_user_roles),
            email_cfg=settings.CONFIG.email,
        )


@app.periodic(cron="0 3 15 5,11 *")
@app.task
def review_user_roles(
    timestamp: int,  # noqa: ARG001 unused-function-argument
):
    send_review_user_roles_notification()
    logger.info("Successfully sent notification emails for user roles review")


@app.periodic(cron="0 */4 * * *")
@app.task
def update_pgp_keys(
    timestamp: int,  # noqa: ARG001 unused-function-argument
    delay: float = 0.25,
):
    """Update PGP key info from a keyserver.

    Update the PGP key status based on key information received from a
    keyserver, e.g. in the event of removing user ID from a key.

    :param delay: delay in seconds between subsequent queries to a keyserver.
        The current rate limits are available at
        https://keys.openpgp.org/about/api#rate-limiting
    """

    keys = PgpKeyInfo.objects.exclude(
        status__in=(
            PgpKeyInfo.Status.DELETED,
            PgpKeyInfo.Status.KEY_REVOKED,
            PgpKeyInfo.Status.APPROVAL_REVOKED,
            PgpKeyInfo.Status.REJECTED,
        )
    )
    errors = []
    changed: list[PgpKeyInfo] = []
    key: PgpKeyInfo
    for key in keys:
        try:
            metadata = download_key_metadata(
                fingerprint=key.fingerprint,
                keyserver_url=settings.CONFIG.pgp.keyserver,
            )
            match metadata.status:
                case KeyStatus.NONVERIFIED:
                    key.status = PgpKeyInfo.Status.DELETED
                    changed.append(key)
                case KeyStatus.REVOKED:
                    key.status = PgpKeyInfo.Status.KEY_REVOKED
                    changed.append(key)
        except KeyDownloadError as e:
            errors.append(f"{key.fingerprint} {e}")
        time.sleep(delay)

    if changed:
        updated_count = PgpKeyInfo.objects.bulk_update(
            changed, ("status",), batch_size=50
        )
        logger.info(
            "Changed status of %s OpenPGP keys: %s",
            updated_count,
            ", ".join(key.fingerprint for key in changed),
        )
        # Create a ticket by email
        subject, body = pgp_key_info_status_all_updated(changed)
        sendmail(
            subject,
            body,
            recipients=(settings.CONFIG.notification.ticket_mail,),
            email_cfg=settings.CONFIG.email,
        )
        # Email each UserID
        for key in changed:
            subject, body = pgp_key_info_status_updated(key)
            sendmail(
                subject,
                body,
                recipients=(key.key_email,),
                email_cfg=settings.CONFIG.email,
            )
    if errors:
        logger.error(
            "OpenPGP key update failed with the following errors: %s", "; ".join(errors)
        )
    not_changed_count = len(keys) - len(changed) - len(errors)
    if not_changed_count > 0:
        logger.debug("No changes detected in %s keys", not_changed_count)


@app.periodic(cron="0 5 * * *")
@app.task(queueing_lock="remove_finished_jobs", pass_context=True)
def remove_finished_jobs(
    context,
    timestamp: int,  # noqa: ARG001 unused-function-argument
):
    return builtin_tasks.remove_old_jobs(
        context,
        max_hours=72,  # 3 days
    )


@app.periodic(cron="0 5 * * *")
@app.task(queueing_lock="remove_old_jobs", pass_context=True)
def remove_old_jobs(
    context,
    timestamp: int,  # noqa: ARG001 unused-function-argument
):
    return builtin_tasks.remove_old_jobs(
        context,
        max_hours=336,  # 14 days
        remove_failed=True,
        remove_cancelled=True,
        remove_aborted=True,
    )
