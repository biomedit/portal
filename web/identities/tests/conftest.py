import pytest

from projects.tests.conftest import (  # noqa: F401 unused-import
    basic_user_auth,
    flag_factory,
    group_factory,
    node_factory,
    data_provider_factory,
    project_factory,
    project_user_role_factory,
    apply_notification_settings,
    user_factory,
)
from .factories import OAuth2TokenFactory, OAuth2ClientFactory


@pytest.fixture
def oauth2_token_factory(db):  # noqa: ARG001 unused-function-argument
    return OAuth2TokenFactory


@pytest.fixture
def oauth2_client_factory(db):  # noqa: ARG001 unused-function-argument
    return OAuth2ClientFactory
