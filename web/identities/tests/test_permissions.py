from django.test import RequestFactory

from projects.tests.factories import (
    make_group_manager,
)
from ..permissions import (
    IsGroupManager,
)


def test_is_group_manager(user_factory, group_factory):
    managed_group, other_group = group_factory.create_batch(2)
    group_manager = make_group_manager(managed_group)
    permission_object = IsGroupManager()

    for user, allowed in ((group_manager, True), (user_factory(), False)):
        request = RequestFactory().request()
        request.user = user
        assert permission_object.has_permission(request, None) is allowed
        assert (
            permission_object.has_object_permission(request, None, managed_group)
            is allowed
        )
        assert (
            permission_object.has_object_permission(request, None, other_group) is False
        )
