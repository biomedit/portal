from django.apps import AppConfig

APP_NAME = "identities"


class IdentitiesConfig(AppConfig):
    name = APP_NAME

    def ready(self):
        from . import signals  # noqa: F401 unused-import
