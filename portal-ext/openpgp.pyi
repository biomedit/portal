from enum import Enum
from typing import Any

class Validity(Enum):
    Expired: Any
    Invalid: Any
    Revoked: Any
    Unknown: Any
    Valid: Any

class UserID:
    name: str | None
    email: str | None
    comment: str | None
    validity: Validity
    validity_info: str | None

    def to_str_without_email(self) -> str | None: ...

class CertInfo:
    email: str | None
    fingerprint: str
    key_id: str
    keys: list[Key]
    primary_key: Key
    uid: UserID | None
    uids: list[UserID]
    validity: Validity
    validity_info: str | None

    @classmethod
    def from_bytes(cls, data: bytes, end_relax: int | None = None) -> CertInfo: ...

class CertParsingError(Exception): ...

class Key:
    key_id: str
    fingerprint: str
    length: int | None
    pub_key_algorithm: int
    validity: Validity
    validity_info: str | None
