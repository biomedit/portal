use pyo3::prelude::*;

mod openpgp;

/// Portal extra functionalities.
#[pymodule]
fn portal_ext(py: Python, m: &Bound<'_, PyModule>) -> PyResult<()> {
    let openpgp_module = PyModule::new(m.py(), "openpgp")?;
    openpgp_module.add(
        "CertParsingError",
        py.get_type::<openpgp::CertParsingError>(),
    )?;
    openpgp_module.add_class::<openpgp::CertInfo>()?;
    openpgp_module.add_class::<openpgp::UserID>()?;
    openpgp_module.add_class::<openpgp::Validity>()?;
    m.add_submodule(&openpgp_module)?;
    Ok(())
}
